<?php

namespace App\Notifications;

use App\Libraries\Channels\Msg91SmsChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserCreated extends Notification
{
    use Queueable;
    private $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', Msg91SmsChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Welcome to intellistocks')
                    ->line("Dear {$this->user->salutation},")
                    ->line('Welcome to intellistocks')
                    ->line('Your account has been created on intelliStocks, please go to this link and verify your Account')
                    ->action('Click to Verify', 'http://intellistocks.com/verify')
                    ->line('Thank you.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => "Dear ".$this->user->salutation.",".$this->message,
            'to' => $this->user->routeNotificationForMail(),
            'mobiles' => $this->user->routeNotificationForMsg91(),
        ];
    }
}
