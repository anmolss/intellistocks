<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Libraries\Channels\Msg91SmsChannel;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;

class SharePnl extends Notification implements ShouldBroadcast
{
    use Queueable;

    var $user, $message, $pnl_file;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $message, LaravelExcelWriter $excel)
    {
        $this->user = $user;
        $this->message = $message;
        $this->pnl_file = $excel;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [ Msg91SmsChannel::class, 'mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->to($notifiable->getAllMail())
                ->subject('Intellistocks: P&l Reports')
                ->greeting("Dear {$this->user->salutation},")
                ->line($this->message)
                ->line('Thank you.')
                ->attach($this->pnl_file->store("csv",false,true)['full']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => "Dear ".$this->user->salutation.", Service report shared, kindly check your email.",
            'to' => $notifiable->getAllMail(),
            'mobiles' => $notifiable->getAllMobile()
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toMsg91($notifiable)
    {
        return [
            'message' => "Dear ".$this->user->salutation.", Service report shared, kindly check your email.",
            'mobiles' => $notifiable->getAllMobile()
        ];
    }
}
