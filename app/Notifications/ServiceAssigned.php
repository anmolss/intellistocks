<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ServiceAssigned extends Notification
{
    use Queueable;
    private $user;
    private $user_service;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $user_service)
    {
        $this->user = $user;
        $this->user_service = $user_service;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Congratulations..It all begins at Rothmans Capital!')
                    ->line("Dear {$this->user->salutation},")
                    ->line("
                        <table border='0' width='680px' cellspacing='0' cellpadding='0'>
                            <tbody>
                                <tr>
                                    <td>
                                        <table cellspacing='0' cellpadding='0'>
                                            <tbody>
                                                <tr>
                                                    <th>Confirmation for {$this->user_service->name}</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h2>Hi! {$this->user->name}</h2>
                                                        <p>Dear {$this->user->name} we have processed your subscription. You are requested to confirm your following details to enable us to activate your services.</p>
                                                        <table cellspacing='0' cellpadding='5'>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Name</td>
                                                                    <td>{$this->user->name}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Mobile</td>
                                                                    <td>{$this->user->mobile}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Email</td>
                                                                    <td><a href='mailto:{$this->user->email}' target='_blank'>{$this->user->email}</a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Address</td>
                                                                    <td>{$this->user->address1}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>City</td>
                                                                    <td>{$this->user->city}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>State</td>
                                                                    <td>{$this->user->state}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Pincode</td>
                                                                    <td>{$this->user->zip_code}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Service</td>
                                                                    <td>{$this->user_service->name}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Activation Date</td>
                                                                    <td>{$this->user_service->activation_date}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Comments</td>
                                                                    <td>{$this->user_service->comments}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <p>Thank you for choosing Rothmans Capital. Kindly click the below button for verification.</p>")
                                                        ->action('I Agree, Confirm', 'http://192.168.1.126/crm/customerservices/verify/448')
                    ->line("
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>We are here to answer your questions and we genuinely value user feedback. So please do not hesitate to contact us at ​ 08527869576 or ​ <a href='mailto:services@rothmanscapital.com' target='_blank'>services@rothmanscapital.com</a> even if it is just to say Hi. <br /><br />Always at your service<br />Team Rothmans</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&nbsp;</p>");
/*
                    ->action('Click to Verify', 'http://intellistocks.com/verify')
                    ->line('Thank you.');
*/
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => "Dear ".$this->user->salutation.", Congratulations..It all begins at Rothmans Capital!",
            'to' => $this->user->routeNotificationForMail(),
        ];
    }
}
