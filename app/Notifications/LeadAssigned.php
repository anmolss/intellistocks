<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class LeadAssigned extends Notification
{
    use Queueable;
    private $lead;
    private $user;
    private $assigner;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($lead, $user, $assigner)
    {
        $this->lead = $lead;
        $this->user = $user;
        $this->assigner = $assigner;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('New Lead Assigned')
                    ->line("Hello {$this->user->name},")
                    ->line("{$this->assigner->name} has assigned you a New Lead - {$this->lead->title}")
                    ->action('View Lead', "http://intellistocks.com/leads/{$this->lead->id}")
                    ->line('Thank you.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
