<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Libraries\Channels\Msg91SmsChannel;

class AlertNotify extends Notification implements ShouldBroadcast
{
    use Queueable;

    var $user, $message, $notice_level;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $message, $notice_level)
    {
        $this->user = $user;
        $this->message = $message;
        $this->notice_level = $notice_level;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $notice_type = ['database'];
        foreach ($this->notice_level as $level){
            switch ($level){
                case 0:
                    $notice_type[] = 'database';
                    break;
                case 1:
                    $notice_type[] = Msg91SmsChannel::class;
                    break;
                case 2:
                    $notice_type[] = 'mail';
                    break;
                default:
                    $notice_type[] = 'database';
                    break;
            }
        }

        return array_unique($notice_type);

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->to($this->user->getAllMail())
                ->subject('Intellistocks Alert')
                ->greeting("Dear {$this->user->salutation}")
                ->line($this->message);
                //->view('admin.email.alert_email') ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => "Dear ".$this->user->salutation.",".$this->message,
            'to' => $this->user->getAllMail(),
            'mobiles' => $this->user->getAllMobile(),
            'notice_level' => $this->notice_level
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toMsg91($notifiable)
    {
        return [
            "message" => "Dear ".$this->user->salutation.",".$this->message,
            "mobiles" => $this->user->getAllMobile()
        ];
    }
}
