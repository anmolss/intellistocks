<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class LeadNoteCreated extends Notification
{
    use Queueable;
    private $lead;
    private $user;
    private $assigner;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($lead, $user, $note)
    {
        $this->lead = $lead;
        $this->user = $user;
        $this->note = $note;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        //
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'note' => $this->note->note,
            'lead_id' => $this->lead->id,
            'lead_title' => $this->lead->title,
            'created_at' => $this->note->created_at,
            'created_by' => $this->user->id,
            'creator_name' => $this->user->name,
        ];
    }
}