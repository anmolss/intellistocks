<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{

	public function services()
	{
	    return $this->belongsToMany(Service::class);
	}

	public function users()
	{
	    return $this->belongsTo(User::class, 'created_by');
	}

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }
}
