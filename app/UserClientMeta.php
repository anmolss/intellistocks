<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class UserClientMeta extends Model
{
	protected $table = 'user_client_meta';
	protected $guarded = ['id'];

	public function user()
    {
        return $this->belongsTo(User::class, 'fk_user_id', 'id');
    }

}