<?php

namespace App;


use Cviebrock\EloquentSluggable\Sluggable;

class ConfigType extends AppModel
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function configs()
    {
        return $this->hasMany(Configuration::class, 'type_id');
    }
}