<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = [
        'symbol', 'exchange', 'instrument',  'expiry', 'option_type', 'strike',
        'base_price', 'target', 'end_date', 'watchlist', 'created_by'
    ];
    public $appends = ['daily_bhav', 'cmp', 'prev_close'];

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function getDailyBhavAttribute(){
        $data = [];

        if ($this->exchange == 'nse'){
            $data = NseDailyBhav::where('symbol', $this->symbol)->first();
        } elseif ($this->exchange == 'nfo'){
            $data = NfoDailyBhav::where('symbol', $this->symbol)
                ->where('instrument', $this->instrument)
                ->where('expiry_date', $this->expiry);

            if(in_array($this->instrument , ['OPTIDX','OPTSTK'])){
                $data =  $data->where('strike', $this->strike)
                    ->where('option_type', $this->option_type);
            }
            $data = $data->first();
        }

        return $this->attributes['daily_bhav'] = $data;
    }

    public function getCmpAttribute(){
        if ($this->exchange == 'nse'){
            return $this->attributes['cmp'] = $this->daily_bhav['last'];
        } elseif ($this->exchange == 'nfo') {
            return $this->attributes['cmp'] = $this->daily_bhav['close'];
        }
    }

    public function getPrevCloseAttribute(){
        if ($this->exchange == 'nse'){
            return $this->attributes['prev_close'] = $this->daily_bhav['prevclose'];
        } elseif ($this->exchange == 'nfo') {
            return $this->attributes['prev_close'] = $this->daily_bhav['setle_pr'];
        }
    }
}
