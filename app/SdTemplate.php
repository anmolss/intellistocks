<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SdTemplate extends Model
{
    protected $guarded = [];

	public function services()
	{
	    return $this->belongsToMany(Service::class);
	}

	public function users()
	{
	    return $this->belongsTo(User::class, 'created_by');
	}


}
