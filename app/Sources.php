<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sources extends Model
{
    //Table name
    protected $table = 'sources';

    //Primary key
    public $primaryKey = 'id';


}
