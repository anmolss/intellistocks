<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ip extends Model
{
    protected $fillable = [
        'ip', 'is_global'
    ];
    public function teams()
    {
        return $this->morphedByMany('App\Team', 'restricted', 'setting_ips', 'ip_id');
    }
    public function users()
    {
        return $this->morphedByMany('App\User', 'restricted', 'setting_ips', 'ip_id');
    }
}
