<?php
namespace App\Libraries\Traits;

use Auth;

trait DeveloperTrait
{

    public static function bootDeveloperTrait()
    {
        static::addGlobalScope(new ManageDeveloperScope());
    }

}