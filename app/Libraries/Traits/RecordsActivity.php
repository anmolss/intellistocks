<?php
namespace App\Libraries\Traits;

use App\Activity;
use App\User;
use Auth;
use ReflectionClass;

trait RecordsActivity
{
    public static function bootRecordsActivity()
    {
        foreach (static::getModelEvents() as $event)
        {
            static::$event(function($model) use ($event) {
                $model->recordActivity($event);
            });
        }
    }

    protected function recordActivity($event)
    {
        $changed = [];
        if($event == 'updated')
        {
            $changed = $this->getDirty();
        }
        if(auth()->check())
        {
            Activity::create([
                'subject_id' => $this->id,
                'subject_type' => get_class($this),
                'name' => $this->getActivityName($this, $event),
                'user_id' => auth()->id(),
                'type' => 'user',
                'before' => json_encode(array_intersect_key($this->fresh()->toArray(), $changed)),
                'after' => json_encode($changed),
            ]);
        }
        else if(is_a($this, User::class)){
            Activity::create([
                'subject_id' => $this->id,
                'subject_type' => get_class($this),
                'name' => $this->getActivityName($this, $event),
                'text' => 'User was updated by' . $this->name,
                'user_id' => $this->id,
                'before' => json_encode(array_intersect_key($this->fresh()->toArray(), $changed)),
                'after' => json_encode($changed),
            ]);
        }

    }

    protected function getActivityName($model, $action)
    {
        $name  = strtolower((new ReflectionClass($model))->getShortName());
        return "{$action}_{$name}";
    }

    protected static function getModelEvents()
    {
        if(isset(static::$recordEvents))
        {     
            return static::$recordEvents;
        }
        
        return [
            'created', 'updated', 'saved'
        ];
    }
}