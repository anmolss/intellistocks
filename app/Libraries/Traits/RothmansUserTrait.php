<?php
namespace App\Libraries\Traits;

use Auth;
use Cache;
use Config;
use Laratrust\Traits\LaratrustUserTrait;


trait RothmansUserTrait
{
    use LaratrustUserTrait {
        LaratrustUserTrait::can as base_can;
    }

    public function can($permission, $requireAll = false)
    {
        if(Config::has('rothmans.bypass_emails') && Auth::check())
        {
            foreach (Config::get('rothmans.bypass_emails') as $email) {
                if (str_is( auth()->user()->email, $email) ) {
                    return true;
                }
            }
        }
        
        if (is_array($permission)) {
            foreach ($permission as $permName) {
                $hasPerm = $this->can($permName);

                if ($hasPerm && !$requireAll) {
                    return true;
                } elseif (!$hasPerm && $requireAll) {
                    return false;
                }
            }
            
            // If we've made it this far and $requireAll is FALSE, then NONE of the perms were found
            // If we've made it this far and $requireAll is TRUE, then ALL of the perms were found.
            // Return the value of $requireAll;
            return $requireAll;
        } else {
            foreach ($this->cachedRoles() as $role) {
                // Validate against the Permission table
                foreach ($role->cachedPermissions() as $perm) {
                    if (str_is( $permission, $perm->name) ) {
                        return true;
                    }
                }
            }

            foreach ($this->cachedPermissions() as $perm) {
                if (str_is( $permission, $perm->name) ) {
                    return true;
                }
            }
        }

        return false;
    }

    public function cachedPermissions()
    {
        $cacheKey = 'laratrust_permissions_for_role_' . $this->getKey();

        return Cache::remember($cacheKey, Config::get('cache.ttl', 60), function () {
            return $this->perms()->get();
        });
    }

}