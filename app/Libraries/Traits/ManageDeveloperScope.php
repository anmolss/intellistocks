<?php
namespace App\Libraries\Traits;

use Config;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;


class ManageDeveloperScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        if(!Config::get('rothmans.developers'))
        {
            $builder->whereNotIn('users.id', [1,2]);
        }
    }
}