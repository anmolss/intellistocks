<?php

namespace App\Libraries\FormElements;

use App\User;
class FileUploadService
{

    public function uploadImage($image = array())
    {
        if($image['userId'] != "")
        {
            $filename = './uploads/user/images/'.$image['userId'];
            $item = User::findOrFail($image['userId']);
        }
        else
        {
            $getUser = User::all()->last();
            $id = ++$getUser->id;
            $filename = './uploads/user/images/'.$id;
            $item = new User();
        }

        $originalImage = $image['image']->getClientOriginalName();

        if (!file_exists($filename)) {
            mkdir($filename);
            $response['create'] = 'file created';
            if(file_exists($filename)) {
                if(!empty($image)) {
                    $response['destination'] = $filename;
                    $image['image']->move($filename, $originalImage);
                    $item->image = $originalImage;
                    $item->save();
                    $response['uploaded'] = 'uploaded succesfully';
                }
                else{
                    $response['uploaded'] = 'uploaded unsuccesful';
                }
            }
        } else {
            if(file_exists($filename)) {
                if(!empty($image)) {
                    $response['destination'] = $filename;
                    $image['image']->move($filename, $originalImage);
                    $item->image = $originalImage;
                    $item->save();
                    $response['uploaded'] = 'uploaded succesfully';
                }
                else{
                    $response['uploaded'] = 'uploaded unsuccesful';
                }
            }
            $response['create'] = 'not create';
        }
        return $response;

    }

    public function getImages($url = '')
    {
        return $url;
    }

}