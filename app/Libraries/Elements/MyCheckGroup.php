<?php

namespace App\Libraries\Elements;

use AdamWathan\BootForms\Elements\CheckGroup;
use AdamWathan\Form\Elements\Element;
use AdamWathan\Form\Elements\Label;
use App\Libraries\Elements\MyLabel;

class MyCheckGroup extends CheckGroup
{

    public function __construct(Label $label)
    {
        $this->label = new MyLabel($label);
    }

    public function render()
    {
        if ($this->inline === true) {
//            $html = $this->label->getControl();
//            $html .= $this->label;
//
//            return $html;
            return $this->label->render();
        }

        $html = '<div';
        $html .= $this->renderAttributes();
        $html .= '>';
        $html .= $this->label->getControl();
        $html .= $this->label;
        $html .= $this->renderHelpBlock();

        $html .= '</div>';

        return $html;
    }


}