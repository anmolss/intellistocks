<?php
/**
 * Created by PhpStorm.
 * User: jayapaliwal
 * Date: 17/06/16
 * Time: 10:03 PM
 */

namespace App\Libraries\Elements;

use AdamWathan\BootForms\Elements\GroupWrapper;


class MyGroupWrapper extends GroupWrapper
{
    protected $formGroup;
    protected $target;

    public function __construct(GroupWrapper $formGroup)
    {
        $this->formGroup = $formGroup->formGroup;
        $this->target = $formGroup->target->control();
    }

    public function __call($method, $parameters)
    {
        call_user_func_array([$this->target, $method], $parameters);
        /*if($method == 'id')
        {
            if('App\Libraries\Elements\MyLabel' != get_class($this->target))
                return $this->label()->id($parameters[0]);

        }*/
        return $this;
    }

}