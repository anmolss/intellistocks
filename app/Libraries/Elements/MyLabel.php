<?php

namespace App\Libraries\Elements;

use AdamWathan\Form\Elements\Element;
use AdamWathan\Form\Elements\Label;

class MyLabel extends Label
{
    protected $element;

    protected $labelBefore;

    protected $label;

    public function __construct(Label $label)
    {
        $this->element = $label->getControl();
        $this->labelBefore = $label->labelBefore;
        $this->label = $label->label;
        $this->attributes = $label->attributes;
    }

    public function render()
    {
        $tags = [];

        if ($this->labelBefore) {
            $tags[] = $this->renderElement();
        }
        $tags[] = sprintf('<label%s>', $this->renderAttributes());

        $tags[] = $this->label;

        $tags[] = '</label>';


        if (! $this->labelBefore) {
            $tags[] = $this->renderElement();
        }
        return implode($tags);
    }

    public function forId($name)
    {
        $this->setAttribute('for', $name);

        return $this;
    }

    public function before(Element $element)
    {
        $this->element = $element;
        $this->labelBefore = true;

        return $this;
    }

    public function after(Element $element)
    {
        $this->element = $element;
        $this->labelBefore = false;

        return $this;
    }

    protected function renderElement()
    {
        if (! $this->element) {
            return '';
        }

        return $this->element->render();
    }

    public function getControl()
    {
        return $this->element;
    }
}
