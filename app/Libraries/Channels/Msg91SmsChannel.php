<?php
/**
 * Created by Jayant.
 * Usage: Use to send sms via this SoftIdea sms provider
 */

namespace App\Libraries\Channels;


use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;

class Msg91SmsChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        if(env('ALLOW_SMS') == 'no'){
            return false;
        }
        if (! $notifiable->routeNotificationFor('msg91')) {
            return;
        }

        $message = $notification->toMsg91($notifiable);

        if(is_array($message)){
            $msg =  $message['message'];
            $mobiles = $message['mobiles'];
        } else {
            $msg = $message;
            $mobiles = $notifiable->routeNotificationFor('msg91');
        }

        if(!$mobiles) {
            return;
        } else {
            if (is_array($mobiles)) {
                $mobiles = implode(",", $mobiles);
            }
        }

        // Send notification to the $notifiable instance...

        $config['sms']['api_type']			= 'post';
        $config['sms']['url']				= 'https://control.msg91.com/api/sendhttp.php';
        $config['sms']['number_variable']	= 'mobiles';
        $config['sms']['text_variable']		= 'message';
        $config['sms']['extra_params']		= [
            'authkey' => '130847AowH7SrJJ2n5825afa4',
            'sender' => 'IntStk',
            'route' => '4',
            'country' => '91',
            'response' => 'json',
            'unicode' => '1'
        ];

        $url = $config['sms']['url'];

        $postData = array();
        $postData[$config['sms']['number_variable']] = $mobiles;
        $postData[$config['sms']['text_variable']] = urlencode($msg);

        foreach ($config['sms']['extra_params'] as $key => $value) {
            $postData[$key] = $value;
        }

        $options = array();
        $options[CURLOPT_RETURNTRANSFER] = true;
        if($config['sms']['api_type'] == 'post') {
            $options[CURLOPT_URL] = $url;
            $options[CURLOPT_POST] = true;
            $options[CURLOPT_POSTFIELDS] = $postData;
        } else {
            $options[CURLOPT_URL] = $url .'?'. http_build_query($postData);
        }

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        if(curl_errno($ch)){
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);
        if(isset($notification->delivery)) {
            $notification->saveRequestId(json_decode($output)->message);
        }
        return $output;
    }

}