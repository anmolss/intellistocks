<?php

namespace App;


class Configuration extends AppModel
{
    public function type()
    {
        return $this->belongsTo(ConfigType::class, 'type_id');
    }

}