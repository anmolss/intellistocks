<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Laratrust\Traits\LaratrustPermissionTrait;

class Permission extends AppModel
{
    use Sluggable, LaratrustPermissionTrait;

    protected $fillable = [
        'name', 'display_name', 'description'
    ];

    public function sluggable()
    {
        return [
            'name' => [
                'source' => 'display_name'
            ]
        ];
    }
}
