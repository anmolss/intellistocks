<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{

	public function departments()
	{
	    return $this->belongsTo(Department::class, 'department_id');
	}
 
}
