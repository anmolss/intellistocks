<?php

namespace App\Http\Requests\Client;

use App\Http\Requests\Request;
use App\Plan;
use Illuminate\Support\Facades\Validator;

class StoreClientUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('client-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'                          => 'required',
            'email'                         => 'required|unique:users|email',
            'address'                       => 'required',
            'zipcode'                       => 'max:6',
            'city'                          => 'required',
            'state'                         => 'required',
            'gender'                        => '',
            'doa'                           => '',
            'dob'                           => '',
            'occupation'                    => '',
            'organisation'                  => '',
            'mobile'                        => 'required|max:10|unique:users',
            'secondary_number'              => 'max:10',
            'company_type'                  => '',
            'username'                      => 'required|unique:users',
            'password'                      => 'required|min:6|confirmed',
            'password_confirmation'         => 'required',
            'fk_relation_manager_id'        => 'required',
            'source'                        => 'required',
            'amount'                        => 'required',
            'discount'                      => '',
            'compliance_manager'            => '',
            'currency'                      => '',
            'denomination'                  => 'required',
            'payment_mode'                  => '',
            'special_note'                  => '',
            'discount'                      => '',
            'services.*.fk_service_id'      => 'required',
            'services.*.fk_plan_id'         => 'required',
            'services.*.payment_type'       => '',
            'services.*.activation_date'    => 'required'
        ];

        // if(Request::get("customer_id")){
        //     $rules = [
        //         'services.*.fk_service_id'      => 'required',
        //         'services.*.fk_plan_id'         => 'required',
        //         'services.*.payment_type'       => 'required',
        //         'services.*.activation_date'    => 'required'
        //     ];
        // }

        // if( null != Request::get("payment_mode") && Request::get("payment_mode") == 1){
        //     $rules['services.*.capital']  = 'required';
        // }

        // if( !empty(Request::get("services")[0]['payment_type']) && Request::get("services")[0]['payment_type'] == 'full'){
        //     $rules['services.*.fk_payment_type']  = 'required';
        // }

        // if( isset(Request::get("services")[0]['force_verify']) && Request::get("services")[0]['force_verify'] == 'yes'){
        //     $rules['services.*.force_verify_reason']  = 'required';
        // }

        // if( !empty(Request::get("services")[0]['payment_type']) && !empty(Request::get("services")[0]['payment_type']) == 'full' ){
        //     $rules['services.*.transaction.*.amount']   = 'required';
        //     $rules['services.*.transaction.*.date']     = 'required';
        // } else if( !empty(Request::get('payment_type')) && !empty(Request::get('payment_type')) == 'partial' ){
        //     $rules['services.*.transaction.*.amount']   = 'required';
        //     $rules['services.*.transaction.*.date']     = 'required';
        // }

        return $rules;
    }

    public function messages()
    {
        return [
            'size'    => 'The :attribute must be exactly :size.',

            'fk_relation_manager_id' => 'Select Relation Manager',

            'services.*.fk_service_id.required'         => 'Select Service',
            'services.*.fk_plan_id.required'            => 'Select Plan',
            'services.*.payment_type.required'          => 'Select Payment',
            'services.*.fk_payment_type.required'       => 'Select Payment Type',
            'services.*.activation_date.required'       => 'Select Activation Date',
            'services.*.capital.required'               => 'Enter Capital Amount',
            'services.*.force_verify_reason.required'   => 'Enter Reason for Force Verify',

            'services.*.transaction.*.date.required' => 'Date is required',
            'services.*.transaction.*.amount.required' => 'Please Enter Amount',
        ];
    }
}