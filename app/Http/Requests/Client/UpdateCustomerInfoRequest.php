<?php

namespace App\Http\Requests\Client;

use App\Http\Requests\Request;

class UpdateCustomerInfoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('client-update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'username' => 'required',
            'zipcode' => 'max:6',
            'city' => '',
            'mobile' => 'required|max:10',
            'secondary_number' => 'max:10',
            'company_type' => '',
        ];
    }
}