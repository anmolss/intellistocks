<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class StoreUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('user-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         $rules = [
            'name' => 'required|max:255',
            'username' => 'required|unique:users|max:255',
            'email' => 'unique:users|email|max:255',
            'official_emailid' => 'required|unique:users|email|max:255',
            'mobile' => 'required|numeric|unique:users,mobile',
            'official_mobile' => 'required|numeric|unique:users,mobile',
            'date_of_joining' => 'required|date',
            'reporting_to' => 'required',
            'department' => 'required',
            'designation' => 'required',
            'zip_code' => 'numeric',
            'address1' => 'max:255',
            'city' => 'max:255',
            'state' => 'max:255',
            'password' => 'required|min:6|confirmed',
            'roles.*' => 'required'
        ];

        return $rules;
    }
}
