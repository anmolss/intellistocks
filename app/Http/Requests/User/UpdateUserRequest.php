<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('user-update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        $rules = [
            'name'             => 'required',
            'official_emailid' => 'required',
            'mobile'           => 'required',
            'official_mobile'  => 'required',
            'date_of_joining'  => 'required',
            'reporting_to'     => 'numeric|between:0,100',
            'department'       => '',
            'designation'      => 'required',
        ];
        
        return $rules;
    }
}