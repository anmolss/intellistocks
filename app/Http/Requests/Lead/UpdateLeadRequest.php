<?php

namespace App\Http\Requests\Lead;

use App\Http\Requests\Request;

class UpdateLeadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('lead-update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        $rules = [
            'title'             => 'required',
            'note'              => 'required',
            'lead_type'         => 'required',
            'status'            => 'required',
            'fk_user_id_assign' => 'required',
            'open_date'         => 'required',
            'close_date'        => 'required',
            'confidence'        => 'numeric|between:0,100',
            'fk_user_id_created'=> '',
            'fk_client_id'      => 'required',
            'contact_date'      => 'required'
        ];
        if(!empty(Request::get('has_discount'))){
            $rules['discount_value'] = 'required|numeric|between:0,100';
        }
        return $rules;
    }
}