<?php
/**
 * Created by PhpStorm.
 * User: hemchander
 * Date: 03/10/17
 * Time: 3:36 PM
 */

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeliveryReportController
{
    function saveDeliveryReport(Request $request){
        $data = $request->get("data");
        $jsonData = json_decode($data,true);
        foreach($jsonData as $key => $value)
        {
            $request_id = $value['requestId'];
            foreach($value['report'] as $key1 => $value1)
            {
                $desc = $value1['desc'];
                if($desc == 'DELIVERED') {
                    $receiver = substr($value1['number'], 2);
                    $user = User::where('mobile', $receiver)->get(['id'])->first();
                    DB::table('service_delivery_user')->where([
                        ['user_id', $user->id],
                        ['request_id', $request_id]
                        ])
                        ->limit(1)
                        ->update(array('delivery_status' => 'success'));
                }
            }
        }
    }
}