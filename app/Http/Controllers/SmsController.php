<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class SmsController extends Controller {

    public static function sendSMS($msg, $mob)
    {
        $config['sms']['api_type']			= 'post';
		$config['sms']['url']				= 'https://control.msg91.com/api/sendhttp.php';
		$config['sms']['number_variable']	= 'mobiles';
		$config['sms']['text_variable']		= 'message';
		$config['sms']['extra_params']		= array('authkey' => '130847AowH7SrJJ2n5825afa4', 'sender' => 'IntStk', 'route' => '4', 'country' => '91', 'response' => 'json', 'unicode' => '1');

        $url = $config['sms']['url'];

        $postData = array();
        $postData[$config['sms']['number_variable']] = $mob;
        $postData[$config['sms']['text_variable']] = urlencode($msg);

        foreach ($config['sms']['extra_params'] as $key => $value) {
            $postData[$key] = $value;
        }

        $options = array();
        $options[CURLOPT_RETURNTRANSFER] = true;
        if($config['sms']['api_type'] == 'post') {
            $options[CURLOPT_URL] = $url;
            $options[CURLOPT_POST] = true;
            $options[CURLOPT_POSTFIELDS] = $postData;
        } else {
            $options[CURLOPT_URL] = $url .'?'. http_build_query($postData);
        }

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        if(curl_errno($ch)){
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        return $output;
    }
}
