<?php 

namespace App\Http\Controllers;
use Mail;
use App\Http\Controllers\Controller;

class EmailController extends Controller {

    public function sendMail($mail_type='', $view_data='', $to='')
    {
        //change allow send to 'yes' to send mail else mail will stop!!! :)
        $allow_send = env('ALLOW_MAIL');
        if(!empty($mail_type) && !empty($view_data) && !empty($to) && $allow_send == 'yes'){
            switch ($mail_type) {
                case 'send_kyc':
                    Mail::send('admin.email.send_kyc', $view_data, 
                    function($message) use ($to) {
                        $message
                            ->to($to['user_email'], $to['user_name'])
                            ->from("no-reply@intellistocks.com","Intelli Stocks")
                            ->subject('Intelli Stocks KYC & RAF Forms!')
                            ->attach('documents/KYC.docx', ['mime'=>'application/docx', 'Content-Disposition'=>'attachment', 'as' => 'KYC_FORM.docx'])
                            ->attach('documents/RAF.docx', ['mime'=>'application/docx', 'Content-Disposition'=>'attachment', 'as' => 'RAF_FORM.docx']);
                    });
                    break;
                case 'verify_service':
                    Mail::send('admin.email.verify_service', $view_data, 
                    function($message) use ($to) {
                        $message
                            ->to($to['user_email'], $to['user_name'])
                            ->from("no-reply@intellistocks.com","Intelli Stocks")
                            ->subject('Congratulations..It all begins at Intelli Stocks!');
                    });
                    break;
                case 'temp_extn_service':
                    Mail::send('admin.email.temp_extn_service', $view_data, 
                    function($message) use ($to) {
                        $message
                            ->to($to['user_email'], $to['user_name'])
                            ->from("no-reply@intellistocks.com","Intelli Stocks")
                            ->subject('Extended Service for '.$to['user_name']);
                    });
                    break;
                case 'terminate_service':
                    Mail::send('admin.email.terminate_service', $view_data, 
                    function($message) use ($to) {
                        $message
                            ->to($to['user_email'], $to['user_name'])
                            ->from("no-reply@intellistocks.com","Intelli Stocks")
                            ->subject('Terminated Service for '.$to['user_name']);
                    });
                    break;
                case 'email_default':
                    Mail::send('admin.email.email_default', $view_data, 
                    function($message) use ($to) {
                        $message
                            ->to($to['user_email'], $to['user_name'])
                            ->from("no-reply@intellistocks.com","Intelli Stocks")
                            ->subject($to['subject']);
                    });
                    break;
                case 'service_renewed':
                    Mail::send('admin.email.service_renewed', $view_data, 
                    function($message) use ($to) {
                        $message
                            ->to($to['user_email'], $to['user_name'])
                            ->from("no-reply@intellistocks.com","Intelli Stocks")
                            ->subject('Renewed Service for '.$to['user_name']);
                    });
                    break;
                default:
                    
                    break;
            }    
        }
    }
}