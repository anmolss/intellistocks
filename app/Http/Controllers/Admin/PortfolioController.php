<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Portfolio;
use Illuminate\Http\Request;
use Flash;
use Validator;

class PortfolioController extends Controller
{
    protected $base_model;
    protected $view_path;
    protected $route_base;
    protected $route_base_prefix = 'admin::';
    protected $append_flash;
    protected $validation_rule;

    public function __construct()
    {
        $this->base_model = new Portfolio();
        $this->view_path = 'admin.services.';
        $this->route_base = 'services';
        $this->validation_rule = [
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'call_wide_amt_limit' => 'required|integer',
            'amount_limit' => 'required|integer',
            'service_type_id' => 'required',
            'trade_type_id' => 'required',
            'min_call' => 'required|integer',
            'max_call' => 'required|integer',
            'max_age_call' => 'required|integer'
        ];
        $this->messages = [];
        $this->append_flash = 'Service';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->base_model->orderBy('id', 'Desc')->paginate(15);

        return view($this->view_path . 'index', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'admin.services.portfolio');
        //return view($this->view_path . 'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation_rule['max_call'] = 'required|integer|min:' . ($request->get('min_call')+1);

        $validator = Validator::make($request->all(), $this->validation_rule,$this->messages);

        if ($validator->fails()) {
            return redirect(route($this->route_base_prefix . $this->route_base.'.create'))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $item = $this->base_model;
            $item->name = $request->get('name');
            $item->description = $request->get('description');
            $item->call_wide_amt_limit = $request->get('call_wide_amt_limit');
            $item->amount_limit = $request->get('amount_limit');
            $item->service_type_id = $request->get('service_type_id');
            $item->trade_type_id = $request->get('trade_type_id');
            $item->min_call = $request->get('min_call');
            $item->max_call = $request->get('max_call');
            $item->max_age_call = $request->get('max_age_call');
            $item->stock_universe = $request->get('stock_universe');
            $item->is_portfolio = $request->has('is_portfolio') ? 1 : 0;
            $item->status = $request->has('status') ? 1 : 0;


            if(! $item->save())
            {
                Flash::success('Error occurred! Please try again after some time ');
                return redirect(route($this->route_base_prefix . $this->route_base . '.create'))
                    ->withInput($request->all());
            }


            Flash::success($this->append_flash . ' added successfully!');
            return redirect(route($this->route_base_prefix . $this->route_base.'.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = $this->base_model->findOrFail($id);
        return view($this->view_path . 'edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validation_rule['max_call'] = 'required|integer|min:' . ($request->get('min_call')+1);

        $validator = Validator::make($request->all(), $this->validation_rule);

        if ($validator->fails()) {
            return redirect(route($this->route_base_prefix . $this->route_base . '.edit', [$this->route_base => $id]))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $item = $this->base_model->findOrFail($id);
            $item->name = $request->get('name');
            $item->description = $request->get('description');
            $item->call_wide_amt_limit = $request->get('call_wide_amt_limit');
            $item->amount_limit = $request->get('amount_limit');
            $item->service_type_id = $request->get('service_type_id');
            $item->trade_type_id = $request->get('trade_type_id');
            $item->min_call = $request->get('min_call');
            $item->max_call = $request->get('max_call');
            $item->max_age_call = $request->get('max_age_call');
            $item->stock_universe = json_encode($request->get('stock_universe'));
            $item->is_portfolio = $request->has('is_portfolio') ? 1 : 0;
            $item->status = $request->has('status') ? 1 : 0;


            if(! $item->save())
            {
                //dd($item);
                Flash::success('Error occurred! Please try again after some time ');
                return redirect(route($this->route_base_prefix . $this->route_base . '.edit', [$this->route_base => $id]))
                    ->withInput($request->all());
            }


            Flash::success($this->append_flash . ' updated successfully!');
            return redirect(route($this->route_base_prefix . $this->route_base.'.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->base_model->find($id);

        if (!$item->delete()) {
            Flash::success('Error occurred! Please try again after some time ');
        } else {
            Flash::success($this->append_flash . ' deleted successfully!');
        }

        return redirect(route($this->route_base_prefix . $this->route_base . ".index"));
    }


}
