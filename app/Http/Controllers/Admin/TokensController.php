<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Token;
use Illuminate\Http\Request;
use Auth;
use Flash;
use Validator;

class TokensController extends Controller
{
    protected $base_model;
    protected $view_path;
    protected $route_base;
    protected $route_base_prefix = 'admin::';
    protected $append_flash;
    protected $validation_rule;

    public function __construct()
    {

        $this->base_model = new Token();
        $this->view_path = 'admin.tokens.';
        $this->route_base = 'tokens';
        $this->validation_rule = [
            'name' => 'required',
            'content' => 'required',
        ];
        $this->append_flash = 'Tokens';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->base_model->orderBy('id', 'Desc')->paginate(15);
        return view($this->view_path . 'index', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->view_path . 'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation_rule);
        if ($validator->fails()) {
            return redirect(route($this->route_base_prefix . $this->route_base.'.create'))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $item = $this->base_model;
            $item->name = $request->get('name');
            $value = strtoupper($item->name);
            $item->value = '['.$value.']';
            $item->content = $request->get('content');
            $item->is_replaceable = $request->get('is_replaceable');
            
            if(! $item->save())
            {
                Flash::success('Error occurred! Please try again after some time ');
                return redirect(route($this->route_base_prefix . $this->route_base . '.create'))
                    ->withInput($request->all());
            }

            Flash::success($this->append_flash . ' added successfully!');
            return redirect(route($this->route_base_prefix . $this->route_base.'.index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //
    public function edit($id)
    {
        
        $item = $this->base_model->findOrFail($id);
        return view($this->view_path . 'edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->validation_rule);

        if ($validator->fails()) {
             return redirect(route($this->route_base_prefix . $this->route_base . '.edit', [$this->route_base => $id]))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $item = $this->base_model->findOrFail($id);

            $item->name = $request->get('name');
            $value = strtoupper($item->name);
            $item->value = '['.$value.']';
            $item->content = $request->get('content');
            $item->is_replaceable = $request->get('is_replaceable');

            if(! $item->save())
            {
                Flash::success('Error occurred! Please try again after some time ');
                return redirect(route($this->route_base_prefix . $this->route_base . '.create'))
                    ->withInput($request->all());
            }

            Flash::success($this->append_flash . ' updated successfully!');
            return redirect(route($this->route_base_prefix . $this->route_base.'.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->base_model->find($id);

        if(! $item->delete())
        {
            Flash::success('Error occurred! Please try again after some time ');
        }
        else
        {
            Flash::success( $this->append_flash . ' deleted successfully!');
        }

        return redirect(route($this->route_base_prefix . $this->route_base.".index"));
    }
}
