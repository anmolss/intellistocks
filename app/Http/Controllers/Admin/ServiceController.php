<?php

namespace App\Http\Controllers\Admin;

use App\Models\Filters\ServiceFilters;
use App\Repositories\ServiceDelivery\ServiceDeliveryRepository;
use App\SdTemplate;
use App\Token;
use App\Watchlist;
use DB;
use Datatables;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Service;
use App\Plan;
use App\User;
use App\Models\Attachment;
use App\Models\UserService;
use App\Models\UserServiceAction;
use Illuminate\Http\Request;
use Flash;
use Validator;
use Illuminate\Support\Str;
use Carbon;
use File;
use Auth;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\SmsController;

class ServiceController extends Controller
{
    protected $base_model;
    protected $view_path;
    protected $route_base;
    protected $route_base_prefix = 'admin::';
    protected $append_flash;
    protected $validation_rule;

    public function __construct()
    {
        $this->base_model = new Service();
        $this->view_path = 'admin.services.';
        $this->route_base = 'services';
        $this->validation_rule = [
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'code' => 'max:10',
            'service_type_id' => 'required',
            'trade_type_id' => 'required',
            'call_type' => 'required',
            'status' => 'required',
            'watchlist' => 'required',
            'allowed_duration' => 'required_if:payment_mode,0',
            'payment_mode' => 'required|in:0,1',
            'template.*.body' => 'required|min:10',
        ];

        $this->append_flash = 'Service';
    }

    public function index(ServiceFilters $filters)
    {
        $list = $this->base_model->selectRaw('services.*, count(user_id) as active_users, count(plans.id) AS total_plans')
            ->leftJoin('user_services', function($q){
                $q->on('user_services.service_id', '=', 'services.id')
                    ->where('user_services.service_status',  'like', 'Active');
            })
            ->leftJoin('plans', 'plans.service_id', '=', 'services.id')
            ->filter($filters)
            ->groupBy('services.id')
            ->orderBy('id', 'desc')->paginate(15);

        $reports = [];
        if($list->count() > 0){
            $reports = (new ServiceDeliveryRepository())->deliveryReportByService($list->pluck( 'id'),[], $list->pluck('name', 'id'), false);
        }

        return view($this->view_path . 'index', compact('list', 'reports'));
    }

    public function indexActiveServices()
    {
        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');
        $plans = Plan::orderBy('name', 'asc')->get()->pluck('name', 'id');
        
        $service_counts = DB::select( 
            DB::raw('SELECT COUNT( 
                CASE WHEN service_status =  "Active" THEN id END ) AS Active, COUNT( 
                CASE WHEN service_status =  "Pending" THEN id END ) AS Pending, COUNT( 
                CASE WHEN service_status =  "Expired" THEN id END ) AS Expired, COUNT( 
                CASE WHEN service_status =  "Suspended" THEN id END ) AS Suspended, COUNT( 
                CASE WHEN service_status =  "TempExtended" THEN id END ) AS TempExtended, COUNT( 
                CASE WHEN service_status =  "Deleted" THEN id END ) AS Deleted, COUNT( 
                CASE WHEN service_status =  "Renewed" THEN id END ) AS Renewed, COUNT( * ) AS Total
                FROM user_services'
            ) 
        )[0];
        return view($this->view_path .'active_service_index')->withServices($services)->withPlans($plans)->withCounts($service_counts);
    }

    public function activeServiceData(Request $request)
    {
        $user_services = UserService::with('transections')->with('user')->with('service')->with('plan')->with('transcript_file')->with('kyc_file')->where('service_status', '<>', 'Deleted')->get();

        return Datatables::of($user_services)
        ->add_column('actions', function ($user_services) {
            $action = '
            <ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i class="icon-gear"></i>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">';
                    /*
                    if($user_services->consent_status == 'No'){
                        $action .= 
                        '<li><a href="'.route('admin::user_services.resendVerification', $user_services->id).'" class="resend_activation"><i class="icon-alarm-check"></i> Resend Activation</a></li>';
                    }

                    $amount_payable = 0;
                    $amount_paid = 0;
                    $balance = 0;

                    if($user_services->plan->service->payment_mode == 0){
                      $amount_payable = $user_services->plan->service_fee;
                    } else {
                      $amount_payable = $user_services->plan->annual_fee;
                      $capital_fee = round($user_services->service_capital / 100, 0, PHP_ROUND_HALF_UP);
                      if($capital_fee > $amount_payable){
                          $amount_payable = $capital_fee;
                      }
                    }

                    $discount = isset($user_services->transection->discount) ? $user_services->transection->discount : 0;
                    $tax = isset($user_services->transection->applied_tax) ? $user_services->transection->applied_tax : 0;

                    if($discount != 0 && $amount_payable != 0){
                      $amount_payable = $amount_payable - ($discount / 100) * $amount_payable;
                    }

                    if ($tax != 0 && $amount_payable != 0) {
                      $amount_payable = $amount_payable + ($tax / 100) * $amount_payable;
                    }

                    if(isset($user_services->transection)) {
                      if($user_services->transection->pay_type == 'full'){
                        if($user_services->transection->cleared == 'Yes'){
                          $amount_paid = $user_services->transection->amount;
                        }
                      } else {
                        $all = $user_services->transection->allPartialTransactions->toArray();
                        if(count($all) > 0){
                          $amount_paid = array_sum(array_map(function($item) { if($item['cleared'] == 'Yes') { return $item['amount']; } else { return 0; } }, $all));
                        }
                      }
                    }

                    $balance = round($amount_payable, 0, PHP_ROUND_HALF_UP) - round($amount_paid, 0, PHP_ROUND_HALF_UP);

                    $activation_date = new Carbon($user_services->activation_date);
                        if(!empty($user_services->plan->duration)){
                            $expiry_date = $activation_date->subDay()->addDays($user_services->plan->duration)->format('d-m-Y');
                        } else {
                            $expiry_date = $activation_date->subDay()->addDays($user_services->plan->duration)->format('d-m-Y');
                        }
                    $balance_warning = ($balance > 0)?'yes':'no';

                    $action .= '
                        <li>
                            <a 
                            href="'.route('admin::user_services.showUserService', $user_services->id).'"
                            data-s_name ="'.$user_services->service->name.'"  
                            data-s_plan ="'.$user_services->plan->name.'"  
                            data-id="'.$user_services->id.'" 
                            data-transcript="'.$user_services->transcript_status.'" 
                            data-kyc="'.$user_services->kyc_status.'" 
                            data-consent="'.$user_services->consent_status.'" 
                            data-activation-date="'.(new Carbon($user_services->activation_date))->format('d-m-Y').'"
                            data-expiry-date="'.$expiry_date.'" 
                            data-balance-warning="'.$balance_warning.'"
                            data-consent-ip="'.$user_services->consent_ip.'"
                            class="service_verify"
                            >
                                <i class="icon-file-check2"></i> Verify Service
                            </a>
                        </li>';
                    */
                    $action .= '
                        <li><a href="'.route('admin::user_services.showUserService', $user_services->id).'"><i class="icon-eye2"></i> View</a></li>
                        <li><a href="'.route('admin::active_service.edit', $user_services->id).'"><i class="icon-pencil7"></i> Edit</a></li>
                        <li class="divider"></li>';

                    switch ($user_services->service_status) {
                        case 'Pending':
                            $action .=
                                '';
                            break;
                        
                        case 'Active':
                            $action .= 
                            '<li><a href="'.route('admin::active_service.renew', $user_services->id).'"><i class="icon-reload-alt"></i> Renew</a></li>

                            <li><a href="'.route('admin::services.tempExtnService').'" class="modal-action" data-toggle="modal" data-id="'.$user_services->id.'" data-action="temp_extn" data-target="#action_modal"><i class="icon-move-horizontal"></i> Temp Extn</a></li>

                            <li class="divider"></li>

                            <li><a href="'.route('admin::services.terminateService').'" class="modal-action" data-toggle="modal" data-id="'.$user_services->id.'" data-action="terminate" data-target="#action_modal"><i class="icon-x"></i> Terminate</a></li>';
                            break;

                        case 'Expired':
                            $action .= 
                                '<li><a href="'.route('admin::active_service.renew', $user_services->id).'"><i class="icon-reload-alt"></i> Renew</a></li>

                                <li><a href="'.route('admin::services.tempExtnService').'" class="modal-action" data-toggle="modal" data-id="'.$user_services->id.'" data-action="temp_extn" data-target="#action_modal"><i class="icon-move-horizontal"></i> Temp Extn</a></li>

                                <li class="divider"></li>';
                            break;

                        case 'Suspended':
                            $action .= 
                                '<li><a href="'.route('admin::active_service.renew', $user_services->id).'"><i class="icon-reload-alt"></i> Renew</a></li>

                                <li><a href="'.route('admin::services.tempExtnService').'" class="modal-action" data-toggle="modal" data-id="'.$user_services->id.'" data-action="temp_extn" data-target="#action_modal"><i class="icon-move-horizontal"></i> Temp Extn</a></li>

                                <li class="divider"></li>';
                            break;

                        default:
                            $action .= '';
                            break;
                    }

                    $action .= 
                        '<li><a href="'.route('admin::services.destroyActiveServices', $user_services->id).'" class="text-danger-700 btn-delete"><i class="icon-trash"></i> Delete</a></li>
                    </ul>
                </li>
            </ul>';
            return $action;
        })
        ->filter(function ($instance) use ($request) {
            if ($request->has('search')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $return = false;
                    if (Str::contains(strtolower($row->user_name), strtolower($request->get('search')))) {
                        $return = true;
                    } else if (Str::contains(strtolower($row->email), strtolower($request->get('search')))) {
                        $return = true;
                    } else if (Str::contains(strtolower($row->service_name), strtolower($request->get('search')))) {
                        $return = true;
                    } else if (Str::contains(strtolower($row->plan_name), strtolower($request->get('search')))) {
                        $return = true;
                    } else if (Str::contains(strtolower($row->mobile), strtolower($request->get('search')))) {
                        $return = true;
                    }
                    return $return;
                });
            }

            if ($request->has('service')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $return = false;
                    foreach ($request->get('service') as $service) {
                        if($row->service_id == $service){
                            $return = true;
                        }
                    }
                    return $return;
                });
            }

            if ($request->has('plan')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $return = false;
                    foreach ($request->get('plan') as $plan) {
                        if($row->plan_id == $plan){
                            $return = true;
                        }
                    }
                    return $return;
                });
            }

            if ($request->has('created_range')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $created_at = date('Y-m-d', strtotime($row->created_at));
                    $created_start = date('Y-m-d', strtotime(explode(" - ", $request->get('created_range'), 2)[0]));
                    $created_end = date('Y-m-d', strtotime(explode(" - ", $request->get('created_range'), 2)[1]));
                    if ($created_start == Carbon::now()->format('Y-m-d') && $created_end == Carbon::now()->format('Y-m-d')) {
                        return true;
                    } else {
                        return (($created_at > $created_start) && ($created_at < $created_end)) ? true : false;
                    }
                });
            }

            if ($request->has('start_range')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $start_at = date('Y-m-d', strtotime($row->activation_date));
                    $start_start = date('Y-m-d', strtotime(explode(" - ", $request->get('start_range'), 2)[0]));
                    $start_end = date('Y-m-d', strtotime(explode(" - ", $request->get('start_range'), 2)[1]));
                    if ($start_start == Carbon::now()->format('Y-m-d') && $start_end == Carbon::now()->format('Y-m-d')) {
                        return true;
                    } else {
                        return (($start_at > $start_start) && ($start_at < $start_end)) ? true : false;
                    }
                });
            }

            if ($request->has('expire_range')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $expire_at = date('Y-m-d', strtotime($row->expire_date));
                    $expire_start = date('Y-m-d', strtotime(explode(" - ", $request->get('expire_range'), 2)[0]));
                    $expire_end = date('Y-m-d', strtotime(explode(" - ", $request->get('expire_range'), 2)[1]));
                    if ($expire_start == Carbon::now()->format('Y-m-d') && $expire_end == Carbon::now()->format('Y-m-d')) {
                        return true;
                    } else {
                        return (($expire_at > $expire_start) && ($expire_at < $expire_end)) ? true : false;
                    }
                });
            }

            if ($request->has('service_status') && !empty($request->get('service_status'))) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row->service_status == $request->get('service_status')) ? true : false;
                });
            }

            if ($request->has('kyc_status') && !empty($request->get('kyc_status'))) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row->kyc_status == $request->get('kyc_status')) ? true : false;
                });
            }

            if ($request->has('transcript_status') && !empty($request->get('transcript_status'))) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row->transcript_status == $request->get('transcript_status')) ? true : false;
                });
            }

            if ($request->has('consent_status') && !empty($request->get('consent_status'))) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row->consent_status == $request->get('consent_status')) ? true : false;
                });
            }
        })
        ->make(true);
    }

    public function create()
    {
        $active_users = false;
        $default_templates = SdTemplate::where('service_id', 0)->get()->groupBy('exchange');
        $tokens = Token::get()->pluck('value', 'id');

        return view($this->view_path . 'create', compact('active_users', 'default_templates', 'tokens'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation_rule);
        if ($validator->fails()) {

            return redirect(route($this->route_base_prefix . $this->route_base.'.create'))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $item = $this->base_model;
            $item->name = $request->get('name');
            $item->description = $request->get('description');
            $item->code = $request->get('code');
            $item->service_type_id = $request->get('service_type_id');
            $item->trade_type_id = $request->get('trade_type_id');
            $item->call_type = $request->get('call_type');
            $item->status = $request->get('status');
            $item->allowed_duration = $request->get('allowed_duration');
            $item->payment_mode = $request->get('payment_mode');


            if(!$item->save())
            {
                Flash::error('Error occurred! Please try again after some time ');
                return redirect(route($this->route_base_prefix . $this->route_base . '.create'))->withInput($request->all());
            }

            $item->watchlist = $request->get('watchlist');
            $item->save();

            $templates = [];
            foreach ($request->get('template') as $template)
            {
                $templates[] = new SdTemplate($template);
            }

            $item->template()->saveMany($templates);

            Flash::success($this->append_flash . ' added successfully!');
            return redirect(route($this->route_base_prefix . $this->route_base.'.index'));
        }
    }

    public function show($id)
    {
        $item = $this->base_model->with(['plans'])->findOrFail($id);
        $users = UserService::with(['user'])->where('service_id', $id)->get();
        $report = [];
        if($item->count() > 0){
            $report = (new ServiceDeliveryRepository())->deliveryReportByService([$item->id],[], [$item->id => $item->name], false, 'all', true);

            if(!empty($report))
                $report = $report[$item->id];
        }

        return view($this->view_path . 'show', compact('item', 'users', 'report'));
    }

    public function edit($id)
    {
        $item = $this->base_model->findOrFail($id);
        $active_users = (UserService::where('service_id', $id)->where('service_status','Active')->count() > 0) ? true : false;

        $default_templates = SdTemplate::where('service_id', $id)->get();
        if($default_templates->isEmpty()) {
            $default_templates = SdTemplate::where('service_id', 0)->get();
        }

        $default_templates = $default_templates->groupBy('exchange');
        $tokens = Token::get()->pluck('value', 'id');

        return view($this->view_path . 'edit', compact('item', 'active_users', 'default_templates', 'tokens'));
    }

    public function editActiveService($id)
    {
        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');
        $plans = Plan::orderBy('name', 'asc')->get()->pluck('name', 'id');
        $service_plans = Service::with('plans')->get()->toArray();
        foreach ($service_plans as $key => $service) {
            foreach ($service['plans'] as $key => $plan) {
                $final[$service['id']][] = array('id' => $plan['id'], 'name' => $plan['name']);
            }
        }
        $service = UserService::with('transections')->with('user')->with('service')->with('plan')->with('transcript_file')->with('kyc_file')->with('user_service_action')->findOrFail($id)->toArray();
        $is_transaction_cleared = 'no';
        foreach ($service['transections'] as $no => $transaction) {
            if($service['latest_transaction_history_id'] == $transaction['transaction_history_id']){
                $service['latest_transactions'][] = $transaction;
            }
            if($transaction['cleared'] == 'Yes'){
                $is_transaction_cleared = 'yes';
            }
        }

        $customer = User::findOrFail($service['user_id']);
        return view($this->view_path . 'edit_active_service', compact('service', 'is_transaction_cleared'))
            ->withCustomer($customer)
            ->withServices($services)
            ->withPlans($plans)
            ->withServicePlans($final);
    }

    public function renewUserService($id)
    {
        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');
        $plans = Plan::orderBy('name', 'asc')->get()->pluck('name', 'id');
        $service_plans = Service::with('plans')->get()->all();
        $service_plans = json_decode(json_encode($service_plans), true);
        foreach ($service_plans as $key => $service) {
            foreach ($service['plans'] as $key => $plan) {
                $final[$service['id']][] = array('id' => $plan['id'], 'name' => $plan['name']);
            }
        }
        $service = UserService::with('transections')->with('user')->with('service')->with('plan')->with('transcript_file')->with('kyc_file')->with('user_service_action')->findOrFail($id)->toArray();
        $customer = User::findOrFail($service['user_id']);
        return view($this->view_path . 'renew_active_service', compact('service'))
            ->withCustomer($customer)
            ->withServices($services)
            ->withPlans($plans)
            ->withServicePlans($final);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->validation_rule);

        if ($validator->fails()) {
            return redirect(route($this->route_base_prefix . $this->route_base . '.edit', [$this->route_base => $id]))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $item = $this->base_model->findOrFail($id);
            $item->name = $request->get('name');
            $item->description = $request->get('description');
            $item->code = $request->get('code');
            $item->service_type_id = $request->get('service_type_id');
            $item->trade_type_id = $request->get('trade_type_id');
            $item->call_type = $request->get('call_type');
            $item->status = $request->get('status');
            $item->watchlist = $request->get('watchlist');
            $item->allowed_duration = $request->get('allowed_duration');
            $item->payment_mode = $request->get('payment_mode');

            if(!$item->save())
            {
                Flash::success('Error occurred! Please try again after some time ');
                return redirect(route($this->route_base_prefix . $this->route_base . '.edit', [$this->route_base => $id]))
                    ->withInput($request->all());
            }

            $templates = [];
            foreach ($request->get('template') as $template)
            {
                if($template['status'] != 0){
                    $temp =  SdTemplate::find($template['id']);
                    $temp->body = $template['body'];
                } else {
                    $temp = new SdTemplate($template);
                }

                $templates[] = $temp;
            }
            $item->template()->saveMany($templates);

            Flash::success($this->append_flash . ' updated successfully!');
            return redirect(route($this->route_base_prefix . $this->route_base.'.index'));
        }
    }

    public function showUserService($id)
    {
        $user_service = UserService::with('transections')->with('user')->with('service')->with('plan')->with('transcript_file')->with('kyc_file')->with('user_service_action')->findOrFail($id)->toArray();
        if(count($user_service['user_service_action']) > 0){
            foreach ($user_service['user_service_action'] as $key => $value) {
                $user = DB::table('users')->where('id', $value['action_user_id'])->get()->first();
                $user_service['user_service_action'][$key]['user'] = $user;
            }
        }
        
        $taxes = DB::table('configurations')->where('type_id', 1)->get()->pluck('name', 'id')->toArray();
        return view( $this->view_path  .'show_user_service')
            ->withUserService($user_service)
            ->withTaxes($taxes);
    }

    public function destroy($id)
    {
        $item = $this->base_model->find($id);

        if (!$item->delete()) {
            Flash::success('Error occurred! Please trUy again after some time ');
        } else {
            Flash::success($this->append_flash . ' deleted successfully!');
        }

        return redirect(route($this->route_base_prefix . $this->route_base . ".index"));
    }

    public function destroyActiveService($id)
    {
        $item =DB::table('user_services')
                ->where('id', $id)
                ->update(['service_status' => 'Deleted']);

        if (!$item) {
            Flash::success('Error occurred! Please try again after some time ');
        } else {
            Flash::success($this->append_flash . ' deleted successfully!');
        }

        return redirect(route($this->route_base_prefix . $this->route_base . ".active_services"));
    }

    public function terminateService(Request $request)
    {
        if ($request->has('action') && $request->get('action') == 'terminate') {
            $content = View($this->view_path .'action_modals.terminate_modal')->withHref(route('admin::services.terminateService'))->withId($request->get('id'))->render();
            return json_encode(array('success' => '1', 'content' => $content));
        }
        if ($request->has('form_type') && $request->get('form_type') == 'real') {
            $user_service = UserService::with('user')->findOrFail($request->get('id'));
            $sms_notification = $request->get('sms_notification');
            $email_notification = $request->get('email_notification');
            $input = [
                'termination_reason'   => $request->get('reason'),
                'comments' => $request->get('comments'),
                'service_status' => 'Suspended',
            ];
            $result = $user_service->fill($input)->save();

            if (isset($sms_notification) && $sms_notification == 'yes') {
                //send sms notification to customer
                $response = SmsController::sendSMS('Your Service "'.json_decode($user_service->service_details, true)['name'].'" has been Terminated due to "'.$request->get('reason').'"', $user_service->user->official_mobile);
            }

            if (isset($email_notification) && $email_notification == 'yes') {
                unset($input);
                //save action
                $input = array(
                    'user_service_id' => $request->get('id'),
                    'action_user_id' => Auth::user()->id,
                    'activity' => 'service_termination',
                    'termination_reason' => $request->get('reason'),
                    'comments' => $request->get('comments'),
                );
                $user_service_action = UserServiceAction::create($input);

                $to['user_email'] = $user_service->user->email;
                $to['user_name'] = $user_service->user->name;
                (new EmailController)->sendMail('terminate_service', ['reason' => $request->get('reason'), 'comments' => $request->get('comments'), 'user_by' => Auth::user(), 'user' => $user_service->user, 'user_service' => $user_service], $to);
            }
            if($result){
                return json_encode(['success_form' => 1, 'msg' => 'Service is Terminated']);    
            } else {
                return json_encode(['success_form' => 2, 'msg' => 'Please Try Again Later']);
            }
        }
    }

    public function tempExtnService(Request $request)
    {
        if ($request->has('action') && $request->get('action') == 'temp_extn') {
            $user_service = UserService::with('user')->findOrFail($request->get('id'));

            $content = View($this->view_path .'action_modals.temp_extn_modal')->withHref(route('admin::services.tempExtnService'))->withId($request->get('id'))->withUserService($user_service)->render();
            return json_encode(array('success' => '1', 'content' => $content));
        }
        if ($request->has('form_type') && $request->get('form_type') == 'real') {
            $user_service = UserService::with('user')->findOrFail($request->get('id'));
            $old_expiry_date = (!empty($user_service->expire_date))?$user_service->expire_date:Carbon::now();

            if($user_service->temp_extn_count <= 2){
                $extn_days = (!empty($request->get('days_text')))?$request->get('days_text'):$request->get('days_select');
            
                $expiry_date = new Carbon($user_service->expire_date);
                if(empty($expiry_date) || $user_service->service_status == 'Expired'){
                    $expiry_date = Carbon::now();
                }
                
                if(!empty($extn_days)){
                    $expiry_date = $expiry_date->subDay()->addDays($extn_days)->toDateString();
                }

                $temp_extn_count = $user_service->temp_extn_count+1;

                $input = [
                    'expire_date'   => $expiry_date,
                    'temp_extn_count' => $temp_extn_count,
                    'service_status' => 'TempExtended',
                ];
                $result = $user_service->fill($input)->save();
                if($result){
                    
                    unset($input);
                    //save action
                    $input = array(
                        'user_service_id' => $request->get('id'),
                        'action_user_id' => Auth::user()->id,
                        'activity' => 'temporary_extend',
                        'extn_days' => $extn_days,
                        'old_expiry_date' => $old_expiry_date,
                        'new_expiry_date' => $expiry_date,
                    );
                    $user_service_action = UserServiceAction::create($input);
                    
                    $to['user_email'] = $user_service->user->email;
                    $to['user_name'] = $user_service->user->name;
                    
                    (new EmailController)->sendMail('temp_extn_service', ['days' => $extn_days, 'user_by' => Auth::user(), 'user' => $user_service->user, 'user_service' => $user_service], $to);
                    return json_encode(['success_form' => 1, 'msg' => 'Service is Extended For '.$extn_days.' Days']); 
                } else {
                    return json_encode(['success_form' => 2, 'msg' => 'Please Try Again Later']);
                }
            } else {
                return json_encode(['success_form' => 2, 'msg' => 'Max Extension Limit Reached']);
            }
        }
    }

    public function portfolio($id)
    {
        $item = $this->base_model->find($id);

        if (!$item->delete()) {
            Flash::success('Error occurred! Please try again after some time ');
        } else {
            Flash::success($this->append_flash . ' deleted successfully!');
        }

        return redirect(route($this->route_base_prefix . $this->route_base . ".index"));
    }

    public function getServices(Request $request)
    {
        return Service::where('service_id', $request['service_id'])->get();
    }

    public function verifyService(Request $request)
    {
        if(isset($request->all()['user_id']) && !empty($request->all()['user_id'])){
            if (isset($request->all()['transcript_status']) && $request->all()['transcript_status'] == 'yes') {
                if(!$request->hasFile('transcript_file')){
                    return json_encode(['success' => 2, 'msg' => 'transcript file is required']);
                }
                $ext = $request->file('transcript_file')->getClientOriginalExtension();
                $name = $request->all()['user_id'].'_transcript.'.$ext;
                $size = $request->file('transcript_file')->getSize();
                $mime = $request->file('transcript_file')->getMimeType();
                $request->file('transcript_file')->move('documents/customer_service_files', $name);

                $input = array(
                    'path' => 'documents/customer_service_files',
                    'file_name' => $request->all()['user_id'].'_transcript.'.$ext,
                    'file_details' => serialize(['name' => $name, 'size' => $size, 'ext' => $ext, 'mime' => $mime]),
                );
                $transcript_file = Attachment::create($input);
            }
            if (isset($request->all()['kyc_status']) && $request->all()['kyc_status'] == 'yes') {
                if(!$request->hasFile('kyc_file')){
                    return json_encode(['success' => 2, 'msg' => 'KYC file is required']);
                }
                $ext = $request->file('kyc_file')->getClientOriginalExtension();
                $name = $request->all()['user_id'].'_kyc.'.$ext;
                $size = $request->file('kyc_file')->getSize();
                $mime = $request->file('kyc_file')->getMimeType();
                $request->file('kyc_file')->move('documents/customer_service_files', $name);
                unset($input);
                $input = array(
                    'path' => 'documents/customer_service_files',
                    'file_name' => $request->all()['user_id'].'_kyc.'.$ext,
                    'file_details' => serialize(['name' => $name, 'size' => $size, 'ext' => $ext, 'mime' => $mime]),
                );
                $kyc_file = Attachment::create($input);
            }

            $trans_s = (isset($request->all()['transcript_status']) && $request->all()['transcript_status'] == 'yes') ? 'Yes' : 'No';
            $kyc_s = (isset($request->all()['kyc_status']) && $request->all()['kyc_status'] == 'yes') ? 'Yes' : 'No';
            $consent_s = (isset($request->all()['consent_status']) && $request->all()['consent_status'] == 'yes') ? 'Yes' : 'No';
            
            $transcript_file_id = (isset($transcript_file->id))?$transcript_file->id:'';
            $kyc_file_id = (isset($kyc_file->id))?$kyc_file->id:'';

            $input = [
                'transcript_status' => $trans_s, 
                'kyc_status' => $kyc_s, 
                'consent_status' => $consent_s,
            ];
            if(!empty($input)){
                if (!empty($transcript_file_id)) {
                    $input['transcript_file'] = $transcript_file_id;
                }
                if (!empty($kyc_file_id)) {
                    $input['kyc_file'] = $kyc_file_id;
                }
                $result = DB::table('users')
                        ->where('id', $request->all()['user_id'])
                        ->update($input);
            }

            if($input['transcript_status'] == 'Yes' && $input['kyc_status'] == 'Yes' && $input['consent_status'] == 'Yes'){
                $services = DB::table('user_services')
                    ->where('user_id', $request->get('user_id'))
                    ->where('service_status', 'Pending')
                    ->whereNotNull('on_boarding')
                    ->whereNotNull('payment_received')
                    ->get()->toArray();
                if(!empty($services)){
                    foreach ($services as $service) {
                        if(strtotime($service->activation_date) <= strtotime(Carbon::now()->format('Y-m-d'))){
                            DB::table('user_services')->where('id', $service->id)->update(['service_status' => 'Active']);
                        }
                    }
                }
            }
            
            if($result){
                echo json_encode(['success' => 1]);
            } else {
                echo json_encode(['success' => 2]);
            }
        }
    }

    public function resendVerification($id)
    {
        $user = User::findOrFail($id);
        if($user->consent_status == 'No'){
            if(!isset($user->verification_code) || empty($user->verification_code)){
                $input = array(
                    'verification_code' => Str::random(25),
                );
                $user->fill($input)->save();
            }
            $user_service = UserService::where('user_id', $user->id)->orderBy('id', 'desc')->get()->first();

            $to['user_email'] = $user->email;
            $to['user_name'] = $user->name;
            (new EmailController)->sendMail('verify_service', ['user' => $user,'user_service' => $user_service], $to);
            
            return json_encode(['success' => 1]);
        } else {
            return json_encode(['success' => 2]);
        }
    }


    public function getWatchlistService(Request $request){
        return (new ServiceDeliveryRepository())->serviceWatchlist();
    }

    public function removeWatchlistService(Request $request)
    {
        $item = $this->base_model->find($request->get('id'));
        $item->watchlist = 0;
        if(! $item->save())
        {
            return ['deleted' => false];
        }
        else
        {
            return ['deleted' => true];
        }
    }

}
