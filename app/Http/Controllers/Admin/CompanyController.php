<?php

namespace App\Http\Controllers\Admin;

use App\CompanyMaster;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Nse;
use App\Bse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use ZipArchive;

use DB;

class CompanyController extends Controller
{
    protected $base_model;
    protected $view_path;
    protected $route_base;
    protected $route_base_prefix = 'admin::';
    protected $append_flash;
    protected $validation_rule;

    public function __construct()
    {
        $this->base_model = new CompanyMaster();
        $this->view_path = 'admin.company.';
        $this->route_base_prefix = 'admin::';
        $this->route_base = 'companies';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = CompanyMaster::all();
        return view($this->view_path . 'index', compact('list'));
    }

    public function addExcel()
    {
        return view($this->view_path . 'add_excel', compact('list'));
    }

    public function uploadFundasheet(Request $request){
        if($request->hasFile('excel_file')){
            $file = $request->file('excel_file');
            $fileName = $file->getClientOriginalName();
            if($file->getMimeType() != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
                return "Please Select Correct File";
            }
            $result = Storage::disk('public')->put($fileName, file_get_contents($file->getRealPath()));
            $file_path = storage_path('app/public/'.$fileName);
            if(file_exists($file_path)){
                config(['excel.import.startRow' => 4 ]);
                $rows = Excel::load($file_path)->get();
                foreach ($rows as $row) {
                    $company_name = $row->names;
                    $status = $row->status;
                    if (!Company::where('name', '=', $company_name)->exists()) {
                        echo $company_name." <span style='color: red;'>not found </span><br>";
                    }
                    $result = DB::table('companies')->where('name', $company_name)->update(['status' => $status]);
                }
                \File::delete($file_path);
                config(['excel.import.startRow' => 1 ]);
                return "<h1 style='color: green;'>Successfully Uploaded!!!</h1>";
            }else{
                return "Error while uploading file";
            }
        }else{
            return "No File Selected";
        }
    }

    public function show($id)
    {
        $company = CompanyMaster::fullDetail()->findOrFail($id)->toArray();
        $bse = Bse::where('ticker_name', $company['security_master']['ticker'])->orderBy('last_trade_time', 'desc')->get();

        foreach ($company['listing_master'] as $key => $perms) {
            $nse_values[$key] = Nse::where('symbol', $perms['scrip_code2_given_by_exchange'])->orderBy('last_traded_time', 'desc')->get();
        }
        $nse = collect($nse_values);
        return view($this->view_path . 'show', compact('company', 'bse', 'nse'));
    }

//    public function importBhav()
//    {
//        $date_of_csv = '2016-08-25';
//       // dd(date(config('rothmans.date_format'),strtotime($date_of_csv)));
//
//        $cmbhav_count = CmBhav::where('timestamp', $date_of_csv)->count();
//        if ($cmbhav_count == 0) {
//            $fieldsInsert = '';
//            if (($handle = fopen("D:/Ruthmans/cm25AUG2016bhav.csv/cm25AUG2016bhav.csv", "r")) !== FALSE) {
//                if (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
//                    $num = count($data);
//                    $fieldsInsert .= "INSERT INTO `cm_demos`(";
//                    for ($c = 0; $c < $num-1; $c++) {
//                        $fieldsInsert .= ($c == 0) ? '' : ', ';
//
//                        $data[$c] = strtolower($data[$c]);
//                        $fieldsInsert .= "`" . $data[$c] . "`";
//                    }
//                    $fieldsInsert .= ') VALUES (';
//                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
//                        for ($c = 0; $c < $num-1; $c++) {
//
//                            $fieldsInsert .= ($c == 0) ? '' : ', ';
//                            $fieldsInsert .= "'" . $data[$c] . "'";
//                        }
//
//                            $fieldsInsert .= ")";
//                        $s = \DB::insert($fieldsInsert) or die;
//                        dd($fieldsInsert);
//                    }
//
//                }
//            }
//                fclose($handle);
//            }
//    }
    private function getBseBhavCopyUrl(Carbon $date)
    {
        $bse_date = date('dmy', strtotime($date));
        $bse_url = 'http://www.bseindia.com/download/BhavCopy/Equity/EQ' . $bse_date . '_CSV.ZIP';

        return $bse_url;
    }

    private function getBhavCopy(Carbon $date, $exchanges = null)
    {
        if(!$exchanges) {
            $exchanges = ['nfo', 'bse', 'nse'];
            if($date->isToday()) {
                $exchanges[] = 'nfo_lot';
            }
        } else {
            if(!is_array($stock)){
                $exchanges = (array) $exchanges;
            }
        }
        $result = [];

        foreach($exchanges as $exchange) {
            $month = strtoupper($date->format('M'));
            $d = $date->format('d');
            if($exchange == 'nse'){
                $url = 'https://www.nseindia.com/content/historical/EQUITIES/'.$date->year.'/'.$month.'/cm'.$d.$month.$date->year.'bhav.csv.zip';
            } else if($exchange == 'bse'){
                $url = 'http://www.bseindia.com/download/BhavCopy/Equity/EQ' . $date->format('dmy') . '_CSV.zip';
            } else if($exchange == 'nfo'){
                $url = 'https://www.nseindia.com/content/historical/DERIVATIVES/'.$date->year.'/'.$month.'/fo'.$d.$month.$date->year.'bhav.csv.zip';
            } else if($exchange == 'nfo_lot'){
                $url = 'https://www.nseindia.com/content/fo/fo_mktlots.csv';
            }

            $bhav_file = storage_path("daily_bhav/".$exchange."/$date->year/$month/$date->day/".basename($url));
            $csv_file = dirname($bhav_file).'/'.basename($bhav_file, '.zip');
            if($exchange == 'bse')
                $csv_file .= '.csv';

            $dirname = dirname($bhav_file);
            if (!is_dir($dirname))
            {
                mkdir($dirname, 0777, true);
            }
            $file = fopen($bhav_file, 'w');

            $client = new \GuzzleHttp\Client();

            while(true) {
                try {
                    $client->head($url,[
                        'headers' => [
                            'User-Agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13',
                            'Accept'     => 'application/x-zip-compressed',
                        ],
                        'verify' => true,
                        //'debug' => true,
                    ]);
                    break;
                } catch (\GuzzleHttp\Exception\ClientException $e) {
                    if($e->getCode() == 404) {
                        $result[$exchange] =  false;
                        break;
                    }
                    break;
                }
            }
            if(isset($result[$exchange]) && $result[$exchange] === false) {
                continue;
            }

            try {
                $res = $client->get($url , [
                    'sink' => $file,
                    'headers' => [
                        'User-Agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13',
                        'Accept'     => 'application/x-zip-compressed',
                    ],
                    'verify' => true,
                    //'debug' => true,
                ]);
                if($res->getStatusCode() != 200){
                    throw new \Exception('unable to download file, response: '.$res->getStatusCode());
                }

                if($exchange != 'nfo_lot' ) {
                    // Get The Zip File From Server
                    $zip = new ZipArchive;

                    if ($zip->open($bhav_file) != "true") {
                        echo "Error :- Unable to open the Zip File";
                    }
                    /* Extract Zip File */
                    $zip->extractTo(dirname($csv_file));
                    $zip->close();
                }

                $result[$exchange] =  $csv_file;
            } catch (\GuzzleHttp\Exception\RequestException $e) {
                dd(Psr7\str($e->getRequest()));
                if ($e->hasResponse()) {
                    echo Psr7\str($e->getResponse());
                }
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                if(is_file($bhav_file)) unlink($bhav_file);
                if(is_file($csv_file)) unlink($csv_file);
                echo 'Caught exception: ',  $e->getMessage(), "\n";
                dd($e->getResponse()->getBody()->getContents());
            } catch (\Exception $e) {
                if(is_file($bhav_file)) unlink($bhav_file);
                if(is_file($csv_file)) unlink($csv_file);
                echo 'Caught exception: ',  $e->getMessage(), "\n";
                dd($e->getMessage());
            } finally {
                fclose($file);
                if(!isset($result[$exchange])) {
                    $result[$exchange] =  false;
                }
            }
        }

        return $result;
    }

    private function loadBseBhav($file_path, Carbon $date) {
        if(file_exists($file_path))
        {
            if($date->isToday()) {
                $table_name = 'bse_daily_bhavs';
                \DB::table($table_name)->truncate();
                $query = sprintf(
                    "LOAD DATA local INFILE '$file_path' 
                    INTO TABLE " .$table_name." 
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\"' 
                    ESCAPED BY '\"' 
                    LINES TERMINATED BY '\\n' 
                    IGNORE 1 LINES 
                    (`sc_code`,`sc_name`,`sc_group`,`sc_type`,`open`,`high`,`low`,`close`,`last`,`prevclose`,`no_trades`,`no_of_shrs`,`net_turnov`,`tdcloindi`,@var1) 
                    set timestamp = '%s'",$date->format('Y-m-d'));
                \DB::connection()->getpdo()->exec($query);
            }

            if(DB::table('bse_historical_bhavs')->whereDate('bhav_date', $date->toDateString())->count() == 0)
            {
                $query = sprintf(
                    "LOAD DATA local INFILE '$file_path' 
                    INTO TABLE `bse_historical_bhavs` 
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\"' 
                    ESCAPED BY '\"' 
                    LINES TERMINATED BY '\\n' 
                    IGNORE 1 LINES 
                    (`sc_code`,`sc_name`,`sc_group`,`sc_type`,`open`,`high`,`low`,`close`,`last`,`prevclose`,`no_trades`,`no_of_shrs`,`net_turnov`,`tdcloindi`,@var1) 
                    SET  bhav_date = '%s'",$date->format('Y-m-d'));
                \DB::connection()->getpdo()->exec($query);
            }
        }

        return \DB::table('daily_bhav_history')->whereDate('bhav_date', $date->toDateString())->update(['bse' => $file_path]);
    }

    private function loadNseBhav($file_path, Carbon $date) {
        if(file_exists($file_path))
        {
            if($date->isToday()) {
                $table_name = 'nse_daily_bhavs';
                \DB::table($table_name)->truncate();
                $query = sprintf(
                    "LOAD DATA local INFILE '$file_path' 
                    INTO TABLE " .$table_name." 
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\"' 
                    ESCAPED BY '\"' 
                    LINES TERMINATED BY '\\n' 
                    IGNORE 1 LINES 
                    (symbol, series, open, high ,low, close, last, prevclose, tottrdqty, tottrdval, @var1, totaltrades, isin) 
                    set timestamp = '%s'",$date->format('Y-m-d'));
                \DB::connection()->getpdo()->exec($query);
            }

            if(DB::table('nse_historical_bhavs')->whereDate('bhav_date', $date->toDateString())->count() == 0)
            {
                $query = sprintf(
                    "LOAD DATA local INFILE '$file_path' 
                    INTO TABLE `nse_historical_bhavs` 
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\"' 
                    ESCAPED BY '\"' 
                    LINES TERMINATED BY '\\n' 
                    IGNORE 1 LINES 
                    (symbol, series, open, high ,low, close, last, prevclose, tottrdqty, tottrdval, @var1, totaltrades, isin) 
                    set bhav_date = '%s'",$date->format('Y-m-d'));
                \DB::connection()->getpdo()->exec($query);
            }
        }

        return \DB::table('daily_bhav_history')->whereDate('bhav_date', $date->toDateString())->update(['nse' => $file_path]);
    }


    private function loadNfoBhav($file_path, Carbon $date) {
        if(file_exists($file_path))
        {
            if($date->isToday()) {
                $table_name = 'nfo_daily_bhavs';
                \DB::table($table_name)->truncate();
                $query = "LOAD DATA local INFILE '$file_path' 
                    INTO TABLE " .$table_name." 
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\"' 
                    ESCAPED BY '\"' 
                    LINES TERMINATED BY '\\n' 
                    IGNORE 1 LINES 
                    (`instrument`,`symbol`, @var1,`strike`,`option_type`,`open`,`high`,`low`,`close`,`setle_pr`,`contracts`,`val_lakhs`,`open_int`,`chg_in_oi`,@var2)
                    set `expiry_date` = STR_TO_DATE(concat(SUBSTRING_INDEX(@var1,'-',-1), '-',month(str_to_date(SUBSTRING_INDEX(SUBSTRING_INDEX(@var1,'-',2),'-',-1),'%b')),'-', SUBSTRING_INDEX(@var1,'-',1)) ,'%Y-%m-%d'), 
                    timestamp = STR_TO_DATE(concat(SUBSTRING_INDEX(@var2,'-',-1), '-',month(str_to_date(SUBSTRING_INDEX(SUBSTRING_INDEX(@var2,'-',2),'-',-1),'%b')),'-', SUBSTRING_INDEX(@var2,'-',1)) ,'%Y-%m-%d');";
                \DB::connection()->getpdo()->exec($query);
            }

            if(DB::table('nfo_historical_bhavs')->whereDate('bhav_date', $date->toDateString())->count() == 0)
            {
                $query = "LOAD DATA local INFILE '$file_path' 
                    INTO TABLE `nfo_historical_bhavs` 
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\"' 
                    ESCAPED BY '\"' 
                    LINES TERMINATED BY '\\n' 
                    IGNORE 1 LINES 
                    (`instrument`,`symbol`, @var1,`strike`,`option_type`,`open`,`high`,`low`,`close`,`setle_pr`,`contracts`,`val_lakhs`,`open_int`,`chg_in_oi`,@var2)
                    set `expiry_date` = STR_TO_DATE(concat(SUBSTRING_INDEX(@var1,'-',-1), '-',month(str_to_date(SUBSTRING_INDEX(SUBSTRING_INDEX(@var1,'-',2),'-',-1),'%b')),'-', SUBSTRING_INDEX(@var1,'-',1)) ,'%Y-%m-%d'),
                    `bhav_date` = STR_TO_DATE(concat(SUBSTRING_INDEX(@var2,'-',-1), '-',month(str_to_date(SUBSTRING_INDEX(SUBSTRING_INDEX(@var2,'-',2),'-',-1),'%b')),'-', SUBSTRING_INDEX(@var2,'-',1)) ,'%Y-%m-%d');";
                \DB::connection()->getpdo()->exec($query);
            }
        }

        return \DB::table('daily_bhav_history')->whereDate('bhav_date', $date->toDateString())->update(['nfo' => $file_path]);
    }
    private function loadNfoLot($file_path, Carbon $date) {
        if(file_exists($file_path) && $date->isToday())
        {
            $table_name = 'nfo_lot_sizes';
            \DB::table($table_name)->truncate();
            $query = "LOAD DATA local INFILE '$file_path' 
                INTO TABLE " .$table_name." 
                FIELDS TERMINATED BY ',' 
                OPTIONALLY ENCLOSED BY '\"' 
                ESCAPED BY '\"' 
                LINES TERMINATED BY '\\n' 
                IGNORE 1 LINES 
                (@val1, @val2, @val3, @val4 ,@val5)
                SET `name`= trim(@val1), `symbol` = trim(@val2), `current` = trim(@val3), `month_1` = trim(@val4), `month_2` = trim(@val5);";
            \DB::connection()->getpdo()->exec($query);
        }

        return \DB::table('daily_bhav_history')->whereDate('bhav_date', $date->toDateString())->update(['nfo_lot' => $file_path]);
    }

    public function importBhav($date = null)
    {
        return $date;
        if(!$date) {
            $date = Carbon::now();
        } else {
            $date = Carbon::parse($date);
        }

        $history = DB::table('daily_bhav_history')->where('bhav_date',$date->format('Y-m-d'))->first();
        if(!$history) {
            DB::table('daily_bhav_history')->insert(
                ['bhav_date' => $date->toDateString(), 'created_at' => Carbon::now()->toDateTimeString()]
            );
        }

        $uris = $this->getBhavCopy($date);
        if($uris){
            foreach($uris as $key => $path) {
                if(!$path)
                    continue;

                switch($key) {
                    case 'bse' :
                        $this->loadBseBhav($path, $date);
                        break;
                    case 'nse' :
                        $this->loadNseBhav($path, $date);
                        break;
                    case 'nfo' :
                        $this->loadNfoBhav($path, $date);
                        break;
                    case 'nfo_lot' :
                        $this->loadNfoLot($path, $date);
                        break;
                }
            }
        } else {
            dd('no data for '.$date->format(config('rothmans.date_format')));
        }

        return $uris;
    }
}