<?php

namespace App\Http\Controllers\Admin;

use App\PostalCode;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libraries\FormElements\FileUploadService;
use Yajra\Datatables\Datatables;
use Auth;
use Hash;
use App\User;
use App\Models\Logs;
use App\GlobalNote;
use App\Role;
use App\Permission;
use App\Department;
use App\Designation;
use App\Skill;
use App\City;
use App\Activity;
use Event;
use App\Events\SendMail;
use Carbon;
use Illuminate\Http\Request;
use Flash;
use Notification;
use App\Notifications\UserCreated;
use App\Notifications\UserUpdated;
use App\Http\Controllers\EmailController;
use App\Repositories\User\UserRepositoryContract;

use App\UserFilters;

use Session;
use Validator;

use Illuminate\Mail\Mailer;
use Mail;

class UsersController extends Controller
{
    protected $base_model;
    protected $view_path;
    protected $route_base;
    protected $route_base_prefix = 'admin::';
    protected $append_flash;
    protected $validation_rule;

    public function __construct()
    {
        $this->base_model = new User();
        $this->view_path = 'admin.users.';
        $this->route_base = 'users';
        $this->validation_rule = [
            'name' => 'required|max:255',
            'username' => 'required|unique:users|max:255',
            'email' => 'unique:users|email|max:255',
            'official_emailid' => 'required|unique:users|email|max:255',
            'mobile' => 'required|numeric|unique:users,mobile',
            'official_mobile' => 'required|numeric|unique:users,mobile',
            'date_of_joining' => 'required|date',
            'reporting_to' => 'required',
            'department' => 'required',
            'designation' => 'required',
            'zip_code' => 'numeric',
            'address1' => 'max:255',
            'city' => 'max:255',
            'state' => 'max:255',
            'password' => 'required|min:6|confirmed',
            'roles.*' => 'required'
        ];
        $this->messages = [
            'mobile.numeric' => 'Mobile number must be valid',
            'official_mobile.numeric' => 'Mobile number must be valid',
            'official_mobile.required' => 'Mobile number is required',
            'official_emailid.required' => 'Email is required',
            'date_of_joining.required' => 'Joining date is required',
        ];
        
        $this->append_flash = 'User';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->except(['_token']);
        $appedable_query = http_build_query(array_filter($request->except(['_token'])));

        $departments[''] = "Select...";
        foreach(Department::get()->pluck('name','id')->toArray() as $key => $val){
            $departments[$key] = $val;
        }
        $departments = collect($departments);

        $skills[''] = "Select...";
        foreach(Skill::get()->pluck('name','id')->toArray() as $key => $val){
            $skills[$key] = $val;
        }
        $skills = collect($skills);
       // $data = $filters;

        return view($this->view_path . 'index', compact('appedable_query', 'data', 'departments', 'skills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = $this->base_model;
        $roles = Role::get();
        $departments = Department::pluck('name','id');
        $states = PostalCode::orderby('state', 'asc')->pluck('state','state');
        $cities = [];
        $zipcode = [];

        $skill_list = Skill::pluck('name','id');
        $report_list = $item->where('id','!=', Auth::user()->id)->pluck('name','id');
        $designations = Designation::where('department_id',0)->get()->pluck('name','id');
        $user_roles = [];
        $roles_array = $roles->keyBy('id')->toArray();
        $p = [];
        $permissions = Permission::all();
        if($permissions->count())
        {
            $permissions = $permissions->groupBy('group_by');
            if($permissions->count())
            {
                foreach ($permissions as $key => $perms)
                {
                    $p[$key] = $perms->groupBy('controllers');
                }
            }
        }

        $permissions = collect($p);
        $role_perms = $roles->keyBy('id')->map(function ($item, $key) {
            return $item->perms->pluck('id');
        });
        $user_perms = [];
        $default_values = $departments->keys()->first();

        return view($this->view_path . 'create',compact('item','roles','user_roles','permissions','roles_array','role_perms', 'user_perms','departments','designations','skill_list','report_list','default_values', 'states', 'cities','zipcode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation_rule,$this->messages);

        if ($validator->fails()) {
            return redirect(route($this->route_base_prefix . $this->route_base.'.create'))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $item = $this->base_model;

            if(!empty($request->get('dob') ))
                $item->dob = date('Y-m-d', strtotime($request->get('dob')));

            if( !empty($request->get('date_of_anniversary')))
                $item->date_of_anniversary = date('Y-m-d', strtotime($request->get('date_of_anniversary')));

            if( !empty($request->get('date_of_joining')) )
                $item->date_of_joining = date('Y-m-d', strtotime($request->get('date_of_joining')));
            if( !empty($request->get('email')) )
                 $item->email = $request->get('email');
            else
                $item->email = null;

            $skills = [];
            if(count($request->get('skills')) > 0)
            {
                foreach ($request->get('skills') as $skill_value)
                {
                    if(is_numeric($skill_value))
                    {
                        $find_skill = Skill::findOrFail($skill_value);
                        if($find_skill)
                        {
                            $skills[] = $find_skill->id;
                        }
                    }
                    else{
                        $skills[] = Skill::firstOrCreate(["name" => $skill_value])->id;
                    }        
                }
            }

            $item->name = $request->get('name');
            $item->username = $request->get('username');
            $item->gender = $request->get('gender');
            $item->marital_status = $request->get('marital_status');
            $item->address1 = $request->get('address1');
            $item->address2 = $request->get('address2');
            $item->zip_code = $request->get('zip_code');
            $item->city = $request->get('city');
            $item->state = $request->get('state');
            $item->mobile = $request->get('mobile');
            $item->official_emailid = $request->get('official_emailid');
            $item->official_mobile = $request->get('official_mobile');
            $item->qualification = $request->get('qualification');
            $item->facebook = $request->get('facebook');
            $item->twitter = $request->get('twitter');
            $item->linkedin = $request->get('linkedin');
            $item->reporting_to = $request->get('reporting_to');
            $item->designation = $request->get('designation');
            $item->department = $request->get('department');
            $item->skills = $skills;
            $item->status = $request->get('status');
            $item->password = bcrypt($request->get('password'));
            if(! $request->has('roles')){
                $item->status = 0;
                 // $message = 'A new User '.$item->email.' created on crm. Please assign roles';
                 //    event(new SendMail($message));
            }


            if(! $item->save())
            {
                Flash::success('Error occurred! Please try again after some time ');
                return redirect(route($this->route_base_prefix . $this->route_base . '.create'))
                    ->withInput($request->all());
            }

            if(Auth::user()->hasRole('super-admin')) {
                if($request->has('roles')){

                    $item->roles()->sync($request->get('roles'));
                }
                else
                {
                    $item->roles()->sync([3]); // assign normal user role if not provide any role
                    //Send Mail
                }

                if($request->has('perms'))
                    $item->perms()->sync($request->get('perms'));
                else
                    $item->perms()->sync([]);
            }

            if(!empty($request->get('note_msg')))
            {
                $list = User::find($item->id);
                $note = new GlobalNote();
                $note->note = $request->get('note_msg');
                $note->user_id = Auth::user()->id;
            
                $list->notes()->save($note);
            }
             
             $activityinput = array_merge(
            ['text' => Auth::user()->name .' created a new profile of ' .$item->name,
            'user_id' => Auth()->id(),
            'type' => 'user',
            'type_id' =>  $item->id]
            );

            Logs::create($activityinput);   

            

            // Notification
            //$item->notify(new UserCreated($item));

            $message = "A new user is create please verify at http://96.126.103.143:8081/verifyUser/" . $item->id;
            $email = $item->email;

            Mail::raw($message, function($m) use ($email){
                $m->to($email);
                $m->subject('New User Created');
                $m->getSwiftMessage();
            });

            Flash::success($this->append_flash . ' added successfully!');
            return redirect(route($this->route_base_prefix . $this->route_base.'.index'));
           }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = $this->base_model->with('notes.user','activities')->findOrFail($id);
        //dd($item->toArray());
        $departments = Department::pluck('name','id');
        $states = PostalCode::orderBy('state','asc')->get()->pluck('state','state');
        $cities = PostalCode::where('state', $item->state)->orderBy('taluk','asc')->get()->pluck('taluk','taluk');
        $zipcode = PostalCode::where('taluk', $item->zip_code)->orderBy('pincode','asc')->get()->pluck('pincode', 'pincode');
        $default_values = $departments->keys()->first();
        $new_designations = [];

        if($item->department){
            foreach($item->department as $key => $dept_value)
            {
                $designations[$key] = Designation::where('department_id',$dept_value)->get();
                foreach($designations[$key]->pluck('name','id')->toArray() as $key => $value)
                {
                    $new_designations[$key] = $value;
                }
            }
            $designations = collect($new_designations);
        }
        else
             $designations = Designation::where('department_id', $departments->keys()->first())->get()->pluck('name','id'); 

        if($item->department)
        {
          foreach($item->department as $key => $dept_value)
            {
        
                    $report_list[$key] = $item->whereRaw("CONCAT(\",\", replace(replace(replace(replace(trim(department), '\t', ''), '[', ''), '\"', ''), ']', '')  , \",\") REGEXP \",(".$dept_value."),\"")->get();

                    foreach($report_list[$key]->pluck('name','id')->toArray() as $key => $value)
                    {
                        $new_report_list[$key] = $value;
                    }
                 
            }
            $report_list = collect($new_report_list);
        }
        else
           $report_list = $item->where('id','!=', Auth::user()->id)->pluck('name','id');

        $skill_list = Skill::pluck('name','id');
        $roles = Role::with('perms')->get();
        
        $user_roles = $item->roles->pluck('id')->toArray();
        $user_perms = $item->perms->pluck('id')->toArray();

        $roles_array = $roles->keyBy('id')->toArray();

        $p = [];
        $permissions = Permission::all();
        if($permissions->count())
        {
            $permissions = $permissions->groupBy('group_by');
            if($permissions->count())
            {
                foreach ($permissions as $key => $perms)
                {
                    $p[$key] = $perms->groupBy('controllers');
                }
            }
        }

        $permissions = collect($p);
        $role_perms = $roles->keyBy('id')->map(function ($item, $key) {
            return $item->perms->pluck('id');
        });

        return view($this->view_path . 'edit', compact('item', 'user_roles','roles','permissions', 'roles_array', 'role_perms' , 'user_perms', 'departments','designations','skill_list','report_list','default_values', 'states', 'cities', 'zipcode'));
    }

    public function designationList(Request $request)
    {
        $designation = [];
        $designation['list'] = Designation::whereIn('department_id', $request->get('id'))->get();
        $admins = $this->base_model->whereRoleIs('super-admin')->get()->pluck('id');
        $designation['users'] = $this->base_model
            ->whereRaw("CONCAT(\",\", replace(replace(replace(replace(trim(department), '\t', ''), '[', ''), '\"', ''), ']', '')  , \",\") REGEXP \",(".implode('|', $request->get('id'))."),\"")
            ->orWhereIn("id", $admins)
            ->get();

        return $designation;
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|max:255',
            'email' => 'email|max:255|unique:users,email,' . $id,
            'official_emailid' => 'required|email|max:255|unique:users,official_emailid,' . $id,
            'mobile' => 'required|numeric|unique:users,mobile,' . $id,
            'official_mobile' => 'required|numeric|unique:users,mobile,' . $id,
            'date_of_joining' => 'required|date',
            'reporting_to' => 'required',
            'department' => 'required',
            'designation' => 'required',
            'zip_code' => 'numeric',
            'address1' => 'max:255',
            'city' => 'max:255',
            'state' => 'max:255',
            'roles.*' => 'required'
        ];

        $messages = [
            'mobile.numeric' => 'Mobile number must be valid',
            'official_emailid.required' => 'Email is required',
            'date_of_joining.required' => 'Joining date is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
           
            return redirect(route($this->route_base_prefix . $this->route_base . '.edit', [$this->route_base => $id]))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $item = $this->base_model->findOrFail($id);

            $skills = [];
            if(count($request->get('skills')) > 0)
            {
                foreach ($request->get('skills') as $skill_value)
                {
                    if(is_numeric($skill_value))
                    {
                        $find_skill = Skill::findOrFail($skill_value);
                        if($find_skill)
                        {
                            $skills[] = $find_skill->id;
                        }
                    }
                    else{
                        $skills[] = Skill::firstOrCreate(["name" => $skill_value])->id;
                    }
                    
                }
            }

            if($request->get('date_of_anniversary') == null)
                $item->date_of_anniversary = null;
            else
                $item->date_of_anniversary = date('Y-m-d', strtotime($request->get('date_of_anniversary')));

            if(!empty($request->get('dob') ))
                $item->dob = date('Y-m-d', strtotime($request->get('dob')));
            else
                $item->dob = null;

            if( !empty($request->get('date_of_anniversary')))
                $item->date_of_anniversary = date('Y-m-d', strtotime($request->get('date_of_anniversary')));
            else
                $item->date_of_anniversary = null;

            if( !empty($request->get('date_of_joining')) )
                $item->date_of_joining = date('Y-m-d', strtotime($request->get('date_of_joining')));
            else
                $item->date_of_joining = null;

            if( !empty($request->get('email')) )
                 $item->email = $request->get('email');
            else
                $item->email = null;

            $item->name = $request->get('name');
            $item->gender = $request->get('gender');
            $item->marital_status = $request->get('marital_status');
            $item->address1 = $request->get('address1');
            $item->address2 = $request->get('address2');
            $item->zip_code = $request->get('zip_code');
            $item->city = $request->get('city');
            $item->state = $request->get('state');
            $item->mobile = $request->get('mobile');
            $item->official_emailid = $request->get('official_emailid');
            $item->official_mobile = $request->get('official_mobile');
            $item->qualification = $request->get('qualification');
            $item->facebook = $request->get('facebook');
            $item->twitter = $request->get('twitter');
            $item->linkedin = $request->get('linkedin');
            $item->reporting_to = $request->get('reporting_to');
            $item->designation = $request->get('designation');
            $item->department = $request->get('department');
            $item->skills = $skills;
            $item->status = $request->get('status');
            $item->password = bcrypt($request->get('password'));

           if(Auth::user()->hasRole('super-admin'))
            {
                
                if($request->has('roles'))
                {
                    $item->roles()->sync($request->get('roles'));
                }
                else{
                    $item->status = 0;
                    $item->roles()->sync([]);
                }

                if($request->has('perms')){
                    $item->perms()->sync($request->get('perms'));
                }
                    
                else{
                    $item->perms()->sync([]);
                }
            }

            if(! $item->save($request->except('_token')))
            {
                Flash::success('Error occurred! Please try again after some time ');
                return redirect(route($this->route_base_prefix . $this->route_base . '.edit', [$this->route_base => $id]))
                    ->withInput($request->all());
            }
        
            $activityinput = array_merge(
            ['text' => Auth::user()->name .' updated ' .$item->name. ' profile' ,
            'user_id' => Auth()->id(),
            'type' => 'user',
            'type_id' =>  $item->id]
            );

            Logs::create($activityinput);
            // Notification
            // if($request->has('$item->notify(new UserCreated($item));') || $request->has('by_email'))
            // {
            //      //dd('notify');
            //     $item->notify(new UserUpdated($item));
            // }       
            Flash::success($this->append_flash . ' updated successfully!');
            return redirect(route($this->route_base_prefix . $this->route_base.'.index'));
        }
    }

    public function note(Request $request)
    {

        if(!empty($request->get('note')))
        {
            $id = $request->get('id');
            $list = User::find($id);
            $note = new GlobalNote();
            $note->note = $request->get('note');
            $note->user_id = Auth::user()->id;
            
                if($list->notes()->save($note))
                {
                    $item = $this->base_model->with('notes.user')->findOrFail($id);
                    $response['notes'] = $item->notes->last();
                    $response['status'] = true;
                }
        }
        else{
            $response['status'] = false;
        }
        echo json_encode($response);
        exit;
    }

    public function addDepartment(Request $request)
    {

        if(!empty($request->get('name')))
        {
            $dept = new Department();
            $dept->name = $request->get('name');
            $dept->description = $request->get('description');
            
                if($dept->save())
                {
                    $response['dept'] = $dept->get()->last();
                    $response['status'] = true;
                }
        }
        else{
            $response['status'] = false;
        }
       
        echo json_encode($response);
        exit;
    }

    public function logs($id)
    {
         $item = $this->base_model->with('logs')->findOrFail($id);
        //  $data = $this->Booking->find('first', array('conditions' => array('Booking.id' => $id)));
        // $rawdata = json_decode($data['Booking']['rawdata'], TRUE);
        // $outbound_info = $rawdata['itineraries'][0]['outbound']['flights']; 
         // $before = json_decode($item['activities']['before'], TRUE);
         //dd(json_decode($item['activities'], TRUE)); 
         return view($this->view_path . 'logs', compact('item'));
    }

    public function saveDesignation(Request $request)
    {
        if(!empty($request->get('name')))
        {
            $desg = new Designation();
            $desg->name = $request->get('name');
            $desg->description = $request->get('description');
            $desg->department_id = $request->get('department_id');
             
                if($desg->save())
                {
                    $response['dept'] = Department::where('id', $desg->department_id)->get();
                    $response['desg'] = $desg->get()->last();
                    $response['status'] = true;
                }
        }
        else{
            $response['desg_msg'] = 'Name field is required';
        }
       
        return $response;
    }

    public function activities()
    {
        $id = Auth::user()->id;
        $item = $this->base_model->with('activities')->findOrFail($id); 
        return view($this->view_path . 'activities', compact('item'));
    }

    public function passwordSettings($id)
    {

        $item = $this->base_model->findOrFail($id);
        return view($this->view_path . 'change_password', compact('item'));
    }

    public function changePassword(Request $request, $id)
    {
        if(Auth::user()->hasRole('super-admin'))
        {
            $passwordrules = [
                'password' => 'required|min:6|confirmed',
            ];
            $pswrd_msgs = [];
        }
        else{
            $passwordrules = [
                'password' => 'required|min:6|confirmed',
                'c_password' => 'required|min:5',
            ];

            $pswrd_msgs = [
                'c_password.required' => 'Current password required',
            ];
        }


        
        $validator = Validator::make($request->all(), $passwordrules,$pswrd_msgs);

        if ($validator->fails()) {
            return redirect(route($this->route_base_prefix . $this->route_base . '.passwordsettings', [$this->route_base => $id]))
                ->withErrors($validator)
                ->withInput($request->all());
        }
        else{
            if(!Auth::user()->hasRole('super-admin'))
            {
                if(Hash::check($request->get('c_password'), Auth::user()->password)){
                    $item = $this->base_model->findOrFail($id);
                    $item->password = bcrypt($request->get('password'));
                    $item->save();
                    Flash::success($this->append_flash . ' change password successfully!');
                    return redirect(route($this->route_base_prefix . $this->route_base.'.index'));

                }
                else{
                    Session::flash('message', "Password doesnot match");
                    return redirect(route($this->route_base_prefix . $this->route_base.'.passwordsettings', [$this->route_base => $id]));
                }
            }

            else{
                $item = $this->base_model->findOrFail($id);
                $item->password = bcrypt($request->get('password'));
                $item->save();
                Flash::success($this->append_flash . ' change password successfully!');
                return redirect(route($this->route_base_prefix . $this->route_base.'.index'));
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if(!Auth::user()->can('delete-user')){
            Flash::success('Error occurred! Please try again after some time ');
            return redirect(route($this->route_base_prefix . $this->route_base.".index"));
        }

        $item = $this->base_model->find($id);

        $activityinput = array_merge(
            ['text' => Auth::user()->name .' delete a user ' .$item->name,
            'user_id' => Auth()->id(),
            'type' => 'user',
            'type_id' =>  $item->id]
            );

            Logs::create($activityinput);

        if(!$item || !$item->delete())
        {
            Flash::success('Error occurred! Please try again after some time ');
        }
        else
        {
            Flash::success( $this->append_flash . ' deleted successfully!');
        }

        return redirect(route($this->route_base_prefix . $this->route_base.".index"));
    }
    

    public function copyImage(FileUploadService $service, Request $request)
    {
        $image =  $request->image;
        $userId = $request->id;

        $rule = [
            'image' => 'required|image|mimes:jpeg,bmp,png',
        ];
        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return response($validator, 200);
        }

        if(!empty($userId))
        {
            $response['msg'] = $service->uploadImage(array('image'=>$image,'userId'=> $userId));
            $response['image'] = $this->base_model->findOrFail($userId);
        }
        // else
        // {
        //     $response = $service->uploadImage(array('image'=>$image,'userId'=>''));
        // }
        echo json_encode($response);
        exit;
    }

    /**
     * search users.
     *
     */
    public function searchBar(UserFilters $filters)
    {
        $response = User::filter($filters)->get();
        echo json_encode($response);
        exit;
    }

    public function allUser(UserFilters $filters)
    {
        $users = User::filter($filters)->select('*')->where('user_type', 'User');
        return Datatables::of($users)
        ->editColumn('created_at', function ($users) {
                 return $users->created_at ? with(new Carbon($users->created_at))
                ->format('d/m/Y') : '';
        })
        ->addColumn('status', function ($users) {
                 return '<td><span class="label label-'.($users->status? "success" : "danger").'">'.($users->status? "Active" : "Inactive").'</span></td>';
        })
        ->add_column('action', function ($users) {
                $edit = '';
                $edit = '<a href="'. route('admin::users.edit', $users->id).' "  ><i class="icon-pencil7"></i></a>' ;

                if(Auth::user()->can('delete-user')) {
                    $edit .= ' <a onclick="del_action(\'' . route('admin::users.destroy', $users->id) . '\')" class="delete_anchor"><i class="glyphicon  glyphicon-remove"></i></a> ';
                }

                return $edit;
        })->make(true); 
    }

    public function checkUsername(Request $request)
    {
           $rules = [
           'username' => 'required|unique:users|max:255',
            ];

        $validator = Validator::make($request->all(), $rules);
        // dd($validator->fails());
        if ($validator->fails()) {
                $errors = $validator->errors();
                $name = $request->get('name');
                $username = $this->random_username($name);
                $response['status'] = 'success';
                $response['username'] = $username;
                $response['message'] =  json_decode($errors); 
        } else {

                  $response['status'] = 'error';
            }
       
        echo json_encode($response);
        exit;
    }

    private function random_username($string) {
        $username = array();
        for($i = 0;$i <=2;$i++){
           $pattern = " ";
            $firstPart = strstr(strtolower($string), $pattern, true);
            $nrRand = rand(0, 1000);
                if($firstPart != false){
                    $secondPart = strstr(strtolower($string), $pattern, false);
                    $username[] = $firstPart.$secondPart.'_'.$nrRand;
                }
                else{
                    $firstPart = strtolower($string);
                    $username[] = $firstPart.'_'.$nrRand;
                } 
        }

        return $username;
    }

     public function getCity(Request $request)
    {
        $response['status'] = 'error';
        if(!empty($request->get('state'))) {
            $cities = PostalCode::selectRaw('Distinct(taluk) as city')->where('state', $request->get('state'))->orderBy('taluk', 'asc')->get()->pluck('city', 'city');
            if(!empty($cities))
            {
                $response['status'] = 'success';
                $response['message'] = 'City list';
                $response['cities'] = $cities;
            }
            else{
                $response['status'] = 'error';
            }
        }
        return $response;
    }

    public function getZipcode(Request $request)
    {
        $response['status'] = 'error';

        if(!empty($request->get('taluk'))) {

            $zipcode = PostalCode::select('pincode')->where('taluk', $request->get('taluk'))->where('state', $request->get('state'))->orderBy('pincode', 'asc')->get()->pluck('pincode', 'pincode');
            if(!empty($zipcode))
            {
                $response['status'] = 'success';
                $response['message'] = 'City list';
                $response['pincode'] = $zipcode;
            }
            else{
                 $response['status'] = 'error';
            }
        }
       
        return $response;
    }

    public function verify(Request $request, $id){

        $user = User::find($id);
        $user->status = 1;
        $user->save();

        return redirect('login');
    }

}
