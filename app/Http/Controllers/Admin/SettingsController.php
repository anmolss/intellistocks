<?php

namespace App\Http\Controllers\Admin;

use App\Ip;
use App\Models\Filters\StockFilters;
use App\NfoDailyBhav;
use App\Repositories\ServiceDelivery\ServiceDeliveryRepository;
use App\SettingIp;
use App\Stock;
use App\Team;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

class SettingsController extends Controller
{
    public function manageIp($type)
    {
        $global_ip = Ip::where(['is_global' => 1])->get()->pluck('ip');

        $team_ids = DB::table('teams')->select(['teams.*'])
            ->leftJoin('setting_ips', 'teams.id', '=', 'setting_ips.restricted_id')
            ->leftJoin('ips', 'ips.id', '=', 'setting_ips.ip_id')
            ->where('setting_ips.restricted_type', 'App\Team')
            ->get();
        $team_list = [];
        if(count($team_ids) > 0)
        {
            $team_ids = collect($team_ids)->pluck('id');
            $team_list = Team::with('ipList')->whereIn('id', $team_ids)->paginate(15);
        }

        $user_ids = DB::table('users')->select(['users.*'])
            ->leftJoin('setting_ips', 'users.id', '=', 'setting_ips.restricted_id')
            ->leftJoin('ips', 'ips.id', '=', 'setting_ips.ip_id')
            ->where('setting_ips.restricted_type', 'App\User')
            ->get();
        $user_list = [];
        if(count($user_ids) > 0)
        {
            $user_ids = collect($user_ids)->pluck('id');
            $user_list = User::with('ipList')->whereIn('id', $user_ids)->paginate(15);
        }

        return view('admin.settings.ip', compact('type', 'global_ip', 'user_list', 'team_list'));
    }

    public function updateIp(Request $request, $type)
    {
        $data = array_filter(explode(", ", $request->get('ip')));
        
        $ips = [];
        foreach ($data as $ip)
        {
            if (!filter_var($ip, FILTER_VALIDATE_IP) === false) {
                $ips[] = Ip::firstOrCreate(["ip" => $ip]);
            }

        }
        $ips = collect($ips)->pluck('id')->toArray();

        if($type == 'teams')
        {
            Team::find($request->get('team_id'))->ipList()->sync($ips);
        }
        else if($type == 'users')
        {
            User::find($request->get('user_id'))->ipList()->sync($ips);
        }
        else if($type == 'global')
        {
            DB::transaction(function() use ($ips)
            {
                DB::table('ips')->update(array('is_global' => 0));
                DB::table('ips')->whereIn('id', $ips)->update(['is_global' => 1]);
            });
        }

        Flash::success('Ip updated successfully!');
        return redirect(route('admin::manage_ip.show', ['type'=> $type]));
    }

    public function getTeam(Request $request)
    {
        return Team::with('ipList')
            ->where('name', 'LIKE', "%{$request->get('name')}%")
            ->orderBy('name', 'ASC')
            ->limit(10)
            ->get();
    }
    public function getUser(Request $request)
    {
        return User::with('ipList')
            ->where('name', 'LIKE', "%{$request->get('name')}%")
            ->orWhere('mobile', 'LIKE', "%{$request->get('name')}%")
            ->orWhere('username', 'LIKE', "%{$request->get('name')}%")
            ->orWhere('email', 'LIKE', "%{$request->get('name')}%")
            ->orderBy('name', 'ASC')
            ->limit(10)
            ->get();
    }

    public function indexWatchlistStock(Request $request){
        $request->merge([ 'watchlist' => true, "created_by" => auth()->user()->id ]);
        $filter = new StockFilters($request);
        $stocks = Stock::filter($filter)->with(['created_by'])->get();

        if($request->ajax()){
            return $stocks;
        }

        return view('admin.settings.stock_watchlist', compact('stocks'));
    }

    public function storeWatchlistStock(Request $request){
        $requestData = $request->except('_tokens');

        if ($requestData['exchange'] == 'nse'){
            $data = Stock::where('symbol', $requestData['symbol'])->where('watchlist', true)->where('created_by', auth()->user()->id)->first();

        } elseif ($requestData['exchange'] == 'nfo'){
            $data = Stock::where('symbol', $requestData['symbol'])
                ->where('instrument', $requestData['instrument'])
                ->where('expiry', Carbon::parse($requestData['expiry'])->toDateString())
                ->where('watchlist', true)
                ->where('created_by', auth()->user()->id);

            if(in_array($requestData['instrument'] , ['OPTIDX','OPTSTK'])){
                $data =  $data->where('strike', $requestData['strike'])
                    ->where('option_type', $requestData['option_type']);
            }
            $data = $data->first();
        }
        if(is_null($data)){
            $requestData;
            $requestData['created_by'] = auth()->user()->id;
            $requestData['watchlist'] = true;
            $requestData['expiry'] = Carbon::parse($requestData['expiry'])->toDateString();


            $stock = new Stock($requestData);

            if($stock->save()){
                return ['msg' => 'Stock saved in watchlist', 'type' => 'success', 'stock' => $stock];
            }
        } else {
            return ['msg' => 'Stock Already in watchlist', 'type' => 'success'];
        }

        return ['msg' => 'Something went wrong!', 'type' => 'error'];
    }


    public function destroyWatchlistStock(Request $request){
        $item = Stock::findOrFail($request->get('id'));

        if(! $item->delete())
        {
            return ['deleted' => false];
        }
        else
        {
            return ['deleted' => true];
        }
    }

    public function getStocksWithWatchlist(Request $request){
        if($request->has('no_group') && $request->get('no_group') == true ) {
            return NfoDailyBhav::selectRaw('trim(symbol) as symbol, ROUND(close,2) as price, DATE_FORMAT(expiry_date, "%d-%b-%Y") as expiry_date, strike, option_type')->where('symbol', $request->get('type'))->where('instrument', $request->get('instrument'))->orderBy('symbol','asc')->get();
        }

        return \DB::select("(SELECT trim(ndb.symbol)  as company,
            trim(ndb.symbol) as symbol, ROUND(ndb.last,2) as price, 'nse' as exchange, ifnull(ss.`instrument`, '') as instrument, ss.watchlist
            FROM `nse_daily_bhavs` ndb left join stocks ss on ndb.symbol = ss.symbol and ss.watchlist = 1 and ss.target = 0 and ss.created_by != ". auth()->user()->id ."
            WHERE ndb.symbol LIKE ? and series = 'EQ' and ss.symbol is null
            )
            union
            (
            SELECT trim(ndb.symbol) as company, trim(ndb.symbol) as symbol, '' as price,'nfo' as exchange, ifnull(ndb.`instrument`, '') as instrument, ss.watchlist
            FROM `nfo_daily_bhavs` ndb 
             left join stocks ss on ndb.symbol = ss.symbol and ss.watchlist = 1  and ss.target = 0 and ss.created_by != ". auth()->user()->id ."
            WHERE ndb.`symbol` LIKE ?  and ss.symbol is null
            group by  ndb.`instrument` , ndb.`symbol`
            )
            ORDER BY company ASC, exchange desc, instrument asc limit 15;
            ", [ $request->get('type')."%", $request->get('type')."%"]);

    }

    public function getStocksWithAlert(Request $request){
        if($request->has('no_group') && $request->get('no_group') == true ) {
            return NfoDailyBhav::selectRaw('trim(symbol) as symbol, ROUND(close,2) as price, DATE_FORMAT(expiry_date, "%d-%b-%Y") as expiry_date, strike, option_type')->where('symbol', $request->get('type'))->where('instrument', $request->get('instrument'))->orderBy('symbol','asc')->get();
        }

        return \DB::select("(SELECT trim(ndb.symbol)  as company,
            trim(ndb.symbol) as symbol, ROUND(ndb.last,2) as price, 'nse' as exchange, '' as instrument
            FROM `nse_daily_bhavs` ndb 
            WHERE ndb.symbol LIKE ? and series = 'EQ'
            )
            union
            (
            SELECT trim(ndb.symbol) as company, trim(ndb.symbol) as symbol, '' as price,'nfo' as exchange, ifnull(ndb.`instrument`, '') as instrument
            FROM `nfo_daily_bhavs` ndb 
            WHERE ndb.`symbol` LIKE ?
            group by  ndb.`instrument` , ndb.`symbol`
            )
            ORDER BY company ASC, exchange desc, instrument asc limit 15;
            ", [ $request->get('type')."%", $request->get('type')."%"]);

    }

    public function indexAlertStock(Request $request){
        $request->merge([ 'watchlist' => 0, "created_by" => auth()->user()->id ]);
        $filter = new StockFilters($request);
        $stocks = Stock::filter($filter)->with(['created_by'])->get();

        if($request->ajax()){
            return $stocks;
        }

        return view('admin.settings.stock_alertlist', compact('stocks'));
    }

    public function storeAlertStock(Request $request){
        $requestData = $request->except('_tokens');

        $requestData;
        $requestData['created_by'] = auth()->user()->id;
        $requestData['watchlist'] = 0;
        $requestData['expiry'] = Carbon::parse($requestData['expiry'])->toDateString();

        $stock = new Stock($requestData);

        if($stock->save()){
            return ['msg' => 'Stock saved in watchlist', 'type' => 'success', 'stock' => $stock];
        }

        return ['msg' => 'Something went wrong!', 'type' => 'error'];
    }


    public function destroyAlertStock(Request $request){
        $item = Stock::findOrFail($request->get('id'));

        if(! $item->delete())
        {
            return ['deleted' => false];
        }
        else
        {
            return ['deleted' => true];
        }
    }


    public function getWatchlistStock(Request $request){
        return (new ServiceDeliveryRepository())->stockWatchList();
    }
}
