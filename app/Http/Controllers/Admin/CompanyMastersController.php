<?php

namespace App\Http\Controllers\Admin;

use App\CompanyMaster;
use App\CompanyNameChange;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompanyMastersController extends Controller
{
    protected $base_model;
    protected $view_path;
    protected $route_base;
    protected $route_base_prefix = 'admin::';
    protected $append_flash;
    protected $validation_rule;

    public function __construct()
    {
        $this->base_model = new CompanyMaster();
        $this->view_path = 'admin.company.';
        $this->route_base_prefix = 'admin::';
        $this->route_base = 'companies';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = CompanyMaster::all();
       // dd($list);
        return view($this->view_path . 'index', compact('list'));
    }
    public function show($company)
    {
        $company = CompanyMaster::fullDetail()->findOrFail($company)->toArray();
        //dd($company);
        return view($this->view_path . 'show', compact('company'));
    }


    /**
     * @return mixed
     */
    public function getStockList(Request $request)
    {
        return CompanyMaster::all()->values('name', 'id');
    }
}