<?php

namespace App\Http\Controllers\Admin;

use App\Models\UserService;
use App\Repositories\ServiceDelivery\ServiceDeliveryRepository;
use App\Service;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Carbon;
use Auth;
use App\Models\Leads;
use App\Repositories\Task\TaskRepositoryContract;
use App\Repositories\Lead\LeadRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Client\ClientRepositoryContract;
use App\Models\Transection;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\Setting\SettingRepositoryContract;

class DashboardController extends Controller
{
	protected $users;
	protected $clients;
	protected $leads;

	public function __construct(
        UserRepositoryContract $users,
        ClientRepositoryContract $clients,
        leadRepositoryContract $leads
    ) {
        $this->users = $users;
        $this->clients = $clients;
        $this->leads = $leads;
    }

    public function index()
    {
        if(Auth::user()->hasRole('customer')){
            return Redirect::to(route('front::dashboard'));
        }
        $dt_start = Carbon::now();
        $dt_end = Carbon::now()->addDays(10);

        $expiringServices = DB::table('user_services')->where('service_status', 'Active')->where('expire_date','>=',$dt_start)->where('expire_date','<=',$dt_end)->count();

        // this week results
        $fromDate = Carbon\Carbon::now()->toDateString();
        $tillDate = Carbon\Carbon::now()->addDays(30)->toDateString();

        $renewals = DB::table('user_services')
                        ->selectRaw('users.id AS user_id, users.name AS user_name, users.email, rm.name as rm_name, users.mobile, user_services.service_details, user_services.plan_details, user_services.activation_date, user_services.expire_date')
                        ->leftJoin('users', 'user_services.user_id', '=', 'users.id')
                        ->leftJoin('user_client_meta', 'user_services.user_id', '=', 'user_client_meta.fk_user_id')
                        ->leftJoin('users as rm', 'user_client_meta.fk_relation_manager_id', '=', 'rm.id')
                        ->leftJoin('plans', 'user_services.plan_id', '=', 'plans.id')
                        ->whereBetween( 'user_services.expire_date', [$fromDate, $tillDate] )
                        ->orderBy('user_services.activation_date', 'asc')
                        ->whereNotNull('user_services.expire_date')
                        ->whereNotNull('user_services.plan_id')->get();

        $lapsed_renewals = DB::table('user_services')
                        ->selectRaw('users.id AS user_id, users.name AS user_name, users.email, rm.name as rm_name, users.mobile, user_services.service_details, user_services.plan_details, user_services.activation_date, user_services.expire_date AS renewal_date, DATE(NOW()) as now')
                        ->leftJoin('users', 'user_services.user_id', '=', 'users.id')
                        ->leftJoin('user_client_meta', 'user_services.user_id', '=', 'user_client_meta.fk_user_id')
                        ->leftJoin('users as rm', 'user_client_meta.fk_relation_manager_id', '=', 'rm.id')
                        ->leftJoin('plans', 'user_services.plan_id', '=', 'plans.id')
                        ->where( 'user_services.expire_date', '<', DB::raw('DATE(NOW())'))
                        ->whereNotNull('user_services.expire_date')
                        ->whereNotNull('plan_id')->get();

        $pending_verifications = DB::table('user_services')
                        ->selectRaw('users.id AS user_id, users.name AS user_name, users.mobile, rm.name as rm_name, user_services.service_details, user_services.activation_date, users.transcript_status, users.kyc_status, users.consent_status')
                        ->leftJoin('users', 'user_services.user_id', '=', 'users.id')
                        ->leftJoin('user_client_meta', 'user_services.user_id', '=', 'user_client_meta.fk_user_id')
                        ->leftJoin('users as rm', 'user_client_meta.fk_relation_manager_id', '=', 'rm.id')
                        ->leftJoin('plans', 'user_services.plan_id', '=', 'plans.id')
                        ->where('service_status', 'Pending')
                        ->where(function($query) {
                            $query->where('users.transcript_status', 'No')
                                ->orWhere('users.kyc_status', 'No')
                                ->orWhere('users.consent_status', 'No');
                        })->get();


        $temp_ext = DB::table('user_services')
                        ->selectRaw('users.id AS user_id, users.name AS user_name, users.email, users.mobile, rm.name as rm_name, user_services.service_details, user_services.plan_details, user_services.activation_date, user_service_actions.extn_days')
                        ->leftJoin('users', 'user_services.user_id', '=', 'users.id')
                        ->leftJoin('user_client_meta', 'user_services.user_id', '=', 'user_client_meta.fk_user_id')
                        ->leftJoin('users as rm', 'user_client_meta.fk_relation_manager_id', '=', 'rm.id')
                        ->leftJoin('plans', 'user_services.plan_id', '=', 'plans.id')
                        ->leftJoin('user_service_actions', 'user_services.id', '=', 'user_service_actions.user_service_id')
                        ->where('service_status', 'TempExtended')
                        ->where('user_service_actions.activity', 'temporary_extend')
                        ->orderBy('user_service_actions.id', 'desc')
                        ->get();

        $transactions = DB::table('transactions')
                        ->selectRaw('users.id AS user_id, users.name AS user_name, transactions.amount as amount, username, mobile, service_details, plan_details, pay_type, payment_type, transactions.id AS invoice_id, transactions.created_at AS created_at')
                        ->leftJoin('user_services', 'fk_user_service_id', '=', 'user_services.id')
                        ->leftJoin('users', 'user_services.user_id', '=', 'users.id')
                        ->leftJoin('plans', 'user_services.plan_id', '=', 'plans.id')
                        ->whereNotNull('transactions.transaction_id')
                        ->orderBy('transactions.id', 'desc')
                        ->limit(5)
                        ->get()->toArray();
        //$today_date = Carbon\Carbon::now()->toDateString();

        $upcoming_payments = DB::table('transactions')
                        ->selectRaw('date(date) as date, transactions.amount as amount, transactions.pay_no, users.username, users.name, rm.name as rm_name, users.id as user_id, users.email, users.mobile, service_details, plan_details, pay_type, transactions.id AS invoice_id, transactions.created_at AS created_at')
                        //->where( 'date', '>=', $today_date )
                        ->leftJoin('user_services', 'fk_user_service_id', '=', 'user_services.id')
                        ->leftJoin('users', 'user_services.user_id', '=', 'users.id')
                        ->leftJoin('user_client_meta', 'user_services.user_id', '=', 'user_client_meta.fk_user_id')
                        ->leftJoin('users as rm', 'user_client_meta.fk_relation_manager_id', '=', 'rm.id')
                        ->leftJoin('plans', 'user_services.plan_id', '=', 'plans.id')
                        ->whereBetween( 'date', [$fromDate, $tillDate] )
                        //->where( 'date', '<=', $tillDate )
                        ->whereNull('transactions.transaction_id')
                        ->orderBy('date', 'asc')
                        ->get();

        $lapsed_payments = DB::table('transactions')
            ->selectRaw('date(date) as date, transactions.amount as amount, transactions.pay_no, users.username, users.name, rm.name as rm_name, users.id as user_id, users.email, users.mobile, service_details, plan_details, pay_type, transactions.id AS invoice_id, transactions.created_at AS created_at')
            //->where( 'date', '>=', $today_date )
            ->leftJoin('user_services', 'fk_user_service_id', '=', 'user_services.id')
            ->leftJoin('users', 'user_services.user_id', '=', 'users.id')
            ->leftJoin('user_client_meta', 'user_services.user_id', '=', 'user_client_meta.fk_user_id')
            ->leftJoin('users as rm', 'user_client_meta.fk_relation_manager_id', '=', 'rm.id')
            ->leftJoin('plans', 'user_services.plan_id', '=', 'plans.id')
            ->where( 'date', '<', $fromDate )
            ->whereNull('transactions.transaction_id')
            ->orderBy('date', 'desc')
            ->get();

        $upcoming_payments_count = count($upcoming_payments);
        $graph_data_transaction = $this->getTransactionsGraphData();
        $graph_data_services = $this->getServicesGraphData();
        $graph_data_plans = $this->getPlansGraphData();
        $graph_data_service_status = $this->getServiceStatusData();

        $inactive_user = DB::table('users')->where('status', 0)->get();

        return view('admin.dashboard', compact(
            'upcoming_payments_count',
            'expiringServices',
            'renewals',
            'lapsed_renewals',
            'pending_verifications',
            'temp_ext',
            'statisctics_payments',
            'transactions',
            'graph_data_transaction',
            'graph_data_services',
            'graph_data_plans',
            'graph_data_service_status',
            'inactive_user',
            'upcoming_payments',
            'lapsed_payments'
        ));
    }

    private function getTransactionsGraphData()
    {
        $transaction_data = Transection::select('payable_amount','date')
                    ->where('cleared', 'yes')
                    ->orderBy('date', 'desc')
                    ->get()
                    ->groupBy(function($val) {
                        return Carbon::parse($val->date)->formatLocalized('%B %Y');
                    }
                );
        if(count($transaction_data) > 0){
            $transaction_data = $transaction_data->toArray();
            $graph_data = array();
            foreach ($transaction_data as $month => $transaction_month_data) {
                $amount = 0;
                foreach ($transaction_month_data as $transaction_data_value) {
                    $amount = $amount+$transaction_data_value['payable_amount'];
                }
                $graph_data[] = [$month, $amount];
            }
            $graph_data[] = ['Month', 'Earning'];
            $graph_data = array_reverse($graph_data);
            return json_encode($graph_data);
        } else {
            return array();
        }
    }

    private function getServicesGraphData()
    {
        $total_active_services = DB::table('user_services')->count();
        $active_services_data = DB::table('user_services')->select('service_details')
                    ->get()
                    ->groupBy(function($val) {
                        return json_decode($val->service_details, true)['name'];
                    }
                );
        
        if(count($active_services_data) > 0){
            $active_services_data = $active_services_data->toArray();
            $graph_data = array();
            $graph_data[] = ['Service', 'Usage(%)'];
            foreach ($active_services_data as $service_name => $per_service_data) {
                $graph_data[] = [$service_name, ((100*count($per_service_data))/$total_active_services)];
            }
            return json_encode($graph_data);
        } else {
            return array();
        }
    }

    private function getPlansGraphData()
    {
        $total_active_plans = DB::table('user_services')->count();
        $active_plans_data = DB::table('user_services')->select('plan_details')
                    ->get()
                    ->groupBy(function($val) {
                        return json_decode($val->plan_details, true)['name'];
                    }
                );
        
        if(count($active_plans_data) > 0){
            $active_plans_data = $active_plans_data->toArray();
            $graph_data = array();
            $graph_data[] = ['Plan', 'Usage(%)'];
            foreach ($active_plans_data as $plan_name => $per_plan_data) {
                $graph_data[] = [$plan_name, ((100*count($per_plan_data))/$total_active_plans)];
            }
            return json_encode($graph_data);
        } else {
            return array();
        }
    }

    private function getServiceStatusData()
    {
        $total_active_services = DB::table('user_services')->count();
        $active_service_status_data = DB::table('user_services')->select('service_status')
                    ->get()
                    ->groupBy('service_status');
        
        if(count($active_service_status_data) > 0){
            $active_service_status_data = $active_service_status_data->toArray();
            $graph_data = array();
            $graph_data[] = ['Service Status', 'Usage(%)'];
            foreach ($active_service_status_data as $service_name => $per_service_data) {
                $graph_data[] = [$service_name, ((100*count($per_service_data))/$total_active_services)];
            }
            return json_encode($graph_data);
        } else {
            return array();
        }
    }
}
