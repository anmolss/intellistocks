<?php

namespace App\Http\Controllers\Admin;

use App\ConfigType;
use App\Configuration;
use App\Plan;
use App\Service;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;
use Validator;
use App\Models\UserService;

class PlansController extends Controller
{
    protected $base_model;
    protected $view_path;
    protected $route_base;
    protected $route_base_prefix = 'admin::';
    protected $append_flash;
    protected $validation_rule;

    protected $taxes;

    public function __construct(Request $request)
    {
        $this->base_model = new Plan();
        $this->view_path = 'admin.plans.';
        $this->route_base = 'plans';
        $this->validation_rule = [
            'name' => 'required|max:255',
            'category' => 'required',

            'min_call' => 'required|numeric|less_than_field:max_call',
            'max_call' => 'required|numeric|greater_than_field:min_call',
            'bill_period' => 'required_if:category,profit_sharing_fixed,profit_sharing_slab|numeric',
            'annual_fee' => 'required_if:category,profit_sharing_fixed,profit_sharing_slab|numeric',

            'duration' => 'required_if:category,time_based|numeric',

            'service_fee' => 'required_if:category,time_based|numeric',

            'grace_period' => 'required|numeric|less_than_field:duration',
            'max_discount' => 'required|numeric|max:100',
            'nav_cal' => 'required|in:0,1',
            'status' => 'required|boolean',

            'plan_detail.*.from' => 'required_if:category,profit_sharing_slab|numeric',
            'plan_detail.*.to' => 'required_if:category,profit_sharing_slab|numeric',
            'plan_detail.*.percent' => 'required_if:category,profit_sharing_slab|numeric',
            'plan_detail.amount' => 'required_if:category,profit_sharing_fixed|numeric',
        ];
        $this->messages = [];
        $this->append_flash = 'Plan';
        $this->taxes = ConfigType::with('configs')->where('slug', 'additional-taxs')->first();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Service $service)
    {

        $list = $this->base_model->with('service')
            ->selectRaw('plans.*, count(user_id) as total_users')
            ->leftJoin('user_services','user_services.plan_id', '=', 'plans.id')
            ->where('plans.service_id', $service->id)
            ->groupBy('plans.id')
            ->orderBy('plans.id', 'Desc')
            ->paginate(15);

        return view($this->view_path . 'index', compact('list', 'service'));
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Service $service)
    {
        $taxes = $this->taxes->configs->toArray();
        if($service->payment_mode == 0){
            $allowed_durations = array_only(config('rothmans.service_duration'), $service->allowed_duration);
        } else {
            $allowed_durations = config('rothmans.service_duration');
        }

        $active_users = false;

        return view($this->view_path . 'create', compact('service', 'taxes', 'allowed_durations', 'active_users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Service $service)
    {
        if(!$request->has('duration')){
            $this->validation_rule['grace_period'] = 'required|numeric|less_than_field:bill_period';
        }
        $validator = Validator::make($request->all(), $this->validation_rule,$this->messages);
        if ($validator->fails()) {
            return redirect(route($this->route_base_prefix . $this->route_base.'.create', ['service'=> $service->id]))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $item = $this->base_model;
            $item->name = $request->get('name');
            $item->category = $request->get('category');
            $item->plan_detail = ($request->has('plan_detail')) ? $request->get('plan_detail') : null;
            $item->nav_cal = ($request->has('nav_cal')) ? $request->get('nav_cal') : 0;
            $item->status = ($request->has('status')) ? $request->get('status') : 0;

            $item->min_call = $request->min_call;
            $item->max_call = $request->max_call;
            $item->bill_period = ($request->bill_period)? $request->bill_period : 0;
            $item->grace_period = $request->grace_period;
            $item->annual_fee = ($request->annual_fee)? $request->annual_fee : 0;
            $item->duration = ($request->has('duration')) ? $request->get('duration') : 0;
            $item->service_fee = ($request->service_fee)? $request->service_fee : 0;
            $item->applicable_tax = $request->applicable_tax;
            $item->max_discount = $request->max_discount;

            $service = Service::find($request->get('service_id'));
            if($service){
                $item->service_id = $service->id;

                if(!$item->save())
                {
                    Flash::success('Error occurred! Please try again after some time ');
                    return redirect(route($this->route_base_prefix . $this->route_base . '.create', ['service'=> $service->id]))
                        ->withInput($request->all());
                }

            }
            else {
                Flash::success('Service Not available');
                return redirect(route($this->route_base_prefix . $this->route_base . '.create', ['service'=> $service->id]))
                    ->withInput($request->all());
            }


            Flash::success($this->append_flash . ' added successfully!');
            return redirect(route($this->route_base_prefix . $this->route_base.'.index', ['service'=> $service->id]));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPlanNew($id) {
        $plan = Plan::where('service_id','1')->pluck('name');
        return response()->json($plan);
    }

    public function getPlan($id)
    {
        $plan = Plan::findOrFail($id);
        $taxes = array_pluck($this->taxes->configs->toArray(), 'name', 'id');

        //get applicable tax
        $tax = 0;
        foreach ($plan->applicable_tax as $value) {
            $got_tax = DB::table('configurations')->select('value')->where('id',$value)->first();
            $tax = $tax+$got_tax->value;
        }

        $applicable_tax = '';
        foreach ($plan->applicable_tax as $value) {
            $applicable_tax .= ' '.$taxes[$value];
        }

        $payment_mode = ( isset($plan->service->payment_mode) && !empty($plan->service->payment_mode) ) ? $plan->service->payment_mode : 0;

        $max_days = 0;
        

        if($payment_mode == 1){
            $service_fee = $plan->annual_fee;
            $max_days = 365;
        } else {
            $service_fee = $plan->service_fee;
            if($plan->duration != 0){
                $max_days = round( (($plan->duration * 40) / 100), 0, PHP_ROUND_HALF_DOWN);    
            }
        }

        $html = 
            '<div class="col-sm-12" style="margin-top:10px;">
                <div class="table-responsive">
                    <table class="table table-xxs table-bordered">
                        <thead>
                            <tr>
                                <th colspan="10" class="text-center bg-primary">Fee Details</th>
                            </tr>
                            <tr>
                                <th>Plan</th>
                                <th>Minimum Call</th>
                                <th>Maximum Call</th>
                                <th>Bill Period</th>
                                <th>Grace Period</th>
                                <th>Applicable Tax</th>
                                <th id="h_special_discount">Special Discount</th>
                                <th>Service Fee</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>'.ucfirst(strtolower(str_replace('_', ' ', $plan->category))).'</td>
                                <td>'.$plan->min_call.'</td>
                                <td>'.$plan->max_call.'</td>
                                <td>'.$plan->bill_period.'</td>
                                <td>'.$plan->grace_period.'</td>
                                <td>'.$applicable_tax.'</td>
                                <td id="max_discount">'.$plan->max_discount .'%</td>
                                <td id="service_fee">'.$service_fee.'</td>
                            </tr>
                            <tr>
                                <td colspan="6" rowspan="4"></td>
                                <td class="text-bold">Net Service Amount</td>
                                <td><span id="net_service_amt">'.$service_fee.'</span></td>
                            </tr>
                            <tr>
                                <td class="text-bold">Discount Amount</td>
                                <td><span id="discount_amt">0</span></td>
                            </tr>
                            <tr>
                                <td class="text-bold" id="applied_tax_value">Applied Tax ('.$tax.'%)</td>
                                <td><span id="applied_tax">'.(($service_fee*$tax)/100).'</span></td>
                            </tr>
                            <tr>
                                <td class="text-bold">Net Payable Amount</td>
                                <td><span id="net_payable_amt">'.($service_fee+(($service_fee*$tax)/100)).'</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>';

        echo json_encode(array('status' => 'success', 'payment_mode' => $payment_mode, 'max_discount' => $plan->max_discount, 'max_days' => $max_days, 'service_fee' => $service_fee, 'applicable_tax' => $tax, 'html' => $html));
    }

    public function getMultiplePlans(Request $request)
    {
        $services1 = $request->all();
        foreach ($services1 as $services) {
            $plans[] = Plan::whereIn('service_id', $services)->get()->pluck('name', 'id');
        }
        return json_encode($plans);
        die();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service, Plan $plan)
    {
        $item = $plan;
        $taxes = $this->taxes->configs->toArray();

        if($service->payment_mode == 0){
            $allowed_durations = array_only(config('rothmans.service_duration'), $service->allowed_duration);
        } else {
            $allowed_durations = config('rothmans.service_duration');
        }

        $active_users = (UserService::where('plan_id', $plan->id)->where('service_status','Active')->count()) ? true : false;

        return view($this->view_path . 'edit', compact('item', 'service', 'taxes', 'allowed_durations', 'active_users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service, Plan $plan)
    {
        if(!$request->has('duration')){
            $this->validation_rule['grace_period'] = 'required|numeric|less_than_field:bill_period';
        }

        $validator = Validator::make($request->all(), $this->validation_rule);

        if ($validator->fails()) {
            return redirect(route($this->route_base_prefix . $this->route_base . '.edit', [$this->route_base => $plan->id, 'service'=> $service->id]))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $item = $this->base_model->find($plan->id);
            $item->name = $request->get('name');
            $item->category = $request->get('category');
            $item->plan_detail = ($request->has('plan_detail')) ? $request->get('plan_detail') : null;
            $item->nav_cal = ($request->has('nav_cal')) ? $request->get('nav_cal') : 0;
            $item->status = ($request->has('status')) ? $request->get('status') : 0;

            $item->min_call = $request->min_call;
            $item->max_call = $request->max_call;
            $item->bill_period = ($request->bill_period)? $request->bill_period : 0;
            $item->grace_period = $request->grace_period;
            $item->annual_fee = ($request->annual_fee)? $request->annual_fee : 0;
            $item->duration = ($request->has('duration')) ? $request->get('duration') : 0;
            $item->service_fee = ($request->service_fee)? $request->service_fee : 0;
            $item->applicable_tax = $request->applicable_tax;
            $item->max_discount = $request->max_discount;


            if($service->id == $request->get('service_id')){
                $item->service_id = $service->id;

                if(!$item->save())
                {
                    Flash::success('Error occurred! Please try again after some time ');
                    return redirect(route($this->route_base_prefix . $this->route_base . '.edit', [$this->route_base => $plan->id, 'service'=> $service->id]))
                        ->withInput($request->all());
                }

            }
            else {
                Flash::success('Service Not available');
                return redirect(route($this->route_base_prefix . $this->route_base . '.edit', [$this->route_base => $plan->id, 'service'=> $service->id]))
                    ->withInput($request->all());
            }

            Flash::success($this->append_flash . ' updated successfully!');
            return redirect(route($this->route_base_prefix . $this->route_base.'.index', ['service'=> $service->id]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service, Plan $plan)
    {

        if (!$plan->delete()) {
            Flash::success('Error occurred! Please try again after some time ');
        } else {
            Flash::success($this->append_flash . ' deleted successfully!');
        }

        return redirect(route($this->route_base_prefix . $this->route_base . ".index"));
    }


}
