<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Filters\TemplateFilters;
use App\Template;
use App\Service;
use App\Token;
use Illuminate\Http\Request;
use Auth;
use Flash;
use Validator;

class TemplatesController extends Controller
{
    protected $base_model;
    protected $view_path;
    protected $route_base;
    protected $route_base_prefix = 'admin::';
    protected $append_flash;
    protected $validation_rule;

    public function __construct()
    {
        $this->base_model = new Template();
        $this->view_path = 'admin.templates.';
        $this->route_base = 'templates';
        $this->validation_rule = [
            'name' => 'required|min:3',
            'body' => 'required',
            'status' => 'required',
        ];
        $this->append_flash = 'Templates';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TemplateFilters $filter)
    {
        $list = $this->base_model->filter($filter)->with('users')->orderBy('id', 'Desc')->paginate(15);

        if(\Request::ajax()){
           return $list->items();
        }

        return view($this->view_path . 'index', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tokens = Token::all('value','content','id');
        return view($this->view_path . 'create', compact('tokens'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation_rule);
        if ($validator->fails()) {
            if($request->ajax()){
                return $validator->errors();
            }
            return redirect(route($this->route_base_prefix . $this->route_base.'.create'))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $item = $this->base_model;
            $item->name = $request->get('name');
            $item->body = $request->get('body');
            $item->status = $request->get('status');
            $item->created_by = Auth::user()->id;
            if(! $item->save())
            {
                if($request->ajax()){
                    return false;
                }
                Flash::success('Error occurred! Please try again after some time ');
                return redirect(route($this->route_base_prefix . $this->route_base . '.create'))
                    ->withInput($request->all());
            }

            if($request->ajax()){
                return $item;
            }

            Flash::success($this->append_flash . ' added successfully!');
            return redirect(route($this->route_base_prefix . $this->route_base.'.index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //
    public function edit($id)
    {
        $item = $this->base_model->with('services')->findOrFail($id);
        $tokens = Token::all('value','content','id');

        return view($this->view_path . 'edit', compact('item','tokens'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->validation_rule);

        if ($validator->fails()) {
             return redirect(route($this->route_base_prefix . $this->route_base . '.edit', [$this->route_base => $id]))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $item = $this->base_model->findOrFail($id);
            $item->name = $request->get('name');
            $item->body = $request->get('body');
            $item->status = $request->get('status');
            $item->created_by = Auth::user()->id;
            if(! $item->save())
            {
                Flash::success('Error occurred! Please try again after some time ');
                return redirect(route($this->route_base_prefix . $this->route_base . '.create'))
                    ->withInput($request->all());
            }
            Flash::success($this->append_flash . ' updated successfully!');
            return redirect(route($this->route_base_prefix . $this->route_base.'.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->base_model->find($id);

        if(! $item->delete())
        {
            Flash::error('Error occurred! Please try again after some time ');
        }
        else
        {
            Flash::success( $this->append_flash . ' deleted successfully!');
        }

        return redirect(route($this->route_base_prefix . $this->route_base.".index"));
    }

}
