<?php

namespace App\Http\Controllers\Admin;

use App\ConfigType;
use App\Configuration;
use App\Plan;
use App\Service;
use App\Models\Transection;
use App\Models\Partialtransaction;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;
use Validator;

class TransactionsController extends Controller
{
	public function editTransaction(Request $request)
	{
		if(isset($request->all()['id']) && isset($request->all()['type'])){
			if($request->all()['type'] == 'full' && !empty($request->all()['id'])){
				$input = array(
					'cheque_no' => (isset($request->all()['cheque_no']))?$request->all()['cheque_no']:'',
		            'transaction_id' => (isset($request->all()['transaction_id']))?$request->all()['transaction_id']:'',
		            'cleared' => $request->all()['cleared'],
		        );
				$transaction = Transection::findOrFail($request->all()['id']);
				if($transaction->fill($input)->save()){
					$return = true;
				} else {
					$return = false;
				}
			} else if ($request->all()['type'] == 'partial' && !empty($request->all()['id'])) {
				$input = array(
					'cheque_no' => (isset($request->all()['cheque_no']))?$request->all()['cheque_no']:'',
		            'transaction_id' => (isset($request->all()['transaction_id']))?$request->all()['transaction_id']:'',
		            'cleared' => $request->all()['cleared'],
		        );
				$partial_transaction = Partialtransaction::findOrFail($request->all()['id']);
				if($partial_transaction->fill($input)->save()){
					$return = true;
				} else {
					$return = false;
				}
			}
			if($return){
				echo json_encode(['success'=>1]);	
			}
		}
	}

	public function getTransactionEditForm(Request $request)
	{
		if(!empty($request->all()['tran_type']) && !empty($request->all()['tran_pay_type']) && !empty($request->all()['tran_id'])){
			switch ($request->all()['tran_pay_type']) {
				case 'cash':
					$transaction = DB::table('transactions')->where('id',$request->all()['tran_id'])->first();
					$form = $this->makeForm('cash', $request->all()['tran_type'], $transaction);
					break;
				case 'cheque':
					$transaction = DB::table('transactions')->where('id',$request->all()['tran_id'])->first();
					$form = $this->makeForm('cheque', $request->all()['tran_type'], $transaction);
					break;
				case 'net_banking':
					$transaction = DB::table('transactions')->where('id',$request->all()['tran_id'])->first();
					$form = $this->makeForm('net_banking', $request->all()['tran_type'], $transaction);
					break;
				default:
					$form = flase;
					break;
			}
			if (!$form) {
				echo json_encode(['success'=>2]);
			} else {
				echo json_encode(['success'=>1,'form'=>$form]);
			}
		}
	}

	private function makeForm($type, $tran_type, $transaction){
		if($transaction->cleared == 'No'){
			$cleared = '<div class="col-md-6">
				            <label class="control-label">Cleared:</label>
				            <select class="form-control" name="cleared">
				              <option value="No" selected>No</option>
				              <option value="Yes">Yes</option>
				            </select>
				        </div>';
		} else if($transaction->cleared == 'Yes'){
			$cleared = '<div class="col-md-6">
				            <label class="control-label">Cleared:</label>
				            <select class="form-control" name="cleared">
				              <option value="No">No</option>
				              <option value="Yes" selected>Yes</option>
				            </select>
				        </div>';
		}
		switch ($type) {
			case 'cash':
				$return = 
					'<h5>Cash</h5>
				      <div class="form-group">
				        <div class="row">
				          <input type="hidden" name="id" value="'.$transaction->id.'">
				          <input type="hidden" name="type" value="'.$tran_type.'">
				          <div class="col-md-6">
				            <label for="transaction_id[]" class="control-label">Transaction Id:</label>
				            <input class="form-control" placeholder="Transaction Id" name="transaction_id" type="text" value="'.$transaction->transaction_id.'">
				            <span class="help-block error_transaction_id" style="display:none;"></span>
				          </div>
				          '.$cleared.'
				        </div>
				      </div>';
				break;
			
			case 'cheque':
				$return = 
					'<h5>Cheque</h5>
				      <div class="form-group">
				        <div class="row">
				          <input type="hidden" name="id" value="'.$transaction->id.'">
				          <input type="hidden" name="type" value="'.$tran_type.'">
				          <div class="col-sm-6">
				            <label for="cheque_number" class="control-label">Cheque Number:</label>
				            <input class="form-control" placeholder="Cheque Number" name="cheque_number" type="text" value="'.$transaction->cheque_number.'">
				          </div>

				          '.$cleared.'
				        </div>
				      </div>';
				break;
			
			case 'net_banking':
				$return = 
					'<h5>Net Banking</h5>
				      <div class="form-group">
				        <div class="row">
				          <input type="hidden" name="id" value="'.$transaction->id.'">
				          <input type="hidden" name="type" value="'.$tran_type.'">
				          <div class="col-md-6">
				            <label for="transaction_id[]" class="control-label">Transaction Id:</label>
				            <input class="form-control" placeholder="Transaction Id" name="transaction_id" type="text" value="'.$transaction->transaction_id.'">
				            <span class="help-block error_transaction_id" style="display:none;"></span>
				          </div>

				          '.$cleared.'
				        </div>
				      </div>
				    ';
				break;
			
			default:
				$return = '';
				break;
		}
		$modal = 
			'<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Edit Transaction</h5>
            </div>
            <div class="modal-body">
            	<form action="'.route('admin::editTransaction').'" class="transaction_edit_form">
					<fieldset>
					<div class="col-sm-12">
            			'.$return.'
            		</div>
				    </fieldset>
				</form>
            </div>
            <div class="modal-footer">
            	<div class="transaction_edit_form_spinner" style="display:none;">
            		<div class="col-sm-1 text-center">
	                    <i class="icon-spinner2 spinner"></i>
	                </div>
            	</div>
              <button type="submit" class="btn btn-primary save_transaction">Save</button>
              <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>';
		return $modal;
	}
}