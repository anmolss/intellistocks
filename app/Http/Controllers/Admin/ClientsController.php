<?php
namespace App\Http\Controllers\Admin;

use App\Notifications\SharePnl;
use App\Plan;
use App\Repositories\ServiceDelivery\ServiceDeliveryRepository;
use App\ServiceDeliveryUser;
use Illuminate\Http\Request;
use App\Models\Leads;
use App\Models\Client;
use App\Models\Logs;
use App\Service;
use App\Sources;
use App\Activity;
use Illuminate\Support\Str;
use App\User;
use App\Models\UserClientMeta;
use App\Models\UserService;
use Datatables;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SmsController;
use Auth;
use Carbon;
use DB;
use Mail;
use App\Role;
use Flash;
use Config;
use Validator;
use App\Clients;
use App\Http\Requests\Client\StoreClientRequest;
use App\Http\Requests\Client\StoreClientUserRequest;
use App\Http\Requests\Client\UpdateClientRequest;
use App\Http\Requests\Client\UpdateCustomerInfoRequest;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Client\ClientRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\SourcesController;
use Excel;


class ClientsController extends Controller
{

    protected $users;
    protected $clients;
    protected $base_model;
    protected $view_path;
    protected $route_base;
    protected $route_base_prefix = 'admin::';
    protected $append_flash;
    protected $validation_rule;
    protected $leads;
   
    public function __construct(
        UserRepositoryContract $users,
        ClientRepositoryContract $clients
    ) {

        $this->view_path = 'admin.clients.';
        $this->users = $users;
        $this->clients = $clients;

        $this->route_base = 'clients';

        $this->clients = $clients;
        $this->users = $users;
        
        $this->messages = [];
        $this->append_flash = 'Client';
    }

    public function index()
    {
        return view($this->view_path .'index');
    }

    public function indexCustomers()
    {
        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');
        $plans = Plan::orderBy('name', 'asc')->get()->pluck('name', 'id');

        // $clients = Client::orderBy('first_name', 'asc')->get();

        $clients = Client::all();

        return view($this->view_path .'index_customers')->withServices($services)->withPlans($plans)->withClients($clients);
    }

    public function customerData(Request $request)
    {
        $users = User::select([
                'users.id', 'name', 'username', 'email', 'mobile', 'service_id', 'plan_id', 'users.created_at',
                DB::raw('(select group_concat(activation_date) from user_services where user_id = users.id GROUP BY user_id) AS activation_dates'),
                DB::raw('(select group_concat(expire_date) from user_services where user_id = users.id GROUP BY user_id) AS expire_dates'),
                DB::raw('(select group_concat(created_at) from user_services where user_id = users.id GROUP BY user_id) AS created_dates'),
                DB::raw('(select group_concat(service_status) from user_services where user_id = users.id GROUP BY user_id) AS service_statuses')
            ])
            ->leftJoin('user_services', 'users.id', '=', 'user_services.user_id')
            ->where('user_type','Customer')
            ->groupBy('users.id')
            ->orderBy('users.id', 'desc')->get();

        return Datatables::of($users)
        ->addColumn('namelink', function ($users) {
                return '<a href="customers/'.$users->id.'" ">'.$users->name.'</a>';
        })
        ->addColumn('service_name', function ($users) use ($request) {
            $user_services = UserService::where('user_id', $users->id)->orderBy('id', 'desc')->where('service_status', '<>', 'Deleted')->get();
            $service_names = '';
            foreach ($user_services as $service) {
                if ($request->has('status')) {
                    if($service->service_status == $request->get('status')){
                        $service_names .= json_decode($service->service_details, true)['name'].' ('.$service->service_status.') ';
                        $service_names .= '<ul class="list-inline list-inline-condensed no-margin">
                            <li>
                                <span class="text-muted">'.Carbon::parse($service->activation_date)->format('d/m/Y').'</span>
                            </li>
                            <li>To</li>
                            <li>
                                <span class="text-muted">'.Carbon::parse($service->expire_date)->format('d/m/Y').'</span>
                            </li>
                        </ul> </br>';
                    }
                } else {
                    $service_names .= json_decode($service->service_details, true)['name'].' ('.$service->service_status.') ';
                    $service_names .= '<ul class="list-inline list-inline-condensed no-margin">
                        <li>
                            <span class="text-muted">'.Carbon::parse($service->activation_date)->format('d/m/Y').'</span>
                        </li>
                        <li>To</li>
                        <li>
                            <span class="text-muted">'.Carbon::parse($service->expire_date)->format('d/m/Y').'</span>
                        </li>
                    </ul> </br>';
                }
            }
            if (!empty($service_names)) {
                return $service_names;            
            } else {
                return NULL;
            }
        })
        ->add_column('actions', function ($users) {
                $action = '<ul class="icons-list">
                            <li><a href="customers/'.$users->id.'?tab=email" title="Mail"><i class="icon-mention"></i></a></li>
                            <li><a href="customers/'.$users->id.'?tab=sms" title="SMS"><i class="icon-envelop3"></i></a></li>';
                if($users->watchlist == 0){
                    $action .= '<li><a href="'.route('admin::clients.add_to_watch', $users->id).'" data_watch_status="1" title="Add to Watch List" class="text-success add_to_watch"><i class="icon-eye-plus"></i></a></li>';
                } else {
                    $action .= '<li><a href="'.route('admin::clients.add_to_watch', $users->id).'" data_watch_status="0" title="Remove From Watch List" class="text-danger add_to_watch"><i class="icon-eye-plus"></i></a></li>';
                }
                $action .= '</ul>';
                //$action .= '<li><a href="customers/destroy/'.$users->id.'" class="text-danger" title="Delete"><i class="icon-trash"></i></a></li></ul>';

                return $action;
        })
        ->filter(function ($instance) use ($request) {
            if ($request->has('search')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $return = false;
                    if (Str::contains(strtolower($row->name), strtolower($request->get('search')))) {
                        $return = true;
                    } else if (Str::contains(strtolower($row->email), strtolower($request->get('search')))) {
                        $return = true;
                    } else if (Str::contains(strtolower($row->username), strtolower($request->get('search')))) {
                        $return = true;
                    } else if (Str::contains(strtolower($row->mobile), strtolower($request->get('search')))) {
                        $return = true;
                    }
                    return $return;
                });
            }

            if ($request->has('service')) {
                $instance->collection = $instance->collection->filter(function ($row, $service) use ($request) {
                    $return = false;
                    foreach ($request->get('service') as $service) {
                        if($row->service_id == $service){
                            $return = true;
                        }
                    }
                    return $return;
                });
            }

            if ($request->has('plan')) {
                $instance->collection = $instance->collection->filter(function ($row, $plan) use ($request) {
                    $return = false;
                    foreach ($request->get('plan') as $plan) {
                        if($row->plan_id == $plan){
                            $return = true;
                        }
                    }
                    return $return;
                });
            }

            if ($request->has('status')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $statuses = explode(',', $row->service_statuses);
                    if(in_array($request->get('status'), $statuses)){
                        return true;
                    }
                });
            }

            if ($request->has('filter_range') && $request->has('select_filter')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $return = false;
                    $filter_dates = explode(',', $row->{$request->get('select_filter')});
                    $range_start = date('Y-m-d', strtotime(explode(" - ", $request->get('filter_range'), 2)[0]));
                    $range_end = date('Y-m-d', strtotime(explode(" - ", $request->get('filter_range'), 2)[1]));
                    if ($range_start == Carbon::now()->format('Y-m-d') && $range_end == Carbon::now()->format('Y-m-d')) {
                        return true;
                    }
                    foreach ($filter_dates as $filter_date) {
                        $filter_date = date('Y-m-d', strtotime($filter_date));
                        if(($filter_date > $range_start) && ($filter_date < $range_end)){
                            $return = true;
                        }
                    }
                    return $return;
                });
            }
        })
        ->make(true);
    }

    public function destroyCustomer($id)
    {
        $customer = User::findOrFail($id);
        echo '<h1>Under Development</h1>';
        dd($customer);
    }

    public function anyData()
    {
        $clients = Client::select(['id', 'name', 'company_name', 'email', 'primary_number']);
        return Datatables::of($clients)
        ->addColumn('namelink', function ($clients) {
            return '<a href="clients/'.$clients->id.'" ">'.$clients->name.'</a>';
        })
        ->add_column('actions', function ($clients) {
            $edit = '';
            $edit = '<a href="'. route('admin::clients.edit', $clients->id).' "  ><i class="glyphicon  glyphicon-pencil"></i></a>' ;
            $action = $edit.' <a href="'.route('admin::clients.destroy', $clients->id).'" class="btn-delete"><i class="glyphicon  glyphicon-remove"></i></a> ';
            return $action;
        })->make(true);
    }

    public function create()
    {
        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');

        return view($this->view_path .'create')
        ->withUsers($this->users->getAllUsersWithDepartments())
        ->withServices($services);
    }

    public function getRelationshipManager() {
        $abc = response()->json([
            'name' => 'dummy',
            'state' => 'India'
        ]);
        return $abc;
    }

    public function createDirect()
    {
        $services = Service::orderBy('name', 'asc')->where('status', 1)->get()->pluck('name', 'id');
        $plans = Plan::orderBy('name', 'asc')->where('status', 1)->get()->pluck('name', 'id');
        $sources = Sources::pluck('type');
        $service_plans = Service::with(
                array(
                'plans' => function($q) {
                    $q->where('status', 1);
                })
            )->where('status', 1)->get()->all();
        $service_plans = json_decode(json_encode($service_plans), true);
        // $hr_users = $this->users->getCustomerRelationshipManagers();
        $role = Role::where('display_name','Relationship Manager')->pluck('id');
        $role_user = DB::table('role_user')->where('role_id','14')->pluck('user_id');
        $hr_users = User::where('id', $role_user)->pluck('id', 'name');

        $cm_users = User::where('user_type', 'user')->pluck('id','name');
        foreach ($service_plans as $key => $service) {
            foreach ($service['plans'] as $key => $plan) {
                $final[$service['id']][] = array('id' => $plan['id'], 'name' => $plan['name']);
            }
        }
        if(isset($final) && count($final) > 0) {
            return view($this->view_path .'create_direct')
                ->withUsers($this->users->getAllUsersWithLeadsDepartments())
                ->withServices($services)
                ->withSources($sources)
                ->withPlans($plans)
                ->withServicePlans($final)
                ->withHrusers($hr_users)
                ->withCmusers($cm_users);
        } else {
            Flash::error('No Service Created.');
            return redirect('clients');
        }
    }

    public function addService($id='')
    {
        $services = Service::orderBy('name', 'asc')->where('status', 1)->get()->pluck('name', 'id');
        $plans = Plan::orderBy('name', 'asc')->where('status', 1)->get()->pluck('name', 'id');
        $service_plans = Service::with(
                array(
                'plans' => function($q) {
                    $q->where('status', 1);
                })
            )->where('status', 1)->get()->all();
        $service_plans = json_decode(json_encode($service_plans), true);
        foreach ($service_plans as $key => $service) {
            foreach ($service['plans'] as $key => $plan) {
                $final[$service['id']][] = array('id' => $plan['id'], 'name' => $plan['name']);
            }
        }

        $customer = User::findOrFail($id);
        return view($this->view_path .'add_service')
            ->withCustomer($customer)
            ->withServices($services)
            ->withPlans($plans)
            ->withServicePlans($final);
    }

    public function storeClientUser(StoreClientUserRequest $request)
    {
        $services = $this->buildServicesArray($request->all());

        $plan = Plan::findOrFail($services[1]['fk_plan_id']);
        $service = DB::table('services')->where('id', $plan->service_id)->get();
        $plan_detail = $plan->toArray();
        $service_detail = $service->toArray();

        $tax = 0;
        foreach ($plan_detail['applicable_tax'] as $value) {
            $got_tax = DB::table('configurations')->select('value')->where('id',$value)->first();
            $tax = $tax+$got_tax->value;
        }

        if($service_detail[0]->payment_mode == 1){
            $net_payable = $plan_detail['annual_fee'];
            /*
            $capital_fee = round($services[1]['service_capital'] / 100, 0, PHP_ROUND_HALF_UP);
            
            if($capital_fee > $net_payable){
                $net_payable = $capital_fee;
            }
            */
        } else {
            $net_payable = $plan_detail['service_fee'];
        }
        
        $net_payable = round($net_payable, 0, PHP_ROUND_HALF_UP);

        $discount = isset($services[1]['discount']) ? $services[1]['discount'] : 0;

        if($discount != 0 && $net_payable != 0){
            $net_payable = $net_payable - ($discount / 100) * $net_payable;
        }

        if($tax > 0){
            $net_payable = $net_payable+(($net_payable*$tax)/100);
        }

        $net_amount = 0;
        if($services[1]['payment_type'] == 'full'){
            $net_amount  = $services[1]['amount'];
        } else {
            foreach ($services[1]['transaction'] as $trans) {
                $net_amount += $trans['amount'];
            }    
        }

        $services[1]['service_details'] = $service_detail[0];
        $services[1]['plan_details'] = $plan_detail;

        $net_amount     = (float)round($net_amount, 2);
        $net_payable    = (float)round($net_payable, 2);

        if($net_amount != $net_payable){
            $str = 'Total amount must be equal to ' . $net_payable . ' and you entered only ' . $net_amount;
            echo json_encode(array('success' => 0, 'msg' => $str, 'alert_type' => 'swal'));
            die();    
        } else{
            $customer_id = $this->clients->createDirect($request->all(), $services);
            Flash::success('Customer added successfully!');
            echo json_encode(array('success' => 1, 'link' => route('admin::clients.customerShow', $customer_id)));
            die();
        }

    }

    public function updateClientUserInfo(UpdateCustomerInfoRequest $request)
    {
        $this->clients->updateCustomer($request->all());
        Flash::success('Customer added successfully!');
        echo json_encode(array('success' => 1));
        die();
    }

    public function store(StoreClientRequest $request)
    {
        
        $client = Client::create($request->all());
        $insertedId = $client->id;
        $insertedName = $client->name;
        $clients = Client::pluck('name', 'id');
        $clients_options = '<option value="'.$insertedId.'" selected>'.$insertedName.'</option>';
        foreach($clients as $key => $value){
            if($key != $insertedId){
              $clients_options .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        if($request->ajax()){
          echo json_encode(array('success' => 1, 'clients' => $clients_options));
          die();
        }
        return redirect('clients');
    }

    public function storeClientNew(StoreClientUserRequest $request) {
        
        $client_new = new Client();

        $name_array = explode(' ',$request->name);

        if (array_key_exists(0, $name_array)) {
            $first_name = $name_array[0];
        } else {
            $first_name = NULL;
        }

        if (array_key_exists(1, $name_array)) {
            $last_name = $name_array[1];
        } else {
            $last_name = NULL;
        }


        $client_new->first_name = $first_name;
        $client_new->last_name = $last_name;
        $client_new->email = $request->input('email');
        $client_new->primary_number = $request->input('mobile');
        $client_new->address = $request->input('address');
        $client_new->zipcode = $request->input('zipcode');
        $client_new->city = $request->input('city');
        $client_new->state = $request->input('state');
        $client_new->gender = $request->input('gender');
        $client_new->dob = $request->input('dob');
        $client_new->doa = $request->input('doa');
        $client_new->occupation = $request->input('occupation');
        $client_new->organisation = $request->input('organisation');
        $client_new->username = $request->input('username');
        $client_new->password = $request->input('password');
        $client_new->source = $request->input('source');
        $client_new->relationship_manager = $request->input('fk_relation_manager_id');
        $client_new->compliance_manager = $request->input('compliance_manager');
        $client_new->amount = $request->input('amount');
        $client_new->denomination = $request->input('denomination');
        $client_new->discount = $request->input('discount');
        $client_new->payment_type = $request->input('payment_type');
        $client_new->payment_mode = $request->input('payment_mode');
        $client_new->special_note = $request->input('special_note');
        $client_new->service_type = $request->input('services[0][fk_service_id]');
        $client_new->service_plan = $request->input('services[0][fk_plan_id]');
        $client_new->currency = $request->input('currency');

        $client_new->save();


        $rm_email = User::where('id', $request->fk_relation_manager_id)->first()->email;

        $message = "Dummy Message";
        $email = $request->email;

        Mail::raw($message, function($m) use ($email){
            $m->to($email);
            $m->subject('New User Created');
            $m->getSwiftMessage();
        });

        Mail::raw($message, function($m) use ($rm_email){
            $m->to($rm_email);
            $m->subject('New User Created');
            $m->getSwiftMessage();
        });


        return redirect('clients');
    }

    public function cvrapiStart(Request $vatRequest)
    {
        return redirect()->back()
        ->with('data', $this->clients->vat($vatRequest));
    }

    public function show($id)
    {
        return view($this->view_path .'show')
          ->withClient($this->clients->find($id));
    }

    public function showCustomer(Request $request, $id)
    {
        $users = User::orderBy('name', 'asc')->get()->pluck('name', 'id');
        $user = User::find($id);

        $relationship_manager = DB::table('user_client_meta')->where('fk_user_id', $user->id)->join('users', 'fk_relation_manager_id', '=', 'users.id')->select('users.*')->get()->first();
        $sv_officer = DB::table('user_client_meta')->where('fk_user_id', $user->id)->join('users', 'fk_suitability_officer_id', '=', 'users.id')->select('users.*')->get()->first();
        $user_client_meta = UserClientMeta::where('fk_user_id', $user->id)->first();
        $user_services = UserService::where('user_id', $user->id)->with(['transections', 'service', 'plan'])->orderBy('id', 'desc')->where('service_status', '<>', 'Deleted')->get();
        $service_deliveries = ServiceDeliveryUser::with(['user', 'plan', 'sd','service'])->where('user_id', $user->id)->orderBy('id', 'Desc')->paginate(15);

      /*
       * PNL
       * */
        $default_delivery_range = date('d/m/Y', strtotime('1 year ago')) . ' - ' . date('d/m/Y', strtotime('today'));
        if($request->has('delivery_range')) {
            $default_delivery_range = $request->get('delivery_range');
        }
        $delivery_range =  array_map(
            function($date) { return date('Y-m-d', strtotime(str_replace('/', '-', $date))); },
            explode(' - ', $default_delivery_range)
        );

        $default_service = [];
        if($request->has('service')) {
            $default_service = (array) $request->get('service');
        }

        $services = Service::whereIn('id', $user_services->pluck('service_id'))->orderBy('name', 'asc')->get()->pluck('name', 'id');
        $pnl_all = (new ServiceDeliveryRepository())->pNLSummery($services, $id);

        $pnl_services = (new ServiceDeliveryRepository())->deliveryReportbyService($default_service, $delivery_range, $services, $id);

        return view($this->view_path . 'show_customer', compact('services', 'pnl_services', 'default_delivery_range', 'default_service', 'pnl_all', 'id', 'sv_officer'))
        ->withClientMeta($user_client_meta)
        ->withUser($user)
        ->withRelationshipManager($relationship_manager)
        ->withUsers($users)
        ->withUserServices($user_services)
        ->withServiceDeliveries($service_deliveries);
    }

    public function downloadPnl(Request $request, $id){
        $default_delivery_range = date('d/m/Y', strtotime('1 year ago')) . ' - ' . date('d/m/Y', strtotime('today'));
        if($request->has('delivery_range')) {
            $default_delivery_range = $request->get('delivery_range');
        }
        $delivery_range =  array_map(
            function($date) { return date('Y-m-d', strtotime(str_replace('/', '-', $date))); },
            explode(' - ', $default_delivery_range)
        );

        $default_service = [];
        if($request->has('service')) {
            $default_service = (array) $request->get('service');
        }

        $pnl_stocks = (new ServiceDeliveryRepository())->deliveryReport($default_service, $delivery_range, 'stock', $id);
        if($request->has('symbols') && count($request->get('symbols')) > 0)
        {
            $pnl_stocks = collect($pnl_stocks)->whereIn('symbol', $request->get('symbols'))->all();
        }


        $data[] = [
            'Sr. No.',
            'Stock Name',
            'Status',
            'Entry date',
            'Entry Price',
            'CMP / EXIT PRICE',
            'Open Profit',
            'Booked Profit',
            'Total Profit',
            'Overall Profit (%)',
            'Open Position / Stock Weight of Total Investment',
            'Net ROI',
        ];
        $i = 1;
        foreach($pnl_stocks as $stock)
        {
            $data[] = [
                $i++,
                $stock['symbol'] ,
                $stock['status'] ,
                $stock['entry_date'] ,
                $stock['avg_price'] ,
                $stock['exit_price'] ,
                $stock['open_profit'] ,
                $stock['booked_profit'] ,
                $stock['total_profit'] ,
                $stock['profit_percent'] ."%",
                $stock['open_stock_weight'] ."%",
                $stock['net_roi'] ."%",
            ];

        }

        return Excel::create('pnl_service_stocks_'.time(), function($excel) use($data)  {
            $excel->sheet('Stock', function($sheet) use($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->download('csv');
    }

    public function sharePnl(Request $request, $id){

        $user = User::find($id);

        $default_delivery_range = date('d/m/Y', strtotime('1 year ago')) . ' - ' . date('d/m/Y', strtotime('today'));
        if($request->has('delivery_range')) {
            $default_delivery_range = $request->get('delivery_range');
        }
        $delivery_range =  array_map(
            function($date) { return date('Y-m-d', strtotime(str_replace('/', '-', $date))); },
            explode(' - ', $default_delivery_range)
        );

        $default_service = [];
        if($request->has('service')) {
            $default_service = (array) $request->get('service');
        }

        $pnl_stocks = (new ServiceDeliveryRepository())->deliveryReport($default_service, $delivery_range, 'stock', $id);
        if($request->has('symbols') && count($request->get('symbols')) > 0)
        {
            $pnl_stocks = collect($pnl_stocks)->whereIn('symbol', $request->get('symbols'))->all();
        }

        $data[] = [
            'Sr. No.',
            'Stock Name',
            'Status',
            'Entry date',
            'Entry Price',
            'CMP / EXIT PRICE',
            'Open Profit',
            'Booked Profit',
            'Total Profit',
            'Overall Profit (%)',
            'Open Position / Stock Weight of Total Investment',
            'Net ROI',
        ];
        $i = 1;
        foreach($pnl_stocks as $stock)
        {
            $data[] = [
                $i++,
                $stock['symbol'] ,
                $stock['status'] ,
                $stock['entry_date'] ,
                $stock['avg_price'] ,
                $stock['exit_price'] ,
                $stock['open_profit'] ,
                $stock['booked_profit'] ,
                $stock['total_profit'] ,
                $stock['profit_percent'] ."%",
                $stock['open_stock_weight'] ."%",
                $stock['net_roi'] ."%",
            ];

        }
        $excel = Excel::create('pnl_service_stocks_'.time(), function($excel) use($data)  {
            $excel->sheet('Stock', function($sheet) use($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
            });
        });

        return $user->notify(new SharePnl($user, $request->get('message'), $excel));

    }

    public function profitAndLossStock(Request $request, $id)
    {
        $default_delivery_range = date('d/m/Y', strtotime('1 year ago')) . ' - ' . date('d/m/Y', strtotime('today'));
        if($request->has('delivery_range')) {
            $default_delivery_range = $request->get('delivery_range');
        }
        $delivery_range =  array_map(
            function($date) { return date('Y-m-d', strtotime(str_replace('/', '-', $date))); },
            explode(' - ', $default_delivery_range)
        );

        $default_service = [];
        if($request->has('service')) {
            $default_service = (array) $request->get('service');
        }

        $default_calltype= 'all';
        if($request->has('call_type')) {
            $default_calltype = $request->get('call_type');
        }

        $service_ids = User::select([ 'user_services.service_id'])
            ->where('users.id',auth()->user()->id)
            ->leftJoin('user_services', 'users.id', '=', 'user_services.user_id')
            ->orderBy('users.id', 'desc')->get()->pluck('service_id');

        $services = Service::whereIn('id', $service_ids)->orderBy('name', 'asc')->get()->pluck('name', 'id');

        $pnl_stocks = (new ServiceDeliveryRepository())->deliveryReport($default_service, $delivery_range, 'stock', $id, $default_calltype);

        return view($this->view_path . 'tabs.p_l_s_class_tab', compact( 'pnl_stocks'));
    }

    public function edit($id)
    {
        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');
        return view($this->view_path .'edit')
        ->withClient($this->clients->find($id))
        ->withServices($services); 
    }

    public function update($id, UpdateClientRequest $request)
    {
        $this->clients->update($id, $request);
        Session()->flash('flash_message', 'Client successfully updated');
          return redirect('clients');
    }

    public function destroy($id)
    {
        $leads = DB::table('leads')->where('fk_client_id', $id)->get();
        foreach ($leads as $lead) {
            DB::table('notes')->where('fk_lead_id', $lead->id)->delete();
            DB::table('activity_log')->where('type', 'lead')->where('type_id', $lead->id)->delete();    
        }
        DB::table('leads')->where('fk_client_id', $id)->delete();
        $this->clients->destroy($id);
        return redirect('clients');
    }

    private function buildServicesArray($request='')
    {
        $services = array();

        $count = 0;

        $service = $request['services'][0];

        if($service['payment_type'] == 'full'){
            if( $service['fk_payment_type'] == 'cash' ){
                if(!empty($service['transaction'][0]['amount']) || !empty($service['transaction'][0]['date'])) {
                    $tmp = array();
                    $tmp['transaction_history_id']  = (isset($service['transaction_history_id'])) ? $service['transaction_history_id'] : '';
                    $tmp['user_service_id']  = (isset($service['user_service_id'])) ? $service['user_service_id'] : '';
                    $tmp['fk_service_id']    = $service['fk_service_id'];
                    $tmp['fk_plan_id']       = $service['fk_plan_id'];
                    $tmp['service_capital']  = (isset($service['capital'])) ? $service['capital'] : 0;
                    $tmp['fk_payment_type']  = $service['fk_payment_type'];
                    $tmp['payment_type']     = $service['payment_type'];
                    $tmp['activation_date']  = date('Y-m-d', strtotime($service['activation_date']));
                    $tmp['force_verify']     = (isset($service['force_verify'])) ? $service['force_verify'] : '';
                    $tmp['comments']         = (isset($service['comments'])) ? $service['comments'] : '';
                    $tmp['force_verify_reason'] = (isset($service['force_verify_reason'])) ? $service['force_verify_reason'] : '';
                    $tmp['force_verify_comment'] = (isset($service['force_verify_comment'])) ? $service['force_verify_comment'] : '';
                    $tmp['id']               = (isset($service['transaction'][0]['id'])) ? $service['transaction'][0]['id'] : '0';
                    $tmp['transaction_id']   = $service['transaction'][0]['transaction_id'];
                    $tmp['received_from']    = $service['transaction'][0]['received_from'];
                    $tmp['amount']           = $service['transaction'][0]['amount'];
                    if(!empty($service['transaction'][0]['transaction_id'])){
                        $tmp['cleared']      = 'Yes';
                    } else {
                        $tmp['cleared']      = 'No';
                    }
                    $tmp['date']             = date('Y-m-d', strtotime($service['transaction'][0]['date']));
                    $tmp['discount']         = (isset($service['discount'])) ? $service['discount'] : 0;

                    $services[++$count] = $tmp;
                }
            }

            if( $service['fk_payment_type'] == 'cheque' ){
                if(!empty($service['transaction'][0]['amount']) || !empty($service['transaction'][0]['date'])) {

                    $tmp = array();
                    $tmp['transaction_history_id']  = (isset($service['transaction_history_id'])) ? $service['transaction_history_id'] : '';
                    $tmp['user_service_id']     = (isset($service['user_service_id'])) ? $service['user_service_id'] : '';
                    $tmp['fk_service_id']       = $service['fk_service_id'];
                    $tmp['fk_plan_id']          = $service['fk_plan_id'];
                    $tmp['service_capital']     = (isset($service['capital'])) ? $service['capital'] : 0;
                    $tmp['fk_payment_type']     = $service['fk_payment_type'];
                    $tmp['payment_type']        = $service['payment_type'];
                    $tmp['activation_date']     = date('Y-m-d', strtotime($service['activation_date']));
                    $tmp['force_verify']        = (isset($service['force_verify'])) ? $service['force_verify'] : '';
                    $tmp['comments']            = (isset($service['comments'])) ? $service['comments'] : '';
                    $tmp['force_verify_reason'] = (isset($service['force_verify_reason'])) ? $service['force_verify_reason'] : '';
                    $tmp['force_verify_comment'] = (isset($service['force_verify_comment'])) ? $service['force_verify_comment'] : '';
                    $tmp['id']                  = (isset($service['transaction'][0]['id'])) ? $service['transaction'][0]['id'] : '0';
                    $tmp['cheque_number']       = $service['transaction'][0]['cheque_number'];
                    $tmp['bank']                = $service['transaction'][0]['bank'];
                    $tmp['amount']              = $service['transaction'][0]['amount'];
                    $tmp['cleared']             = (isset($service['transaction'][0]['cleared']))?$service['transaction'][0]['cleared']:0;
                    $tmp['date']                = date('Y-m-d', strtotime($service['transaction'][0]['date']));    
                    $tmp['discount']            = (isset($service['discount'])) ? $service['discount'] : 0;

                    $services[++$count] = $tmp;
                }
            }

            if( $service['fk_payment_type'] == 'net_banking' ){
                if(!empty($service['transaction'][0]['amount']) || !empty($service['transaction'][0]['date'])) {

                    $tmp = array();
                    $tmp['transaction_history_id']  = (isset($service['transaction_history_id'])) ? $service['transaction_history_id'] : '';
                    $tmp['user_service_id']     = (isset($service['user_service_id'])) ? $service['user_service_id'] : '';
                    $tmp['fk_service_id']       = $service['fk_service_id'];
                    $tmp['fk_plan_id']          = $service['fk_plan_id'];
                    $tmp['service_capital']     = (isset($service['capital'])) ? $service['capital'] : 0;
                    $tmp['fk_payment_type']     = $service['fk_payment_type'];
                    $tmp['payment_type']        = $service['payment_type'];
                    $tmp['activation_date']     = date('Y-m-d', strtotime($service['activation_date']));
                    $tmp['force_verify']        = (isset($service['force_verify'])) ? $service['force_verify'] : '';
                    $tmp['comments']            = (isset($service['comments'])) ? $service['comments'] : '';
                    $tmp['force_verify_reason'] = (isset($service['force_verify_reason'])) ? $service['force_verify_reason'] : '';
                    $tmp['force_verify_comment'] = (isset($service['force_verify_comment'])) ? $service['force_verify_comment'] : '';
                    $tmp['id']                  = (isset($service['transaction'][0]['id'])) ? $service['transaction'][0]['id'] : '0';
                    $tmp['type']                = $service['transaction'][0]['type'];
                    $tmp['transaction_id']      = $service['transaction'][0]['transaction_id'];
                    $tmp['received_from']       = $service['transaction'][0]['received_from'];
                    $tmp['amount']              = $service['transaction'][0]['amount'];
                    if(!empty($service['transaction'][0]['transaction_id'])){
                        $tmp['cleared']      = 'Yes';
                    } else {
                        $tmp['cleared']      = 'No';
                    }
                    $tmp['date']                = date('Y-m-d', strtotime($service['transaction'][0]['date']));
                    $tmp['discount']            = (isset($service['discount'])) ? $service['discount'] : 0;

                    $services[++$count] = $tmp;
                }
            }
        } else {
            foreach ($request['services'] as $key => $service) {
                $tmp = array();
                $tmp['transaction_history_id']  = (isset($service['transaction_history_id'])) ? $service['transaction_history_id'] : '';
                $tmp['user_service_id']      = (isset($service['user_service_id'])) ? $service['user_service_id'] : '';
                $tmp['fk_service_id']        = $service['fk_service_id'];
                $tmp['fk_plan_id']           = $service['fk_plan_id'];
                $tmp['service_capital']      = (isset($service['capital'])) ? $service['capital'] : 0;
                $tmp['fk_payment_type']      = 'other';
                $tmp['payment_type']         = $service['payment_type'];
                $tmp['activation_date']      = date('Y-m-d', strtotime($service['activation_date']));
                $tmp['force_verify']         = (isset($service['force_verify'])) ? $service['force_verify'] : '';
                $tmp['comments']             = (isset($service['comments'])) ? $service['comments'] : '';
                $tmp['force_verify_reason']  = (isset($service['force_verify_reason'])) ? $service['force_verify_reason'] : '';
                $tmp['force_verify_comment'] = (isset($service['force_verify_comment'])) ? $service['force_verify_comment'] : '';
                $tmp['discount']             = (isset($service['discount'])) ? $service['discount'] : 0;

                foreach ($service['transaction'] as $pay_no => $transaction) {
                    if(!empty($transaction['amount']) || !empty($transaction['date'])) {
                        $tmp['transaction'][$pay_no]['id']                  = (isset($transaction['id'])) ? $transaction['id'] : '0';
                        $tmp['transaction'][$pay_no]['payment_type']        = $transaction['fk_payment_type'];
                        $tmp['transaction'][$pay_no]['amount']              = $transaction['amount'];
                        $tmp['transaction'][$pay_no]['date']                = date('Y-m-d', strtotime($transaction['date']));

                        if( $transaction['fk_payment_type'] == 'cash' ){
                            $tmp['transaction'][$pay_no]['transaction_id']  = (!empty($transaction['transaction_id'])) ? $transaction['transaction_id'] : '';
                            $tmp['transaction'][$pay_no]['received_from']   = (!empty($transaction['received_from'])) ? $transaction['received_from'] : '';
                            if(!empty($transaction['transaction_id'])){
                                $tmp['transaction'][$pay_no]['cleared'] = 'Yes';
                            } else {
                                $tmp['transaction'][$pay_no]['cleared'] = 'No';
                            }
                        }

                        if( $transaction['fk_payment_type'] == 'net_banking' ){
                            $tmp['transaction'][$pay_no]['type']           = (!empty($transaction['type'])) ? $transaction['type'] : '';
                            $tmp['transaction'][$pay_no]['transaction_id'] = (!empty($transaction['transaction_id'])) ? $transaction['transaction_id'] : '';
                            $tmp['transaction'][$pay_no]['received_from']  = (!empty($transaction['received_from'])) ? $transaction['received_from'] : '';
                            if(!empty($transaction['transaction_id'])){
                                $tmp['transaction'][$pay_no]['cleared'] = 'Yes';
                            } else {
                                $tmp['transaction'][$pay_no]['cleared'] = 'No';
                            }
                        }

                        if( $transaction['fk_payment_type'] == 'cheque' ){
                            $tmp['transaction'][$pay_no]['cheque_number']  = (!empty($transaction['cheque_number'])) ? $transaction['cheque_number'] : '';
                            $tmp['transaction'][$pay_no]['bank']    = (!empty($transaction['bank'])) ? $transaction['bank'] : '';
                            $tmp['transaction'][$pay_no]['cleared'] = isset($transaction['cleared']) ? $transaction['cleared'] : 'No';
                        }
                    }
                }

                $services[++$count] = $tmp;
            }
        }
        return $services;
    }

    public function sendClientSms(Request $request)
    {
        $response = SmsController::sendSMS($request->get('message'), $request->get('number'));
        $response = json_decode($response, true);
        if($response['type'] == 'success'){
            return json_encode(['success' => 1]);
        } else {
            return json_encode(['success' => 2]);
        }
    }

    public function sendClientEmail(Request $request)
    {
        $to['user_email'] = $request->get('email');
        $to['user_name'] = $request->get('name');
        $to['subject'] = $request->get('subject');
        (new EmailController)->sendMail('email_default', ['user_by' => Auth::user()->name, 'username' => $request->get('name'), 'message_text' => $request->get('message')], $to);
        return json_encode(['success' => 1]);
    }

    public function addToWatchList($id, Request $request)
    {
        $user = User::findOrFail($id);
        if($request->get('watch_status') == 1){
            $input = [
                'watchlist' => 1,
            ];
            $msg = 'added to watch list';
        } else {
            $input = [
                'watchlist' => 0,
            ];
            $msg = 'removed from watch list';
        }
        
        if ($user->fill($input)->save()) {
            $data = ['response' => 1, 'msg' => $msg];
        } else {
            $data = ['response' => 2];
        }
        return response()->json($data);
    }

    public function getWatchlistUser(){

        $watchlist_users = User::selectRaw('users.id ,users.id AS user_id, users.name AS user_name, users.email, users.mobile, user_services.service_details, user_services.plan_details, user_services.activation_date, user_services.service_capital')
            ->leftJoin('user_services', 'user_services.user_id', '=', 'users.id')
            ->leftJoin('plans', 'user_services.plan_id', '=', 'plans.id')
            ->leftJoin('watchlists', 'watchlists.watchable_id', '=', 'users.id')
            ->whereNotNull('user_services.plan_id')
            ->where('user_services.service_status', '=', "Active")
            ->where('watchlists.watchable_type', '=', "App\\User")
            ->where('watchlists.watched_by', '=', auth()->user()->id)
            ->get();

        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');
        foreach($watchlist_users as $key => $user) {
            $watchlist_users[$key]->pnl = (new ServiceDeliveryRepository())->pNLSummery($services,  $user->user_id);
            $activation_date = Carbon::parse($user->activation_date)->addDays(json_decode($user->plan_details)->duration);
            $now = Carbon::now();
            $watchlist_users[$key]->days_left = ($activation_date->diff($now)->days < 1) ? 'today' : $activation_date->diffForHumans($now);

            $watchlist_users[$key]->expiry_date = Carbon::parse($user->activation_date)->addDays(json_decode($user->plan_details)->duration)->format('d-m-Y');
            $watchlist_users[$key]->service_details = json_decode($user->service_details)->name;
            $watchlist_users[$key]->plan_details = json_decode($user->plan_details)->name;
        }

        return $watchlist_users;
    }


    public function getPendingServiceActivation(Request $request) {

        return  DB::table('user_services')
            ->selectRaw('users.id AS user_id, users.name AS user_name, users.mobile, rm.name as rm_name, 
            user_services.id as us_id, user_services.service_details, user_services.activation_date, 
            if(user_services.payment_received is null, 0, 1) as payment_received,
            if(user_services.portfolio_review is null, 0, 1) as portfolio_review,
            if(user_services.on_boarding is null, 0, 1) as on_boarding,
            if(user_services.portfolio_shared is null, 0, 1) as portfolio_shared')

            ->leftJoin('users', 'user_services.user_id', '=', 'users.id')
            ->leftJoin('user_client_meta', 'user_services.user_id', '=', 'user_client_meta.fk_user_id')
            ->leftJoin('users as rm', 'user_client_meta.fk_relation_manager_id', '=', 'rm.id')
            ->leftJoin('plans', 'user_services.plan_id', '=', 'plans.id')
            ->orWhereNull('user_services.payment_received')
            ->orWhereNull('user_services.portfolio_review')
            ->orWhereNull('user_services.on_boarding')
            ->orWhereNull('user_services.portfolio_shared')
            ->get();
    }

    public function updatePendingServiceActivation(Request $request) {

        $user_service = UserService::with('user')
            ->where('id', $request->get('data_id'))
            ->first();
        $input = [
            $request->get('key') => Carbon::now(),
        ];
        $on_boarding = $user_service->on_boarding;
        if($request->get('key') == 'on_boarding'){
            $on_boarding = Carbon::now();
            if(Carbon::now() > Carbon::parse($user_service->activation_date) && $on_boarding != null && $user_service->payment_received != null
                && $user_service->user->transcript_status == 'Yes' && $user_service->user->kyc_status == 'Yes' && $user_service->user->consent_status == 'Yes'){
                $input['service_status'] = 'Active';
            } else {
                $input['service_status'] = 'Pending';
            }
        }

        $data = UserService::where('id' , $request->get('data_id'))
                ->update($input);
        if($data) {
            return ['response' => 1, 'post' =>$data];
        } else {
            return ['response' => 0, 'post' => $data];
        }
    }
}
