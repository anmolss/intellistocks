<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Leads;
use App\Models\Client;
use App\Models\Logs;
use App\Activity;
use App\Service;
use App\Plan;
use App\Models\Note;
use Illuminate\Support\Str;
use App\User;
use Datatables;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use Auth;
use App\Role;
use Carbon;
use DB;
use Flash;
use Config;
use Validator;
use App\Http\Requests\Lead\StoreLeadRequest;
use App\Http\Requests\Lead\UpdateLeadFollowUpRequest;
use App\Repositories\Lead\LeadRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Client\ClientRepositoryContract;
use App\Http\Requests\Lead\UpdateLeadRequest;
use App\Http\Requests\Lead\LeadCompleteRequest;

class LeadsController extends Controller
{
    protected $base_model;
    protected $view_path;
    protected $route_base;
    protected $route_base_prefix = 'admin::';
    protected $append_flash;
    protected $validation_rule;
    protected $leads;
    protected $users;
    protected $clients;
    protected $roles;
       

        public function __construct(
            LeadRepositoryContract $leads,
            UserRepositoryContract $users,
            ClientRepositoryContract $clients
            
        ) {
           $this->base_model = new Leads();
            $this->view_path = 'admin.leads.';
            $this->route_base = 'leads';
            
            
            $this->clients = $clients;
            $this->users = $users;
            $this->leads = $leads;
         
            $this->validation_rule = [
            'title' => 'required|max:255',
            'fee_type' => 'required',
            'detail.amount' => 'sometimes|required|numeric',
            'detail.type' => 'sometimes|required|in:percent,amount',
            'detail.*.from' => 'sometimes|required|numeric',
            'detail.*.to' => 'sometimes|required|numeric',
            'detail.*.percent' => 'sometimes|required|numeric',
            'nav_cal' => 'required|in:0,1',
            'billing_type' => 'required|in:0,1',
            'status' => 'required|boolean',
        ];
        $this->messages = [];
        $this->append_flash = 'Lead';
        }

    public function index()
    {
        $users = User::pluck('name', 'id');
        return view($this->view_path . 'index')
            ->withAssignUsers($this->users->getAllUsersWithLeadsDepartments())
            ->withUsers($users);
    }

    public function anyData($type = '', Request $request)
    {
        if (Auth::user()->hasRole('super-admin')) {
            if ($type == 'open_pass') {
                $leads = Leads::select(
                    ['id', 'title', 'fk_user_id_created', 'fk_client_id', 'fk_user_id_assign', 'created_at', 'close_date', 'contact_date', 'open_date', 'status', 'source', 'priority', 'lead_type']
                )->whereDate('open_date', '<=', date('Y-m-d H:i:s'))->where('status', '0')->get();
            } else {
                $leads = Leads::select(
                    ['id', 'title', 'fk_user_id_created', 'fk_client_id', 'fk_user_id_assign', 'created_at', 'close_date', 'contact_date', 'open_date', 'status', 'source', 'priority', 'lead_type']
                )->get();
            }
        } else {
            if ($type == 'open_pass') {
                $leads = Leads::select(
                    ['id', 'title', 'fk_user_id_created', 'fk_client_id', 'fk_user_id_assign', 'created_at', 'close_date', 'contact_date', 'open_
                    date', 'status', 'source', 'priority', 'lead_type']
                )->where('fk_user_id_assign', Auth()->id())->orWhere('fk_user_id_created', Auth()->id())->whereDate('open_date', '<=', date('Y-m-d H:i:s'))->where('status', '0')->get();
            } else {
                $leads = Leads::select(
                    ['id', 'title', 'fk_user_id_created', 'fk_client_id', 'fk_user_id_assign', 'created_at', 'close_date', 'contact_date', 'open_date', 'status', 'source', 'priority', 'lead_type']
                )->where('fk_user_id_assign', Auth()->id())->orWhere('fk_user_id_created', Auth()->id())->get();
            }
            
        }

        return Datatables::of($leads)
        ->addColumn('titlelink', function ($leads) {
                return '<a href="leads/'.$leads->id.'" ">'.$leads->title.'</a>';
        })
        ->editColumn('fk_user_id_created', function ($leads) {
                return $leads->createdBy->name;
        })
         ->editColumn('created_at', function ($leads) {
                 return $leads->created_at ? with(new Carbon($leads->created_at))
                ->format('d/m/Y') : '';
        })
         ->editColumn('close_date', function ($leads) {
                return $leads->close_date ? with(new Carbon($leads->close_date))
                ->format('d/m/Y') : '';
        })
        ->editColumn('contact_date', function ($leads) {
                return $leads->contact_date ? with(new Carbon($leads->contact_date))
                ->format('d/m/Y') : '';
        })
        ->editColumn('open_date', function ($leads) {
                if ($leads->open_date < date('Y-m-d', time())) {
                    $open_date = '<span class="label label-danger">'.with(new Carbon($leads->open_date))
                ->format('d/m/Y').'</span>';
                } else {
                    $open_date = $leads->contact_date ? with(new Carbon($leads->open_date))
                ->format('d/m/Y') : '';
                }
                return $open_date;
        })
        ->editColumn('status', function ($leads) {
                if ($leads->status == '0') {
                    $status = '<span class="label label-warning">'.Config::get('rothmans.lead_status')[$leads->status].'</span>';
                } else if ($leads->status == '1') {
                    $status = '<span class="label label-info">'.Config::get('rothmans.lead_status')[$leads->status].'</span>';
                } else if ($leads->status == '2') {
                    $status = '<span class="label label-success">'.Config::get('rothmans.lead_status')[$leads->status].'</span>';
                } else if ($leads->status == '3') {
                    $status = '<span class="label label-primary">'.Config::get('rothmans.lead_status')[$leads->status].'</span>';
                } else if ($leads->status == '4') {
                    $status = '<span class="label label-danger">'.Config::get('rothmans.lead_status')[$leads->status].'</span>';
                } else {
                    $status = '';
                }

                return $status;
        })
        ->editColumn('source', function ($leads) {
                return Config::get('rothmans.lead_source')[$leads->source];
        })
        ->editColumn('priority', function ($leads) {
                if ($leads->priority == '0') {
                    $priority = '<span class="label label-warning">'.Config::get('rothmans.lead_priority')[$leads->priority].'</span>';
                } else if ($leads->priority == '1') {
                    $priority = '<span class="label label-info">'.Config::get('rothmans.lead_priority')[$leads->priority].'</span>';
                } else if ($leads->priority == '2') {
                    $priority = '<span class="label label-success">'.Config::get('rothmans.lead_priority')[$leads->priority].'</span>';
                }
                return $priority;
        })
        ->editColumn('fk_user_id_assign', function ($leads) {
                return $leads->assignee->name;
        })
         ->add_column('actions', function ($leads) {
                $action = '
                <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="icon-gear"></i>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                ';
                $edit = '';
                if($leads->status == 0){
                    $edit = '<li><a href="'.route('admin::leads.edit', $leads->id).'"><i class="icon-pencil7"></i> Edit</a></li>';
                }
                $view = '<li><a href="'.route('admin::leads.show', $leads->id).'">View <i class="icon-eye"></i></a></li>';
                $delete = '<li><a href="'.route('admin::leads.destroy', $leads->id).'" class="text-danger-700 btn-delete">Delete <i class="icon-trash"></i></a></li>';
                $action .= $edit.$view.'<li class="divider"></li>'.$delete;
                $action .= '
                        </ul>
                    </li>
                </ul>
                ';
                return $action;
        })
        ->filter(function ($instance) use ($request) {
            if ($request->has('search')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['title'], $request->get('search')) ? true : false;
                });
            }

            if ($request->has('status')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row['status'] == $request->get('status')) ? true : false;
                });
            }

            if ($request->has('priority')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row['priority'] == $request->get('priority')) ? true : false;
                });
            }

            if ($request->has('source')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row['source'] == $request->get('source')) ? true : false;
                });
            }

            if ($request->has('created_by')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row['fk_user_id_created'] == $request->get('created_by')) ? true : false;
                });
            }

            if ($request->has('assigned_to')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return ($row['fk_user_id_assign'] == $request->get('assigned_to')) ? true : false;
                });
            }

            if ($request->has('created_range')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $created_at = date('Y-m-d', strtotime($row['created_at']));
                    $created_start = date('Y-m-d', strtotime(explode(" - ", $request->get('created_range'), 2)[0]));
                    $created_end = date('Y-m-d', strtotime(explode(" - ", $request->get('created_range'), 2)[1]));
                    if ($created_start == Carbon::now()->format('Y-m-d') && $created_end == Carbon::now()->format('Y-m-d')) {
                        return true;
                    } else {
                        return (($created_at > $created_start) && ($created_at < $created_end)) ? true : false;
                    }
                });
            }

            if ($request->has('close_range')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $close_at = date('Y-m-d', strtotime($row['close_date']));
                    $close_start = date('Y-m-d', strtotime(explode(" - ", $request->get('close_range'), 2)[0]));
                    $close_end = date('Y-m-d', strtotime(explode(" - ", $request->get('close_range'), 2)[1]));
                    if ($close_start == Carbon::now()->format('Y-m-d') && $close_end == Carbon::now()->format('Y-m-d')) {
                        return true;
                    } else {
                        return (($close_at > $close_start) && ($close_at < $close_end)) ? true : false;
                    }
                });
            }

            if ($request->has('open_range')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $open_date = date('Y-m-d', strtotime($row['open_date']));
                    $open_start = date('Y-m-d', strtotime(explode(" - ", $request->get('open_range'), 2)[0]));
                    $open_end = date('Y-m-d', strtotime(explode(" - ", $request->get('open_range'), 2)[1]));
                    if ($open_start == Carbon::now()->format('Y-m-d') && $open_end == Carbon::now()->format('Y-m-d')) {
                        return true;
                    } else {
                        return (($open_date > $open_start) && ($open_date < $open_end)) ? true : false;
                    }
                });
            }

            if ($request->has('follow_range')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    $contact_date = date('Y-m-d', strtotime($row['contact_date']));
                    $contact_start = date('Y-m-d', strtotime(explode(" - ", $request->get('follow_range'), 2)[0]));
                    $contact_end = date('Y-m-d', strtotime(explode(" - ", $request->get('follow_range'), 2)[1]));
                    if ($contact_start == Carbon::now()->format('Y-m-d') && $contact_end == Carbon::now()->format('Y-m-d')) {
                        return true;
                    } else {
                        return (($contact_date > $contact_start) && ($contact_date < $contact_end)) ? true : false;
                    }
                });
            }
        })
        ->make(true);

    }

    public function create()
    {
        $leads = Leads::orderBy('title', 'asc')->get()->pluck('title', 'id');
        $clients = Client::pluck('name', 'id');
        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');
        $users = User::pluck('name', 'id');
        return view($this->view_path . 'create', compact('leads'))
            ->withUsers($this->users->getAllUsersWithLeadsDepartments())
            ->withClients($clients)
            ->withServices($services);

    }

    public function store(StoreLeadRequest $request)
    {
        $getInsertedId = $this->leads->create($request);
        Flash::success($this->append_flash . ' added successfully!');
        return redirect(route($this->route_base_prefix . $this->route_base.'.show', $getInsertedId));
       
    }

    public function show($id)
    {
        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');
        $plans = Plan::orderBy('name', 'asc')->get()->pluck('name', 'id');
        $service_plans = Service::with('plans')->get()->all();
        $service_plans = json_decode(json_encode($service_plans), true);
        $hr_users = $this->users->getCustomerRelationshipManagers();
        foreach ($service_plans as $key => $service) {
            foreach ($service['plans'] as $key => $plan) {
                $final[$service['id']][] = array('id' => $plan['id'], 'name' => $plan['name']);
            }
        }
        
        return view( $this->view_path  .'show')
            ->withLeads($this->leads->find($id))
            ->withUsers($this->users->getAllUsersWithLeadsDepartments())
            ->withServices($services)
            ->withPlans($plans)
            ->withServicePlans($final)
            ->withHrusers($hr_users);
    }


    public function edit($id)
    {
        $lead = Leads::findOrFail($id);
        if ($lead['status'] != '0') {
            Session()->flash('flash_message', 'Lead already open');
            return redirect()->back();
        }
        $leads = Leads::orderBy('title', 'asc')->get()->pluck('title', 'id');
        $clients = Client::pluck('name', 'id');
        $users = User::pluck('name', 'id');
        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');
        return view($this->view_path . 'edit', compact('leads'))
            ->withLead($this->leads->find($id))
            ->withUsers($this->users->getAllUsersWithLeadsDepartments())
            ->withClients($clients)
            ->withServices($services);
    }

    public function update($id, UpdateLeadRequest $request)
    {
        $this->leads->update($id, $request);
        Session()->flash('flash_message', 'Lead successfully updated');
          
        return redirect('leads');
    }

    public function updateAssign($id, Request $request)
    {
        $this->leads->updateAssign($id, $request);
        Session()->flash('flash_message', 'New user is assigned');
        return redirect()->back();
    }

    public function updateBasicInfo($id, Request $request)
    {
        
        if (!empty($request->all()['has_discount'])) {
            $validator = Validator::make($request->all(), [
                'confidence' => 'numeric|between:0,100',
                'discount_value' => 'required|numeric|between:0,100'
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'confidence' => 'numeric|between:0,100'
            ]);
        }
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $this->leads->updateBasicInfo($id, $request);
        Session()->flash('flash_message', 'Info Updated');
        return redirect()->back();
    }

    public function updateFollowup(UpdateLeadFollowUpRequest $request, $id)
    {
        $this->leads->updateFollowup($id, $request);
        Session()->flash('flash_message', 'New follow up date is set');
        return redirect()->back();
    }

    public function completeLead(LeadCompleteRequest $request, $id)
    {
        $services = $this->buildServicesArray($request->all());
        $plan = Plan::findOrFail($services[1]['fk_plan_id']);
        $service = DB::table('services')->where('id', $plan->service_id)->get();
        $plan_detail = $plan->toArray();
        $service_detail = $service->toArray();

        if($service_detail[0]->payment_mode == 1){
            $net_payable = $plan_detail['annual_fee'];
            $capital_fee = $services[1]['service_capital'] / 100;
            if($capital_fee > $net_payable){
                $net_payable = $capital_fee;
            }
        } else {
            $net_payable = $plan_detail['service_fee'];        
        }
        $discount = isset($services[1]['discount']) ? $services[1]['discount'] : 0;

        if($discount != 0 && $net_payable != 0){
            $net_payable = $net_payable - ($discount / 100) * $net_payable;
        }

        $net_amount = 0;
        if($services[1]['payment_type'] == 'full'){
            $net_amount  = $services[1]['amount'];
        } else {
            foreach ($services[1]['transaction'] as $trans) {
                $net_amount += $trans['amount'];
            }    
        }

        $services[1]['service_details'] = $service_detail[0];
        $services[1]['plan_details'] = $plan_detail;

        if($net_amount != $net_payable){
            $str = 'Total amount must be equal to ' . $net_payable . ' and you entered only ' . $net_amount;
            echo json_encode(array('success' => 0, 'msg' => $str, 'alert_type' => 'swal'));
            die();    
        } else{
            $this->leads->completeLead($request->all(), $services);
            Flash::success('Lead Has Been Completed!');
            echo json_encode(array('success' => 1));
            die();    
        }
    }

    public function destroy($id)
    {
        $item = $this->leads->find($id);
        
        DB::table('notes')->where('fk_lead_id', $id)->delete();
        DB::table('activity_log')->where('type', 'lead')->where('type_id', $id)->delete();
        if (!$item->delete()) {
            Flash::success('Error occurred! Please try again after some time ');
        } else {
            Flash::success($this->append_flash . ' deleted successfully!');
        }

        return redirect(route($this->route_base_prefix . $this->route_base . ".index"));
    }

    public function updateStatus($id, Request $request)
    {
        $this->leads->updateStatus($id, $request);
        Session()->flash('flash_message', 'Lead is completed');
        return redirect()->back();
    }

    private function buildServicesArray($request='')
    {
        $services = array();

        $count = 0;

        $service = $request['services'][0];

        if($service['payment_type'] == 'full'){
            if( $service['fk_payment_type'] == 'cash' ){
                if(!empty($service['transaction'][0]['amount']) || !empty($service['transaction'][0]['date'])) {
                    $tmp = array();

                    $tmp['fk_service_id']    = $service['fk_service_id'];
                    $tmp['fk_plan_id']       = $service['fk_plan_id'];
                    $tmp['service_capital']  = (isset($service['capital'])) ? $service['capital'] : 0;
                    $tmp['fk_payment_type']  = $service['fk_payment_type'];
                    $tmp['payment_type']     = $service['payment_type'];
                    $tmp['activation_date']  = date('Y-m-d', strtotime($service['activation_date']));
                    $tmp['force_verify']     = (isset($service['force_verify'])) ? $service['force_verify'] : '';
                    $tmp['comments']         = (isset($service['comments'])) ? $service['comments'] : '';
                    $tmp['force_verify_reason'] = (isset($service['force_verify_reason'])) ? $service['force_verify_reason'] : '';
                    $tmp['transaction_id']   = $service['transaction'][0]['transaction_id'];
                    $tmp['received_from']    = $service['transaction'][0]['received_from'];
                    $tmp['amount']           = $service['transaction'][0]['amount'];
                    $tmp['date']             = date('Y-m-d', strtotime($service['transaction'][0]['date']));
                    $tmp['discount']         = (isset($service['discount'])) ? $service['discount'] : 0;

                    $services[++$count] = $tmp;
                }
            }

            if( $service['fk_payment_type'] == 'cheque' ){
                if(!empty($service['transaction'][0]['amount']) || !empty($service['transaction'][0]['date'])) {

                    $tmp = array();

                    $tmp['fk_service_id']       = $service['fk_service_id'];
                    $tmp['fk_plan_id']          = $service['fk_plan_id'];
                    $tmp['service_capital']     = (isset($service['capital'])) ? $service['capital'] : 0;
                    $tmp['fk_payment_type']     = $service['fk_payment_type'];
                    $tmp['payment_type']        = $service['payment_type'];
                    $tmp['activation_date']     = date('Y-m-d', strtotime($service['activation_date']));
                    $tmp['force_verify']        = (isset($service['force_verify'])) ? $service['force_verify'] : '';
                    $tmp['comments']            = (isset($service['comments'])) ? $service['comments'] : '';
                    $tmp['force_verify_reason'] = (isset($service['force_verify_reason'])) ? $service['force_verify_reason'] : '';
                    $tmp['cheque_number']       = $service['transaction'][0]['cheque_number'];
                    $tmp['bank']                = $service['transaction'][0]['bank'];
                    $tmp['amount']              = $service['transaction'][0]['amount'];
                    $tmp['date']                = date('Y-m-d', strtotime($service['transaction'][0]['date']));    
                    $tmp['discount']            = (isset($service['discount'])) ? $service['discount'] : 0;

                    $services[++$count] = $tmp;
                }
            }

            if( $service['fk_payment_type'] == 'net_banking' ){
                if(!empty($service['transaction'][0]['amount']) || !empty($service['transaction'][0]['date'])) {

                    $tmp = array();
                    $tmp['fk_service_id']       = $service['fk_service_id'];
                    $tmp['fk_plan_id']          = $service['fk_plan_id'];
                    $tmp['service_capital']     = (isset($service['capital'])) ? $service['capital'] : 0;
                    $tmp['fk_payment_type']     = $service['fk_payment_type'];
                    $tmp['payment_type']        = $service['payment_type'];
                    $tmp['activation_date']     = date('Y-m-d', strtotime($service['activation_date']));
                    $tmp['force_verify']        = (isset($service['force_verify'])) ? $service['force_verify'] : '';
                    $tmp['comments']            = (isset($service['comments'])) ? $service['comments'] : '';
                    $tmp['force_verify_reason'] = (isset($service['force_verify_reason'])) ? $service['force_verify_reason'] : '';
                    $tmp['type']                = $service['transaction'][0]['type'];
                    $tmp['transaction_id']      = $service['transaction'][0]['transaction_id'];
                    $tmp['received_from']       = $service['transaction'][0]['received_from'];
                    $tmp['amount']              = $service['transaction'][0]['amount'];
                    $tmp['date']                = date('Y-m-d', strtotime($service['transaction'][0]['date']));
                    $tmp['discount']            = (isset($service['discount'])) ? $service['discount'] : 0;

                    $services[++$count] = $tmp;
                }
            }
        } else {
            foreach ($request['services'] as $key => $service) {
                $tmp = array();
                $tmp['fk_service_id']                       = $service['fk_service_id'];
                $tmp['fk_plan_id']                          = $service['fk_plan_id'];
                $tmp['service_capital']                     = (isset($service['capital'])) ? $service['capital'] : 0;
                $tmp['fk_payment_type']                     = 'other';
                $tmp['payment_type']                        = $service['payment_type'];
                $tmp['activation_date']                     = date('Y-m-d', strtotime($service['activation_date']));
                $tmp['force_verify']                        = (isset($service['force_verify'])) ? $service['force_verify'] : '';
                $tmp['comments']                            = (isset($service['comments'])) ? $service['comments'] : '';
                $tmp['force_verify_reason']                 = (isset($service['force_verify_reason'])) ? $service['force_verify_reason'] : '';
                $tmp['discount']                            = (isset($service['discount'])) ? $service['discount'] : 0;

                foreach ($service['transaction'] as $pay_no => $transaction) {
                    if(!empty($transaction['amount']) || !empty($transaction['date'])) {
                        $tmp['transaction'][$pay_no]['payment_type']     = $transaction['fk_payment_type'];
                        $tmp['transaction'][$pay_no]['amount']           = $transaction['amount'];
                        $tmp['transaction'][$pay_no]['date']             = date('Y-m-d', strtotime($transaction['date']));

                        if( $transaction['fk_payment_type'] == 'cash' ){
                            $tmp['transaction'][$pay_no]['transaction_id']   = (!empty($transaction['transaction_id'])) ? $transaction['transaction_id'] : '';
                            $tmp['transaction'][$pay_no]['received_from']    = (!empty($transaction['received_from'])) ? $transaction['received_from'] : '';
                        }

                        if( $transaction['fk_payment_type'] == 'net_banking' ){
                            $tmp['transaction'][$pay_no]['type']             = (!empty($transaction['type'])) ? $transaction['type'] : '';
                            $tmp['transaction'][$pay_no]['transaction_id']   = (!empty($transaction['transaction_id'])) ? $transaction['transaction_id'] : '';
                            $tmp['transaction'][$pay_no]['received_from']    = (!empty($transaction['received_from'])) ? $transaction['received_from'] : '';
                        }

                        if( $transaction['fk_payment_type'] == 'cheque' ){
                            $tmp['transaction'][$pay_no]['cheque_number']   = (!empty($transaction['cheque_number'])) ? $transaction['cheque_number'] : '';
                            $tmp['transaction'][$pay_no]['bank']   = (!empty($transaction['bank'])) ? $transaction['bank'] : '';
                        }
                    }
                }

                $services[++$count] = $tmp;
            }
        }
        return $services;
    }
}
