<?php

namespace App\Http\Controllers\Admin;

use App\BseDailyBhav;
use App\CompanyMaster;
use App\Http\Controllers\SmsController;
use App\Models\Filters\DeliveryFilters;
use App\Models\Client;
use App\NfoDailyBhav;
use App\Notifications\AlertNotify;
use App\Notifications\ServiceNotify;
use App\NseDailyBhav;
use App\Plan;
use App\Repositories\ServiceDelivery\ServiceDeliveryRepository;
use App\Repositories\User\UserRepository;
use App\SdTemplate;
use App\Service;
use App\ServiceDelivery;
use App\ServiceDeliveryService;
use App\ServiceDeliveryUser;
use App\Token;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Flash;
use Illuminate\Support\Facades\DB;
use Validator;

class ServiceDeliveryController extends Controller
{
    protected $base_model;
    protected $view_path;
    protected $route_base;
    protected $route_base_prefix = 'admin::';
    protected $append_flash;
    protected $validation_rule;


    public function __construct(Request $request)
    {
        $this->base_model = new ServiceDeliveryRepository() ;
        $this->view_path = 'admin.service_delivery.';
        $this->route_base = 'service_delivery';
        $this->validation_rule = [
            'symbol' => 'required',
            'expiry' => 'required_if:exchange,nfo',
            'option_type' => 'required_if:instrument,option|in:ce,pe',
            'strike' => 'required_if:instrument,options',
            'exchange' => 'required|in:bse,nse,nfo',
            'instrument' => 'required|in:stock,future,option',
            'nfo_instrument' => 'required_if:instrument,nfo',
            'price' => 'required|numeric',
            'range_from' => 'required',
            'range_to' => 'required',
            'stop_loss' => 'numeric',
            'target_1' => 'numeric',
            'target_2' => 'numeric',
            'service' => 'required',
            'service.*.qty' => 'required|numeric',
            'service.*.lot_size' => 'required_if:instrument,future,options|numeric',
            'service.*.value' => 'required_if:instrument,future,options|numeric',
            'call_type' => 'required|in:new,re_entry,exit',
            'customers' => 'required',
            'date_time' => 'required|date',
            'signature' => 'present',
            'remark' => 'present',
            'mode.*' => 'required',

        ];
        $this->messages = [];
        $this->append_flash = 'Service Delivery ';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DeliveryFilters $filters)
    {
        $list = $this->base_model->filterData($filters)->paginate(15);
        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');
        return view($this->view_path . 'index', compact('list', 'services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->view_path . 'create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adjust()
    {
        return view($this->view_path . 'adjust');
    }

    public function customerData(Request $request){
        $users = User::whereIn('id', explode(',', $request->input('user_ids')))->get(['id', 'name', 'email', 'status']);
        return $users;
    }

    public function store(Request $request)
    {
        $internal_test = false; // flage true if want to debug it quickly no data saved

        if($internal_test){
            dump($request->all());
        }
        $validator = Validator::make($request->all(), $this->validation_rule);
        if ($validator->fails()) {
            return redirect(route($this->route_base_prefix . $this->route_base.'.create'))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $data = $request->except(['_token','customers', 'service', 'msg']);
            if($request->has('price_range') && $request->get('price_range') == 1 && $request->has('range_from') && $request->has('range_to')) {
                $data['price'] = ($request->get('range_from') + $request->get('range_to'))/2;
            }

            $data['given_by'] = auth()->user()->id;
            if($data['instrument'] == 'stock')
            {
                $data['expiry'] = null;
            }

            $item = new ServiceDelivery($data);
            if($internal_test !== true){
                if(!$item->save())
                {
                    Flash::error('Error occurred! Please try again after some time ');
                    return redirect(route($this->route_base_prefix . $this->route_base . '.create'))
                        ->withInput($request->all());
                }
            }

            $req_services = $request->get('service');

            $db_service  = Service::whereIn('id', array_keys($request->get('service')))->get();
            $db_service = $db_service->keyBy('id')->toArray();

            $result_user = [];

            foreach($request->get('customers') as $percent => $services)
            {
                foreach($services as $service_id => $plan_custs)
                {
                    if(($request->has('only_service') && $request->get('only_service') ==  $service_id) || !$request->has('only_service') )
                    {
                        if(!isset($req_services[$service_id]['service_delivery_service_id'])) {
                            $req_services[$service_id]['lot_size'] = isset($req_services[$service_id]['lot_size']) ? $req_services[$service_id]['lot_size'] : 0;
                            $req_services[$service_id]['value'] = isset($req_services[$service_id]['value']) ? $req_services[$service_id]['value'] : 0;
                            $data = [
                                'service_id' => $service_id,
                                'service_delivery_id' => $item->id,
                                'qty' => $req_services[$service_id]['qty'],
                                'template_id' => $req_services[$service_id]['template_id'],
                                'lot_size' => isset($req_services[$service_id]['lot_size']) ? $req_services[$service_id]['lot_size'] : 0,
                                'value' => isset($req_services[$service_id]['value']) ? $req_services[$service_id]['value'] : 0
                            ];
                            if($internal_test === true){
                                $req_services[$service_id]['service_delivery_service_id'] = 1;
                            } else {
                                $delivery = new ServiceDeliveryService($data);
                                $delivery->save();
                                $req_services[$service_id]['service_delivery_service_id'] = $delivery->id;
                            }

                        }

                        foreach($plan_custs as $plan_id => $customers)
                        {
                            $symbol_data = [
                                'symbol' => $item->symbol,
                                'exchange' => $item->exchange,
                                'expiry_date' => $item->expiry,
                                'option_type' => $item->option_type,
                                'strike' => $item->strike,
                                'nfo_instrument' => $item->nfo_instrument,
                                'qty' => $req_services[$service_id]['qty'],
                                'lot_size' => $req_services[$service_id]['lot_size'],
                            ];

                            $customers = User::whereIn('id', $customers)->get();
                            foreach ($customers as $customer) {

                                $limit = $customer->getStockSummary($symbol_data, $service_id, $plan_id ,$item->call_type);

                                if($internal_test === true)
                                    dump($limit, $req_services, $customer, $service_id);

                                $nfo_qty = 0;
                                $exit_qty = 0;
                                if($request->get('call_type') != 'exit'){
                                    if($item->exchange != 'nfo'){
                                        $qty = round($req_services[$service_id]['qty'] * ($percent/100), 2, PHP_ROUND_HALF_DOWN);
                                    } else {
                                        $nfo_qty = (int) round($req_services[$service_id]['qty'] * ($percent/100),0);
                                        $qty = $nfo_qty * $req_services[$service_id]['lot_size'] * $item->price;
                                    }
                                    $qty = ($qty > $limit['avail_limit'])? $limit['avail_limit'] : $qty;
                                }
                                else{
                                    if(!isset($req_services[$service_id]['qty_5plus'])){
                                        $req_services[$service_id]['qty_5plus'] = 0;
                                    }

                                    if(!isset($req_services[$service_id]['qty_5less'])){
                                        $req_services[$service_id]['qty_5less'] = 0;
                                    }

                                    if($item->exchange != 'nfo'){
                                        if($limit['used_total'] >= 5){
                                            $exit_qty = $req_services[$service_id]['qty_5plus'];
                                            $qty = round(bcmul($req_services[$service_id]['qty_5plus'] , bcdiv($limit['used_total'], 100)), 2, PHP_ROUND_HALF_DOWN);
                                        }
                                        else {
                                            $exit_qty = $req_services[$service_id]['qty_5less'];
                                            $qty = round( bcmul($req_services[$service_id]['qty_5less'] , bcdiv($limit['used_total'],100)),  2, PHP_ROUND_HALF_DOWN);
                                        }
                                    } else {
                                        $p = round(bcmul(bcdiv($limit['used_total'] , $limit['service_capital'] ) , 100), 2, PHP_ROUND_HALF_DOWN);
                                        if($p >= 5){
                                            $nfo_qty = round(bcmul($req_services[$service_id]['qty_5plus'] , bcdiv($limit['nfo_qty'],100)));
                                        }
                                        else {
                                            $nfo_qty = round(bcmul($req_services[$service_id]['qty_5less'] , bcdiv($limit['nfo_qty'],100)));
                                        }
                                        $qty = round(bcmul($nfo_qty , bcmul($req_services[$service_id]['lot_size'],$item->price)), 2, PHP_ROUND_HALF_DOWN);
                                    }
                                }

                                $keywords = [
                                    '[SERVICE]' => $db_service[$service_id]['name'],
                                    '[SERVICECODE]' => $db_service[$service_id]['code'],
                                    '[QTY]' => $qty,
                                    '[SYMBOL]' => $item->symbol,
                                    '[EXPIRYMONTH]' => Carbon::parse($item->expiry)->format('M-y'),
                                    '[PRICE]' => $item->price,
                                    '[REMARK]' => $item->remark,
                                    '[SIGNATURE]' => $item->signature,
                                    '[TARGET1]' => $item->target_1,
                                    '[TARGET2]' => $item->target_2,
                                    '[STOPLOSS]' => $item->stop_loss,
                                    '[RANGE]' => $item->range_from. " - " . $item->range_to,
                                    '[INVESTORTYPE]' => config('rothmans.investor_type.'.$customer->investor_type),
                                    '[INVESTORHORIZON]' => config('rothmans.horizon.'.$customer->horizon),
                                ];

                                if($item->exchange != 'nfo'){
                                    if($db_service[$service_id]['payment_mode'] == 0) {
                                        if($request->get('call_type') == 'exit'){
                                            //$keywords['[QTY]'] =  $exit_qty . "%";
                                            $keywords['[QTY]'] =  $exit_qty;
                                        } else {
                                            //$keywords['[QTY]'] =  $qty . "%";
                                            $keywords['[QTY]'] =  $qty;
                                        }
                                    }
                                    else {
                                        $base_amt = $qty * $limit['service_capital'] / 100;
                                        $keywords['[QTY]'] = round( $base_amt / $item->price, 0);
                                    }
                                } else {
                                    $keywords['[QTY]'] =  $nfo_qty;
                                }

                                if(strpos('', $request->get('msg')[$service_id]) === false)
                                {
                                    $request->get('msg')[$service_id] .= "\n\r[DISCLAIMER]";
                                }

                                //$msg_str =  $request->get('msg')[$service_id] . "\n\r Disclaimer: whatever with [INVESTORTYPE] and [INVESTORHORIZON]";

                                $message = str_replace(array_keys($keywords), array_values($keywords), $request->get('msg')[$service_id]);


                                $data = [
                                    'service_id' => $service_id,
                                    'plan_id' => $plan_id,
                                    'service_delivery_id' => $item->id,
                                    'service_delivery_service_id' => $req_services[$service_id]['service_delivery_service_id'],
                                    'user_id' => $customer->id,
                                    'call_type' => $item->call_type,
                                    'symbol' => $item->symbol,
                                    'qty' => ($item->exchange != 'nfo')? $qty: 0,
                                    'nfo_qty' => $nfo_qty,
                                    'value' => ($item->exchange == 'nfo')? $qty: 0,
                                    'msg' => $message
                                ];

                                if($internal_test === true)
                                    dump($limit, $data, $req_services, $item, $keywords);

                                $error = false;

                                if($item->exchange != 'nfo'  && $data['qty'] <= 0){
                                    $error = true;
                                } else if($item->exchange == 'nfo'  && $data['nfo_qty'] <= 0){
                                    $error = true;
                                }

                                if($error){
                                    $result_user[] = [
                                        'id' => $customer->id,
                                        'name' => $customer->name,
                                        'result' => false
                                    ];
                                    continue;
                                }
                                if($internal_test !== true) {
                                    $delivery = new ServiceDeliveryUser($data);
                                    if ($delivery->save()) {
                                        $service_notify = new ServiceNotify($customer, $message,$item->mode, $db_service[$service_id]['name'], $item->signature);
                                        $service_notify->delivery = $delivery;
                                        $customer->notify($service_notify);
                                    } else {
                                        $result_user[] = [
                                            'id' => $customer->id,
                                            'name' => $customer->name,
                                            'result' => false
                                        ];
                                    }
                                }

                            }
                        }
                    }

                }
            }

            if($internal_test === true)
                dd('test');

            if($request->has('only_service')) {
                return $request->get('only_service');
            } else {
                Flash::success($this->append_flash . ' added successfully!');
                return redirect(route($this->route_base_prefix . $this->route_base . '.create'));
            }


        }
    }

    public function getClients(Request $request)
    {
        if(!$request->has('qty') || !$request->has('call_type'))
        {
            return [
                'error' => 'Insufficient parameters',
                'customers' => [],
            ];
        }
        if((config('rothmans.allowed_stock_limit.max') < $request->get('qty')
            || config('rothmans.allowed_stock_limit.min') > $request->get('qty'))
            && $request->get('call_type') != 'exit' && $request->get('exchange') == 'nse')
        {
            return [
                'error' => 'Quantity is not permissible limit',
                'customers' => [],
            ];
        }

        $user_qry = User::select(['users.id', 'users.name', 'users.email','plans.name as plan_name','user_services.plan_id', 'user_services.service_id', 'user_services.service_capital'])
                ->where('user_type','Customer')
            ->where('service_status','Active')
            ->where('user_services.service_id',$request->get('service_id'))
            ->leftJoin('user_services', 'users.id', '=', 'user_services.user_id')
            ->leftJoin('plans', 'plans.id', '=', 'user_services.plan_id')
            ->orderBy('users.id', 'desc');


        $existing_user_id = ServiceDeliveryUser::selectRaw('distinct(user_id)')
            ->leftJoin('service_deliveries', 'service_deliveries.id', '=', 'service_delivery_user.service_delivery_id')
            ->where('service_deliveries.symbol', $request->get('symbol'))
            ->where('service_deliveries.exchange', $request->get('exchange'));

        if($request->get('exchange') == 'nfo')
        {
            $existing_user_id = $existing_user_id->where('service_deliveries.nfo_instrument',$request->get('nfo_instrument'))
                ->where('service_deliveries.expiry',Carbon::parse($request->get('expiry_date'))->toDateString());

            if(in_array($request->get('nfo_instrument'), ['OPTIDX', 'OPTSTK'])) {
                $existing_user_id = $existing_user_id->where('service_deliveries.option_type',$request->get('option_type'))
                    ->where('service_deliveries.strike',$request->get('strike'));
            }
        }

        $existing_user_id = $existing_user_id->where('service_delivery_user.service_id',$request->get('service_id'))
            ->get()->pluck('user_id');

        if(count($existing_user_id) > 0){
            if($request->get('call_type') == 'new' && $request->get('include_existing') == false)
                $user_qry = $user_qry->whereNotIn('users.id', $existing_user_id);
            else if(in_array($request->get('call_type'), ['re_entry', 'exit']))
                $user_qry = $user_qry->whereIn('users.id', $existing_user_id);
        }

        $base_users = $user_qry->groupBy(['users.id', 'user_services.service_id', 'user_services.plan_id'])->get();

        $min_qty = [
            '75' => 99.99,
            '50' => 74.99,
            '25' => 49.99,
            '24.99' => 24.99,
        ];
        $changed_min_qty = $min_qty;
        $users  = [];

        $symbol_data = [
            'symbol' => $request->get('symbol'),
            'exchange' => $request->get('exchange'),
            'expiry_date' => $request->get('expiry_date'),
            'option_type' => $request->get('option_type'),
            'strike' => $request->get('strike'),
            'nfo_instrument' => $request->get('nfo_instrument'),
            'qty' => $request->get('qty'),
            'lot_size' => $request->get('lot_size'),
        ];

        if($base_users) {
            foreach($base_users as $user) {
                $temp = $user->getStockSummary($symbol_data, $user->service_id, $user->plan_id ,$request->get('call_type'));
                if($temp) {
                    $arr = $user->toArray();
                    $arr['service_deliveries'] = $temp;
                    $users[] = $arr;
                }
            }

            foreach ($users as $key => $user) {
                if( $user['service_deliveries']['used_total'] == 0 && $request->get('call_type') == 'exit')
                {
                    unset($users[$key]);
                    continue;
                }
                if(($request->get('exchange') != 'nfo'
                        && ($request->get('call_type') == 'new' || $request->get('call_type') == 're_entry')
                        && config('rothmans.allowed_stock_limit.max') <= $user['service_deliveries']['used_total'])

                        || ($request->get('call_type') == 'exit')
                            && config('rothmans.allowed_stock_limit.min') >= $user['service_deliveries']['used_total']
                ) {
                    unset($users[$key]);
                    continue;
                }

                $user['service_deliveries']['avail_limit'] = (float)config('rothmans.allowed_stock_limit.max') - (float)$user['service_deliveries']['used_mapped_perten'];
                if($request->get('call_type') == 're_entry' && $user['service_deliveries']['avail_limit'] == 0){
                    unset($users[$key]);
                    continue;
                }
                if($user['service_deliveries']['avail_limit'] > 0){
                    if($request->get('call_type') == 'exit'
                        && abs($user['service_deliveries']['used_total']) >= config('rothmans.allowed_stock_limit.min'))
                    {
                        if($request->get('exchange') == 'nfo') {
                            $available_lmt = round(abs($user['service_deliveries']['nfo_qty']) * 100 / $request->get('qty'), 2, PHP_ROUND_HALF_DOWN);
                        } else {
                            $available_lmt = round(abs($user['service_deliveries']['used_total']) * 100 / $request->get('qty'), 2, PHP_ROUND_HALF_DOWN);
                        }

                    } elseif($request->get('call_type') == 're_entry' || $request->get('call_type') == 'new'){
                        if($request->get('exchange') != 'nfo') {
                            $available_lmt = round( $user['service_deliveries']['avail_limit'] * 100 / $request->get('qty') , 2, PHP_ROUND_HALF_DOWN);
                            if($available_lmt >= 100){
                                $available_lmt = 100;
                            }
                        } else {
                            $max_qty = floor($user['service_deliveries']['avail_limit'] / ($request->get('lot_size') * $request->get('price')));
                            if($max_qty < 1){
                                $available_lmt = 0;
                            }
                            else if($max_qty >= $request->get('qty')){
                                $available_lmt = 100;
                            } else {
                                $available_lmt = ($max_qty / $request->get('qty')) * 100;
                            }
                        }
                    }
                } else {
                    if($request->get('exchange') == 'nse') {
                        $available_lmt = 100;
                    } else {
                        $max_qty = floor($user['service_deliveries']['avail_limit'] / ($request->get('lot_size') * $request->get('price')));
                        if($max_qty < 1){
                            $available_lmt = 0;
                        }
                        else if($max_qty >= $request->get('qty')){
                            $available_lmt = 100;
                        } else {
                            $available_lmt = ($max_qty / $request->get('qty')) * 100;
                        }
                    }
                }

                if($available_lmt == 0) {
                    unset($users[$key]);
                    continue;
                }
                switch ($available_lmt) {
                    case $available_lmt >= 100:
                        $users[$key]['remaining_qty'] = 100;
                        break;
                    case $available_lmt < 100 && $available_lmt >= 75:
                        $users[$key]['remaining_qty'] = 75;
                        if($changed_min_qty['75'] > $available_lmt)
                            $changed_min_qty['75'] = $available_lmt;
                        break;
                    case $available_lmt < 75 && $available_lmt >= 50:
                        $users[$key]['remaining_qty'] = 50;
                        if($changed_min_qty['50'] > $available_lmt)
                            $changed_min_qty['50'] = $available_lmt;
                        break;
                    case $available_lmt < 50 && $available_lmt >= 25:
                        $users[$key]['remaining_qty'] = 25;
                        if($changed_min_qty['25'] > $available_lmt)
                            $changed_min_qty['25'] = $available_lmt;
                        break;
                    default:
                        $users[$key]['remaining_qty'] = 24.99;
                        if($changed_min_qty['24.99'] > $available_lmt)
                            $changed_min_qty['24.99'] = $available_lmt;
                }

                $users[$key]['service_id'] = $request->get('service_id');
                $users[$key]['available_qty'] = $available_lmt;
            }

            $updates = array_diff($changed_min_qty, $min_qty);
            if($updates)
            {
                foreach($users as $key => $val){
                    if(isset($updates[$val['remaining_qty']])) {
                        $users[$key]['available_qty'] = $updates[$val['remaining_qty']];
                    }
                }
            }
            if(!$users) {
                return [
                    'error' => 'No Customers match provided parameters',
                    'customers' => [],
                ];
            }
            return [
                'customers' => array_values($users)
            ];
        }
        else {
            return [
                'error' => 'No Customers match provided parameters',
                'customers' => [],
            ];
        }
    }

    public function getTemplates(Request $request){
        $template =  SdTemplate::where('service_id', $request->get('service_id'))->where('template_type', $request->get('call_type'))->where('exchange', $request->get('exchange'))->orderBy('name', 'asc')->first();
        if(!$template)
        {
            $template =  SdTemplate::where('service_id', 0)->where('template_type', $request->get('call_type'))->where('exchange', $request->get('exchange'))->orderBy('name', 'asc')->first();
        }

        return $template;
    }

    public function getServices(Request $request){
        if($request->get('call_type') == 'new') {
            if($request->get('exchange') == 'nfo') {
                return Service::select('services.*')
                    ->leftJoin('user_services', 'user_services.service_id', '=', 'services.id')
                    ->whereNotNull('user_services.service_id')
                    ->where('service_status','Active')
                    ->where('services.status',1)
                    //->where('payment_mode', 1)
                    ->groupBy('services.id')
                    ->orderBy('name', 'asc')->get();
            } else {
                return Service::select('services.*')
                    ->leftJoin('user_services', 'user_services.service_id', '=', 'services.id')
                    ->whereNotNull('user_services.service_id')
                    ->where('service_status','Active')
                    ->where('services.status',1)
                    ->groupBy('services.id')
                    ->orderBy('name', 'asc')->get();
            }
        } elseif( in_array($request->get('call_type'), ['re_entry', 'exit'])) {
            $open_services = $this->base_model->getOpenServices($request);
            if(count($open_services) > 0) {
                $ids = collect($open_services)->pluck('id');
                return Service::whereIn('id', $ids)->where('status',1)->get();
            }
            return [];
        }
    }

    public function getOpenServices(Request $request){
        return $this->base_model->getOpenServices($request);
    }

    public function getStocks(Request $request){
        if($request->has('no_group') && $request->get('no_group') == true ) {
            return NfoDailyBhav::selectRaw('trim(symbol) as symbol, ROUND(close,2) as price, DATE_FORMAT(expiry_date, "%d-%b-%Y") as expiry_date, strike, option_type')->where('symbol', $request->get('type'))->where('instrument', $request->get('instrument'))->orderBy('symbol','asc')->get();
        }

        /*return \DB::select("(SELECT cm.name as company,
             if( lm.scrip_code2_given_by_exchange <> '', trim(ndb.symbol), trim(bdb.sc_name) ) as symbol,
             if( lm.scrip_code2_given_by_exchange <> '', ROUND(ndb.last,2), ROUND(bdb.last,2) ) as price,
             if( lm.scrip_code2_given_by_exchange <> '', 'nse', 'bse' ) as exchange, '' as instrument
            FROM `company_masters` cm
            left join `listing_masters` lm on lm.`security_code` = cm.`id`
            join `nse_daily_bhavs` ndb on ndb.`symbol` = lm.scrip_code2_given_by_exchange and ndb.series = 'EQ'
            LEFT OUTER JOIN `nse_daily_bhavs` ndb2 ON (ndb.timestamp < ndb2.timestamp AND ndb.symbol = ndb2.symbol)
            join `bse_daily_bhavs` bdb on bdb.`sc_code` = lm.scrip_code1_given_by_exchange and lm.scrip_code2_given_by_exchange <> ''
            LEFT OUTER JOIN `bse_daily_bhavs` bdb2 on (bdb.timestamp < bdb2.timestamp AND bdb.sc_code = bdb2.sc_code) and lm.scrip_code2_given_by_exchange <> ''
            
            WHERE lm.exchange_group = 'A'
            and ndb2.symbol IS NULL
            and bdb2.sc_code IS NULL
            and (
            cm.`name` LIKE ? or
            bdb.`sc_name` LIKE ? or
            ndb.symbol LIKE ? 
            )
            )
              union 
            (
            SELECT cm.name as company, trim(ndb.symbol) as symbol, '' as price,'nfo' as `exchange`, ndb.`instrument` 
            FROM  `company_masters` cm
            left join `listing_masters` lm on lm.`security_code` = cm.`id`
            join `nfo_daily_bhavs` ndb on ndb.`symbol` = lm.scrip_code2_given_by_exchange 
            
            WHERE ndb.`symbol` LIKE ?
            group by  ndb.`instrument` , ndb.`symbol`
            ) 
            
            ORDER BY company ASC, exchange desc, instrument asc;
            ", [ $request->get('type')."%", $request->get('type')."%", $request->get('type')."%", $request->get('type')."%"]);*/

        /*return \DB::select("(SELECT trim(ndb.symbol)  as company,
            trim(ndb.symbol) as symbol, ROUND(ndb.last,2) as price, 'nse' as exchange, '' as instrument
            FROM `nse_daily_bhavs` ndb
            WHERE ndb.symbol LIKE ? and series = 'EQ'
            )
            union
            (
            SELECT trim(ndb.symbol) as company, trim(ndb.symbol) as symbol, '' as price,'nfo' as `exchange`, ndb.`instrument`
            FROM `nfo_daily_bhavs` ndb
            WHERE ndb.`symbol` LIKE ?
            group by  ndb.`instrument` , ndb.`symbol`
            )
            ORDER BY company ASC, exchange desc, instrument asc;
            ", [ $request->get('type')."%", $request->get('type')."%"]);*/

        return \DB::select("( SELECT cm.name as company,
                 if( ndb.`symbol` <> '', trim(ndb.symbol), trim(bdb.sc_name) ) as symbol,
                 if( ndb.`symbol` <> '', ROUND(ndb.last,2), ROUND(bdb.last,2) ) as price,
                 if( ndb.`symbol` <> '', 'nse', 'bse' ) as exchange, '' as instrument,
                 cm.status

               FROM `companies` cm
                 left join `nse_daily_bhavs` ndb on ndb.`symbol` = cm.`symbol` and ndb.series = 'EQ'
                 left join `bse_daily_bhavs` bdb on bdb.`sc_code` = cm.`bse_code` and cm.bse_code <> ''
            
               WHERE cm.symbol is not null and (
                 cm.`name` LIKE ? or
                 cm.symbol LIKE ?)
            )
            
             union  
               
            (
                SELECT cm.name as company, trim(ndb.symbol) as symbol, open as price,'nfo' as `exchange`, ndb.`instrument`, cm.status 
                FROM  `companies` cm
                join `nfo_daily_bhavs` ndb on ndb.`symbol` = cm.`symbol`
                
                WHERE cm.name LIKE ? or cm.symbol LIKE ?
                group by  ndb.`instrument` , ndb.`symbol`
            ) 
           
            ORDER BY company ASC, exchange desc, instrument asc;

            ", [ $request->get('type')."%", $request->get('type')."%", $request->get('type')."%", $request->get('type')."%"]);

    }

    public function getStrike(Request $request){

        $result = NfoDailyBhav::selectRaw('strike as id,  strike as name');

        if($request->has('nfo_intruments'))
            $result->where('instrument', $request->get('nfo_intruments'));
        if($request->has('symbol'))
            $result->where('symbol', $request->get('symbol'));
        if($request->has('expiry_date'))
            $result->whereDate('expiry_date', Carbon::parse($request->get('expiry_date'))->toDateString());
        if($request->has('option_type'))
            $result->where('option_type', $request->get('option_type'));

        return $result->get();
    }

    public function getNfoStock(Request $request){

        $result = NfoDailyBhav::selectRaw('trim(symbol) as symbol, `close` as price, expiry_date, strike, option_type');
        if($request->has('nfo_instrument'))
            $result->where('instrument', $request->get('nfo_instrument'));
        if($request->has('symbol'))
            $result->where('symbol', $request->get('symbol'));
        if($request->has('expiry_date'))
            $result->whereDate('expiry_date', Carbon::parse($request->get('expiry_date'))->toDateString());
        if($request->has('option_type'))
            $result->where('option_type', $request->get('option_type'));
        if($request->has('strike'))
            $result->where('strike', $request->get('strike'));

        return $result->first();
    }

    public function getServiceDeliveries(Request $request){
        $result = $this->base_model->getAllDataQuery()
            ->where('symbol', $request->get('symbol'))
            ->whereRaw(sprintf("DATE(created_at) = '%s'", \Carbon::parse($request->get('delivery_date'))->format('Y-m-d')))
            ->where('exchange', $request->get('exchange'))
            ->orderBy('created_at', 'desc');

        if($request->get('exchange') == 'nfo')
            $result->where('instrument', $request->get('nfo_instrument'));

        return $result->get();
    }

    public function recallDelivery(Request $request){

    }

    public function editDelivery(Request $request){
        $result = $this->base_model->getAllDataQuery()
            ->where('symbol', $request->get('symbol'))
            ->whereRaw(sprintf("DATE(created_at) = '%s'", \Carbon::parse($request->get('delivery_date'))->format('Y-m-d')))
            ->where('exchange', $request->get('exchange'))
            ->orderBy('created_at', 'desc');

        if($request->get('exchange') == 'nfo')
            $result->where('instrument', $request->get('nfo_instrument'));

        return $result->get();
    }

    public function userList()
    {
        $list = ServiceDeliveryUser::with(['user', 'plan', 'sd','service'])->orderBy('id', 'Desc')->paginate(15);
        return view($this->view_path . 'users_list', compact('list'));
    }

    public function pnlServiceBased(Request $request){

        $default_delivery_range = date('d/m/Y', strtotime('1 year ago')) . ' - ' . date('d/m/Y', strtotime('today'));
        if($request->has('delivery_range')) {
            $default_delivery_range = $request->get('delivery_range');
        }
        $delivery_range =  array_map(
            function($date) { return date('Y-m-d', strtotime(str_replace('/', '-', $date))); },
            explode(' - ', $default_delivery_range)
        );

        $default_service = [];
        if($request->has('service')) {
            $default_service = (array) $request->get('service');
        }

        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');

        $pnl_services = $this->base_model->deliveryReportbyService($default_service, $delivery_range, $services, false);

        return view($this->view_path . 'service_based', compact('services', 'pnl_services', 'default_delivery_range', 'default_service'));
    }

    public function pnlStockBased(Request $request){
        $default_delivery_range = date('d/m/Y', strtotime('1 month ago')) . ' - ' . date('d/m/Y', strtotime('today'));
        if($request->has('delivery_range')) {
            $default_delivery_range = $request->get('delivery_range');
        }
        $delivery_range =  array_map(
            function($date) { return date('Y-m-d', strtotime(str_replace('/', '-', $date))); },
            explode(' - ', $default_delivery_range)
        );

        $default_service = [];
        if($request->has('service')) {
            $default_service = $request->get('service');
        }

        $services = Service::orderBy('name', 'asc')->get()->pluck('name', 'id');

        $user_count = User::selectRaw('count(users.id) as total')
            ->leftJoin('user_services', 'users.id', '=', 'user_services.user_id');
        if($default_service){
            $user_count = $user_count->whereIn('service_id',$default_service);
        }
        $user_count = $user_count->where('service_status','Active')->first()->total;

        $pnl_stocks = $this->base_model->deliveryReport($default_service, $delivery_range);

        return view($this->view_path . 'stock_based', compact('services', 'pnl_stocks', 'default_delivery_range', 'default_service', 'user_count'));
    }


    public function sendAlerts()
    {
        $status = [
            ['key' => 'active', 'val'=>'Active'],
            ['key' => 'expired', 'val'=>'Expired'],
            ['key' => 'expiring', 'val'=>'Expiring soon'],
            ['key' => 'renewed', 'val'=>'Renewed'],
            ['key' => 'lost', 'val'=>'Lost'],
            ['key' => 'pending', 'val'=>'Pending'],
            ['key' => 'suspend', 'val'=>'Suspended'],
            ['key' => 'temp_extend', 'val'=>'TempExtended'],
        ];

        $services = Service::orderBy('name', 'asc')->get();
        $users = User::select(['users.id', 'name', 'username', 'email', 'mobile', 'service_id', 'plan_id'])
            ->leftJoin('user_services', 'users.id', '=', 'user_services.user_id')
            ->where('user_type','Customer')
            ->groupBy('users.id')
            ->orderBy('users.id', 'desc')->get();
        $m_users = (new UserRepository)->getCustomerRelationshipManagers();
        $managers = [];
        foreach($m_users as $key => $val)
        {
            $managers[] = [
                'key' => $key,
                'val' => $val,
            ];
        }
        return view($this->view_path . 'send_alerts', compact('services', 'users', 'status', 'managers'));
    }

    public function postAlerts(Request $request)
    {
        $validation_rule = [
            'users' => 'required',
            'message' => 'required',
        ];

        $validator = Validator::make($request->all(), $validation_rule);
        if ($validator->fails()) {
            return redirect(route('admin::service_delivery.send_alerts'))
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $users = User::select(['users.id', 'name', 'username', 'email', 'mobile', 'other_mobiles', 'other_emails'])
                ->whereIn('id', explode(', ', $request->get('users')))->get();

            foreach($users as $user)
            {
                $user->notify(new AlertNotify($user, $request->get('message'),$request->get('mode') ));
            }
            Flash::success('Alert sent successfully!');
            return redirect(route('admin::service_delivery.send_alerts'));

        }
    }

    public function filterAlertUser(Request $request)
    {
        $users = User::select(['users.id', 'name', 'username', 'email', 'mobile'])
            ->leftJoin('user_services', 'users.id', '=', 'user_services.user_id');
        if($request->get('services')){
            $users = $users->whereIn('service_id',$request->get('services'));
        }

        if($request->get('managers')){
            $users = $users->leftJoin('user_client_meta', 'users.id', '=', 'user_client_meta.fk_user_id')
                ->whereIn('fk_relation_manager_id',$request->get('managers'));
        }

        if($request->get('status')){
            switch($request->get('status')) {
                case 'active':
                    $users = $users->where('user_services.service_status','Active');
                    break;
                case 'expired':
                    $users = $users->where('user_services.service_status','Expired');
                    break;
                case 'expiring':
                    $users = $users->where('user_services.service_status','Active');
                    break;
                case 'renewed':
                    $users = $users->where('user_services.service_status','Renewed');
                    break;
                case 'lost':
                    $users = $users->where('user_services.service_status','Deleted');
                    break;
                case 'suspend':
                    $users = $users->where('user_services.service_status','Suspended');
                    break;
                case 'temp_extend':
                    $users = $users->where('user_services.service_status','TempExtended');
                    break;
                case 'pending':
                    $users = $users->where('user_services.service_status','Pending');
                    break;
            }
        }

        $users = $users->get();
        return $users;
    }

    public function openCalls(Request $request){
        return view($this->view_path . 'open_calls');
    }


    public function stockAction(Request $request){
        return view($this->view_path . 'stock_wise_action');
    }


    public function updateSuitability(Request $request){
        $current_status =  ServiceDeliveryUser::select(['id', 'suitability'])
            ->whereIn('id', array_keys($request->get('suitable')))
            ->get()->pluck('suitability', 'id');

        $updated = array_diff_assoc($request->get('suitable'), $current_status->toArray());

        $result = [];
        foreach($updated as $key => $suit){
            $result[$suit][] = $key;
        }

        foreach ($result as $suit => $ids)
        {
            if(count($ids) == 0)
                continue;

            $suit_by = 0;
            if($suit == 1){
                $suit_by = auth()->user()->id;
            }

            ServiceDeliveryUser::whereIn('id', $ids)
                ->update(['suitability' => $suit, 'suitability_by' => $suit_by]);
        }
    }

}
