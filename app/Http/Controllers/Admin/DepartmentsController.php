<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Models\Logs;
use App\Activity;

use App\User;
use Datatables;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use Auth;
use App\Role;
use Carbon;
use DB;
use Flash;
use Config;
use Validator;

use App\Models\Department;

use App\Http\Requests\Department\StoreDepartmentRequest;
use App\Http\Requests\Department\UpdateDepartmentRequest;
use App\Repositories\Department\DepartmentRepositoryContract;

class DepartmentsController extends Controller
{

    protected $departments;
    protected $base_model;
    protected $view_path;
    protected $route_base;
    protected $route_base_prefix = 'admin::';
    protected $append_flash;
    protected $validation_rule;
   

    public function __construct(DepartmentRepositoryContract $departments)
    {
        $this->departments = $departments;
         $this->view_path = 'admin.departments.';
            $this->route_base = 'leads';
      //  $this->middleware('user.is.admin', ['only' => ['create', 'destroy']]);
    }
    public function index()
    {
        return view($this->view_path.'index')
        ->withDepartment($this->departments->getAllDepartments());
    }
    public function create()
    {
        return view($this->view_path.'create');
    }

    public function store(StoreDepartmentRequest $request)
    {

        $this->departments->create($request);
        Flash::success('Successfully Created New Department');
        
        return redirect('departments');
    }

     

    public function edit($id)
    {
        return view($this->view_path .'edit')
        ->withDepartment($this->departments->find($id));
    }
    public function update($id,UpdateDepartmentRequest $request)
    {
        $this->departments->update($id,$request);
        Flash::success('Successfully Updated Department');
        
        return redirect('departments');
    }

   
    public function destroy($id)
    {
        $this->departments->destroy($id);
        Flash::success('Successfully Deleted Department');
        dd('asdjajksdn');
        die();
        return redirect('departments');
    }
    
}
