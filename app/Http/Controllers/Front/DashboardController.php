<?php

namespace App\Http\Controllers\Front;

use App\Repositories\ServiceDelivery\ServiceDeliveryRepository;
use App\Service;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Carbon;
use Auth;
use App\Models\Transection;
use App\Models\UserService;

class DashboardController extends Controller
{
    public function index()
    {
    	//$transactions = Transection::with('userService')->get()->toArray();
    	$user_services = UserService::where('user_id', Auth::user()->id)->with('transections')->with('user')->orderBy('service_status', 'asc')->get();
        $services = Service::whereIn('id', collect($user_services)->pluck('service_id'))->orderBy('name', 'asc')->get()->pluck('name', 'id');
    	/*foreach ($transactions as $transaction) {
            if($transaction['user_service']['user_id'] == Auth::user()->id){
                $date = new Carbon($transaction['date']);
                $date = $date->format('d-m-Y');
                $timeline_transactions[$date][] = $transaction;
            }
    	}*/
        $today = Carbon::now()->format('Y-m-d');
        $after_month = Carbon::now()->addDay(30)->format('Y-m-d');
        $expiring_services = UserService::where('user_id', Auth::user()->id)->where('expire_date','>=',$today)->where('expire_date','<=',$after_month)->get()->toArray();

        $pnl_all = (new ServiceDeliveryRepository())->pNLSummery($services, Auth::user()->id);
        return view('front_new.dashboard', compact( 'user_services', 'expiring_services', 'pnl_all'));
    }
}
