<?php

namespace App\Http\Controllers\Front;

use App\Notifications\AlertNotify;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DB;
use Carbon;
use App\Models\Transection;
use App\Models\UserService;
use App\User;
use Auth;

class UserController extends Controller
{
    public function viewProfile()
    {
    	$users = User::orderBy('name', 'asc')->get()->pluck('name', 'id');
    	$user = User::where('id', Auth::user()->id)->with('customerMeta')->get()->first()->toArray();
    	$no_menu = true;
    	return view('front_new.users.view_profile', compact('user', 'users', 'no_menu'));
    }

    public function edit(Request $request)
    {
    	$user = User::findOrFail(Auth::user()->id);
    	if ($request->isMethod('post')) {
			$input = $request->all();
			$result = $user->fill($input)->save();
		  	return redirect(route('front::users.viewProfile'));
		}
    	return view('front.users.edit', compact('user'));
    }

    public function changePassword(Request $request)
    {
    	$user = User::findOrFail(Auth::user()->id);
    	if ($request->isMethod('post')) {
	    	$error = array();
	    	if (!Hash::check($request->get('current_password'), Auth::user()->password)) {
			    $error[] = 'Current Password is Wrong';
			}
			if($request->get('new_password') != $request->get('repeat_new_password')){
				$error[] = 'Confirm Password Dosn\'t Match';	
			}
			if(strlen($request->get('new_password')) < 6){
				$error[] = 'Password Must be Atlease 6 Character Long';	
			}
			if(!empty($error)){
				return view('front.users.change_password', compact('user', 'error'));
			} else {
				$input = [
					'password' => bcrypt($request->get('new_password')),
				];
				$result = $user->fill($input)->save();
				if($result){
					$flash = 'Password Changed Successfully';
				} else {
					$flash = 'Password Change Failed';
				}
				return view('front.users.view_profile', compact('user','flash'));
			}
		}
    	return view('front.users.change_password', compact('user'));
    }

    public function messages(Request $request)
    {
        dd(Auth::user()->salutation);
        /*$notifications = Auth::user()->notifications;
        dd($notifications);*/

        return view('front_new.users.messages');
    }
    public function notifications(Request $request) {
        if($request->has('unread')) {
            if($request->get('unread'))
                $notifications =  Auth::user()->unreadNotifications();
            else
                $notifications =  Auth::user()->readNotifications();
        }
        if($request->has('search')){

        }

        return $notifications->get();
    }
}