<?php

namespace App\Http\Controllers\Front;

use App\Repositories\ServiceDelivery\ServiceDeliveryRepository;
use App\Service;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Carbon;
use Auth;
use App\Models\Transection;
use App\Models\UserService;
use App\ServiceDeliveryUser;
use Excel;

class ServiceController extends Controller
{
    public function indexUserServices()
    {
    	$user_services = UserService::where('user_id', Auth::user()->id)->with('transections')->with('user')->get();
    	return view('front.services.user_services_index', compact('user_services'));
    }

    public function viewUserServices($id)
    {
    	$user_service = UserService::with('transections')->with('user')->with('service')->with('plan')->with('transcript_file')->with('kyc_file')->with('user_service_action')->where('user_id', Auth::user()->id)->findOrFail($id)->toArray();
        if(count($user_service['user_service_action']) > 0){
            foreach ($user_service['user_service_action'] as $key => $value) {
                $user = DB::table('users')->where('id', $value['action_user_id'])->get()->first();
                $user_service['user_service_action'][$key]['user'] = $user;
            }
        }
        $taxes = DB::table('configurations')->where('type_id', 1)->get()->pluck('name', 'id')->toArray();
        $no_menu = true;
    	return view('front_new.services.user_service_view', compact('user_service', 'taxes', 'no_menu'));
    }

    public function profitAndLoss(Request $request)
    {
        $delivery_range = ["startdate" => date('Y-m-d', strtotime('1 year ago')), "enddate" => date('Y-m-d', strtotime('today'))];
        /*if($request->has('startdate')) {
            $default_delivery_range = $request->get('delivery_range');
        }*/
        if($request->has('startdate')) {
            $delivery_range['startdate'] = date('Y-m-d', strtotime($request->get('startdate')));
        }
        if($request->has('enddate')) {
            $delivery_range['enddate'] = date('Y-m-d', strtotime($request->get('enddate')));
        }
        $delivery_range =  array_values($delivery_range);

        $default_service = [];
        if($request->has('service')) {
            $default_service = (array) $request->get('service');
        }

        $service_ids = User::select([ 'user_services.service_id'])
            ->where('users.id',auth()->user()->id)
            ->leftJoin('user_services', 'users.id', '=', 'user_services.user_id')
            ->orderBy('users.id', 'desc')->get()->pluck('service_id');

        $services = Service::whereIn('id', $service_ids)->orderBy('name', 'asc')->get()->pluck('name', 'id');

        $pnl_services = (new ServiceDeliveryRepository())->deliveryReportbyService($default_service, $delivery_range, $services, true);


        if($request->has('download')){
            $data[] = [
                'Sr. No.',
				'Service Name',
				'Available limit',
				'Total calls',
				'Open Calls',
				'Close Calls',
				'Positive Calls',
				'Wining % ',
				'Open P&L',
				'Realized P&L',
				'Total P&L',
                "Today's P&L",
                'Gross Contribution of winning calls',
                'Gross Contribution of Losing calls',
                'Cumulative Returns from all calls',
                'Reward Vs Risk ',
                'Average months Position held',
            ];
            $i = 1;
            foreach($pnl_services as $service)
            {
                $data[] = [
                    $i++,
                    $service['service_name'],
                    100 - $service['user'][auth()->user()->id]['avail_qty']. "%",
                    $service['total_call'],
                    $service['open_call'],
                    $service['close_call'],
                    $service['positive_call'],
                    $service['winning_percent']. "%",
                    $service['open_profit'],
                    $service['booked_profit'],
                    $service['total_profit'],
                    $service['today_pnl'],
                    $service['gross_profit']. "%",
                    $service['gross_loss']. "%",
                    $service['cumulative_return_all_call']. "%",
                    $service['r_v_r'],
                    $service['months_pos'],
                ];

            }
            
            return Excel::create('pnl_'.time(), function($excel) use($data)  {
                $excel->sheet('Service', function($sheet) use($data) {
                    $sheet->fromArray($data, null, 'A1', false, false);
                });
            })->download('csv');
        }

        $dates = [
            'last_year' => [(new Carbon('first day of january last year')), (new Carbon('last day of december last year'))],
            'this_year' => [(new Carbon('first day of january this year')), (new Carbon('last day of december this year'))],
            'last_month' => [(new Carbon('first day of last month')), (new Carbon('last day of last month'))],
            'this_month' => [(new Carbon('first day of this month')), (new Carbon('last day of this month'))],
            'this_quarter' => [(new Carbon('-3 months'))->firstOfQuarter(), (new Carbon('now'))->lastOfQuarter()],
        ];

        return view('front_new.services.profit_and_loss', compact('services', 'pnl_services', 'default_delivery_range', 'default_service', 'dates'));
    }

    public function profitAndLossStock(Request $request)
    {
        $delivery_range = ["startdate" => date('Y-m-d', strtotime('1 year ago')), "enddate" => date('Y-m-d', strtotime('today'))];
        /*if($request->has('startdate')) {
            $default_delivery_range = $request->get('delivery_range');
        }*/
        if($request->has('startdate')) {
            $delivery_range['startdate'] = date('Y-m-d', strtotime($request->get('startdate')));
        }
        if($request->has('enddate')) {
            $delivery_range['enddate'] = date('Y-m-d', strtotime($request->get('enddate')));
        }
        $delivery_range =  array_values($delivery_range);

        $default_service = [];
        if($request->has('service')) {
            $default_service = (array) $request->get('service');
        }

        $default_calltype= 'all';
        if($request->has('call_type')) {
            $default_calltype = $request->get('call_type');
        }

        $service_ids = User::select([ 'user_services.service_id'])
            ->where('users.id',auth()->user()->id)
            ->leftJoin('user_services', 'users.id', '=', 'user_services.user_id')
            ->orderBy('users.id', 'desc')->get()->pluck('service_id');

        $services = Service::whereIn('id', $service_ids)->orderBy('name', 'asc')->get()->pluck('name', 'id');

        $pnl_stocks = (new ServiceDeliveryRepository())->deliveryReport($default_service, $delivery_range, 'stock', true, $default_calltype);
        $symbols = collect($pnl_stocks)->pluck('symbol', 'symbol');

        if($request->has('symbols') && count($request->get('symbols')) > 0)
        {
            $pnl_stocks = collect($pnl_stocks)->whereIn('symbol', $request->get('symbols'))->all();
        }


        if($request->has('download')){
            $data[] = [
                'Sr. No.',
                'Stock Name',
                'Status',
                'Entry date',
                'Entry Price',
                'CMP / EXIT PRICE',
                'Open Profit',
                'Booked Profit',
                'Total Profit',
                'Overall Profit (%)',
                'Open Position / Stock Weight of Total Investment',
                'Net ROI',
            ];
            $i = 1;
            foreach($pnl_stocks as $stock)
            {
                $data[] = [
                    $i++,
                    $stock['symbol'] ,
                    $stock['status'] ,
                    $stock['entry_date'] ,
                    $stock['avg_price'] ,
                    $stock['exit_price'] ,
                    $stock['open_profit'] ,
                    $stock['booked_profit'] ,
                    $stock['total_profit'] ,
                    $stock['profit_percent'] ."%",
                    $stock['open_stock_weight'] ."%",
                    $stock['net_roi'] ."%",
                ];

            }

            return Excel::create('pnl_service_stocks_'.time(), function($excel) use($data)  {
                $excel->sheet('Stock', function($sheet) use($data) {
                    $sheet->fromArray($data, null, 'A1', false, false);
                });
            })->download('csv');
        }

        $dates = [
            'last_year' => [(new Carbon('first day of january last year')), (new Carbon('last day of december last year'))],
            'this_year' => [(new Carbon('first day of january this year')), (new Carbon('last day of december this year'))],
            'last_month' => [(new Carbon('first day of last month')), (new Carbon('last day of last month'))],
            'this_month' => [(new Carbon('first day of this month')), (new Carbon('last day of this month'))],
            'this_quarter' => [(new Carbon('-3 months'))->firstOfQuarter(), (new Carbon('now'))->lastOfQuarter()],
        ];
        return view('front_new.services.profit_and_loss_stock', compact('services', 'pnl_stocks', 'default_delivery_range', 'default_service', 'dates', 'symbols'));
    }

    public function indexServiceCalls(Request $request)
    {
        $services = Service::get()->pluck('name', 'id');
        $service_calls = ServiceDeliveryUser::with(['user', 'plan', 'sd','service'])->where('user_id', Auth::user()->id)->orderBy('id', 'Desc');
        if($request->has('startdate')){
            $service_calls = $service_calls->where('created_at', '>=', Carbon::parse($request->get('startdate'))->format('Y-m-d h:i:s'));
        }
        if($request->has('enddate')){
            $service_calls = $service_calls->where('created_at', '<=', Carbon::parse($request->get('enddate'))->format('Y-m-d h:i:s'));
        }
        if($request->has('symbol')){
            $service_calls = $service_calls->where( 'symbol', 'LIKE', '%'.$request->get('symbol').'%');
        }
        if($request->has('service')){
            $service_calls = $service_calls->whereHas('service', function ($query) use ($request) {
                $query->where('id', $request->get('service'));
            });
        }
        if($request->has('call_type')){
            $service_calls = $service_calls->where( 'call_type', $request->get('call_type'));
        }
        $service_calls = $service_calls->get();
        $dates = [
            'last_year' => [(new Carbon('first day of january last year')), (new Carbon('last day of december last year'))],
            'this_year' => [(new Carbon('first day of january this year')), (new Carbon('last day of december this year'))],
            'last_month' => [(new Carbon('first day of last month')), (new Carbon('last day of last month'))],
            'this_month' => [(new Carbon('first day of this month')), (new Carbon('last day of this month'))],
            'this_quarter' => [(new Carbon('-3 months'))->firstOfQuarter(), (new Carbon('now'))->lastOfQuarter()],
        ];
        return view('front_new.services.service_calls', compact('service_calls', 'dates', 'services'));
    }

    public function paymentHistory()
    {
        $user_services = UserService::with('transections')->where('user_id', Auth::user()->id)->get()->toArray();
        if(count($user_services) > 0){
            foreach ($user_services as $key => $user_service) {
                $transactions = $user_service['transections'];
                foreach ($transactions as $transaction) {
                    $date = new Carbon($transaction['date']);
                    $date = $date->format('d-m-Y');
                    $user_services[$key]['timeline_transactions'][$date][] = $transaction;
                }
            }
        }
        return view('front_new.services.payment_history', compact('user_services'));
    }
}