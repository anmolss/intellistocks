<?php

namespace App\Http\Controllers;

use App\Events\TestNotify;
use App\Notifications\TestNote;
use App\User;
use Illuminate\Http\Request;
use Redis;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'test_notify']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function test_notify(Request $request)
    {
        $user  = User::first();
        $user->notify(new TestNote($request->all()));

        broadcast(new TestNotify($request->all()));

        dd($request->all());
    }

    public function nonMigratedUsers()
    {
        $a = json_decode('[{"title":"Mr.","name":"Rajesh Mulchandani","mobile":"2087040002","email":"","address1":"","city":"","zip_code":"","other_emails":",services@rothmanscapital.com","other_mobiles":"","password":"$2y$10$7o6NJjtWk3GCWn.MDCYmOerq06uzZW4wL5\/ff5JM92OnPYrP5JUtO","user_type":"Customer"},{"title":"Mr.","name":"RCJ RCJ","mobile":"9417500951","email":"","address1":"","city":"","zip_code":"","other_emails":",services@rothmanscapital.com","other_mobiles":"","password":"$2y$10$urTiNYHCL3fApsjnvyRhPOnIUY1kyG2y8IWwUqx5Mi6j.daDQIg4u","user_type":"Customer"},{"title":"Mr.","name":"Arun Poojari","mobile":"","email":"arunpoojari@hotmail.com","address1":"","city":"","zip_code":"","other_emails":"veenapoojari@live.com","other_mobiles":",1234567890","password":"$2y$10$ASHRBzkS1\/xBxvvMz1Oby.4raJiASrfesTMHZOvj8STIHawE8WVXG","user_type":"Customer"},{"title":"","name":"Arun Poojari","mobile":"","email":"veenapoojari@live.com","address1":"","city":"","zip_code":"","other_emails":"","other_mobiles":"","password":"$2y$10$vN58CsJ3ixQnRtrPhJXzRuc2MDqBUWqs2rpOEKsnMeKS.yR9a3TAu","user_type":"Customer"},{"title":"","name":"Sanjay garg","mobile":"9810218083","email":"","address1":"","city":"","zip_code":"","other_emails":",sirsaassociates@gmail.com","other_mobiles":"","password":"$2y$10$GY2DQr.uwfAKvOZvn6VTbu7w6C55q1RTSC8s2Z\/Pbf7LmK8SdtNJO","user_type":"Customer"},{"title":"","name":"Dinesh Jain","mobile":"","email":"dk_jain08@yahoo.co.in","address1":"","city":"","zip_code":"","other_emails":"","other_mobiles":",9319702180","password":"$2y$10$CuJfDsEZ0zfq.fA2x6tC0.rDEIr4r3jw7\/kTl06Ew6c.LmqAETeLy","user_type":"Customer"},{"title":"","name":"Ganesh Kale","mobile":"","email":"ganeshkale042@gmail.com","address1":"","city":"","zip_code":"","other_emails":"","other_mobiles":",7709035090","password":"$2y$10$Fu2ImKWh.EYYYizsTHUQCeiStd.x3ETTTxH\/cOCnFML0N.q8rUMJO","user_type":"Customer"},{"title":"Mr.","name":"Rajesh Mulchandani","mobile":"9999999999","email":"","address1":"","city":"","zip_code":"","other_emails":",services@rothmanscapital.com","other_mobiles":"","password":"$2y$10$9y4ivLFc4VlhXmFYo3kAweRI4TgxcQtINHK\/7udbMYPf6iTwvnoc.","user_type":"Customer"},{"title":"Mr.","name":"test test","mobile":"9638527410","email":"","address1":"test","city":"Bombuflat","zip_code":"110092","other_emails":",mani@cozyvision.com","other_mobiles":"","password":"$2y$10$ebHN36Ktbt0WFyVsYRhL3ehNEvrIUxSayxYMy7ezG2JOnNas69Ovm","user_type":"Customer"},{"title":"Mr.","name":"James Bond","mobile":"9944","email":"","address1":"valid address","city":"Abohar","zip_code":"","other_emails":",test@yopmail.com","other_mobiles":"","password":"$2y$10$WCMRXD2SH1lhIuukdoh.g.F\/OANazREYbC66eWZVdoj0N57j0QizG","user_type":"Customer"},{"title":"Miss.","name":"Manasvi Bansal","mobile":"","email":"manasvibansal29@gmail.com","address1":"","city":"","zip_code":"","other_emails":"","other_mobiles":",9654727358","password":"$2y$10$5euV.3n9D9JIZ.tgscO0ouBHRC0oVs5POlJGhyMg8eChvnQNws0WW","user_type":"Customer"},{"title":"Mr.","name":"Dhruv  Hingorani ","mobile":"8802312372","email":"","address1":"","city":"Gurgaon","zip_code":"","other_emails":"hingoranidhruv@gmail.com,services@rothmanscapital.com","other_mobiles":"","password":"$2y$10$UPPKlbzn9Y5705v7GhYSV.ixUR7WYcKA15hW0CU5.T3J3OP7rEMXW","user_type":"Customer"}]', true);
        return view('non_migrated', compact('a'));
    }

    public function migrateUsers()
    {
        $old_users = DB::table('customers')->select('title', DB::raw('CONCAT(firstname, " " , lastname) AS name'), 'number AS mobile', 'email', 'address AS address1', 'city', 'pincode AS zip_code')->get();
        $senitized = $old_users->map(function ($item, $key) {
            if ( strpos( $item->email, ',' ) !== false ) {
                $emails = explode(',', $item->email);
                $item->email = $emails[0];
                unset($emails[0]);
                $item->other_emails = (count($emails) > 1)?implode(',', $emails):$emails[1];
            } else {
                $item->other_emails = '';
            }

            if ( strpos( $item->mobile, ',' ) !== false ) {
                $mobiles = explode(',', $item->mobile);
                $item->mobile = $mobiles[0];
                unset($mobiles[0]);
                $item->other_mobiles = (count($mobiles) > 1)?implode(',', $mobiles):$mobiles[1];
            } else {
                $item->other_mobiles = '';
            }
            return $item;
        });
        $not_inserted = [];
        foreach ($senitized as $key => $value) {
            $user_mobile = DB::table('users')->where('mobile', $value->mobile)->first();
            $user_email = DB::table('users')->where('email', $value->email)->first();
            if ($user_mobile) {
                $value->other_mobiles = $value->other_mobiles.','.$value->mobile;
                $value->mobile = '';
            }
            if ($user_email) {
                $value->other_emails = $value->other_emails.','.$value->email;
                $value->email = '';
            }
            $value->password = bcrypt('asd@123');
            $value->user_type = 'Customer';
            if(!empty($value->mobile) && !empty($value->email)){
                DB::table('users')->insert(json_decode(json_encode($value), true));
            } else {
                $not_inserted[] = $value;
            }
        }
        dd(json_encode($not_inserted));
    }
}
