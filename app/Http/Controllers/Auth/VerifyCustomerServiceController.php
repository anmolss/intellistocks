<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use DB;
use App\Models\UserService;
use App\User;
use Request;
use Carbon;

class VerifyCustomerServiceController extends Controller
{
    
    public function __construct()
    {
        //
    }

    public function verifyService(Request $request, $verification_code)
    {
        $user = User::where('verification_code', '=' ,$verification_code)->get()->first();
        if(!empty($user)){
            if (!empty($user->consent_ip) && $user->consent_status == 'Yes') {
                $status = '4';   
            } else {
                $input = array(
                    'consent_status' => 'Yes',
                    'consent_ip' => Request::ip()
                );

                if($user->transcript_status == 'Yes' && $user->kyc_status == 'Yes'){
                    $services = DB::table('user_services')->where('user_id', $user->id)->where('service_status', 'Pending')->get()->toArray();
                    if(!empty($services)){
                        foreach ($services as $service) {
                            if(strtotime($service->activation_date) <= strtotime(Carbon::now()->format('Y-m-d'))){
                                DB::table('user_services')->where('id', $service->id)->update(['service_status' => 'Active']);
                            }
                        }
                    }
                }
                
                if($user->fill($input)->save()){
                    $status = '1';
                } else {
                    $status = '2';
                }
            }
        } else {
            $status = '3';
        }

        return view('activation')->withStatus($status);
    }
}