<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use DB;
use App\Models\UserService;
use Request;
use Carbon;

class CronController extends Controller
{
    
    public function __construct()
    {
        //
    }

    public function run()
    {
        $crons = DB::table('crons')->where('next_run', '<=', Carbon::now()->format('Y-m-d H:i:s'))->where('status', '=', 'pending')->get();
        if(!empty($crons)){

            foreach ($crons as $cron) {

                switch ($cron->type) {
                    case 'renewService':

                        DB::table('crons')->where('id', $cron->id)->update(['last_run' => Carbon::now()->format('Y-m-d H:i:s'), 'status' => 'running']);
                        $next_run = $this->renewService();
                        DB::table('crons')->where('id', $cron->id)->update(['next_run' => $next_run, 'status' => 'pending']);
                    
                        break;

                    default:
                        return false;
                        break;
                }

            }

        }
    }


    public function renewService()
    {
        $services = UserService::where('service_status', 'Renewed')->where('activation_date', '<=', Carbon::now()->format('Y-m-d'))->get();
        
        if (!empty($services)) {
            foreach ($services as $service) {
                DB::table('user_services')->where('id', $service->id)->update(['service_status' => 'Active']);    
            }
        }
        
        return Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
    }
}