<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Notifications\LoggedIn;
use Illuminate\Support\Facades\Redirect;
use App\Models\Logs;
use Auth;
use Flash;
use Lang;
use Carbon;
use Config;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        return view('admin.users.login');
    }

    public function login(Request $request)
    {
        // get our login input
        $login = $request->get('login');

        foreach (Config::get('rothmans.bypass_emails') as $email) {
            if (str_is( $login, $email) ) {
                Config::set('rothmans.developers', true);
            }
        }

        // check login field
        $login_type = filter_var( $login, FILTER_VALIDATE_EMAIL ) ? 'email' : 'mobile';

        // merge our login field into the request with either email or username as key
        $request->merge([ $login_type => $login ]);
        $request->merge([ 'user_type' => 'User' ]);
        $request->merge([ 'status' => '1' ]);
        // let's validate and set our credentials
        if ( $login_type == 'email' ) {
            $this->validate($request, [
                'login'    => 'required|email',
                'password' => 'required',
            ]);
            $credentials = $request->only( 'email', 'password', 'user_type');
        } else {
            $this->validate($request, [
                'login' => 'required|digits:10',
                'password' => 'required',
            ]);
            $credentials = $request->only( $login_type , 'password' );
        }

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
            return $this->sendLoginResponse($request);
        } elseif ( $login_type == 'email' ) {
            $request->merge([ 'official_emailid' => $login ]);
            $credentials = $request->only( 'official_emailid', 'password' );
            if ($this->guard()->attempt($credentials, $request->has('remember'))) {
                return $this->sendLoginResponse($request);
            }
        }

        $this->incrementLoginAttempts($request);

        return redirect('/login')
            ->withInput($request->only('login', 'remember'))
            ->withErrors([
                'login' => Lang::get('auth.failed')
            ]);
    }

    protected function sendLoginResponse(Request $request)
    {
        $time = Carbon::now(); 
         $activityinput = array_merge(
            ['text' => Auth::user()->name . ' login at '.$time->toDayDateTimeString(),
            'user_id' => Auth()->id(),
            'type' => 'user']
        );
        Logs::create($activityinput);

        if($this->guard()->user()->hasRole('customer')){
            $redirectTo = route('front::dashboard');
        } else {
            $redirectTo = route('admin::dashboard');
        }

        $request->session()->regenerate();
        $this->clearLoginAttempts($request);
        Flash::success('User logged in successfully!');
        
        return $this->authenticated($request, $this->guard()->user()) ? : Redirect::to($redirectTo)->header('Cache-Control', 'no-store, no-cache, must-revalidate'); //redirect()->intended($redirectTo)->header('Cache-Control', 'no-store, no-cache, must-revalidate'); before it was $this->redirectPath()   
    }

    public function logout()
    {  
        $user = Auth::user();
        $time = Carbon::now(); 
         $activityinput = array_merge(
            ['text' => Auth::user()->name . ' logout at '.$time->toDayDateTimeString(),
            'user_id' => Auth()->id(),
            'type' => 'user']
        );

        Logs::create($activityinput);
        Auth::logout();
        //Notification
        //$user->notify(new LoggedIn($user));
        Flash::success('success', 'You have been successfully logged out!');
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }


}
