<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use View;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $relation_manager;
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->relation_manager = DB::table('user_client_meta')->where('user_client_meta.fk_user_id', Auth::user()->id)->join('users', 'users.id', '=', 'user_client_meta.fk_relation_manager_id')->get()->first();
            View::share('relation_manager', $this->relation_manager);
            return $next($request);
        });
    }
}
