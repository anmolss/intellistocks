<?php

namespace App\Http\Middleware;

use App\User;
use Auth;
use Closure;
use Config;
use Flash;

class IpRestriction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Config::has('rothmans.bypass_emails') && Auth::check())
        {
            foreach (Config::get('rothmans.bypass_emails') as $email) {
                if (str_is( auth()->user()->email, $email) ) {
                    return $next($request);
                }
            }
        }
        $ips = User::getAllowedIps();

        if (count($ips) > 0 && !in_array($request->ip(), $ips))
        {
            if(Auth::check())
            {
                Auth::logout();
            }

            return redirect('/login')
                ->withErrors([
                'login' => 'You are unauthorized to access this info from this location.'
            ]);;
        }

        return $next($request);
    }
}
