<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $admin )
    {
        if(Auth::check() /*&& Auth::user()->is_admin == $admin*/){
            return $next($request);
        } else if(!Auth::check()) {
            return redirect()->guest('login');
        }

        return redirect('/login');
    }
}
