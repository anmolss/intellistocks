<?php

namespace App\Http\Middleware;

use Closure;
use Menu;

class MenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('MainMenu', function($m) {
            $m->add('Dashboard', ['route' => "admin::dashboard"])->data('icon' , 'icon-home4');

            if(auth()->check() && auth()->user()->can(['create-users', 'edit-users']))

            {
                $m->add('Employees', '#')->data('icon', 'icon-people');
                $m->employees->add('List Employee', ['route' => "admin::users.index"]);
            }

            if(auth()->check() && auth()->user()->can(array('manage-client','manage-customer')))
            {
                $m->add('Clients', '#')->data('icon' , 'icon-user');
                $m->clients->add('List Clients', ['route' => "admin::clients.customers"]);
                $m->clients->add('Add New Client', ['route' => "admin::clients.createDirect"]);

            }

            if(auth()->check() && auth()->user()->can('manage-leads'))
            {
                $m->add('Leads', '#')->data('icon' , 'icon-graph');
                $m->leads->add('List Leads', ['route' => "admin::leads.index"]);
                $m->leads->add('List Persons', ['route' => "admin::clients.index"]);
                $m->leads->add('Add New Person', ['route' => "admin::clients.create"]);
            }

            if(auth()->check() && auth()->user()->can('manage-services'))
            {
                $m->add('Services', '#')->data('icon' , 'icon-magazine');
                $m->services->add('List All Services', ['route' => "admin::services.index"]);
                $m->services->add('Add New Service', ['route' => "admin::services.create"]);
                //$m->services->add('Plans', ['route' => "admin::plans.index"]);

                //$m->services->add('Edit or Recall', ['route' => "admin::service_delivery.adjust"]);

                $m->services->add('Service Delivery', ['route' => "admin::service_delivery.index"]);

                //$m->services->add('Services User List', ['route' => "admin::service_delivery.userlist"]);

                /*Changed by Metazone
                  Date : 28-September-2017
                  Comment : Temporarily Commented
                */
                /*$m->services->add('P&L', ['route' => "admin::pnl.service"]);
                $m->services->add('User Service List', ['route' => "admin::services.active_services"]);*/


                /*$m->services->add('P&L', ['nickname' => 'pnl_menu']);
                $m->pnl_menu->add('Service Based', ['route' => "admin::pnl.service"]);
                $m->pnl_menu->add('Stock Based', ['route' => "admin::pnl.stock"]);*/

            }

            if(auth()->check() && auth()->user()->can('manage-roles'))
            {
                $m->add('Roles', '#')->data('icon' , 'icon-user-check');
                $m->roles->add('List Roles', ['route' => "admin::roles.index"]);
                $m->roles->add('Add New Role', ['route' => "admin::roles.create"]);

                $m->add('Permissions', '#')->data('icon' , 'icon-user-lock');
                $m->permissions->add('List Permissions', ['route' => "admin::permissions.index"]);
                //$m->permissions->add('Add New Permission', ['route' => "admin::permissions.create"]);
            }

            if(auth()->check() && auth()->user()->can('manage-teams'))
            {
                $m->add('Teams', '#')->data('icon', 'icon-users4');
                $m->teams->add('List Team', ['route' => "admin::teams.index"]);
                if(auth()->check() && auth()->user()->can('create-teams')) {
                    $m->teams->add('Add New Team', ['route' => "admin::teams.create"]);
                }
            }


            if(auth()->check() && auth()->user()->can('manage-departments'))
            {
                $m->add('Departments', '#')->data('icon' , 'icon-grid6');
                $m->departments->add('List Departments', ['route' => "admin::departments.index"]);
                $m->departments->add('Add New Departhment', ['route' => "admin::departments.create"]);
            }

            if(auth()->check() && auth()->user()->can('manage-designations'))
            {
                $m->add('Designation', '#')->data('icon' , 'icon-grid6');
                $m->designation->add('List Designation', ['route' => "admin::designations.index"]);
                $m->designation->add('Add New Designation', ['route' => "admin::designations.create"]);
            }

            if(auth()->check() && auth()->user()->can('manage-settings'))
            {
                $m->add('Settings', '#')->data('icon', 'icon-gear');
                $m->settings->add('Ip Restriction', ['url' => route('admin::manage_ip.show', ['type'=>'global'])]);
                //$m->settings->add('Token', ['route' => "admin::tokens.index"]);
                $m->settings->add('Template', ['route' => "admin::templates.index"]);
                $m->settings->add('Stock Watchlist', ['route' => "admin::stock_watchlist.index"]);
                $m->settings->add('Stock Alert', ['route' => "admin::stock_alert.index"]);
            }

            /*if(auth()->check() && auth()->user()->can('developer'))
            {
                $m->add('Portfolio Template', ['route' => "admin::portfolios.create"]);
                $m->add('Activities', ['url' => route('admin::users.activities')]);
                $m->add('Company List',['route'=>"admin::company.index"]);
            }*/

        });

        Menu::make('CustomerFrontMenu', function($m) {
            $m->add('', ['route' => "front::dashboard"])->data('icon' , 'icon-grid2');
            $m->add('My Services', ['route' => "front::services.userServiceIndex"])->data('icon', '');
            $m->add('Service Calls', ['route' => "front::services.ServiceCallIndex"])->data('icon', '');
            $m->add('My Profile', ['route' => "front::users.viewProfile"])->data('icon', '');
            $m->add('Profit & Loss', ['route' => "front::services.profitAndLoss"])->data('icon', '');
            /*$m->add('Profit & Loss Stock', ['route' => "front::services.profitAndLossStock"])->data('icon', '');*/
        });

        return $next($request);
    }
}
