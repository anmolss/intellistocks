<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceDeliveryService extends Model
{
    protected $table = 'service_delivery_service';

    protected $fillable = [
        'service_id','plan_id','service_delivery_id', 'qty', 'lot_size', 'value', 'template_id'
    ];

    public function service(){
        return $this->belongsTo(Service::class, 'service_id');
    }

    public function plan(){
        return $this->belongsTo(Plan::class, 'plan_id');
    }

    public function sd(){
        return $this->belongsTo(ServiceDelivery::class, 'service_delivery_id');
    }
}
