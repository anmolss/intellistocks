<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyMaster extends AppModel
{
    protected $primaryKey = 'id';
    protected $dates = ['board_meet_date'];

    protected $casts = [
        'board_meet_date' => 'date_format',
    ];

    protected $foreign_key = 'company_id';

    public function CompanyNameChange()
    {
        return $this->hasOne('App\CompanyNameChange', $this->foreign_key);
    }

    public function AnnualGeneralMeeting()
    {
        return $this->hasMany('App\AnnualGeneralMeeting', $this->foreign_key);
    }

    public function Auditor()
    {
        return $this->hasMany('App\Auditor', $this->foreign_key);
    }

    public function BackgroundInfo()
    {
        return $this->hasOne('App\BackgroundInfo', $this->foreign_key);
    }

    public function Banker()
    {
        return $this->hasMany('App\Banker', $this->foreign_key);
    }

    public function BoardMeeting()
    {
        return $this->hasMany('App\BoardMeeting', $this->foreign_key)->orderBy('board_meet_date','desc');
    }

    public function Bonuse()
    {
        return $this->hasMany('App\Bonuse', $this->foreign_key);
    }

    public function CapitalStructure()
    {
        return $this->hasMany('App\CapitalStructure', $this->foreign_key);
    }

    public function ConsolidatedNinemonthsResult()
    {
        return $this->hasMany('App\ConsolidatedNinemonthsResult', $this->foreign_key);
    }

    public function ConsolidatedHalfyearlyResult()
    {
        return $this->hasMany('App\ConsolidatedNinemonthsResult', $this->foreign_key);
    }

    public function ConsolidatedQuarterlyResult()
    {
        return $this->hasMany('App\ConsolidatedQuarterlyResult', $this->foreign_key);
    }

    public function CurrentData()
    {
        return $this->hasOne('App\CurrentData', $this->foreign_key);
    }

    public function Dividend()
    {
        return $this->hasMany('App\Dividend', $this->foreign_key);
    }

    public function FinanceBanking()
    {
        return $this->hasMany('App\FinanceBanking', $this->foreign_key);
    }

    public function KeyExecutive()
    {
        return $this->hasMany('App\KeyExecutive', $this->foreign_key);
    }

    public function Location()
    {
        return $this->hasMany('App\Location', $this->foreign_key);
    }

    public function ManagementTeam()
    {
        return $this->hasMany('App\ManagementTeam', $this->foreign_key);
    }

    public function NinemonthsResult()
    {
        return $this->hasMany('App\NinemonthsResult', $this->foreign_key);
    }

    public function Product()
    {
        return $this->hasMany('App\Product', $this->foreign_key);
    }

    public function QuarterlyResult()
    {
        return $this->hasMany('App\QuarterlyResult', $this->foreign_key);
    }

    public function RatiosBanking()
    {
        return $this->hasMany('App\RatiosBanking', $this->foreign_key);
    }

    public function RatiosConslidatedNonBanking()
    {
        return $this->hasMany('App\RatiosConslidatedNonBanking', $this->foreign_key);
    }

    public function RatiosNonBanking()
    {
        return $this->hasMany('App\RatiosNonBanking', $this->foreign_key);
    }

    public function RawMaterial()
    {
        return $this->hasMany('App\RawMaterial',  $this->foreign_key);
    }

    public function Registrar()
    {
        return $this->hasOne('App\Registrar',  $this->foreign_key);
    }

    public function Right()
    {
        return $this->hasMany('App\Right',  $this->foreign_key);
    }

    public function SecurityMaster()
    {
        return $this->hasOne('App\SecurityMaster',  $this->foreign_key);
    }

    public function ListingMaster()
    {
        return $this->hasMany('App\ListingMaster', 'security_code');
    }

    public function ShareHolding()
    {
        return $this->hasMany('App\ShareHolding', $this->foreign_key);
    }

    public function Split()
    {
        return $this->hasOne('App\Split',  $this->foreign_key);
    }

    public function Subsidiarie()
    {
        return $this->hasMany('App\Subsidiarie', $this->foreign_key);
    }

    public function scopeFullDetail($query)
    {
        return $query->with([
            'AnnualGeneralMeeting',
            'CompanyNameChange',
            'Auditor',
            'BackgroundInfo',
            'Banker',
            'BoardMeeting',
            'CapitalStructure',
            'ConsolidatedNinemonthsResult',
            'ConsolidatedHalfyearlyResult',
            'ConsolidatedQuarterlyResult',
            'CurrentData',
            'Dividend',
            'FinanceBanking',
            'KeyExecutive',
            'Location',
            'ManagementTeam',
            'NinemonthsResult',
            'Product',
            'QuarterlyResult',
            'RatiosBanking',
            'RatiosConslidatedNonBanking',
            'RatiosNonBanking',
            'RawMaterial',
            'Registrar',
            'Right',
            'ListingMaster',
            'SecurityMaster',
            'ShareHolding',
            'Split',
            'Subsidiarie',
            'Bonuse'
        ]);
    }
}