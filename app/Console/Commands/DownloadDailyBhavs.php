<?php

namespace App\Console\Commands;

use Carbon\CarbonInterval;
use Illuminate\Console\Command;
use Carbon\Carbon;
use ZipArchive;

use DB;

class DownloadDailyBhavs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:dailybhav {--date=} {--exchange=} {--duration=} {--start_date=} {--end_date=} {--remove_files=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'download daily bhav from all the stocks: * date format : d-m-Y';

    var $last_access;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->last_access = Carbon::now();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(!$this->option('date')) {
            $date = Carbon::now();
        } else {
            $date = Carbon::parse($this->option('date'));
        }


        if($this->option('duration')) {
            $start_date = Carbon::parse($this->option('duration'))->gt(Carbon::today()) ? Carbon::today() : Carbon::parse($this->option('duration'));
            $end_date = Carbon::parse($this->option('duration'))->lt(Carbon::today()) ? Carbon::today() : Carbon::parse($this->option('duration'));
            $dates = $this->date_range($start_date, $end_date);
            $bar = $this->output->createProgressBar(count($dates));
            foreach($dates as $date) {
                $this->setupBhav($date);
                $bar->advance();
            }
            $bar->finish();
        } else if($this->option('start_date') && $this->option('end_date')){
            $dates = $this->date_range(Carbon::parse($this->option('start_date')), Carbon::parse($this->option('end_date')));
            $bar = $this->output->createProgressBar(count($dates));
            foreach($dates as $date) {
                $this->setupBhav($date);
                $bar->advance();
            }
            $bar->finish();
        }else {
            $this->setupBhav($date);
        }


    }

    function date_range(Carbon $from, Carbon $to, $inclusive = true, $weekend = false)
    {
        if ($from->gt($to)) {
            return null;
        }

        // Clone the date objects to avoid issues, then reset their time
        $from = $from->copy()->startOfDay();
        $to = $to->copy()->startOfDay();

        // Include the end date in the range
        if ($inclusive) {
            $to->addDay();
        }

        $step = CarbonInterval::day();
        $period = new \DatePeriod($from, $step, $to);

        // Convert the DatePeriod into a plain array of Carbon objects
        $range = [];

        foreach ($period as $day) {
            $date = new Carbon($day);

            if(!$weekend && ($date->dayOfWeek === Carbon::SATURDAY || $date->dayOfWeek === Carbon::SUNDAY))
                continue;

            $range[] = $date;
        }

        $range = array_reverse($range);

        return ! empty($range) ? $range : null;
    }

    private function setupBhav(Carbon $date, $exchanges = null)
    {
        $history = DB::table('daily_bhav_history')->where('bhav_date',$date->format('Y-m-d'))->first();
        if(!$history) {
            DB::table('daily_bhav_history')->insert(
                ['bhav_date' => $date->toDateString(), 'created_at' => Carbon::now()->toDateTimeString()]
            );
        }

        $uris = $this->getBhavCopy($date, $this->option('exchange'));
        if($uris){
            foreach($uris as $key => $path) {
                $path = str_replace('\\', '/', $path);
                if(!$path)
                    continue;

                switch($key) {
                    case 'bse' :
                        $this->loadBseBhav($path, $date);
                        break;
                    case 'nse' :
                        $this->loadNseBhav($path, $date);
                        break;
                    case 'nfo' :
                        $this->loadNfoBhav($path, $date);
                        break;
                    case 'nfo_lot' :
                        $this->loadNfoLot($path, $date);
                        break;
                }
            }
        } else {
            echo 'no data for '.$date->format(config('rothmans.date_format'));
        }

    }

    private function getBhavCopy(Carbon $date, $exchanges = null)
    {
        if(!$exchanges) {
            $exchanges = ['bse', 'nse', 'nfo'];
            //$exchanges = ['nfo', 'nse'];
            if($date->isToday()) {
                $exchanges[] = 'nfo_lot';
            }
        } else {
            if(!is_array($exchanges)){
                $exchanges = (array) $exchanges;
            }
        }

        $result = [];

        foreach($exchanges as $exchange) {
            $month = strtoupper($date->format('M'));
            $d = $date->format('d');
            if($exchange == 'nse'){
                $url = 'https://www.nseindia.com/content/historical/EQUITIES/'.$date->year.'/'.$month.'/cm'.$d.$month.$date->year.'bhav.csv.zip';
            } else if($exchange == 'bse'){
                $url = 'http://www.bseindia.com/download/BhavCopy/Equity/EQ' . $date->format('dmy') . '.zip';

            } else if($exchange == 'nfo'){
                $url = 'https://www.nseindia.com/content/historical/DERIVATIVES/'.$date->year.'/'.$month.'/fo'.$d.$month.$date->year.'bhav.csv.zip';
            } else if($exchange == 'nfo_lot'){
                $url = 'https://www.nseindia.com/content/fo/fo_mktlots.csv';
            }

            $bhav_file = storage_path("daily_bhav/".$exchange."/$date->year/$month/$date->day/".basename($url));
            $csv_file = dirname($bhav_file).'/'.basename($bhav_file, '.zip');
            if($exchange == 'bse')
                $csv_file .= '.csv';

            $dirname = dirname($bhav_file);
            if (!is_dir($dirname))
            {
                mkdir($dirname, 0777, true);
            }

            if(is_file($csv_file)) {
                $result[$exchange] =  $csv_file;
                continue;
            }

            $file = fopen($bhav_file, 'w');

            $client = new \GuzzleHttp\Client();

            while(true) {
                if($exchange == 'bse' && $this->last_access->diffInMinutes(Carbon::now(), true) >= 1 ) {
                    sleep(10);
                    continue;
                } else if($exchange == 'bse') {
                    $this->last_access = Carbon::now();
                }

                try {
                    $client->head($url,[
                        'headers' => [
                            'User-Agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13',
                            'Accept'     => 'application/x-zip-compressed',
                            'Cache-Control' => 'max-age=0',
                            'Keep-Alive' => 300,
                            'Accept-Charset' => 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
                        ],
                        'verify' => true,
                        //'debug' => true,
                    ]);
                    break;
                } catch (\GuzzleHttp\Exception\ClientException $e) {
                    echo $e->getMessage();
                    if($e->getCode() == 404) {
                        $result[$exchange] =  false;
                        break;
                    }
                    break;
                }
            }
            if(isset($result[$exchange]) && $result[$exchange] === false) {
                continue;
            }

            try {
                $res = $client->get($url , [
                    'sink' => $file,
                    'headers' => [
                        'User-Agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13',
                        'Accept'     => 'application/x-zip-compressed',
                        'Cache-Control' => 'max-age=0',
                        'Keep-Alive' => 300,
                        'Accept-Charset' => 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
                    ],
                    'verify' => true,
                    //'debug' => true,
                ]);
                if($res->getStatusCode() != 200){
                    throw new \Exception('unable to download file, response: '.$res->getStatusCode());
                }

                if($exchange != 'nfo_lot' ) {
                    // Get The Zip File From Server
                    $zip = new ZipArchive;

                    if ($zip->open($bhav_file) != "true") {
                        echo "Error :- Unable to open the Zip File";
                    }
                    /* Extract Zip File */
                    $zip->extractTo(dirname($csv_file));
                    $zip->close();
                }

                $result[$exchange] =  $csv_file;
            } catch (\GuzzleHttp\Exception\RequestException $e) {
                if(is_file($bhav_file)) unlink($bhav_file);
                if(is_file($csv_file)) unlink($csv_file);
                if ($e->hasResponse()) {
                    echo Psr7\str($e->getResponse());
                }
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                if(is_file($bhav_file)) unlink($bhav_file);
                if(is_file($csv_file)) unlink($csv_file);
                echo 'Caught exception: ',  $e->getMessage(), "\n";
                dd($e->getResponse()->getBody()->getContents());
            } catch (\Exception $e) {
                if(is_file($bhav_file)) unlink($bhav_file);
                if(is_file($csv_file)) unlink($csv_file);
                echo 'Caught exception: ',  $e->getMessage(), "\n";
                dd($e->getMessage());
            } finally {
                fclose($file);
                if(!isset($result[$exchange])) {
                    $result[$exchange] =  false;
                }
            }
        }

        return $result;
    }

    private function loadBseBhav($file_path, Carbon $date) {
        if(file_exists($file_path))
        {
            if($date->isToday()) {
                $table_name = 'bse_daily_bhavs';
                \DB::table($table_name)->truncate();
                $query = sprintf(
                    "LOAD DATA local INFILE '$file_path' 
                    INTO TABLE " .$table_name." 
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\"' 
                    ESCAPED BY '\"' 
                    LINES TERMINATED BY '\\n' 
                    IGNORE 1 LINES 
                    (`sc_code`,`sc_name`,`sc_group`,`sc_type`,`open`,`high`,`low`,`close`,`last`,`prevclose`,`no_trades`,`no_of_shrs`,`net_turnov`,`tdcloindi`,@var1) 
                    set timestamp = '%s'",$date->format('Y-m-d'));
                \DB::connection()->getpdo()->exec($query);
            }

            if(DB::table('bse_historical_bhavs')->whereDate('bhav_date', $date->toDateString())->count() == 0)
            {
                $query = sprintf(
                    "LOAD DATA local INFILE '$file_path' 
                    INTO TABLE `bse_historical_bhavs` 
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\"' 
                    ESCAPED BY '\"' 
                    LINES TERMINATED BY '\\n' 
                    IGNORE 1 LINES 
                    (`sc_code`,`sc_name`,`sc_group`,`sc_type`,`open`,`high`,`low`,`close`,`last`,`prevclose`,`no_trades`,`no_of_shrs`,`net_turnov`,`tdcloindi`,@var1) 
                    SET  bhav_date = '%s'",$date->format('Y-m-d'));
                \DB::connection()->getpdo()->exec($query);
            }
        }

        return \DB::table('daily_bhav_history')->whereDate('bhav_date', $date->toDateString())->update(['bse' => $file_path]);
    }

    private function loadNseBhav($file_path, Carbon $date) {
        if(file_exists($file_path))
        {
            if($date->isToday()) {
                $table_name = 'nse_daily_bhavs';
                \DB::table($table_name)->truncate();
                $query = sprintf(
                    "LOAD DATA local INFILE '$file_path' 
                    INTO TABLE " .$table_name." 
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\"' 
                    ESCAPED BY '\"' 
                    LINES TERMINATED BY '\\n' 
                    IGNORE 1 LINES 
                    (symbol, series, open, high ,low, close, last, prevclose, tottrdqty, tottrdval, @var1, totaltrades, isin) 
                    set timestamp = '%s'",$date->format('Y-m-d'));
                \DB::connection()->getpdo()->exec($query);
            }

            //dd(DB::table('nse_historical_bhavs')->whereDate('bhav_date', $date->toDateString())->count());

            if(DB::table('nse_historical_bhavs')->whereDate('bhav_date', $date->toDateString())->count() == 0)
            {
                $query = sprintf(
                    "LOAD DATA local INFILE '$file_path' 
                    INTO TABLE `nse_historical_bhavs` 
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\"' 
                    ESCAPED BY '\"' 
                    LINES TERMINATED BY '\\n' 
                    IGNORE 1 LINES 
                    (symbol, series, open, high ,low, close, last, prevclose, tottrdqty, tottrdval, @var1, totaltrades, isin) 
                    set bhav_date = '%s'",$date->format('Y-m-d'));
                \DB::connection()->getpdo()->exec($query);
            }
        }

        return \DB::table('daily_bhav_history')->whereDate('bhav_date', $date->toDateString())->update(['nse' => $file_path]);
    }


    private function loadNfoBhav($file_path, Carbon $date) {
        if(file_exists($file_path))
        {
            if($date->isToday()) {
                $table_name = 'nfo_daily_bhavs';
                \DB::table($table_name)->truncate();
                $query = "LOAD DATA local INFILE '$file_path' 
                    INTO TABLE " .$table_name." 
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\"' 
                    ESCAPED BY '\"' 
                    LINES TERMINATED BY '\\n' 
                    IGNORE 1 LINES 
                    (`instrument`,`symbol`, @var1,`strike`,`option_type`,`open`,`high`,`low`,`close`,`setle_pr`,`contracts`,`val_lakhs`,`open_int`,`chg_in_oi`,@var2)
                    set `expiry_date` = STR_TO_DATE(concat(SUBSTRING_INDEX(@var1,'-',-1), '-',month(str_to_date(SUBSTRING_INDEX(SUBSTRING_INDEX(@var1,'-',2),'-',-1),'%b')),'-', SUBSTRING_INDEX(@var1,'-',1)) ,'%Y-%m-%d'), 
                    timestamp = STR_TO_DATE(concat(SUBSTRING_INDEX(@var2,'-',-1), '-',month(str_to_date(SUBSTRING_INDEX(SUBSTRING_INDEX(@var2,'-',2),'-',-1),'%b')),'-', SUBSTRING_INDEX(@var2,'-',1)) ,'%Y-%m-%d');";
                \DB::connection()->getpdo()->exec($query);
            }

            if(DB::table('nfo_historical_bhavs')->whereDate('bhav_date', $date->toDateString())->count() == 0)
            {
                $query = "LOAD DATA local INFILE '$file_path' 
                    INTO TABLE `nfo_historical_bhavs` 
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\"' 
                    ESCAPED BY '\"' 
                    LINES TERMINATED BY '\\n' 
                    IGNORE 1 LINES 
                    (`instrument`,`symbol`, @var1,`strike`,`option_type`,`open`,`high`,`low`,`close`,`setle_pr`,`contracts`,`val_lakhs`,`open_int`,`chg_in_oi`,@var2)
                    set `expiry_date` = STR_TO_DATE(concat(SUBSTRING_INDEX(@var1,'-',-1), '-',month(str_to_date(SUBSTRING_INDEX(SUBSTRING_INDEX(@var1,'-',2),'-',-1),'%b')),'-', SUBSTRING_INDEX(@var1,'-',1)) ,'%Y-%m-%d'),
                    `bhav_date` = STR_TO_DATE(concat(SUBSTRING_INDEX(@var2,'-',-1), '-',month(str_to_date(SUBSTRING_INDEX(SUBSTRING_INDEX(@var2,'-',2),'-',-1),'%b')),'-', SUBSTRING_INDEX(@var2,'-',1)) ,'%Y-%m-%d');";
                \DB::connection()->getpdo()->exec($query);
            }
        }

        return \DB::table('daily_bhav_history')->whereDate('bhav_date', $date->toDateString())->update(['nfo' => $file_path]);
    }
    private function loadNfoLot($file_path, Carbon $date) {
        if(file_exists($file_path) && $date->isToday())
        {
            $table_name = 'nfo_lot_sizes';
            \DB::table($table_name)->truncate();
            $query = "LOAD DATA local INFILE '$file_path' 
                INTO TABLE " .$table_name." 
                FIELDS TERMINATED BY ',' 
                OPTIONALLY ENCLOSED BY '\"' 
                ESCAPED BY '\"' 
                LINES TERMINATED BY '\\n' 
                IGNORE 1 LINES 
                (@val1, @val2, @val3, @val4 ,@val5)
                SET `name`= trim(@val1), `symbol` = trim(@val2), `current` = trim(@val3), `month_1` = trim(@val4), `month_2` = trim(@val5);";
            \DB::connection()->getpdo()->exec($query);
        }

        return \DB::table('daily_bhav_history')->whereDate('bhav_date', $date->toDateString())->update(['nfo_lot' => $file_path]);
    }
}
