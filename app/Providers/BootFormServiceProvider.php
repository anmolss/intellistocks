<?php

namespace App\Providers;

use AdamWathan\BootForms\BootForm;
use AdamWathan\BootForms\BootFormsServiceProvider;
use App\Libraries\MyForm;
use App\Libraries\MyFormBuilder;

class BootFormServiceProvider extends BootFormsServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        parent::register();
    }

    protected function registerBasicFormBuilder()
    {
        $this->app['bootform.basic'] = $this->app->share(function ($app) {
            return new MyFormBuilder($app['adamwathan.form']);
        });
    }

}
