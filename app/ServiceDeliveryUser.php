<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceDeliveryUser extends Model
{
    protected $table = 'service_delivery_user';

    protected $fillable = [
        'service_id','service_delivery_service_id','plan_id','service_delivery_id', 'user_id', 'call_type', 'symbol',
        'qty', 'nfo_qty','value', 'msg', 'suitability', 'suitability_by', 'user_service_id', 'request_id'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function plan(){
        return $this->belongsTo(Plan::class, 'plan_id');
    }

    public function sd(){
        return $this->belongsTo(ServiceDelivery::class, 'service_delivery_id');
    }

    public function service(){
        return $this->belongsTo(Service::class, 'service_id');
    }
}
