<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use Sluggable;
    protected $fillable = [
        'name','description','category','detail', 'status',
        'min_call', 'max_call', 'bill_period', 'grace_period',
        'service_fee', 'applicable_tax'
    ];

    protected $casts = [
        'applicable_tax' => 'array',
        'plan_detail' => 'json',
    ];


    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }


    public function sluggable()
    {
        return [
            'name' => [
                'source' => 'name'
            ]
        ];
    }
}
