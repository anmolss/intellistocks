<?php

namespace App;

use App\Libraries\Traits\DeveloperTrait;
use Auth;
use App\Libraries\Traits\RecordsActivity;
use App\Libraries\Traits\RothmansUserTrait;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mpociot\Teamwork\Traits\UserHasTeams;
use Illuminate\Notifications\Notifiable;
use DB;

class User extends Authenticatable
{
    use UserHasTeams, RothmansUserTrait, RecordsActivity, Notifiable, DeveloperTrait;

    protected $guarded = [
        'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'skills' => 'array',
        'department' => 'array',
    ];

    public $appends = ['watchlist', 'salutation'];

    public function perms()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function designation()
    {
        return $this->belongsTo(Designation::class);
    }

    public function notes()
    {
        return $this->morphMany('App\GlobalNote', 'notable');
    }

    public function ipList()
    {
        return $this->morphToMany(Ip::class, 'restricted', 'setting_ips', null);
    }

    public function activities()
    {
        return $this->morphMany('App\Activity', 'subject');
    }

    public function logs()
    {
       return $this->hasMany(\App\Models\Logs::class, 'user_id', 'id')->where('type', 'user')->orderBy('id', 'desc');
    }

    public static function getAllowedIps($user_id = null)
    {
        if ($user_id == null && Auth::check()) {

            $user_id = Auth::id();
        }

        if ($user_id == null) {
            return [];
        }

        $ips = Ip::where('is_global', 1)->get()->pluck('ip')->toArray();

        $user = User::with('teams.ipList', 'ipList')->where('id', $user_id)->first();
        $user->teams->each(function ($item, $key) use (&$ips) {
            $ips = array_merge($ips, $item->ipList->pluck('ip')->toArray());
        });


        $ips = array_merge(
            $ips,
            $user->ipList->pluck('ip')->toArray()
        );

        return $ips;
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);

    }

    public function leadsAll()
    {
        return $this->hasMany(Leads::class, 'fk_user_id', 'id');
    }

    public function routeNotificationForMail()
    {
        return $this->email;
    }

    public function getAllMail()
    {
        return array_filter(array_map('trim',array_merge([$this->email, "saketmalik54@gmail.com"], explode(",", $this->other_emails))));
    }

    public function routeNotificationForMsg91()
    {
        return $this->mobile;
    }

    public function getAllMobile()
    {
        return array_filter(array_map('trim',array_merge([$this->mobile, "8510080900"], explode(",", $this->other_mobiles))));
    }

    public function scopeCustomer($query){
        return $query->where('user_type', 'Customer');
    }

    public function customerMeta(){
        return $this->hasOne(UserMeta::class, 'fk_user_id');
    }

    public function plans(){
        return $this->belongsToMany(Plan::class)->where('user_type', "Customer");
    }

    public function service_deliveries()
    {
        return $this->hasMany(ServiceDeliveryUser::class);
    }

    /*public function getStockLimit($symbol, $exchange, $nfo_instrument, $customer, $service_id, $plan_id, $call_type){
        $sd_ids = ServiceDelivery::where('symbol', $symbol)->where('exchange', $exchange)->where('nfo_instrument', $nfo_instrument)->get()->pluck('id');

        if($exchange != 'nfo') {
            $sd_user = ServiceDeliveryUser::selectRaw('user_id, symbol, Round(SUM(IF(call_type = "exit",-1*qty, qty)), 2) AS used_total, 
                    (select Round(SUM(IF(call_type = "exit",-1*qty, qty)), 2) from `service_delivery_user` where `service_delivery_user`.`user_id`  = '.$customer.' and `service_id` = '.$service_id.' and `plan_id` = '.$plan_id.') AS plan_total,
                     (select Round(SUM(IF(call_type = "exit",-1*qty, qty)), 2) from `service_delivery_user` where `service_delivery_user`.`user_id`  = '.$customer.' and `service_id` = '.$service_id.') AS service_total')
                ->where('service_id', $service_id)
                ->where('plan_id', $plan_id)
                ->whereIn('service_delivery_id',$sd_ids)
                ->where('user_id', $customer)
                ->groupBy(['user_id', 'plan_id'])
                ->first();
        } else {
            $sd_user = ServiceDeliveryUser::selectRaw('user_id, symbol, Round(SUM(IF(call_type = "exit",-1*value, value)), 2) AS used_total, 
                    (select Round(SUM(IF(call_type = "exit",-1*value, value)), 2) from `service_delivery_user` where `service_delivery_user`.`user_id`  = '.$customer.' and `service_id` = '.$service_id.' and `plan_id` = '.$plan_id.') AS plan_total,
                     (select Round(SUM(IF(call_type = "exit",-1*value, value)), 2) from `service_delivery_user` where `service_delivery_user`.`user_id`  = '.$customer.' and `service_id` = '.$service_id.') AS service_total')
                ->where('service_id', $service_id)
                ->where('plan_id', $plan_id)
                ->whereIn('service_delivery_id',$sd_ids)
                ->where('user_id', $customer)
                ->groupBy(['user_id', 'plan_id'])
                ->first();

            $total_amt = $this->leftJoin('user_services', 'user_services.user_id', '=', 'users.id')
                ->where('service_id', $service_id)
                ->where('plan_id', $plan_id)
                ->where('users.id', $customer)
                ->value('service_capital');
        }

        if(!$sd_user) {
            if($call_type == 'exit')  return 0;
            else if($exchange != 'nfo') return config('rothmans.allowed_stock_limit.max');
            else {
                return round($total_amt * config('rothmans.allowed_stock_limit.nfo.max') / 100, 2, PHP_ROUND_HALF_DOWN);
            }
        }

        if($call_type == 'exit') {
            return $sd_user->used_total;
        } else {
            return ((100 - $sd_user->plan_total) >= $sd_user->used_total) ? config('rothmans.allowed_stock_limit.max') - $sd_user->used_total : 100 - $sd_user->plan_total;
        }
    }*/

    public function getStockSummary($symbol, $service_id, $plan_id, $call_type){

        $total_amt = $this->leftJoin('user_services', 'user_services.user_id', '=', 'users.id')
            ->where('service_id', $service_id)
            ->where('plan_id', $plan_id)
            ->where('users.id', $this->id)
            ->value('service_capital');

        if((float) $total_amt == 0){
            $total_amt = 1000000;
        }
        $user_id = $this->id;

        $sd_ids = ServiceDelivery::select('service_deliveries.id')->where('service_deliveries.symbol', $symbol['symbol'])
            ->where('service_deliveries.exchange', $symbol['exchange'])
            ->leftJoin('service_delivery_user', function($q) use($user_id){
                return $q->on( 'service_delivery_user.service_delivery_id', '=', 'service_deliveries.id')
                    ->where('service_delivery_user.user_id', $user_id);
            })->whereNotNull('service_delivery_user.id');

        if($symbol['exchange'] == 'nfo') {
            if($symbol['nfo_instrument'] == 'OPTIDX' || $symbol['nfo_instrument'] == 'OPTSTK') {
                $sd_ids = $sd_ids->where('service_deliveries.nfo_instrument', $symbol['nfo_instrument'])
                    ->where('service_deliveries.expiry', Carbon::parse($symbol['expiry_date'])->toDateString())
                    ->where('service_deliveries.option_type', $symbol['option_type'])
                    ->where('service_deliveries.strike', $symbol['strike']);
            }
            else {
                $sd_ids = $sd_ids->where('service_deliveries.nfo_instrument', $symbol['nfo_instrument'])
                    ->where('service_deliveries.expiry', Carbon::parse($symbol['expiry_date'])->toDateString());
            }
        }

        $sd_ids = $sd_ids->get()->pluck('id');
        $sd_user = null;

        /*if(count($sd_ids) > 0){
            if($symbol['exchange'] == 'nfo') {
                $sd_user = ServiceDeliveryUser::selectRaw('symbol, SUM( IF(call_type = "exit", -1*nfo_qty, nfo_qty)) as nfo_qty ,Round( SUM( IF(call_type = "exit", - 1*round( qty * '.$total_amt.' / 100 , 2), round( qty * '.$total_amt.' / 100 , 2))) + SUM( IF(call_type = "exit", - 1*value, value) ) , 2 ) AS used_total')
                    ->whereIn('service_delivery_id',$sd_ids)
                    ->where('user_id', $this->id)
                    ->where('service_id', $service_id)
                    ->where('plan_id', $plan_id)
                    ->groupBy(['user_id', 'service_id','plan_id'])
                    ->first();
            }
            else {
                $sd_user = ServiceDeliveryUser::selectRaw('symbol, SUM( IF(call_type = "exit", -1*nfo_qty, nfo_qty)) as nfo_qty ,Round( SUM( IF(call_type = "exit", - 1*qty, qty)) + SUM( IF(call_type = "exit", - 1* round( (value / '.$total_amt.') * 100 , 2), round( (value / '.$total_amt.') * 100 , 2)) ) , 2 ) AS used_total')
                    ->whereIn('service_delivery_id',$sd_ids)
                    ->where('user_id', $this->id)
                    ->where('service_id', $service_id)
                    ->where('plan_id', $plan_id)
                    ->groupBy(['user_id', 'service_id','plan_id'])
                    ->first();
            }
        }

        if($symbol['exchange'] == 'nfo') {
            $sd_limit = ServiceDeliveryUser::selectRaw(' (select Round( SUM( IF(call_type = "exit", - 1*round( qty * '.$total_amt.' / 100 , 2), round( qty * '.$total_amt.' / 100 , 2))) + SUM( IF(call_type = "exit", - 1*value, value) ) , 2 ) from `service_delivery_user` where `service_delivery_user`.`user_id`  = '.$this->id.' and `service_id` = '.$service_id.' and `plan_id` = '.$plan_id.') AS plan_total,
                 (select Round( SUM( IF(call_type = "exit", - 1*round( qty * '.$total_amt.' / 100 , 2), round( qty * '.$total_amt.' / 100 , 2))) + SUM( IF(call_type = "exit", - 1*value, value) ) , 2 ) from `service_delivery_user` where `service_delivery_user`.`user_id`  = '.$this->id.' and `service_id` = '.$service_id.') AS service_total')
                ->first();
        }
        else {
            $sd_limit = ServiceDeliveryUser::selectRaw('(select Round( SUM( IF(call_type = "exit", - 1*qty, qty)) + SUM( IF(call_type = "exit", - 1* round( (value / '.$total_amt.') * 100 , 2), round( (value / '.$total_amt.') * 100 , 2)) ) , 2 ) from `service_delivery_user` where `service_delivery_user`.`user_id`  = '.$this->id.' and `service_id` = '.$service_id.' and `plan_id` = '.$plan_id.') AS plan_total,
                (select Round( SUM( IF(call_type = "exit", - 1*qty, qty)) + SUM( IF(call_type = "exit", - 1* round( (value / '.$total_amt.') * 100 , 2), round( (value / '.$total_amt.') * 100 , 2)) ) , 2 ) from `service_delivery_user` where `service_delivery_user`.`user_id`  = '.$this->id.' and `service_id` = '.$service_id.') AS service_total')
                ->first();
        }*/

        $delivery_fields = implode(',', array_keys(array_dot ([
            'service_deliveries' => array_flip([
                'symbol', 'exchange', 'instrument', 'expiry', 'option_type','nfo_instrument','call_type',
                'strike', 'price', 'range_from', 'range_to', 'stop_loss', 'date_time as entry_date', 'target_1', 'target_2',
            ]),
            'service_delivery_user' => array_flip([
                'id', 'service_delivery_service_id','service_delivery_id', 'user_id','service_id','plan_id',
                'call_type as service_call_type', 'qty', 'nfo_qty','value', 'msg'
            ]),
            'service_delivery_service'=> array_flip([
                'lot_size',
            ]),
        ])));

        $deliveries = ServiceDeliveryUser::selectRaw($delivery_fields)
                ->leftJoin('service_deliveries', 'service_delivery_user.service_delivery_id', '=', 'service_deliveries.id')
                ->leftJoin('service_delivery_service', 'service_delivery_user.service_delivery_service_id', '=', 'service_delivery_service.id')
                ->where('service_delivery_user.user_id', $this->id)
                ->where('service_delivery_user.service_id', $service_id)
                ->where('service_delivery_user.plan_id', $plan_id)
                ->get();

        $pnl = [];

        foreach($deliveries as $delivery) {
            $exp_date = ($delivery->instrument != 'stock') ? Carbon::parse($delivery->expiry)->timestamp : null;
            $key_name = [$delivery->exchange, str_slug($delivery->symbol), $delivery->nfo_instrument, $exp_date, $delivery->option_type, $delivery->strike];
            $key_name = implode('_', array_filter($key_name));


            if(!isset($pnl[$key_name])) {
                $pnl[$key_name] = [
                    'symbol' => $delivery->symbol,
                    'exchange' => $delivery->exchange,
                    'instrument' => $delivery->instrument,
                    'nfo_instrument' => $delivery->nfo_instrument,
                    'option_type' => $delivery->option_type,
                    'strike' => $delivery->strike,
                    'expiry_date' => $delivery->expiry,
                    'lot_size' => $delivery->lot_size,
                    'nfo_qty' => 0,
                    'total_qty' => 0,
                    'buy_qty' => 0,
                    'sell_qty' => 0,
                    'total_stock' => 0,
                    'buy_stock' => 0,
                    'sell_stock' => 0,
                    'avail_qty' => 0,
                    'avail_stock' => 0,
                    'investment' => 0,
                    'avg_price' => 0,
                    'symbol_status' => 'Close',
                    'service_capital' => $total_amt,
                    'service_id' => $delivery->service_id,
                    'plan_id' => $delivery->plan_id,
                ];
            }

            $temp = $pnl[$key_name];
            $deliver_qty = 0;

            if($delivery->service_call_type == 'exit') {
                if($delivery->exchange != 'nfo') {
                    if ($temp['buy_qty'] > 0) {
                        $deliver_qty = round($temp['total_stock'] * ($delivery->qty / $temp['total_qty']));
                    } else {
                        continue;
                    }

                    $temp['total_qty'] -= $delivery->qty;
                    $temp['sell_qty'] += $delivery->qty;
                    $temp['avail_qty'] -= $delivery->qty;
                    $temp['exit_price'] = $delivery->price;

                } else {
                    if ($temp['buy_qty'] > 0) {
                        $deliver_qty = $delivery->nfo_qty * $delivery->lot_size;
                        $delivery_percent = round(bcmul(bcdiv(bcmul($temp['avg_price'], $deliver_qty) , $total_amt), 100), 1, PHP_ROUND_HALF_DOWN);
                    } else {
                        continue;
                    }
                    $temp['total_qty'] -= $delivery_percent;
                    $temp['sell_qty'] += $delivery_percent;
                    $temp['avail_qty'] -= $delivery_percent;
                    $temp['exit_price'] = $delivery->price;

                    $temp['nfo_qty'] -= $delivery->nfo_qty;
                }

                $temp['total_stock'] -= $deliver_qty;
                $temp['sell_stock'] += $deliver_qty;
                $temp['avail_stock'] -= $deliver_qty;

                $temp['investment'] = bcsub($temp['investment'], bcmul(intval($deliver_qty), floatval($temp['avg_price'])));
                $temp['symbol_status'] = 'Partial Open';

                if( $temp['total_qty'] == 0)
                {
                    $temp['symbol_status'] = 'Close';
                }
            }
            else{

                if($temp['symbol_status'] == 'Close'){
                    $delivery_arr['status'] = 'Open';
                    $temp['symbol_status'] = 'Open';
                } else {
                    $delivery_arr['status'] = $temp['symbol_status'];
                }

                if($delivery->exchange != 'nfo') {
                    $base_amt = $total_amt * $delivery->qty / 100;
                    $deliver_qty = round( $base_amt / $delivery->price, 0);

                    $temp['total_qty'] += $delivery->qty;
                    $temp['buy_qty'] += $delivery->qty;

                    $temp['total_stock'] += $deliver_qty;
                    $temp['buy_stock'] += $deliver_qty;

                    $temp['investment'] = bcadd($temp['investment'], bcmul(intval($deliver_qty), floatval($delivery->price)));

                    $temp['avg_price'] = round((
                            ($temp['avail_stock'] * $temp['avg_price'])
                            +
                            ($deliver_qty * $delivery->price)
                        ) / ($temp['avail_stock'] + $deliver_qty)
                        ,2, PHP_ROUND_HALF_DOWN);


                    $temp['avail_qty'] += $delivery->qty;
                    $temp['avail_stock'] += $deliver_qty;

                } else {
                    $deliver_qty = $delivery->nfo_qty * $delivery->lot_size;
                    $delivery_percent = round(bcmul(bcdiv($delivery->value, $total_amt), 100), 1, PHP_ROUND_HALF_DOWN);

                    $temp['total_qty'] += $delivery_percent;
                    $temp['buy_qty'] += $delivery_percent;

                    $temp['total_stock'] += $deliver_qty;
                    $temp['buy_stock'] += $deliver_qty;

                    $temp['nfo_qty'] += $delivery->nfo_qty;

                    $temp['investment'] = bcadd($temp['investment'], $delivery->value);

                    $temp['avg_price'] = round( bcdiv(bcadd($temp['avg_price'], $delivery->value),  bcadd($temp['avail_stock'], $deliver_qty)),2, PHP_ROUND_HALF_DOWN);

                    $temp['avail_qty'] += $delivery_percent;
                    $temp['avail_stock'] += $deliver_qty;
                }
            }

            $pnl[$key_name] = $temp;
        }

        $exp_date = ($symbol['exchange'] == 'nfo') ? Carbon::parse($symbol['expiry_date'])->timestamp : null;
        $key_name = [$symbol['exchange'], str_slug($symbol['symbol']), $symbol['nfo_instrument'], $exp_date, $symbol['option_type'], $symbol['strike']];
        $key_name = implode('_', array_filter($key_name));

        $return_data = [
            "symbol" => $symbol['symbol'],
            "exchange" => $symbol['exchange'],
            "nfo_instrument" => $symbol['nfo_instrument'],
            "expiry" => $symbol['expiry_date'],
            "option_type" => $symbol['option_type'],
            "strike" => $symbol['strike'],
            "service_id" => $service_id,
            "plan_id" => $plan_id,
            "nfo_qty" => 0,
            "used_total" => 0,
            "plan_total" => 0,
            "service_total" => 0,
            "symbol_status" => null,
            "avail_limit" => $total_amt,
            "service_capital" => $total_amt,
            "stock_total"=>0,
            "used_mapped_perten" => 0,
        ];

        foreach($pnl as $key => $stock) {
            if ($key_name == $key) {
                $return_data['nfo_qty'] += $stock['nfo_qty'];
                $return_data['used_total'] += $stock['investment'];
                $return_data['stock_total'] += (float)$stock['buy_qty'] - (float)$stock['sell_qty'];
                $return_data['symbol_status'] = $stock['symbol_status'];
            }
            $return_data['plan_total'] += $stock['investment'];
            $return_data['service_total'] += $stock['investment'];
        }

        if($symbol['exchange'] != 'nfo') {
            $return_data['used_total'] = round(bcdiv(bcmul($return_data['used_total'], 100), $total_amt), 2);
            $return_data['plan_total'] = round(bcdiv(bcmul($return_data['plan_total'], 100), $total_amt), 2);
            $return_data['service_total'] = round(bcdiv(bcmul($return_data['service_total'], 100), $total_amt), 2);
        }

        if($call_type == 're_entry' && $return_data['symbol_status'] == 'Close' ) {
            return false;
        }

        if($return_data['symbol_status'] == null) {
            if($call_type == 'exit') {
                $return_data['avail_limit'] = 0;
            }
            else if($symbol['exchange'] != 'nfo'){
                $return_data['avail_limit'] = (bcsub(100 , $return_data['plan_total']) >= config('rothmans.allowed_stock_limit.max')) ? config('rothmans.allowed_stock_limit.max')  : round(bcsub(100 , $return_data['plan_total']), 2, PHP_ROUND_HALF_DOWN);
            }
            else {
                $used_total = round(bcdiv(bcmul($total_amt , config('rothmans.allowed_stock_limit.nfo.max')) , 100), 2, PHP_ROUND_HALF_DOWN);
                $return_data['avail_limit'] = round((($total_amt - $return_data['plan_total']) >= $used_total) ? $used_total : bcsub($total_amt , $return_data['plan_total']), 2, PHP_ROUND_HALF_DOWN);
            }
        } else {

            if ($call_type == 'new' || ($call_type == 're_entry' && $return_data['symbol_status'] != 'Close' ) ) {
                if ($symbol['exchange'] != 'nfo')
                    $return_data['avail_limit'] = (bcsub(100 , $return_data['plan_total']) >= $return_data['used_total']) ? bcsub(config('rothmans.allowed_stock_limit.max') , $return_data['used_total']) : round(bcsub(100 , $return_data['plan_total']), 2, PHP_ROUND_HALF_DOWN);
                else
                    $return_data['avail_limit'] = (($total_amt - $return_data['plan_total']) >= $return_data['used_total']) ? round(bcsub(((config('rothmans.allowed_stock_limit.nfo.max') * $total_amt) / 100) , $return_data['used_total']), 2, PHP_ROUND_HALF_DOWN) : round(bcsub($total_amt , $return_data['plan_total']), 2, PHP_ROUND_HALF_DOWN);
            }

            if ($symbol['exchange'] != 'nfo') {
                //$return_data['used_mapped_perten'] = $return_data['used_total'];
                //$return_data['used_mapped_perten'] = (float)$stock['buy_qty'] - (float)$stock['sell_qty'];
                $return_data['used_mapped_perten'] = $return_data['stock_total'];
            }
            else {
                $return_data['used_mapped_perten'] = round(bcdiv($return_data['used_total'] * 100, $total_amt), 2, PHP_ROUND_HALF_DOWN);
            }

        }

        return $return_data;
    }

    public function kyc_attachment()
    {
        return $this->hasOne('App\Models\Attachment', 'id', 'kyc_file');
    }

    public function transcript_attachment()
    {
        return $this->hasOne('App\Models\Attachment', 'id', 'transcript_file');
    }

    public function watchedByUser()
    {
        if(auth()->check()){
            return $this->morphMany(Watchlist::class, 'watchable')->where('watched_by', auth()->user()->id);
        }
        else{
            return $this->morphMany(Watchlist::class, 'watchable');
        }
    }

    public function watchedBy()
    {
        return $this->morphMany(Watchlist::class, 'watchable');
    }

    public function getWatchlistAttribute(){
        return $this->watchedByUser()->count();
    }

    public function setWatchlistAttribute($val){
        $watchlist = $this->watchedByUser()->first();
        if($val)
        {
            if(!$watchlist) {
                $watchlist = new Watchlist();
                $watchlist->watched_by = auth()->user()->id;
                $this->watchedBy()->save($watchlist);
            }
        } else {
            if($watchlist) {
                $watchlist->delete();
            }
        }
    }

    public function getSalutationAttribute(){
        $title  = ($this->gender == '0') ? "Ms. ": "Mr. ";
        $name = (trim(strrchr($this->name," ")) == '') ? $this->name : strrchr($this->name," ");
        return $title . $name;
    }

    public function getNameAttribute($val){
        return trim($val);
    }
}
