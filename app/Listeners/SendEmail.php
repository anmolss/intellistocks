<?php

namespace App\Listeners;

use App\Events\SendMail;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendEmail
{
    //use InteractsWithQueue;
    use DispatchesJobs;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  SendMail  $event
     * @return void
     */
    public function handle(SendMail $event)
    {
        $message = $event->message;
        $email = 'savitha.somanbca@gmail.com';
        //dd($email);
        if($email != null)
        {
            Mail::raw($message, function($m) use ($email){
                $m->to($email);
                $m->subject('New User Created');
                $m->getSwiftMessage();
            });
        }
    }
}
