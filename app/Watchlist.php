<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Watchlist extends Model
{
    protected $fillable = [
        'watched_by'
    ];

    public function watchable()
    {
        return $this->morphTo();
    }

}
