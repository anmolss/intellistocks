<?php
namespace App\Models;

//use App\Libraries\Traits\RecordsActivity;
use App\Libraries\Traits\RothmansUserTrait;
use Illuminate\Database\Eloquent\Model;

class UserServiceAction extends Model
{
	protected $table = 'user_service_actions';
	protected $fillable = [
        'user_service_id',
        'action_user_id',
        'activity',
        'transcript_status',
        'kyc_status',
        'consent_status',
        'extn_days',
        'old_expiry_date',
        'new_expiry_date',
        'termination_reason',
        'comments'
    ];
	protected $guarded = ['id'];

	public function user()
    {
        return $this->belongsTo(\App\User::class, 'action_user_id', 'id');
    }

    public function user_service()
    {
        return $this->belongsTo(\App\UserService::class, 'user_service_id', 'id');
    }
}