<?php

namespace App\Models\Filters;

use App\QueryFilter;
class DeliveryFilters extends QueryFilter
{
    public function symbol($symbol)
    {
        return $this->builder->where('symbol','LIKE','%'.$symbol.'%');
    }

    public function delivery_range($dates)
    {
        $delivery_range = array_map(
            function($date){ return date('Y-m-d', strtotime(str_replace('/', '-', $date))); },
            explode('-', $dates)
        );
        return $this->builder->whereBetween('service_deliveries.created_at', $delivery_range);
    }

    public function service($service)
    {
        return $this->builder
                ->leftJoin('service_delivery_service', 'service_delivery_service.service_delivery_id', '=', 'service_deliveries.id')
                ->whereIn('service_id',$service)
                ->with(['services' => function($q) use($service){
                return $q->whereIn('service_id',$service);
            }]);
    }

}