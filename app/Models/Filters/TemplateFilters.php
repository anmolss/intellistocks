<?php
namespace App\Models\Filters;

use App\QueryFilter;

class TemplateFilters extends QueryFilter
{
    public function name($name)
    {
        return $this->builder->where('templates.name','LIKE','%'.$name.'%');
    }

    public function delivery_range($dates)
    {
        $delivery_range = array_map(
            function($date){ return date('Y-m-d', strtotime(str_replace('/', '-', $date))); },
            explode('-', $dates)
        );
        return $this->builder->whereBetween('templates.created_at', $delivery_range);
    }

    public function status($status)
    {
        return $this->builder->where('templates.status', $status);
    }

    public function type($type)
    {
        if(config('rothmans.template_type.'.$type, null) === null){
            return;
        }
        return $this->builder->where('templates.type', $type);
    }

    public function created_by($users)
    {
        if(!is_array($users))
        {
            $users = [$users];
        }
        return $this->builder->whereIn('templates.created_by', $users);
    }
}