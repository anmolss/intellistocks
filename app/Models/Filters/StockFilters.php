<?php
namespace App\Models\Filters;

use App\QueryFilter;

class StockFilters extends QueryFilter
{
    public function symbol($symbol)
    {
        return $this->builder->where('stocks.symbol','LIKE','%'.$symbol.'%');
    }

    public function exchange($exchange)
    {
        return $this->builder->where('stocks.exchange',$exchange);
    }

    public function instrument($instrument)
    {
        return $this->builder->where('stocks.instrument',$instrument);
    }

    public function expiry($data)
    {
        return $this->builder->where('stocks.expiry',$data);
    }

    public function option_type($data)
    {
        return $this->builder->where('stocks.option_type',$data);
    }

    public function strike($data)
    {
        return $this->builder->where('stocks.strike',$data);
    }

    public function created_by($users)
    {
        if(!is_array($users))
        {
            $users = [$users];
        }
        return $this->builder->whereIn('stocks.created_by', $users);
    }

    public function end_date($dates)
    {
        $delivery_range = array_map(
            function($date){ return date('Y-m-d', strtotime(str_replace('/', '-', $date))); },
            explode('-', $dates)
        );
        return $this->builder->whereBetween('stocks.end_date', $delivery_range);
    }

    public function watchlist($data)
    {
        return $this->builder->where('stocks.watchlist',$data);
    }


}