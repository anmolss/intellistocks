<?php
namespace App\Models\Filters;

use App\QueryFilter;

class ServiceFilters extends QueryFilter
{
    public function name($name)
    {
        return $this->builder->where('services.name','LIKE','%'.$name.'%');
    }

    public function delivery_range($dates)
    {
        $delivery_range = array_map(
            function($date){ return date('Y-m-d', strtotime(str_replace('/', '-', $date))); },
            explode('-', $dates)
        );
        return $this->builder->whereBetween('services.created_at', $delivery_range);
    }

    public function status($status)
    {
        return $this->builder->where('services.status', $status);
    }

    public function watchlist($status)
    {
        return $this->builder->where('services.watchlist', $status);
    }
}