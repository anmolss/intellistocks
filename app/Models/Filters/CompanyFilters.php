<?php

namespace App\Models\Filters;

use App\QueryFilter;
class CompanyFilters extends QueryFilter
{
    public function symbol($symbol)
    {
        return $this->builder->where('symbol','LIKE',$symbol.'%');
    }

    public function name($name)
    {
        return $this->builder->where('name','LIKE',$name.'%');
    }

}