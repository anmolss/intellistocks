<?php
namespace App\Models;

//use App\Libraries\Traits\RecordsActivity;
use App\Libraries\Traits\RothmansUserTrait;
use Illuminate\Database\Eloquent\Model;
use Carbon;

class Leads extends Model
{
    
    use  RothmansUserTrait; //RecordsActivity;
    protected $fillable = [
        'title',
        'note',
        'status',
        'lead_type',
        'fk_user_id_assign',
        'fk_user_id_created',
        'fk_user_id_updated',
        'fk_client_id',
        'contact_date',
        'open_date',
        'confidence',
        'close_date',
        'priority',
        'source',
        'capital',
        'has_discount',
        'discount_type',
        'discount_value',
        
    ];
    protected $dates = ['contact_date','open_date','close_date'];

    protected $hidden = ['remember_token'];
    
    public function assignee()
    {
        return $this->belongsTo(\App\User::class, 'fk_user_id_assign');
    }
    public function createdBy()
    {
        return $this->belongsTo(\App\User::class, 'fk_user_id_created');
    }
    public function updatedBy()
    {
        return $this->belongsTo(\App\User::class, 'fk_user_id_updated');
    }
    public function clientAssignee()
    {
        return $this->belongsTo(Client::class, 'fk_client_id');
    }
    public function notes()
    {
        return $this->hasMany(Note::class, 'fk_lead_id', 'id');
    }
        // create a virtual attribute to return the days until deadline
    public function getDaysUntilContactAttribute()
    {
        return \Carbon\Carbon::now()->startOfDay()->diffInDays($this->contact_date, false);
    }
    public function activity()
    {
        return $this->hasMany(\App\Activity::class, 'subject_id', 'id')->where('subject_type', 'App\Models\Leads');
    }

    public function logs()
    {
       return $this->hasMany(Logs::class, 'type_id', 'id')->where('type', 'lead')->orderBy('id', 'desc');
    }

    public function followups()
    {
       return $this->hasMany(LeadFollowups::class, 'fk_lead_id', 'id');
    }
}
