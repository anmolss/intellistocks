<?php
namespace App\Models;

//use App\Libraries\Traits\RecordsActivity;
use App\Libraries\Traits\RothmansUserTrait;
use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
	protected $table 	= 'transaction_history';
	protected $fillable = ['user_service_id','created_by'];
	protected $guarded 	= ['id'];
}