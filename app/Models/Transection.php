<?php
namespace App\Models;

//use App\Libraries\Traits\RecordsActivity;
use App\Libraries\Traits\RothmansUserTrait;
use Illuminate\Database\Eloquent\Model;

class Transection extends Model
{
	protected $table = 'transactions';
	protected $fillable = ['fk_user_service_id', 'transaction_history_id', 'transaction_type', 'payment_type', 'amount', 'date', 'recipient', 'cheque_number', 'bank', 'type', 'transaction_id', 'received_from', 'pay_type', 'discount', 'service_fee', 'applied_tax', 'payable_amount','cleared','pay_no'];
	protected $guarded = ['id'];

	public function userService()
    {
        return $this->belongsTo(UserService::class, 'fk_user_service_id', 'id');
    }

    public function allPartialTransactions()
    {
        return $this->hasMany(Partialtransaction::class, 'payment_id', 'id');
    }

}