<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    
    protected $fillable =
    [
        'path',
        'file_name',
        'file_details',
    ];
}