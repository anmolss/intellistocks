<?php
namespace App\Models;

//use App\Libraries\Traits\RecordsActivity;
use App\Libraries\Traits\RothmansUserTrait;
use Illuminate\Database\Eloquent\Model;

class UserClientMeta extends Model
{
	protected $table = 'user_client_meta';
	protected $fillable = ['fk_source', 'fk_relation_manager_id', 'fk_service_id', 'fk_user_id', 'fk_client_id', 'fk_suitability_officer_id'];
	protected $guarded = ['id'];

	public function user()
    {
        return $this->belongsTo(User::class, 'fk_user_id', 'id');
    }

}