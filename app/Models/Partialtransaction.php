<?php
namespace App\Models;

//use App\Libraries\Traits\RecordsActivity;
use App\Libraries\Traits\RothmansUserTrait;
use Illuminate\Database\Eloquent\Model;

class Partialtransaction extends Model
{
	protected $table 	= 'partial_transactions';
	protected $fillable = ['payment_id', 'payment_type', 'amount', 'date', 'recipient', 'cheque_number', 'bank', 'type', 'transaction_id', 'received_from', 'pay_no', 'cleared'];
	protected $guarded 	= ['id'];
}