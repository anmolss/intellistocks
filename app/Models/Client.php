<?php
namespace App\Models;

//use App\Libraries\Traits\RecordsActivity;
use App\Libraries\Traits\RothmansUserTrait;
use Illuminate\Database\Eloquent\Model;
use Carbon;

class Client extends Model
{

     use  RothmansUserTrait;//, RecordsActivity;
    protected $fillable = [
            'name',
            'company_name',
            'fk_service_id',
            'email',
            'address',
            'zipcode',
            'city',
            'primary_number',
            'secondary_number',
            'company_type'];

    public function userAssignee()
    {
        return $this->belongsTo(User::class, 'fk_user_id', 'id');
    }

    public function alltasks()
    {
        return $this->hasMany(Tasks::class, 'fk_client_id', 'id')
        ->orderBy('status', 'asc')
        ->orderBy('created_at', 'desc');
    }

    public function allleads()
    {
        return $this->hasMany(Leads::class, 'fk_client_id', 'id')
        ->orderBy('status', 'asc')
        ->orderBy('created_at', 'desc');
    }

    public function service()
    {
        return $this->hasOne(Service::class, 'fk_service_id', 'id');
    }

    public function tasks()
    {
        return $this->hasMany(Tasks::class, 'fk_client_id', 'id');
    }
    public function leads()
    {
        return $this->hasMany(Tasks::class, 'fk_client_id', 'id');
    }
    public function documents()
    {
        return $this->hasMany(Document::class, 'fk_client_id', 'id');
    }
    public function invoices()
    {
        return $this->belongsToMany(Invoice::class);
    }
}
