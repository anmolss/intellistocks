<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadFollowups extends model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'lead_follow_up_history';
    protected $fillable = [
        'old_contact_date',
        'new_contact_date',
        'note',
        'created_by',
        'fk_lead_id',
        'fk_user_id'
        ];
    protected $guarded = ['id'];

    public function lead()
    {
        return $this->belongsTo(Leads::class, 'fk_lead_id', 'id');
    }
}
