<?php
namespace App\Models;

//use App\Libraries\Traits\RecordsActivity;
use App\Libraries\Traits\RothmansUserTrait;
use App\Plan;
use App\Service;
use App\User;
use Illuminate\Database\Eloquent\Model;

class UserService extends Model
{
	protected $table = 'user_services';
    
	protected $guarded = ['id'];

	public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function transections()
    {
        return $this->hasMany(Transection::class, 'fk_user_service_id', 'id');
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id', 'id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }

    public function transcript_file()
    {
        return $this->belongsTo(Attachment::class, 'transcript_file', 'id');
    }

    public function kyc_file()
    {
        return $this->belongsTo(Attachment::class, 'kyc_file', 'id');
    }

    /*
    public function partial_transaction()
    {
        return $this->hasManyThrough(Partialtransaction::class, Transection::class, 
            'fk_user_service_id', 'payment_id', 'id');
    }
    */
    
    public function user_service_action()
    {
        return $this->hasMany(UserServiceAction::class, 'user_service_id', 'id');
    }
}