<?php

namespace App;


class NseDailyBhav extends AppModel
{
    protected $primaryKey = 'symbol';
    public $incrementing = false;

    public function watchlist(){
        return $this->morphOne(Stock::class, 'watchable','exchange', 'symbol', 'symbol');
    }
}