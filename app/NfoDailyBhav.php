<?php

namespace App;

use Carbon\Carbon;

class NfoDailyBhav extends AppModel
{

    protected $primaryKey = 'symbol';
    public $incrementing = false;

    public $appends = ['lot_size'];

    public function watchlist(){
        return $this->morphMany(Stock::class, 'watchable','exchange', 'symbol', 'symbol');
    }

    public function lotsize(){
        return $this->hasOne(NfoLotSize::class,'symbol', 'symbol');
    }

    public function getLotSizeAttribute(){
        $lotsize = $this->lotsize()->first();

        if(Carbon::parse('last thu of this month')->equalTo(Carbon::parse($this->attributes['expiry_date']))) {
            return $lotsize->current;
        }
        else if(Carbon::parse('last thu of +1 month')->equalTo(Carbon::parse($this->attributes['expiry_date']))) {
            return $lotsize->month_1;
        }
        else if(Carbon::parse('last thu of +2 month')->equalTo(Carbon::parse($this->attributes['expiry_date']))) {
            return $lotsize->month_2;
        } else {
            return $lotsize->current;
        }
    }
}