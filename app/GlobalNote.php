<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalNote extends Model
{
    /**
     * Get all of the owning notable models.
     */
    public function notable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}