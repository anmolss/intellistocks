<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ServiceDelivery extends Model
{
    protected $fillable = [
        'symbol', 'expiry', 'option_type', 'instrument', 'exchange', 'nfo_instrument','call_type', 'strike', 'template_id',
        'price', 'range_from', 'range_to', 'qty', 'stop_loss', 'date_time', 'remark', 'signature', 'target_1', 'target_2',
        'mode', 'msg', 'customers', 'updated_at', 'created_at', 'given_by'
    ];

    protected $casts = [
        'mode' => 'array'
    ];

    /**
     * @return array
     */
    public function services()
    {
        $subQuery = "select * from (select service_id as s_id, service_delivery_id as sd_id, count(*) as customers_count, 
(select count(*) from service_delivery_user where delivery_status = 'success' and service_id = s_id and service_delivery_id = sd_id) as success_count, 
(select count(*) from service_delivery_user where delivery_status = 'failed' and service_id = s_id and service_delivery_id = sd_id) as failure_count,
(select GROUP_CONCAT(user_id)  from service_delivery_user where service_id = s_id and service_delivery_id = sd_id) as user_id_group,
(select GROUP_CONCAT(user_id)  from service_delivery_user where delivery_status = 'success' and service_id = s_id and service_delivery_id = sd_id) as success_user_ids,
(select GROUP_CONCAT(user_id)  from service_delivery_user where delivery_status = 'failed' and service_id = s_id and service_delivery_id = sd_id) as failure_user_ids, 
msg, qty, nfo_qty from service_delivery_user group by service_id, service_delivery_id, msg, qty, nfo_qty)result";
        return $this->hasMany(ServiceDeliveryService::class)->join(DB::raw( sprintf( '(%s) result', $subQuery ) )
            ,[
                ['result.s_id', '=', 'service_delivery_service.service_id'],
                ['result.sd_id', '=', 'service_delivery_service.service_delivery_id'],
            ]);

        //return $this->hasMany(ServiceDeliveryService::class);
    }
    /**
     * @return array
     */
    public function template()
    {
        return $this->belongsTo(SdTemplate::class);
    }

    /**
     * @return array
     */
    public function sd_users()
    {
        return $this->hasMany(ServiceDeliveryUser::class);
    }

    public function call_by(){
        return $this->belongsTo(User::class, 'given_by');
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query->fullData());
    }

    public function scopeFullData($query){
        return $query->with(['services.service', 'template', 'sd_users'])->orderBy('service_deliveries.id', 'Desc');
    }

    public function setExpiryAttribute($val) {
        $this->attributes['expiry'] = Carbon::parse($val)->format('Y-m-d');
    }

}
