<?php

namespace app;

use App\QueryFilter;
class UserFilters extends QueryFilter
{
    public function name($name)
    {
        return $this->builder->where('name','LIKE','%'.$name.'%')->orWhere('username', 'LIKE','%'.$name.'%')->orWhere('email', 'LIKE','%'.$name.'%');
    }

    public function mobile($number){
        return $this->builder->where('name','LIKE','%'.$number.'%');
    }

    public function department($department){
        return $this->builder->where('department',$department);
    }

    public function status($status_str){
        $status = 1;
        if($status_str == 'inactive') $status = 0;

        return $this->builder->where('status',$status);
    }

    public function designation($designation){
        return $this->builder->where('designation',$designation);
    }

    public function reporting_to($manager){
        return $this->builder->where('reporting_to',$manager);
    }

    public function city($city){
        return $this->builder->where('city','LIKE','%'.$city.'%');
    }

    public function state($state){
        return $this->builder->where('state','LIKE','%'.$state.'%');
    }

    public function skills($skills) {
        if(!is_array($skills)) $skills = [$skills];
        return $this->builder->whereRaw("CONCAT(\",\", replace(replace(replace(replace(trim(skills),'\t',''), '[', ''), '\"', ''), ']', '')  , \",\") REGEXP \",(".implode('|', $skills)."),\"");
    }

}