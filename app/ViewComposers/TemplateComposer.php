<?php

namespace App\ViewComposers;


use App\Template;
use App\Token;
use Illuminate\View\View;

class TemplateComposer
{
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        $this->tokens = Token::all('value','content','id');
        $this->templates = Template::where('status', 1)->select('name', 'body')->get()->toJson();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'tokens' => $this->tokens,
            'templates' => $this->templates
        ]);
    }

}