<?php
namespace App\Repositories\Department;

interface DepartmentRepositoryContract
{
	public function find($id);

    public function getAllDepartments();
    
    public function listAllDepartments();

    public function create($requestData);

    public function update($id, $requestData);

    public function destroy($id);
}
