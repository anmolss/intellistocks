<?php
namespace App\Repositories\Department;

use App\Models\Department;

class DepartmentRepository implements DepartmentRepositoryContract
{

    public function find($id)
    {
        return Department::findOrFail($id);
    }
    public function getAllDepartments()
    {
        return Department::all();
    }

    public function listAllDepartments()
    {
        return Department::pluck('name', 'id');
    }

    public function create($requestData)
    {
        Department::create($requestData->all());
    }

    public function update($id, $requestData)
    {
        $department = Department::findOrFail($id);
        $department->fill($requestData->all())->save();
    }
    public function destroy($id)
    {
        Department::findorFail($id)->delete();
    }
}
