<?php
namespace App\Repositories\Client;

use App\Models\Client;
use App\Models\UserClientMeta;
use App\Models\UserService;
use App\Models\Transection;
use App\Models\TransactionHistory;
use App\User;
use DB;
use Carbon;
use App\Models\Industry;
use App\Models\Invoices;
use App\Models\TaskTime;
use Mail;
use Illuminate\Support\Str;
use Auth;
use App\Http\Controllers\EmailController;
//use Notification;
//use App\Notifications\ServiceAssigned;

class ClientRepository implements ClientRepositoryContract
{

    public function find($id)
    {
        return Client::findOrFail($id);
    }
    
    public function listAllClients()
    {
        return Client::pluck('name', 'id');
    }

    public function getInvoices($id)
    {
        $invoice = Client::findOrFail($id)->invoices()->with('tasktime')->get();

        return $invoice;
    }

    public function getAllClientsCount()
    {
        return Client::all()->count();
    }
   
    public function create($requestData)
    {
        Client::create($requestData);
    }

    public function createDirect($requestData='', $services='')
    {

        //begin transection
        DB::beginTransaction();
        if(isset($requestData['customer_id'])){
            $user = User::findOrFail($requestData['customer_id']);
            $user_client = UserClientMeta::where('fk_user_id',$requestData['customer_id'])->first();
        } else {
            $input = array(
                'name' => $requestData['name'],
                'email' => $requestData['email'],
                'username' => $requestData['username'],
                'gender' => $requestData['gender'],
                'official_mobile' => $requestData['secondary_number'],
                'password' => bcrypt($requestData['password']),
                'mobile' => $requestData['mobile'],
                'address1' => $requestData['address1'],
                'address2' => $requestData['address2'],
                'state' => $requestData['state'],
                'city' => $requestData['city'],
                'zip_code' => $requestData['zipcode'],
                'user_type' => 'Customer',
                'verification_code' => Str::random(25),
                'other_emails' => str_replace(" ", "", $requestData['other_emails']),
                'other_mobiles' => str_replace(" ", "", $requestData['other_mobiles']),
            );
            $user = User::create($input);
            //save metadata
            unset($input);
            $input = array(
                    'fk_relation_manager_id' => $requestData['fk_relation_manager_id'],
                    'fk_suitability_officer_id' => $requestData['fk_suitability_officer_id'],
                    'fk_source' => $requestData['source'],
                    'fk_user_id' => $user->id,
                    //'fk_client_id' => $requestData['fk_client_id'],
                );
            $user_client = UserClientMeta::create($input);
        }
        
        foreach ($services as $key => $service) {
            if(isset($service['user_service_id']) && !empty($service['user_service_id'])){
                unset($input);
                $user_service = UserService::findOrFail($service['user_service_id']);
                $input['service_capital'] = (!empty($service['service_capital']))?$service['service_capital']:'1000000';
                $user_service->fill($input)->save();
                $force_verified = false;
            } else {
                //save services
                unset($input);
                if(isset($service['force_verify']) && $service['force_verify'] == 'yes'){
                    if (isset($service['activation_date']) && strtotime($service['activation_date']) <= strtotime(Carbon::now()->format('Y-m-d'))) {
                        $status = "Active";
                    } else {
                        $status = "Pending";
                    }
                    $service_verified = 'Yes';
                    $force_verify_reason = $service['force_verify_reason'];
                    $force_verify_comment = $service['force_verify_comment'];
                    $consent_ip = 'Force Verified';
                    $consent_status = 'Yes';
                    $force_verified = true;
                } else if($user->transcript_status == 'Yes' && $user->kyc_status == 'Yes' && $user->consent_status == 'Yes'){
                    if (isset($service['activation_date']) && strtotime($service['activation_date']) <= strtotime(Carbon::now()->format('Y-m-d'))) {
                        $status = "Active";
                    } else {
                        $status = "Pending";
                    }
                    $service_verified = 'No';
                    $force_verify_reason = '';
                    $force_verify_comment = '';
                    $consent_ip = '';
                    $consent_status = 'No';
                    $force_verified = false;
                } else {
                    $status = "Pending";
                    $service_verified = 'No';
                    $force_verify_reason = '';
                    $force_verify_comment = '';
                    $consent_ip = '';
                    $consent_status = 'No';
                    $force_verified = false;
                }
                
                $activation_date = new Carbon($service['activation_date']);
                if(!empty($service['plan_details']['duration'])){
                    $expiry_date = $activation_date->subDay()->addDays($service['plan_details']['duration'])->toDateString();
                } else {
                    $expiry_date = $activation_date->subDay()->addDays(15)->toDateString();
                }
                
                $input = array(
                    'user_id' => $user->id,
                    'service_id' => $service['fk_service_id'],
                    'plan_id' => $service['fk_plan_id'],
                    'service_capital' => (!empty($service['service_capital']))?$service['service_capital']:'1000000',
                    'activation_date' => $service['activation_date'],
                    'expire_date' => $expiry_date,
                    'service_status' =>  $status,
                    'comments' =>  $service['comments'],
                    'service_verified' => $service_verified,
                    'force_verify_reason' => $force_verify_reason,
                    'force_verify_comment' => $force_verify_comment,
                    'consent_status' => $consent_status,
                    'consent_ip' => $consent_ip,
                    'verification_code' => Str::random(25),
                    'service_details' => json_encode($service['service_details']),
                    'plan_details' => json_encode($service['plan_details']),
                );
                /*if($force_verified == true) {
                    $curr_time = Carbon\Carbon::now()->toDateTimeString();
                    $input['payment_received'] = $curr_time;
                    $input['on_boarding'] = $curr_time;
                }*/

                $user_service = UserService::create($input);
            }

            //create transaction history and add renew trait
            if($user_service){
                if(isset($service['transaction_history_id']) && !empty($service['transaction_history_id'])){
                    $transaction_history = TransactionHistory::findOrFail($service['transaction_history_id']);
                } else {
                    unset($input);
                    $input = array(
                        'user_service_id' => $user_service->id,
                        'created_by' => Auth::user()->id,
                    );
                    $transaction_history = TransactionHistory::create($input);
                    DB::table('user_services')
                        ->where('id', $user_service->id)
                        ->update(['latest_transaction_history_id' => $transaction_history->id]);
                }
                if(isset($requestData['user_service_renew']) && $requestData['user_service_renew'] == 'yes'){
                    unset($input);
                    if(Carbon::now() > (new Carbon($services[1]['activation_date']))){
                        $input = [
                            'service_status' => 'Renewed',
                            'activation_date' => $service['activation_date'],
                        ];
                    } else {
                        $input = [
                            'service_status' => 'Active',
                            'activation_date' => $service['activation_date'],
                        ];
                    }
                    $user_service_updated = UserService::where('id', $service['user_service_id'])->update($input);
                }
            }



            //save user plan
            $user_plan = DB::table('plan_user')->where('plan_id', $service['fk_plan_id'])->where('user_id', $user->id)->get();
            if(!$user_plan){
                $user_plan = DB::table('plan_user')->insert(
                    ['plan_id' => $service['fk_plan_id'], 'user_id' => $user->id]
                );
            } else {
                $user_plan = 1;
            }

            //save transections
            unset($input);

            //calculate tax
            $pay_data = $this->getpayableAmount($service['fk_plan_id'], $service['discount']);

            //delete Old Transactions
            if(isset($requestData['user_service_edit']) && $requestData['user_service_edit'] == 'yes'){
                DB::table('transactions')->where('transaction_history_id', '=', $transaction_history->id)->delete();
            }

            $transaction = false;
            if( $service['fk_payment_type'] == 'cash' ){

                if($service['payment_type'] == 'full'){
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'transaction_history_id'=> $transaction_history->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'amount'                => $service['amount'],
                        'date'                  => $service['date'],
                        'transaction_id'        => $service['transaction_id'],
                        'received_from'         => $service['received_from'],
                        'discount'              => $service['discount'],
                        'service_fee'           => $pay_data['service_fee'],
                        'payable_amount'        => $pay_data['payable_amnt'],
                        'applied_tax'           => $pay_data['tax'],
                        'cleared'               => $service['cleared']
                    );
                } else {
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'discount'              => $service['discount'],
                        'service_fee'           => $pay_data['service_fee'],
                        'payable_amount'        => $pay_data['payable_amnt'],
                        'applied_tax'           => $pay_data['tax']
                    );
                }
                
                $transaction = Transection::create($input);
            }

            if( $service['fk_payment_type'] == 'cheque' ){
                if($service['payment_type'] == 'full'){
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'transaction_history_id'=> $transaction_history->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'amount'                => $service['amount'],
                        'date'                  => $service['date'],
                        'cheque_number'         => $service['cheque_number'],
                        'bank'                  => $service['bank'],
                        'discount'              => $service['discount'],
                        'service_fee'           => $pay_data['service_fee'],
                        'payable_amount'        => $pay_data['payable_amnt'],
                        'applied_tax'           => $pay_data['tax'],
                        'cleared'               => $service['cleared']
                    );
                } else {
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'discount'              => $service['discount'],
                        'service_fee'           => $pay_data['service_fee'],
                        'payable_amount'        => $pay_data['payable_amnt'],
                        'applied_tax'           => $pay_data['tax']
                    );
                }
                
                $transaction = Transection::create($input);
            }
            
            if( $service['fk_payment_type'] == 'net_banking' ){
                if($service['payment_type'] == 'full'){
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'transaction_history_id'=> $transaction_history->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'amount'                => $service['amount'],
                        'date'                  => $service['date'],
                        'transaction_id'        => $service['transaction_id'],
                        'received_from'         => $service['received_from'],
                        'type'                  => $service['type'],
                        'discount'              => $service['discount'],
                        'service_fee'           => $pay_data['service_fee'],
                        'payable_amount'        => $pay_data['payable_amnt'],
                        'applied_tax'           => $pay_data['tax'],
                        'cleared'               => $service['cleared']
                    );
                } else {
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'discount'              => $service['discount'],
                        'service_fee'           => $pay_data['service_fee'],
                        'payable_amount'        => $pay_data['payable_amnt'],
                        'applied_tax'           => $pay_data['tax']
                    );
                }
                
                $transaction = Transection::create($input);
            }


            /*
            if( $service['fk_payment_type'] == 'other' ){
                if($service['payment_type'] == 'full'){
                    //Do it Later
                } else {
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'discount'              => $service['discount'],
                        'service_fee'           => $pay_data['service_fee'],
                        'payable_amount'        => $pay_data['payable_amnt'],
                        'applied_tax'           => $pay_data['tax']
                    );

                    $transaction = Transection::create($input);
                }
            }
            */

            $clear_partial = false;
            if($service['payment_type'] == 'partial'){
                foreach ($service['transaction'] as $pay_no => $trans) {
                    unset($partial_transaction);
                    if(!empty($trans['amount'])){
                        $partial_transaction['fk_user_service_id'] = $user_service->id;
                        $partial_transaction['transaction_history_id'] = $transaction_history->id;
                        $partial_transaction['pay_type']       = $service['payment_type'];
                        $partial_transaction['discount']       = $service['discount'];
                        $partial_transaction['service_fee']    = $pay_data['service_fee'];
                        $partial_transaction['payable_amount'] = $pay_data['payable_amnt'];
                        $partial_transaction['applied_tax']    = $pay_data['tax'];
                        $partial_transaction['payment_type']   = $trans['payment_type'];
                        $partial_transaction['amount']         = $trans['amount'];
                        $partial_transaction['date']           = $trans['date'];
                        $partial_transaction['pay_no']         = $pay_no + 1;


                        if( $trans['payment_type'] == 'cash' ){
                            $partial_transaction['transaction_id'] = $trans['transaction_id'];
                            $partial_transaction['received_from']  = $trans['received_from'];
                            if(!empty($trans['transaction_id'])){
                               $partial_transaction['cleared']     = 'Yes';
                                $clear_partial = true;
                            }
                        }

                        if( $trans['payment_type'] == 'net_banking' ){
                            $partial_transaction['transaction_id'] = $trans['transaction_id'];
                            $partial_transaction['received_from']  = $trans['received_from'];
                            $partial_transaction['type']           = $trans['type'];
                            if(!empty($trans['transaction_id'])){
                               $partial_transaction['cleared']     = 'Yes';
                               $clear_partial = true;
                            }
                        }

                        if( $trans['payment_type'] == 'cheque' ){
                            $partial_transaction['cheque_number'] = $trans['cheque_number'];
                            $partial_transaction['bank']  = $trans['bank'];
                            $partial_transaction['cleared'] = $trans['cleared'];
                            if($trans['cleared'] == 'Yes')
                            {
                                $clear_partial = true;
                            }
                        }
                        $transaction = Transection::create($partial_transaction);
                    }
                }
            }


            if($user_service && $transaction && !$force_verified){
                $input = [];
                $pay_received = $user_service->payment_received;
                if($transaction->cleared == 'Yes' || ($service['payment_type'] == 'partial' &&  $clear_partial == true)){
                    if($pay_received == null) {
                        $pay_received = Carbon\Carbon::now();
                        $input = [
                            'payment_received' => $pay_received
                        ];
                    }
                }

                if(Carbon::now() > (new Carbon($services[1]['activation_date'])) && $user_service->on_boarding != null && $pay_received != null
                && $user->transcript_status == 'Yes' && $user->kyc_status == 'Yes' && $user->consent_status == 'Yes'){
                    $input['service_status'] = 'Active';
                } else {
                    $input['service_status'] = 'Pending';
                }
                $user_service_updated = UserService::where('id', $user_service->id)->update($input);
            }

            if( !$user || !$user_client || !$user_service || !$user_plan ) {
                DB::rollBack();
            } else {
                DB::commit();
                if((!isset($requestData['user_service_edit']) || $requestData['user_service_edit'] != 'yes') && (!isset($requestData['user_service_renew']) || $requestData['user_service_renew'] != 'yes')){
                    if(!isset($requestData['customer_id'])){
                        $to['user_email'] = $user->email;
                        $to['user_name'] = $user->name;
                        (new EmailController)->sendMail('send_kyc', ['user_by' => Auth::user(), 'user' => $user,'user_service' => $user_service], $to);
                        if(!$force_verified){
                            $to['user_email'] = $user->email;
                            $to['user_name'] = $user->name;
                            (new EmailController)->sendMail('verify_service', ['user' => $user,'user_service' => $user_service], $to);
                        }
                    }
                }
                if(isset($requestData['user_service_renew']) && $requestData['user_service_renew'] == 'yes'){
                    $to['user_email'] = $user->email;
                    $to['user_name'] = $user->name;
                    (new EmailController)->sendMail('service_renewed', ['user_by' => Auth::user(), 'user' => $user,'user_service' => $user_service], $to);
                }
            }
        }
        $user->perms()->sync([]);
        $user->roles()->sync(['13']);
        return $user->id;
    }

    private function getpayableAmount($plan_id='', $discount=0)
    {
        if(!empty($plan_id)){
            $plan = DB::table('plans')->where('id',$plan_id)->first();
            $service = DB::table('services')->where('id', $plan->service_id)->first();

            $taxes = json_decode($plan->applicable_tax, true);
            $tax = 0;
            foreach ($taxes as $value) {
                $got_tax = DB::table('configurations')->select('value')->where('id',$value)->first();
                $tax = $tax+$got_tax->value;
            }
            
            //get service fee
            if($service->payment_mode == 1){
                $service_fee = $plan->annual_fee;
            } else {
                $service_fee = $plan->service_fee;
            }
            
            //substract discount
            if($discount > 0){
                $pay_amnt = $service_fee-(($service_fee*$discount)/100);
            } else {
                $pay_amnt = $service_fee;
            }
            
            //add tax
            if($tax > 0){
                $pay_amnt = $pay_amnt+(($pay_amnt*$tax)/100);
            }

            $pay_amnt = round($pay_amnt, 0, PHP_ROUND_HALF_UP);

            return ['service_fee' => $service_fee, 'tax' => $tax, 'payable_amnt' => $pay_amnt];
        }
    }

    public function updateCustomer($requestData)
    {
        $input = array(
            'name' => $requestData['name'],
            'email' => $requestData['email'],
            'username' => $requestData['username'],
            'gender' => $requestData['gender'],
            'official_mobile' => $requestData['secondary_number'],
            'mobile' => $requestData['mobile'],
            'other_emails' => $requestData['other_emails'],
            'other_mobiles' => $requestData['other_mobiles'],
            'address1' => $requestData['address1'],
            'address2' => $requestData['address2'],
            'state' => $requestData['state'],
            'city' => $requestData['city'],
            'zip_code' => $requestData['zipcode'],
            'investor_type' => ($requestData['investor_type'] != '') ? $requestData['investor_type'] : null,
            'horizon' => ($requestData['horizon'] != '') ? $requestData['horizon'] : null

        );
        $user = User::findOrFail($requestData['user_id']);
        
        $user->fill($input)->save();
    }

    public function update($id, $requestData)
    {
        $client = Client::findOrFail($id);
        $client->fill($requestData->all())->save();
    }

    public function destroy($id)
    {
        try {
            $client = Client::find($id);

            $client->delete();
            
        } catch (\Illuminate\Database\QueryException $e) {
            Session()->flash('flash_message_warning', 'Client can NOT have, leads, or tasks assigned when deleted');
        }
    }
    
}
