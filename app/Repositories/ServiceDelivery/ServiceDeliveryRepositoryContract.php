<?php
namespace App\Repositories\ServiceDelivery;

interface ServiceDeliveryRepositoryContract
{

    public function find($id);

    public function create($requestData);

    public function update($id, $requestData);

    public function destroy($id);

    //
    public function getOpenServices($request);

    public function getServicesWithUserStatus($request);

    public function getAllDataQuery();

    public function deliveryReport($default_service = [], $delivery_range, $based_on = 'stock', $logged_in_user, $default_calltype);
   
}
