<?php
namespace App\Repositories\ServiceDelivery;

use App\BseDailyBhav;
use App\BseHistoricalBhav;
use App\Models\Filters\DeliveryFilters;
use App\Models\UserService;
use App\NfoDailyBhav;
use App\NfoHistoricalBhav;
use App\NseDailyBhav;
use App\NseHistoricalBhav;
use app\QueryFilter;
use App\Service;
use App\ServiceDelivery;
use App\ServiceDeliveryUser;
use App\Stock;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;


class ServiceDeliveryRepository implements ServiceDeliveryRepositoryContract
{

    public function find($id)
    {
        return ServiceDelivery::findOrFail($id);
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

   
    public function create($requestData)
    {
        ServiceDelivery::create($requestData);
    }


    public function update($id, $requestData)
    {
        $sd = ServiceDelivery::findOrFail($id);
        $sd->fill($requestData->all())->save();
    }

    public function destroy($id)
    {
        try {
            $sd = ServiceDelivery::find($id);
            $sd->delete();
            
        } catch (\Illuminate\Database\QueryException $e) {
            Session()->flash('flash_message_warning', 'Please try again');
        }
    }

    public function getAllDataQuery(){
        return ServiceDelivery::with(['services.service', 'template', 'sd_users'])->orderBy('id', 'Desc');
    }

    public function filterData(DeliveryFilters $filters ){
        return ServiceDelivery::filter($filters)
            ->select('service_deliveries.*', 'companies.name', 'companies.status', 'nse_daily_bhavs.last')
            ->leftJoin('companies', 'service_deliveries.symbol', '=', 'companies.symbol')
            ->leftJoin('nse_daily_bhavs', 'service_deliveries.symbol', '=', 'nse_daily_bhavs.symbol');
    }

    public function getOpenServices($request)
    {
        $sd = ServiceDelivery::selectRaw('services.*, service_deliveries.id as sd_ids')
            ->leftJoin('service_delivery_service', 'service_delivery_service.service_delivery_id', '=', 'service_deliveries.id')
            ->leftJoin('services', 'service_delivery_service.service_id', '=', 'services.id')
            ->where('symbol', $request->get('symbol'))
            ->where('exchange', $request->get('exchange'));

        if($request->get('exchange') == 'nfo') {
            if($request->get('nfo_instrument') == 'OPTIDX' || $request->get('nfo_instrument') == 'OPTSTK') {
                $sd = $sd->where('nfo_instrument', $request->get('nfo_instrument'))
                    ->where('expiry', Carbon::parse($request->get('expiry_date'))->toDateString())
                    ->where('option_type', $request->get('option_type'))
                    ->where('strike', $request->get('strike'));
            }
            else {
                $sd = $sd->where('nfo_instrument', $request->get('nfo_instrument'))
                    ->where('expiry', Carbon::parse($request->get('expiry_date'))->toDateString());
            }
        }

         $sd = $sd->whereRaw('service_delivery_service.service_id IS NOT NULL')
            //->groupBy('service_delivery_service.service_id')
            ->orderBy('services.name', 'desc')->get();


        $user_qry = User::select(['users.id', 'users.name','plans.name as plan_name','user_services.plan_id', 'user_services.service_id', 'user_services.service_capital'])
            ->where('user_type','Customer')
            /*->where('service_status','Active')*/
            ->whereIn('user_services.service_id',$sd->pluck('id'))
            ->leftJoin('user_services', 'users.id', '=', 'user_services.user_id')
            ->leftJoin('plans', 'plans.id', '=', 'user_services.plan_id')
            ->orderBy('users.id', 'desc');
        //return $user_qry->get();
        $existing_user_id = ServiceDeliveryUser::selectRaw('distinct(user_id)')
            ->whereIn('service_delivery_id',$sd->pluck('sd_ids'));
            if($request->get('exchange') == 'nfo') {
                $existing_user_id =  $existing_user_id ->havingRaw('Round(SUM( IF(call_type = "exit", - 1* nfo_qty,nfo_qty) ) , 2 ) >  0');
            }
            else {
                $existing_user_id =  $existing_user_id ->havingRaw('Round( SUM( IF(call_type = "exit", - 1*qty, qty)), 2 ) >  0');
            }
        $existing_user_id = $existing_user_id->groupBy(['user_id', 'plan_id'])
                    ->get()
                    ->pluck('user_id');

        $user_qry = $user_qry->whereIn('users.id', $existing_user_id);

        $base_users = $user_qry->groupBy(['users.id', 'user_services.service_id', 'user_services.plan_id'])->get();

        $symbol_data = [
            'symbol' => $request->get('symbol'),
            'exchange' => $request->get('exchange'),
            'expiry_date' => $request->get('expiry_date'),
            'option_type' => $request->get('option_type'),
            'strike' => $request->get('strike'),
            'nfo_instrument' => $request->get('nfo_instrument'),
            'qty' => $request->get('qty'),
            'lot_size' => $request->get('lot_size'),
        ];
        $users  = [];
        if($base_users) {
            foreach($base_users as $user) {
                $temp = $user->getStockSummary($symbol_data, $user->service_id, $user->plan_id ,$request->get('call_type'));
                if($temp) {
                    $arr = $user->toArray();
                    $arr['service_deliveries'] = $temp;
                    $users[] = $arr;
                }
            }

            if($users) {
                $users = collect($users)->reject(function ($value, $key) {
                    return $value['service_deliveries']['used_mapped_perten'] == 0;
                })->all();
            }

            if(count($users) == 0){
                return [];
            }

            $users = collect($users)->groupBy('service_deliveries.service_id');
            $sd = collect($sd)->keyBy('id')->toArray();

            foreach($users as $service_id => $user) {
                $sd[$service_id]['cust_5plus']['customers'] = $user->filter(function ($value, $key) {
                    return $value['service_deliveries']['used_mapped_perten'] >= 5;
                })->all();
                $sd[$service_id]['cust_5plus']['count'] =count($sd[$service_id]['cust_5plus']['customers']);

                $sd[$service_id]['cust_5less']['customers'] = $user->filter(function ($value, $key) {
                    return $value['service_deliveries']['used_mapped_perten'] < 5;
                })->all();
                $sd[$service_id]['cust_5less']['count'] =count($sd[$service_id]['cust_5less']['customers']);
            }

            foreach($sd as  $service_id => $data){
                if(!isset($sd[$service_id]['cust_5less']) && !isset($sd[$service_id]['cust_5plus']) )
                {
                    unset($sd[$service_id]);
                }
                else if((isset($sd[$service_id]['cust_5less']) && $sd[$service_id]['cust_5less']['count'] == 0)
                    && (isset($sd[$service_id]['cust_5plus']) && $sd[$service_id]['cust_5plus']['count'] == 0)){
                    unset($sd[$service_id]);
                }
            }

            return array_values($sd);
        }
        else {
            return [];
        }

    }

    public function getServicesWithUserStatus($request){
        $sd = Service::selectRaw('services.id')
            ->leftJoin('service_delivery_service', 'service_delivery_service.service_id', '=', 'services.id')
            ->leftJoin('service_deliveries', 'service_delivery_service.service_delivery_id', '=', 'service_deliveries.id')
            ->where('symbol', $request->get('symbol'))
            ->where('exchange', $request->get('exchange'));
            if($request->get('exchange') == 'nfo'){
                $sd->where('nfo_instrument', $request->get('nfo_instrument'))
                    ->where('expiry', Carbon::parse($request->get('expiry_date'))->toDateString())
                    ->where('option_type', $request->get('option_type'))
                    ->where('strike', $request->get('strike'));
            }
        return $sd = $sd->whereRaw('service_delivery_service.service_id IS NOT NULL')
            ->groupBy('service_delivery_service.service_id')
            ->orderBy('services.name', 'desc')->get()->pluck('id');


        $user_qry = User::select(['users.id', 'users.name','plans.name as plan_name','user_services.plan_id', 'user_services.service_id', 'user_services.service_capital'])
            ->where('user_type','Customer')
            /*->where('service_status','Active')*/
            ->whereIn('user_services.service_id',$sd->pluck('id'))
            ->leftJoin('user_services', 'users.id', '=', 'user_services.user_id')
            ->leftJoin('plans', 'plans.id', '=', 'user_services.plan_id')
            ->orderBy('users.id', 'desc');

        $existing_user_id = ServiceDeliveryUser::selectRaw('distinct(user_id)')
            ->leftJoin('service_deliveries', 'service_deliveries.id', '=', 'service_delivery_user.service_delivery_id')
            ->where('service_deliveries.symbol', $request->get('symbol'))
            ->where('service_deliveries.exchange', $request->get('exchange'))->get()->pluck('user_id');

        if($request->get('include_existing') == false)
            $user_qry = $user_qry->whereNotIn('users.id', $existing_user_id);
        else if(in_array($request->get('call_type'), ['re_entry', 'exit']))
            $user_qry = $user_qry->whereIn('users.id', $existing_user_id);

        $user_qry = $user_qry->whereIn('users.id', $existing_user_id);


        $base_users = $user_qry->groupBy(['users.id', 'user_services.service_id', 'user_services.plan_id'])->get();

        return [$sd, $base_users];
    }

    public function deliveryReportByService($default_service = [], $delivery_range, $services, $logged_in_user = false, $default_calltype = 'all', $combined = false){

        $services = collect($services);

        if($services->isEmpty()) {
            return [];
        }
        $services = $services->toArray();

        $pnl_services = $this->deliveryReport($default_service, $delivery_range, 'service', $logged_in_user, $default_calltype  , $combined);

        $user_count = User::selectRaw('user_services.service_id, count(users.id) as total')
            ->leftJoin('user_services', 'users.id', '=', 'user_services.user_id');
        if($default_service){
            $user_count = $user_count->whereIn('service_id',$default_service);
        }
        $user_count = $user_count->where('service_status','Active')
            ->groupBy('user_services.service_id')
            ->get()->pluck('total', 'service_id');
        $temp = [];
        foreach ($pnl_services as $key => $value) {
            array_set($temp, $key, $value);
        }
        $pnl_services = $temp;

        foreach ($pnl_services as $key => $value) {
            $temp = [];
            $temp['service_id'] = $key;
            $temp['service_name'] = $services[$key];
            $temp['close_call'] = 0;
            $temp['open_call'] = 0;
            $temp['net_roi']     = 0;
            $temp['avail_limit_user'] = 0;
            $temp['open_profit'] = 0;
            $temp['booked_profit'] = 0;
            $temp['total_profit'] = 0;
            $temp['positive_call'] = 0;
            $temp['gross_profit'] = 0;
            $temp['gross_loss'] = 0;
            $temp['months_pos'] = 0;
            $temp['today_pnl'] = 0;
            $temp['service_capital'] = 0;
            $temp['cmp'] = 0;
            $temp['prev_close'] = 0;


            foreach ($value as $stock) {
                $temp['close_call'] += $stock['close_call'];
                $temp['open_call'] += $stock['open_call'];

                $temp['positive_call'] += ($stock['profit_percent'] > 0) ? 1 : 0;

                $temp['gross_profit'] = round(bcadd($temp['gross_profit'],($stock['profit_percent'] >= 0) ? $stock['profit_percent'] : 0), 2);
                $temp['gross_loss'] = round(bcadd($temp['gross_loss'],($stock['profit_percent'] < 0) ? $stock['profit_percent'] : 0), 2);

                $temp['months_pos'] = round(bcadd($temp['months_pos'], $stock['months_pos']), 2);
                $temp['total_profit'] = round(bcadd($temp['total_profit'], $stock['total_profit']), 2);
                $temp['open_profit'] = round(bcadd($temp['open_profit'], $stock['open_profit']), 2);
                $temp['booked_profit'] = round(bcadd($temp['booked_profit'], $stock['booked_profit']), 2);
                $temp['today_pnl'] = round(bcadd($temp['today_pnl'], $stock['today_pnl']), 2);
                $temp['net_roi'] = round(bcadd($temp['net_roi'], $stock['net_roi']), 2);

                $temp['avail_limit_user'] += 1;
                $temp['service_capital'] = $stock['service_capital'];

                $temp['cmp'] = round(bcadd($temp['cmp'], $stock['cmp']), 2);
                $temp['prev_close'] = round(bcadd($temp['prev_close'], $stock['prev_close']), 2);


                foreach ($stock['user'] as $user_id => $user) {
                    if(!isset($temp['user'][$user_id])){
                        $temp['user'][$user_id] = $user;
                    } else {
                        $temp['user'][$user_id]['total_qty'] += $user['total_qty'];
                        $temp['user'][$user_id]['sell_qty'] += $user['sell_qty'];
                        $temp['user'][$user_id]['buy_qty'] += $user['buy_qty'];
                        $temp['user'][$user_id]['avail_qty'] += $user['avail_qty'];
                    }
                }
            }

            $temp['avail_user_total'] = count(array_where($temp['user'], function ($value, $key) {
                return $value['avail_qty'] < 100;
            }));
            $temp['user_total'] = $user_count[$key];
            $temp['total_call'] = $temp['open_call'] + $temp['close_call'];

            $temp['cumulative_return_all_call'] = $temp['gross_profit'] + $temp['gross_loss'];
            $temp['winning_percent'] = round(bcmul(bcdiv($temp['positive_call'] , $temp['total_call']) , 100), 2, PHP_ROUND_HALF_DOWN);

            $temp['months_pos'] = round(bcdiv($temp['months_pos'], count($value)), 2, PHP_ROUND_HALF_DOWN);

            $temp['net_roi'] = round(bcdiv( bcmul($temp['net_roi'] , 12) , $temp['months_pos']) , 2, PHP_ROUND_HALF_DOWN);

            if($temp['gross_loss'] != 0){
                $temp['r_v_r'] = round(bcdiv($temp['gross_profit'] , abs($temp['gross_loss'])), 2, PHP_ROUND_HALF_DOWN);
            } else {
                $temp['r_v_r'] = $temp['gross_profit'];
            }

            $pnl_services[$key] = ['stocks' => $pnl_services[$key]];
            $pnl_services[$key] += $temp;
        }
        return $pnl_services;
    }


    public function deliveryReport($default_service = [], $delivery_range = null ,$based_on = 'stock', $logged_in_user = null, $default_calltype = 'all', $combined = false, $default_stocks = [] ){

        $default_delivery_range = [date('d/m/Y', strtotime('10 year ago')),  date('d/m/Y', strtotime('today'))];
        if($delivery_range) {
            $default_delivery_range = $delivery_range;
        }
        $delivery_range =  array_map(
            function($date) { return date('Y-m-d', strtotime(str_replace('/', '-', $date))); },
            $default_delivery_range
        );
        $delivery_range[0] .= ' 00:00:00';
        $delivery_range[1] .= ' 23:59:59';

        $default_amount = 1000000;

        $call_type = 'all';
        if(in_array($default_calltype, ['Close', 'Open'])){
            $call_type = $default_calltype;
        }


        $delivery_fields = implode(',', array_keys(array_dot ([
            'service_deliveries' => array_flip([
                'symbol', 'exchange', 'instrument', 'expiry', 'option_type','nfo_instrument','call_type',
                'strike', 'price', 'range_from', 'range_to', 'stop_loss', 'date_time as entry_date', 'target_1', 'target_2',
            ]),
            'service_delivery_user' => array_flip([
                'id', 'service_delivery_service_id','service_delivery_id', 'user_id','service_id','plan_id',
                'call_type as service_call_type', 'qty', 'nfo_qty','value', 'msg'
            ]),
            'service_delivery_service'=> array_flip([
                'lot_size',
            ]),
            'user_services' =>  array_flip([
                'service_capital', 'service_status'
            ]),
            'users' =>  array_flip(['title', 'name as user_name', 'gender', 'username', 'email', 'mobile', 'other_emails', 'other_mobiles']),
            'services' =>  array_flip(['name as service_name', 'code as service_code']),
        ])));


        $deliveries = ServiceDeliveryUser::selectRaw($delivery_fields)
            ->leftJoin('service_deliveries', 'service_delivery_user.service_delivery_id', '=', 'service_deliveries.id')
            ->leftJoin('user_services', function($query){
                $query->on('service_delivery_user.service_id', '=', 'user_services.service_id')
                    ->on('service_delivery_user.user_id', '=', 'user_services.user_id')
                    ->on('service_delivery_user.service_id', '=', 'user_services.service_id')
                    ->on('service_delivery_user.plan_id', '=', 'user_services.plan_id');
            })
            ->leftJoin('users', 'users.id', '=', 'user_services.user_id')
            ->leftJoin('services', 'services.id', '=', 'user_services.service_id')
            ->leftJoin('service_delivery_service', 'service_delivery_service.id', '=', 'service_delivery_user.service_delivery_service_id');
        if(count($default_service) > 0)
            $deliveries = $deliveries->whereIn('service_delivery_user.service_id',$default_service);
                //->whereIn('user_services.service_id',$default_service);

        if($logged_in_user === true) {
            $deliveries = $deliveries->where('service_delivery_user.user_id',auth()->user()->id);
        } else if($logged_in_user){
            $deliveries = $deliveries->where('service_delivery_user.user_id',$logged_in_user);
        }


        $deliveries = $deliveries->whereBetween('service_delivery_user.created_at', $delivery_range)
            /*->where('user_services.service_status','Active')
            ->where('service_delivery_user.symbol','IOC')
            ->where('service_deliveries.exchange','nfo')*/
            ->orderBy('service_delivery_user.id', 'asc')
            ->orderBy('service_deliveries.date_time', 'asc')
            ->get();


        $pnl = [];

        foreach ($deliveries as $delivery){

            $exp_date = ($delivery->instrument != 'stock') ? Carbon::parse($delivery->expiry)->timestamp : null;
            $key_name = [$delivery->exchange, str_slug($delivery->symbol), $delivery->nfo_instrument, $exp_date, $delivery->option_type, $delivery->strike];
            $key_name = implode('_', array_filter($key_name));

            $key_name = $key_name . ".users.".$delivery->user_id;

            if($based_on == 'service'){
                $key_name = $delivery->service_id. "__" .$key_name;
            }

            if($delivery->service_capital != 0){
                $default_amount = (float) $delivery->service_capital;
            }

            if(!isset($pnl[$key_name])) {
                $pnl[$key_name] = [
                    'symbol' => $delivery->symbol,
                    'exchange' => $delivery->exchange,
                    'instrument' => $delivery->instrument,
                    'nfo_instrument' => $delivery->nfo_instrument,
                    'option_type' => $delivery->option_type,
                    'strike' => $delivery->strike,
                    'expiry_date' => $delivery->expiry,
                    'lot_size' => $delivery->lot_size,
                    'nfo_qty' => 0,
                    'status' => 0,
                    'entry_date' => date(config('rothmans.date_format'), strtotime('today')),
                    'exit_date' => date(config('rothmans.date_format'), strtotime('today')),
                    'total_qty' => 0,
                    'buy_qty' => 0,
                    'sell_qty' => 0,
                    'total_stock' => 0,
                    'total_stock_till_yesterday' => 0,
                    'buy_stock' => 0,
                    'sell_stock' => 0,
                    'avail_qty' => 0,
                    'avail_stock' => 0,
                    'investment' => 0,
                    'entry_price' => 0,
                    'exit_price' => 0,
                    'open_profit' => 0,
                    'booked_profit' => 0,
                    'avg_price' => 0,
                    'target_mean' => 0,
                    'sl_mean' => 0,
                    'open_user' => 0,
                    'target_del_count' => 0,
                    'sl_del_count' => 0,
                    'users' => [],
                    'open_call' => 0,
                    'close_call' => 0,
                    'total_call' => 0,
                    'months_pos' => 0,
                    'abs_return' => 0,
                    'symbol_status' => 'Close',
                    'ultimate_profit' => 0,
                    'service_capital' => $default_amount,
                    'cmp' => 0,
                    'prev_close' => 0,
                    'service_id' => $delivery->service_id,
                    'service_name' => $delivery->service_name,
                    'plan_id' => $delivery->plan_id,
                ];
            }

            $temp = $pnl[$key_name];
            $delivery_arr = $delivery->toArray();
            $deliver_qty = 0;

            if($delivery->service_call_type == 'exit') {
                if($delivery->exchange != 'nfo') {
                    if ($temp['buy_qty'] > 0) {
                        $deliver_qty = round($temp['total_stock'] * ($delivery->qty / $temp['total_qty']));
                        $temp['booked_profit'] += bcsub($delivery->price , $temp['avg_price']) * $deliver_qty;
                    } else {
                        continue;
                    }
                    $temp['ultimate_profit'] += bcsub($delivery->price, $temp['avg_price']) * $deliver_qty;

                    $temp['total_qty'] -= $delivery->qty;
                    $temp['sell_qty'] += $delivery->qty;
                    $temp['avail_qty'] -= $delivery->qty;
                    $temp['exit_price'] = $delivery->price;

                } else {
                    if ($temp['buy_qty'] > 0) {
                        $deliver_qty = $delivery->nfo_qty * $delivery->lot_size;
                        $delivery_percent = round(bcmul(bcdiv(bcmul($temp['avg_price'], $deliver_qty), $default_amount), 100), 1, PHP_ROUND_HALF_DOWN);
                        $temp['booked_profit'] += bcsub($delivery->value , bcmul($temp['avg_price'], $deliver_qty ));
                    } else {
                        continue;
                    }
                    $temp['ultimate_profit'] += bcsub($delivery->value, bcmul($temp['avg_price'], $deliver_qty ));

                    $temp['total_qty'] = bcsub($temp['total_qty'], $delivery_percent);
                    $temp['sell_qty'] = bcadd($temp['sell_qty'], $delivery_percent);
                    $temp['avail_qty'] = bcsub($temp['avail_qty'], $delivery_percent);
                    $temp['exit_price'] = $delivery->price;

                    $temp['nfo_qty'] -= $delivery->nfo_qty;
                }

                $temp['total_stock'] -= $deliver_qty;
                $temp['sell_stock'] += $deliver_qty;
                $temp['avail_stock'] -= $deliver_qty;


                if($temp['total_stock_till_yesterday'] > 0)
                    $temp['total_stock_till_yesterday'] -= $deliver_qty;


                $delivery_arr['status'] = 'Partial Open';
                $temp['symbol_status'] = 'Partial Open';

                if(Carbon::parse($temp['exit_date'])->lte(Carbon::parse($delivery->entry_date)))
                {
                    $temp['exit_date'] = Carbon::parse($delivery->entry_date)->toDateString();
                }

                if($temp['open_call'] > 0 && $temp['total_qty'] == 0)
                {
                    $temp['open_call'] --;
                    $temp['close_call'] ++;
                    $delivery_arr['status'] = 'Close';
                    $temp['symbol_status'] = 'Close';
                }
            }
            else{
                if($temp['total_qty'] == 0 )
                {
                    $temp['open_call'] ++;
                }
                if($temp['symbol_status'] == 'Close'){
                    $delivery_arr['status'] = 'Open';
                    $temp['symbol_status'] = 'Open';
                } else {
                    $delivery_arr['status'] = $temp['symbol_status'];
                }

                if($temp['entry_price'] == 0 || $temp['total_qty'] == 0){
                    $temp['entry_price'] = $delivery->price;
                }

                if(Carbon::parse($temp['entry_date'])->gt(Carbon::parse(date(config('rothmans.date_format'), strtotime($delivery->entry_date)))))
                {
                    $temp['entry_date'] = date(config('rothmans.date_format'), strtotime($delivery->entry_date));
                }

                if($delivery->exchange != 'nfo') {
                    $base_amt = $default_amount * $delivery->qty / 100;
                    $deliver_qty = round( $base_amt / $delivery->price, 0);

                    $temp['total_qty'] += $delivery->qty;
                    $temp['buy_qty'] += $delivery->qty;

                    $temp['total_stock'] += $deliver_qty;
                    $temp['buy_stock'] += $deliver_qty;

                    if(!Carbon::parse($delivery->entry_date)->isToday())
                        $temp['total_stock_till_yesterday'] += $deliver_qty;

                    $temp['investment'] = bcadd($temp['investment'], bcmul(intval($deliver_qty), floatval($delivery->price)));

                    $temp['avg_price'] = round((
                            ($temp['avail_stock'] * $temp['avg_price'])
                            +
                            ($deliver_qty * $delivery->price)
                        ) / ($temp['avail_stock'] + $deliver_qty)
                        ,2, PHP_ROUND_HALF_DOWN);


                    $temp['avail_qty'] += $delivery->qty;
                    $temp['avail_stock'] += $deliver_qty;

                } else {
                    $deliver_qty = $delivery->nfo_qty * $delivery->lot_size;
                    $delivery_percent = round(bcmul(bcdiv($delivery->value, $default_amount), 100), 1, PHP_ROUND_HALF_DOWN);

                    $temp['total_qty'] += $delivery_percent;
                    $temp['buy_qty'] += $delivery_percent;

                    $temp['total_stock'] += $deliver_qty;
                    $temp['buy_stock'] += $deliver_qty;

                    $temp['nfo_qty'] += $delivery->nfo_qty;

                    if(!Carbon::parse($delivery->entry_date)->isToday())
                        $temp['total_stock_till_yesterday'] += $deliver_qty;

                    $temp['investment'] = bcadd($temp['investment'], $delivery->value);

                    $temp['avg_price'] = round( bcdiv(bcadd($temp['avg_price'], $delivery->value),  bcadd($temp['avail_stock'], $deliver_qty)),2, PHP_ROUND_HALF_DOWN);


                    $temp['avail_qty'] += $delivery_percent;
                    $temp['avail_stock'] += $deliver_qty;
                }

                if($delivery->stop_loss != 0) {
                    $temp['sl_mean'] += $delivery->stop_loss;
                    $temp['sl_del_count'] += 1;
                }

                if($delivery->target_1 != 0){
                    if($delivery->target_2 != 0){
                        $temp['target_mean'] += ($delivery->target_1 + $delivery->target_2) / 2;
                    } else {
                        $temp['target_mean'] += $delivery->target_1;
                    }

                    $temp['target_del_count'] += 1;
                }
            }

            if($temp['symbol_status'] != 'Open'){
                $delivery_arr['booked_profit'] = round($temp['booked_profit'], 2, PHP_ROUND_HALF_DOWN);
            } else {
                $delivery_arr['booked_profit'] = 0;
            }

            if(isset($temp['deliveries']) && count($temp['deliveries']) > 0) {
                $curr_price = $delivery->price;
                $last_del_key = key( array_slice( $temp['deliveries'], -1, 1, TRUE ));
                $last_delivery = $temp['deliveries'][$last_del_key];

                $last_delivery['open_profit'] = round(
                    bcmul(bcsub($curr_price, $last_delivery['avg_price']), $last_delivery['total_stock'])
                    , 2 , PHP_ROUND_HALF_DOWN);
                $last_delivery['total_profit'] = bcadd($last_delivery['open_profit'] , $last_delivery['booked_profit']);

                $temp['deliveries'][$last_del_key] = $last_delivery;
            }


            $delivery_arr['avg_price'] = $temp['avg_price'];
            $delivery_arr['curr_qty'] = $deliver_qty;
            $delivery_arr['total_stock'] = $temp['total_stock'];
            $delivery_arr['open_profit'] = 0;

            $delivery_arr['total_profit'] = bcadd($delivery_arr['booked_profit'], $delivery_arr['open_profit']);

            $temp['deliveries'][] = $delivery_arr;

            $pnl[$key_name] = $temp;

            if($temp['symbol_status'] == 'Close' && $combined === false) {
                unset($pnl[$key_name]);
                $key_name = str_replace(".users.".$delivery->user_id, uniqid().".users.".$delivery->user_id, $key_name);
                $pnl[$key_name] = $temp;
            }
        }

        //echo '<pre>'; print_r($pnl); echo '</pre>';
        //return [];
        foreach ($pnl as $key => $val) {
            if($val['exchange'] == 'nse') {
                $bhav = NseDailyBhav::selectRaw("Distinct(trim(symbol)) as symbol, ROUND(last,2) as price, prevclose as prev_close")->whereRaw("trim(symbol) LIKE '".$val['symbol']."'")->where('series', 'LIKE', 'EQ')->orderBy('timestamp', 'desc')->first();
            }
            elseif($val['exchange'] == 'bse') {
                $bhav = BseDailyBhav::selectRaw("Distinct(trim(sc_name)) as symbol, ROUND(last,2) as price, prevclose as prev_close")->whereRaw("trim(sc_name) LIKE '".$val['symbol']."'")->orderBy('timestamp', 'desc')->first();
            }
            elseif($val['exchange'] == 'nfo') {
                $bhav = NfoDailyBhav::selectRaw("Distinct(trim(symbol)) as symbol, ROUND(close,2) as price, ROUND(setle_pr,2) as prev_close, expiry_date")->whereRaw("trim(symbol) LIKE '".$val['symbol']."'")->where('instrument', 'LIKE', $val['nfo_instrument'])->where('option_type', 'LIKE', $val['option_type'])->where('expiry_date', 'LIKE', $val['expiry_date'])->where('strike', 'LIKE', $val['strike'])->first();
            }

            if(!isset($bhav)){
                $bhav = new NfoDailyBhav();
            }
            $curr_price = $bhav->price;
            $val['cmp'] = $bhav->price;
            $val['prev_close'] = $bhav->prev_close;

            $current_investment = $val['investment'];

            if($val['total_qty'] == 0 && $val['buy_qty'] > 0){
                $val['status'] = 'Close';
                $val['booked_profit'] = round($val['booked_profit'], 2, PHP_ROUND_HALF_DOWN);
                $val['open_stock_weight'] = 0;
                $val['close_stock_weight'] = round($val['sell_qty'],2, PHP_ROUND_HALF_DOWN);

            } elseif($val['sell_qty'] == 0){
                $val['status'] = 'Open';
                $val['open_profit'] = round(bcsub(bcmul($curr_price , $val['total_stock']) , bcmul($val['avg_price'] , $val['total_stock'])), 2 , PHP_ROUND_HALF_DOWN);
                $val['open_stock_weight'] = round($val['total_qty'], 2, PHP_ROUND_HALF_DOWN);
                $val['close_stock_weight'] = 0;
                $val['exit_price'] = $curr_price;
                $val['open_user'] = 1;
            } else {
                $val['status'] = 'Partial Open';
                $val['booked_profit'] = round($val['booked_profit'], 2,PHP_ROUND_HALF_DOWN);
                $val['open_profit'] = round(
                    bcsub(bcmul($curr_price , $val['total_stock']) , bcmul($val['avg_price'] , $val['total_stock']))
                    , 2 , PHP_ROUND_HALF_DOWN);
                $val['open_stock_weight'] = round($val['total_qty'], 2, PHP_ROUND_HALF_DOWN);
                $val['close_stock_weight'] = round($val['sell_qty'], 2, PHP_ROUND_HALF_DOWN);
                $val['exit_price'] = $curr_price;

                $val['open_user'] = 1;
                $val['exit_date'] = date(config('rothmans.date_format'), strtotime('today'));
            }

            if(isset($val['deliveries']) && count($val['deliveries']) > 0) {
                $last_del_key = key( array_slice( $val['deliveries'], -1, 1, TRUE ));
                $last_delivery = $val['deliveries'][$last_del_key];

                $last_delivery['open_profit'] = round(
                    bcsub(bcmul($curr_price , $last_delivery['total_stock']) ,  bcmul($last_delivery['avg_price'] , $last_delivery['total_stock']))
                    , 2 , PHP_ROUND_HALF_DOWN);
                $last_delivery['total_profit'] = bcadd($last_delivery['open_profit'] , $last_delivery['booked_profit']);

                $val['deliveries'][$last_del_key] = $last_delivery;
            }

            $val['sl_del_count'] = ($val['sl_del_count'] > 0) ? $val['sl_del_count'] : 1;
            $val['target_del_count'] = ($val['target_del_count'] > 0) ? $val['target_del_count'] : 1;

            $val['sl_mean'] = round(bcdiv($val['sl_mean'] , $val['sl_del_count']) ,2, PHP_ROUND_HALF_DOWN);
            $val['target_mean'] = round(bcdiv($val['target_mean'] , $val['target_del_count']) ,2, PHP_ROUND_HALF_DOWN);

            if($curr_price != 0){
                $val['sl_p_away'] = round(bcdiv(bcsub($curr_price, $val['sl_mean']) , $curr_price ) * 100 , 2, PHP_ROUND_HALF_DOWN);
                $val['target_p_away'] = round(bcdiv( bcsub($val['target_mean'] , $curr_price) , $curr_price) * 100 , 2, PHP_ROUND_HALF_DOWN);
            }else{
                $val['sl_p_away'] = 0;
                $val['target_p_away'] = 0;
            }

            $val['total_profit'] = round(bcadd($val['booked_profit'] , $val['open_profit']), 2, PHP_ROUND_HALF_DOWN);

            if($current_investment == 0)
            {
                dd($val);
            }
            $val['profit_percent'] = round(bcmul(bcdiv($val['total_profit'], $current_investment), 100), 2, PHP_ROUND_HALF_DOWN);

            $val['total_stock_weight'] = bcadd($val['open_stock_weight'], $val['close_stock_weight']);

            $val['net_roi'] = round( bcdiv( bcmul($val['profit_percent'], $val['total_stock_weight']) , 100) , 2, PHP_ROUND_HALF_DOWN);

            $days_held = Carbon::parse($val['exit_date'])->diffInDays(Carbon::parse($val['entry_date'])) > 0 ? Carbon::parse($val['exit_date'])->diffInDays(Carbon::parse($val['entry_date'])) : 1;
            $val['months_pos'] = round( bcdiv($days_held , 30), 2, PHP_ROUND_HALF_DOWN);

            $val['abs_return'] = round(bcdiv( bcmul($val['profit_percent'] , 25) , 100) , 2, PHP_ROUND_HALF_DOWN);

            $val['today_pnl'] = round(bcmul( bcsub($curr_price , $bhav->prev_close) , $val['total_stock_till_yesterday']) , 2, PHP_ROUND_HALF_DOWN);

            $pnl[$key] = $val;
        }

        $temp = [];
        foreach ($pnl as $key => $value) {
            array_set($temp, $key, $value);
        }
        $pnl = $temp;
        foreach($pnl as $key => $val){

            if($call_type != 'all'){
                $allow_pnl = true;
                if($call_type == 'Open' && current($val['users'])['status'] == 'Close')
                {
                    $allow_pnl = false;
                }
                else if($call_type == 'Close' &&  in_array(current($val['users'])['status'], ['Open', 'Partial Open']))
                {
                    $allow_pnl = false;
                }

                if(!$allow_pnl){
                    unset($pnl[$key]);
                    continue;
                }
            }

            $stock = [
                'symbol' => current($val['users'])['symbol'],
                'exchange' => current($val['users'])['exchange'],
                'instrument' => current($val['users'])['instrument'],
                'nfo_instrument' => current($val['users'])['nfo_instrument'],
                'option_type' => current($val['users'])['option_type'],
                'strike' => current($val['users'])['strike'],
                'expiry_date' => current($val['users'])['expiry_date'],
                'status' => current($val['users'])['status'],
                'entry_date' => current($val['users'])['entry_date'],
                'exit_date' => current($val['users'])['exit_date'],
                'total_qty' => 0,
                'buy_qty' => 0,
                'sell_qty' => 0,
                'total_stock' => 0,
                'buy_stock' => 0,
                'sell_stock' => 0,
                'avail_qty' => 0,
                'avail_stock' => 0,
                'entry_price' => 0,
                'exit_price' => 0,
                'open_profit' => 0,
                'booked_profit' => 0,
                'avg_price' => 0,
                'target_mean' => 0,
                'sl_mean' => 0,
                'open_user' => 0,
                'target_del_count' => 0,
                'sl_del_count' => 0,
                'open_call' => 0,
                'close_call' => 0,
                'total_call' => 0,
                'months_pos' => 0,
                'abs_return' => 0,
                'ultimate_profit' => 0,
                'deliveries' => [],
                'sl_p_away' => 0,
                'target_p_away' => 0,
                'total_profit' => 0,
                'profit_percent' => 0,
                'total_stock_weight' => 0,
                'net_roi' => 0,
                'open_stock_weight' => 0,
                'close_stock_weight' => 0,
                'service_capital' => 0,
                'cmp' => 0,
                'prev_close' => 0,
            ];
            foreach($val['users'] as $user_id => $usr_data){

                $stock['total_qty'] += $usr_data['total_qty'];
                $stock['buy_qty'] += $usr_data['buy_qty'];
                $stock['sell_qty'] += $usr_data['sell_qty'];
                $stock['total_stock'] += $usr_data['total_stock'];
                $stock['buy_stock'] += $usr_data['buy_stock'];
                $stock['sell_stock'] += $usr_data['sell_stock'];
                $stock['avail_qty'] += $usr_data['avail_qty'];
                $stock['avail_stock'] += $usr_data['avail_stock'];
                $stock['entry_price'] += $usr_data['entry_price'];
                $stock['exit_price'] += $usr_data['exit_price'];
                $stock['open_profit'] += $usr_data['open_profit'];
                $stock['booked_profit'] += $usr_data['booked_profit'];
                $stock['avg_price'] += $usr_data['avg_price'];
                $stock['target_mean'] += $usr_data['target_mean'];
                $stock['sl_mean'] += $usr_data['sl_mean'];
                $stock['open_user'] += $usr_data['open_user'];
                $stock['target_del_count'] += $usr_data['target_del_count'];
                $stock['sl_del_count'] += $usr_data['sl_del_count'];
                $stock['open_call'] += $usr_data['open_call'];
                $stock['close_call'] += $usr_data['close_call'];
                $stock['total_call'] += $usr_data['total_call'];
                $stock['months_pos'] += $usr_data['months_pos'];
                $stock['abs_return'] += $usr_data['abs_return'];
                $stock['ultimate_profit'] += $usr_data['ultimate_profit'];
                $stock['deliveries'] += $usr_data['deliveries'];

                $stock['open_stock_weight'] += $usr_data['open_stock_weight'];
                $stock['close_stock_weight'] += $usr_data['close_stock_weight'];
                $stock['sl_del_count'] += $usr_data['sl_del_count'];
                $stock['target_del_count'] += $usr_data['target_del_count'];
                $stock['sl_mean'] += $usr_data['sl_mean'];
                $stock['target_mean'] += $usr_data['target_mean'];
                $stock['sl_p_away'] += $usr_data['sl_p_away'];
                $stock['target_p_away'] += $usr_data['target_p_away'];
                $stock['total_profit'] += $usr_data['total_profit'];
                $stock['profit_percent'] += $usr_data['profit_percent'];
                $stock['total_stock_weight'] += $usr_data['total_stock_weight'];
                $stock['net_roi'] += $usr_data['net_roi'];
                $stock['today_pnl'] = $usr_data['today_pnl'];
                $stock['service_capital'] = $usr_data['service_capital'];
                $stock['cmp'] = $usr_data['cmp'];
                $stock['prev_close'] = $usr_data['prev_close'];
                $stock['services'][$usr_data['service_id']] = $usr_data['service_name'];

            }

            $stock['deliveries'] = collect($stock['deliveries'])->sortBy('id')->toArray();
            $stock['user'] = $val['users'];
            $pnl[$key] = $stock;
        }

        if($based_on == 'service'){
            foreach($pnl as $key => $val) {
                $new_key = str_replace('__', '.', $key);
                $pnl[$new_key] = $val;
                unset($pnl[$key]);
            }
        }
        return $pnl;
    }


    public function pNLSummery( $services, $logged_in_user = false){
        if($logged_in_user === false) {
            $logged_in_user = auth()->user()->id;
        }
        $reports = $this->deliveryReportbyService([], [], $services, $logged_in_user);

        $user_services = UserService::where('user_id', $logged_in_user)->orderBy('id', 'desc')->where('service_status', '<>', 'Deleted')->get();

        $result = [
            'total_calls' => 0,
            'total_profit' => 0,
            'positive_call' => 0,
            'gross_profit' => 0,
            'gross_loss' => 0,
            'today_pnl' => 0,
            'service_count' => 0,
            'total_capital' => 0,
        ];

        $default_result = [
            'total_call' => 0,
            'total_profit' => 0,
            'positive_call' => 0,
            'gross_profit' => 0,
            'gross_loss' => 0,
            'today_pnl' => 0,
            'service_count' => 0,
            'total_capital' => 0,
        ];

        foreach($user_services as $service) {
            $default_result['service_id'] = $service->service_id;
            $default_result['service_name'] = json_decode($service->service_details)->name;

            $report = isset($reports[$service->service_id]) ? $reports[$service->service_id] : $default_result;

            $result['total_calls'] += $report['total_call'];
            $result['total_profit'] += $report['total_profit'];
            $result['positive_call'] += $report['positive_call'];
            $result['gross_profit'] += $report['gross_profit'];
            $result['gross_loss'] += $report['gross_loss'];
            $result['today_pnl'] += $report['today_pnl'];
            $result['total_capital'] += $service->service_capital;
            $result['service_count'] ++;

            $result['services'][$service->service_id] = $report;
        }

        return $result;
    }

    public function stockWatchList(){
        $stock_reports = $this->deliveryReport([],[], 'stock', null, 'all', true);

        $reports = Stock::where('watchlist', true)->where('created_by', auth()->user()->id)->get()->take(10);
        if($reports)
        {
            $reports = $reports->toArray();
        }

        foreach($reports as $key => $report) {

            $key_name = [$report['exchange'], str_slug($report['symbol']), $report['instrument'], $report['option_type'], $report['strike']];
            $key_name = implode('_', array_filter($key_name));


            if($report['exchange'] == 'nse') {
                $bhav_min = NseHistoricalBhav::selectRaw("ROUND(last,2) as lowest, bhav_date")
                            ->whereRaw("trim(symbol) LIKE '".$report['symbol']."'")
                            ->where('series', 'LIKE', 'EQ')
                            ->whereRaw("last = (SELECT MIN(`last`) from `nse_historical_bhavs` WHERE `symbol` = '".$report['symbol']."' and series like 'EQ')")
                            ->first();

                $bhav_max = NseHistoricalBhav::selectRaw("ROUND(last,2) AS  `highest`, bhav_date")
                    ->whereRaw("trim(symbol) LIKE '".$report['symbol']."'")
                    ->where('series', 'LIKE', 'EQ')
                    ->whereRaw("last = (SELECT MAX(`last`) from `nse_historical_bhavs` WHERE `symbol` = '".$report['symbol']."' and series like 'EQ')")
                    ->first();
            }
            elseif($report['exchange'] == 'bse') {
                $bhav_min = BseHistoricalBhav::selectRaw("ROUND(last,2) as lowest, bhav_date")
                    ->whereRaw("trim(symbol) LIKE '".$report['symbol']."'")
                    ->where('series', 'LIKE', 'EQ')
                    ->whereRaw("last = (SELECT MIN(`last`) from `bse_historical_bhavs` WHERE `symbol` = '".$report['symbol']."')")
                    ->first();

                $bhav_max = BseHistoricalBhav::selectRaw("ROUND(last,2) AS  `highest`, bhav_date")
                    ->whereRaw("trim(sc_name) LIKE '".$report['symbol']."'")
                    ->whereRaw("last = (SELECT MAX(`last`) from `bse_historical_bhavs` WHERE `sc_name` = '".$report['symbol']."')")
                    ->first();
            }
            elseif($report['exchange'] == 'nfo') {
                if($report['option_type'] == '') {
                    $report['option_type'] = 'XX';
                }
                $bhav_min = NfoHistoricalBhav::selectRaw("MIN(ROUND(close,2)) AS  `lowest` , bhav_date")
                    ->whereRaw("trim(symbol) LIKE '".$report['symbol']."'")
                    ->where('instrument', 'LIKE', $report['instrument'])
                    ->where('option_type', 'LIKE', $report['option_type'])
                    ->where('expiry_date', 'LIKE', $report['expiry'])
                    ->where('strike', 'LIKE', $report['strike'])
                    ->whereRaw("close = (select MIN(ROUND(close,2)) from `nfo_historical_bhavs` where trim(symbol) LIKE '".$report['symbol']."' and `instrument` LIKE '".$report['instrument']."' and `option_type` LIKE '".$report['option_type']."' and `expiry_date` LIKE '".$report['expiry']."' and `strike` LIKE '".$report['strike']."' )")
                    ->first();

                $bhav_max = NfoHistoricalBhav::selectRaw("MAX(ROUND(close,2)) AS  `highest` , bhav_date")
                    ->whereRaw("trim(symbol) LIKE '".$report['symbol']."'")
                    ->where('instrument', 'LIKE', $report['instrument'])
                    ->where('option_type', 'LIKE', $report['option_type'])
                    ->where('expiry_date', 'LIKE', $report['expiry'])
                    ->where('strike', 'LIKE', $report['strike'])
                    ->whereRaw("close = (select MAX(ROUND(close,2)) from `nfo_historical_bhavs` where trim(symbol) LIKE '".$report['symbol']."' and `instrument` LIKE '".$report['instrument']."' and `option_type` LIKE '".$report['option_type']."' and `expiry_date` LIKE '".$report['expiry']."' and `strike` LIKE '".$report['strike']."' )")
                    ->first();
            }
            if(!$report['prev_close'])
                continue;

            $temp = [
                'name' => $report['symbol'],
                'cmp' => $report['cmp'],
                'prev_close' => $report['prev_close'],
                'change' => number_format($report['cmp'] - $report['prev_close'],2),
                'change_p' => number_format((($report['cmp'] - $report['prev_close']) / $report['prev_close']) * 100, 2),
                'gain_loss' => number_format($report['cmp'] - $report['prev_close'], 2),
                'gain_loss_p' => number_format((( $report['cmp'] - $report['prev_close']) / $report['prev_close']) * 100, 2),
                'stock_high' => $bhav_max->highest,
                'stock_high_date' => $bhav_max->bhav_date,
                'stock_low' => $bhav_min->lowest,
                'stock_low_date' => $bhav_min->bhav_date,
                'services' => (isset($stock_reports[$key_name])) ? $stock_reports[$key_name]['services'] : [],
            ];

            $reports[$key] = $report + $temp;
        }

        return $reports;
    }

    public function serviceWatchlist(){
        $result = [];

        $services = Service::all()->where('watchlist', 1);

        if($services->count() > 0) {
            $reports = $this->deliveryReportByService($services->pluck('id'),[], $services->pluck('name', 'id'), false, true);

            $temp = [
                'cmp' => 0,
                'prev_close' => 0,
                'today_pnl' => 0,
            ];

            foreach($services as $service) {
                $report = isset($reports[$service->id]) ? $reports[$service->id] : $temp;
                $result[] = [
                    'id' => $service->id,
                    'name' => $service->name,
                    'cmp' => ($report['cmp'] != 0) ?  $report['cmp'] : 'NA',
                    'prev_close' =>($report['prev_close'] != 0) ?  $report['prev_close'] : 'NA',
                    'change' => ($report['prev_close'] != 0) ? number_format($report['cmp'] - $report['prev_close'],2) : 'NA',
                    'change_p' => ($report['prev_close'] != 0) ? number_format((($report['cmp'] - $report['prev_close']) / $report['prev_close']) * 100, 2) : 'NA',
                    'gain_loss' => ($report['prev_close'] != 0) ? number_format($report['cmp'] - $report['prev_close'], 2) : 'NA',
                    'gain_loss_p' => ($report['prev_close'] != 0) ? number_format((( $report['cmp'] - $report['prev_close']) / $report['prev_close']) * 100, 2) : 'NA',
                    'today_pnl' => ($report['today_pnl'] != 0) ? $report['today_pnl']: 'NA',
                ];

            }
        }

        return $result;
    }

}