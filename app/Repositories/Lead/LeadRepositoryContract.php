<?php
namespace App\Repositories\Lead;

interface LeadRepositoryContract
{

    public function find($id);
    
    public function create($requestData);

    public function updateStatus($id, $requestData);

    public function updateFollowup($id, $requestData);

    public function update($id, $requestData);

    public function updateAssign($id, $requestData);

    public function updateBasicInfo($id, $requestData);

    public function allLeads();

    public function allCompletedLeads();

    public function percantageCompleted();

    public function completedLeadsToday();

    public function createdLeadsToday();

    public function completedLeadsThisMonth();

    public function createdLeadsMonthly();

    public function completedLeadsMonthly();
}
