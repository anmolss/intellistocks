<?php
namespace App\Repositories\Lead;

use App\Models\Leads;
use App\Models\Logs;
use App\Models\LeadFollowups;
use App\Models\UserClientMeta;
use App\Models\UserService;
use App\Models\Transection;
use Notifynder;
use Carbon;
use Config;
use DB;
use App\User;
use Datatables;
use Notification;
use App\Notifications\LeadAssigned;

class LeadRepository implements LeadRepositoryContract
{

    public function find($id)
    {
        return Leads::findOrFail($id);
    }

    public function create($requestData)
    {
        

        $fk_client_id = $requestData->get('fk_client_id');
        $input = $requestData = array_merge(
            $requestData->all(),
            ['fk_user_id_created' => \Auth::id(),
             'contact_date' => $requestData->contact_date ." " . $requestData->contact_time . ":00",
             'open_date' => $requestData->open_date ." " . $requestData->open_time . ":00",
             'close_date' => $requestData->close_date ." " . $requestData->close_time . ":00"]
        );

        $lead = Leads::create($input);

        $insertedId = $lead->id;
        // Session()->flash('flash_message', 'Lead successfully added!'); //Snippet in Master.blade.php
        
        //$lead->assignee->notify(new LeadAssigned($lead, $lead->assignee, auth()->user()));

        $activityinput = array_merge(
            ['text' => 'Lead ' . $lead->title .
            ' was created by '. $lead->createdBy->name .
            ' and assigned to ' . $lead->assignee->name,
            'user_id' => Auth()->id(),
            'type' => 'lead',
            'type_id' =>  $insertedId]
        );

        Logs::create($activityinput);
    
        return $insertedId;
    }

    public function update($id, $requestData)
    {

        $input = $requestData = array_merge(
        $requestData->all(),
            ['fk_user_id_updated' => \Auth::id(),
             'contact_date' => $requestData->contact_date ." " . $requestData->contact_time . ":00",
             'open_date' => $requestData->open_date ." " . $requestData->open_time . ":00",
             'close_date' => $requestData->close_date ." " . $requestData->close_time . ":00"]
        );

        $lead = Leads::findOrFail($id);
        if($lead->assignee->id != $requestData['fk_user_id_assign']){
            //$lead->assignee->notify(new LeadAssigned($lead, $lead->assignee, auth()->user()));
        }
        
        $lead->fill($input)->save();

        $activityinput = array_merge(
            ['text' => 'Lead ' . $lead->title .
            ' was updated by '. $lead->updatedBy->name .
            ' and assigned to ' . $lead->assignee->name,
            'user_id' => Auth()->id(),
            'type' => 'lead',
            'type_id' =>  $id]
        );

        Logs::create($activityinput);

        
    }

    public function updateStatus($id, $requestData)
    {
        $lead = Leads::findOrFail($id);

        if ($requestData->get('status') != '') {
            $status = $requestData->get('status');
        } else {
            $status = '2';
        }
        
        $input = array_replace($requestData->all(), ['status' => $status]);
        $lead->fill($input)->save();

        $activityinput = array_merge(
            ['text' => 'Lead Status Changed to '.Config::get('rothmans.lead_status')[$status].' by '. Auth()->user()->name,
            'user_id' => Auth()->id(),
            'type' => 'lead',
            'type_id' =>  $id]
        );

        Logs::create($activityinput);
    }

    public function updateFollowup($id, $requestData)
    {
        $lead = Leads::findOrFail($id);
        $follow_up_history = array_merge(
            [
            'old_contact_date' => $lead->contact_date, 
            'new_contact_date' => $requestData->contact_date.' '.$requestData->contact_time.':00', 
            'note' => $requestData->note,  'created_by' => Auth()->id(), 'fk_lead_id' => $lead->id, 
            'fk_user_id' => $lead->fk_user_id_assign
            ]
        );
        LeadFollowups::create($follow_up_history);

        $input = $requestData->all();
        $input = $requestData =
         [ 'contact_date' => $requestData->contact_date ." " . $requestData->contact_time . ":00"];
        $lead->fill($input)->save();

        $activityinput = array_merge(
            ['text' => Auth()->user()->name.' Inserted a new time for this lead',
            'user_id' => Auth()->id(),
            'type' => 'lead',
            'type_id' =>  $id]
        );
        Logs::create($activityinput);

    }

    public function updateAssign($id, $requestData)
    {
        $lead = Leads::findOrFail($id);

        $input = $requestData->get('fk_user_id_assign');
        $input = array_replace($requestData->all());

        if($lead->assignee->id != $requestData->all()['fk_user_id_assign']){
            //$lead->assignee->notify(new LeadAssigned($lead, $lead->assignee, auth()->user()));
        }
        
        $lead->fill($input)->save();
        $insertedName = $lead->assignee->name;

        $activityinput = array_merge(
            ['text' => auth()->user()->name.' assigned lead to '. $insertedName,
            'user_id' => Auth()->id(),
            'type' => 'lead',
            'type_id' =>  $id]
        );
        Logs::create($activityinput);
        
    }

    public function updateBasicInfo($id, $requestData)
    {
        $lead = Leads::findOrFail($id);
        $input = array_replace($requestData->all());
        $lead->fill($input)->save();

        $activityinput = array_merge(
            ['text' => auth()->user()->name.' updated Lead',
            'user_id' => Auth()->id(),
            'type' => 'lead',
            'type_id' =>  $id]
        );
        Logs::create($activityinput);
    }

    public function completeLead($requestData='', $services='')
    {
        //begin transection
        DB::beginTransaction();

        unset($lead);
        $lead = DB::table('leads')->where('id', $requestData['lead_id'])->update(['status' => 2, 'completed_by' => Auth()->id()]);
        $activityinput = array_merge(
            ['text' => auth()->user()->name.' has Completed The Lead',
            'user_id' => Auth()->id(),
            'type' => 'lead',
            'type_id' =>  $requestData['lead_id']]
        );
        Logs::create($activityinput);
        //$lead = DB::raw("UPDATE leads SET status=2 WHERE id=" . $requestData['lead_id']);
        
        $input = array(
                'name' => $requestData['name'],
                'email' => $requestData['email'],
                'username' => $requestData['username'],
                'official_mobile' => $requestData['secondary_number'],
                'password' => bcrypt($requestData['password']),
                'mobile' => $requestData['mobile'],
                'address1' => $requestData['address1'],
                'address2' => $requestData['address2'],
                'state' => $requestData['state'],
                'city' => $requestData['city'],
                'zip_code' => $requestData['zipcode'],
                'user_type' => 'Customer',
            );
        $user = User::create($input);

        //save metadata
        unset($input);
        $input = array(
                'fk_relation_manager_id' => $requestData['fk_relation_manager_id'],
                'fk_source' => $requestData['source'],
                'fk_user_id' => $user->id,
                'fk_client_id' => $requestData['fk_client_id'],
            );
        $user_client = UserClientMeta::create($input);

        foreach ($services as $key => $service) {
            //save services
            unset($input);
            if(isset($service['force_verify']) && $service['force_verify'] == 'yes'){
                if (isset($service['activation_date']) && strtotime($service['activation_date']) <= strtotime(Carbon::now()->format('Y-m-d'))) {
                    $status = "Active";
                } else {
                    $status = "Pending";
                }
                $service_verified = 'Yes';
                $force_verify_reason = $service['force_verify_reason'];
            }else{
                $status = "Pending";
                $service_verified = 'No';
                $force_verify_reason = '';
            }
            
            $input = array(
                'user_id' => $user->id,
                'service_id' => $service['fk_service_id'],
                'plan_id' => $service['fk_plan_id'],
                'service_capital' => $service['service_capital'],
                'activation_date' => $service['activation_date'],
                'service_status' =>  $status,
                'comments' =>  $service['comments'],
                'service_verified' => $service_verified,
                'force_verify_reason' => $force_verify_reason,
                'service_details' => json_encode($service['service_details']),
                'plan_details' => json_encode($service['plan_details']),
            );
            $user_service = UserService::create($input);
            
            //save user plan
            $user_plan = DB::table('plan_user')->where('plan_id', $service['fk_plan_id'])->where('user_id', $user->id)->get();
            if(!$user_plan){
                $user_plan = DB::table('plan_user')->insert(
                    ['plan_id' => $service['fk_plan_id'], 'user_id' => $user->id]
                );    
            } else {
                $user_plan = 1;
            }
            
            //save transections
            unset($input);
            $transaction = false;
            if( $service['fk_payment_type'] == 'cash' ){

                if($service['payment_type'] == 'full'){
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'amount'                => $service['amount'],
                        'date'                  => $service['date'],
                        'transaction_id'        => $service['transaction_id'],
                        'received_from'         => $service['received_from'],
                        'discount'              => $service['discount']
                    );
                } else {
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'discount'              => $service['discount']
                    );
                }
                
                $transaction = Transection::create($input);
            }

            if( $service['fk_payment_type'] == 'cheque' ){
                if($service['payment_type'] == 'full'){
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'amount'                => $service['amount'],
                        'date'                  => $service['date'],
                        'cheque_number'         => $service['cheque_number'],
                        'bank'                  => $service['bank'],
                        'discount'              => $service['discount']
                    );
                } else {
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'discount'              => $service['discount']
                    );
                }
                
                $transaction = Transection::create($input);
            }
            
            if( $service['fk_payment_type'] == 'net_banking' ){
                if($service['payment_type'] == 'full'){
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'amount'                => $service['amount'],
                        'date'                  => $service['date'],
                        'transaction_id'        => $service['transaction_id'],
                        'received_from'         => $service['received_from'],
                        'type'                  => $service['type'],
                        'discount'              => $service['discount']
                    );
                } else {
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'discount'              => $service['discount']
                    );
                }
                
                $transaction = Transection::create($input);
            }

            if( $service['fk_payment_type'] == 'other' ){
                if($service['payment_type'] == 'full'){
                    //Do it Later
                } else {
                    $input = array(
                        'fk_user_service_id'    => $user_service->id,
                        'payment_type'          => $service['fk_payment_type'],
                        'pay_type'              => $service['payment_type'],
                        'discount'              => $service['discount']
                    );

                    $transaction = Transection::create($input);
                }
            }
            
            if($transaction && $service['payment_type'] == 'partial'){
                foreach ($service['transaction'] as $pay_no => $trans) {
                    unset($partial_transactions);

                    $partial_transactions['payment_id']     = $transaction->id;
                    $partial_transactions['payment_type']   = $trans['payment_type'];
                    $partial_transactions['amount']         = $trans['amount'];
                    $partial_transactions['date']           = $trans['date'];
                    $partial_transactions['pay_no']         = $pay_no + 1;

                    if( $trans['payment_type'] == 'cash' ){
                        $partial_transactions['transaction_id'] = $trans['transaction_id'];
                        $partial_transactions['received_from']  = $trans['received_from'];
                    }

                    if( $trans['payment_type'] == 'net_banking' ){
                        $partial_transactions['transaction_id'] = $trans['transaction_id'];
                        $partial_transactions['received_from']  = $trans['received_from'];
                        $partial_transactions['type']           = $trans['type'];
                    }

                    if( $trans['payment_type'] == 'cheque' ){
                        $partial_transactions['cheque_number'] = $trans['cheque_number'];
                        $partial_transactions['bank']  = $trans['bank'];
                    }

                    DB::table('partial_transactions')->insert($partial_transactions);
                }
            }
        }

        if( !$user || !$user_client || !$user_service || !$transaction || !$user_plan) {
            DB::rollBack();
        } else {
            DB::commit();
        }

        $user->perms()->sync([]);
        $user->roles()->sync(['2']);
    }

    public function allLeads()
    {
        return Leads::all()->count();
    }

    public function allCompletedLeads()
    {
        return Leads::where('status', 2)->count();
    }

    public function percantageCompleted()
    {
        if (!$this->allLeads() || !$this->allCompletedLeads()) {
            $totalPercentageLeads = 0;
        } else {
            $totalPercentageLeads =  $this->allCompletedLeads() / $this->allLeads() * 100;
        }

        return $totalPercentageLeads;
    }

    public function completedLeadsToday()
    {
        return Leads::whereRaw(
            'date(updated_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('status', 2)->count();
    }

    public function createdLeadsToday()
    {
        return Leads::whereRaw(
            'date(created_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->count();
    }

    public function completedLeadsThisMonth()
    {
        return DB::table('leads')
                 ->select(DB::raw('count(*) as total, updated_at'))
                 ->where('status', 2)
                 ->whereBetween('updated_at', array(Carbon::now()->startOfMonth(), Carbon::now()))->get();
    }

    public function createdLeadsMonthly()
    {
        return DB::table('leads')
             ->select(DB::raw('count(*) as month, updated_at'))
             ->where('status', 2)
             ->groupBy(DB::raw('YEAR(updated_at), MONTH(updated_at)'))
             ->get();
    }

    public function completedLeadsMonthly()
    {
        return DB::table('leads')
         ->select(DB::raw('count(*) as month, created_at'))
         ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at)'))
         ->get();
    }
}
