<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use Sluggable;

    protected $casts = [
        'allowed_duration' => 'json',
    ];
    public $appends = ['watchlist'];

    public function plans()
    {
        return $this->hasMany(Plan::class);
    }

    public function template()
	{
	    return $this->hasMany(SdTemplate::class);
	}

    public function watchedByUser()
    {
        return $this->morphMany(Watchlist::class, 'watchable')->where('watched_by', auth()->user()->id);
    }

    public function watchedBy()
    {
        return $this->morphMany(Watchlist::class, 'watchable');
    }

    public function getWatchlistAttribute(){
        return $this->watchedByUser()->count();
    }

    public function setWatchlistAttribute($val){
        $watchlist = $this->watchedByUser()->first();
        if($val)
        {
            if(!$watchlist) {
                $watchlist = new Watchlist();
                $watchlist->watched_by = auth()->user()->id;
                $this->watchedBy()->save($watchlist);
            }
        } else {
            if($watchlist) {
                $watchlist->delete();
            }
        }
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'code' => [
                'source' => 'name'
            ]
        ];
    }
}
