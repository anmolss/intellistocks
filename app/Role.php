<?php

namespace App;

use Laratrust\Traits\LaratrustRoleTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Role extends AppModel
{
    use Sluggable, LaratrustRoleTrait{
        LaratrustRoleTrait::permissions as perms;
    }


    protected $fillable = [
        'name', 'display_name', 'description'
    ];

    public function sluggable()
    {
        return [
            'name' => [
                'source' => 'display_name'
            ]
        ];
    }
}
