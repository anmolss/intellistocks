<?php

namespace App;

use Mpociot\Teamwork\TeamworkTeam;

class Team extends TeamworkTeam
{

    public function ipList()
    {
        return $this->morphToMany(IP::class, 'restricted', 'setting_ips', null);
    }
}
