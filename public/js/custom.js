jQuery(document).ready(function(){
  // File input
    $(".file-styled").uniform({
        fileButtonClass: 'action btn btn-xs bg-info-800'
    });
  /* Active User Service Action Modals*/
    var $balance_warning = 'no';
    jQuery(document).delegate('.user_verify', 'click', function(e) {
      e.preventDefault();
      jQuery(':input','#verify_form')
      .not(':button, :submit, :reset, :hidden')
      .val('')
      .removeAttr('checked')
      .removeAttr('selected');
      jQuery('.verify_warning').hide();
      jQuery('.transcript_file').hide();
      jQuery('.kyc_file').hide();
      jQuery('.balance_warning').hide();

      var $user_id = jQuery(this).attr('data-id');
      var $transcript_status = jQuery(this).attr('data-transcript');
      var $kyc_status = jQuery(this).attr('data-kyc');
      var $consent_status = jQuery(this).attr('data-consent');
      var $consent_ip = jQuery(this).attr('data-consent-ip');
      if ($consent_ip == '' && $consent_status == 'Yes') {
        $consent_ip = 'Force Verified';
      } else if ($consent_ip == '' && $consent_status == 'No') {
        $consent_ip = 'N/A';
      } else {
        $consent_ip = $consent_ip;
      }
      jQuery('.consent_ip').html($consent_ip);
      var $activation_date = jQuery(this).attr('data-activation-date');
      var $expiry_date = jQuery(this).attr('data-expiry-date');
      $balance_warning = jQuery(this).attr('data-balance-warning');

      jQuery('.user_id').val($user_id);
      if ($transcript_status == 'Yes') {
        jQuery('.transcript_status').prop('checked',true);
      } else {
        jQuery('.transcript_status').prop('checked',false);
      }

      if ($kyc_status == 'Yes') {
        jQuery('.kyc_status').prop('checked',true);
      } else {
        jQuery('.kyc_status').prop('checked',false);
      }

      if ($consent_status == 'Yes') {
        jQuery('.consent_status').prop('checked',true);
      } else {
        jQuery('.consent_status').prop('checked',false);
      }

      jQuery('.activation_date').html($activation_date);
      jQuery('.expiry_date').html($expiry_date);

      jQuery('#verify_modal').modal('toggle');
    });
    jQuery(document).delegate(".checkbox", 'change', function(){
      if(jQuery('.transcript_status').is(':checked') && jQuery('.kyc_status').is(':checked') && jQuery('.consent_status').is(':checked')) {
        if ($balance_warning == 'yes') {
          jQuery('.balance_warning').show();
        } else {
          jQuery('.balance_warning').hide();
        }
        jQuery('.verify_warning').show();
      } else {
        jQuery('.verify_warning').hide();
      }

      if(jQuery('.transcript_status').is(':checked')){
        jQuery('.transcript_file').show();
      } else {
        jQuery('.transcript_file').hide();
      }

      if(jQuery('.kyc_status').is(':checked')){
        jQuery('.kyc_file').show();
      } else {
        jQuery('.kyc_file').hide();
      }
    });

    jQuery(document).delegate('.save_verification', 'click', function(e) {
      e.preventDefault();
      var $url = jQuery('.verify_modal_form').attr('action');
      var $data = jQuery('.verify_modal_form').serializeArray();
      var data = new FormData($("#verify_form")[0]);
      
      jQuery.ajax({
          type: 'POST',
          url: $url,
          data: data,
          dataType: "json",
          async:false,
          processData: false,
          contentType: false,
          beforeSend: function() {
            jQuery('#verify_modal').find('#verify_success').html('').hide();
            jQuery('#verify_modal').find('#verify_failed').html('').hide();
            jQuery('.save_verification').attr("disabled", true);
            jQuery('.save_verification').fadeTo('slow','0.4');
            jQuery(".save_verification").html('Please wait...');
            jQuery(".verify_service_form_spinner").show();
          },
          success: function(data) {
            if(data.success == 1){
              jQuery('#verify_success').html("Informations saved successfully.Please wait while reloading page...").show();
                setTimeout(function(){
                  window.location.reload()
                }, 1500);
            } else if (data.success == 2) {
              jQuery('#verify_modal').find('#verify_failed').html(data.msg).show();
            } else {
              jQuery('#verify_success').html("Error in saving.Please wait while reloading page...");
              setTimeout(function(){
                window.location.reload()
              }, 1000);
            }
          },
          complete: function() {
            jQuery('.save_verification').attr("disabled", false);
            jQuery('.save_verification').fadeTo('slow','1');
            jQuery(".save_verification").html('Save');
            jQuery(".verify_service_form_spinner").hide();
          }
      });
      return false;
    });

    jQuery(document).delegate('.resend_activation', 'click', function(e) {
      e.preventDefault();
      var $url = jQuery(this).attr('href');
      swal({   
        title: "Do You Really Want to Resend Activation Request",   
        text: "Click OK to Resend Mail",   
        type: "warning",   
        showCancelButton: true,   
        closeOnConfirm: false,   
        showLoaderOnConfirm: true, 
      }, function(){   
        jQuery.ajax({
          type: 'GET',
          url: $url,
          dataType: "json",
          success: function(data) {
            if(data.success == 1){
              swal("Verification Request Sent");
            } else {
              swal("User Consent Status is Already Verified");
            }
          }
        });
      });
      
    });
  /* *************************************************************************************************************************** */

  /* Add & Remove Services */
    jQuery('#add_service').on('click', function(){
      var $content  = jQuery('#service_template').clone().removeAttr('id');

      $total_services++;

      $content.find('.service_row_template').attr('data-count', $total_services);
      $content.find('.service_row_template').removeClass('.service_row_template').addClass('service_row');

      $content.find('select[name="fk_service_id"]').attr('name', 'services['+ $total_services +'][fk_service_id]');
      $content.find('select[name="fk_plan_id"]').attr('name', 'services['+ $total_services +'][fk_plan_id]');
      $content.find('select[name="fk_payment_type"]').attr('name', 'services['+ $total_services +'][fk_payment_type]');
      $content.find('select[name="payment_type"]').attr('name', 'services['+ $total_services +'][payment_type]');

      $content.show();

      jQuery('#service_append').append($content);
    });

    jQuery( "#service_append" ).delegate( ".remove_service", "click", function() {
      $total_services--;
      jQuery(this).closest('.row').remove();
    });
  /* *************************************************************************************************************************** */

  /* Full || Cash - Cheque - Net Banking */
    jQuery( "#service_append" ).delegate( ".fk_payment_type", "change", function() {
      var $case           = jQuery(this).val();
      var $type           = jQuery(this).closest('.service_row').find('.payment_type').val();
      var $current_index  = jQuery(this).closest('.service_row').attr('data-count');

      makePaymentClone($type, $case, $current_index, jQuery(this));
    });

    jQuery( "#service_append" ).find(".payment_type").bind("change", function(e) {
      var $this = jQuery(this);
      if($is_edit == false || $change_warning_count == 1){
        var $type           = jQuery(this).val();
        var $current_index  = jQuery(this).closest('.service_row').attr('data-count');

        jQuery(this).closest('.service_row').find('.fk_payment_type').val('');

        if($type == 'partial'){
          jQuery(this).closest('.service_row').find('.fk_payment_type').attr('disabled', true);
        } else {
          jQuery(this).closest('.service_row').find('.fk_payment_type').attr('disabled', false);
        }

        jQuery(this).closest('.service_row').find('.payment_type_append').html('');

        var $case           = jQuery(this).closest('.service_row').find('.fk_payment_type').val();

        makePaymentClone($type, $case, $current_index, jQuery(this));
      } else {
        swal({   
          title: "Are you sure?",   
          text: "You will lose all the transaction data filled in form!",   
          type: "warning",   
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes, do it!",   
          closeOnConfirm: true 
        },function(isConfirm){
          if (isConfirm) {     
            var $type           = jQuery($this).val();
            var $current_index  = jQuery(this).closest('.service_row').attr('data-count');

            jQuery($this).closest('.service_row').find('.fk_payment_type').val('');

            if($type == 'partial'){
              jQuery($this).closest('.service_row').find('.fk_payment_type').attr('disabled', true);
            } else {
              jQuery($this).closest('.service_row').find('.fk_payment_type').attr('disabled', false);
            }

            jQuery($this).closest('.service_row').find('.payment_type_append').html('');

            var $case = jQuery($this).closest('.service_row').find('.fk_payment_type').val();

            makePaymentClone($type, $case, $current_index, jQuery($this));
            lastValue = jQuery($this).val();
          } else {
            jQuery($this).val(lastValue);
          } 
        });
      }
      //lastValue = jQuery($this).val();
      $change_warning_count++;
    });
  /* *************************************************************************************************************************** */

  /* Partial || Cash - Cheque - Net Banking */
    jQuery( "#service_append" ).delegate( ".fk_partial_payment_type0", "change", function() {
      var $current_index  = jQuery(this).closest('.service_row').attr('data-count');
      var $content = '';

      if (jQuery(this).val() == 'cash') {
        $content = jQuery('#payment_mode_partial_cash_template').clone().removeAttr('id');
        $content.find('input[name="transaction_id"]').attr('name', 'services['+ $current_index +'][transaction][0][transaction_id]');
        $content.find('input[name="received_from"]').attr('name', 'services['+ $current_index +'][transaction][0][received_from]');
        $content.find('select[name="cleared"]').attr('name', 'services['+ $current_index +'][transaction][0][cleared]');
      }

      if (jQuery(this).val() == 'net_banking') {
        $content = jQuery('#payment_mode_partial_net_banking_template').clone().removeAttr('id');
        $content.find('select[name="type"]').attr('name', 'services['+ $current_index +'][transaction][0][type]');
        $content.find('input[name="transaction_id"]').attr('name', 'services['+ $current_index +'][transaction][0][transaction_id]');
        $content.find('input[name="received_from"]').attr('name', 'services['+ $current_index +'][transaction][0][received_from]');
        $content.find('select[name="cleared"]').attr('name', 'services['+ $current_index +'][transaction][0][cleared]');
      }

      if (jQuery(this).val() == 'cheque') {
        $content = jQuery('#payment_mode_partial_cheque_template').clone().removeAttr('id');
        $content.find('input[name="cheque_number"]').attr('name', 'services['+ $current_index +'][transaction][0][cheque_number]');
        $content.find('input[name="bank"]').attr('name', 'services['+ $current_index +'][transaction][0][bank]');
        $content.find('select[name="cleared"]').attr('name', 'services['+ $current_index +'][transaction][0][cleared]');
      }

      if($content != ''){
        $content.show();
        jQuery('.extra-filed-container0').html($content);
      }
    });

    jQuery( "#service_append" ).delegate( ".fk_partial_payment_type1", "change", function() {
      var $current_index  = jQuery(this).closest('.service_row').attr('data-count');
      var $content = '';

      if (jQuery(this).val() == 'cash') {
        $content = jQuery('#payment_mode_partial_cash_template').clone().removeAttr('id');
        $content.find('input[name="transaction_id"]').attr('name', 'services['+ $current_index +'][transaction][1][transaction_id]');
        $content.find('input[name="received_from"]').attr('name', 'services['+ $current_index +'][transaction][1][received_from]');
        $content.find('select[name="cleared"]').attr('name', 'services['+ $current_index +'][transaction][1][cleared]');
      }

      if (jQuery(this).val() == 'net_banking') {
        $content = jQuery('#payment_mode_partial_net_banking_template').clone().removeAttr('id');
        $content.find('select[name="type"]').attr('name', 'services['+ $current_index +'][transaction][1][type]');
        $content.find('input[name="transaction_id"]').attr('name', 'services['+ $current_index +'][transaction][1][transaction_id]');
        $content.find('input[name="received_from"]').attr('name', 'services['+ $current_index +'][transaction][1][received_from]');
        $content.find('select[name="cleared"]').attr('name', 'services['+ $current_index +'][transaction][1][cleared]');
      }

      if (jQuery(this).val() == 'cheque') {
        $content = jQuery('#payment_mode_partial_cheque_template').clone().removeAttr('id');
        $content.find('input[name="cheque_number"]').attr('name', 'services['+ $current_index +'][transaction][1][cheque_number]');
        $content.find('input[name="bank"]').attr('name', 'services['+ $current_index +'][transaction][1][bank]');
        $content.find('select[name="cleared"]').attr('name', 'services['+ $current_index +'][transaction][1][cleared]');
      }

      if($content != ''){
        $content.show();
        jQuery('.extra-filed-container1').html($content);
      }
    });

    jQuery( "#service_append" ).delegate( ".fk_partial_payment_type2", "change", function() {
      var $current_index  = jQuery(this).closest('.service_row').attr('data-count');
      var $content = '';

      if (jQuery(this).val() == 'cash') {
        $content = jQuery('#payment_mode_partial_cash_template').clone().removeAttr('id');
        $content.find('input[name="transaction_id"]').attr('name', 'services['+ $current_index +'][transaction][2][transaction_id]');
        $content.find('input[name="received_from"]').attr('name', 'services['+ $current_index +'][transaction][2][received_from]');
        $content.find('select[name="cleared"]').attr('name', 'services['+ $current_index +'][transaction][2][cleared]');
      }

      if (jQuery(this).val() == 'net_banking') {
        $content = jQuery('#payment_mode_partial_net_banking_template').clone().removeAttr('id');
        $content.find('select[name="type"]').attr('name', 'services['+ $current_index +'][transaction][2][type]');
        $content.find('input[name="transaction_id"]').attr('name', 'services['+ $current_index +'][transaction][2][transaction_id]');
        $content.find('input[name="received_from"]').attr('name', 'services['+ $current_index +'][transaction][2][received_from]');
        $content.find('select[name="cleared"]').attr('name', 'services['+ $current_index +'][transaction][2][cleared]');
      }

      if (jQuery(this).val() == 'cheque') {
        $content = jQuery('#payment_mode_partial_cheque_template').clone().removeAttr('id');
        $content.find('input[name="cheque_number"]').attr('name', 'services['+ $current_index +'][transaction][2][cheque_number]');
        $content.find('input[name="bank"]').attr('name', 'services['+ $current_index +'][transaction][2][bank]');
        $content.find('select[name="cleared"]').attr('name', 'services['+ $current_index +'][transaction][2][cleared]');
      }

      if($content != ''){
        $content.show();
        jQuery('.extra-filed-container2').html($content);
      }
    });
  /* *************************************************************************************************************************** */

  /* Service & Plan Selction */
    jQuery( document ).on( "change", ".service_type", function() {
      jQuery('.reflect_capital_amnt').val('');
      $service = jQuery(this).val();
      var $el = $(this).parents('.row').find(".plans");
      var $el_details = $(this).parents('.row').find(".plan_append");
      var $el_service = $(this).parents('.row').find(".service_row");

      $el.empty();
      $el_details.empty();
      if ($service == '' || $service == '0') {
        $el.append('<option value="">Select Plan</option>');
        $el_details.html(jQuery('#empty_plan_template').html());
      } else {
        $plans = $service_plans[$service];
        $.each($plans, function(key,value) {

          $el.append($("<option></option>").attr("value", value.id).text(value.name));
          if(key == 0){
            getPlan(value.id, $el, $el_details);

            $el_service.parents('.row').find('.payment_type').val('');
            $el_service.parents('.row').find('.fk_payment_type').val('');
            $el_service.parents('.row').find('.payment_type_append').empty();
          }
        });
      }
    });

    jQuery( "#service_append" ).delegate( ".plans", "change", function() {
      var plan_id = $(this).val();
      var $el_details = $(this).parents('.row').find(".plan_append");

      getPlan(plan_id, $(this), $el_details);
    });
  /* *************************************************************************************************************************** */

  /* Save Form */
    jQuery( '.client_create_form' ).submit(function( event ) {
      event.preventDefault();
      jQuery('.form_spinner').hide();
      var data = jQuery(this).serializeArray();
      var $url = jQuery(this).attr('action');

      manageRate(jQuery(this), 'form_save');

      jQuery.ajax({
          type: 'POST',
          url: $url,
          data: data,
          dataType: "json",
          beforeSend: function() {
            // setting a timeout
            jQuery('.form_spinner').show();
            jQuery('#lead_complete_submit').attr('disabled','disabled');
          },
          success: function(data) {
            if(data.success == 1){
              jQuery('#lead_complete_submit').removeAttr('disabled'); 
              jQuery('#modal_form_lead_complete').modal('toggle');
              jQuery('#lead_complete_form').trigger("reset");
              jQuery('span.help-block').html('');
              jQuery('#fk_client_id').html(data.clients);
              jQuery('#fk_client_id').select2({width: '40%'});
              swal("Data Saved!", "Data Has Been Saved!", "success")
              window.location.replace(data.link);
            } else if(data.success == 0){
              if(data.alert_type == 'swal'){
                swal("Sorry !!", data.msg, "info");
                jQuery('.form_spinner').hide();
              }
            }
          },
          error: function(data) { // if error occured
              jQuery('span.help-block').hide();

              var errors = data.responseJSON;
              if(errors != undefined){
              jQuery.each(errors, function (index, item) {

                  jQuery('.service_row').each(function (index1, item1) {
                      var $temp = index.split('.');
                      var $tmp = item['0'].split('.');

                      if ($temp[2] == 'transaction') {
                          if (jQuery(this).find('.payment_type_append').find('.error_' + $temp[4] + $temp[3]).length > 0) {
                              jQuery(this).find('.error_' + $temp[4] + $temp[3]).html(item['0']).show();
                          }
                      } else {
                          if (jQuery(this).find('.error_services_' + $temp[2]).length > 0) {
                              jQuery(this).find('.error_services_' + $temp[2]).html(item['0']).show();
                          }
                      }
                  });

                  if (jQuery('#error_' + index).length > 0) {
                      jQuery('#error_' + index).html(item['0']).show();
                  } 
              });
            }else{
                  if(data.readyState == 4 && data.responseText != undefined){
                      swal("Success!", "Data Added Successfully.", "success")
                  }else{
                      swal("Error!", "Unable to Add!!!.", "error")
                  }
              }

            jQuery('span.help-block').animate({scrollTop: 0}, "slow");
          },
          complete: function() {
            jQuery('.form_spinner').hide();
          }
      });
    });
  /* *************************************************************************************************************************** */

  /* Manage Rate */
    jQuery(document).on('keyup', '.reflect_amt_key_up', function(){
      manageRate(jQuery(this), 'form_filling');
      var amount_field_name = jQuery(this).attr('name');
      var pay_no = parseInt(jQuery('.select_partial_pay').val());
      var total_payable_amount = parseFloat(jQuery('#net_payable_amt').html()).toFixed(2);

      if( pay_no == 3 && (amount_field_name == 'services[0][transaction][1][amount]' || amount_field_name == 'services[0][transaction][0][amount]' || amount_field_name == 'services[0][discount]') ) {
        var first_pay = jQuery('input[name="services[0][transaction][0][amount]"]').val();
        var second_pay = jQuery("input[name='services[0][transaction][1][amount]']").val();
        jQuery("input[name='services[0][transaction][2][amount]']").val(parseFloat(total_payable_amount) - (parseFloat(first_pay) + parseFloat(second_pay)));
      } else if ( pay_no == 2 && (amount_field_name == 'services[0][transaction][0][amount]' || amount_field_name == 'services[0][discount]')) {
        var first_pay = jQuery('input[name="services[0][transaction][0][amount]"]').val();
        jQuery("input[name='services[0][transaction][1][amount]']").val(parseFloat(total_payable_amount) - parseFloat(first_pay));
      } else if (amount_field_name == 'services[0][discount]' && jQuery('.payment_type').val() == 'full'){
        jQuery("input[name='services[0][transaction][0][amount]']").val(parseFloat(total_payable_amount));
      }
    });

    jQuery(document).on('keyup', '.reflect_capital_amnt', function(){
      var $capital_amnt = parseFloat(jQuery(this).val());
      if($capital_amnt == 0 || $capital_amnt == undefined || isNaN($capital_amnt)){
        $capital_amnt = 0;
      }
      if($capital_amnt != 0){
        var $capital_fee = parseFloat(($capital_amnt)/100);
        jQuery(this).parents('.service_row').attr('data-net_capital_amount', $capital_fee);
      }
      
      manageRate(jQuery(this), 'form_filling');
      var total_payable_amount = parseFloat(jQuery('#net_payable_amt').html()).toFixed(2);
      if (jQuery('.payment_type').val() == 'full'){
        jQuery("input[name='services[0][transaction][0][amount]']").val(parseFloat(total_payable_amount));
      }
    });
  /* *************************************************************************************************************************** */

  jQuery('#force_verify_check').change(function() {

      if(this.checked) {
        jQuery('#force_verify_reason').show();
      } else {
        jQuery('#force_verify_reason').hide();
      }
  });

  jQuery(document).delegate('.select_partial_pay', 'change', function(){
    jQuery('.1st_partial_pay').hide();
    jQuery('.2nd_partial_pay').hide();
    jQuery('.3rd_partial_pay').hide();
    if (jQuery(this).val() == 1) {
      jQuery("input[name='services[0][transaction][0][amount]']").val(parseFloat(jQuery('#net_payable_amt').html()).toFixed(2));
      jQuery('.1st_partial_pay').show();
      jQuery('.2nd_partial_pay').find('.extra-filed-container1').html('');
      jQuery('.2nd_partial_pay').find('input').val('0');
      jQuery('.2nd_partial_pay').find('select').prop('selectedIndex',0);
      jQuery('.3rd_partial_pay').find('.extra-filed-container2').html('');
      jQuery('.3rd_partial_pay').find('input').val('0');
      jQuery('.3rd_partial_pay').find('select').prop('selectedIndex',0);
    } else if (jQuery(this).val() == 2) {
      jQuery('.1st_partial_pay').show();
      jQuery('.2nd_partial_pay').show();
      jQuery('.3rd_partial_pay').find('.extra-filed-container2').html('');
      jQuery('.3rd_partial_pay').find('input').val('0');
      jQuery('.3rd_partial_pay').find('select').prop('selectedIndex',0);
    } else if (jQuery(this).val() == 3) {
      jQuery('.1st_partial_pay').show();
      jQuery('.2nd_partial_pay').show();
      jQuery('.3rd_partial_pay').show();
    } else {
      jQuery('.1st_partial_pay').find('.extra-filed-container0').html('');
      jQuery('.1st_partial_pay').find('input').val('0');
      jQuery('.1st_partial_pay').find('select').prop('selectedIndex',0);
      jQuery('.2nd_partial_pay').find('.extra-filed-container1').html('');
      jQuery('.2nd_partial_pay').find('input').val('0');
      jQuery('.2nd_partial_pay').find('select').prop('selectedIndex',0);
      jQuery('.3rd_partial_pay').find('.extra-filed-container2').html('');
      jQuery('.3rd_partial_pay').find('input').val('0');
      jQuery('.3rd_partial_pay').find('select').prop('selectedIndex',0);
    }
  });
});

function calculate_tax(ele) {
  jQuery(ele).attr();
}

function makePaymentClone(payment_mode, payment_type, index, ele){
  if(payment_mode != ''){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
    
    var $today_date = dd+'-'+mm+'-'+yyyy;

    var $content = '';

    if(payment_mode == 'full' &&  payment_type != ''){
      var $service_fee = 0;
      var $show_amount = parseFloat(jQuery('#net_payable_amt').html()).toFixed(2);
      if (payment_type == 'cash') {
        var $content  = jQuery('#payment_type_cash_full_template').clone().removeAttr('id'); 
        $content.find('input[name="transaction_id"]').attr('name', 'services['+ index +'][transaction][0][transaction_id]');
        $content.find('input[name="received_from"]').attr('name', 'services['+ index +'][transaction][0][received_from]');
        $content.find('input[name="amount"]').val($show_amount);
        $content.find('input[name="amount"]').attr('name', 'services['+ index +'][transaction][0][amount]');
        $content.find('input[name="date"]').val($today_date);
        $content.find('input[name="date"]').attr('name', 'services['+ index +'][transaction][0][date]');
        $content.find('select[name="cleared"]').attr('name', 'services['+ index +'][transaction][0][cleared]');
      } else if (payment_type == 'cheque') {
        var $content = jQuery('#payment_type_cheque_full_template').clone().removeAttr('id'); 
        $content.find('input[name="cheque_number"]').attr('name', 'services['+ index +'][transaction][0][cheque_number]');
        $content.find('input[name="bank"]').attr('name', 'services['+ index +'][transaction][0][bank]');
        $content.find('input[name="amount"]').val($show_amount);
        $content.find('input[name="amount"]').attr('name', 'services['+ index +'][transaction][0][amount]');
        $content.find('input[name="date"]').val($today_date);
        $content.find('input[name="date"]').attr('name', 'services['+ index +'][transaction][0][date]');
        $content.find('select[name="cleared"]').attr('name', 'services['+ index +'][transaction][0][cleared]');
      } else if (payment_type == 'net_banking') {
        var $content = jQuery('#payment_type_net_banking_full_template').clone().removeAttr('id');
        $content.find('select[name="type"]').attr('name', 'services['+ index +'][transaction][0][type]');
        $content.find('input[name="transaction_id"]').attr('name', 'services['+ index +'][transaction][0][transaction_id]');
        $content.find('input[name="received_from"]').attr('name', 'services['+ index +'][transaction][0][received_from]');
        $content.find('input[name="amount"]').val($show_amount);
        $content.find('input[name="amount"]').attr('name', 'services['+ index +'][transaction][0][amount]');
        $content.find('input[name="date"]').val($today_date);
        $content.find('input[name="date"]').attr('name', 'services['+ index +'][transaction][0][date]');
        $content.find('select[name="cleared"]').attr('name', 'services['+ index +'][transaction][0][cleared]');
      }
    } else if(payment_mode == 'partial' &&  payment_type == ''){
      jQuery('.1st_partial_pay').hide();
      jQuery('.2nd_partial_pay').hide();
      jQuery('.3rd_partial_pay').hide();
      var $content = jQuery('#payment_mode_partial_template').clone().removeAttr('id');

      $content.find('select[name="fk_payment_type0"]').attr('name', 'services['+ index +'][transaction][0][fk_payment_type]');
      $content.find('input[name="amount0"]').val(0);
      $content.find('input[name="amount0"]').attr('name', 'services['+ index +'][transaction][0][amount]');
      $content.find('input[name="date0"]').val($today_date);
      $content.find('input[name="date0"]').attr('name', 'services['+ index +'][transaction][0][date]');

      $content.find('select[name="fk_payment_type1"]').attr('name', 'services['+ index +'][transaction][1][fk_payment_type]');
      $content.find('input[name="amount1"]').val(0);
      $content.find('input[name="amount1"]').attr('name', 'services['+ index +'][transaction][1][amount]');
      $content.find('input[name="date1"]').val($today_date);
      $content.find('input[name="date1"]').attr('name', 'services['+ index +'][transaction][1][date]');

      $content.find('select[name="fk_payment_type2"]').attr('name', 'services['+ index +'][transaction][2][fk_payment_type]');
      $content.find('input[name="amount2"]').val(0);
      $content.find('input[name="amount2"]').attr('name', 'services['+ index +'][transaction][2][amount]');
      $content.find('input[name="date2"]').val($today_date);
      $content.find('input[name="date2"]').attr('name', 'services['+ index +'][transaction][2][date]');
    }

    if($content != ''){
      $content.show();
      ele.closest('.service_row').find('.payment_type_append').html($content);

      restrictDates(ele.closest('.service_row'));

      if (jQuery().datepicker) {
        $('.date-picker').datepicker({
            language: 'en',
            orientation: "left",
            autoclose: true,
            orientation: 'right'
        });
      }
    }
  }
}

function getPlan(plan_id, $el, $el_details) {
  jQuery.ajax({
      type: 'GET',
      url: $getPlanUrl+'/'+plan_id,
      dataType: "json",
      beforeSend: function() {
        $el.parents('.row').find(".plan_spinner").show();
      },
      success: function(data) {
        if(data.status == 'success'){

          var $payment_type = $el.parents('.row').find('.payment_type');
          var $capital_container = $el.parents('.row').find('#capital_container');

          jQuery('input[name="payment_mode"]').val(data.payment_mode);

          if($payment_type.length == 1 && data.payment_mode == 1) {
            $payment_type.find("option[value*='partial']").prop('disabled',true);
            $capital_container.show();
          } else {
            $payment_type.find("option[value*='partial']").prop('disabled',false);
            $capital_container.show();
          }

          $el.parents('.service_row').attr('data-max_days', data.max_days);
          $el.parents('.service_row').attr('data-discount', data.max_discount);
          $el.parents('.service_row').attr('data-service_fee', data.service_fee);
          $el.parents('.service_row').attr('data-net_payable_amount', data.service_fee);
          $el.parents('.service_row').attr('data-net_capital_amount', data.service_fee);
          $el.parents('.service_row').attr('data-tax', data.applicable_tax);

          jQuery('#net_service_amt').html(data.service_fee);
          jQuery('#discount_amt').html(0);
          jQuery('#net_payable_amt').html(data.service_fee);

          if($is_edit == false){
            $el.parents('.service_row').find('.txt-discount').val('');
            $el.parents('.service_row').find('#discount-container').hide();
          }

          if(data.max_discount != 0){
            $el.parents('.service_row').find('#discount-container').show();
            $el.parents('.service_row').find('.txt-discount').attr('max', data.max_discount);
          }

          if (data.html != '') {
            $el_details.html(data.html);
          } else {
            $el_details.html(jQuery('#empty_plan_template').html());
          }
          
        }
      },
      complete: function() {
        $el.parents('.row').find(".plan_spinner").hide();
        if($is_edit == true){
          setTimeout(
            function(){ 
              var $discount_ele = $('.edit_row').find(".reflect_amt_key_up"); 
              manageRate($discount_ele, 'form_filling');
              restrictDates($el.parents('.service_row'));

              if (jQuery().datepicker) {
                jQuery('.payment_type_append').find('.date-picker').datepicker('update');
              }
            }
          , 100);          
        }
        //jQuery('.edit_row').find('.payment_type').trigger('change');
      }
  });
}

function restrictDates(ele){
  $restrict_me  = ele.find('.payment_type_append').find('.restrict-me');
  $max_days     = ele.attr('data-max_days');
  if($restrict_me.length > 0){
    $.each( $restrict_me, function( index, value ){
      jQuery(value).attr('data-date-start-date', '-0d');
      jQuery(value).attr('data-date-end-date', '+' + $max_days +'d');
    });
  }
}

function manageRate(ele, type){
  var $element = ele;
  if(type == 'form_filling'){
    var $element = ele.parents('.service_row');
  } else {
    var $element = ele.find('.service_row');
  }

  var $service_fee = parseFloat($element.attr('data-service_fee'));
  //var $net_capital_amount = parseFloat($element.attr('data-net_capital_amount'));
  var $net_capital_amount = 0;
  var $org_discount = parseFloat($element.attr('data-discount'));
  var $txt_discount = parseFloat(jQuery('.txt-discount').val());
  var $applied_tax = parseFloat($element.attr('data-tax'));

  if($service_fee == 0 || $service_fee == undefined || isNaN($service_fee)){
    $service_fee = 0;
  }

  if($net_capital_amount == 0 || $net_capital_amount == undefined || isNaN($net_capital_amount)){
    $net_capital_amount = 0;
  }
  
  if($service_fee < $net_capital_amount){
    $service_fee = $net_capital_amount;
    jQuery('#net_service_amt').html($service_fee.toFixed(2));
    $element.attr('data-net_payable_amount', $service_fee.toFixed(2));
  } else {
    jQuery('#net_service_amt').html($service_fee.toFixed(2));
    $element.attr('data-net_payable_amount', $service_fee.toFixed(2));
  }

  if($txt_discount == 0 || $txt_discount == undefined || isNaN($txt_discount)){
    $txt_discount = 0;
  }

  if($applied_tax == 0 || $applied_tax == undefined || isNaN($applied_tax)){
    $applied_tax = 0;
  }

  var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;

  if( !numberRegex.test($txt_discount) ) {
      swal("Sorry !!", "Please Enter Valid Discount", "info");
      ele.val('');
      return false;
  }

  if($txt_discount > $org_discount){
    swal("Sorry !!", "Discount cannot be greater then " + $org_discount + '%', "info");
    ele.val('');
    return false;
  }

  var $discount_amt       = 0;
  var $net_payable_amount = 0;
  var $applied_tax_amnt   = 0;

  if($txt_discount == 0 || $txt_discount == undefined || isNaN($txt_discount)){
    $net_payable_amount = $service_fee.toFixed(2);
  } else if($service_fee != 0){
    $discount_amt = parseFloat(($txt_discount / 100) * $service_fee);
    $discount_amt = $discount_amt.toFixed(2);
    $net_payable_amount = $service_fee - $discount_amt;
    $net_payable_amount = $net_payable_amount.toFixed(2);
  }

  if($applied_tax != 0){
    $applied_tax_amnt = parseFloat(($net_payable_amount*$applied_tax)/100).toFixed(2);
    $net_payable_amount = parseFloat(parseFloat($net_payable_amount)+parseFloat(($net_payable_amount*$applied_tax)/100)).toFixed(2);
  }
  
  jQuery('#discount_amt').html($discount_amt);
  jQuery('#net_payable_amt').html($net_payable_amount);
  jQuery('#applied_tax').html($applied_tax_amnt);

  $payment_type_amount  = $element.find('.payment_type_append').find('.reflect_amt_key_up');

  var $amount = 0;
  if($payment_type_amount.length > 0){    
    $.each( $payment_type_amount, function( index, value ){
      $tmp_amt = parseFloat(jQuery(value).val());

      if($tmp_amt == 0 || $tmp_amt == undefined || isNaN($tmp_amt)){
        $tmp_amt = 0;
      }

      $amount = parseFloat($amount) + $tmp_amt;
    });
    /*
    if($amount > $net_payable_amount){
      swal("Sorry !!", "Total amount cannot be greater then " + $net_payable_amount, "info");
      ele.val('');
      return false;
    }
    */
    if(type == 'form_save'){
      if($amount != $net_payable_amount){
        swal("Sorry !!", "Total amount must be equal to " + $net_payable_amount + ' and you entered only ' + $amount, "info");
        return false;
      }
    }
  }
}