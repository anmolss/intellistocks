/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


( function($) {
    $( document ).ready( function() { 
        $('.profile_popup').hide();
        $('.profile_user').click(function(){ 
            $(".profile_popup").slideDown('slow');
        });
         $('.prof_close').click(function(){ 
            $(".profile_popup").slideUp('slow')
        });
          var table = $('#example').DataTable( {   
            searching: false,             
            paging: false,
            bInfo : false,
            "order": [[1, 'asc']]
        } );
    });

    //active menu link
    var current_url= window.location.href;
    $('.page-menu-ct').find('a').each(function(){
        if(current_url == $(this).attr('href')){
            $(this).parents('li').addClass('active');
        }
    });
} )( jQuery );