<?php

Auth::routes();

Route::get('/verifyUser/{id}', 'Admin\UsersController@verify');

//Route::get('send', 'DeliveryReportController@send');
Route::post('delivery_report', 'DeliveryReportController@saveDeliveryReport');

Route::group(['namespace' => 'Auth', 'as' => 'auth::'], function()
{
    Route::get('accept/{token}', 'LoginController@acceptInvite')->name('teams.accept_invite');
    Route::get('customer_services/verify/{verification_code}', 'VerifyCustomerServiceController@verifyService')->name('services.verify_mail');
    Route::get('cron', 'CronController@run')->name('cron.run');
});

//Route::get('/pending_users', 'HomeController@nonMigratedUsers');
Route::get('/migrate_users', 'HomeController@migrateUsers');

Route::get('/home', function(){
    return redirect('/', 301);
});

Route::group(['namespace' => 'Front', 'prefix' => 'management','as' => 'front::','middleware' => ['auth', 'role:customer'] ], function () {

    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::get('active_services', 'ServiceController@indexUserServices')->name('services.userServiceIndex');
    Route::get('service_calls', 'ServiceController@indexServiceCalls')->name('services.ServiceCallIndex');
    Route::get('payment_history', 'ServiceController@paymentHistory')->name('services.PaymentHistory');
    Route::get('active_services/{id}', 'ServiceController@viewUserServices')->name('services.userServiceView');
    Route::match(['GET', 'POST'], 'profit_and_loss', 'ServiceController@profitAndLoss')->name('services.profitAndLoss');
    Route::match(['GET', 'POST'], 'profit_and_loss_stock', 'ServiceController@profitAndLossStock')->name('services.profitAndLossStock');

    Route::get('profile', 'UserController@viewProfile')->name('users.viewProfile');
    Route::match(['GET', 'POST'], 'profile/edit', 'UserController@edit')->name('users.edit');
    Route::match(['GET', 'POST'], 'profile/change-password', 'UserController@changePassword')->name('users.changePassword');

    Route::get('messages', 'UserController@messages')->name('users.messages');

});

Route::group(['namespace' => 'Admin', 'as' => 'admin::','middleware' => ['auth', 'restrict_ip', 'role:super-admin|manager|normal-user|pre-sales|retail-sales|hni-sales|manager-1|customer'] ], function () {

    Route::get('/', 'DashboardController@index')->name('dashboard');

});

//please define allowed roles to access this route here like -> 'role:super-admin|manager' in middleware
Route::group(['namespace' => 'Admin', 'as' => 'admin::','middleware' => ['auth', 'restrict_ip', 'role:super-admin|manager|normal-user|pre-sales|retail-sales|hni-sales|manager-1'] ], function () {

    //Route::get('/', 'DashboardController@index')->name('dashboard');

     /**
     * CLIENTS
     */
    Route::get('clients/index', 'ClientsController@index')->name('clients.index');
    Route::get('clients/data', 'ClientsController@anyData')->name('clients.data');

    Route::get('clients/get_relationship', 'ClientsController@getRelationshipManager');
    Route::get('clients/get_compliance', 'ClientsController@getComplianceManager');

    Route::post('clients/store_client_new', 'ClientsController@storeClientNew')->name('store_client_new');

    Route::get('customers', 'ClientsController@indexCustomers')->name('clients.customers');
    Route::post('customerData', 'ClientsController@customerData')->name('clients.customerData');
    Route::match(['GET', 'POST'],'customers/{id}', 'ClientsController@showCustomer')->name('clients.customerShow');
    Route::get('customers/{id}/pnl_service', 'ClientsController@profitAndLossStock')->name('clients.customerPnlService');
    Route::get('customers/destroy/{id}', 'ClientsController@destroyCustomer')->name('customers.destroy');

    Route::get('clients/createDirect', 'ClientsController@createDirect')->name('clients.createDirect');
    Route::post('clients/create/cvrapi', 'ClientsController@cvrapiStart');
    //Route::post('clients/upload/{id}', 'DocumentsController@upload');
    Route::resource('clients', 'ClientsController');
    Route::post('clients/destroy/{id}', 'ClientsController@destroy')->name('clients.destroy');
    Route::post('clients/storeClientUser', 'ClientsController@storeClientUser')->name('clients.store_client_user');
    Route::post('clients/updateClientUserInfo', 'ClientsController@updateClientUserInfo')->name('clients.update_client_user_info');
    Route::get('clients/{id}/addService', 'ClientsController@addService')->name('clients.addService');
    Route::post('clients/addClientService', 'ClientsController@addClientService')->name('clients.addClientService');
    Route::post('clients/sendClientSms', 'ClientsController@sendClientSms')->name('clients.send_sms');
    Route::post('clients/sendClientEmail', 'ClientsController@sendClientEmail')->name('clients.send_email');
    Route::match(['GET', 'POST'],'clients/addToWatch/{id}', 'ClientsController@addToWatchList')->name('clients.add_to_watch');

    Route::get('client/download_pnl/{id}', 'ClientsController@downloadPnl')->name('clients.download_pnl');
    Route::post('client/share_pnl/{id}', 'ClientsController@sharePnl')->name('clients.share_pnl');
    Route::get('client/watched', 'ClientsController@getWatchlistUser')->name("clients.watched_by_me");

    Route::get('client/get_pending_service_activation', 'ClientsController@getPendingServiceActivation')->name("clients.get_pending_service_activation");
    Route::post('client/update_pending_service_activation', 'ClientsController@updatePendingServiceActivation')->name("clients.update_pending_service_activation");
    /**
    * LEADS
    */
    Route::get('leads/data/{type}', 'LeadsController@anyData')->name('leads.data');
    Route::resource('leads', 'LeadsController');
    Route::patch('leads/updateassign/{id}', 'LeadsController@updateAssign');
    Route::post('leads/notes/{id}', 'NotesController@store');
    Route::patch('leads/updatestatus/{id}', 'LeadsController@updateStatus')->name('leads.updatestatus');
    Route::patch('leads/updatefollowup/{id}', 'LeadsController@updateFollowup')->name('leads.followup');
    Route::patch('leads/updatebasicinfo/{id}', 'LeadsController@updateBasicInfo')->name('leads.basicinfo');

    Route::patch('leads/completeLead/{id}', 'LeadsController@completeLead')->name('leads.completeLead');

    Route::get('leads/{id}', 'LeadsController@show')->name('leads.show');
    Route::post('leads/destroy/{id}', 'LeadsController@destroy')->name('leads.destroy');
    Route::post('leads/{id}/edit/', 'LeadsController@edit')->name('leads.edit');

    /**
    * DEPARTMENTS
    */
    Route::resource('departments', 'DepartmentsController');
    Route::post('departments/destroy/{id}', 'DepartmentsController@destroy')->name('departments.destroy');

    Route::resource('designations', 'DesignationsController');


    Route::match(['GET', 'POST'], 'users/{users}/changepassword', 'UsersController@changePassword')->name('users.changepassword');
    Route::match(['GET'], 'users/{users}/passwordsettings', 'UsersController@passwordSettings')->name('users.passwordsettings');
    Route::match(['GET'], 'users/{users}/logs', 'UsersController@logs')->name('users.logs');
    Route::match(['GET'], 'users/activities', 'UsersController@activities')->name('users.activities');
    Route::match(['GET', 'POST'], 'users/showpermission', 'UsersController@showPermission')->name('users.showpermission');

    Route::match(['GET'], 'users/{users}/{roles}/edit', 'UsersController@edit')->name('users.edit');
    Route::match(['GET', 'POST'], 'users/note', 'UsersController@note')->name('users.note');
    Route::match(['GET', 'POST'], 'users/copyimage', 'UsersController@copyImage')->name('users.copyimage');
    Route::match(['GET', 'POST'], 'users/designation', 'UsersController@designationList')->name('users.designation');
    Route::match(['GET', 'POST'], 'users/selectdesignation', 'UsersController@selectDesignation')->name('users.selectdesignation');
    Route::match(['GET', 'POST'], 'users/searchbar', 'UsersController@searchBar')->name('users.searchbar');
    Route::match(['GET', 'POST'], 'users/add', 'UsersController@addDepartment')->name('users.add');
    Route::match(['GET', 'POST'], 'users/check', 'UsersController@checkUsername')->name('users.check');
    Route::match(['GET', 'POST'], 'users/getcity', 'UsersController@getCity')->name('users.getcity');
    Route::match(['GET', 'POST'], 'users/getzipcode', 'UsersController@getZipcode')->name('users.getzipcode');
    Route::match(['GET', 'POST'], 'users/savedesignation', 'UsersController@saveDesignation')->name('users.savedesignation');
    Route::post('users/alluser', 'UsersController@allUser')->name('users.alluser');

    Route::resource('users', 'UsersController');

    Route::match(['GET', 'POST'], 'roles/{role}/permissions', 'RolesController@addPermissions')->name('roles.permissions');
    Route::resource('roles', 'RolesController');
    Route::resource('permissions', 'PermissionsController');
    Route::resource('templates', 'TemplatesController');
    Route::resource('tokens', 'TokensController');
    //Route::resource('companies', 'CompanyMastersController');
    Route::get('company/import/{date?}', 'CompanyController@importBhav');
    Route::get('company/get_upload_excel_page', 'CompanyController@addExcel');
    Route::post('company/upload_fundasheet', 'CompanyController@uploadFundasheet');
    Route::resource('company', 'CompanyController');

    Route::post('settings/manage/ip/selectteam', 'SettingsController@getTeam')->name('manage_ip.selectteam');
    Route::post('settings/manage/ip/selectuser', 'SettingsController@getUser')->name('manage_ip.selectuser');
    Route::get('settings/manage/ip/{type}', 'SettingsController@manageIp')->name('manage_ip.show');
    Route::post('settings/manage/ip/{type}', 'SettingsController@updateIp')->name('manage_ip.update');

    Route::get('settings/manage/watchlist/stock', 'SettingsController@indexWatchlistStock')->name('stock_watchlist.index');
    Route::get('settings/manage/watchlist/stock/ajax', 'SettingsController@getWatchlistStock')->name('stock_watchlist.list');
    Route::post('settings/manage/watchlist/add_stock', 'SettingsController@storeWatchlistStock')->name('stock_watchlist.store');
    Route::post('settings/manage/watchlist/remove_stock', 'SettingsController@destroyWatchlistStock')->name('stock_watchlist.destroy');
    Route::post('settings/manage/watchlist/get_all_stock', 'SettingsController@getStocksWithWatchlist')->name('stock_watchlist.get_all_stock');

    Route::post('settings/manage/alert/get_all_stock', 'SettingsController@getStocksWithAlert')->name('stock_alert.get_all_stock');
    Route::get('settings/manage/alert/stock', 'SettingsController@indexAlertStock')->name('stock_alert.index');
    Route::post('settings/manage/alert/add_stock', 'SettingsController@storeAlertStock')->name('stock_alert.store');
    Route::post('settings/manage/alert/remove_stock', 'SettingsController@destroyAlertStock')->name('stock_alert.destroy');

    Route::resource('portfolios', 'PortfolioController');
    Route::group(['prefix' => 'services/{service}'], function()
    {
        Route::resource('plans', 'PlansController');
    });
    Route::get('getPlan/{id}', 'PlansController@getPlan')->name('getPlan');
    Route::post('getMultiplePlans', 'PlansController@getMultiplePlans')->name('getMultiplePlans');

    Route::get('services/watched', 'ServiceController@getWatchlistService')->name("services.watched_by_me");
    Route::post('services/remove_watched', 'ServiceController@removeWatchlistService')->name("services.remove_my_watched");
    Route::resource('services', 'ServiceController');

    Route::resource('scrip', '');
    Route::match(['GET', 'POST'], 'transactions', 'TransactionsController@getTransactionEditForm')->name('transactions.getForm');
    Route::post('editTransaction', 'TransactionsController@editTransaction')->name('editTransaction');
    Route::post('verifyService', 'ServiceController@verifyService')->name('verifyService');
    Route::get('user_services/{id}', 'ServiceController@showUserService')->name('user_services.showUserService');
    Route::get('user_services/resend_verification/{id}', 'ServiceController@resendVerification')->name('user_services.resendVerification');

    /**
    * Service Delivery Management
    */
    /*Route::get('service_delivery/get_client', 'ServiceDeliveryController@getClients');
    Route::get('service_delivery/get_stocks', 'ServiceDeliveryController@getStocks');
     Route::get('service_delivery/get_open_services', 'ServiceDeliveryController@getOpenServices');
    Route::get('service_delivery/get_nfo_stocks', 'ServiceDeliveryController@getNfoStock');*/


    Route::get('service_delivery/adjust', 'ServiceDeliveryController@adjust')->name('service_delivery.adjust');
    Route::get('service_delivery/get_service_deliveries', 'ServiceDeliveryController@getServiceDeliveries');
    Route::post('service_delivery/get_service_deliveries', 'ServiceDeliveryController@getServiceDeliveries');
    Route::get('service_delivery/profit_loss/service', 'ServiceDeliveryController@pnlServiceBased')->name('pnl.service');
    Route::get('service_delivery/profit_loss/stock', 'ServiceDeliveryController@pnlStockBased')->name('pnl.stock');
    Route::post('service_delivery/get_nfo_stocks', 'ServiceDeliveryController@getNfoStock');
    Route::post('service_delivery/get_services', 'ServiceDeliveryController@getServices');
    Route::post('service_delivery/get_client', 'ServiceDeliveryController@getClients');
    Route::post('service_delivery/get_templates', 'ServiceDeliveryController@getTemplates');
    Route::post('service_delivery/get_stocks', 'ServiceDeliveryController@getStocks');
    Route::post('service_delivery/get_open_services', 'ServiceDeliveryController@getOpenServices');
    Route::get('service_delivery/userlist', 'ServiceDeliveryController@userList')->name('service_delivery.userlist');
    Route::get('service_delivery/send_alerts', 'ServiceDeliveryController@sendAlerts')->name('service_delivery.send_alerts');
    Route::post('service_delivery/post_alerts', 'ServiceDeliveryController@postAlerts')->name('service_delivery.post_alerts');
    Route::post('service_delivery/filter_user', 'ServiceDeliveryController@filterAlertUser')->name('send_alert.filter_user');
    Route::get('service_delivery/open_calls', 'ServiceDeliveryController@openCalls')->name('service_delivery.open_calls');
    Route::post('service_delivery/update_suitabilty', 'ServiceDeliveryController@updateSuitability')->name('service_delivery.update_suitabilty');
    Route::resource('service_delivery', 'ServiceDeliveryController');
    Route::get('serviceCustomerData', 'ServiceDeliveryController@customerData')->name('service_delivery.serviceCustomerData');

    /**
    * Active Services
    */
    Route::get('active_services', 'ServiceController@indexActiveServices')->name('services.active_services');
    Route::get('activeServiceData', 'ServiceController@activeServiceData')->name('services.activeServiceData');
    Route::post('activeServices/destroy/{id}', 'ServiceController@destroyActiveService')->name('services.destroyActiveServices');
    Route::post('terminateService', 'ServiceController@terminateService')->name('services.terminateService');
    Route::post('tempExtnService', 'ServiceController@tempExtnService')->name('services.tempExtnService');
    Route::get('active_services/edit/{id}', 'ServiceController@editActiveService')->name('active_service.edit');
    Route::get('active_services/renew/{id}', 'ServiceController@renewUserService')->name('active_service.renew');


    Route::group(['prefix' => 'teams'], function()
    {
        Route::get('/', 'TeamController@index')->name('teams.index');
        Route::get('create', 'TeamController@create')->name('teams.create');
        Route::post('teams', 'TeamController@store')->name('teams.store');
        Route::get('edit/{id}', 'TeamController@edit')->name('teams.edit');
        Route::put('edit/{id}', 'TeamController@update')->name('teams.update');
        Route::delete('destroy/{id}', 'TeamController@destroy')->name('teams.destroy');
        Route::get('switch/{id}', 'TeamController@switchTeam')->name('teams.switch');

        Route::get('members/{id}', 'TeamMemberController@show')->name('teams.members.show');
        Route::get('members/resend/{invite_id}', 'TeamMemberController@resendInvite')->name('teams.members.resend_invite');
        Route::post('members/{id}', 'TeamMemberController@invite')->name('teams.members.invite');
        Route::delete('members/{id}/{user_id}', 'TeamMemberController@destroy')->name('teams.members.destroy');
    });

    Route::group(['prefix' => 'api'], function()
    {
        Route::get('get_stock_list', 'CompanyMastersController@getStockList')->name('api.get_stock_list');
    });

    Route::group(['prefix' => 'event'], function()
    {
        Route::get('my_event/{msg}', function($msg){
            event(new \App\Events\MyEvent($msg));
            return "DONE";
        })->name('event.my_event');
    });

    /**
    * Sources
    */
    Route::resource('sources', 'SourcesController');
    

});

