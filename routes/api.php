<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/get_plan/{id}', 'Admin\PlansController@getPlanNew', function () {
    return $data;
});

Route::get('/add_source_new/{val}', 'Admin\SourcesController@store', function(Request $request) {
    return $request->val;
});