
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rothmans CRM</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/colors.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <style type="text/css">
        body {
            background-color: #484848;
        }
        h1  {
            color: #fff;
        }
    </style>
    <!-- Core JS files -->
    <script type="text/javascript" src="{{asset('js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <script type="text/javascript" src="{{asset('js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/ui/ripple.min.js')}}"></script>
    <!-- /theme JS files -->

</head>

<body class="login-container">

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">
                {{Html::image('images/logo.svg', 'Intelli Stocks', ['class' => 'col-md-6 col-md-offset-3 border-bottom-grey-300'])}}

                <div class="col-sm-12 text-center">
                    @if($status == '1')
                        <h1>Congratulations! Your details have been confirmed successfully. Your services will be activated in next 24-48 hours.</h1>
                    @elseif($status == '2')
                        <h1>Oh Snap! Something went wrong try again later.</h1>
                    @elseif($status == '3')
                        <h1>Oh Snap! Either your service is already activated or dosn't exist, Contact Admin.</h1>
                    @elseif($status == '4')
                        <h1>Looks Like Your Service Activation Request is already Registered.</h1>
                    @endif
                </div>


                <!-- Footer -->
                <div class="footer text-muted text-center">
                    &copy; {{date('Y')}}. {{link_to('/', 'Rothmans CRM')}}
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
</div>
<!-- /page container -->

</body>
</html>