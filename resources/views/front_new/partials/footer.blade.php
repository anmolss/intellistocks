<div class="footer">
	<div class="container">
		<div class="row">
			<div class="footer_block clearfix">
				<div class="col-xs-12 col-sm-4">
					<div class="ftr-one">
						<a href="{{ url('/management') }}" class="ft-logo">
						{{ Html::image('images/logo.svg', 'Rothmans CRM') }}
						</a>
						<p>
							We help you reach immense wealth. Follow our leads backed by knowledge & Expertise.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>