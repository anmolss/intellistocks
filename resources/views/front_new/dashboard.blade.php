@extends('front_new.layouts.default')

@section('page_title', 'Dashboard')
@section('page_subtitle', 'Welcome,  '.Auth::user()->name)

@section('content')
	@if(count($expiring_services) > 0)
	<!--
		<div class="row">
			<div class="col-sm-12">
				<div class="panel no-border-radius panel-body border-left-warning">
					<h3>Expiring Services in next 30 Days</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-xxs table-framed">
							<thead>
								<tr>
									<th>#</th>
									<th>Service</th>
									<th>Expiry</th>
								</tr>
							</thead>
							<tbody>
								@foreach($expiring_services as $no => $expiring_service)
									<tr>
										<td>{{$no+1}}</td>
										<td>{{json_decode($expiring_service['service_details'],true)['name']}} ( {{json_decode($expiring_service['plan_details'],true)['name']}} )</td>
										<td>{{(new Carbon($expiring_service['expire_date']))->format('d-m-Y h:i A')}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
			    </div>
			</div>
		</div>
	-->
	@endif
	<div class="row">
		<div class="col-sm-9">
			<div class="panel no-border-radius panel-white border-grey" id="overall_summary_panel">
				<div class="panel-heading text-center p-10">
					<h6 class="panel-title text-uppercase no-padding">Overall Summary</h6>
				</div>
				<div class="panel-body text-center row">
					<div class="col-md-5">
						<h4 class="text-semibold no-padding">Total Service Capital</h4>
						<p class="text-muted text-semibold small">allowed to invest in different service</p>
						<h4 class="text-primary">{{ money_format('%!i', $pnl_all['total_capital']) }}</h4>
					</div>
					<div class="col-md-3">
						<h4 class="no-padding">Total Profit/Loss </h4>
						<p class="text-muted text-semibold small">&nbsp;</p>
						<h4 class="{{ ($pnl_all['total_profit'] > 0)?'text-success':'text-danger' }}">{{ money_format('%!i', $pnl_all['total_profit']) }}</h4>
					</div>
					<div class="col-md-4">
						<h4 class="no-padding">Today's Profit/Loss</h4>
						<p class="text-muted text-semibold small">&nbsp;</p>
						<h4 class="{{ ($pnl_all['today_pnl'] > 0)?'text-success':'text-danger' }}">{{ money_format('%!i', $pnl_all['today_pnl']) }}</h4>
					</div>
				</div>
			</div>

			@if(isset($pnl_all['services']))
				<div class="panel no-border-radius panel-white panel-body border-grey">
					<table class="table text-center table-borderless">
						<tbody>
							<tr class="text-center text-size-small text-bold text-uppercase">
								<td>Service</td>
								<td>Service Capital</td>
								<td>Total Profit/Loss</td>
								<td>Today's Profit/Loss</td>
								<td>Realised Profit/Loss</td>
								<td>Open Profit/Loss</td>
							</tr>

						@foreach($pnl_all['services'] as $service)
							<tr>
								<td class="text-primary">{{ $service['service_name'] }}</td>
								<td class="text-primary">{{ money_format('%!i', $service['service_capital']) }}</td>
								<td class="{{ ($service['total_profit'] > 0)?'text-success':'text-danger' }}">{{ money_format('%!i', $service['total_profit']) }}</td>
								<td class="{{ ($service['today_pnl'] > 0)?'text-success':'text-danger' }}">{{ money_format('%!i', $service['today_pnl']) }}</td>
								<td class="{{ ($service['booked_profit'] > 0)?'text-success':'text-danger' }}">{{ money_format('%!i', $service['booked_profit']) }}</td>
								<td class="{{ ($service['open_profit'] > 0)?'text-success':'text-danger' }}">{{ money_format('%!i', $service['open_profit']) }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			@endif
		</div>
		<div class="col-sm-3">
			<div class="panel no-border-radius panel-white border-grey mb-10">
				<div class="panel-heading text-center p-10">
					<h6 class="panel-title text-uppercase no-padding">Services and Plans</h6>
				</div>
			</div>
				@forelse($user_services as $no => $user_service)
					@if($user_service->service_status == 'Active')
					<div class="panel no-border-radius bg-green-600">
						<div class="panel-heading  ml-15 mr-15 no-padding pt-15">
							<h4 class="panel-title text-capitalize no-padding">{{json_decode($user_service['service_details'], true)['name']}}</h4>
						</div>
						<div class="panel-body ">
							<p><span class="text-uppercase text-semibold">Plan : </span>{{json_decode($user_service->plan_details, true)['name']}}</p>
							<br>
							<p><span class="text-uppercase text-semibold">Status : </span>
								@if($user_service->service_status == 'Active')
									<?php $date_label = 'Renewal Date'; ?>
									<span class="bg-success text-highlight">{{$user_service->service_status}}</span>
								@elseif($user_service->service_status == 'Pending')
									<?php $date_label = 'hide'; ?>
									<span class="bg-warning text-highlight">{{$user_service->service_status}}</span>
								@elseif($user_service->service_status == 'Expired')
									<?php $date_label = 'Expired'; ?>
									<span class="bg-danger text-highlight">{{$user_service->service_status}}</span>
								@else
									<?php $date_label = 'Expired'; ?>
									<span class="bg-slate-800 text-highlight">{{$user_service->service_status}}</span>
								@endif
							</p>
							<br>
							@if($date_label != 'hide')
								<p><span class="text-uppercase text-semibold">{{$date_label}} :</span> <br>{{(new Carbon($user_service->expire_date))->formatLocalized('%A')}} </span>, {{(new Carbon($user_service->expire_date))->formatLocalized('%B %d, %Y')}}</p>
							@endif
							<a href="https://www.intellistocks.com/intelli-demo/intelliportfolio/purchase-2?plan=6" target="_blank" class="btn btn-default btn-block">Renew</a>

						</div>
					</div>
					@endif
				@empty
					<div class="panel no-border-radius panel-body text-center">
						<h4 class="text-muted p-5">No Active Services Found...!!!</h4>
					</div>
				@endforelse
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel no-border-radius panel-flat table-responsive">
				<table class="table text-center table-xlg">
					<thead>
						<tr class="bg-teal text-uppercase">
			              	<th rowspan="2" class="text-center" width="50">#</th>
			              	<th rowspan="2" class="text-center">Service</th>
			              	<th rowspan="2" class="text-center">Plan</th>
			              	<th colspan="3" class="text-center">Amount</th>
			              	<th colspan="3" class="text-center">Date</th>
			              	<th rowspan="2" class="text-center" width="85">Status</th>
			              	<!--
			              	<th rowspan="2" class="text-center" width="125">Actions</th>
			            	-->
			            </tr>
			            <tr class="bg-teal text-uppercase">
			              	<th class="text-center" width="100">Payable</th>
			              	<th class="text-center" width="100">Paid</th>
			              	<th class="text-center" width="100">Uncleared</th>
			              	<th class="text-center" width="115">Activation</th>
			              	<th class="text-center" width="115">Expiry</th>
			              	<th class="text-center" width="125">Created</th>
			            </tr>
					</thead>
					<tbody>
						@forelse($user_services as $key => $user_service)
			            <tr>
			              <td>{{$key+1}}</td>
			              <td>{{json_decode($user_service->service_details, true)['name']}}</td>
			              <td>{{json_decode($user_service->plan_details, true)['name']}}</td>
			              <?php
			                foreach ($user_service->transections as $transaction) {
				                if($user_service->latest_transaction_history_id == $transaction->transaction_history_id){
				                	$amount_payable = $transaction->payable_amount;
				                } else {
				                	$amount_payable = 0;
				                }
				            }    
				                if(isset($user_service->transections)) {
				                    $all = $user_service->transections->toArray();
				                    if(count($all) > 0){
				                      $amount_paid = array_sum(
				                          array_map(
				                            function($item) use ($user_service)  {
				                              if($item['cleared'] == 'Yes' && $user_service->latest_transaction_history_id == $item['transaction_history_id']) { 
				                                return $item['amount']; 
				                              } else { 
				                                return 0; 
				                              } 
				                            }, $all
				                          )
				                        );
				                  }
				                }

				              $balance = round($amount_payable, 0, PHP_ROUND_HALF_UP) - round($amount_paid, 0, PHP_ROUND_HALF_UP);

			              ?>

			              <td>{{$amount_payable}} Rs.</td>
			              <td>{{$amount_paid}} Rs.</td>
			              <td>{{$balance}} Rs.</td>
			              <td>{{(new Carbon($user_service->activation_date))->format('d-m-Y')}}</td>
			              <td>{{(!empty($user_service->expire_date))?((new Carbon($user_service->expire_date))->format('d-m-Y')):'N/A'}}</td>
			              <td>{{(new Carbon($user_service->created_at))->format('d-m-Y h:i A')}}</td>
			              <td>
			                @if($user_service->service_status == 'Active')
			                  <label class="bg-success text-highlight">{{$user_service->service_status}}</label>
			                @else
			                  <label class="bg-warning text-highlight">{{$user_service->service_status}}</label>
			                @endif
			              </td>
			              <!--
			              <td class="text-center">
			                <ul class="icons-list">
	                            <li><a href="{{route('front::services.userServiceView', $user_service->id)}}" class="btn btn-info btn-sm" title="View"><i class="fa fa-eye"></i></a></li>
	                        </ul>
			              </td>
			              -->
			            </tr>
			          @empty
			            <tr>
			              <td colspan="11">
			                <div class="alert bg-warning alert-rounded">
			                  <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
			                  <span class="text-semibold">No Data!</span> No Services Assigned.
			                </div>
			              </td>
			            </tr>
			          @endforelse 
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<br>
@endsection

<style type="text/css">
	#overall_summary_panel h4 {
		padding-bottom: 0px !important;
	}
</style>