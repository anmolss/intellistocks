@extends('front.layouts.app')
@section('page_title', 'Profile')

@section('page_subtitle', 'Edit Profile')
@section('content')
	<div class="row">
		<div class="col-sm-6">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-user position-left"></i> Uneditable details</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
	            	</div>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

				<table class="table table-borderless table-xs content-group-sm">
					<tbody>
						<tr>
							<td><i class="icon-vcard position-left"></i> Username:</td>
							<td class="text-right">{{(!empty($user['username']))?$user['username']:'N/A'}}</td>
						</tr>
						<tr>
							<td><i class="icon-mail-read position-left"></i> Email:</td>
							<td class="text-right">
								{{(!empty($user['email']))?$user['email']:'N/A'}} 
								<a href="#">Change</a>
							</td>
						</tr>
						<tr>
							<td><i class="icon-iphone position-left"></i> Mobile:</td>
							<td class="text-right">
								{{(!empty($user['mobile']))?$user['mobile']:'N/A'}}
								<a href="#">Change</a>
							</td>
						</tr>
						<tr>
							<td><i class="icon-lock position-left"></i> Password:</td>
							<td class="text-right">
								<a href="{{route('front::users.changePassword')}}">Change</a>
							</td>
						</tr>
						<tr>
							<td><i class="icon-calendar52 position-left"></i> Joined On:</td>
							<td class="text-right">{{(!empty($user['created_at']))?(new Carbon($user['created_at']))->format('d/m/Y'):'N/A'}}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-pencil position-left"></i> Editable details</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
	            	</div>
					<a class="heading-elements-toggle"><i class="icon-more"></i></a>
				</div>
				<div class="panel-body">
					{!! Form::open(['route' => 'front::users.edit', 'class' => 'form-horizontal']) !!}
						<fieldset class="content-group">
							<legend class="text-bold">Edit Profile</legend>

							<div class="form-group">
								<label class="control-label col-lg-2">Name</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" name="name" placeholder="Enter your Name" value="{{$user['name']}}">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-2">Add Line 1:</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" name="address1" placeholder="Enter your Address Line 1" value="{{$user['address1']}}">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-2">Add Line 2:</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" name="address2" placeholder="Enter your Address Line 2" value="{{$user['address2']}}">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-2">City:</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" name="city" placeholder="Enter your City" value="{{$user['city']}}">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-2">State:</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" name="state" placeholder="Enter your State" value="{{$user['state']}}">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-2">Zip Code:</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" name="zip_code" placeholder="Enter your Zip Code" value="{{$user['zip_code']}}">
								</div>
							</div>
						</fieldset>
						<div class="text-right">
							<button type="submit" class="btn btn-primary legitRipple">Submit <i class="icon-arrow-right14 position-right"></i><span class="legitRipple-ripple"></span></button>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection