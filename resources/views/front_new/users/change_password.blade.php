@extends('front.layouts.app')
@section('page_title', 'Profile')

@section('page_subtitle', 'Change Password')
@section('content')
	<div class="row">
		<div class="col-sm-6">
			@if(isset($error) && !empty($error))
				<div class="panel bg-danger">
					<div class="panel-heading">
						<h6 class="panel-title">Error!</h6>
					</div>

					<div class="panel-body">
						<ul class="list no-margin-bottom">
							@foreach($error as $no => $e)
								<li>{{$e}}</li>
							@endforeach
						</ul>
					</div>
				</div>
			@endif
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-lock position-left"></i> Change Password</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
	            	</div>
					<a class="heading-elements-toggle"><i class="icon-more"></i></a>
				</div>
				<div class="panel-body">
					{!! Form::open(['route' => 'front::users.changePassword', 'class' => 'form-horizontal']) !!}
						<fieldset class="content-group">
							<div class="form-group">
								<label class="control-label col-lg-4">Current Password:</label>
								<div class="col-lg-8">
									<input type="password" class="form-control" name="current_password">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-4">New Password:</label>
								<div class="col-lg-8">
									<input type="password" class="form-control" name="new_password">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-4">Repeat New Password:</label>
								<div class="col-lg-8">
									<input type="password" class="form-control" name="repeat_new_password">
								</div>
							</div>
						</fieldset>
						<div class="text-right">
							<button type="submit" class="btn btn-primary legitRipple">Submit <i class="icon-arrow-right14 position-right"></i><span class="legitRipple-ripple"></span></button>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection