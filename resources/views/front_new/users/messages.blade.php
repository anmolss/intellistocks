@extends('front_new.layouts.default')
@section('page_title', 'Messages')

@section('content')
    <div class="menu-discription">
        <div class="discription-blk">
            <h3 class="discription-ttl">Messages</h3>
            <h5 class="discription-subttl">Check All messages sent to you</h5>
        </div>
    </div>

    <div class="message-box" ng-controller="MessagesController" ng-cloak>

        <div class="message-list-box">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" class="form-control no-padding" placeholder="Left and right icons">
                    <span class="input-group-addon dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></span>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">One more line</a></li>
                    </ul>
                </div>
            </div>
            <div class="message-list">

                <ul class="media-list media-list-bordered" when-scroll-ends="loadMoreRecords()">
                    <li class="media" ng-repeat="message in messageList ">
                        <div class="media-body">
                            <h6 class="media-heading">@{{ message.dateValue | date }} <span class="status-mark bg-primary"></span></h6>
                            @{{ message.text }}
                        </div>
                    </li>
                </ul>
            </div>

        </div>
        <div class="message-desc">
            <h4 class="message-title heading-divided">Subject</h4>
        </div>


    </div>

@endsection


@push('scripts')
    @include('admin.partials.angular_import')

    <script type="text/javascript">

        app.constant('chunkSize', 50);

        app.controller('MessagesController', function($scope, chunkSize) {
            $scope.messageList = [];

            var currentIndex = 0;
            var todayDate = new Date();
            $scope.loadMoreRecords = function() {
                // Mocking stock values
                // In an real application, data would be retrieved from an
                // external system

                var stock;
                var i = 0;
                while (i < chunkSize) {
                    currentIndex++;
                    var newDate = new Date();
                    newDate.setDate(todayDate.getDate() - currentIndex);
                    if (newDate.getDay() >= 1 && newDate.getDay() <= 50) {
                        stock = {
                            dateValue: newDate,
                            text: 20.0 + Math.random() * 10
                        };
                        $scope.messageList.push(stock);
                        i++;
                    }
                }
            };

            $scope.loadMoreRecords();
        });

        app.directive('whenScrollEnds', function() {
            return {
                restrict: "A",
                link: function(scope, element, attrs) {
                    var visibleHeight = element.height();
                    var threshold = 100;

                    element.scroll(function() {
                        var scrollableHeight = element.prop('scrollHeight');
                        var hiddenContentHeight = scrollableHeight - visibleHeight;

                        if (hiddenContentHeight - element.scrollTop() <= threshold) {
                            // Scroll is almost at the bottom. Loading more rows
                            scope.$apply(attrs.whenScrollEnds);
                        }
                    });
                }
            };
        });
    </script>

@endpush