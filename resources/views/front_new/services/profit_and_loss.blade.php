@extends('front_new.layouts.default')
@section('page_title', 'Profit And Loss')

@section('page_subtitle', 'View Service')

@section('content')
	<style type="text/css">
		table#example thead tr th {
			min-width: 100px;
		}
	</style>
	<!--- **- Inner Menu Content Starts Here -** -->
	<div class="menu-discription">
		<div class="discription-blk">
			<h3 class="discription-ttl">Historical Performances</h3>
			<h5 class="discription-subttl">Following Services Calls are being made to you till yet.</h5>
		</div>
	</div>
	<!--- **- Form Content Starts Here -** -->
	<div class="form-info-block">
		<div class="form-topstrip clearfix">
			<h6>Showing <span> {{count($pnl_services)}} service(s)</span> from {{Request::input('startdate', Carbon::parse('1 year ago')->format('d-m-Y'))}} to {{Request::input('enddate', Carbon::now()->format('d-m-Y'))}}</h6>
			<div class="form-rightstrip">
				<div class="link-group clearfix">
					<a href="#" class="filter-lnk">filter</a>
					<a href="{{route(Route::currentRouteName(), array_merge(Request::all(), ["download" => true]))}}" target="_blank" class="download-lnk">download</a>
				</div>
			</div>
		</div>
		{!! Form::open(['route' => 'front::services.profitAndLoss', 'method' => 'GET', 'id' => 'filter_form']) !!}
		<div class="form-content">
			<h5 class="form-ttl">Filter By</h5>
			<div class="form-block clearfix">
				<label class="frm-lbl" for="name">By Service:</label>
				<div class="frm-input-blk">
					{!! Form::select('service[]', $services, $default_service, ['class' => 'frm-input', 'id' => 'services', 'multiple'  ] ) !!}
				</div>
			</div>
			<div class="form-block clearfix last-frm-blk">
				<label class="frm-lbl" for="startdate">By Date:</label>
				<div class="frm-input-blk clearfix">
					<div class="first-input input-blk">
						<input type="text" id="startdate" name="startdate" class="frm-input" value="{{Request::input('startdate', Carbon::parse('1 year ago')->format('d-m-Y'))}}"/>
					</div>
					<div class="last-input input-blk">
						<input type="text" id="enddate" name="enddate" class="frm-input" value="{{Request::input('enddate', Carbon::now()->format('d-m-Y'))}}"/>
					</div>
				</div>
			</div>
			<div class="form-btn-group clearfix">
				<ul class="frmbtn-menu clearfix">
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['this_month'][0]->format('d-m-Y')}}" data-end-date="{{$dates['this_month'][1]->format('d-m-Y')}}">this month</a></li>
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['last_month'][0]->format('d-m-Y')}}" data-end-date="{{$dates['last_month'][1]->format('d-m-Y')}}">previous month</a></li>
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['this_quarter'][0]->format('d-m-Y')}}" data-end-date="{{$dates['this_quarter'][1]->format('d-m-Y')}}">this quarter</a></li>
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['this_year'][0]->format('d-m-Y')}}" data-end-date="{{$dates['this_year'][1]->format('d-m-Y')}}">this year</a></li>
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['last_year'][0]->format('d-m-Y')}}" data-end-date="{{$dates['last_year'][1]->format('d-m-Y')}}">previous year</a></li>
				</ul>
			</div>
			<div class="submit-blk">
				<input type="submit" value="apply" class="submit-btn appBtn"/>
				<a href="javascript:;" class=" appBtn">Cancel</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>


	<div class="panel panel-flat table-responsive small">
		<table class="table table-hover" id="customers-table">
			<thead>
			<tr>
				<th></th>
				<th>Service Name</th>
				<th>Available limit</th>
				<th>Total calls</th>
				<th>Open Calls</th>
				<th>Close Calls</th>
				<th>Positive Calls</th>
				<th>Wining % </th>
				<th>Open P&L</th>
				<th>Realized P&L</th>
				<th>Total P&L</th>
				{{--<th>Nifty Returns P&L</th>
                <th>Return Multiple of Nifty</th>
				<th>ROI Annualized</th>
				<th>Customers with remaining calls</th>
				<th>Expiring/ed Calls</th>
				<th>Today's P&L</th>
				<th>Gross Contribution of winning calls</th>
				<th>Gross Contribution of Losing calls</th>
				<th>Cumulative Returns from all calls</th>
				<th>Reward Vs Risk </th>
				<th>Average months Position held</th>
				<th>Weighted Average cash deployed</th>--}}
			</tr>
			</thead>
			<tbody>
			@if($pnl_services)
				@foreach($pnl_services as $key => $service)
					<tr>
						<th class="clickable cursor-pointer text-blue-700" data-toggle="collapse" data-parent="#customers-table" data-target=".{{str_slug($key)}}"><i class="fa fa-plus-square"></i></th>
						<td>{{ Html::link(route('front::services.profitAndLossStock')."?service[]=".$service['service_id'], $service['service_name'],['target' => '_blank'] ) }}</td>
						<td>{{ 100 - $service['user'][auth()->user()->id]['avail_qty'] }}%</td>
						<td>{{ Html::link(route('front::services.profitAndLossStock')."?service[]=".$service['service_id'], $service['total_call'],['target' => '_blank'] ) }}</td>
						<td>{{ Html::link(route('front::services.profitAndLossStock')."?call_type=Open&service[]=".$service['service_id'], $service['open_call'],['target' => '_blank'] ) }}</td>
						<td>{{ Html::link(route('front::services.profitAndLossStock')."?call_type=Close&service[]=".$service['service_id'], $service['close_call'],['target' => '_blank'] ) }}</td>
						<td>{{ $service['positive_call'] }}</td>
						<td>{{ $service['winning_percent'] }}% </td>
						<td>{{ $service['open_profit'] }}</td>
						<td>{{ $service['booked_profit'] }}</td>
						<td>{{ $service['total_profit'] }}</td>
						{{--<td>Nifty Returns P&L</td>
						<td>Return Multiple of Nifty</td>
						<td>{{ $service['net_roi'] }}%</td>
						<td>{{ $service['avail_user_total'] }}</td>--}}
						{{--<td>Expiring/ed Calls</td>--}}


					</tr>
					<tr class="collapse {{str_slug($key)}}">
						<td colspan="11" class="no-padding">
							<table class="table bg-teal">
								<thead>
								<tr class="bg-teal-700">
									<th colspan="2">Today's P&L</th>
									<th>Gross Contribution of winning calls</th>
									<th>Gross Contribution of Losing calls</th>
									<th>Cumulative Returns from all calls</th>
									<th>Reward Vs Risk </th>
									<th>Average months Position held</th>
									{{--<th>Weighted Average cash deployed</th>--}}
								</tr>
								</thead>
								<tbody>
									<tr class="bg-teal">
										<td colspan="2">{{ $service['today_pnl'] }}</td>
										<td>{{ $service['gross_profit'] }}%</td>
										<td>{{ $service['gross_loss'] }}%</td>
										<td>{{ $service['cumulative_return_all_call'] }}%</td>
										<td>{{ $service['r_v_r'] }}</td>
										<td>{{ $service['months_pos'] }}</td>
										{{--<td>NA</td>--}}
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				@endforeach
			@else
				<tr>
					<th colspan="11" align="center">No Result Found</th>
				</tr>
			@endif
			</tbody>
		</table>
	</div>
@endsection


@push('scripts')
<script type="text/javascript" src="{{asset('js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{asset('bower_components/StickyTableHeaders/js/jquery.stickytableheaders.min.js') }}"></script>
<script type="text/javascript">

    /*$('.daterange-delivery_range').daterangepicker({
        applyClass: 'bg-info',
        cancelClass: 'btn-default',
        locale: {
            format: 'DD/MM/YYYY'
        }
    });*/
    // Enable Select2 select for the length option
    $('select').select2({
        minimumResultsForSearch: Infinity,
        ropdownCssClass: 'border-teal',
        containerCssClass: 'border-teal text-primary-700',
    });
    $('table#customers-table').stickyTableHeaders();


    jQuery(document).ready(function () {
        $( "#startdate" ).datepicker({
            showOn: "button",
            buttonImage: "{{asset('new_front/images/calender.png')}}",
            buttonImageOnly: true,
            buttonText: "Select date",
            dateFormat: 'dd-mm-yy'
        });
        $( "#enddate" ).datepicker({
            showOn: "button",
            buttonImage: "{{asset('new_front/images/calender.png')}}",
            buttonImageOnly: true,
            buttonText: "Select date",
            dateFormat: 'dd-mm-yy'
        });
    });
    function date_filter(e) {
        var start_date = jQuery(e).attr('data-start-date');
        var end_date = jQuery(e).attr('data-end-date');
        jQuery('#startdate').val(start_date);
        jQuery('#enddate').val(end_date);
        jQuery( "#filter_form" ).submit();
    }
</script>

@endpush
