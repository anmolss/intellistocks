@extends('front_new.layouts.default')

@section('page_title', 'Payment History')
@section('page_subtitle', 'Welcome,  '.Auth::user()->name)

@section('content')
	@foreach ($user_services as $user_service)
		<?php $timeline_transactions = $user_service['timeline_transactions']; ?>
		<div class="row">
			<div class="col-sm-12">
				<div class="navbar navbar-default navbar-component navbar-xs text-center">
					<h4 class="navbar-text" style="float:none !important;"><i class="fa fa-direction"></i> <span class="text-semibold">Payment History (Service : {{json_decode($user_service['service_details'], true)['name']}})</span></h4>
				</div>
				@if(isset($timeline_transactions) && !empty($timeline_transactions) && (count($timeline_transactions)>0))
				<!-- Timeline -->
				<div class="timeline timeline-left content-group">
					<div class="timeline-container">
						@foreach ($timeline_transactions as $date => $transactions)
							<!-- Date stamp -->
							<div class="timeline-date text-muted">
								<i class="fa fa-history position-left"></i><span class="text-semibold">{{(new Carbon($date))->formatLocalized('%A')}} </span>, {{(new Carbon($date))->formatLocalized('%B %d, %Y')}}
							</div>
							<!-- /date stamp -->

							@foreach (array_chunk($transactions, 2, true) as $column)
							    <!-- Invoices -->
								<div class="timeline-row">
									<div class="timeline-icon">
										<div class="bg-success">
											<i class="fa fa-money"></i>
										</div>
									</div>

									<div class="row">
							        @foreach ($column as $transaction)
							        	@if($transaction['payment_type'] == 'cash')
							        		<?php $color = 'success'; ?>
										@elseif($transaction['payment_type'] == 'cheque')
											<?php $color = 'primary'; ?>
										@elseif($transaction['payment_type'] == 'net_banking')
											<?php $color = 'warning'; ?>
										@endif
							            <div class="col-lg-6">
											<div class="panel border-left-lg border-left-{{$color}} invoice-grid timeline-content {{($transaction['cleared'] == 'Yes')?'bg-success':'bg-warning'}}">
												<div class="panel-body">
													<div class="row">
														<div class="col-sm-6">
															<h6 class="text-semibold no-margin-top" style="color: #fff;">{{json_decode($user_service['service_details'], true)['name']}}</h6>
															<ul class="list list-unstyled">
																<li>Invoice #: &nbsp;{{($transaction['pay_type'] == 'partial')?$transaction['id'].'_'.$transaction['pay_no']:$transaction['id']}}</li>
																<li>Issued on: <span class="text-semibold">{{(new Carbon($transaction['created_at']))->format('d-m-Y h:i A')}}</span></li>
																<li>Pay Type: {{ucwords($transaction['pay_type'])}}</li>
															</ul>
														</div>

														<div class="col-sm-6">
															<h6 class="text-semibold text-right no-margin-top" style="color: #fff;">Rs. {{$transaction['amount']}}</h6>
															<ul class="list list-unstyled text-right">
																<li>Paid Via: <span class="text-semibold">{{$transaction['payment_type']}}</span></li>
																@if($transaction['payment_type'] == 'cash')
																<li>Transaction No: {{$transaction['transaction_id']}}</li>
																<li>Received From: {{(!empty($transaction['received_from']))?$transaction['received_from']:'N/A'}}</li>
																@elseif($transaction['payment_type'] == 'cheque')
																<li>Cheque No: {{$transaction['cheque_number']}}</li>
																<li>Bank: {{$transaction['bank']}}</li>
																@elseif($transaction['payment_type'] == 'net_banking')
																<li>Transaction No: {{$transaction['transaction_id']}}</li>
																<li>Received From: {{(!empty($transaction['received_from']))?$transaction['received_from']:'N/A'}}</li>
																@endif
																<li>Cleared Status: {{$transaction['cleared']}}</li>
															</ul>
														</div>
													</div>
												</div>

												<div class="panel-footer panel-footer-condensed">
													<div class="heading-elements">
														<span class="heading-text text-default">
															<span class="status-mark border-danger position-left"></span> Pay Date: <span class="text-semibold">{{$date}}</span>
														</span>
														<!--
														<ul class="list-inline list-inline-condensed heading-text pull-right">
															<li><a href="#" class="text-default" data-toggle="modal" data-target="#invoice"><i class="icon-eye8"></i></a></li>
															<li class="dropdown">
																<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
																<ul class="dropdown-menu dropdown-menu-right">
																	<li><a href="#"><i class="icon-printer"></i> Print invoice</a></li>
																	<li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
																</ul>
															</li>
														</ul>
														-->
													</div>
												</div>
											</div>
										</div>
							        @endforeach
							      	</div>
								</div>
								<!-- /invoices -->
							@endforeach
						@endforeach
					</div>
				</div>
				@else
				<div class="navbar navbar-default navbar-component navbar-xs text-center">
					<h4 class="navbar-text" style="float:none !important;">No Transactions Are Made Yet</h4>
				</div>
				@endif
			</div>
		</div>
	@endforeach
@endsection