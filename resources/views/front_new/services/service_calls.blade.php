@extends('front_new.layouts.default')

@section('page_title', 'Service Calls')
@section('page_subtitle', 'Welcome,  '.Auth::user()->name)

@section('content')
<style type="text/css">
	table#example thead tr th {
	    min-width: 100px;
	}
</style>
<!--- **- Inner Menu Content Starts Here -** -->
<div class="menu-discription">
	<div class="discription-blk">
		<h3 class="discription-ttl">Service Calls </h3>
		<h5 class="discription-subttl">Following Services Calls are being made to you till yet.</h5>
	</div>
</div>
<!--- **- Form Content Starts Here -** -->
<div class="form-info-block">
	<div class="form-topstrip clearfix">
		<h6>Showing <span> {{count($service_calls)}} calls</span> from {{Request::input('startdate', Carbon::now()->format('d-m-Y'))}} to {{Request::input('enddate', Carbon::now()->format('d-m-Y'))}}</h6>
		<div class="form-rightstrip">
			<div class="link-group clearfix">
				<a href="#" class="filter-lnk">filter</a>
				<a href="#" class="download-lnk">download</a>
			</div>
		</div>
	</div>
	{!! Form::open(['route' => 'front::services.ServiceCallIndex', 'method' => 'GET', 'id' => 'filter_form']) !!}
		<div class="form-content">
			<h5 class="form-ttl">Filter By</h5>
			<div class="form-block clearfix">
				<label class="frm-lbl" for="service">By Service:</label>
				<div class="frm-input-blk">
					{!! Form::select('service', $services->toArray(), Request::input('service'), ['class' => 'form-control select2', 'placeholder' => 'Select Service'] )!!}
				</div>
			</div>
			<div class="form-block clearfix">
				<label class="frm-lbl" for="symbol">By Symbol:</label>
				<div class="frm-input-blk">
					<input type="text" id="symbol" name="symbol" value="{{Request::input('symbol')}}" class="frm-input"/>
				</div>
			</div>
			<div class="form-block clearfix">
				<label class="frm-lbl" for="call_type">By Call Type:</label>
				<div class="frm-input-blk">
					{!! Form::select('call_type',  config('rothmans.front_sd_call_type'), Request::input('call_type'), ['class' => 'form-control select2'] )!!}
				</div>
			</div>			
			<div class="form-block clearfix last-frm-blk">
				<label class="frm-lbl" for="startdate">By Date:</label>
				<div class="frm-input-blk clearfix">
					<div class="first-input input-blk">
						<input type="text" id="startdate" name="startdate" class="frm-input" value="{{Request::input('startdate', Carbon::now()->format('d-m-Y'))}}"/>
					</div>
					<div class="last-input input-blk">
						<input type="text" id="enddate" name="enddate" class="frm-input" value="{{Request::input('enddate', Carbon::now()->format('d-m-Y'))}}"/>
					</div>
				</div>
			</div>
			<div class="form-btn-group clearfix">
				<ul class="frmbtn-menu clearfix">
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['this_month'][0]->format('d-m-Y')}}" data-end-date="{{$dates['this_month'][1]->format('d-m-Y')}}">this month</a></li>
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['last_month'][0]->format('d-m-Y')}}" data-end-date="{{$dates['last_month'][1]->format('d-m-Y')}}">previous month</a></li>
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['this_quarter'][0]->format('d-m-Y')}}" data-end-date="{{$dates['this_quarter'][1]->format('d-m-Y')}}">this quarter</a></li>
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['this_year'][0]->format('d-m-Y')}}" data-end-date="{{$dates['this_year'][1]->format('d-m-Y')}}">this year</a></li>
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['last_year'][0]->format('d-m-Y')}}" data-end-date="{{$dates['last_year'][1]->format('d-m-Y')}}">previous year</a></li>
				</ul>
			</div>
			<div class="submit-blk">
				<input type="submit" value="apply" class="submit-btn appBtn"/>
				<a href="javascript:;" class=" appBtn">Cancel</a>
			</div>
		</div>
	{!! Form::close() !!}
</div>
<table id="example" cellspacing="0" width="100%" class="table-example">
	<thead>
		<tr>
			<th>Service Name</th>
            <th>Date</th>
			<th>Symbol</th>
            <th>Call Type</th>
			<th>Quantity</th>
			<th>Price</th>
            <th>Message</th>
		</tr>
	</thead>
	<tbody>
		@forelse($service_calls as $key => $service_call)
			<tr>
                <td>{{ $service_call->service->name }}</td>
                <td>{{ \Carbon\Carbon::parse($service_call->created_at)->format(config('rothmans.date_format')) }}</td>
				<td>{{ $service_call->symbol }}</td>
				<td>{{ config('rothmans.front_sd_call_type.'.$service_call->call_type ) }}</td>
				@if($service_call->sd->exchange == 'nse')
                	<td>{{ $service_call->qty }}%</td>
				@else
					<td>{{ $service_call->value }}</td>
				@endif
				<td>{{ $service_call->sd->price }}</td>
                <td align="left">{{ $service_call->msg }}</td>
            </tr>
        @empty
            <tr>
				<td colspan="11">
					<div class="alert bg-warning alert-rounded">
						<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
						<span class="text-semibold">No Data!</span> No Record Found.
					</div>
				</td>
            </tr>
        @endforelse 
	</tbody>
</table>
@endsection

@push('scripts')
	<script type="text/javascript">
		jQuery(document).ready(function () {
			$( "#startdate" ).datepicker({
	            showOn: "button",
	            buttonImage: "{{asset('new_front/images/calender.png')}}",
	            buttonImageOnly: true,
	            buttonText: "Select date",
	            dateFormat: 'dd-mm-yy' 
	        });
	        $( "#enddate" ).datepicker({
	            showOn: "button",
	            buttonImage: "{{asset('new_front/images/calender.png')}}",
	            buttonImageOnly: true,
	            buttonText: "Select date",
	            dateFormat: 'dd-mm-yy' 
	        });
		});
		function date_filter(e) {
			var start_date = jQuery(e).attr('data-start-date');
			var end_date = jQuery(e).attr('data-end-date');
			jQuery('#startdate').val(start_date);
			jQuery('#enddate').val(end_date);
			jQuery( "#filter_form" ).submit();
		}
	</script>
@endpush