@extends('front_new.layouts.default')
@section('page_title', 'Profit And Loss')

@section('page_subtitle', 'View By Stock')
@section('content')
	<style type="text/css">
		table#example thead tr th {
			min-width: 100px;
		}
	</style>
	<!--- **- Inner Menu Content Starts Here -** -->
	<div class="menu-discription">
		<div class="discription-blk">
			<h3 class="discription-ttl">Historical Performances</h3>
			<h5 class="discription-subttl">Following Services Calls are being made to you till yet.</h5>
		</div>
	</div>
	<!--- **- Form Content Starts Here -** -->
	<div class="form-info-block">
		<div class="form-topstrip clearfix">
			<h6>Showing <span> {{count($pnl_stocks)}} stock(s)</span> from {{Request::input('startdate', Carbon::parse('1 year ago')->format('d-m-Y'))}} to {{Request::input('enddate', Carbon::now()->format('d-m-Y'))}}</h6>
			<div class="form-rightstrip">
				<div class="link-group clearfix">
					<a href="#" class="filter-lnk">filter</a>
					<a href="{{route(Route::currentRouteName(), array_merge(Request::all(), ["download" => true]))}}" class="download-lnk">download</a>
				</div>
			</div>
		</div>
		{!! Form::open([ 'method' => 'GET', 'id' => 'filter_form']) !!}
		<div class="form-content">
			<h5 class="form-ttl">Filter By</h5>
			<div class="form-block clearfix">
				<label class="frm-lbl" for="name">By Symbol: </label>
				<div class="frm-input-blk">
					{!! Form::select('symbols[]', $symbols, Request::input('symbols', ''), ['class' => 'frm-input', 'id' => 'services', 'multiple'  ] ) !!}
				</div>
			</div>
			<div class="form-block clearfix">
				<label class="frm-lbl" for="name">By Call Type: </label>
				<div class="frm-input-blk">
					{!! Form::select('call_type', [ '' =>'All', 'Open' => 'Open', 'Close' => 'Close'], Request::input('call_type', ''), ['class' => 'frm-input'  ] ) !!}
				</div>
			</div>
			<div class="form-block clearfix last-frm-blk">
				<label class="frm-lbl" for="startdate">By Date:</label>
				<div class="frm-input-blk clearfix">
					<div class="first-input input-blk">
						<input type="text" id="startdate" name="startdate" class="frm-input" value="{{Request::input('startdate', Carbon::parse('1 year ago')->format('d-m-Y'))}}"/>
					</div>
					<div class="last-input input-blk">
						<input type="text" id="enddate" name="enddate" class="frm-input" value="{{Request::input('enddate', Carbon::now()->format('d-m-Y'))}}"/>
					</div>
				</div>
			</div>
			<div class="form-btn-group clearfix">
				<ul class="frmbtn-menu clearfix">
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['this_month'][0]->format('d-m-Y')}}" data-end-date="{{$dates['this_month'][1]->format('d-m-Y')}}">this month</a></li>
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['last_month'][0]->format('d-m-Y')}}" data-end-date="{{$dates['last_month'][1]->format('d-m-Y')}}">previous month</a></li>
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['this_quarter'][0]->format('d-m-Y')}}" data-end-date="{{$dates['this_quarter'][1]->format('d-m-Y')}}">this quarter</a></li>
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['this_year'][0]->format('d-m-Y')}}" data-end-date="{{$dates['this_year'][1]->format('d-m-Y')}}">this year</a></li>
					<li><a href="javascript:;" onclick="date_filter(this)" data-start-date="{{$dates['last_year'][0]->format('d-m-Y')}}" data-end-date="{{$dates['last_year'][1]->format('d-m-Y')}}">previous year</a></li>
				</ul>
			</div>
			<div class="submit-blk">
				<input type="submit" value="apply" class="submit-btn appBtn"/>
				<a href="javascript:;" class=" appBtn">Cancel</a>
			</div>
		</div>
		{!! Form::hidden('service[]', $default_service[0]) !!}
		{!! Form::close() !!}
	</div>


	<div class="panel panel-flat small">
		<table class="table table-hover table-xxs" id="customers-table">
				<thead>
				<tr class="bg-blue">
					<th>Stock Name</th>
					<th>Status</th>
					<th>Entry date</th>
					<th>Entry Price</th>
					<th>CMP / EXIT PRICE</th>
					{{--<th>Close / Current Date</th>--}}
					<th>Open Profit</th>
					<th>Booked Profit</th>
					<th>Total Profit</th>
					<th>Overall Profit (%)</th>
					<th><span data-toggle="tooltip" title="Open Position / Stock Weight of Total Investment">Open Stock Weight (%)</span></th>
					{{--<th><span data-toggle="tooltip" title="Closed Position / Stock Weight of Total Investment">Close Stock Weight (%)</span></th>
					<th><span data-toggle="tooltip" title="Stock Weight of Total Investment">Total Stock Weight (%)</span></th>--}}
					<th>Net ROI</th>
					{{--<th>Tgt <br> Sl</th>
					<th>Away % from Tgt and SL</th>--}}
				</tr>
				</thead>
				<tbody>
				@foreach($pnl_stocks as $key => $stock)
					<tr>
						<th class="clickable cursor-pointer text-blue-700" data-toggle="collapse" data-parent="#customers-table" data-target=".{{str_slug($key)}}">{{ $stock['symbol'] }}+</th>
						<td>{{ $stock['status'] }}</td>
						<td>{{ $stock['entry_date'] }}</td>
						<td>{{ $stock['avg_price'] }}</td>
						<td>{{ $stock['exit_price'] }}</td>
						{{--<td>{{ $stock['exit_date'] }}</td>--}}
						<td>{{ $stock['open_profit'] }}</td>
						<td>{{ $stock['booked_profit'] }}</td>
						<td>{{ $stock['total_profit'] }}</td>
						<td>{{ $stock['profit_percent'] }}%</td>
						<td>{{ $stock['open_stock_weight'] }}%</td>
						{{--<td>{{ $stock['close_stock_weight'] }}%</td>
						<td>{{ $stock['total_stock_weight'] }}%</td>--}}
						<td>{{ $stock['net_roi'] }}%</td>

						{{--<th>{{ $stock['target_mean'] }} <br> {{ $stock['sl_mean'] }}</th>
						<th>{{ $stock['target_p_away'] }}% <br> {{ $stock['sl_p_away'] }}%</th>--}}
					</tr>

					<tr class="collapse {{str_slug($key)}}">
						<td colspan="17" class="no-padding">
							<table class="table bg-teal table-xxs">
								<caption class="text-center"><h3>Transaction History</h3></caption>
								<thead>
								<tr class="bg-teal-700">
									<th>Transactions Date</th>
									<th>Stock Name</th>
									<th>Price</th>
									<th>Qty(by %)</th>
									<th>Qty(by number)</th>
									<th>Call Type</th>
									<th>Status</th>

									{{--<th>Open Profit</th>
									<th>Booked Profit</th>
									<th>Total</th>--}}
								</tr>
								</thead>
								<tbody>
								@foreach($stock['deliveries'] as $key => $stock)
									<tr class="bg-teal">
										<td>{{ \Carbon\Carbon::parse($stock['entry_date'])->format(config('rothmans.date_format')) }}</td>
										<td>{{ $stock['symbol'] }}</td>
										<td>{{ $stock['price'] }}</td>
										<td>{{ $stock['qty'] }}%</td>
										<td>{{ $stock['curr_qty'] }}</td>
										<td>{{ config('rothmans.front_sd_call_type.'.$stock['service_call_type']) }}</td>
										<td>{{ $stock['status'] }}</td>
										{{--<td>{{ $stock['open_profit'] }}</td>
										<td>{{ $stock['booked_profit'] }}</td>
										<td>{{ $stock['total_profit'] }}</td>--}}
									</tr>
								@endforeach
								</tbody>
							</table>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>

	</div>
@endsection


@push('scripts')
<script type="text/javascript" src="{{asset('js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{asset('bower_components/StickyTableHeaders/js/jquery.stickytableheaders.min.js') }}"></script>
<script type="text/javascript">

	/*$('.daterange-delivery_range').daterangepicker({
	 applyClass: 'bg-info',
	 cancelClass: 'btn-default',
	 locale: {
	 format: 'DD/MM/YYYY'
	 }
	 });*/
    // Enable Select2 select for the length option
    $('select').select2({
        minimumResultsForSearch: Infinity,
        ropdownCssClass: 'border-teal',
        containerCssClass: 'border-teal text-primary-700',
    });
    $('table#customers-table').stickyTableHeaders();


    jQuery(document).ready(function () {
        $( "#startdate" ).datepicker({
            showOn: "button",
            buttonImage: "{{asset('new_front/images/calender.png')}}",
            buttonImageOnly: true,
            buttonText: "Select date",
            dateFormat: 'dd-mm-yy'
        });
        $( "#enddate" ).datepicker({
            showOn: "button",
            buttonImage: "{{asset('new_front/images/calender.png')}}",
            buttonImageOnly: true,
            buttonText: "Select date",
            dateFormat: 'dd-mm-yy'
        });
    });
    function date_filter(e) {
        var start_date = jQuery(e).attr('data-start-date');
        var end_date = jQuery(e).attr('data-end-date');
        jQuery('#startdate').val(start_date);
        jQuery('#enddate').val(end_date);
        jQuery( "#filter_form" ).submit();
    }
</script>

@endpush
