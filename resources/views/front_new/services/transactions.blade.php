<!--- **- Inner Menu Content Starts Here -** -->
<div class="menu-discription">
	<div class="discription-blk">
		<h3 class="discription-ttl">Transaction History </h3>
		<h5 class="discription-subttl">This report helps you understand how various transactions you have done using your funds.</h5>
		<p>To know more about fund transfers and flows through your account, see <a href="#"> Ledger Balance</a></p>
	</div>
</div>
<!--- **- Form Content Starts Here -** -->
<div class="form-info-block">
	<div class="form-topstrip clearfix">
		<h6>Showing <span> 18 transactions</span> from Apr 01, 2016 to Mar 31, 2017</h6>
		<div class="form-rightstrip">
			<div class="link-group clearfix">
				<a href="#" class="filter-lnk">filter</a>
				<a href="#" class="download-lnk">download</a>
			</div>
		</div>
	</div>
	<div class="form-content">
		<h5 class="form-ttl">Filter By</h5>
		<div class="form-block clearfix">
			<label class="frm-lbl" for="name">By Name:</label>
			<div class="frm-input-blk">
				<input type="text" id="name" name="name" class="frm-input"/>
			</div>
		</div>
		<div class="form-block clearfix last-frm-blk">
			<label class="frm-lbl" for="startdate">By Date:</label>
			<div class="frm-input-blk clearfix">
				<div class="first-input input-blk">
					<input type="text" id="startdate" name="startdate" class="frm-input" value="12/2/2017"/>
				</div>
				<div class="last-input input-blk">
					<input type="text" id="enddate" name="enddate" class="frm-input" value="02/3/2017"/>
				</div>
			</div>
		</div>
		<div class="form-btn-group clearfix">
			<ul class="frmbtn-menu clearfix">
				<li><a href="#">this month</a></li>
				<li><a href="#">previous month</a></li>
				<li><a href="#">this quarter</a></li>
				<li><a href="#">this year</a></li>
				<li><a href="#">previous year</a></li>
			</ul>
		</div>
		<div class="submit-blk">
			<input type="submit" value="apply" class="submit-btn appBtn"/>
			<a href="#" class=" appBtn">Cancel</a>
		</div>
	</div>
</div>
<table id="example" cellspacing="0" width="100%" class="table-example">
	<thead>
		<tr>
			<th>Date of Transaction</th>
			<th>Transacted Item</th>
			<th>Transaction Type</th>
			<th>Action</th>
			<th>Quantity</th>
			<th>Price per share/unit</th>
			<th>Detail</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>17 Oct 2016</td>
			<td>ICICI BANK</td>
			<td>Exhg - BSE</td>
			<td>Sell</td>
			<td>10%</td>
			<td>150.00</td>
			<td></td>
		</tr>
		<tr>
			<td>17 Oct 2016</td>
			<td>ICICI BANK</td>
			<td>Exhg - BSE</td>
			<td>Sell</td>
			<td>10%</td>
			<td>150.00</td>
			<td></td>
		</tr>
		<tr>
			<td>17 Oct 2016</td>
			<td>ICICI BANK</td>
			<td>Exhg - BSE</td>
			<td>Sell</td>
			<td>10%</td>
			<td>150.00</td>
			<td></td>
		</tr>
		<tr>
			<td>17 Oct 2016</td>
			<td>ICICI BANK</td>
			<td>Exhg - BSE</td>
			<td>Sell</td>
			<td>10%</td>
			<td>150.00</td>
			<td></td>
		</tr>
		<tr>
			<td>17 Oct 2016</td>
			<td>ICICI BANK</td>
			<td>Exhg - BSE</td>
			<td>Sell</td>
			<td>10%</td>
			<td>150.00</td>
			<td></td>
		</tr>
		<tr>
			<td>17 Oct 2016</td>
			<td>ICICI BANK</td>
			<td>Exhg - BSE</td>
			<td>Sell</td>
			<td>10%</td>
			<td>150.00</td>
			<td></td>
		</tr>
		<tr>
			<td>17 Oct 2016</td>
			<td>ICICI BANK</td>
			<td>Exhg - BSE</td>
			<td>Sell</td>
			<td>10%</td>
			<td>150.00</td>
			<td></td>
		</tr>
		<tr>
			<td>17 Oct 2016</td>
			<td>ICICI BANK</td>
			<td>Exhg - BSE</td>
			<td>Sell</td>
			<td>10%</td>
			<td>150.00</td>
			<td></td>
		</tr>
		<tr>
			<td>17 Oct 2016</td>
			<td>ICICI BANK</td>
			<td>Exhg - BSE</td>
			<td>Sell</td>
			<td>10%</td>
			<td>150.00</td>
			<td></td>
		</tr>
		<tr>
			<td>17 Oct 2016</td>
			<td>ICICI BANK</td>
			<td>Exhg - BSE</td>
			<td>Sell</td>
			<td>10%</td>
			<td>150.00</td>
			<td></td>
		</tr>
	</tbody>
</table>