<!DOCTYPE html>
<!--[if IE 9]>
<html class="lt-ie10" lang="en" ng-app="rothmansApp">
<![endif]-->
<html class="no-js" lang="en" ng-app="rothmansApp">
	<head>
		<title>{{ config('app.name', 'Intellistocks') }}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="bJePYEuRX7I9uYYg0YKhRUU6AHyJg29QcyeJeIVU6QA" />
		<!-- CSRF Token -->
	    <meta name="csrf-token" content="{{ csrf_token() }}">
	    <!-- Scripts -->
	    <script>
	        window.Laravel = <?php echo json_encode([ 'csrfToken' => csrf_token() ]); ?>
	    </script>
		<script src="https://use.typekit.net/uif8svw.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		@section('styles')
	        <link href="{{ asset('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
			<link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	        <link href="{{ asset('new_front/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
	        <link href="{{ asset('new_front/css/jquery-ui.css') }}" rel="stylesheet" type="text/css">
	        <link href="{{ asset('new_front/css/core.css') }}" rel="stylesheet" type="text/css">
	        <link href="{{ asset('new_front/css/components.css') }}" rel="stylesheet" type="text/css">
	        <link href="{{ asset('new_front/css/colors.css') }}" rel="stylesheet" type="text/css">
	        <link href="{{ asset('new_front/css/style.css') }}" rel="stylesheet" type="text/css">
	        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	    @show
	    <style type="text/css">
	    	body {
	    		font-family: 'Montserrat', sans-serif !important;
	    	}
	    </style>
	</head>
	<body>
		<!--- **- Main  Class Starts Here -** -->
		<div class="main">
			<!--- **- Header Starts Here -** -->
			<div class="header">
				<div class="container">
					<div class="row">
						<div class="header-ct clearfix">
							<div class="col-xs-4 col-sm-6 logo-sm">
								<div class="logo">
									<a href="{{ url('/management') }}">
									<img src="{{ asset('new_front/images/logo.svg') }}" alt="logo"/>
									</a>
								</div>
							</div>
							<div class="col-xs-8c ol-sm-6 logo-sm">
								<div class="profile_block clearfix">
									<a href="#"  class="more_reports">More Reports</a>                               
									<a href="#"  class="profile_user"><i class="fa fa-user"></i></a>
									<div class="profile_popup clearfix" style="display:none;">
										<span class="prof_close"><i class="fa fa-times"></i></span>
										<h4 class="profile_title">Latest Updates</h4>
										<div class="profile_right">
											<h5 class="name_title"><a href="{{ route('front::users.viewProfile') }}"><i class="fa fa-user name_profile"></i>{{ Auth::user()->name }}</a></h5>
											<h6 class="profile_subttl">Contact Details of Relationship Manager</h6>
											<div class="prof_info">
												<span><i class="fa fa-user"></i>&nbsp;{{$relation_manager->name}}</span>
												<a href="tell:{{$relation_manager->mobile}}">
												<i class="fa fa-phone"></i>
												+91 {{$relation_manager->mobile}}
												</a>
												<a href="mailto:{{$relation_manager->email}}">
												<i class="fa fa-envelope"></i>
												{{$relation_manager->email}}
												</a>
											</div>
											<a class="btn btn-primary pull-right btn-sm" href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> Logout</a>
											<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
			                                        {{ csrf_field() }}
			                                </form>
										</div>
										<div class="profile_left">
											<h6 class="profile_subttl">Message Center <sup class="msg-count">280</sup></h6>
											<p>
												Dear Customer, your due amount of Rs. 59441 against your Trading A/c EX723640 has exceeded 5
												days. To avoid liquidation of your stocks, kindly clear the due amount or reduce your 
												positions. Team Intellistocks.
											</p>
											<h6 class="profile_subttl">Latest Update</h6>
											<span>No Updates</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--- **- Inner Content Starts Here -** -->
			<div class="content_wrapper">
				<div class="container">
					<div class="row">
						<div class="firstContent clearfix">
							<div class="col-xs-12">
								@if(!isset($no_menu))
									<!--- **- Breadcrumbs Starts Here -** -->
									@include('front_new.partials.breadcrumb')
									<!--- **- Inner Menu Starts Here -** -->
									<div class="page-menu-ct clearfix">
										<h2 class="page_title">@yield('page_title')</h2>
										<div class="menu-wrapper">
											<ul class="page-menu clearfix">
												<li><a href="{{ route("front::dashboard") }}">Overview</a></li>
												<li><a href="{{ route('front::services.profitAndLoss') }}">PERFORMANCE</a></li>
												<li><a href="{{ route('front::services.ServiceCallIndex') }}">Service Calls</a></li>
												<li><a href="{{ route('front::services.PaymentHistory') }}">Payment History</a></li>
												{{--<li><a href="{{ route('front::users.messages') }}">Messages</a></li>--}}
												<li><a href="#">Messages</a></li>
												<li><a href="javascript:;">Offers</a></li>
											</ul>
										</div>
									</div>
								@endif

								@yield('content')
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--- **- Footer Starts Here -** -->
			@include('front_new.partials.footer')
		</div>

		@section('scripts')
		<script type="text/javascript" src="{{asset('new_front/js/jquery.js') }}"></script>
		<script type="text/javascript" src="{{asset('new_front/js/bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{asset('js/plugins/forms/selects/select2.min.js') }}"></script>
		<script type="text/javascript" src="{{asset('new_front/js/jquery.dataTables.min.js') }}"></script>
		<script type="text/javascript" src="{{asset('new_front/js/jquery-ui.js') }}"></script>
		<script type="text/javascript" src="{{asset('new_front/js/custom.js') }}"></script>
			<script type="text/javascript">
				$('select').select2({
					allowclear: true,
					placeholder: null
				});
			</script>
		@show
		@stack('scripts')
	</body>
</html>