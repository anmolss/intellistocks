<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>
	<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([ 'csrfToken' => csrf_token() ]); ?>
    </script>

    @section('styles')
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="{{ asset('front_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front_assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front_assets/css/core.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front_assets/css/components.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front_assets/css/colors.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front_assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    @show

</head>

<body class="navbar-bottom">

	<!-- Page header -->
	<div class="page-header page-header-inverse bg-dark">

		<!-- Main navbar -->
		<div class="navbar navbar-inverse navbar-transparent">
			<div class="navbar-header">
				<a class="navbar-brand" href="{{ url('/management') }}">{{ Html::image('images/logo.svg', 'Rothmans CRM', ['style' => 'width:100px; height:auto;']) }}</a>
				<ul class="nav navbar-nav pull-right visible-xs-block">
					<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-grid3"></i></a></li>
				</ul>
			</div>

			<div class="navbar-collapse collapse" id="navbar-mobile">
				<div class="navbar-right" style="padding: 20px;">
					<ul class="nav navbar-nav">
						@include('front.partials.front_menu_items', array('items' => $CustomerFrontMenu->roots()))

						<li class="dropdown dropdown-user">
							<a class="dropdown-toggle menu-link" data-toggle="dropdown">
								<i class="icon-user"></i>
								<span>{{ Auth::user()->name }}</span>
								<i class="caret"></i>
							</a>

							<ul class="dropdown-menu dropdown-menu-right">
								<li><a href="{{route('front::users.changePassword')}}"><i class="icon-lock2"></i> Change Password</a></li>
								<li><a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> Logout</a></li>
								<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                </form>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- /main navbar -->


		<!-- Floating menu -->
		<ul class="fab-menu fab-menu-top-right" data-fab-toggle="click">
			<li>
				<a class="fab-menu-btn btn bg-yellow btn-float btn-rounded btn-icon">
					<i class="fab-icon-open icon-plus3"></i>
					<i class="fab-icon-close icon-cross2"></i>
				</a>

				<ul class="fab-menu-inner">
					<li>
						<div data-fab-label="Edit Profile">
							<a href="{{route('front::users.edit')}}" class="btn bg-success btn-rounded btn-icon btn-float">
								<i class="icon-pencil"></i>
							</a>
						</div>
					</li>
					<li>
						<div data-fab-label="Change Password">
							<a href="{{route('front::users.changePassword')}}" class="btn btn-warning btn-rounded btn-icon btn-float">
								<i class="icon-lock2"></i>
							</a>
						</div>
					</li>
					<li>
						<div data-fab-label="View Profile">
							<a href="{{route('front::users.viewProfile')}}" class="btn btn-info btn-rounded btn-icon btn-float">
								<i class="icon-eye2"></i>
							</a>
						</div>
					</li>
				</ul>
			</li>
		</ul>
		<!-- /floating menu -->
	</div>
	<!-- /page header -->

	@include('front.partials.breadcrumb')
	
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Dashboard content -->
				<div class="row">
					@yield('content')
				</div>
				<!-- /dashboard content -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	@include('front.partials.footer')
	
	@section('scripts')
	<!-- Core JS files -->
	<script type="text/javascript" src="{{asset('front_assets/js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{asset('front_assets/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{asset('front_assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{asset('front_assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<script type="text/javascript" src="{{asset('front_assets/js/plugins/ui/nicescroll.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('front_assets/js/plugins/ui/drilldown.js')}}"></script>
	<script type="text/javascript" src="{{asset('front_assets/js/plugins/ui/fab.min.js')}}"></script>
	<!-- /core JS files -->
	
	<!-- Theme JS files -->
	<script type="text/javascript" src="{{asset('front_assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
	<script type="text/javascript" src="{{asset('front_assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
	<script type="text/javascript" src="{{asset('front_assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('front_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('front_assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script type="text/javascript" src="{{asset('front_assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('front_assets/js/plugins/pickers/daterangepicker.js')}}"></script>

	<script type="text/javascript" src="{{asset('front_assets/js/core/app.js')}}"></script>

	<script type="text/javascript" src="{{asset('front_assets/js/plugins/ui/ripple.min.js')}}"></script>
	<!-- /theme JS files -->
	@show

	@stack('scripts')

</body>
</html>
