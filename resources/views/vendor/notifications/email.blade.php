
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style type="text/css">
        body{
            padding:0;
            margin:0;
            font-family: 'Open Sans', sans-serif;
            color: #000;
        }
        table,table tr,table tr td{
            border-spacing:0 !important;
        }
        html {
            box-sizing: border-box;
        }
        *, *:before, *:after {
            box-sizing: inherit;
        }
        body{
            margin: 0;
            padding: 0;

        }
        a, a:hover, a:focus{
            outline: none;
            text-decoration: none;
            color: #000;
        }
        ul{
            margin: 0;
            padding: 0;
        }
        ul li{
            list-style: none;
            margin: 0;
            padding: 0;
        }
        @media only screen and (max-width:720px){
            table[class=table-720]{
                width:100% !important;
            }

        }   @media only screen and (max-width:720px){
            table[class=table-sm]{
                width:100% !important;
                text-align:center !important;
                border:none !important;
                padding:0 !important;
            }

        }</style>
</head>

<?php
$style = [
    /* Layout ------------------------------ */

    'body' => 'margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;',
    'email-wrapper' => 'width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;',

    /* Masthead ----------------------- */

    'email-masthead' => 'padding: 25px 0; text-align: center;',
    'email-masthead_name' => 'font-size: 16px; font-weight: bold; color: #2F3133; text-decoration: none; text-shadow: 0 1px 0 white;',

    'email-body' => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;',
    'email-body_inner' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0;',
    'email-body_cell' => 'padding: 35px;',

    'email-footer' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;',
    'email-footer_cell' => 'color: #AEAEAE; padding: 35px; text-align: center;',

    /* Body ------------------------------ */

    'body_action' => 'width: 100%; margin: 30px auto; padding: 0; text-align: center;',
    'body_sub' => 'margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;',

    /* Type ------------------------------ */

    'anchor' => 'color: #3869D4;',
    'header-1' => 'margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;',
    'paragraph' => 'margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;',
    'paragraph-sub' => 'margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;',
    'paragraph-center' => 'text-align: center;',

    /* Buttons ------------------------------ */

    'button' => 'display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
                 background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                 text-align: center; text-decoration: none; -webkit-text-size-adjust: none;',

    'button--green' => 'background-color: #22BC66;',
    'button--red' => 'background-color: #dc4d2f;',
    'button--blue' => 'background-color: #3869D4;',
];
?>

<body>
<table class="body table-720" width="720" style="background: #ffffff; border: 1px solid #40bcfb; padding: " cellspacing="0" cellpadding="0" width="100%" border="0" valign="center" align="center">
    <tr align="center">
        <td>
            <table class="table-720" width="720" cellspacing="0" cellpadding="0" valign="center" align="center" border="0">
                <tr>
                    <td width="48%" style="background: #1a3868; height: 7px;"></td>
                    <td width="37%" style="background: #2698d3; height: 7px;"></td>
                    <td width="15%" style="background: #000000; height: 7px;"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr align="center">
        <td>
            <table class="table-720" width="720" cellspacing="0" cellpadding="0" valign="center" align="center" border="0">
                <tr>
                    <td  style=" padding: 10px;" width="40%">
                        <a href="#" class="logo"><img src="{{ asset('images/logo_mini.png') }}"></a><br />
                        <a href="#" style="font-size: 10.94px;color: #1e3c6a; font-weight: 600; display: inline-block;" >A Rothman​s Investment Advisory Brand <span style="color: #000; display: block;">SEBI Registered Investment Advisor</span> </a>
                    </td>
                    <td style="float:right;text-align:right; display: inline-block;  padding: 10px; ">
                        <p style="margin: 0; font-size: 12.5px;  margin: 0 0 0px; display: inline-block; "> Please add <a href="#" style="font-weight: bold;  color: #000;">support@intellistocks.com </a> </p>
                        <p style="margin: 0; font-size: 12.5px;  margin: 0 0 0px; display: inline-block; ">to your address book to ensure delivery into your inbox.</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;  margin: 30px 0;display: inline-block;width: 100%; vertical-align: middle;color: #fff;background: #00a9ff;">
            <h1>Intellistocks Alert</h1>
        </td>
    </tr>
    <tr>
        <td style=" padding:15px 15px;;display: inline-block;width: 100%; vertical-align: middle;">
            <h2 style="color: #000;font-size: 14.58px;margin: 0 0 20px;display: inline-block;font-weight: bold;width: 100%;">
                @if (! empty($greeting))
                    {{ $greeting }}
                @else
                    @if ($level == 'error')
                        Whoops!
                    @else
                        Hello!
                    @endif
                @endif,</h2>

            <!-- Intro -->
            @foreach ($introLines as $line)
                <p style="font-size: 13.33px;font-weight:bold;color: #000;">
                    {{ $line }}
                </p>
            @endforeach


        <!-- Action Button -->
            @if (isset($actionText))
                <table style="{{ $style['body_action'] }}" align="center" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center">
                            <?php
                            switch ($level) {
                                case 'success':
                                    $actionColor = 'button--green';
                                    break;
                                case 'error':
                                    $actionColor = 'button--red';
                                    break;
                                default:
                                    $actionColor = 'button--blue';
                            }
                            ?>

                            <a href="{{ $actionUrl }}"
                               style="{{ $style['button'] }} {{ $style[$actionColor] }}"
                               class="button"
                               target="_blank">
                                {{ $actionText }}
                            </a>
                        </td>
                    </tr>
                </table>
            @endif


            <!-- Outro -->
            @foreach ($outroLines as $line)
                <p style="font-size: 11.66px;color: #000;">
                    {{ $line }}
                </p>
            @endforeach


            <p style="font-size: 11.66px;color: #000;">Our research is based on thorough, timely, in-depth analysis of companies, industries, markets and world economies. </p>
            <p style="font-size: 11.66px;color: #000;">Happy Earning, <br/>Team Intellistocks</p>


            <!-- Sub Copy -->
            @if (isset($actionText))
                <table style="{{ $style['body_sub'] }}">
                    <tr>
                        <td style="{{ $fontFamily }}">
                            <p style="{{ $style['paragraph-sub'] }}">
                                If you’re having trouble clicking the "{{ $actionText }}" button,
                                copy and paste the URL below into your web browser:
                            </p>

                            <p style="{{ $style['paragraph-sub'] }}">
                                <a style="{{ $style['anchor'] }}" href="{{ $actionUrl }}" target="_blank">
                                    {{ $actionUrl }}
                                </a>
                            </p>
                        </td>
                    </tr>
                </table>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            <h2 style="font-size: 14.58px;color: #000;display: inline-block;width: 100%;
    padding: 0 0 0 15px;border-bottom: 1px solid #9c9c9c;">Get In Touch With Us</h2>
        </td>
    </tr>
    <tr align="center">
        <td>
            <table class="table-720" width="720" cellspacing="0" cellpadding="0" valign="center" align="center" border="0">
                <tr>
                    <td style="display: inline-block; vertical-align: top;border-right:1px solid #8a8a8a;
    float: left; width:50% ">
                        <ul style="display: inline-block;width: 100%; margin: 20px 0 0 27px;" >
                            <li style="width: 100%;vertical-align: middle;margin: 10px 0;"> <a href="#"><img src="{{ asset('images/phone.png') }}" style="vertical-align: middle;">: 0124 668 8250</a></li>
                            <li style="width: 100%;vertical-align: middle;margin: 10px 0;"> <a href="#"><img src="{{ asset('images/email.png') }}" style="vertical-align: middle;">: support@intellistocks.com</a></li>
                            <li style="width: 100%;vertical-align: middle;margin: 10px 0;"><a href="#"><img src="{{ asset('images/globle.png') }}" style="vertical-align: middle;">: Intellistocks.com</a></li>
                        </ul>
                    </td>
                    <td style="display: inline-block; vertical-align: middle; padding-left: 100px;" >
                        <h2>Follow Us</h2>
                        <ul style="font-size: 20.83px; text-align: center;">
                            <li style="display: inline-block;"><a href="#"><img src="{{ asset('images/facebook.png') }}"></a></li>
                            <li style="display: inline-block;"><a href="#"><img src="{{ asset('images/twitter.png') }}"></a></li>
                            <li style="display: inline-block;"><a href="#"><img src="{{ asset('images/linkdink.png') }}"></a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="display: inline-block;width: 100%;margin: 30px 0 0;padding: 9px 27px 28px;vertical-align: middle;float: left; background: #f5f5f5;">
            <p style="display: inline-block;width: 100%;vertical-align: top;font-size: 11.67px;"><strong>Disclaimer: </strong>Intellistocks.com is owned by Rothmans Investment Advisory which is a SEBI registered investment  advisor. Please see our website's services section should you wish to receive advice from us. All the services on this  website are marketed by Intellistocks Inc. Our recommendations rely on historical data, past performance and back tests  does not guarantee future results, and the likelihood of investment outcomes is hypothetical in nature. Unless otherwise specified, all return figures shown above are for illustrative purposes only, and are not actual customer or model returns. Actual returns will vary greatly and depend on personal and market circumstances. All investments entail risks. There is no guarantee that investment strategies will achieve the desired results under all market conditions and each investor should evaluate its ability to invest for a long term especially during periods of a market downturn. No representation is being made that any account, product, or strategy will or is likely to achieve profits, losses, or results similar to those discussed, if any. The information contained herein is based on our analysis and upon sources that we consider reliable. We, however, do not vouch for the accuracy or the completeness thereof. This material is for personal information and we are not responsible for any loss incurred based upon it. This website is solely for the personal information of the visitor. The products discussed or recommended here may not be suitable for all investors. Investors must make their own informed decisions based on their specific objectives and financial position and using such independent advice, as they believe necessary. By visiting our website, you agree that all the claims or causes of action arising out of or relating to your use of this Website will be at Gurgaon jurisdiction only. All Disputes are subjected to exclusive jurisdiction of the courts at Gurgaon.</p>
        </td>
    </tr>

</table>
</body>
</html>