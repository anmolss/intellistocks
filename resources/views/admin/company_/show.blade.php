@extends('admin.layout.app')

@section('page_title', $company['company_name'])
@section('page_subtitle', 'List of company')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="over-title margin-bottom-15">
                Company
            </h5>
        </div>
        <div class="panel-body">
            <h4>Company Master</h4>
            <table class="table table-striped table-hover" id="sample-table-2">

                <tbody>
                @if(count($company) > 0)
                    @include('admin.company.basic_list', compact('company'))
                @else
                    <tr>
                        <td colspan="6" align="center">Not Found</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

