@extends('admin.layout.app')

@section('page_title', 'Company')
@section('page_subtitle', 'List of company')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="over-title margin-bottom-15">
                Company
            </h5>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-hover" id="sample-table-2">
                <thead>
                <tr>
                    <th>Company_code</th>
                    <th>company_name</th>
                </tr>
                </thead>
                <tbody>
                @if(count($list) > 0)
                    @foreach($list as  $key => $item)
                        <tr>
                            <td>
                                <a href="{{route('admin::companies.show', ['companies' => $item->company_code])}}" tooltip-placement="top" tooltip="Edit"  class="text-primary-600">{{ $item->company_code }}</a>
                            </td>
                            <td>{{ $item->company_name }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" align="center">No Roles Found</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        $('a.delete_anchor').on('click', function(){
            if(confirm('Are you sure you want to detele this?')){
                var form = $(this).closest("form");
                form.submit();
            } else {
                return false;
            }
        });
    </script>
@endsection