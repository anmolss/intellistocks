@foreach($company as $key => $value)
    @if(is_array($value))
    </tbody>
    </table>
    <h4>{{camel_case($key)}}</h4>
    <table class="table table-striped table-hover" id="sample-table-2">
    <tbody>

    @include('admin.company.basic_list', ['company' => $value])
    @else
        <tr>
            <td>
                {{camel_case($key)}}
            </td>
            <td>{{ $value }}</td>
        </tr>

    @endif
@endforeach