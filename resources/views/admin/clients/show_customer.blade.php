@extends('admin.layout.app')

@section('page_title', 'Customer')
@section('page_subtitle', 'view')
@section('content')
<link href="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css" />
	<div class="panel panel-flat">
		<div class="panel-body">
			<ul class="icons-list pull-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-cog7"></i>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#" data-toggle="modal" data-target="#customer_edit_form"><i class="icon-pencil4"></i> Edit Customer</a></li>
						@if($user->consent_status == 'No')
                          <li>
                              <a href="{{route('admin::user_services.resendVerification', $user->id)}}" class="resend_activation"><i class="icon-alarm-check"></i> Resend Verification Mail</a>
                          </li>
                        @endif
						<li>
							<a  href="javascript:"
								data-id="{{$user->id}}" 
								data-transcript="{{$user->transcript_status}}" 
								data-kyc="{{$user->kyc_status}}" 
								data-consent="{{$user->consent_status}}"
								data-consent-ip="{{$user->consent_ip}}"
								class="user_verify"> <i class="icon-file-check2"></i> Verify Customer
                            </a>
						</li>
					</ul>
				</li>
			</ul>
			@include('admin.partials.customerheader')
			<div class="clearfix"></div>
			<br />
			<div class="col-md-12 currenttask">
		        <ul class="nav nav-tabs nav-tabs-xs nav-tabs-solid nav-tabs-component nav-justified small">
					<li {{(!isset($_REQUEST['tab']))?'class=active':''}}><a data-toggle="tab" href="#profile"><small>Profile </small></a></li>
					<li><a data-toggle="tab" href="#services"><small>Summary </small></a></li>
		          	<li><a data-toggle="tab" href="#transactions"><small>Transactions</small></a></li>
		          	<li {{(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'email')?'class=active':''}}><a data-toggle="tab" href="#email"><small>Email</small></a></li>
		          	<li {{(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'sms')?'class=active':''}}><a data-toggle="tab" href="#sms"><small>SMS</small></a></li>
		          	<li {{(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'service_delivery_report')?'class=active':''}}><a data-toggle="tab" href="#service_delivery_report">Logs</a></li>
					@foreach($pnl_services as $service)
						<li {{(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'p_l_'.$service['service_id'].'_class')?'class=active':''}}><a data-toggle="tab" href="#p_l_{{$service['service_id']}}_class"><small>{{$service['service_name']}}</small></a></li>
					@endforeach
		        </ul>
		        <div class="tab-content">
		        	@include('admin.clients.tabs.profile')
		        	@include('admin.clients.tabs.activeservicestab')
			        @include('admin.clients.tabs.transectiontab')
			        @include('admin.clients.tabs.emailtab')
			        @include('admin.clients.tabs.smstab')
			        @include('admin.clients.tabs.service_delivery_report_tab')
					@include('admin.clients.tabs.p_l_class_tab')
		        </div>
		    </div>
		</div>
	</div>
	<!-- Customer Edit form modal -->
		<div id="customer_edit_form" class="modal fade">
			<div class="modal-dialog ">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title">Edit Customer</h5>
					</div>

					{!! Form::open(['route' => 'admin::clients.update_client_user_info', 'class' => 'ui-form client_edit_form']) !!}
						<div class="modal-body">
							{!! Form::hidden('user_id', $user->id) !!}
							<div class="row">
								<div class="form-group col-sm-5">
					                {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
					                {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
					                <span class="help-block" id="error_name" style="display:none;"></span>
					            </div>

					            <div class="form-group col-sm-5">
					                {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
					                {!! Form::email('email',$user->email, ['class' => 'form-control']) !!}
					                <span class="help-block" id="error_email" style="display:none;"></span>
					            </div>

								<div class="form-group col-md-2">
									{!! Form::label('gender', 'Gender:', ['class' => 'control-label']) !!}
									{!! Form::select('gender', ['1' => 'Male', '0' => 'Female'], $user->gender, ['class' => 'form-control' ] )!!}
									<span class="help-block" id="error_gender" style="display:none;"></span>
								</div>
							</div>

							<div class="row">
								<div class="form-group col-sm-12">
				                    <label class="control-label">Other Emails</label>
				                    <input type="text" name="other_emails" class="form-control" value="{{$user->other_emails}}" data-role="tagsinput">
				                </div>
							</div>

							<div class="row">
								<div class="form-group col-sm-12">
				                    <label class="control-label">Other Numbers</label>
				                    <input type="text" name="other_mobiles" class="form-control" value="{{$user->other_mobiles}}" data-role="tagsinput">
				                </div>
							</div>

							<div class="row">
								<div class="form-group col-md-6 ">
					                {!! Form::label('username', 'Username:', ['class' => 'control-label']) !!}
					                {!! Form::text('username',$user->username, ['class' => 'form-control']) !!}
					                <span class="help-block" id="error_username" style="display:none;"></span>
					            </div>

					            <div class="form-group col-sm-6">
			                        {!! Form::label('state', 'State:', ['class' => 'control-label']) !!}
			                        {!! Form::text('state',$user->state, ['class' => 'form-control']) !!}
			                        <span class="help-block" id="error_state" style="display:none;"></span>
			                    </div>
							</div>

							<div class="row">
								<div class="form-group col-sm-6">
			                        {!! Form::label('mobile', 'Primary Number:', ['class' => 'control-label']) !!}
			                        {!! Form::text('mobile',  $user->mobile, ['class' => 'form-control']) !!}
			                        <span class="help-block" id="error_mobile" style="display:none;"></span>
			                    </div>

								<div class="form-group col-sm-6">
			                        {!! Form::label('city', 'City:', ['class' => 'control-label']) !!}
			                        {!! Form::text('city',$user->city, ['class' => 'form-control']) !!}
			                        <span class="help-block" id="error_city" style="display:none;"></span>
			                    </div>
							</div>
								
							<div class="row">
								<div class="form-group col-sm-6">
			                        {!! Form::label('secondary_number', 'Secondary Number:', ['class' => 'control-label']) !!}
			                        {!! Form::text('secondary_number',  $user->official_mobile, ['class' => 'form-control']) !!}
			                        <span class="help-block" id="error_secondary_number" style="display:none;"></span>
			                    </div>

			                    <div class="form-group col-sm-6">
			                        {!! Form::label('zipcode', 'Zipcode:', ['class' => 'control-label']) !!}
			                        {!! Form::text('zipcode', $user->zip_code, ['class' => 'form-control']) !!}
			                        <span class="help-block" id="error_zipcode" style="display:none;"></span>
			                    </div>
							</div>

							<div class="row">
					            <div class="form-group col-sm-12">
			                        {!! Form::label('address1', 'Address Line 1:', ['class' => 'control-label']) !!}
			                        {!! Form::text('address1', $user->address1, ['class' => 'form-control']) !!}
			                        <span class="help-block" id="error_address1" style="display:none;"></span>
			                    </div>
			                    <div class="form-group col-sm-12">
			                        {!! Form::label('address2', 'Address Line 2:', ['class' => 'control-label']) !!}
			                        {!! Form::text('address2', $user->address2, ['class' => 'form-control']) !!}
			                        <span class="help-block" id="error_address2" style="display:none;"></span>
			                    </div>
							</div>

							<div class="row">
								<div class="form-group col-sm-6">
									{!! Form::label('investor_type', 'Investor Type:', ['class' => 'control-label ']) !!}
									{!! Form::select('investor_type', Config::get('rothmans.investor_type'), $user->investor_type, ['class' => 'form-control ' ] )!!}
									<span class="help-block" id="error_investor_type" style="display:none;"></span>
								</div>
								<div class="form-group col-sm-6">
									{!! Form::label('horizon', 'Investor Horizon:', ['class' => 'control-label ']) !!}
									{!! Form::select('horizon', Config::get('rothmans.horizon'), $user->horizon, ['class' => 'form-control ' ] )!!}
									<span class="help-block" id="error_horizon" style="display:none;"></span>
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
							{!! Form::submit('Save', ['class' => 'btn btn-primary', 'id' => 'customer_update_submit']) !!}
					        <div class="form_spinner col-sm-1 text-center" style="display:none;">
					            <i class="icon-spinner2 spinner"></i>
					        </div>
						</div>
						{!! Form::close() !!}
					</form>
				</div>
			</div>
		</div>
		<!-- /horizontal form modal -->
@stop
@push('scripts')
<script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery( '.client_edit_form' ).submit(function( event ) {
        event.preventDefault();
        jQuery('.form_spinner').hide();
        var data = jQuery(this).serializeArray();
        var $url = jQuery(this).attr('action');
        jQuery.ajax({
            type: 'POST',
            url: $url,
            data: data,
            dataType: "json",
            beforeSend: function() {
                // setting a timeout
                jQuery('.form_spinner').show();
            },
            success: function(data) {
                if(data.success == 1){
                    jQuery('#customer_edit_form').modal('toggle');
                    jQuery('span.help-block').html('');
                    swal("Customer Updated!", "Customer Has Been Updated!", "success")
                    window.location.reload();
                }
            },
            error: function(data) { // if error occured
                jQuery('span.help-block').hide();
                var errors = data.responseJSON;
                if(errors != undefined) {
                    jQuery.each(errors, function (index, item) {
                        if (jQuery('#error_' + index).length > 0) {
                            jQuery('#error_' + index).show().html(item['0']);
                        }
                    });
                }else{
                    swal("Error!", "Unable to update, invalid input.", "error")
				}
                jQuery('span.help-block').animate({scrollTop: 0}, "slow");
            },
            complete: function() {
                jQuery('.form_spinner').hide();
            }
        });
    });
</script>
@endpush