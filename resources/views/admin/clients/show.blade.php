@extends('admin.layout.app')

@section('page_title', 'Client')
@section('page_subtitle', 'view')
@section('content')
<div class="panel panel-flat"> 
    <div class="panel-body"> 

        <div class="row">
          @include('admin.partials.clientheader')
        </div>
        <div class="row">
          <div class="col-md-12 currenttask">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#lead">Leads</a></li>
              <li ><a data-toggle="tab" href="#task">Tasks</a></li>
              <li><a data-toggle="tab" href="#docuemnt">Documents</a></li>
              <li><a data-toggle="tab" href="#invoice">Invoices</a></li>
            </ul>
            <div class="tab-content">
              @include('admin.clients.tabs.leadtab')
            </div>
          </div>
        </div>
        <div class="col-md-4 currenttask">
          <div class="boxspace">
            <!--Tasks stats at some point-->
          </div>
        </div>
    </div>
</div>
@stop