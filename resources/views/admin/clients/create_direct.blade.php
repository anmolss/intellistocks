@extends('admin.layout.app')

@section('page_title', 'Direct Client')
@section('page_subtitle', 'Create Direct Client')

@section('content')
    <link href="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css"/>
    <style type="text/css">
        .help-block {
            color: #c10000;
            font-weight: normal !important;
        }
    </style>

    <!-- Advanced legend -->
    <div class="form-horizontal row">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Create Client Form</h5>
            </div>

            <div class="panel-body ">
                    {{-- {!! Form::open(['route' => 'admin::clients.store_client_user', 'class' => 'ui-form client_create_form']) !!}  --}}
                {{!! Form::open(['action'=>'Admin\ClientsController@storeClientNew', 'method'=>'POST', 'class' => 'ui-form client_create_form']) !!}}
                <input type="hidden" name="payment_mode" value="0">

                <fieldset>
                    <legend class="text-semibold">
                        <i class="icon-user-tie position-left"></i>
                        Personal Details
                        <a class="control-arrow" data-toggle="collapse" data-target="#demo1">
                            <i class="icon-circle-down2"></i>
                        </a>
                    </legend>

                    <div class="collapse in" id="demo1">
                        <div class="row form-group">

                            <div class="col-md-4">
                                {!! Form::label('name', 'Name: *', ['class' => 'control-label']) !!}
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                <span class="help-block" id="error_name" style="display:none;"></span>
                            </div>

                            <div class="col-md-4">
                                {!! Form::label('email', 'Primary Email: *', ['class' => 'control-label']) !!}
                                {!! Form::email('email',null, ['class' => 'form-control', 'id' => 'id_primary_email', 'onchange' => 'updateDefaultUsername(); updateDefaultPassword();']) !!}
                                <span class="help-block" id="error_email" style="display:none;"></span>
                            </div>


                            {{--  <div class="col-md-1">  --}}
                                <!-- {!! Form::label('gender', 'Gender:', ['class' => 'control-label']) !!}
                                {!! Form::select('gender', ['1' => 'Male', '0' => 'Female'], 1, ['class' => 'form-control' ] )!!}
                                <span class="help-block" id="error_gender" style="display:none;"></span> -->
                            {{--  </div>  --}}

                            <div class="col-sm-4">
                                {!! Form::label('mobile', 'Primary Mobile: *', ['class' => 'control-label']) !!}
                                {!! Form::text('mobile',  null, ['class' => 'form-control']) !!}
                                <span class="help-block" id="error_mobile" style="display:none;"></span>
                            </div>

                            {{--  <div class="col-sm-2">  --}}
                                <!-- {!! Form::label('secondary_number', 'Secondary Number:', ['class' => 'control-label']) !!}
                                {!! Form::text('secondary_number',  null, ['class' => 'form-control']) !!}
                                <span class="help-block" id="error_secondary_number" style="display:none;"></span> -->
                            {{--  </div>  --}}
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-6">
                                <label class="control-label">Other Emails</label>
                                <input type="text" name="other_emails" class="form-control" value=""
                                       data-role="tagsinput">
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label">Other Numbers</label>
                                <input type="text" name="other_mobiles" class="form-control" value=""
                                       data-role="tagsinput">
                            </div>
                        </div>

                        {{--<div class="row form-group" ng-controller="globalCtrl">
                            <div class="col-sm-6">
                                <label class="control-label">Other Emails</label>
                                <ui-select multiple tagging-label="(Other Emails)"
                                           tagging
                                           ng-model="$parent.other_emails"
                                           sortable="true"
                                           title="Add new email">
                                    <ui-select-match placeholder="Add Emails...">@{{$item}}</ui-select-match>
                                    <ui-select-choices repeat="email in $parent.default_emails track by $index"
                                                       refresh="selectEmailFunc($select.selected)"
                                                       refresh-delay="20">
                                        @{{email}}
                                    </ui-select-choices>
                                </ui-select>

                                {!! Form::hidden("other_emails",null , ['ng-value'=> 'other_emails_comma']) !!}

                                <span class="help-block">
                                    <b ng-repeat="email in other_emails">@{{email}}@{{$last ? '' : ', '}}</b>
                                </span>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label">Other Numbers</label>

                                <ui-select multiple tagging-label="(Other Mobiles)"
                                           tagging
                                           ng-model="$parent.other_mobiles"
                                           sortable="true"
                                           title="Add new mobile">
                                    <ui-select-match placeholder="Add Ips...">@{{$item}}</ui-select-match>
                                    <ui-select-choices repeat="mobile in $parent.default_mobiles track by $index"
                                                       refresh="selectMobileFunc($select.selected)"
                                                       refresh-delay="20">
                                        @{{mobile}}
                                    </ui-select-choices>
                                </ui-select>
                                {!! Form::hidden("other_emails",null , ['ng-value'=> 'other_mobiles_comma']) !!}
                                <span class="help-block">
                                    <b ng-repeat="mobile in other_mobiles">@{{mobile}}@{{$last ? '' : ', '}}</b>
                                    <br>
                                    <b ng-repeat="mobile in default_mobiles">@{{mobile}}@{{$last ? '' : ', '}}</b>
                                </span>
                            </div>
                        </div>--}}

                        <div class="row form-group">
                            <div class="col-md-3">
                                {!! Form::label('address', 'Address: *', ['class' => 'control-label']) !!}
                                {!! Form::text('address', null, ['class' => 'form-control']) !!}
                                <span class="help-block" id="error_address" style="display:none;"></span>
                            </div>

                            <div class="col-md-3">
                                {!! Form::label('zipcode', 'Pin:', ['class' => 'control-label']) !!}
                                {!! Form::text('zipcode', null, ['class' => 'form-control', 'id' => 'id_zipcode', 'onchange' => 'cityState()']) !!}
                                <span class="help-block" id="error_zipcode" style="display:none;"></span>
                            </div>

                            <div class="col-md-3">
                                {!! Form::label('city', 'City: *', ['class' => 'control-label']) !!}
                                {!! Form::text('city',null, ['class' => 'form-control', 'id'=>'id_city']) !!}
                                <span class="help-block" id="error_city" style="display:none;"></span>
                            </div>

                            <div class="col-md-3">
                                <!-- {!! Form::label('address2', 'Address Line 2:', ['class' => 'control-label']) !!}
                                {!! Form::text('address2', null, ['class' => 'form-control']) !!}
                                <span class="help-block" id="error_address2" style="display:none;"></span> -->
                            </div>
    
                            <div class="col-md-3">
                                {!! Form::label('state', 'State: *', ['class' => 'control-label']) !!}
                                {!! Form::text('state',null, ['class' => 'form-control', 'id'=>'id_state']) !!}
                                <span class="help-block" id="error_state" style="display:none;"></span>
                            </div>

                        </div>

                         <div class="row form-group">
                            <div class="col-md-2">
                                {!! Form::label('gender', 'Gender:', ['class' => 'control-label']) !!}
                                {!! Form::select('gender', ['1' => 'Male', '0' => 'Female'], 1, ['class' => 'form-control' ] )!!}
                                <span class="help-block" id="error_gender" style="display:none;"></span>
                            </div>

                            <!-- Fix code / Change backend  -->
                            <div class="col-md-2">
                                {!! Form::label('dob', 'DOB:', ['class' => 'control-label']) !!}
                                {!! Form::text('dob',null, ['class' => 'form-control date-picker', 'data-date-format' => 'dd-mm-yyyy', 'placeholder'=>'dd-mm-yyyy']) !!}
                                <span class="help-block" id="error_state" style="display:none;"></span>
                            </div>
                            <!-- Fix code / Change backend -->
                            <div class="col-md-2">
                                {!! Form::label('doa', 'DOA:', ['class' => 'control-label']) !!}
                                {!! Form::text('doa',null, ['class' => 'form-control date-picker', 'data-date-format' => 'dd-mm-yyyy', 'placeholder'=>'dd-mm-yyyy']) !!}
                                <span class="help-block" id="error_state" style="display:none;"></span>
                            </div>
                            <!-- Fix code / Change backend -->
                            <div class="col-md-3">
                                {!! Form::label('occupation', 'Occupation:', ['class' => 'control-label']) !!}
                                {!! Form::select('occupation', ['Test 1' => 'Test 1', 'Test 2' => 'Test 2'], 1, ['class' => 'form-control' ] )!!}
                                <span class="help-block" id="error_gender" style="display:none;"></span>
                            </div>
                            <!-- Fix code / Change backend -->
                            <div class="col-md-3">
                                {!! Form::label('organisation', 'Organisation:', ['class' => 'control-label']) !!}
                                {!! Form::text('organisation',null, ['class' => 'form-control']) !!}
                                <span class="help-block" id="error_gender" style="display:none;"></span>
                            </div>
                        </div>

                        <!-- <div class="row form-group">
                            <div class="col-md-4">
                                {!! Form::label('fk_relation_manager_id', 'Relashionship Manager:', ['class' => 'control-label']) !!}
                                {!! Form::select('fk_relation_manager_id', $hrusers, null, ['class' => 'form-control' ] )!!}
                                <span class="help-block" id="error_fk_relationship_manager_id"
                                      style="display:none;"></span>
                            </div>

                            <div class="col-md-4">
                                {!! Form::label('fk_suitability_officer_id', 'Suitability Verification Officer:', ['class' => 'control-label']) !!}
                                {!! Form::select('fk_suitability_officer_id', $hrusers, null, ['class' => 'form-control' ] )!!}
                                <span class="help-block" id="error_fk_suitability_officer_id"
                                      style="display:none;"></span>
                            </div>

                            <div class="col-md-4 ">
                                {!! Form::label('source', 'Soruce:', ['class' => 'control-label ']) !!}
                                {!! Form::select('source', Config::get('rothmans.lead_source'), null, ['class' => 'form-control ' ] )!!}
                                <span class="help-block" id="error_source" style="display:none;"></span>
                            </div>

                        </div> -->

                        <!-- {{--<div class="row form-group">
                            <div class="col-md-4 ">
                                {!! Form::label('source', 'Soruce:', ['class' => 'control-label ']) !!}
                                {!! Form::select('source', Config::get('rothmans.lead_source'), null, ['class' => 'form-control ' ] )!!}
                                <span class="help-block" id="error_source" style="display:none;"></span>
                            </div>

                            <div class="col-md-4 ">
                                {!! Form::label('investor_type', 'Invester Type:', ['class' => 'control-label ']) !!}
                                {!! Form::select('investor_type', Config::get('rothmans.investor_type'), null, ['class' => 'form-control ' ] )!!}
                                <span class="help-block" id="error_investor_type" style="display:none;"></span>
                            </div>

                            <div class="col-md-4 ">
                                {!! Form::label('horizon', 'Investment Horizon:', ['class' => 'control-label ']) !!}
                                {!! Form::select('horizon', Config::get('rothmans.horizon'), null, ['class' => 'form-control ' ] )!!}
                                <span class="help-block" id="error_horizon" style="display:none;"></span>
                            </div>

                        </div>--}} -->

                        <div class="row form-group">
                            <div class="col-md-3">
                                {!! Form::label('username', 'Username: *', ['class' => 'control-label']) !!}
                                {!! Form::text('username',null, ['class' => 'form-control', 'id' => 'id_username']) !!}
                                <span class="help-block" id="error_username" style="display:none;"></span>
                            </div>
                            <div class="col-md-3">
                                {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
                                {!! Form::password('password', ['class' => 'form-control', 'id'=>'id_password']) !!}
                                <span class="help-block" id="default_password_msg"></span>
                                <span class="help-block" id="error_password" style="display:none;"></span>
                            </div>
                            <div class="col-md-3">
                                {!! Form::label('password_confirmation', 'Confirm Password:', ['class' => 'control-label']) !!}
                                {!! Form::password('password_confirmation',  ['class' => 'form-control', 'id' => 'id_password_confirmation']) !!}
                                <span class="help-block" id="error_password_confirmation"
                                      style="display:none;"></span>
                            </div>
                            <div class="col-md-2">
                                {!! Form::label('source', 'Soruce:', ['class' => 'control-label ']) !!}
                                {!! Form::select('source', $sources, null, ['class' => 'form-control', 'id'=>'id_sources' ] )!!}
                                <span class="help-block" id="error_source" style="display:none;"></span>
                            </div>
                            <div class="col-md-1">
                                <span class="btn btn-primary" data-toggle="modal" data-target="#myModal">+</span>
                            </div>
                        </div>

                    </div>
                </fieldset>

                <fieldset>
                    <legend class="text-semibold">
                        <i class="icon-magazine position-left"></i>
                        Services Details
                        <a class="control-arrow" data-toggle="collapse" data-target="#demo2">
                            <i class="icon-circle-down2"></i>
                        </a>
                    </legend>

                    <div class="collapse in" id="demo2">
                        <div class="form-group" id="service_append">
                            <div class="col-sm-12">
                                <div class="col-sm-12 well service_row" data-count='0'>
                                    <div class="row">
                                        <div class="col-md-6">
                                            {!! Form::label('fk_relation_manager_id', 'Relashionship Manager:', ['class' => 'control-label']) !!}
                                            <select class="form-control" name="fk_relation_manager_id">
                                                @foreach ($hrusers as $id=>$state)
                                                    <option value="{{ $state }}">{{ $id }}</option>
                                                @endforeach
                                            </select>
                                            {{--  {!! Form::select('fk_relation_manager_id', $hrusers, null, ['class' => 'form-control' ] )!!}  --}}
                                            <span class="help-block" id="error_fk_relationship_manager_id"
                                                style="display:none;"></span>
                                        </div>
                                        <!-- Fix code/ Change backend  -->
                                        <div class="col-md-6" name="compliance_manager">
                                            {!! Form::label('compliance_manager', 'Compliance Manager:', ['class' => 'control-label']) !!}
                                            <select class="form-control">
                                                @foreach ($cmusers as $id=>$state)
                                                    <option value="{{ $state }}">{{ $id }}</option>
                                                @endforeach
                                            </select>
                                            {{--  {!! Form::select('compliance_manager', $cmusers, null, ['class' => 'form-control' ] )!!}  --}}
                                            <span class="help-block" id="error_fk_suitability_officer_id"
                                                style="display:none;"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        {{--  <div class="col-md-4">  --}}
                                            <!-- Fix code/ Change Backend -->
                                            <!-- {!! Form::label('username', 'Service Name:', ['class' => 'control-label']) !!}
                                            {!! Form::text('username',null, ['class' => 'form-control']) !!}
                                            <span class="help-block" id="error_username" style="display:none;"></span> -->
                                        {{--  </div>  --}}
                                        <div class="col-sm-6">
                                            {!! Form::label('services[0][fk_service_id]', 'Service Type: *', ['class' => 'control-label']) !!}
                                            {!! Form::select('services[0][fk_service_id]', [null=>'Select Service'] + $services->toArray(), null, ['class' => 'form-control select2-search-modal service_type', 'id' => 'id_service', 'onchange'=>'getPlan()'] )!!}
                                            <span class="help-block error_services_fk_service_id"
                                                    style="display:none;"></span>
                                        </div>
                                        <div class="col-sm-6">
                                            {!! Form::label('services[0][fk_plan_id]', 'Select Plan: *', ['class' => 'control-label']) !!}
                                            {!! Form::select('services[0][fk_plan_id]', [null=>'Select Plans'], null, ['class' => 'form-control select2-search-modal plans'] )!!}
                                            <span class="help-block error_services_fk_plan_id"
                                                  style="display:none;"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <!-- Fix code/ Change Backend -->
                                            <!-- {!! Form::label('username', 'Start Date:', ['class' => 'control-label']) !!}
                                            {!! Form::text('username',null, ['class' => 'form-control']) !!}
                                            <span class="help-block" id="error_username" style="display:none;"></span> -->
                                        </div>
                                        <div class="col-md-6">
                                            <!-- Fix code/ Change Backend -->
                                            <!-- {!! Form::label('username', 'End Date:', ['class' => 'control-label']) !!}
                                            {!! Form::text('username',null, ['class' => 'form-control']) !!}
                                            <span class="help-block" id="error_username" style="display:none;"></span> -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <!-- Fix code/ Change Backend -->
                                            <!-- {!! Form::label('username', 'Capital:', ['class' => 'control-label']) !!}
                                            {!! Form::text('username',null, ['class' => 'form-control']) !!}
                                            <span class="help-block" id="error_username" style="display:none;"></span> -->
                                        </div>
                                        <div class="col-md-3">
                                            <!-- Fix code/ Change Backend -->
                                            <!-- {!! Form::label('username', 'Fee Structure:', ['class' => 'control-label']) !!}
                                            {!! Form::text('username',null, ['class' => 'form-control']) !!}
                                            <span class="help-block" id="error_username" style="display:none;"></span> -->
                                        </div>
                                        <div class="col-md-3">
                                            <!-- Fix code/ Change Backend -->
                                            <!-- {!! Form::label('username', 'Amount:', ['class' => 'control-label']) !!}
                                            {!! Form::text('username',null, ['class' => 'form-control']) !!}
                                            <span class="help-block" id="error_username" style="display:none;"></span> -->
                                        </div>
                                        <div class="col-md-3">
                                            <!-- Fix code/ Change Backend -->
                                            <!-- {!! Form::label('username', 'Discount:', ['class' => 'control-label']) !!}
                                            {!! Form::text('username',null, ['class' => 'form-control']) !!}
                                            <span class="help-block" id="error_username" style="display:none;"></span> -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <!-- Fix code/ Change Backend -->
                                            <!-- {!! Form::label('username', 'Plan Details:', ['class' => 'control-label']) !!}
                                            {!! Form::text('username',null, ['class' => 'form-control']) !!}
                                            <span class="help-block" id="error_username" style="display:none;"></span> -->
                                        </div>
                                        <div class="col-md-6">
                                            <!-- Fix code/ Change Backend -->
                                            <!-- {!! Form::label('username', 'Payment Type:', ['class' => 'control-label']) !!}
                                            {!! Form::text('username',null, ['class' => 'form-control']) !!}
                                            <span class="help-block" id="error_username" style="display:none;"></span> -->
                                        </div>
                                        <div class="col-md-6">
                                            <!-- Fix code/ Change Backend -->
                                            <!-- {!! Form::label('username', 'Payment Mode:', ['class' => 'control-label']) !!}
                                            {!! Form::text('username',null, ['class' => 'form-control']) !!}
                                            <span class="help-block" id="error_username" style="display:none;"></span> -->
                                        </div>
                                    </div>
                                    <br>
                                    {{--  <div class="heading" style="margin-left: 5px;">Enter Capital</div>  --}}
                                    <div class="row">
                                        <div class="col-sm-2">
                                            {!! Form::label('currency', 'Enter Currency: *', ['class' => 'control-label']) !!}
                                            {!! Form::select('currency', [null=>'Select Currency', 'INR'=>'INR', 'USD'=>'USD'], null, ['class' => 'form-control select2-search-modal', 'id' => 'id_currency'] )!!}
                                            <span class="help-block error_currency"
                                                    style="display:none;"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            {!! Form::label('amount', 'Enter Amount: *', ['class' => 'control-label']) !!}
                                            {!! Form::text('amount', null, ['class' => 'form-control select2-search-modal reflect_capital_amnt', 'onchange' => 'reflectAmount()', 'id' => 'id_amount'] )!!}
                                            <span class="help-block error_amount"
                                                    style="display:none;"></span>
                                        </div>
                                        <div class="col-sm-3">
                                            {!! Form::label('denomination', 'Enter Denomination: *', ['class' => 'control-label']) !!}
                                            {{--  {!! Form::text('denomination', null, ['class' => 'form-control select2-search-modal reflect_capital_amnt', 'onchange' => 'reflectAmount()', 'id' => 'id_denomination'] )!!}  --}}
                                            {!! Form::select('denomination', [null=>'Select Denomination', 'K'=>'K', 'Cr'=>'Cr'], null, ['class' => 'form-control select2-search-modal', 'id' => 'id_denomination', 'onchange' => 'reflectAmount()'] )!!}
                                            <span class="help-block error_denomination"
                                                    style="display:none;"></span>
                                        </div>
                                        <div class="col-sm-3">
                                            {!! Form::label('discount', 'Enter Discount (%): *', ['class' => 'control-label']) !!}
                                            {!! Form::number('discount', null, ['class' => 'form-control select2-search-modal', 'onchange'=>'reflectDiscount()', 'id'=>'id_discount', 'max' =>'8', 'min'=>'0' ] )!!}
                                            <span class="help-block error_discount"
                                                    style="display:none;"></span>
                                        </div>
                                        {{--  <div class="col-md-6">
                                            <div class="form-group" id="capital_container" style="display:none;">
                                                <label class="control-label">Enter Capital:</label>
                                                <input type="text" name="services[0][capital]"
                                                        class="form-control reflect_capital_amnt">
                                                <span class="help-block error_services_capital"
                                                        style="display:none;"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group" id="discount-container" style="display:none;">
                                                <label for="" class="control-label">Discount: </label>
                                                <input class="form-control reflect_amt_key_up txt-discount"
                                                       placeholder="Discount (%)" name="services[0][discount]"
                                                       type="number" value="0" min="0" max="0">
                                                <span class="help-block error_discount" style="display:none;"></span>
                                            </div>
                                        </div>  --}}
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            {!! Form::label('payment_type[]', 'Payment Type:', ['class' => 'control-label']) !!}
                                            {!! Form::select('services[0][payment_type]', ['' => 'Select Payment Type', 'full' => 'Full', 'partial' => 'Partial'], null, ['class' => 'form-control select2-search-modal payment_type'] )!!}
                                            <span class="help-block error_services_payment_type"
                                                  style="display:none;"></span>
                                        </div>

                                        <div class="col-sm-4">
                                            <label class="control-label">Payment Mode:</label>
                                            <select class="form-control select2-search-modal fk_payment_type"
                                                    name="payment_Mode">
                                                <option value="">Select Payment Mode</option>
                                                <option value="payment_gateway">Payment Gateway</option>
                                                <option value="bank">Bank</option>
                                                <option value="cash">Cash</option>
                                            </select>
                                            <span class="help-block error_payment_mode"
                                                  style="display:none;"></span>
                                        </div>

                                        <div class="col-sm-4">
                                            <label class="control-label">Activation Date:</label>
                                            <input type="text" name="services[0][activation_date]"
                                                   class="form-control date-picker service-activation-date"
                                                   data-date-format="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>">
                                            <span class="help-block error_services_activation_date"
                                                  style="display:none;"></span>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="payment_append form-group">
                                        <div class="col-sm-12">
                                            <div class="row payment_type_append"></div>
                                        </div>
                                    </div>
                                    <div class="plan_append">
                                        <div class="col-sm-12" style="margin-top:10px;">
                                            <div class="table-responsive">
                                                <table class="table table-xxs table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="10" class="text-center bg-primary">    Fee Details
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td class="text-center">Please Select Service & Plan</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="plan_spinner col-sm-1 text-center" style="display:none;">
                                        <i class="icon-spinner2 spinner"></i>
                                    </div>

                                    <div class="row form-group" style="margin-top:10px;">
                                        <div class="col-md-5">
                                            <label class="control-label">Special Note:</label>
                                            <textarea name="services[0][comments]" rows="4"
                                                      class="form-control"></textarea>
                                            <span class="help-block error_services_comments"
                                                  style="display:none;"></span>
                                        </div>

                                        @ability('super-admin', 'create-customer')
                                            <div class="col-md-2">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="services[0][force_verify]" value="yes"
                                                           class="styled" id="force_verify_check">
                                                    <span class="text-bold text-danger">Verify Forcefully</span>
                                                </label>
                                            </div>

                                            <div class="col-sm-5" id="force_verify_reason" style="display:none;">
                                            <div class="form-group">
                                                <select name="services[0][force_verify_reason]" class="form-control">
                                                    <option value="">Select Reason</option>
                                                    <option value="one">One</option>
                                                    <option value="two">Two</option>
                                                    <option value="three">Three</option>
                                                </select>
                                                <span class="help-block error_services_force_verify_reason"
                                                      style="display:none;"></span>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Comment:</label>
                                                <textarea name="services[0][force_verify_comment]" rows="1"
                                                          class="form-control"></textarea>
                                                <span class="help-block error_services_force_verify_comment"
                                                      style="display:none;"></span>
                                            </div>
                                            </div>
                                        @endability

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <div class="text-right">
                    {{--  {!! Form::submit('Create New Client', ['class' => 'btn btn-primary']) !!}  --}}
                    <span class="btn btn-primary" data-toggle="modal" data-target="#myModal1" onclick="amountModal()">Create New Client</span>
                    <div class="form_spinner col-sm-1 text-center" style="display:none;">
                        <i class="icon-spinner2 spinner"></i>
                    </div>
                </div>
                <div id="myModal1" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Confirm Amount</h4>
                            </div>
                            <div class="modal-body">
                               <h3>Amount Entered is: <span id="cnf_currency"> <span id="cnf_amount"> <span id="cnf_denomination"></h3>
                            </div>
                            <div class="modal-footer">
                                {{--  {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}  --}}
                                {!! Form::submit('Confirm', ['class' => 'btn btn-primary', 'onclick'=>'closeConfirm()']) !!}
                                <button type="button" class="btn btn-default" data-dismiss="modal" id="close_cnf">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- /a legend -->

    
    {{--  {!! Form::open(['action' => 'Admin\SourcesController@index', 'method' => 'POST']) !!}  --}}
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Source</h4>
                </div>
                <div class="modal-body">
                    <!-- {!! Form::label('new_source', 'Soruce:', ['class' => 'control-label ']) !!} -->
                    {!! Form::text('new_source', null, ['class' => 'form-control', 'id'=>'id_new_source' ] )!!}
                    <span class="help-block" id="error_new_source" style="display:none;"></span>
                </div>
                <div class="modal-footer">
                    {{--  {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}  --}}
                    <button type="button" onclick="addSource()" class="btn btn-default" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--  {!! Form::close() !!}  --}}

    @include('admin.partials.jquery_service_plan_templates')
@stop

@push('scripts')
<script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDl-YkYZQhwjH3xfDX225trPvRgD6U3ieQ"></script>
<script type="text/javascript">
    var $total_services;
    var $service_plans;
    var $is_edit = false;
    var $getPlanUrl = '{{url('getPlan')}}';
    var $getCustomerUrl = "{{route('admin::clients.customers')}}";
    var $change_warning_count = 0;

    jQuery(document).ready(function () {
        $total_services = 0;
        $service_plans = <?php echo json_encode($service_plans); ?>;
        jQuery('#name').on('keyup', function (e) {
            var username = jQuery(this).val().replace(/ /g, "_").toLowerCase();
            jQuery('#username').val(username);
        });
    });

    function updateDefaultUsername() {
        var email = document.getElementById('id_primary_email');
        var username = document.getElementById('id_username');
        username.value = email.value;
    }

    function closeConfirm() {
        var close =  document.getElementById('close_cnf');
        close.click();
        reflectAmount();
        reflectDiscount();
    }

    function getPlan() {
        console.log('iske andr');
        var service = document.getElementById('id_service');
        var s_value = service.value;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                console.log('iske andr');
                console.log(this.responseText);
            }
        };
        xmlhttp.open("GET", "/api/get_plan/"+s_value, true);
        xmlhttp.send();
    }

    function addSource() {
        var source = document.getElementById('id_new_source');
        var s_value = source.value;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                console.log('success');
                var sor = document.getElementById('id_sources');
                var option = document.createElement("option");
                option.text = s_value;
                sor.add(option);
            }
        };
        xmlhttp.open("GET", "/api/add_source_new/"+s_value, true);
        xmlhttp.send();
    }


    function updateDefaultPassword() {
        var date = new Date();
        var hour = date.getMinutes();
        var email = document.getElementById('id_primary_email');
        var password = document.getElementById('id_password');
        var password_confirmation = document.getElementById('id_password_confirmation');
        var default_password = email.value.split('@')[0]+(hour+1000);
        password.value = default_password;
        password_confirmation.value = default_password;
        var message = document.getElementById('default_password_msg');
        message.innerHTML = "Default Password is set to '"+default_password+"'.";
    }

    function reflectAmount() {
            var amount = document.getElementById("id_amount");
        var service_amount = document.getElementById('service_fee');
        var amount_d = document.getElementById("id_denomination").value;
        if(service_amount!=null) {
            var service_amount_net = document.getElementById('net_service_amt');
            if(amount_d=="K") {
                service_amount.innerHTML = parseInt(amount.value)*1000;
                service_amount_net.innerHTML = (parseFloat(amount.value)*1000).toFixed(2);
            } else if(amount_d=="Cr") {
                service_amount.innerHTML = parseInt(amount.value)*10000000;
                service_amount_net.innerHTML = (parseFloat(amount.value)*10000000).toFixed(2);
            } else {
                service_amount.innerHTML = amount.value;
                service_amount_net.innerHTML = parseFloat(amount.value).toFixed(2);
            }
        }
        reflectNetPayable();
    }

    function amountModal() {
        var curr = document.getElementById('id_currency');
        var am = document.getElementById('cnf_amount');
        am.innerHTML = '0';
        var service_amount = document.getElementById('net_service_amt');
            am.innerHTML = curr.value + " " + service_amount.innerHTML;
        


        // var amound_d = document.getElementById('id_denomination').value;
        // var cr = document.getElementById('cnf_currency');
        // var dn = document.getElementById('cnf_denomination');
        // var amount = document.getElementById("id_amount").value;
        
        // if(amound_d=="K") {
        //     amount = parseInt(amount)*1000;
        // } else if(amount_d=="Cr"){
        //     amount = parseInt(amount)*10000000;            
        // } else {
        //     amount = parseInt(amount);
        // }
        
        // var denomination = document.getElementById("id_denomination").value;
        if(amount!='') {
            // cr.innerHTML = currency;
            am.innerHTML = service_amount.innerHTML;
            // dn.innerHTML = denomination;
        }
    }
    

    var reflectDiscount = function() {
        var discount_new = document.getElementById("id_discount");
        var amount_d = document.getElementById("id_denomination").value;
        var discount = document.getElementById('max_discount');
        if(discount_new!=null) {
            var amount = document.getElementById("id_amount");
            discount.innerHTML = discount_new.value+"%";
            if(amount_d=="K") {
                var discount_amount = ((parseInt(amount.value)*1000)*parseInt(discount_new.value))/100;
                var discount_amt = document.getElementById('discount_amt');
                discount_amt.innerHTML = parseFloat(discount_amount).toFixed(2);
                reflectNetPayable();
            } else if(amount_d=="Cr") {
                var discount_amount = ((parseInt(amount.value)*10000000)*parseInt(discount_new.value))/100;
                var discount_amt = document.getElementById('discount_amt');
                discount_amt.innerHTML = parseFloat(discount_amount).toFixed(2);
                reflectNetPayable();
            } else {
                var discount_amount = (parseInt(amount.value)*parseInt(discount_new.value))/100;
                var discount_amt = document.getElementById('discount_amt');
                discount_amt.innerHTML = parseFloat(discount_amount).toFixed(2);
                reflectNetPayable();
            }
        }
    }

    function reflectNetPayable() {
        var amount_d = document.getElementById("id_denomination").value;
        if(amount_d=="K") {
            var amount = parseInt(document.getElementById("id_amount").value)*1000;
        } else if(amount_d=="Cr") {
            var amount = parseInt(document.getElementById("id_amount").value)*10000000;
        } else {
            var amount = parseInt(document.getElementById("id_amount").value);
        }

        var discount_new = parseInt(document.getElementById("id_discount").value);
        if(isNaN(discount_new)) {
            discount_new = 0;
        }
        var net_payable = document.getElementById('net_payable_amt');
        var applied_tax_value = document.getElementById('applied_tax_value').innerHTML;
        applied_tax_value = applied_tax_value.split('(');
        applied_tax_value = applied_tax_value[1];
        applied_tax_value = applied_tax_value.split("%");
        applied_tax_value = parseInt(applied_tax_value[0]);
        var applied_tax = (applied_tax_value*amount)/100;
        var applied_tax_html = document.getElementById('applied_tax');
        
        // if(amount!=undefined && discount_new!=undefined) {
            var discount_amount = (parseInt(amount)*parseInt(discount_new))/100;
            var net_payable_value = amount-discount_amount+applied_tax;
            net_payable.innerHTML = parseInt(net_payable_value).toFixed(2);
            applied_tax_html.innerHTML = parseInt(applied_tax).toFixed(2);
        // }
    }

    // function usdIntoInr() {
    //     var usd = document.getElementById('id_currency');
    //     var inr = (usd.value)/65;
    //     console.log(inr);
    // }

    function cityState() {
        var zip = document.getElementById('id_zipcode').value;
        console.log(zip);
        var lat;
        var lng;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                geocoder.geocode({'latLng': results[0].geometry.location}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        var loc = getCityState(results);
                    }
                }
            });
            }
        });

        function getCityState(results)
        {
            var a = results[0].address_components;
            var city, state;
            for(i = 0; i <  a.length; ++i)
            {
                var t = a[i].types;
                if(compIsType(t, 'administrative_area_level_1'))
                    state = a[i].long_name; //store the state
                else if(compIsType(t, 'locality'))
                    city = a[i].long_name; //store the city
            }
            var stateHtml = document.getElementById('id_state');
            var cityHtml = document.getElementById('id_city');
            stateHtml.value = state;
            cityHtml.value = city;
            return (city + ', ' + state)
        }

        function compIsType(t, s) {
            for(z = 0; z < t.length; ++z)
                if(t[z] == s)
                    return true;
            return false;
        }
    }



    {{--
    app.controller('globalCtrl', function($scope, $http) {
        $scope.default_emails = ['raveena.gandhi@intellistocks.com','manasvi.bansal@intellistocks.com','pankajj@intellistocks.com'];
        $scope.default_mobiles = ['9654727358','9999404366','8826019880'];
        $scope.other_emails = [];
        $scope.other_mobiles = [];

        $scope.selectEmailFunc = function($select) {
            var temp = [];
            _.each($select, function(data){
                var splited = data.split(",");
                if(splited.length > 1)
                    temp.concat(splited);
                else
                    temp = data;
            });
            $select = temp;

            _.remove($select, function (email) {
                var emailformat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return !(email !== '' && email !== undefined && email.match(emailformat));
            });
        };

        $scope.selectMobileFunc = function($select) {
            _.each($select, function(data, $index){
                var splited = data.split(",");
                if(splited.length > 1){
                    $select.splice( $index, 1 );
                    $select.concat(splited);
                }
            });

            _.uniq($select);

            _.remove(_.uniq($select), function (mobile) {
                var mobileformat = /^[789]\d{9}$/;
                return !(mobile !== '' && mobile !== undefined && mobile.match(mobileformat));
            });
        };
        $scope.$watch("other_emails", function (newValue) {
            $scope.selectEmailFunc($scope.other_emails);
            $scope.other_emails_comma = newValue.join(", ");
        });
        $scope.$watch("other_mobiles", function (newValue) {
            $scope.selectMobileFunc($scope.other_mobiles);
            $scope.other_mobiles_comma = newValue.join(", ");
        });

    });
    --}}

</script>
@endpush