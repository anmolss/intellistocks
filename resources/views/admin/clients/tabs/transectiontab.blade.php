<div id="transactions" class="tab-pane fade in">
  <div class="boxspace">
    @if(empty($user_services->all()))
      
    @endif
    <div class="col-sm-12">
      <div class="panel-group panel-group-control panel-group-control-right" id="accordion-styled">
        <div class="panel">
          <div class="panel-heading bg-teal-300">
              <h6 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group1">Unpaid Transactions</a>
              </h6>
          </div>
          <div id="accordion-styled-group1" class="panel-collapse collapse in">
            <div class="panel-body">
              @forelse($user_services as $no1 => $user_service)
                @forelse($user_service->transections as $no2 => $transaction)
                  @if($transaction != '' && $transaction->cleared == 'No')
                    <div class="col-md-6">
                      <div class="panel invoice-grid bg-@if($transaction->payment_type == 'cash')success @elseif($transaction->payment_type == 'cheque')primary @elseif($transaction->payment_type == 'net_banking')warning @endif">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-6">
                              <h6 class="text-semibold no-margin-top">{{'Service : '.json_decode($user_service->service_details, true)['name']}}</h6>
                              <ul class="list list-unstyled">
                                <li>Invoice #: &nbsp;{{$transaction->id}}</li>
                                <li>Issued on: <span class="text-semibold">{{(new Carbon($transaction->date))->format('d/m/Y')}}</span></li>
                                @if($transaction->payment_type == 'cheque')
                                  <li>Cheque No: <span class="text-semibold">{{$transaction->cheque_number}}</span></li>
                                @elseif($transaction->payment_type == 'net_banking')
                                  <li>Transaction Id: <span class="text-semibold">{{$transaction->transaction_id}}</span></li>
                                @elseif($transaction->payment_type == 'cash')
                                  <li>Transaction Id: <span class="text-semibold">{{$transaction->transaction_id}}</span></li>
                                @endif
                              </ul>
                            </div>

                            <div class="col-sm-6">
                              <h6 class="text-semibold text-right no-margin-top">{{$transaction->amount}} Rs.</h6>
                              <ul class="list list-unstyled text-right">
                                <li>Method: <span class="text-semibold"><mark>{{strtoupper(str_replace("_"," ",$transaction->payment_type))}}</mark></span></li>
                                @if($transaction->payment_type == 'cheque')
                                  <li>Bank: <span class="text-semibold">{{$transaction->bank}}</span></li>
                                @elseif($transaction->payment_type == 'net_banking')
                                  <li>type: <span class="text-semibold">{{$transaction->type}}</span></li>
                                  <li>Received From: <span class="text-semibold">{{$transaction->received_from}}</span></li>
                                @endif
                              </ul>
                            </div>
                          </div>
                        </div>

                        <div class="panel-footer panel-footer-condensed">
                          <div class="heading-elements">
                            <span class="heading-text text-default">
                              <span class="status-mark border-danger position-left"></span> Created On : <span class="text-semibold">{{$transaction->created_at}}</span>
                            </span>

                            <ul class="list-inline list-inline-condensed heading-text pull-right">
                              <li class="dropdown">
                                <a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                  <li><a href="javascript:" data-id="{{$transaction->id}}" data-type="full" data-pay-type="{{$transaction->payment_type}}" class="edit_transaction"><i class="icon-file-plus"></i> Edit transaction</a></li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  @endif
                @empty
                  <div class="alert bg-warning alert-rounded">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold">No Data!</span> No Unpaid Transactions Available.
                  </div>
                @endforelse
              @empty
                <div class="alert bg-warning alert-rounded">
                  <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                  <span class="text-semibold">No Data!</span> No Services Assigned.
                </div>
              @endforelse
            </div>
          </div>
        </div>
        <div class="panel">
          <div class="panel-heading bg-teal-300">
              <h6 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group2">Paid Transactions</a>
              </h6>
          </div>
          <div id="accordion-styled-group2" class="panel-collapse collapse">
            <div class="panel-body">
              @forelse($user_services as $no1 => $user_service)
                @forelse($user_service->transections as $no2 => $transaction)
                  @if($transaction != '' && $transaction->cleared == 'Yes')
                    <div class="col-md-6">
                      <div class="panel invoice-grid bg-@if($transaction->payment_type == 'cash')success @elseif($transaction->payment_type == 'cheque')primary @elseif($transaction->payment_type == 'net_banking')warning @endif">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-6">
                              <h6 class="text-semibold no-margin-top">{{'Service : '.json_decode($user_service->service_details, true)['name']}}</h6>
                              <ul class="list list-unstyled">
                                <li>Invoice #: &nbsp;{{$transaction->id}}</li>
                                <li>Paid on: <span class="text-semibold">{{(new Carbon($transaction->date))->format('d/m/Y')}}</span></li>
                                @if($transaction->payment_type == 'cheque')
                                  <li>Cheque No: <span class="text-semibold">{{$transaction->cheque_number}}</span></li>
                                @elseif($transaction->payment_type == 'net_banking')
                                  <li>Transaction Id: <span class="text-semibold">{{$transaction->transaction_id}}</span></li>
                                @elseif($transaction->payment_type == 'cash')
                                  <li>Transaction Id: <span class="text-semibold">{{$transaction->transaction_id}}</span></li>
                                @endif
                              </ul>
                            </div>

                            <div class="col-sm-6">
                              <h6 class="text-semibold text-right no-margin-top">{{$transaction->amount}} Rs.</h6>
                              <ul class="list list-unstyled text-right">
                                <li>Method: <span class="text-semibold"><mark>Partial - {{strtoupper(str_replace("_"," ",$transaction->payment_type))}}</mark></span></li>
                                @if($transaction->payment_type == 'cheque')
                                  <li>Bank: <span class="text-semibold">{{$transaction->bank}}</span></li>
                                @elseif($transaction->payment_type == 'net_banking')
                                  <li>type: <span class="text-semibold">{{$transaction->type}}</span></li>
                                  <li>Received From: <span class="text-semibold">{{$transaction->received_from}}</span></li>
                                @elseif($transaction->payment_type == 'cash')
                                  <li>Received From: <span class="text-semibold">{{$transaction->received_from}}</span></li>
                                @endif
                              </ul>
                            </div>
                          </div>
                        </div>

                        <div class="panel-footer panel-footer-condensed">
                          <div class="heading-elements">
                            <span class="heading-text text-default">
                              <span class="status-mark border-danger position-left"></span> Created On : <span class="text-semibold">{{$transaction->created_at}}</span>
                            </span>

                            <ul class="list-inline list-inline-condensed heading-text pull-right">
                              <li class="dropdown">
                                <a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                  <li><a href="javascript:" data-id="{{$transaction->id}}" data-type="partial" data-pay-type="{{$transaction->payment_type}}" class="edit_transaction"><i class="icon-file-plus"></i> Edit transaction</a></li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  @endif
                @empty
                  <div class="alert bg-warning alert-rounded">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold">No Data!</span> No Paid Transactions Available.
                  </div>
                @endforelse
              @empty
                <div class="alert bg-warning alert-rounded">
                  <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                  <span class="text-semibold">No Data!</span> No Services Assigned.
                </div>
              @endforelse
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="transaction_edit_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="transaction_modal_form_spinner" style="display:none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Please Wait...</h5>
            </div>
            <div class="modal-body text-center">
              <div class="col-sm-1 text-center">
                  <i class="icon-spinner2 spinner"></i>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
          </div>
            <div id="modal_content_append"></div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
  jQuery(document).ready(function() {
    jQuery('.edit_transaction').on('click', function() {
      var $tran_id = jQuery(this).attr('data-id');
      var $tran_type = jQuery(this).attr('data-type');
      var $tran_pay_type = jQuery(this).attr('data-pay-type');
      getTransactionForm($tran_type, $tran_pay_type, $tran_id);
    });

    jQuery(document).delegate('.save_transaction', 'click', function(e) {
      e.preventDefault();
      var $url = jQuery('.transaction_edit_form').attr('action');
      var $data = jQuery('.transaction_edit_form').serializeArray();
      jQuery.ajax({
          type: 'POST',
          url: $url,
          data: $data,
          dataType: "json",
          beforeSend: function() {
            jQuery(".transaction_edit_form_spinner").show();
          },
          success: function(data) {
            if(data.success = 1){
              location.reload();
            }
          },
          complete: function() {
            jQuery(".transaction_edit_form_spinner").hide();
          }
      });
    });
  });

  function getTransactionForm(tran_type, tran_pay_type, tran_id) {
    jQuery.ajax({
        type: 'GET',
        url: '{{route('admin::transactions.getForm')}}' + '?tran_type=' + tran_type +'&tran_pay_type=' + tran_pay_type + '&tran_id='+tran_id,
        dataType: "json",
        beforeSend: function() {
          jQuery('#transaction_edit_modal').modal('toggle');
          jQuery(".transaction_modal_form_spinner").show();
        },
        success: function(data) {
          if(data.success = 1){
            jQuery('#transaction_edit_modal #modal_content_append').html(data.form);
          }
        },
        complete: function() {
          jQuery(".transaction_modal_form_spinner").hide();
        }
    }); 
  }
</script>
@endpush