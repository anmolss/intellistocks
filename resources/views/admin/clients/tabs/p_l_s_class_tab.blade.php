<div id="p_l_s_class" class="tab-pane fade in">
  <div class="boxspace">
      <div class="panel panel-flat small" style="width: 104%;">
          <table class="table table-hover table-xxs" id="customers-table">
              <thead>
              <tr class="bg-blue">
                  <th>Stock Name</th>
                  <th>Status</th>
                  <th>Entry date</th>
                  <th>Entry Price</th>
                  <th>CMP / EXIT PRICE</th>
                  <th>Close / Current Date</th>
                  <th>Open Profit</th>
                  <th>Booked Profit</th>
                  <th>Total Profit</th>
                  <th>Overall Profit (%)</th>
                  <th><span data-toggle="tooltip" title="Open Position / Stock Weight of Total Investment">Open Stock Weight (%)</span></th>
                  <th><span data-toggle="tooltip" title="Closed Position / Stock Weight of Total Investment">Close Stock Weight (%)</span></th>
                  <th><span data-toggle="tooltip" title="Stock Weight of Total Investment">Close Stock Weight (%)</span></th>
                  <th>Net ROI</th>
                  {{--<th>Tgt <br> Sl</th>
                  <th>Away % from Tgt and SL</th>--}}
              </tr>
              </thead>
              <tbody>
              @foreach($pnl_stocks as $key => $stock)
                  <tr>
                      <th class="clickable cursor-pointer text-blue-700" data-toggle="collapse" data-parent="#customers-table" data-target=".{{str_slug($key)}}">{{ $stock['symbol'] }}+</th>
                      <td>{{ $stock['status'] }}</td>
                      <td>{{ $stock['entry_date'] }}</td>
                      <td>{{ $stock['avg_price'] }}</td>
                      <td>{{ $stock['exit_price'] }}</td>
                      <td>{{ $stock['exit_date'] }}</td>
                      <td>{{ $stock['open_profit'] }}</td>
                      <td>{{ $stock['booked_profit'] }}</td>
                      <td>{{ $stock['total_profit'] }}</td>
                      <td>{{ $stock['profit_percent'] }}%</td>
                      <td>{{ $stock['open_stock_weight'] }}%</td>
                      <td>{{ $stock['close_stock_weight'] }}%</td>
                      <td>{{ $stock['total_stock_weight'] }}%</td>
                      <td>{{ $stock['net_roi'] }}%</td>

                      {{--<th>{{ $stock['target_mean'] }} <br> {{ $stock['sl_mean'] }}</th>
                      <th>{{ $stock['target_p_away'] }}% <br> {{ $stock['sl_p_away'] }}%</th>--}}
                  </tr>
                @if($stock['exchange'] != 'nfo')
                  <tr class="collapse {{str_slug($key)}}">
                      <td colspan="17" class="no-padding">
                          <table class="table bg-teal table-xxs">
                              <caption class="text-center"><h3>Transaction History</h3></caption>
                              <thead>
                              <tr class="bg-teal-700">
                                  <th>Transactions Date</th>
                                  <th>Stock Name</th>
                                  <th>Price</th>
                                  <th>Qty</th>
                                  <th>Call Type</th>
                                  <th>Status</th>

                                  <th>Open Profit</th>
                                  <th>Booked Profit</th>
                                  <th>Total</th>
                              </tr>
                              </thead>
                              <tbody>
                              @foreach($stock['deliveries'] as $key => $stock)
                                  <tr class="bg-teal">
                                      <td>{{ \Carbon\Carbon::parse($stock['entry_date'])->format(config('rothmans.date_format')) }}</td>
                                      <td>{{ $stock['symbol'] }}</td>
                                      <td>{{ $stock['price'] }}</td>
                                      <td>{{ $stock['qty'] }}%<br> {{ $stock['curr_qty'] }}</td>
                                      <td>{{ config('rothmans.front_sd_call_type.'.$stock['service_call_type']) }}</td>
                                      <td>{{ $stock['status'] }}</td>
                                      <td>{{ $stock['open_profit'] }}</td>
                                      <td>{{ $stock['booked_profit'] }}</td>
                                      <td>{{ $stock['total_profit'] }}</td>
                                  </tr>
                              @endforeach
                              </tbody>
                          </table>
                      </td>
                  </tr>
                @else
                    <tr class="collapse {{str_slug($key)}}">
                        <td colspan="17" class="no-padding">
                            <table class="table bg-teal table-xxs">
                                <caption class="text-center"><h3>Transaction History</h3></caption>
                                <thead>
                                <tr class="bg-teal-700">
                                    <th>Transactions Date</th>
                                    <th>Stock Name</th>
                                    <th>Price</th>
                                    <th>Lot</th>
                                    <th>Call Type</th>
                                    <th>Status</th>

                                    <th>Open Profit</th>
                                    <th>Booked Profit</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($stock['deliveries'] as $key => $stock)
                                    <tr class="bg-teal">
                                        <td>{{ \Carbon\Carbon::parse($stock['entry_date'])->format(config('rothmans.date_format')) }}</td>
                                        <td>{{ $stock['symbol'] }}</td>
                                        <td>{{ $stock['price'] }}</td>
                                        <td>{{ $stock['nfo_qty'] }}</td>
                                        <td>{{ config('rothmans.front_sd_call_type.'.$stock['service_call_type']) }}</td>
                                        <td>{{ $stock['status'] }}</td>
                                        <td>{{ $stock['open_profit'] }}</td>
                                        <td>{{ $stock['booked_profit'] }}</td>
                                        <td>{{ $stock['total_profit'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>
                    </tr>
                  @endif
              @endforeach
              </tbody>
          </table>

      </div>
  </div>
</div>


