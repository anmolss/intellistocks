<div id="p_l_all_class" class="tab-pane fade in">
  <div class="boxspace">
      <div class="panel panel-flat table-responsive small">
          <table class="table table-hover" id="customers-table">
          <tbody>

          <tr>
              <th>Service Count</th>
              <td>{{ $pnl_all['service_count']}}</td>
          </tr>

          <tr>
              <th>Total Capital</th>
              <td>{{ $pnl_all['total_capital'] }}</td>
          </tr>

          <tr>
              <th>Total calls</th>
              <td>{{ $pnl_all['total_calls']}}</td>
          </tr>
          <tr>
              <th>Positive Calls</th>
              <td>{{ $pnl_all['positive_call'] }}</td>
          </tr>

          <tr>
              <th>Total P&L</th>
              <td>{{ $pnl_all['total_profit'] }}</td>
          </tr>
          <tr>
              <th>Today's Pnl</th>
              <td>{{ $pnl_all['today_pnl'] }}</td>
          </tr>

          </tbody>
          </table>
      </div>

  </div>
</div>

@push('scripts')
<script type="text/javascript">

    $('.daterange-delivery_range').daterangepicker({
        applyClass: 'bg-info',
        cancelClass: 'btn-default',
        locale: {
            format: 'DD/MM/YYYY'
        }
    });
    // Enable Select2 select for the length option
    $('select').select2({
        minimumResultsForSearch: Infinity
    });

    $('.pnl_service_model').on('click', function(e){
        e.preventDefault();
        $('#PnLServiceModel').modal('show').find('.modal-title').text($(this).text())
        $('#PnLServiceModel').modal('show').find('.modal-body').html('<p>Loading...</p>').load($(this).attr('href'));
    }).on('show.bs.modal', function () {
        $(this).find('.modal-body').css({
            width:'auto', //probably not needed
            height:'auto', //probably not needed
            'max-height':'100%'
        });
    });
</script>

@endpush
