<div id="sms" class="tab-pane fade in {{(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'sms')?'active':''}}">
  	<div class="boxspace">
		{!! Form::open(['route' => 'admin::clients.send_sms', 'class' => 'ui-form send_sms_form']) !!}
			<h1>Send SMS</h1>
			<div class="clearfix"></div>
			<br>
	  		<div class="form-group">
				{!! Form::label('mobile', 'Mob No:', ['class' => 'col-lg-1 control-label']) !!}
				<div class="col-lg-11">
					<div class="form-control-static">{{$user->mobile}}</div>
					{!! Form::hidden('number', $user->mobile, ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="clearfix"></div>
			<br>
			<div class="form-group">
				<label class="col-lg-1 control-label">Message:</label>
				<div class="col-lg-11">
					{{ Form::textarea('message', null, ['size' => '3x3', 'class' => 'form-control maxlength-textarea sms_message', 'placeholder' => 'Enter Message Here', 'maxlength' => '140']) }}
				</div>
			</div>
			<div class="clearfix"></div>
			<br>


			@include('admin.partials.inline_template', ['add_msg_to' => 'sms_message', "type" => 'user_sms', 'lable_col_size' => 1])


			<div class="sms_spinner col-sm-1 text-center" style="display:none;">
                <i class="icon-spinner2 spinner"></i>
            </div>
			<div class="text-right">
				<button type="submit" id="sms_submit_form" class="btn btn-primary">Send <i class="icon-arrow-right14 position-right"></i></button>
			</div>
		{!! Form::close() !!}
  	</div>
</div>
@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function(){
		//start Maxlength
		    $('.maxlength').maxlength();
		    $('.maxlength-threshold').maxlength({
		        threshold: 15
		    });
		    $('.maxlength-custom').maxlength({
		        threshold: 10,
		        warningClass: "label label-primary",
		        limitReachedClass: "label label-danger"
		    });
		    $('.maxlength-options').maxlength({
		        alwaysShow: true,
		        threshold: 10,
		        warningClass: "text-success",
		        limitReachedClass: "text-danger",
		        separator: ' of ',
		        preText: 'You have ',
		        postText: ' chars remaining.',
		        validate: true
		    });
		    $('.maxlength-textarea').maxlength({
		        alwaysShow: true
		    });
		    $('.maxlength-label-position').maxlength({
		        alwaysShow: true,
		        placement: 'top'
		    });
	    //end maxlength

	    jQuery( '.send_sms_form' ).submit(function(e) {
	      	e.preventDefault();
	      	jQuery('.sms_spinner').hide();
	    	var data = jQuery(this).serializeArray();
		    var $url = jQuery(this).attr('action');
		    jQuery.ajax({
		        type: 'POST',
		        url: $url,
		        data: data,
		        dataType: "json",
		        beforeSend: function() {
		            jQuery('.sms_spinner').show();
		            jQuery('#sms_submit_form').attr('disabled',true);
		        },
		        success: function(data) {
		            if(data.success == 1){
		              	swal("SMS sent!", "SMS has been successfully sent", "success");
		            } else {
		            	swal("SMS Failed!", "Failed to send SMS", "error");
		            }
		        },
		        error: function(data) {
		            console.log(data);
		        },
		        complete: function() {
		            jQuery('.sms_spinner').hide();
		            jQuery('.send_sms_form').find('.sms_message').val('');
		            jQuery('#sms_submit_form').attr('disabled',false);
		        }
		    });
	    });
	});
</script>
@endpush