<div id="profile" class="tab-pane fade in {{(!isset($_REQUEST['tab']))?'active':''}}">
	<div class="boxspace">
		<div class="table-responsive">
			<table class="table table-xxs table-bordered">
				<tbody>
					<tr>
						<td><span class="text-black">Name </span></td>
						<td>{{$user->name}}</td>
					</tr>
					<tr>
						<td><span class="text-black">Primary Email </span></td>
						<td><a href="tel:{{$user->email}}">{{$user->email}}</a></td>
					</tr>
					@if(!empty($user->other_emails))
						<?php $other_emails = explode(',', $user->other_emails); ?>
						<tr>
							<td rowspan="{{count($other_emails)+1}}"><span class="text-black">Other Emails </span></td>
						</tr>
						@foreach($other_emails AS $other_email)
							<tr>
								<td><a href="tel:{{$other_email}}">{{$other_email}}</a></td>
							</tr>
						@endforeach
					@endif
					<tr>
						<td><span class="text-black">Primary Number </span></td>
						<td><a href="tel:{{$user->mobile}}">{{$user->mobile}}</a></td>
					</tr>
					<tr>
						<td><span class="text-black">Secondary Number </span></td>
						<td><a href="tel:{{$user->official_mobile}}">{{$user->official_mobile}}</a></td>
					</tr>
					@if(!empty($user->other_mobiles))
						<?php $other_mobiles = explode(',', $user->other_mobiles); ?>
						<tr>
							<td rowspan="{{count($other_mobiles)+1}}"><span class="text-black">Other Numbers </span></td>
						</tr>
						@foreach($other_mobiles AS $other_mobile)
							<tr>
								<td><a href="tel:{{$other_mobile}}">{{$other_mobile}}</a></td>
							</tr>
						@endforeach
					@endif
					<tr>
						<td><span class="text-black">Transcript Status </span></td>
						<td>{{$user->transcript_status}}
						@if($user->transcript_status == 'Yes')
							<a class="btn btn-primary btn-xs pull-right" href="{{asset($user->transcript_attachment->path.'/'.$user->transcript_attachment->file_name)}}"><i class="fa fa-download"></i></a>
						@endif
						</td>
					</tr>
					<tr>
						<td><span class="text-black">KYC Status </span></td>
						<td>{{$user->kyc_status}}
							@if($user->kyc_status == 'Yes')
								<a class="btn btn-primary btn-xs pull-right" href="{{asset($user->kyc_attachment->path.'/'.$user->kyc_attachment->file_name)}}"><i class="fa fa-download"></i></a>
							@endif
						</td>
					</tr>
					<tr>
						<td><span class="text-black">RAF Status </span></td>
						<td>{{$user->raf_status}}
							@if($user->raf_status == 'Yes')
								<a class="btn btn-primary btn-xs pull-right" href="{{asset($user->raf_attachment->path.'/'.$user->raf_attachment->file_name)}}"><i class="fa fa-download"></i></a>
							@endif
						</td>
					</tr>
					<tr>
						<td><span class="text-black">Consent Status </span></td>
						<td>{{$user->consent_status}}</td>
					</tr>
					<tr>
						<td><span class="text-black">Consent IP </span></td>
						<td>{{$user->consent_ip}}</td>
					</tr>


					<tr>
						<td><span class="text-black">Relationship Manager </span></td>
						<td>{{ (isset($relationship_manager->name)?$relationship_manager->name:'N/A') }}</td>
					</tr>

					<tr>
						<td><span class="text-black">Suitability Verification Manager</span></td>
						<td>
							@if($sv_officer)
								{{$sv_officer->name}}
							@endif

						</td>
					</tr>

					<tr>
						<td><span class="text-black">Investor Type </span></td>
						<td>{{ config('rothmans.investor_type.'.$user->investor_type) }}</td>
					</tr>

					<tr>
							<td><span class="text-black">Investor Horizon</span></td>
						<td>{{ config('rothmans.horizon.'.$user->horizon) }}</td>
					</tr>

					<tr>
						<td><span class="text-black">Client Ranking</span></td>
						<td></td>
					</tr>

					<tr>
						<td><span class="text-black">Client potential</span></td>
						<td></td>
					</tr>

					<tr>
						<td><span class="text-black">Revenue Comparision </span></td>
						<td></td>
					</tr>
					<tr>
						<td><span class="text-black">Last conversation with Client by RM</span></td>
						<td></td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>
</div>