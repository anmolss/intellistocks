<div id="service_delivery_report" class="tab-pane fade in {{(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'service_delivery_report')?'active':''}}">
  <div class="boxspace">
    <div class="caption text-center">
      <h1 class="text-semibold no-margin">Service Delivery</h1>

      <div class="clearfix"></div>
      <br>
      <div class="table-responsiv">
        <table class="table table-striped table-hover table-bordered text-center">
          <thead class="bg-blue">
          <tr>
            <th>Service Name</th>
            <th>Date</th>
            <th>Symbol</th>
            <th>Call Type</th>
            <th>Quantity / Lot(s)</th>
            <th>Message</th>
            <th class="text-center">Suitability Verification <br><input type="checkbox" class="styled" id="suitability_checked_all" data-sdid="0"></th>

          </tr>
          </thead>
          <tbody>
          @if(count($service_deliveries) > 0)
            @foreach($service_deliveries as  $key => $item)
              <tr>
                <td>{{ $item->service->name }}</td>
                <td>{{ \Carbon\Carbon::parse($item->created_at)->format(config('rothmans.date_format')) }}</td>
                <td>{{ $item->symbol }}</td>
                <td>{{ $item->call_type }}</td>
                @if($item->sd->exchange == 'nse')
                  <td>{{ $item->qty }}%</td>
                @else
                  <td>{{ $item->nfo_qty }}</td>
                @endif
                <td>{{ $item->msg }}</td>
                <td>
                    <input type="checkbox" class="styled suitability_checked" data-sdid="{{$item->id}}"
                    @if($item->suitability)v] checked @endif>
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="7" align="center">No Service Delivery Found</td>
            </tr>
          @endif
          </tbody>
        </table>

      </div>


      <div class="row">
        {!! $service_deliveries->appends(Request::except('page'))->appends(['tab'=> 'service_delivery_report'])->links() !!}
      </div>
    </div>
  </div>
</div>

@push('scripts')
<script type="text/javascript">
    var suitable_var = {};
    $('document').ready(function(){
        var select_all = false;
        $('.suitability_checked').each(function (obj) {
            suitable_var[$(this).data('sdid')] = $(this).prop("checked") ? 1: 0;
        });

        $('#suitability_checked_all').click(function(){
            select_all = true;
            if($(this).is(':checked')){
                $('input.suitability_checked').prop("checked",true).trigger('change');
            }
            else{
                $('input.suitability_checked').prop("checked",false).trigger('change');
            }
            $('input.suitability_checked').prop("disabled",true);
            $.uniform.update();

            suitabilty_update();
            select_all = false;

            $('input.suitability_checked').prop("disabled",false);
            $.uniform.update();
        })

        $('.suitability_checked').change(function () {
            suitable_var[$(this).data('sdid')] = $(this).prop("checked") ? 1: 0;
            if(select_all === false){
                suitabilty_update();
            }
        });
    });

    function suitabilty_update() {
        $.ajax({
            method: "POST",
            url: "{{ route('admin::service_delivery.update_suitabilty') }}",
            data: {suitable: suitable_var}
        })
        .done(function( data ) {
            show_stack_top_right('success', 'Suitability updated', 2000);
        });
    }
</script>

@endpush