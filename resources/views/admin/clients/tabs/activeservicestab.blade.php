<div id="services" class="tab-pane fade in">
  <div class="boxspace">
    <div class="panel panel-flat table-responsive small">
        <table class="table table-hover" id="customers-table">
        <tbody>

        <tr>
            <th>Service Count</th>
            <td>{{ $pnl_all['service_count']}}</td>
        </tr>

        <tr>
            <th>Total Capital</th>
            <td>{{ $pnl_all['total_capital'] }}</td>
        </tr>

        <tr>
            <th>Total calls</th>
            <td>{{ $pnl_all['total_calls']}}</td>
        </tr>
        <tr>
            <th>Positive Calls</th>
            <td>{{ $pnl_all['positive_call'] }}</td>
        </tr>

        <tr>
            <th>Total P&L</th>
            <td>{{ $pnl_all['total_profit'] }}</td>
        </tr>
        <tr>
            <th>Today's Pnl</th>
            <td>{{ $pnl_all['today_pnl'] }}</td>
        </tr>

        </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
    <a href="{!! route('admin::clients.addService', $user->id) !!}" class="btn btn-primary pull-right">Add New Service</a>
    <div class="clearfix"></div>
    <br>
    <div class="table-responsiv">
      <table class="table table-striped table-hover table-bordered text-center">
        <thead>
          <tr class="bg-blue">
            <th rowspan="2" class="text-center" width="50">#</th>
            <th rowspan="2" class="text-center">Service</th>
            <th rowspan="2" class="text-center">Plan</th>
            <th colspan="4" class="text-center">Amount</th>
            <th colspan="2" class="text-center">Date</th>
            <th rowspan="2" class="text-center" width="85">Status</th>
            <th rowspan="2" class="text-center" width="125">Actions</th>
          </tr>
          <tr class="bg-blue">
            <th class="text-center" width="100">Service Capital</th>
            <th class="text-center" width="100">Payable</th>
            <th class="text-center" width="100">Paid</th>
            <th class="text-center" width="100">Uncleared</th>
            <th class="text-center" width="115">Activation</th>
            <th class="text-center" width="115">Expiry</th>
            <!--<th class="text-center" width="125">Created</th>-->
          </tr>
        </thead>
        <tbody>
        <?php $total_capital = $total_payable = $total_paid = $total_uncleared = 0; ?>
        @forelse($user_services as $key => $user_service)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$user_service->service->name}}</td>
            <td>{{$user_service->plan->name}}</td>
            <?php
            foreach ($user_service->transections as $transaction) {
              if($user_service->latest_transaction_history_id == $transaction->transaction_history_id){
                $amount_payable = $transaction->payable_amount;
              } else {
                $amount_payable = 0;
              }
            }
            if(isset($user_service->transections)) {
              $all = $user_service->transections->toArray();
              if(count($all) > 0){
                $amount_paid = array_sum(
                  array_map(
                    function($item) use ($user_service)  {
                      if($item['cleared'] == 'Yes' && $user_service->latest_transaction_history_id == $item['transaction_history_id']) { 
                        return $item['amount']; 
                      } else { 
                        return 0; 
                      } 
                    }, $all
                  )
                );
              }
            }

            $balance = round($amount_payable, 0, PHP_ROUND_HALF_UP) - round($amount_paid, 0, PHP_ROUND_HALF_UP);
            $total_capital = $total_capital+$user_service->service_capital;
            $total_payable = $total_payable+$amount_payable;
            $total_paid = $total_paid+$amount_paid;
            $total_uncleared = $total_uncleared+$balance;
            ?>
            <td>{{($user_service->service_capital > 0)?'Rs.'.$user_service->service_capital:'N/A'}}</td>
            <td>Rs. {{$amount_payable}}</td>
            <td>Rs. {{$amount_paid}}</td>
            <td>Rs. {{$balance}}</td>
            <td>{{(new Carbon($user_service->activation_date))->format('d-m-Y')}}</td>
            <td>{{(!empty($user_service->expire_date))?((new Carbon($user_service->expire_date))->format('d-m-Y')):'N/A'}}</td>
            <!--
            <td>{{(new Carbon($user_service->created_at))->format('d-m-Y h:i A')}}</td>
            -->
            <td>
              @if($user_service->service_status == 'Active')
                <label class="bg-success text-highlight">{{$user_service->service_status}}</label>
              @else
                <label class="bg-warning text-highlight">{{$user_service->service_status}}</label>
              @endif
            </td>
            <td>
              <?php
                $activation_date = new Carbon($user_service->activation_date);
                if(!empty($user_service->plan->duration)){
                  $expiry_date = $activation_date->subDay()->addDays($user_service->plan->duration)->format('d-m-Y');
                } else {
                  $expiry_date = $activation_date->subDay()->addDays($user_service->plan->duration)->format('d-m-Y');
                }
              ?>
              
                
              <ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i class="icon-gear"></i>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <!--
                        @if($user_service->consent_status == 'No')
                          <li>
                              <a href="{{route('admin::user_services.resendVerification', $user_service->id)}}" class="resend_activation"><i class="icon-alarm-check"></i> Resend Activation</a>
                          </li>
                        @endif
                        <li>
                            <a 
                              href="javascript:"
                              data-s_name ="{{$user_service->service->name}}"  
                              data-s_plan ="{{$user_service->plan->name}}"  
                              data-id="{{$user_service->id}}" 
                              data-transcript="{{$user_service->transcript_status}}" 
                              data-kyc="{{$user_service->kyc_status}}" 
                              data-consent="{{$user_service->consent_status}}" 
                              data-activation-date="{{(new Carbon($user_service->activation_date))->format('d-m-Y')}}" data-expiry-date="{{$expiry_date}}" 
                              data-balance-warning="{{($balance > 0)?'yes':'no'}}"
                              data-consent-ip="{{$user_service->consent_ip}}"
                              class="service_verify"
                            >
                                <i class="icon-file-check2"></i> Verify Service
                            </a>
                        </li>
                      -->
                        <li>
                            <a href="{{route('admin::user_services.showUserService', $user_service->id)}}"><i class="icon-eye2"></i> View</a>
                        </li>
                        <li>
                          <a href="{{route('admin::active_service.edit', $user_service->id)}}"><i class="icon-pencil7"></i> Edit</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                          <a href="{{route('admin::active_service.renew', $user_service->id)}}"><i class="icon-reload-alt"></i> Renew</a>
                        </li>
                        <li>
                            <a href="{{route('admin::services.tempExtnService')}}" class="modal-action" data-toggle="modal" data-id="{{$user_service->id}}" data-action="temp_extn" data-target="#action_modal"><i class="icon-move-horizontal"></i> Temp Extn</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{route('admin::services.terminateService')}}" class="modal-action" data-toggle="modal" data-id="{{$user_service->id}}" data-action="terminate" data-target="#action_modal"><i class="icon-x"></i> Terminate</a>
                        </li>
                        <li>
                            <a href="{{route('admin::services.destroyActiveServices', $user_service->id)}}" class="text-danger-700 btn-delete"><i class="icon-trash"></i> Delete</a>
                        </li>
                    </ul>
                </li>
            </ul>
            </td>
          </tr>
        @empty
          <tr>
            <td colspan="11">
              <div class="alert bg-warning alert-rounded">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                <span class="text-semibold">No Data!</span> No Services Assigned.
              </div>
            </td>
          </tr>
        @endforelse
        @if(!empty($user_services))
          <tr class="bg-info">
            <td colspan="3"> Total Amount</td>
            <td>Rs. {{$total_capital}}</td>
            <td>Rs. {{$total_payable}}</td>
            <td>Rs. {{$total_paid}}</td>
            <td>Rs. {{$total_uncleared}}</td>
            <td colspan="4"></td>
          </tr>
        @endif
        </tbody>
      </table>
    </div>
  </div>
</div>
@include('admin.partials.action_modal')
<div id="verify_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
          <form class="verify_modal_form" id="verify_form" action="{{route('admin::verifyService')}}" enctype="multipart/form-data">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Verify Customer</h5>
            </div>
            <div class="modal-body">
              <input type="hidden" class="user_id" name="user_id" value="">
              <div class="col-md-6">
                <label class="checkbox-inline">
                  <input type="checkbox" name="transcript_status" value="yes" class="transcript_status checkbox">
                  <span class="text-bold text-danger">Transcript of Onboarding call</span>
                </label>
              </div>
              <div class="col-md-6 transcript_file" style="display:none;">
                <input type="file" name="transcript_file" class="file-styled">
              </div>
              <div class="clearfix"></div>
              <br>
              <div class="col-md-6">
                <label class="checkbox-inline">
                  <input type="checkbox" name="kyc_status" value="yes" class="kyc_status checkbox">
                  <span class="text-bold text-danger">KYC Form</span>
                </label>
              </div>
              <div class="col-md-6 kyc_file" style="display:none;">
                <input type="file" name="kyc_file" class="file-styled">
              </div>
              <div class="clearfix"></div>
              <br>
              <div class="col-md-6">
                <label class="checkbox-inline">
                  <input type="checkbox" name="consent_status" value="yes" class="consent_status checkbox">
                  <span class="text-bold text-danger">Consent</span>
                </label>
              </div>
              <div class="col-md-6">
                <p><strong>Consent IP : </strong><span class="consent_ip"></span></p>
              </div>
              <div class="clearfix"></div>
              <br>
              <div class="col-md-12" style="margin:10px 0px;">
                 <div id="verify_success" class="alert alert-success" style="display:none;"></div>
                 <div id="verify_failed" class="alert alert-danger" style="display:none;"></div>
              </div>
            </div>
            <div class="modal-footer">
              <span class="verify_service_form_spinner" style="display:none;">
                <i class="icon-spinner2 spinner"></i>
              </span>
              <button type="submit" class="btn btn-primary save_verification">Save</button>
              <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
          </form>
            <div id="modal_content_append"></div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">

    $('.daterange-delivery_range').daterangepicker({
        applyClass: 'bg-info',
        cancelClass: 'btn-default',
        locale: {
            format: 'DD/MM/YYYY'
        }
    });
    // Enable Select2 select for the length option
    $('select').select2({
        minimumResultsForSearch: Infinity
    });

    $('.pnl_service_model').on('click', function(e){
        e.preventDefault();
        $('#PnLServiceModel').modal('show').find('.modal-title').text($(this).text())
        $('#PnLServiceModel').modal('show').find('.modal-body').html('<p>Loading...</p>').load($(this).attr('href'));
    }).on('show.bs.modal', function () {
        $(this).find('.modal-body').css({
            width:'auto', //probably not needed
            height:'auto', //probably not needed
            'max-height':'100%'
        });
    });
</script>

@endpush