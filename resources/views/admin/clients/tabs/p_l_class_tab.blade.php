@if($pnl_services)
    @foreach($pnl_services as $key => $service)
        <div id="p_l_{{ $service['service_id'] }}_class" ng-controller="p_l_{{ $service['service_id'] }}_class" class="tab-pane fade in {{(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'p_l_'.$service['service_id'].'_class')?'active':''}}">
          <div class="boxspace">
              {{--<form class="form-horizontal" action="#" id="customer-frm-filter" method="GET">
                  <div class="panel panel-flat">
                      <div class="panel-heading">
                          <h6 class="panel-title text-semibold"><i class="icon-equalizer position-left"></i> Filters</h6>
                          <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
                      <div class="panel-body">
                          <div class="row">
                              <div class="col-sm-12">
                                  <fieldset>
                                      <div class="row">
                                          <div class="col-sm-4">
                                              <div class="form-group">
                                                  {!! Form::label('creation_range', 'Creation Date:', ['class' => 'control-label col-lg-4']) !!}
                                                  <div class="col-lg-8">
                                                      {!! Form::text('delivery_range', $default_delivery_range, ['class' => 'form-control daterange-delivery_range']) !!}
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-sm-3">
                                              <div class="form-group">
                                                  {!! Form::label('service', 'Service:', ['class' => 'control-label col-lg-3']) !!}
                                                  <div class="col-lg-9">
                                                      {!! Form::select('service[]', $services, $default_service, ['class' => 'form-control', 'id' => 'services', 'multiple'  ] ) !!}
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-sm-4 ">
                                              <button type="submit" class="btn btn-primary col-sm-3 col-sm-offset-2">Filter <i class="icon-arrow-right14 position-right"></i></button>
                                              <a title="Reset Filter!" id="reset_search" href="javascript:" class="btn btn-success btn-rounded col-sm-3 col-sm-offset-2">Reset <i class="icon-spinner11 position-right"></i></a>
                                          </div>
                                      </div>
                                  </fieldset>
                              </div>
                          </div>
                      </div>
                  </div>
              </form>--}}
              <div class="panel panel-flat table-responsive">
                  <table class="table table-hover" id="customers-table">
                      <tbody>
                          <tr>
                              <th>Service Name</th>
                              <td>{{ $service['service_name']}}</td>

                              <th>Today's P&L</th>
                              <td>{{ $service['today_pnl'] }}</td>
                          </tr>
                          <tr>
                              <th>Available limit</th>
                              <td>{{ round(bcsub(100, $service['user'][$user->id]['avail_qty']), 2) }}%</td>

                              <th>Gross Contribution of winning calls</th>
                              <td>{{ $service['gross_profit'] }}%</td>
                          </tr>
                              <th>Total calls</th>
                              <td>{{ Html::link(route('admin::clients.customerPnlService', $user->id)."?service[]=".$service['service_id'], $service['total_call'],['target' => '_blank', 'class' => 'pnl_service_model'] ) }}</td>

                              <th>Gross Contribution of Losing calls</th>
                              <td>{{ $service['gross_loss'] }}%</td>

                          </tr>
                          <tr>
                              <th>Open Calls</th>
                              <td>{{ Html::link(route('admin::clients.customerPnlService', $user->id)."?call_type=Open&service[]=".$service['service_id'], $service['open_call'],['target' => '_blank', 'class' => 'pnl_service_model'] ) }}</td>

                              <th>Cumulative Returns from all calls</th>
                              <td>{{ $service['cumulative_return_all_call'] }}%</td>

                          </tr>
                          <tr>
                              <th>Close Calls</th>
                              <td>{{ Html::link(route('admin::clients.customerPnlService', $user->id)."?call_type=Close&service[]=".$service['service_id'], $service['close_call'],['target' => '_blank', 'class' => 'pnl_service_model'] ) }}</td>

                              <th>Reward Vs Risk </th>
                              <td>{{ $service['r_v_r'] }}</td>
                          </tr>
                          <tr>
                              <th>Positive Calls</th>
                              <td>{{ $service['positive_call'] }}</td>

                              <th>Average months Position held</th>
                              <td>{{ $service['months_pos'] }}</td>
                          </tr>
                          <tr>
                              <th>Wining % </th>
                              <td>{{ $service['winning_percent'] }}% </td>
                          </tr>
                          <tr>
                              <th>Open P&L</th>
                              <td>{{ $service['open_profit'] }}</td>
                          </tr>
                          <tr>
                              <th>Realized P&L</th>
                              <td>{{ $service['booked_profit'] }}</td>
                          </tr>
                          <tr>
                              <th>Total P&L</th>
                              <td>{{ $service['total_profit'] }}</td>

                              <td colspan="2" class="text-right">
                                  <div class="checkbox-inline mt-10 mr-20">
                                      <label class="control-label"><input apply-uniform type="checkbox" ng-model="report_full" /> Consolidated</label>
                                  </div>
                                  <a class="btn btn-primary share_pnl_modal" target="" data-service="{{$service['service_id']}}">Share Report</a>
                                  <a class="btn btn-primary" target="" href="{{ route('admin::clients.download_pnl', ['id'=> $id, 'service' => $service['service_id']] ) }}">Download</a>
                              </td>
                          </tr>

                      </tbody>
                  </table>
              </div>

              <div class="table-responsive dataTable">
                  <table class="table table-hover table-xxs" id="customers-table">
                      <thead>
                      <tr>
                          <th class="cursor-pointer  @{{ (propertyName == 'symbol') ? (reverse) ? 'sorting_asc' : 'sorting_desc' : 'sorting' }}" ng-click="sortBy('symbol')">Stock Name</th>
                          <th class="cursor-pointer  @{{ (propertyName == 'status') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('status')">Status</th>
                          <th class="cursor-pointer  @{{ (propertyName == 'entry_date') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('entry_date')">Entry date</th>
                          <th class="cursor-pointer  @{{ (propertyName == 'avg_price') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('avg_price')">Entry Price</th>
                          <th class="cursor-pointer  @{{ (propertyName == 'exit_price') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('exit_price')">CMP / EXIT PRICE</th>
                          <th class="cursor-pointer  @{{ (propertyName == 'exit_date') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('exit_date')">Close / Current Date</th>
                          <th class="cursor-pointer  @{{ (propertyName == 'open_profit') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('open_profit')">Open Profit</th>
                          <th class="cursor-pointer  @{{ (propertyName == 'booked_profit') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('booked_profit')">Booked Profit</th>
                          <th class="cursor-pointer  @{{ (propertyName == 'total_profit') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('total_profit')">Total Profit</th>
                          <th class="cursor-pointer  @{{ (propertyName == 'profit_percent') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('profit_percent')">Overall Profit (%)</th>
                          <th class="cursor-pointer  @{{ (propertyName == 'open_stock_weight') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('open_stock_weight')"><span data-toggle="tooltip" title="Open Position / Stock Weight of Total Investment">Open Stock Weight (%)</span></th>
                          <th class="cursor-pointer  @{{ (propertyName == 'close_stock_weight') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('close_stock_weight')"><span data-toggle="tooltip" title="Closed Position / Stock Weight of Total Investment">Close Stock Weight (%)</span></th>
                          <th class="cursor-pointer  @{{ (propertyName == 'total_stock_weight') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('total_stock_weight')"><span data-toggle="tooltip" title="Stock Weight of Total Investment">Close Stock Weight (%)</span></th>
                          <th class="cursor-pointer  @{{ (propertyName == 'net_roi') ? ((reverse) ? 'sorting_asc' : 'sorting_desc') : 'sorting' }}" ng-click="sortBy('net_roi')">Net ROI</th>
                      </tr>
                      </thead>

                      <tbody ng-if="!report_full" ng-repeat="(key,stock) in stocks | orderBy:propertyName:reverse">
                          <tr>
                              <th class="clickable cursor-pointer text-blue-700" data-toggle="collapse"
                                  data-parent="#customers-table" data-target=".@{{ key }}">
                                  @{{ stock.symbol }} +
                                  <br>
                                  <small>(
                                      @{{ stock.exchange }}
                                      <span ng-if="stock.instrument != 'stock'">
                                          , @{{ stock.expiry_date | date:'MM-yy' }}
                                      </span>
                                  )</small>
                              </th>
                              <td>@{{ stock.status }}</td>
                              <td>@{{ stock.entry_date | date }}</td>
                              <td>@{{ stock.avg_price | currency:"&#8377;" }}</td>
                              <td>@{{ stock.exit_price | currency:"&#8377;" }}</td>
                              <td>@{{ stock.exit_date | date }}</td>
                              <td>@{{ stock.open_profit | currency:"&#8377;"}}</td>
                              <td>@{{ stock.booked_profit | currency:"&#8377;" }}</td>
                              <td>@{{ stock.total_profit | currency:"&#8377;" }}</td>
                              <td>@{{ stock.profit_percent }}%</td>
                              <td>@{{ stock.open_stock_weight }}%</td>
                              <td>@{{ stock.close_stock_weight }}%</td>
                              <td>@{{ stock.total_stock_weight }}%</td>
                              <td>@{{ stock.net_roi }}%</td>

                              {{--<th>{{ $stock['target_mean'] }} <br> {{ $stock['sl_mean'] }}</th>
                              <th>{{ $stock['target_p_away'] }}% <br> {{ $stock['sl_p_away'] }}%</th>--}}
                          </tr>

                          <tr ng-if="stock.exchange != 'nfo'" class="collapse @{{ key }}">
                              <td colspan="17" class="no-padding">
                                  <table class="table bg-teal table-xxs">
                                      <caption class="text-center">
                                          <h3>
                                              Transaction History
                                          </h3>
                                      </caption>
                                      <thead>
                                      <tr class="bg-teal-700">
                                          <th>Transactions Date</th>
                                          <th>Stock Name</th>
                                          <th>Price</th>
                                          <th>Qty</th>
                                          <th>Call Type</th>
                                          <th>Status</th>
                                          <th>Open Profit</th>
                                          <th>Booked Profit</th>
                                          <th>Total</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                          <tr ng-repeat="delivery in stock.deliveries" class="bg-teal">
                                              <td>@{{ delivery.entry_date | date:'dd-MM-yyyy' }}</td>
                                              <td>@{{ delivery.symbol }}</td>
                                              <td>@{{ delivery.price | currency:"&#8377;" }}</td>
                                              <td>@{{ delivery.qty }}%<br> @{{ delivery.curr_qty }}</td>
                                              <td>@{{ delivery.service_call_type }}</td>
                                              <td>@{{ delivery.status }}</td>
                                              <td>@{{ delivery.open_profit | currency:"&#8377;" }}</td>
                                              <td>@{{ delivery.booked_profit | currency:"&#8377;" }}</td>
                                              <td>@{{ delivery.total_profit | currency:"&#8377;" }}</td>
                                          </tr>
                                      </tbody>
                                  </table>
                              </td>
                          </tr>

                          <tr ng-if="stock.exchange == 'nfo'" class="collapse @{{ key }}">
                              <td colspan="17" class="no-padding">
                                  <table class="table bg-teal table-xxs">
                                      <caption class="text-center">
                                          <h3>
                                              Transaction History<br>
                                              <small class="text-blue-700">(
                                                  @{{ stock.exchange }}
                                                  , @{{ stock.nfo_instrument }}
                                                  , @{{ stock.expiry_date | date:'MM-yy' }}
                                                  <span ng-if="stock.instrument == 'option'">
                                                      , @{{ stock.option_type | uppercase }}
                                                      , @{{ stock.strike }}
                                                  </span>
                                                  )</small>
                                          </h3>
                                      </caption>
                                      <thead>
                                      <tr class="bg-teal-700">
                                          <th>Transactions Date</th>
                                          <th>Stock Name</th>
                                          <th>Price</th>
                                          <th>Lot</th>
                                          <th>Lot Size</th>
                                          <th>Call Type</th>
                                          <th>Status</th>
                                          <th>Open Profit</th>
                                          <th>Booked Profit</th>
                                          <th>Total</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                          <tr ng-repeat="delivery in stock.deliveries" class="bg-teal">
                                              <td>@{{ delivery.entry_date | date:'dd-MM-yyyy' }}</td>
                                              <td>@{{ delivery.symbol }}</td>
                                              <td>@{{ delivery.price | currency:"&#8377;" }}</td>
                                              <td>@{{ delivery.nfo_qty }}</td>
                                              <td>@{{ delivery.lot_size }}</td>
                                              <td>@{{ delivery.service_call_type }}</td>
                                              <td>@{{ delivery.status }}</td>
                                              <td>@{{ delivery.open_profit | currency:"&#8377;" }}</td>
                                              <td>@{{ delivery.booked_profit | currency:"&#8377;" }}</td>
                                              <td>@{{ delivery.total_profit | currency:"&#8377;" }}</td>
                                          </tr>
                                      </tbody>
                                  </table>
                              </td>
                          </tr>
                      </tbody>

                      <tbody ng-if="report_full" ng-repeat="(key,stock) in consolidated | orderBy:propertyName:reverse">
                          <tr>
                              <th class="clickable cursor-pointer text-blue-700" data-toggle="collapse"
                                  data-parent="#customers-table" data-target=".@{{ key }}">
                                  @{{ stock.symbol }} +
                              </th>
                              <td>@{{ stock.status }}</td>
                              <td>@{{ stock.entry_date | date }}</td>
                              <td>@{{ stock.avg_price | currency:"&#8377;" }}</td>
                              <td>@{{ stock.exit_price | currency:"&#8377;" }}</td>
                              <td>@{{ stock.exit_date | date }}</td>
                              <td>@{{ stock.open_profit | currency:"&#8377;"}}</td>
                              <td>@{{ stock.booked_profit | currency:"&#8377;" }}</td>
                              <td>@{{ stock.total_profit | currency:"&#8377;" }}</td>
                              <td>@{{ stock.profit_percent | number }}%</td>
                              <td>@{{ stock.open_stock_weight  | number}}%</td>
                              <td>@{{ stock.close_stock_weight | number }}%</td>
                              <td>@{{ stock.total_stock_weight | number }}%</td>
                              <td>@{{ stock.net_roi  | number}}%</td>
                          </tr>
                          <tr class="collapse @{{ key }}">
                              <td colspan="14">
                                  <table class="table table-xxs">
                                      <tbody ng-repeat="stock_inner in stock.stocks">
                                          <tr>
                                              <th>
                                                  @{{ stock_inner.symbol }} +
                                                  <br>
                                                  <small>(
                                                      @{{ stock_inner.exchange }}
                                                      <span ng-if="stock.instrument != 'stock'">
                                              , @{{ stock_inner.expiry_date | date:'MM-yy' }}
                                          </span>
                                                      )</small>
                                              </th>
                                              <td>@{{ stock_inner.status }}</td>
                                              <td>@{{ stock_inner.entry_date | date }}</td>
                                              <td>@{{ stock_inner.avg_price | currency:"&#8377;" }}</td>
                                              <td>@{{ stock_inner.exit_price | currency:"&#8377;" }}</td>
                                              <td>@{{ stock_inner.exit_date | date }}</td>
                                              <td>@{{ stock_inner.open_profit | currency:"&#8377;"}}</td>
                                              <td>@{{ stock_inner.booked_profit | currency:"&#8377;" }}</td>
                                              <td>@{{ stock_inner.total_profit | currency:"&#8377;" }}</td>
                                              <td>@{{ stock_inner.profit_percent }}%</td>
                                              <td>@{{ stock_inner.open_stock_weight }}%</td>
                                              <td>@{{ stock_inner.close_stock_weight }}%</td>
                                              <td>@{{ stock_inner.total_stock_weight }}%</td>
                                              <td>@{{ stock_inner.net_roi }}%</td>
                                          </tr>

                                          <tr ng-if="stock_inner.exchange != 'nfo'">
                                              <td colspan="17" class="no-padding">
                                                  <table class="table bg-teal table-xxs">
                                                      <caption>
                                                              Transaction History
                                                      </caption>
                                                      <thead>
                                                      <tr class="bg-teal-700">
                                                          <th>Transactions Date</th>
                                                          <th>Stock Name</th>
                                                          <th>Price</th>
                                                          <th>Qty</th>
                                                          <th>Call Type</th>
                                                          <th>Status</th>
                                                          <th>Open Profit</th>
                                                          <th>Booked Profit</th>
                                                          <th>Total</th>
                                                      </tr>
                                                      </thead>
                                                      <tbody>
                                                      <tr ng-repeat="delivery in stock_inner.deliveries" class="bg-teal">
                                                          <td>@{{ delivery.entry_date | date:'dd-MM-yyyy' }}</td>
                                                          <td>@{{ delivery.symbol }}</td>
                                                          <td>@{{ delivery.price | currency:"&#8377;" }}</td>
                                                          <td>@{{ delivery.qty }}%<br> @{{ delivery.curr_qty }}</td>
                                                          <td>@{{ delivery.service_call_type }}</td>
                                                          <td>@{{ delivery.status }}</td>
                                                          <td>@{{ delivery.open_profit | currency:"&#8377;" }}</td>
                                                          <td>@{{ delivery.booked_profit | currency:"&#8377;" }}</td>
                                                          <td>@{{ delivery.total_profit | currency:"&#8377;" }}</td>
                                                      </tr>
                                                      </tbody>
                                                  </table>
                                              </td>
                                          </tr>

                                          <tr ng-if="stock_inner.exchange == 'nfo'">
                                          <td colspan="17" class="no-padding">
                                              <table class="table bg-teal table-xxs">
                                                  <caption>

                                                          Transaction History
                                                          <strong class="text-blue-700">(
                                                              @{{ stock_inner.exchange }}
                                                              , @{{ stock_inner.nfo_instrument }}
                                                              , @{{ stock_inner.expiry_date | date:'MM-yy' }}
                                                              <span ng-if="stock_inner.instrument == 'option'">
                                                      , @{{ stock_inner.option_type | uppercase }}
                                                                  , @{{ stock_inner.strike }}
                                                  </span>
                                                              )</strong>
                                                  </caption>
                                                  <thead>
                                                  <tr class="bg-teal-700">
                                                      <th>Transactions Date</th>
                                                      <th>Stock Name</th>
                                                      <th>Price</th>
                                                      <th>Lot</th>
                                                      <th>Lot Size</th>
                                                      <th>Call Type</th>
                                                      <th>Status</th>
                                                      <th>Open Profit</th>
                                                      <th>Booked Profit</th>
                                                      <th>Total</th>
                                                  </tr>
                                                  </thead>
                                                  <tbody>
                                                  <tr ng-repeat="delivery in stock_inner.deliveries" class="bg-teal">
                                                      <td>@{{ delivery.entry_date | date:'dd-MM-yyyy' }}</td>
                                                      <td>@{{ delivery.symbol }}</td>
                                                      <td>@{{ delivery.price | currency:"&#8377;" }}</td>
                                                      <td>@{{ delivery.nfo_qty }}</td>
                                                      <td>@{{ delivery.lot_size }}</td>
                                                      <td>@{{ delivery.service_call_type }}</td>
                                                      <td>@{{ delivery.status }}</td>
                                                      <td>@{{ delivery.open_profit | currency:"&#8377;" }}</td>
                                                      <td>@{{ delivery.booked_profit | currency:"&#8377;" }}</td>
                                                      <td>@{{ delivery.total_profit | currency:"&#8377;" }}</td>
                                                  </tr>
                                                  </tbody>
                                              </table>
                                          </td>
                                      </tr>
                                      </tbody>
                                  </table>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
        </div>


        @push('scripts')
        <script type="text/javascript">
            app.controller('p_l_{{ $service['service_id'] }}_class',function($scope, $http, $log, $document, sweet, $window ){
                var stocks = {!! json_encode($service['stocks']) !!}

                $scope.propertyName = 'symbol';
                $scope.reverse = false;
                $scope.report_full = true;

                $scope.stocks  = Object.keys(stocks).map(function(key) {
                    return stocks[key];
                });

                var grouped = _.groupBy($scope.stocks, function(stocks) {
                    return stocks.symbol;
                });

                $scope.consolidated = [];
                angular.forEach(grouped, function(stock, symbol) {
                    var status = 'Open';
                    var close = 0;
                    angular.forEach(stock, function(val){
                        if(val.status != 'Open')
                        {
                            status = 'Partial Open';
                        }
                        if(val.status == 'Close')
                            close++;
                    });

                    if(close == stock.length) {
                        status = 'Close';
                    }

                    var temp = {
                        symbol: symbol,
                        status: status,
                        avg_price: _.sumBy(stock, 'avg_price') / stock.length,
                        entry_date: _.min(stock, 'entry_date').entry_date,
                        exit_price: _.min(stock, 'exit_date').exit_price,
                        exit_date: _.min(stock, 'exit_date').exit_date,
                        open_profit: _.sumBy(stock, 'open_profit'),
                        booked_profit: _.sumBy(stock, 'booked_profit'),
                        total_profit: _.sumBy(stock, 'total_profit'),
                        profit_percent: _.sumBy(stock, 'profit_percent'),
                        open_stock_weight: _.sumBy(stock, 'open_stock_weight'),
                        close_stock_weight: _.sumBy(stock, 'close_stock_weight'),
                        total_stock_weight: _.sumBy(stock, 'total_stock_weight'),
                        net_roi: _.sumBy(stock, 'net_roi'),
                        stocks: stock
                    };
                    $scope.consolidated.push(temp);
                });

                $scope.sortBy = function(propertyName) {
                    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                    $scope.propertyName = propertyName;
                };
            });
        </script>

        @endpush

    @endforeach
@endif

<div id="PnLServiceModel" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Stock</h4>
            </div>
            <div class="modal-body">
                <p>Loading...</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="share_pnl" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Share Report to {{$user->name}} </h4>
            </div>
            <div class="modal-body">
                <div class="form-group"><label class="control-label" for="">message</label><textarea id="share_message" rows="5" class="form-control" placeholder="Additional Message... "></textarea></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" onclick="share_report()" class="btn btn-primary">Share <i class="icon-share2"></i></button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">

    $('.daterange-delivery_range').daterangepicker({
        applyClass: 'bg-info',
        cancelClass: 'btn-default',
        locale: {
            format: 'DD/MM/YYYY'
        }
    });
    // Enable Select2 select for the length option
    $('select').select2({
        minimumResultsForSearch: Infinity
    });

    $('.pnl_service_model').on('click', function(e){
        e.preventDefault();
        $('#PnLServiceModel').modal('show').find('.modal-title').text($(this).text())
        $('#PnLServiceModel').modal('show').find('.modal-body').html('<p>Loading...</p>').load($(this).attr('href'));
    }).on('show.bs.modal', function () {
        $(this).find('.modal-body').css({
            width:'auto', //probably not needed
            height:'auto', //probably not needed
            'max-height':'100%'
        });
    });

    var current_service = 0;

    $('.share_pnl_modal').on('click', function(e){
        e.preventDefault();
        current_service = $(this).data('service');
        $('#share_pnl').modal('show').find('.modal-body textarea').val('');
    });

    function share_report() {
        $.ajax({
            method: "POST",
            url: "{{ route('admin::clients.share_pnl', ['id'=> $id] ) }}",
            data: {service: current_service, message: $("#share_message").val()}
        })
        .done(function( data ) {
            $('#share_pnl').modal('hide');
            swal({
                type: 'success',
                title: 'Success ',
                html: 'Report shared Successgully'
            });

        });
    }
</script>

@endpush