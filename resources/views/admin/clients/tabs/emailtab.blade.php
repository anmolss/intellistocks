<div id="email" class="tab-pane fade in {{(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'email')?'active':''}}">
  	<div class="boxspace">
	  	{!! Form::open(['route' => 'admin::clients.send_email', 'class' => 'ui-form send_email_form']) !!}
			<h1>Send Email</h1>
			<div class="clearfix"></div>
			<br>
	  		<div class="form-group">
				<label class="col-lg-1 control-label">Email Id:</label>
				<div class="col-lg-11">
					<div class="form-control-static">{{$user->email}}</div>
					{!! Form::hidden('email', $user->email, ['class' => 'form-control']) !!}
					{!! Form::hidden('name', $user->name, ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="clearfix"></div>
			<br>
	  		<div class="form-group">
				<label class="col-lg-1 control-label">Subject:
					<span class="text-danger">*</span>
				</label>
				<div class="col-lg-11">
					{!! Form::text('subject', null, ['class' => 'form-control email_subject', 'placeholder' => 'Subject']) !!}
				</div>
			</div>
			<div class="clearfix"></div>
			<br>
			<div class="form-group">
				<label class="col-lg-1 control-label">Message:</label>
				<div class="col-lg-11">
					{!! Form::textarea('message', null, ['size' => '5x5', 'class' => 'form-control email_message', 'placeholder' => 'Enter Message Here', 'maxlength' => '140']) !!}
				</div>
			</div>
			<div class="clearfix"></div>
			<br>


			@include('admin.partials.inline_template', ['add_msg_to' => 'email_message', "type" => 'user_email', 'lable_col_size' => 1])


		<div class="email_spinner col-sm-1 text-center" style="display:none;">
                <i class="icon-spinner2 spinner"></i>
            </div>
			<div class="text-right">
				<button type="submit" class="btn btn-primary">Send <i class="icon-arrow-right14 position-right"></i></button>
			</div>
		{!! Form::close() !!}
  	</div>
</div>
@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function(){

	    jQuery( '.send_email_form' ).submit(function(e) {
	      	e.preventDefault();
	      	jQuery('.email_spinner').hide();
	    	var data = jQuery(this).serializeArray();
		    var $url = jQuery(this).attr('action');
		    jQuery.ajax({
		        type: 'POST',
		        url: $url,
		        data: data,
		        dataType: "json",
		        beforeSend: function() {
		            jQuery('.email_spinner').show();
		            jQuery('#email_submit_form').attr('disabled',true);
		        },
		        success: function(data) {
		            if(data.success == 1){
		              	swal("Email sent!", "Email has been successfully sent", "success");
		            } else {
		            	swal("Email Failed!", "Failed to send Email", "error");
		            }
		        },
		        error: function(data) {
		            console.log(data);
		        },
		        complete: function() {
		            jQuery('.email_spinner').hide();
		            jQuery('.send_email_form').find('.email_message').val('');
		            jQuery('.send_email_form').find('.email_subject').val('');
		            jQuery('#email_submit_form').attr('disabled',false);
		        }
		    });
	    });
	});
</script>
@endpush