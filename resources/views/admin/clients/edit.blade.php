@extends('admin.layout.app')

@section('page_title', 'Clients')
@section('page_subtitle', 'Edit Client')
@section('content')
<style type="text/css">
    .help-block{
        color: #c10000;
        font-weight: normal !important;
    }
</style>
<div class="panel panel-flat"> 
    <div class="panel-body"> 
        {!! Form::model($client, [
                'method' => 'PATCH',
                'route' => ['admin::clients.update', $client->id],
                ]) !!}

        <div class="row">
            <div class="form-group col-md-6">
                {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                 @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                {!! Form::email('email', null, ['class' => 'form-control']) !!}
                 @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-4">
                {!! Form::label('company_name', 'Company name:', ['class' => 'control-label']) !!}
                {!! Form::text('company_name',  null, ['class' => 'form-control']) !!}
                 @if ($errors->has('company_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('company_name') }}</strong>
                    </span>
                @endif
            </div>  
            <div class="form-group col-md-4">
                {!! Form::label('company_type', 'Company type:', ['class' => 'control-label']) !!}
                {!! Form::text('company_type',  null, ['class' => 'form-control']) !!}
                @if ($errors->has('company_type'))
                    <span class="help-block">
                        <strong>{{ $errors->first('company_type') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-md-4">
                {!! Form::label('fk_service_id', 'Service Interested In:', ['class' => 'control-label']) !!}
                <div class="col-md-12">
                    {!! Form::select('fk_service_id', $services, null, ['class' => 'form-control ' ] )!!}
                </div>
                 @if ($errors->has('fk_service_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fk_service_id ') }}</strong>
                    </span>
                @endif
            </div>
        </div>
       

        <div class="form-group">
            {!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
            {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
            @if ($errors->has('address'))
                <span class="help-block">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
            @endif
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                {!! Form::label('zipcode', 'Zipcode:', ['class' => 'control-label']) !!}
                {!! Form::text('zipcode', null, ['class' => 'form-control']) !!}
                @if ($errors->has('zipcode'))
                    <span class="help-block">
                        <strong>{{ $errors->first('zipcode') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-md-8">
                {!! Form::label('city', 'City:', ['class' => 'control-label']) !!}
                {!! Form::text('city', null, ['class' => 'form-control']) !!}
                @if ($errors->has('city'))
                    <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                    </span>
                @endif
            </div>
        </div>
         <div class="row">
            <div class="form-group col-md-6">
        
                {!! Form::label('primary_number', 'Primary Number:', ['class' => 'control-label']) !!}
                {!! Form::text('primary_number',  null, ['class' => 'form-control']) !!}
                @if ($errors->has('primary_number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('primary_number') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-md-6">
                {!! Form::label('secondary_number', 'Secondary Number:', ['class' => 'control-label']) !!}
                {!! Form::text('secondary_number',  null, ['class' => 'form-control']) !!}
                @if ($errors->has('secondary_number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('secondary_number') }}</strong>
                    </span>
                @endif
            </div>  
        </div>
        {!! Form::submit('Update Client', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
     </div>
</div>        
@stop