@extends('admin.layout.app')

@section('page_title', 'Clients')
@section('page_subtitle', 'List of Leads')
@section('content')
<form class="form-horizontal" action="#" id="customer-frm-filter" method="GET">
    <div class="panel panel-flat panel-collapsed">
        <div class="panel-heading">
            <h6 class="panel-title text-semibold"><i class="icon-equalizer position-left"></i> Filters</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse" class=""></a></li>
                </ul>
            </div>
        <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('service', 'Service:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::select('service', $services, null, ['class' => 'form-control select service_type', 'multiple' => 'multiple', 'id' => 'services'  ] ) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('plan', 'Plan:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                    {!! Form::select('plan', $plans, null, ['class' => 'form-control select select_plans', 'multiple' => 'multiple', 'id' => 'plans'] ) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary btn-block"><span class="hidden-sm">Filter </span><i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('select_filter', 'Select Filter:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::select('select_filter', ['created_dates' => 'Creation Date', 'activation_dates' => 'Activation Date', 'expire_dates' => 'Expiry Date'], null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('filter_range', 'Filter Date:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::text('filter_range', null, ['class' => 'form-control daterange-filter_range']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <a title="Reset Filter!" id="reset_search" href="javascript:" class="btn btn-success btn-rounded btn-block"><span class="hidden-sm">Reset </span><i class="icon-spinner11 position-right"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('status', 'Select Status:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::select('status', ['' => 'Select Status', 'Active' => 'Active', 'Pending' => 'Pending', 'Expired' => 'Expired', 'Suspended' => 'Suspended'], null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</form>
    <div class="container-fluid" style="background:white; margin-bottom:20px;">
        <div class="table-responsive">          
            <table class="table">
                <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    {{--  <th>Service</th>  --}}
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <span style="display:none">{{$i=0}}</span>
                @foreach ($clients as $id=>$state) 
                    <tr>
                        <td>{{$i+=1}}</td>
                    <td>{{$state->first_name}} {{$state->last_name}}</td>
                        <td>{{$state->email}}</td>
                        <td>{{$state->primary_number}}</td>
                        {{--  <td>
                            @foreach ($services as $sid=>$sfield)
                                @if($sfield->id == $state->fk_service_id)
                                    {{$sfield->name}}
                                @endif
                            @endforeach
                        </td>  --}}
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
<div class="panel panel-flat" style="display:none;">
    <div  class="panel-body" >
       <table class="table table-hover " id="customers-table">
            <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Service</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {{--  <tr>
                    <td>1</td>
                    <td>Name</td>
                    <td>Email</td>
                    <td>Mobile</td>
                    <td>Service</td>
                    <td>Action</td>
                </tr>
                @foreach ($clients as $id=>$state) 
                    <tr>
                        <td>$state->first_name</td>
                        <td>$state->email</td>
                        <td>$state->mobile</td>
                    </tr>
                @endforeach  --}}
            </tbody>
        </table>
    </div>
</div>
@stop

@push('scripts')
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.service_type').on('change', function(){
            var services = jQuery(this).val();
            jQuery.ajax({
                type: 'POST',
                url: '{{url('getMultiplePlans')}}',
                data: {'services':services},
                dataType: "json",
                success: function(data) {
                    jQuery(".select_plans").empty();
                    jQuery.each(data[0], function(key,value) {
                        jQuery(".select_plans").append(jQuery("<option></option>").attr("value", key).text(value));
                        jQuery('select').select2({
                            minimumResultsForSearch: Infinity,
                        });
                    });
                }
            });
        });
    });
</script>
<script>
$(function() {
    $('.daterange-filter_range').daterangepicker({
        applyClass: 'bg-info',
        cancelClass: 'btn-default'
    });
    $('.daterange-filter_range').val('');
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [{ 
            orderable: false,
            width: '100px'
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    var oTable = $('#customers-table').DataTable({
        processing: true,
        serverSide: true,
        "order": [ [0, 'desc'] ],
        ajax: {
            type: 'POST',//change to POST due to parameter limit exceed
            url: '{!! route('admin::clients.customerData') !!}',
            data: function (d) {
                d.search = $('.dataTables_filter input[type=search]').val();
                d.service = $('#services').val();
                d.plan = $('#plans').val();
                d.status = $('#status').val();
                d.select_filter = $('#select_filter').val();
                d.filter_range = $('#filter_range').val();
            }
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'namelink', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'mobile', name: 'mobile' },
            { data: 'service_name', name: 'service_name' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false},
        ]
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Name Search...');

    $('.dataTables_filter input[type=search]').on('input',function(e){
        oTable.draw();
    });

    $('#customer-frm-filter').on('submit', function(e) {
        e.preventDefault();
        oTable.draw();
    });
   
    $('#reset_search').on('click', function(e) {
        oTable.search( '' );
        $('#customer-frm-filter').trigger("reset");
        $('.select').select2();
        oTable.draw();
    });

    // Enable Select2 select for the length option
    $('select').select2({
        minimumResultsForSearch: Infinity
    });

    jQuery(document).delegate(".add_to_watch", 'click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var url = jQuery(this).attr('href');
        var watch_status = jQuery(this).attr('data_watch_status');
        if(watch_status == 1){
            var question = 'You are about to add this user to watchlist';
        } else {
            var question = 'You are about to remove this user from watchlist';
        }
        swal({   
            title: "Are You Sure?",   
            text: question,   
            type: "info",   
            showCancelButton: true,   
            closeOnConfirm: false,   
            showLoaderOnConfirm: true, 
        }, function(){   
            jQuery.ajax({
                type: 'POST',
                url: url,
                data: {watch_status:watch_status},
                dataType: "json",
                success: function(data) {
                    if (data.response == 1) {
                        swal({   
                            title: "Done",   
                            text: data.msg,   
                            type: "success", 
                            closeOnConfirm: true 
                        }, 
                        function(){   
                            oTable.draw();
                        });
                    } else {
                        swal("Error!", "Action Failed", "error");
                    }
                }
            }); 
        });
    });
});
</script>
@endpush