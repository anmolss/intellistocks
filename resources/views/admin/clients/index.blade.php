@extends('admin.layout.app')

@section('page_title', 'Clients')
@section('page_subtitle', 'List of Leads')
@section('content')
<div class="panel panel-flat">
    <div  class="panel-body" >
       <table class="table table-hover " id="clients-table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Company</th>
                    <th>Email</th>
                    <th>Number</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>   

@stop

@push('scripts')
<script>
$(function() {
     $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [{
            orderable: false,
            width: '100px'
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });
    $('#clients-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin::clients.data') !!}',
        columns: [

            { data: 'namelink', name: 'name' },
            { data: 'company_name', name: 'company_name' },
            { data: 'email', name: 'email' },
            { data: 'primary_number', name: 'primary_number' },

             { data: 'actions', name: 'actions', orderable: false, searchable: false},


        ]
    });
     // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');


    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });
});
</script>
@endpush