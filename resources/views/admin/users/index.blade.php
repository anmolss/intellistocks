@extends('admin.layout.app')

@section('page_title', 'Employees')
@section('page_subtitle', 'List of employee')

@section('content')

  {!! BootForm::open()->method('get') !!}
        {!! BootForm::bind($data) !!}
    <div class="panel panel-flat panel-collapsed">
        <div class="panel-heading">
            <h6 class="panel-title text-semibold"><i class="icon-equalizer position-left"></i> Filters</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse" class=""></a></li>
                </ul>
            </div>
        <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                         <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">  
                            <div class="col-lg-9">
                            {!! BootForm::text('Name', 'name')->id('name') !!}
                            </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">  
                            <div class="col-lg-9">
                            {!! BootForm::text('City', 'city')->id('city')!!}
                            </div>
                                </div>
                            </div>
                         </div>
                         <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                  <div class="col-lg-9">
                                  {!! BootForm::select('Departments', 'department',$departments)->addClass('form-control select')->id('department') !!}
                                  </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                  <div class="col-lg-9">
                                    {!! BootForm::select('Status', 'status',['' => 'Select...', "active" => 'Active', 'inactive' => 'Inactive'])->addClass('form-control select')->id('status') !!}
                                  </div>
                                </div>
                            </div>

                             <div class="col-sm-4">
                                {!! BootForm::submit('Search <i class="icon-arrow-right14 position-right"></i>')->addClass('btn btn-primary col-sm-3 col-sm-offset-2') !!}
                                <a href="{{Request::url()}}" class="btn btn-success btn-rounded col-sm-3 col-sm-offset-2" id="reset">Reset<i class="icon-spinner11 position-right"></i></a>
                             </div>
                         </div>
             </fieldset>
                </div>
            </div>
        </div>
    </div>
 {!! BootForm::close() !!}

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="over-title margin-bottom-15">
                Users
                @permission('create-users')
                    <a href="{{route('admin::users.create')}}" class="btn btn-primary pull-right">Add New <i class="fa fa-plus"></i> </a>
                @endpermission
            </h5>
        </div>
        <div class="panel-body">
            <table class="table datatable-dom-position" id="user_table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Username</th>
                    <th>Created On</th>
                    <th>Status</th>
                    <th class="" >Actions</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    {!! BootForm::open()->delete()->id('delete-helper-form')!!}
    {!! BootForm::close() !!}

@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
    $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                width: '100px'
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            },
            language: {
                processing: "<img src='{{asset('build/img/jquery.easytree/loading.gif')}}'> Carregando..."
            }
        });

          var table =   $('#user_table').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [ [4, 'desc'] ],
                "ajax" : {
                    "url" : '{{ url('users/alluser?'.$appedable_query) }}',
                    "type" : "POST"//change to POST due to request paramter limit exceeded
                },
                //"ajax": '{{ url('users/alluser?'.$appedable_query) }}',
                "columns": [
                    { data: 'name', name: 'name' },
                    { data: 'official_emailid', name: 'official_emailid' },
                    { data: 'mobile', name: 'mobile' },
                    { data: 'username', name: 'username' },
                    { data: 'created_at', name: 'created_at', },
                    { data: 'status', name: 'status' },
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });

            // Add placeholder to the datatable filter option
            $('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');


            // Enable Select2 select for the length option
            $('.dataTables_length select').select2({
                minimumResultsForSearch: Infinity,
                width: 'auto'
            });

    </script>
@endsection

@section('scripts')
    @parent

    <script type="text/javascript">
            function del_action(src){
                if(confirm('Are you sure you want to detele this?')){
                    var form = $('#delete-helper-form').attr('action',src);
                    form.submit();
                } else {
                    return false;
                }
            }

            $('.select').select2({
             });
    </script>
@endsection

