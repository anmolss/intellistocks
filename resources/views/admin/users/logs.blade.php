@extends('admin.layout.app')

@section('page_title', 'User')
@section('page_subtitle', 'Logs')


@section('content')

				<div class="panel panel-flat">
			        <div class="panel-heading">
			           
			        </div>
			        <div class="panel-body">
			            <table class="table table-striped table-hover" id="sample-table-2">
			                <thead>
			                    <tr>
									<tr>
										<th>#</th>
										<th>Activities</th>
										<th>Created</th>
									</tr>
							</thead>
								<tbody>
								@if(!empty(count($item->logs) > 0))
									@foreach($item->logs as $key => $activity)
									<?php //print_r($activity); ?>
										<tr>
											<td>{{$key + 1}}</td>
											<td>{{$activity->text}}</td>
											<td>{{$activity->created_at->format(config('rothmans.date_format'))}}</td>
											
										</tr>
									@endforeach
								@else
									
									 <tr>
                        				<td colspan="6" align="center">No Activity logs Found</td>
                    				</tr>
								@endif
								</tbody>
						</table>
					
					</div>
				</div>
			
@endsection