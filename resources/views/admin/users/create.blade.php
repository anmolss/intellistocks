@extends('admin.layout.app')

@section('page_title', 'Employee')
@section('page_subtitle', 'Create new User')


@section('content')

    {!! BootForm::open()->post()->action(route('admin::users.store'))->novalidate() !!}
    	<!-- {!! BootForm::bind($default_values) !!} -->
        @include('admin.users._form', ['submitBtnText' => "Add New Employee"])

    {!! BootForm::close() !!}

@endsection