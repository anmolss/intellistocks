@extends('admin.layout.app')

@section('page_title', 'User')
@section('page_subtitle', 'Change Password')


@section('content')
    @include('admin.partials.toolbar')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Password <small class="text-bold">Settings</small>
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
    <div class="content">

        <!-- User profile -->
        <div class="row">
            <div class="col-lg-9">
                <div class="tabbable">
                    <div class="tab-content">
    <div id="update">
    <!-- Account settings -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {!! BootForm::open()->post()->action(route('admin::users.changepassword', ['users' => $item->id] ))->multipart()->novalidate() !!}
            
            @if(!Auth::user()->hasRole('super-admin'))
                <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        {!! BootForm::password('Current password <span class="text-danger">*</span>', 'c_password')->required()!!}

                        @if (Session::has('message'))
                            <p class="help-block">{{ Session::get('message') }}</p>
                        @endif

                    </div>
                </div>
            </div>
            @endif
            

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        {!! BootForm::password('New password <span class="text-danger">*</span>', 'password')->required()!!}

                    </div>

                    <div class="col-md-6">
                        {!! BootForm::password('Repeat password <span class="text-danger">*</span>', 'password_confirmation')->required() !!}

                    </div>
                </div>
            </div>

            <div class="text-right">
                {!! BootForm::submit('Edit password<i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right') !!}
            </div>
            {!! BootForm::close() !!}

        </div>
    </div>
    <!-- /account settings -->
</div>
                    </div>
                    </div>
                </div>

            <div class="col-lg-3">
            @include('admin.users.thumbnail')
            </div>


            </div>
@endsection