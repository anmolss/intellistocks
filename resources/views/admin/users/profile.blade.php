<div class="tab-pane fade in" id="profile">

    <!-- Profile info -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Profile information</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        {!! BootForm::text('Username', 'username')->value($item->username)->required()!!}
                    </div>
                    <div class="col-md-6">
                        {!! BootForm::text('Name ', 'name')->value($item->name)->required()!!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        {!! BootForm::text('Address 1', 'address1')->value($item->address1)->required()!!}
                    </div>
                    <div class="col-md-6">
                        {!! BootForm::text('Address 2', 'address2')->value($item->address2)->required()!!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-4">
                        {!! BootForm::text('City', 'city')->value($item->city)->required()!!}
                    </div>
                    <div class="col-md-4">
                        {!! BootForm::text('State', 'state')->value($item->state)->required()!!}
                    </div>
                    <div class="col-md-4">
                        {!! BootForm::text('ZIP code', 'zip_code')->value($item->zip_code)->required()!!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        {!! BootForm::text('Email ', 'email')->value($item->email)->required()!!}
                    </div>
                    <div class="col-md-6">
                        {!! BootForm::text('Mobile ', 'mobile')->value($item->mobile)->required()!!}
                    </div>
                </div>
            </div>

            @if(Route::getFacadeRoot()->current()->uri() == 'users/create')
                @include('admin.users.password')
            @endif

        </div>

    </div>
    <!-- /profile info -->

</div>