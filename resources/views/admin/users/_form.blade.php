<div class="content">
    <!-- User profile -->
    
    <div class="row">
        <div class="col-lg-9">
            <div class="tabbable">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="settings">
                        <!-- Profile info -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h6 class="panel-title">Profile information</h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! BootForm::text('Name <span class="text-danger">*</span>', 'name')->required()!!}
                                            </div>
                                          
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! BootForm::text('Address 1', 'address1')->required()!!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! BootForm::text('Address 2', 'address2')->required()!!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                         <div class="col-md-4">
                                                {!! BootForm::select('State', 'state', $states)->id('state_list')->addClass('selectbox multiple-select')->required()!!}
                                            </div>
                                            <div class="col-md-4">
                                                {!! BootForm::select('City', 'city', $cities)->id('city_list')->addClass('selectbox multiple-select')->required()!!}
                                            </div>
                                            <div class="col-md-4">
                                                {!! BootForm::select('ZIP code', 'zip_code', $zipcode)->id('zip_code')->addClass('selectbox multiple-select')->required()!!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                         <label class="display-block">Gender</label>
                                             <label class="radio-inline">
                                                    {!! BootForm::radio('Male', 'gender')->value(1)->addClass('styled')->checked() !!}
                                             </label>
                                             <label class="radio-inline">
                                             {!! BootForm::radio('Female', 'gender')->value(0)->addClass('styled') !!}
                                             </label>
                                             </div>
                                             
                                          
                                        <div class="col-md-6">
                                             <label class="display-block">Marital Status</label>
                                             <label class="radio-inline">
                                                    {!! BootForm::radio('Married', 'marital_status')->value(1)->addClass('styled')->checked() !!}
                                             </label>
                                             <label class="radio-inline">
                                                    {!! BootForm::radio('Unmarried', 'marital_status')->value(0)->addClass('styled') !!}
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                            
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! BootForm::text('Date of birth', 'dob')->placeholder('Date of birth')->required()->addGroupClass('daterange-single')->id('dob') !!}

                                            </div>
                                            <div class="col-md-6">
                                                @if(isset($item) && $item->marital_status == 0)
                                                    {!! BootForm::text('Date of Anniversary', 'date_of_anniversary')->placeholder('Date of Anniversary')->required()->addGroupClass('daterange-single')->id('anniversary')->disabled() !!}
                                                @else
                                                    {!! BootForm::text('Date of Anniversary', 'date_of_anniversary')->placeholder('Date of Anniversary')->required()->addGroupClass('daterange-single')->id('anniversary') !!}
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! BootForm::text('Email', 'email')->required()!!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! BootForm::text('Mobile <span class="text-danger">*</span>', 'mobile')->required()!!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                {!! BootForm::text('Facebook', 'facebook')->required()!!}
                                            </div>
                                            <div class="col-md-4">
                                                {!! BootForm::text('Twitter', 'twitter')->required()!!}
                                            </div>
                                            <div class="col-md-4">
                                                {!! BootForm::text('Linkedin', 'linkedin')->required()!!}
                                            </div>
                                        </div>
                                    </div>

                                    
                                    @if(Route::getFacadeRoot()->current()->uri() == 'users/create')
                                      @include('admin.users.password')
                                    @endif

                            </div>
                        </div>

                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h6 class="panel-title">Basic Information</h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>
                        <div class="panel-body">
                            
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    {!! BootForm::text('Official Email <span class="text-danger">*</span>', 'official_emailid')->required()!!}
                                </div>
                                <div class="col-md-6">
                                    {!! BootForm::text('Official Ph no. <span class="text-danger">*</span>', 'official_mobile')->required()!!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                             <div class="col-md-6">
                                {!! BootForm::select('Department <span class="text-danger">*</span> <a href="#" class="pull-right" data-toggle="modal" data-target="#modal_theme_info"> <i class=" icon-plus-circle2"></i></a>', 'department',$departments)->addClass('multiple-select')->defaultValue('')->multiple()->id('select1')->required()!!}

                             </div>
                                <div class="col-md-6">
                                    {!! BootForm::select('Designation <span class="text-danger">*</span><a href="#" data-toggle="modal"  class="pull-right"  data-target="#modal_theme_info1"><i class="icon-plus-circle2"></i></a>', 'designation',$designations)->addClass('selectbox multiple-select')->id('select2')->required()!!}
                                     
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    {!! BootForm::select('Qualification', 'qualification', ['Graduation', 'Post graduation', 'Certification '])->addClass('selectbox') !!}
                                </div>
                                <div class="col-md-6">
                                    {!! BootForm::select('Skills', 'skills',$skill_list)->addClass('tags-multiple')->multiple() !!}
                                </div>


                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    {!! BootForm::select('Reporting to', 'reporting_to',$report_list)->id('reporting_to')->addClass('multiple-select')->required()!!}
                                </div>
                                <div class="col-md-6">
                                    {!! BootForm::text('Date of joining ', 'date_of_joining')->placeholder('Date of joining')->required()->addGroupClass('daterange-single')->id('joining_date') !!}
                                </div>
                            </div>
                         </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    {!! BootForm::select('Status', 'status',['1' => 'Active', '0' => 'Inactive'],'select') !!}
                                </div>
                            </div>
                        </div>

                         @if(Route::getFacadeRoot()->current()->uri() == 'users/create')
                          <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! BootForm::textarea('Note <span class="text-danger">*</span>', 'note_msg')->rows(3)->cols(1)->placeholder('Enter your message...') !!}
                                </div>
                            </div>
                        </div>
                        @endif
                        
                         @if(Route::getFacadeRoot()->current()->uri() != 'users/create')
                        <div class="text-right" style="margin-right: 178px;margin-bottom: -45px;">
                            <label class="display-block text-semibold" style="margin-right: 136px;">Notify By</label>
                            <label class="checkbox-inline checkbox-right">
                                {!! BootForm::inlineCheckbox('SMS', 'by_sms')->addClass('styled')->checked()!!}
                            </label>

                            <label class="checkbox-inline checkbox-right">
                                 {!! BootForm::inlineCheckbox('Email', 'by_email')->addClass('styled')->checked()!!}
                            </label>
                        </div>
                         @endif
                        <div class="text-right">
                             {!! BootForm::submit($submitBtnText . ' <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right') !!}
                        </div>

                     
                            </div>
                        </div>
                    @if(Route::getFacadeRoot()->current()->uri() != 'users/create')            
                    <div class="row">
                      <div class="col-md-12">
                        <div class="timeline timeline-left">
                            <div class="timeline-row">
                                <div class="timeline-icon">
                                    <div class="bg-info-400">
                                        <i class="icon-comment-discussion"></i>
                                    </div>
                                </div>

                                <div class="panel panel-flat timeline-content">
                                    <div class="panel-heading">
                                        <h6 class="panel-title">Notes</h6>    
                                    </div>

                                    <div class="panel-body">
                                        <ul class="media-list chat-list content-group" id="notes_list">
                                            @foreach($item->notes as $notes)
                                                <li class="media">
                                                    <div class="media-left">
                                                         <img src="@if(!empty($notes->user->image)){{ asset('uploads/user/images/'.$notes->user->id.'/'.$notes->user->image) }}@else {{asset('images/placeholder.jpg')}} @endif" class="img-circle" id="myfile" alt="" >
                                                    </div>

                                                    <div class="media-body">
                                                        <div class="media-content">{{$notes->note}}  </div>
                                                        <span class="media-annotation display-block mt-10">Send by: {{$notes->user->name}}</span>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                        {!! BootForm::textarea('Note <span class="text-danger">*</span>', 'note_msg')->rows(3)->cols(1)->placeholder('Enter your message...') !!}
                                        <span id="notes_response"></span>

                                        <div class="row">
                                            <div class="col-xs-6">
                                            </div>

                                            <div class="col-xs-6 text-right">
                                                {!! BootForm::submit('Add Note <i class="icon-circle-right2"></i>')->id('note_submit')->addClass('btn-primary pull-right') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    <!-- /Notes list -->
                    @endif

                    </div>
                    @if(Route::getFacadeRoot()->current()->uri() != 'users/create')       
                    <div class="tab-pane fade in" id="profile">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <fieldset class="step" id="step1">
                                <h6 class="form-wizard-title text-semibold">
                                    <span class="form-wizard-count"><i class="icon-user position-left"></i></span>
                                    Personal info
                                </h6>
                                <div class="panel-body profile-section">
                                    <div class="row">
                                        <div class=" col-md-9 col-lg-9 "> 
                                          <table class="table table-user-information">
                                            <tbody>
                                              <tr>
                                                <td class="content-group text-semibold">Username:</td>
                                                <td>{{$item->username}}</td>
                                              </tr>
                                              <tr>
                                                <td class="content-group text-semibold">Email:</td>
                                                <td>{{$item->email}}</td>
                                              </tr>
                                              <tr>
                                                <td class="content-group text-semibold">Phone no.</td>
                                                <td>{{$item->mobile}}</td>
                                              </tr>
                                              <tr>
                                                <td class="content-group text-semibold">Date of birth</td>
                                                <td>{{date(config('rothmans.date_format'),strtotime($item->dob))}}</td>
                                              </tr>
                                              <tr>
                                                <td class="content-group text-semibold">Gender</td>
                                                <td>{{($item->gender? "Male" : "Female")}}</td>
                                              </tr>
                                                <tr>
                                                <td class="content-group text-semibold">Address</td>
                                                <td>{{$item->mobile}}</td>
                                              </tr>
                                             
                                            </tbody>
                                          </table>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                     <div class="col-md-12">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <div class="heading-elements">
                                        <ul class="icons-list">
                                            <li><a data-action="collapse"></a></li>
                                            <li><a data-action="reload"></a></li>
                                            <li><a data-action="close"></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="tabbable tab-content-bordered">
                                        <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
                                            <li class="active"><a href="#bordered-justified-tab1" data-toggle="tab">Logs</a></li>
                                            <li><a href="#bordered-justified-tab2" data-toggle="tab">--</a></li>
                                        </ul>

                                        <div class="tab-content">
                                            <div class="tab-pane has-padding active" id="bordered-justified-tab1">

                                                            <div class="table-responsive">
                                                                <table class="table">
                                                                   <!--  <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>Product</th>
                                                                            <th>Payment Taken</th>
                                                                            <th>Status</th>
                                                                        </tr>
                                                                    </thead> -->
                                                                    <tbody>
                                                                    @foreach($item->activities as $key => $activity)
                                                                        <tr>
                                                                            <td>{{$key + 1}}</td>
                                                                            <td>{{$activity->name}}</td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>

                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                
                                            </div>

                                            <!-- <div class="tab-pane has-padding" id="bordered-justified-tab2">
                                                Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid laeggin.
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>

                    </div>
                    @endif
                    </div>
            </div>
        </div>
       <!--  </div> -->
         <div class="col-lg-3">
         @if(isset($item->id))
            @include('admin.users.thumbnail')
        @endif
            <!-- Roles -->
                <div class="panel panel-flat" ng-controller="checkboxController">
                    <div class="panel-body">
                        <div class="tabbable">
                            <ul class="nav nav-tabs nav-tabs-solid nav-tabs-component">
                                <li class="active"><a href="#solid-rounded-tab1" data-toggle="tab">Role</a></li>
                                <li><a href="#solid-rounded-tab2" data-toggle="tab" >Permission</a></li>
                            </ul>

                            <div class="tab-content" >
                                <div class="tab-pane active" id="solid-rounded-tab1">
                                    {{--{{ Html::ul($roles) }}--}}
                                    <ul class="media-list media-list-linked pb-5">
                                        @foreach($roles as $role)
                                            <li class="media-header">
                                                @if(in_array($role->id , (array) $user_roles))
                                                    {!! BootForm::inlineCheckbox($role->display_name, 'roles[]')->addClass('styled')->value($role->id)->checked()->attribute('ng-model', 'selected_roles['.$role->id.']')->attribute('ng-change', 'stateChanged()') !!}
                                                @else
                                                    {!! BootForm::inlineCheckbox($role->display_name, 'roles[]')->addClass('styled')->value($role->id)->attribute('ng-model', 'selected_roles['.$role->id.']')->attribute('ng-change', 'stateChanged()') !!}
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>

                                <div class="tab-pane" id="solid-rounded-tab2">
                                    <ul>
                                        @foreach( $permissions as $group => $controllers)
                                            <li class="folder expanded">{{ $group }}
                                                <ul>
                                                @foreach( $controllers as $controller => $c_perms)
                                                        <li class="expanded">{{ $controller }}
                                                            <ul class="list-unstyled">
                                                    @foreach( $c_perms as $perm)
                                                        <li>
                                                        @if(in_array($perm->id , (array) $role_perms))
                                                           {!! BootForm::inlineCheckbox($perm->display_name, 'perms[]')->addClass('styled')->value($perm->id)->checked()->attribute('ng-checked', 'perms['.$perm->id.']')->attribute('ng-disabled', 'perms_dis['.$perm->id.']') !!}
                                                        @else
                                                        {!! BootForm::inlineCheckbox($perm->display_name, 'perms[]')->addClass('styled')->value($perm->id)->attribute('ng-checked', 'perms['.$perm->id.']')->attribute('ng-disabled', 'perms_dis['.$perm->id.']')  !!}
                                                        @endif
                                                        </li>
                                                    @endforeach
                                                            </ul>
                                                @endforeach
                                                </ul>

                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
                
            <!-- /roles -->

        </div>
        <div id="modal_theme_info" class="modal fade">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h6 class="modal-title">New Department</h6>
                </div>
                {!! BootForm::open()->post()->novalidate() !!}
                <div class="modal-body">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        {!! BootForm::text('Department name:', 'dept_name')->required() !!}
                    </div>

                    <div class="form-group">
                        {!! BootForm::textarea('Department description:', 'dept_description') !!}
                         <span id="dept_msg">
                                <strong></strong>
                         </span>
                    </div>
                </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
              {!! BootForm::submit('Create New Department')->addClass('btn btn-info')->id('add_dept') !!}
            </div>
            {!! BootForm::close() !!}
          </div>
        </div>
      </div>
      <div id="modal_theme_info1" class="modal fade">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h6 class="modal-title">New Designation</h6>
                </div>
                {!! BootForm::open()->post()->novalidate() !!}
                <div class="modal-body">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        {!! BootForm::text('Designation name:', 'desg_name')->required() !!}
                    </div>
                    <div class="form-group">
                    {!! BootForm::select('Department <span class="text-danger">*</span>', 'desg_department_id',$departments)->id('desg_department_id')->addClass('selectbox')->required()!!}
                    </div>
                    <div class="form-group">
                        {!! BootForm::textarea('Designation description:', 'desg_description') !!}
                         <span id="desg_msg">
                                <strong></strong>
                            </span>
                    </div>
                </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
              {!! BootForm::submit('Create New Designation')->addClass('btn btn-info')->id('add_desg') !!}
            </div>
            {!! BootForm::close() !!}
          </div>
        </div>
      </div>

       
    </div>
    <!-- /user profile -->
</div>

@section('scripts')
    @parent
    <script type="text/javascript">

    $(".tags-multiple").select2({
     tags: true,
    });
    $(".multiple-select").select2({

    });
    
    $("#select1").change(function() {
        var arr = new Array();  
        $( "#select1 option:selected" ).each(function() {
           arr.push ( $(this).val());  
        });
        $('#select2').html('')
      getDesignations(arr);
    });

    $('document').ready(function(){
        $("#select1").trigger('change');
    });

      function getDesignations(id)
        {
            if(id.length == 0)
            {
                return ;
            }

            var values = {!! $designations->toJson() !!};
            @if(isset($item->designation))
                var default_designation = {{$item->designation}};
            @elseif(old('designation'))
                var default_designation = {{old('designation')}};
            @else
            var default_designation = '';
            @endif
            $.ajax({
                method: 'GET',
                url: '{!! route('admin::users.designation') !!}',
                data: {'id' : id},
                datatype:'JSON',
            }).success(function (data) {
                $('#reporting_to').html('');
                 $('#select2').html('');
                 console.log(data);
                 var json = data;
                 if(json.list.length != 0)
                 {
                    for (var i=0;i<json.list.length;++i)
                    {
                        var selected = '';
                          if(default_designation !== '' &&  default_designation == json.list[i].id)
                           {
                             var selected = 'selected';
                           }
                         $('#select2').append('<option ' + selected + ' value="' + json.list[i].id + '">' + json.list[i].name + '</option>');
                    }

                  }
                  if(json.users.length != 0)
                 {
                    for (var i=0;i<json.users.length;++i)
                    {
                         $('#reporting_to').append('<option ' + selected + ' value="' + json.users[i].id + '">' + json.users[i].name + '</option>');
                    }

                  } 
                  else{
                    $('#reporting_to').html('');
                  }

                  $("#select2").select2("destroy").select2();
            });
        } 
    
    app.controller('checkboxController',function($scope, $timeout){
        $scope.roles = {!! $role_perms->toJson() !!};
        $scope.perms = [];
        $scope.perms_dis = [];
        $scope.selected_roles = [];
        @foreach((array) $user_roles as $role) $scope.selected_roles[{{ $role }}] = true; @endforeach

        $scope.stateChanged = function () {
            $scope.perms = $scope.perms_dis = [];
            _.forEach($scope.selected_roles, function (n, key) {
                if (n) {
                    _.forEach($scope.roles[key], function (p, id) {
                        $scope.perms[p] = $scope.perms_dis[p] = true;
                    });
                }
            });
            $timeout(jQuery.uniform.update, 0);
        };

        $scope.stateChanged();

        @foreach((array) $user_perms as $perm) $scope.perms[{{ $perm }}] = true; @endforeach

        $timeout(jQuery.uniform.update, 0);
    });

    </script>


    <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>

    <script type="text/javascript">

        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var dob = $('#dob').datepicker({
            'format' : 'yyyy-mm-dd',
            onRender: function(date) {
                return date.valueOf() > now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            dob.hide();
        }).data('datepicker');
        
        var anniversary = $('#anniversary').datepicker({
            'format' : 'yyyy-mm-dd',
            onRender: function(date) {
                return date.valueOf() <= dob.date.valueOf() && date.valueOf() >= now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            anniversary.hide();
        }).data('datepicker');

        var joining = $('#joining_date').datepicker({
            'format' : 'yyyy-mm-dd',
            onRender: function(date) {
                return date.valueOf();
            }
        }).on('changeDate', function(ev) {
            joining.hide();
        }).data('datepicker');

        $("#note_submit").on('click', function(e) {
        e.preventDefault();
        var note = $("textarea[name='note_msg']").val();
         @if(isset($item->id))
            var id = {{$item->id}};
         @else
            var id = '';
        @endif
        $.ajax({
                    method: 'GET',
                    url: '{!! route('admin::users.note') !!}',
                    data: {'note' : note,'id': id},
                    datatype:'JSON',
                }).success(function (data) {
                    $("#notes_response").removeClass();
                   var json = $.parseJSON(data);
                   if(json.status == 1)
                    {
                        $('#notes_response').html('Notes added successfully');
                        $("#notes_response").addClass("alert-info");
                    }
                    else{
                        $('#notes_response').html('Please add some notes');
                        $("#notes_response").addClass("alert-danger");
                    }
                   $("textarea[name='note_msg']").val('');
                   if(json.notes.user.image != null)
                    var image = '{{asset('uploads/user/images')}}'+ '/' +json.notes.user.id + '/' +json.notes.user.image;
                   else
                    var image = '{{asset('images/placeholder.jpg')}}';

                      setTimeout(function(){
                        $('#notes_response').html('');
                      }, 2000)  

                      $('#notes_list').append('<li class="media"><div class="media-left"><img src="' + image + '" class="img-circle" id="myfile" alt=""></div><div class="media-body"><div class="media-content">'+ json.notes.note + '</div><span class="media-annotation display-block mt-10">Send by: '+ json.notes.user.name + '</span></div></li>');
                });
        });

        $("#add_dept").on('click', function(e) {
        e.preventDefault();
        var name = $("input[name='dept_name']").val();
        var description = $("textarea[name='dept_description']").val();
        $.ajax({
                    method: 'GET',
                    url: '{!! route('admin::users.add') !!}',
                    data: {'name' : name,'description': description},
                    datatype:'JSON',
                }).success(function (data) {
                    var json = $.parseJSON(data);
                    if(json.status == 1)
                    {
                        $('#dept_msg strong').html('Department added successfully');
                        $("#dept_msg").addClass("alert-info");
                    }
                    else{
                        $('#dept_msg strong').html('Name field is required');
                        $("#dept_msg").addClass("alert-danger");
                    }
                     $('#select1').append('<option value="' + json.dept.id + '" selected>' + json.dept.name + '</option>').select2().trigger('change');
                     $('#desg_department_id').append('<option value="' + json.dept.id + '" selected>' + json.dept.name + '</option>').select2().trigger('change');
                      setTimeout(function () {
                        $('#dept_msg strong').html('');
                        $("input[name='dept_name']").val('')
                        $("textarea[name='dept_description']").val('');
                        $('#modal_theme_info .close').click();
                         }, 1000);

                });
        });

$( "#username" ).change(function() {
    var username = $("input[name='username']").val();
    var name = $("input[name='name']").val();
    $.ajax({
            method: 'GET',
            url: '{!! route('admin::users.check') !!}',
            data: {'username' : username, 'name' : name},
            datatype:'JSON',
            }).success(function (data) {
                var json = $.parseJSON(data);
                if(json.status == 'success')
                {
                    // console.log(json.message.username[0]);
                    $('#username_list li').show();
                    $("#user_exist").text(json.message.username[0]);
                   for (var i=0;i<json.username.length;++i)
                    {
                        console.log(json.username[i]);
                       $("#random_user"+i).text(json.username[i]);
                       $( "#random_user"+i ).click(function(event) {
                        event.preventDefault();
                            $("input[name='username']").val($(this).text());
                            $("#user_exist").text('');
                       });

                    }
                }
                if(json.status == 'error')
                {
                    $("#user_exist").text('');
                    $('#username_list li').hide();

                }

            });
});
        

        $("#add_desg").on('click', function(e) {
        e.preventDefault();
        var name = $("input[name='desg_name']").val();
        var description = $("textarea[name='desg_description']").val();
        var department_id = $("select[name='desg_department_id']").val();
        $.ajax({
                    method: 'GET',
                    url: '{!! route('admin::users.savedesignation') !!}',
                    data: {'name' : name,'description': description,'department_id': department_id},
                    datatype:'JSON',
                }).success(function (data) {
                    
                    var json = data;
                    if(json.status == 1)
                    {
                        $('#desg_msg strong').html('Designation added successfully');
                        $("#desg_msg").addClass("alert-info");
                    }
                    else
                    {
                        $('#desg_msg strong').html('Name field is required');
                        $("#desg_msg").addClass("alert-danger");
                    }
                    $('#select2').append('<option value="' + json.desg.id + '" selected>' + json.desg.name + '</option>').select2().trigger('change');

                      setTimeout(function () {
                        $('#desg_msg strong').html('');
                        $("input[name='desg_name']").val('');
                        $("textarea[name='desg_description']").val('');
                        $('#modal_theme_info1 .close').click();
                     }, 1000);
                });
        });

        $("input[name$='marital_status']").click(function(){
            if($(this).val() == 1)
            {
                $("input[name$='date_of_anniversary']").attr('disabled', false);
            } else
            {
                $("input[name$='date_of_anniversary']").attr('disabled', true).val('');
            }
        });

        // if($("input[name$='marital_status']").val() != 1){
        //     $("input[name$='date_of_anniversary']").attr('disabled', true).val('');
        // }

        if($("input[name='marital_status']:checked").val() == 1){
            $("input[name$='date_of_anniversary']").attr('disabled', false);
        }
        else{
            $("input[name$='date_of_anniversary']").attr('disabled', true).val('');
        }

        $("#state_list").change(function() {
            var state_id = $("#state_list").val();
            $('#city_list').html('');
            var default_designation = '';
            $.ajax({
                method: 'GET',
                url: '{!! route('admin::users.getcity') !!}',
                data: {'state' : state_id},
                datatype:'JSON',
            })
            .success(function (data) {
                var city = data.cities;
                 $('#city_list').html('');
                if(data.status == 'success')
                {
                    $.each(city, function(val, key) {
                        $('#city_list').append('<option value="'+ key + '">' + val + '</option>');
                    });
                }
            })
            .complete(function() {
                $("#city_list").select2("destroy").select2();
            });
        });

    $("#city_list").change(function() {
        var city_id = $("#city_list").val();
        var state_id = $("#state_list").val();
        $('#zip_code').html('');
        $.ajax({
        method: 'GET',
        url: '{!! route('admin::users.getzipcode') !!}',
        data: {taluk: city_id, state:state_id},
        datatype:'JSON',
        }).success(function (data) {
            var pincode = data.pincode;
             $('#zip_code').html('');
            if(data.status == 'success')
            {
                $.each(pincode, function(val, key) {
                    $('#zip_code').append('<option value="'+ key + '">' + val + '</option>');
                });
            }
        })
        .complete(function() {
            $("#zip_code").select2("destroy").select2();
        });
    });


</script>
@endsection

