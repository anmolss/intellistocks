@extends('admin.layout.login')

@section('content')
    <!-- Advanced login -->
    {!! BootForm::open()->post()->action(url('login')) !!}

        <div class="panel panel-body login-form">
            <div class="text-center">
                {{Html::image('images/logo.png', 'Ruthmans CRM', ['class' => 'col-md-12 border-bottom-grey-300'])}}
                <h5 class="content-group">Login to your account <small class="display-block">Your credentials</small></h5>
            </div>
            {!! BootForm::inputGroup('Email Id or Mobile no.', 'login')->hideLabel()->placeholder('Email Id or Mobile no.')->beforeAddon('<i class="icon-user text-muted"></i>') !!}
            {!! BootForm::inputGroup('Password', 'password')->type('password')->placeholder('Password')->hideLabel()->beforeAddon('<i class="icon-lock2 text-muted"></i>') !!}

            <div class="form-group login-options">
                <div class="row">
                    <div class="col-sm-6">
                        {!! BootForm::checkbox('Remember', 'remember')->inline()->addClass('styled')->checked() !!}
                    </div>
                    <div class="col-sm-6 text-right">
                        {{link_to('/password/reset', 'Forgot password?')}}
                    </div>
                </div>
            </div>

            <div class="form-group">
               {!! BootForm::submit('Login <i class="icon-arrow-right14 position-right"></i>')->addClass('btn bg-pink-400 btn-block') !!}
            </div>

            {{--<div class="content-divider text-muted form-group"><span>or sign in with</span></div>
            <ul class="list-inline form-group list-inline-condensed text-center">
                <li><a href="{{url('redirect/facebook')}}" class="btn border-indigo text-indigo btn-flat btn-icon btn-rounded"><i class="icon-facebook"></i></a></li>
                <li><a href="{{url('redirect/google')}}" class="btn border-danger-700 text-danger-700 btn-flat btn-icon btn-rounded"><i class="icon-google-plus"></i></a></li>
                <li><a href="{{url('redirect/linkedin')}}" class="btn border-primary-700 text-primary-700 btn-flat btn-icon btn-rounded"><i class="icon-linkedin2"></i></a></li>
                <li><a href="{{url('redirect/yahoo')}}" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-yahoo"></i></a></li>
            </ul>

            <div class="content-divider text-muted form-group"><span>Don't have an account?</span></div>
            {{link_to('/register', 'Sign up', ['class'=> 'btn btn-default btn-block content-group'])}}
            <span class="help-block text-center no-margin">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span>--}}
        </div>

    {!! BootForm::close() !!}
    <!-- /advanced login -->
@endsection