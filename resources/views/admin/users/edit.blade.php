@extends('admin.layout.app')

@section('page_title', 'Employee')
@section('page_subtitle', 'Edit Employee')

@section('content')
    @include('admin.partials.toolbar')


            {!! BootForm::open()->put()->action(route('admin::users.update', ['users' => $item->id] ))->multipart()->novalidate() !!}
            {!! BootForm::bind($item) !!}

            @include('admin.users._form', ['submitBtnText' => "Edit Employee"])
    {!! BootForm::close() !!}
     
@endsection
