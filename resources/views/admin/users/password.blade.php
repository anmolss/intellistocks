<div class="form-group">
    <div class="row">
   		 <div class="col-md-4">
            {!! BootForm::text('Username <span class="text-danger">*</span>', 'username')->id('username')->required()!!}
            <p class="help-block" id="user_exist"></p>
                <ul class="list-inline list-inline-condensed no-margin-bottom text-nowrap" id="username_list"><li><a href="#" id="random_user0" class="label bg-success heading-text"></a></li><li><a href="#" id="random_user1" class="label bg-success heading-text"></a></li><li><a href="#" id="random_user2" class="label bg-success heading-text"></a></li></ul>
        </div>

        <div class="col-md-4">
            {!! BootForm::password('Password <span class="text-danger">*</span>', 'password')->required()!!}
        </div>

        <div class="col-md-4">
            {!! BootForm::password('Confirm Password <span class="text-danger">*</span>', 'password_confirmation')->required() !!}
        </div>
    </div>
</div>