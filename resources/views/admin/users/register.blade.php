@extends('admin.layout.login')


@section('content')
    {!! BootForm::open()->post()->action(url('register'))->addClass('form-register') !!}
        <div class="panel panel-body login-form">
            <div class="text-center">
                <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                <h5 class="content-group">Create account <small class="display-block">All fields are required</small></h5>
            </div>

            <div class="content-divider text-muted form-group"><span>Your credentials</span></div>

            {!! BootForm::inputGroup('Name', 'name')->hideLabel()->placeholder('Name')->beforeAddon('<i class="icon-user text-muted"></i>') !!}
            {!! BootForm::inputGroup('Mobile no.', 'mobile')->hideLabel()->placeholder('Mobile no.')->beforeAddon('<i class="icon-mobile text-muted"></i>') !!}
            {!! BootForm::inputGroup('Email Id', 'email')->hideLabel()->placeholder('Email Id')->beforeAddon('<i class="icon-mention text-muted"></i>') !!}
            {!! BootForm::inputGroup('Username', 'username')->hideLabel()->placeholder('Username')->beforeAddon('<i class="icon-user-check text-muted"></i>') !!}


            {!! BootForm::inputGroup('Password', 'password')->type('password')->placeholder('Password')->hideLabel()->beforeAddon('<i class="icon-user-lock text-muted"></i>') !!}
            {!! BootForm::inputGroup('Confirm Password', 'password_confirmation')->type('password')->placeholder('Confirm Password')->hideLabel()->beforeAddon('<i class="icon-user-lock text-muted"></i>') !!}


            <div class="content-divider text-muted form-group"><span>Additions</span></div>

            <div class="form-group">
                {!! BootForm::checkbox('Accept '.link_to('#', 'terms of service'), 'accept')->inline()->addClass('styled') !!}
            </div>
            {!! BootForm::submit('Register <i class="icon-arrow-right14 position-right"></i>')->addClass('btn bg-pink-400 btn-block btn-lg legitRipple') !!}
        </div>
    {!! BootForm::close() !!}
@endsection