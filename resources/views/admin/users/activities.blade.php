@extends('admin.layout.app')

@section('page_title', 'User')
@section('page_subtitle', 'Activities')


@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="over-title margin-bottom-15">
                Activities
            </h5>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-hover" id="sample-table-2">
                <tbody>
	                @foreach($item->activities as $key => $activity)
                        <tr>
                            <td>{{$key + 1}}</td>
							<td>{{$activity->name}}</td>
							<td></td>
							<td></td>
                        </tr>
	                @endforeach              
                </tbody>
            </table>
        </div>
    </div>
@endsection