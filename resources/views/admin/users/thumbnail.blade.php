<!-- User thumbnail -->
<div class="thumbnail" >
    <div class="thumb thumb-rounded thumb-slide" ng-controller="MyCtrl">
        <img src="@if(!empty($item->image)){{ asset('uploads/user/images/'.$item->id.'/'.$item->image) }}@else {{asset('images/placeholder.jpg')}} @endif" id="myimage" alt="" style="width:175px;height:175px">
        <img  ngf-thumbnail="myFile" ng-src="" class=""  style="width:175px;height:175px;display:none !important" id="thumb">

        
        <div class="caption" >
            <!-- <form name="myForm"> -->
            <span>
            @if(empty($item->image))
                <a href="#" class="btn bg-success-400 btn-icon btn-xs" id= "input_file" data-popup="lightbox"><i class="icon-plus2"></i></a>
                <div style='height: 0px;width:0px; overflow:hidden;'><input type="hidden" value = "<?php echo isset($item->id)? $item->id:'';?>" ng-model="userId" id="userId" name="userId" >
                    <input type="file" ngf-select ng-model="myFile" ng-change="uploadPic()" id="file" name="file" value="upload" accept="image/*"/></div>
                    @else
                     <a href="#" class="btn bg-success-400 btn-icon btn-xs" id= "input_file"><i class="icon-link"></i></a>
                <div style='height: 0px;width:0px; overflow:hidden;'><input type="hidden" value = "<?php echo isset($item->id)? $item->id:'';?>" ng-model="userId" id="userId" name="userId" >
                <input type="file" ngf-select ng-model="myFile" ng-change="uploadPic()" id="file" name="file" value="upload" accept="image/*"/></div>
                  @endif
             </span>
            <!--  </form> -->
        </div>
        
       

    </div>
    <div class="caption text-center">
        <h6 class="text-semibold no-margin">@if(!empty($item->name)){{ $item->name }}@endif</h6>
    </div>
</div>
<!-- /user thumbnail -->

@section('scripts')
    @parent
    <script type="text/javascript">
        var APP_URL = {!! json_encode(url('/')) !!};
        $('#input_file').click(function(e){
            e.preventDefault();
            $('#file').click();

        });
        app.controller('MyCtrl',function ($scope, $attrs, $http, $sce) {
            
            $scope.uploadPic = function() {
                if($("#file")[0].files[0] == undefined)
                {
                    return;
                }
                //$("#myimage").hide();
                var formData = new FormData();
                formData.append('id',$("#userId").val());
                formData.append('image', $("#file")[0].files[0]);

                $http({
                    method: 'POST',
                    url: APP_URL + '/users/copyimage',
                    data:formData,
                    withCredentials: true,
                    headers: {'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function (data) {
                    var src = '{{ asset("uploads/user/images") }}'+ '/' + data.image.id + '/' + data.image.image;
                    console.log(src);
                    $('#myimage').attr('src', src);
                });
            }

        });
    </script>
@endsection