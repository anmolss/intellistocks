<fieldset>
    <div class="row">
        @if(!isset($inline))
            {!! BootForm::text('Name <span class="text-danger">*</span>', 'name')->required()->addGroupClass('col-lg-6')->attribute('ng-model', '$ctrl.template.data.name')!!}
            <div class="col-lg-4">
                <label class="display-block text-semibold" style="margin-right: 136px;">Status</label>
                <label class="checkbox">
                    {!! BootForm::inlineCheckbox('Active', 'status')->addClass('styled')->checked()!!}
                </label>
            </div>
        @else
            {!! BootForm::text('Name <span class="text-danger">*</span>', 'name')->required()->addGroupClass('col-lg-12')->attribute('ng-model', '$ctrl.template.data.name')!!}
            {!! BootForm::hidden('status', 1)->attribute('ng-model', '$ctrl.template.data.name') !!}
        @endif
    </div>

    {!! BootForm::textarea('Body <span class="text-danger">*</span>', 'body')->id('template_message_body')->rows(2)->addClass('text-semibold')->required()->attribute('ng-model', '$ctrl.template.data.body') !!}
    <div class="form-group">
        <div class="well well-sm border-left-lg border-left-info">
        @foreach ($tokens as $token_val)
            <a onclick="insertAtCaret('template_message_body', '{{$token_val->value}}')" class="label bg-blue-700 text-bold m-5">{{$token_val->value}}</a>
        @endforeach

        </div>
        <span class="help-block text-info-800"><strong>NOTE :-</strong> Click on tags to use in message.</span>
    </div>
    @if(!isset($inline))
        <div class="row">
            <div class="col-md-6">
                <div>
                    <span class="text-danger">*</span> Required Fields
                </div>
            </div>
            <div class="col-md-6">
                {!! BootForm::submit($submitBtnText . ' <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right') !!}
            </div>
        </div>
    @endif
</fieldset>
