@extends('admin.layout.app')

@section('page_title', 'Template')
@section('page_subtitle', 'Create new Template')


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->post()->action(route('admin::templates.store'))->novalidate() !!}
                @include('admin.templates._form', ['submitBtnText' => "Add New Template"])
            {!! BootForm::close() !!}
        </div>
    </div>

@endsection