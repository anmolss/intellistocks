<div class="row">
	<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal_form_horizontal">Launch <i class="icon-play3 position-right"></i></button>

	<div id="modal_form_horizontal" class="modal fade in">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h5 class="modal-title">Complete Lead</h5>
				</div>

				<form action="#" class="form-horizontal">
					<div class="modal-body">
						<div class="form-group">
							<label class="col-lg-3 control-label">Select your state:</label>
							<div class="col-lg-9">
								<select data-placeholder="Select your state" class="select">
									<option></option>
									<optgroup label="Alaskan/Hawaiian Time Zone">
										<option value="AK">Alaska</option>
										<option value="HI">Hawaii</option>
									</optgroup>
									<optgroup label="Pacific Time Zone">
										<option value="CA">California</option>
										<option value="OR">Oregon</option>
										<option value="WA">Washington</option>
									</optgroup>
									<optgroup label="Mountain Time Zone">
										<option value="AZ">Arizona</option>
										<option value="CO">Colorado</option>
										<option value="WY">Wyoming</option>
									</optgroup>
									<optgroup label="Central Time Zone">
										<option value="AL">Alabama</option>
										<option value="KS">Kansas</option>
										<option value="KY">Kentucky</option>
									</optgroup>
									<optgroup label="Eastern Time Zone">
										<option value="CT">Connecticut</option>
										<option value="DE">Delaware</option>
										<option value="WV">West Virginia</option>
									</optgroup>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-3">Email</label>
							<div class="col-sm-9">
								<input type="text" placeholder="eugene@kopyov.com" class="form-control">
								<span class="help-block">name@domain.com</span>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Submit form</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
