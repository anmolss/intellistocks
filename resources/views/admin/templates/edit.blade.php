@extends('admin.layout.app')

@section('page_title', 'Templates')
@section('page_subtitle', 'Edit template')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Template <small class="text-bold">Edit</small>
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->put()->action(route('admin::templates.update', ['templates' => $item->id] )) !!}
            {!! BootForm::bind($item) !!}
            
                @include('admin.templates._form', ['submitBtnText' => "Edit Template"])

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection