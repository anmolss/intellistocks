<script type="text/javascript" src="{{asset('bower_components/angular/angular.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/angular-bootstrap/ui-bootstrap-tpls.js')}}"></script>
<script src="{{ asset('bower_components/angular-touch/angular-touch.min.js') }}"></script>
<script src="{{ asset('bower_components/angular-animate/angular-animate.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/angular-sanitize/angular-sanitize.min.js') }}"></script>
<script type="text/javascript" src="{{asset('bower_components/lodash/dist/lodash.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('bower_components/angular-ui-select/dist/select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/angular-ui-grid/ui-grid.min.js') }}"></script>

<script type="text/javascript" src="{{asset('bower_components/ng-file-upload/ng-file-upload-shim.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/ng-file-upload/ng-file-upload.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{asset('bower_components/jpkleemans-angular-validate/dist/angular-validate.min.js') }}"></script>
<script type="text/javascript" src="{{asset('bower_components/bootstrap-ui-datetime-picker/dist/datetime-picker.min.js') }}"></script>
<script type="text/javascript" src="{{asset('bower_components/angular-validation/dist/angular-validation.js') }}"></script>
<script type="text/javascript" src="{{asset('bower_components/angular-validation/dist/angular-validation-rule.js') }}"></script>
<script type="text/javascript" src="{{asset('bower_components/angular-sweetalert/dist/ngSweetAlert.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/angular-apps.js') }}"></script>

