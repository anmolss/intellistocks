<div class="form-group" ng-controller="inlineTemplate_{{$add_msg_to}}" ng-cloak>
    <label class="control-label {{$lable_col_size ? 'col-lg-'.$lable_col_size : ''}}">Select Template</label>
    <div class="input-group {{$lable_col_size ? 'col-lg-'.(12-$lable_col_size) : ''}}">
        <ui-select ng-model="$parent.template"
                   sortable="true" class="form-control"
                   title="Select Template">
            <ui-select-match placeholder="Select Template...">@{{$select.selected.name}}</ui-select-match>
            <ui-select-choices repeat="template in base_templates track by $index"
                               refresh-delay="20">
                <div ng-bind-html="template.name | highlight: $select.search"></div>
            </ui-select-choices>
        </ui-select>

        <a class="input-group-btn pt-20" ng-click="open()"><i class="icon-plus-circle2 text-primary"></i></a>
    </div>
    <span ng-show="noResults" class="help-block"><i class="glyphicon glyphicon-remove"></i> No Results Found</span>

    <script type="text/ng-template" id="customTemplate.html">
        <a>
            <strong ng-bind-html="match.model.name | uibTypeaheadHighlight:query"></strong>
            <br>
            <span ng-bind-html="match.model.body | limitTo:20"></span>...
        </a>
    </script>

    <script type="text/ng-template" id="final_{{$add_msg_to}}.html">
        <div class="modal-header">
            <h3 class="modal-title" id="modal-title">Message Preview</h3>
        </div>
        <form novalidate name="template" ng-submit="$ctrl.template.submit(template)">
        <div class="modal-body" id="modal-body">
            @if(BootForm::open())
            @endif
            @include('admin.templates._form', [ 'tokens'=> $tokens, 'inline' => true])
        </div>
        <div class="modal-footer">
            <button class="btn btn-warning pull-left" type="reset" ng-click="$ctrl.cancel()">Cancel</button>
            <button class="btn btn-primary pull-right" ng-disabled="!template.$valid" type="submit" >Add New Template</button>
        </div>
        </form>
    </script>
</div>
@push('scripts')
<script type="text/javascript">

    app.controller('inlineTemplate_{{$add_msg_to}}',function($scope, $http, sweet, $uibModal){

        $scope.getTemplate = function(val){
            return $http({
                method: 'GET',
                url: '{{url('/templates')}}',
                params:{name: val, type: '{{$type}}', ajax: true}
            }).then(function successCallback(response) {
                return _.map(response.data, function (item) {
                    return {
                        name: item.name,
                        body: item.body
                    };
                });
            }, function errorCallback(response) {
                $scope.template = null;
            });
        };

        $scope.base_templates = {!! $templates !!};

        $scope.getToken = function(val){
            return $http({
                method: 'GET',
                url: '{{url('/templates')}}',
                params:{name: val, type: '{{$type}}', ajax: true}
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                $scope.template = null;
            });
        };

        $scope.$watch('template', function (newval) {
            if(newval !== null && typeof newval === 'object')
            {
                $('.{{$add_msg_to}}').val(newval.body);
            }
        });

        $scope.open = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'final_{{$add_msg_to}}.html',
                controller: 'ModalInstanceCtrl_{{$add_msg_to}}',
                controllerAs: '$ctrl'
            });
            modalInstance.result.then(function (template) {
                $scope.template = template;
                $scope.base_templates = $scope.base_templates.push(template);
            }, function () {

            });
        };
    });

    app.controller('ModalInstanceCtrl_{{$add_msg_to}}', function ( $scope, $uibModalInstance,$http, sweet) {
        var $ctrl = this;

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss(null);
        };

        $ctrl.template = {
            data:{
                status: 1
            },
            submit: function (form) {
                if (form.$valid){
                    return $http({
                        method: 'POST',
                        url: '{{route('admin::templates.store')}}',
                        params:$ctrl.template.data
                    }).then(function successCallback(response) {
                        sweet.show('Delivered!', 'Template Added sent successfully', 'success');
                        $uibModalInstance.close({
                            name: response.data.name,
                            body: response.data.body
                        });
                        return;
                    }, function errorCallback(response) {
                        return;
                    });
                }

            }
        };
    });

</script>

@endpush