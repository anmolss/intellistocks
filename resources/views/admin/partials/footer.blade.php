<div class="footer text-muted text-center">
    &copy; {{date('Y')}}. {{link_to('/', 'Rothmans CRM')}}
</div>