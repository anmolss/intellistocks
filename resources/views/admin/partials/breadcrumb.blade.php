<div class="page-header page-header">
    <div class="page-header-content">
        <div class="page-title pt-5">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">@yield('page_title')</span> - @yield('page_subtitle')</h4>
        </div>
    </div>
</div>


    {{--<div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="components_breadcrumbs.html">Components</a></li>
            <li class="active">Breadcrumb</li>
        </ul>

        <ul class="breadcrumb-elements">
            <li><a href="#" class="legitRipple"><i class="icon-comment-discussion position-left"></i> Support</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown">
                    <i class="icon-gear position-left"></i>
                    Settings
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                    <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                    <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                </ul>
            </li>
        </ul>
        <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a></div>--}}

@section('scripts')
    @parent
    <script type="text/javascript">
         $('ul.nav li.dropdown').hover(function() {
        // $('a#.nav li.dropdown').hover(function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });
 </script>
@endsection
