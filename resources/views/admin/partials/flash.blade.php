
@section('scripts')
    @parent
    <script type="text/javascript">

        function show_stack_top_right(type, msg, delay) {
            if(!delay)
            {
                delay = 10000;
            }
            var opts = {
                text: msg,
                type: type,
                opacity:.7,
                delay: delay
            };
            switch (type) {
                case 'error':
                    opts.title = "Error";
                    opts.addclass = "bg-danger";
                    opts.type = "error";
                    break;

                case 'info':
                    opts.title = "Info";
                    opts.addclass = "bg-info";
                    opts.type = "info";
                    break;

                case 'success':
                    opts.title = "Successfully";
                    opts.addclass = "bg-success";
                    opts.type = "success";
                    break;

                case 'warning':
                    opts.title = "Warning";
                    opts.addclass = "bg-warning";
                    opts.type = "warning";
                    break;
                default:
                    opts.title = "Successfully";
                    opts.addclass = "bg-success";
                    opts.type = "success";
                    break;
            }
            new PNotify(opts);
        }

        @if (session()->has('flash_notification.message'))
                show_stack_top_right('{{ session('flash_notification.level') }}', {!! json_encode(session('flash_notification.message')) !!});
        @endif
    </script>
@endsection

