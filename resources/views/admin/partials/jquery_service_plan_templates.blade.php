<div id="service_template" class="row" style="display:none;">
  <hr />
  <div class="col-sm-12 well service_row_template">
    <div class="col-sm-3">
      <label class="control-label">Service Type:</label>
      <select class="form-control select2-search-modal service_type" name="fk_service_id">
        <option value="">Select Service</option>
        @foreach($services as $key_service => $service)
        <option value="{{$key_service}}">{{$service}}</option>
        @endforeach
      </select>
      <span class="help-block error_fk_service_id" style="display:none;"></span>
    </div>
    
    <div class="col-sm-3">
      <label for="fk_plan_id" class="control-label">Plan:</label>
      <select class="form-control select2-search-modal plans" name="fk_plan_id">
        <option value="">Select Plan</option>
      </select>
      <span class="help-block error_fk_plan_id" style="display:none;"></span>
    </div>

    <div class="col-sm-3">
      <label class="control-label">Payment:</label>
      <select class="form-control select2-search-modal fk_payment_type" name="fk_payment_type">
        <option value="">Select Payment</option>
        <option value="cash">Cash</option>
        <option value="cheque">Cheque</option>
        <option value="net_banking">Net Banking </option>
      </select>
      <span class="help-block error_fk_payment_type" style="display:none;"></span>
    </div>

    <div class="col-sm-2">
      <label class="control-label">Payment Type:</label>
      <select class="form-control select2-search-modal payment_type" name="payment_type">
        <option value="">Payment Type</option>
        <option value="full">Full</option>
        <option value="partial">Partial</option>
      </select>
      <span class="help-block error_payment_type" style="display:none;"></span>
    </div>

    <div class="col-sm-1">
      <label class="control-label col-sm-12">&nbsp;</label>
      <button type="button" class="btn btn-danger btn-icon remove_service"><i class="icon-minus2"></i></button>
    </div>
     <div class="clearfix"></div>
              <div  style="margin-top:10px;">
               <div class="col-sm-2">
                <label class="control-label">Activation Date:</label>
                <input type="date" name="services[0][activation_date]" data-date-format="dd-mm-yyyy" class="form-control" >
                <span class="help-block error_payment_type" style="display:none;"></span>
              </div>
              <div class="col-sm-2">
                
                <div style="padding-top:40px;">
                <input type="checkbox" name="services[0][force_verify]" data-date-format="dd-mm-yyyy"  value="yes"> Verify Forcefully
                <span class="help-block error_payment_type" style="display:none;"></span>
                </div>
              </div>
             </div>
    <div class="plan_spinner col-sm-1 text-center" style="display:none;">
        <i class="icon-spinner2 spinner"></i>
    </div>

    <div class="plan_append"></div>
    <div class="payment_append">
      <div class="col-sm-12">
        <div class="row payment_type_append"></div>
      </div>
    </div>
  </div>
</div>

<div id="payment_type_cash_full_template" style="display:none;">
   <div class="col-sm-12">
      <h5>Cash Pay</h5>
      <div class="form-group">
        <div class="row">
          <div class="col-sm-2">
            <label for="amount" class="control-label">Amount:</label>
            <input class="form-control reflect_amt_key_up" placeholder="Amount" name="amount" type="text">
            <span class="help-block error_amount0" style="display:none;"></span>
          </div>
          <div class="col-sm-3">
            <label for="date" class="control-label">Date:</label>
            <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="date" type="text">
            <span class="help-block error_date0" style="display:none;"></span>
          </div>
          <div class="col-sm-3">
            <label for="transaction_id" class="control-label">Transaction Id:</label>
            <input class="form-control" placeholder="Transaction Id" name="transaction_id" type="text">
            <span class="help-block error_transaction_id" style="display:none;"></span>
          </div>
          <div class="col-sm-4">
            <label for="received_from" class="control-label">Name:</label>
            <input class="form-control" placeholder="Name" name="received_from" type="text">
            <span class="help-block error_received_from" style="display:none;"></span>
          </div>
          <!--
          <div class="col-md-2">
            <label class="control-label">Cleared:</label>
            <select class="form-control" name="cleared">
              <option value="No">No</option>
              <option value="Yes">Yes</option>
            </select>
          </div>
          -->
        </div>
      </div>
   </div>
</div>

<div id="payment_type_cheque_full_template" style="display:none;">
  <div class="col-sm-12">
      <h5>Cheque Pay</h5>
      <div class="form-group">
        <div class="row">
          <div class="col-sm-2">
            <label for="amount" class="control-label">Amount:</label>
            <input class="form-control reflect_amt_key_up" placeholder="Amount" name="amount" type="text">
            <span class="help-block error_amount0" style="display:none;"></span>
          </div>
          <div class="col-sm-3">
            <label for="date[]" class="control-label">Date:</label>
            <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="date" type="text">
            <span class="help-block error_date0" style="display:none;"></span>
          </div>
          <div class="col-sm-2">
            <label for="cheque_number" class="control-label">Cheque Number:</label>
            <input class="form-control" placeholder="Cheque Number" name="cheque_number" type="text">
            <span class="help-block error_cheque_number" style="display:none;"></span>
          </div>
          <div class="col-sm-3">
            <label for="bank" class="control-label">Bank:</label>
            <input class="form-control" placeholder="Bank" name="bank" type="text">
            <span class="help-block error_bank" style="display:none;"></span>
          </div>
          <div class="col-md-2">
            <label class="control-label">Cleared:</label>
            <select class="form-control" name="cleared">
              <option value="No">No</option>
              <option value="Yes">Yes</option>
            </select>
          </div>
        </div>
      </div>
   </div>
</div>

<div id="payment_type_net_banking_full_template" style="display:none;">
  <div class="col-sm-12">
      <h5>Net Banking</h5>
      <div class="form-group">
        <div class="row">
          <div class="col-md-2">
            <label for="amount[]" class="control-label">Amount:</label>
            <input class="form-control reflect_amt_key_up" placeholder="Amount" name="amount" type="text">
            <span class="help-block error_amount0" style="display:none;"></span>
          </div>

          <div class="col-md-2">
            <label for="date[]" class="control-label">Date:</label>
            <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="date" type="text">
            <span class="help-block error_date0" style="display:none;"></span>
          </div>

          <div class="col-md-2">
            <label for="type[]" class="control-label">Type:</label>
            <select class="form-control select2-search-modal" name="type">
              <option value="NEFT">NEFT</option>
              <option value="RTGS">RTGS</option>
            </select>
            <span class="help-block error_type" style="display:none;"></span>
          </div>

          <div class="col-md-3">
            <label for="transaction_id[]" class="control-label">Transaction Id:</label>
            <input class="form-control" placeholder="Transaction Id" name="transaction_id" type="text">
            <span class="help-block error_transaction_id" style="display:none;"></span>
          </div>
        
          <div class="col-md-3">
            <label for="received_from[]" class="control-label">Name:</label>
            <input class="form-control" placeholder="Name" name="received_from" type="text">
            <span class="help-block error_received_from" style="display:none;"></span>
          </div>
          <!--
          <div class="col-md-2">
            <label class="control-label">Cleared:</label>
            <select class="form-control" name="cleared">
              <option value="No">No</option>
              <option value="Yes">Yes</option>
            </select>
          </div>
          -->
        </div>
      </div>
  </div>
</div>

<div id="payment_mode_partial_template" style="display:none;">
  <div class="col-sm-3">
    <div class="form-group">
      <label class="form-label">Select Partial Pay Count</label>
      <select class="form-control select_partial_pay">
        <option value="">Select Count</option>
        <option value="2">TWO</option>
        <option value="3">THREE</option>
      </select>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="row 1st_partial_pay" style="display:none;">
      <h4>#1st Pay</h4>
      <div class="col-md-2">
        <div class="form-group">
          <label class="control-label">Payment:</label>
          <select class="form-control select2-search-modal fk_partial_payment_type0" name="fk_payment_type0">
            <option value="">Payment Type</option>
            <option value="cash">Cash</option>
            <option value="cheque">Cheque</option>
            <option value="net_banking">Net Banking </option>
          </select>
          <span class="help-block error_fk_payment_type" style="display:none;"></span>
        </div>
      </div>

      <div class="col-md-10">
        <div class="row">
          <div class="col-md-5">
            <div class="col-md-6">
              <div class="form-group">
                <label for="amount[]" class="control-label">Amount:</label>
                <input class="form-control reflect_amt_key_up" placeholder="Amount" name="amount0" type="text">
                <span class="help-block error_amount0" style="display:none;"></span>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="date[]" class="control-label">Date:</label>
                <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="date0" type="text">
                <span class="help-block error_date0" style="display:none;"></span>
              </div>
            </div>
          </div>

          <div class="col-md-7 extra-filed-container0">
          </div>
        </div>
      </div>
    </div>

    <div class="row 2nd_partial_pay" style="display:none;">
      <h4>#2nd Pay</h4>
      <div class="col-md-2">
        <div class="form-group">
          <label class="control-label">Payment:</label>
          <select class="form-control select2-search-modal fk_partial_payment_type1" name="fk_payment_type1">
            <option value="">Payment Type</option>
            <option value="cash">Cash</option>
            <option value="cheque">Cheque</option>
            <option value="net_banking">Net Banking </option>
          </select>
          <span class="help-block error_fk_payment_type" style="display:none;"></span>
        </div>
      </div>

      <div class="col-md-10">
        <div class="row">
          <div class="col-md-5">
            <div class="col-md-6">
              <div class="form-group">
                <label for="amount[]" class="control-label">Amount:</label>
                <input class="form-control reflect_amt_key_up" placeholder="Amount" name="amount1" type="text">
                <span class="help-block error_amount1" style="display:none;"></span>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="date[]" class="control-label">Date:</label>
                <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="date1" type="text">
                <span class="help-block error_date1" style="display:none;"></span>
              </div>
            </div>
          </div>

            <div class="col-md-7 extra-filed-container1">
            </div>
        </div>
      </div>
    </div>

    <div class="row 3rd_partial_pay" style="display:none;">
      <h4>#3rd Pay</h4>
      <div class="col-md-2">
        <div class="form-group">
          <label class="control-label">Payment:</label>
          <select class="form-control select2-search-modal fk_partial_payment_type2" name="fk_payment_type2">
            <option value="">Payment Type</option>
            <option value="cash">Cash</option>
            <option value="cheque">Cheque</option>
            <option value="net_banking">Net Banking </option>
          </select>
          <span class="help-block error_fk_payment_type" style="display:none;"></span>
        </div>
      </div>

      <div class="col-md-10">
        <div class="row">
          <div class="col-md-5">
            <div class="col-md-6">
              <div class="form-group">
                <label for="amount[]" class="control-label">Amount:</label>
                <input class="form-control reflect_amt_key_up" placeholder="Amount" name="amount2" type="text">
                <span class="help-block error_amount2" style="display:none;"></span>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="date[]" class="control-label">Date:</label>
                <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="date2" type="text">
                <span class="help-block error_date2" style="display:none;"></span>
              </div>
            </div>
          </div>

            <div class="col-md-7 extra-filed-container2">
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="payment_mode_partial_cash_template" style="display:none;">
  <div class="col-md-6">
    <div class="form-group">
      <label for="transaction_id[]" class="control-label">Transaction Id:</label>
      <input class="form-control" placeholder="Transaction Id" name="transaction_id" type="text">
      <span class="help-block error_transaction_id" style="display:none;"></span>
    </div>
  </div>

  <div class="col-md-6">
    <div class="form-group">
      <label for="received_from[]" class="control-label">Name:</label>
      <input class="form-control" placeholder="Name" name="received_from" type="text">
      <span class="help-block error_received_from" style="display:none;"></span>
    </div>
  </div>
  <!--
  <div class="col-md-2">
    <label class="control-label">Cleared:</label>
    <select class="form-control" name="cleared">
      <option value="No">No</option>
      <option value="Yes">Yes</option>
    </select>
  </div>
  -->
</div>

<div id="payment_mode_partial_cheque_template" style="display:none;">
  <div class="col-md-5">
    <div class="form-group">
      <label for="cheque_number" class="control-label">Cheque Number:</label>
      <input class="form-control" placeholder="Cheque Number" name="cheque_number" type="text">
      <span class="help-block error_transaction_id" style="display:none;"></span>
    </div>
  </div>

  <div class="col-md-5">
    <div class="form-group">
      <label for="bank" class="control-label">Bank:</label>
      <input class="form-control" placeholder="Bank" name="bank" type="text">
      <span class="help-block error_received_from" style="display:none;"></span>
    </div>
  </div>

  <div class="col-md-2">
    <label class="control-label">Cleared:</label>
    <select class="form-control" name="cleared">
      <option value="No">No</option>
      <option value="Yes">Yes</option>
    </select>
  </div>
  
</div>

<div id="payment_mode_partial_net_banking_template" style="display:none;">
  <div class="col-md-3">
    <label for="type[]" class="control-label">Type:</label>
    <select class="form-control select2-search-modal" name="type">
      <option value="NEFT">NEFT</option>
      <option value="RTGS">RTGS</option></select>
    <span class="help-block error_type" style="display:none;"></span>
  </div>

  <div class="col-md-4">
    <div class="form-group">
      <label for="transaction_id[]" class="control-label">Transaction Id:</label>
      <input class="form-control" placeholder="Transaction Id" name="transaction_id" type="text">
      <span class="help-block error_transaction_id" style="display:none;"></span>
    </div>
  </div>

  <div class="col-md-5">
    <div class="form-group">
      <label for="received_from[]" class="control-label">Name:</label>
      <input class="form-control" placeholder="Name" name="received_from" type="text">
      <span class="help-block error_received_from" style="display:none;"></span>
    </div>
  </div>
  <!--
  <div class="col-md-2">
    <label class="control-label">Cleared:</label>
    <select class="form-control" name="cleared">
      <option value="No">No</option>
      <option value="Yes">Yes</option>
    </select>
  </div>
  -->
</div>

<div id="empty_plan_template" style="display:none;">
  <div class="col-sm-12" style="margin-top:10px;">
    <div class="table-responsive">
        <table class="table table-xxs table-bordered">
            <thead>
                <tr>
                    <th colspan="10" class="text-center bg-primary">Plan Details</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center">Please Select Service & Plan</td>
                </tr>
            </tbody>
        </table>
    </div>
  </div>
</div>