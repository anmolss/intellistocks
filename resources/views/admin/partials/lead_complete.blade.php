<div id="modal_form_lead_complete" class="modal fade in">
    <div class="modal-dialog modal-full">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h5 class="modal-title">Complete Lead</h5>
        </div>

        {!! Form::open([
                'method' => 'PATCH',
                'route' => ['admin::leads.completeLead', $leads->id],
                'class' => 'ui-form',
                'id'    => 'lead_complete_form',
                ]) !!}

              {{ Form::hidden('fk_client_id', $leads->clientAssignee->id) }}
              {{ Form::hidden('lead_id', $leads->id) }}
              
              <input type="hidden" name="payment_mode" value="0">
          <div class="row modal-body">
            <fieldset>
              <legend class="text-semibold">
                <i class="icon-user-tie position-left"></i>
                Customer Info
                <a class="control-arrow" data-toggle="collapse" data-target="#demo1">
                  <i class="icon-circle-down2"></i>
                </a>
              </legend>

              <div class="collapse in" id="demo1">
                
                 <div class="col-sm-12 form-group">    

                    <div class="form-group col-md-3">
                        {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
                        {!! Form::text('name', $leads->clientAssignee->name, ['class' => 'form-control']) !!}
                        <span class="help-block" id="error_name" style="display:none;"></span>
                    </div>

                    <div class="form-group col-md-3">
                        {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                        {!! Form::email('email',$leads->clientAssignee->email, ['class' => 'form-control']) !!}
                        <span class="help-block" id="error_email" style="display:none;"></span>
                    </div>

                    <div class="form-group col-sm-3">
                        {!! Form::label('mobile', 'Primary Number:', ['class' => 'control-label']) !!}
                        {!! Form::text('mobile',  $leads->clientAssignee->primary_number, ['class' => 'form-control']) !!}
                        <span class="help-block" id="error_mobile" style="display:none;"></span>
                    </div>

                    <div class="form-group col-sm-3">
                        {!! Form::label('secondary_number', 'Secondary Number:', ['class' => 'control-label']) !!}
                        {!! Form::text('secondary_number',  $leads->clientAssignee->secondary_number, ['class' => 'form-control']) !!}
                        <span class="help-block" id="error_secondary_number" style="display:none;"></span>
                    </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group row">
                      <div class="form-group col-md-3">
                        {!! Form::label('address1', 'Address Line 1:', ['class' => 'control-label']) !!}
                        {!! Form::text('address1', $leads->clientAssignee->address, ['class' => 'form-control']) !!}
                        <span class="help-block" id="error_address1" style="display:none;"></span>
                      </div>
                      <div class="form-group col-md-3">
                        {!! Form::label('address2', 'Address Line 2:', ['class' => 'control-label']) !!}
                        {!! Form::text('address2', null, ['class' => 'form-control']) !!}
                        <span class="help-block" id="error_address2" style="display:none;"></span>
                      </div>

                      <div class="form-group col-md-2">
                          {!! Form::label('state', 'State:', ['class' => 'control-label']) !!}
                          {!! Form::text('state',$leads->clientAssignee->state, ['class' => 'form-control']) !!}
                          <span class="help-block" id="error_state" style="display:none;"></span>
                      </div>

                      <div class="form-group col-md-2">
                          {!! Form::label('city', 'City:', ['class' => 'control-label']) !!}
                          {!! Form::text('city',$leads->clientAssignee->city, ['class' => 'form-control']) !!}
                          <span class="help-block" id="error_city" style="display:none;"></span>
                      </div>

                      <div class="form-group col-md-2">
                          {!! Form::label('zipcode', 'Zipcode:', ['class' => 'control-label']) !!}
                          {!! Form::text('zipcode', $leads->clientAssignee->zipcode, ['class' => 'form-control']) !!}
                          <span class="help-block" id="error_zipcode" style="display:none;"></span>
                      </div>
                  </div>  
                </div>

                <div class="col-sm-12">
                    <div class="form-group row">
                      <div class="form-group col-md-6">
                          {!! Form::label('fk_relation_manager_id', 'Relashionship Manager:', ['class' => 'control-label']) !!}
                          <div class="col-md-12">
                              {!! Form::select('fk_relation_manager_id', $hrusers, null, ['class' => 'form-control' ] )!!}
                          </div>
                          <span class="help-block" id="error_fk_relationship_manager_id" style="display:none;"></span>
                      </div>
                      <div class="col-md-6 form-group">
                          {!! Form::label('source', 'Soruce:', ['class' => 'control-label ']) !!}
                          {!! Form::select('source', Config::get('rothmans.lead_source'), null, ['class' => 'form-control ' ] )
                           !!}
                          <span class="help-block" id="error_source" style="display:none;"></span>
                      </div>
                    </div>
                </div>
                
                <div class="col-sm-12">
                  <div class="row form-group">
                    <div class="form-group col-md-4">
                        {!! Form::label('username', 'Username:', ['class' => 'control-label']) !!}
                        {!! Form::text('username', str_replace(' ', '_', strtolower($leads->clientAssignee->name)), ['class' => 'form-control']) !!}
                        <span class="help-block" id="error_username" style="display:none;"></span>
                    </div>
                    <div class="form-group col-md-4">
                        {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
                        {!! Form::text('password', str_random(6), ['class' => 'form-control']) !!}
                        <span class="help-block" id="error_password" style="display:none;"></span>
                    </div>
                    <div class="form-group col-md-4">
                      &nbsp;
                    </div>
                  </div>
                </div>
              </div>
            </fieldset>

            <fieldset>
              <legend class="text-semibold">
                <i class="icon-magazine position-left"></i>
                Services & Plans
                <a class="control-arrow" data-toggle="collapse" data-target="#demo2">
                  <i class="icon-circle-down2"></i>
                </a>
              </legend>

              <div class="collapse in" id="demo2">
                <div class="form-group" id="service_append">
                  <div class="col-sm-12">
                    <div class="col-sm-12 well service_row" data-count='0'>
                      <div class="col-sm-3">
                        {!! Form::label('services[0][fk_service_id]', 'Service Type:', ['class' => 'control-label']) !!}
                        {!! Form::select('services[0][fk_service_id]', [null=>'Select Service'] + $services->toArray(), null, ['class' => 'form-control select2-search-modal service_type'] )!!}
                        <span class="help-block error_services_fk_service_id" style="display:none;"></span>
                      </div>
                      
                      <div class="col-sm-3">
                        {!! Form::label('fk_plan_id', 'Plan:', ['class' => 'control-label']) !!}
                        {!! Form::select('services[0][fk_plan_id]', [null=>'Select Plan'], null, ['class' => 'form-control select2-search-modal plans'] )!!}
                        <span class="help-block error_services_fk_plan_id" style="display:none;"></span>
                      </div>

                      <div class="col-sm-2">
                        {!! Form::label('payment_type[]', 'Payment Type:', ['class' => 'control-label']) !!}
                        {!! Form::select('services[0][payment_type]', ['' => 'Select Payment Type', 'full' => 'Full', 'partial' => 'Partial'], null, ['class' => 'form-control select2-search-modal payment_type'] )!!}
                        <span class="help-block error_services_payment_type" style="display:none;"></span>
                      </div>

                      <div class="col-sm-2">
                        <label class="control-label">Payment:</label>
                        <select class="form-control select2-search-modal fk_payment_type" name="services[0][fk_payment_type]">
                          <option value="">Select Payment Type</option>
                          <option value="cash">Cash</option>
                          <option value="cheque">Cheque</option>
                          <option value="net_banking">Net Banking </option>
                        </select>
                        <span class="help-block error_services_fk_payment_type" style="display:none;"></span>
                      </div>

                      <div class="col-sm-2">
                        <label class="control-label">Activation Date:</label>
                        <input type="text" name="services[0][activation_date]" class="form-control date-picker service-activation-date" data-date-format="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>">
                        <span class="help-block error_services_activation_date" style="display:none;"></span>
                      </div>

                      <div class="clearfix"></div>

                      <div class="form-group col-md-6" id="capital_container" style="display:none; margin-top:10px;">
                          <label class="control-label">Enter Capital:</label>
                          <input type="text" name="services[0][capital]" class="form-control reflect_capital_amnt">
                          <span class="help-block error_services_capital" style="display:none;"></span>
                      </div>

                      <div class="plan_append">
                        <div class="col-sm-12" style="margin-top:10px;">
                            <div class="table-responsive">
                                <table class="table table-xxs table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="10" class="text-center bg-primary">Plan Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">Please Select Service & Plan</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="row form-group">
                        <div class="col-md-4">
                          <div class="form-group" id="discount-container" style="display:none">
                            <label for="" class="control-label">Discount: </label>
                            <input class="form-control reflect_amt_key_up txt-discount" placeholder="Discount (%)" name="services[0][discount]" type="number" value="0" min="0" max="0">
                            <span class="help-block error_discount" style="display:none;"></span>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="plan_spinner col-sm-1 text-center" style="display:none;">
                          <i class="icon-spinner2 spinner"></i>
                      </div>
                      <div class="clearfix"></div>
                      <div class="payment_append form-group">
                        <div class="col-sm-12">
                          <div class="row payment_type_append"></div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="row form-group" style="margin-top:10px;">
                        <div class="col-md-5">
                          <label class="control-label">Add Extra Comments:</label>
                          <textarea name="services[0][comments]" rows="1" class="form-control"></textarea>
                          <span class="help-block error_services_comments" style="display:none;"></span>
                        </div>

                        <div class="col-md-2">
                          <label class="checkbox-inline">
                            <input type="checkbox" name="services[0][force_verify]" value="yes" class="styled" id="force_verify_check">
                            <span class="text-bold text-danger">Verify Forcefully</span>
                          </label>
                        </div>

                        <div class="col-sm-5" id="force_verify_reason" style="display:none;">
                          <label class="control-label">Reason:</label>
                          <textarea name="services[0][force_verify_reason]" rows="1" class="form-control"></textarea>
                          <span class="help-block error_services_force_verify_reason" style="display:none;"></span>
                        </div>

                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
              </div>
            </fieldset>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
            {!! Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => 'lead_complete_submit']) !!}
            <div class="form_spinner col-sm-1 text-center" style="display:none;">
                <i class="icon-spinner2 spinner"></i>
            </div>
          </div>
          {!! Form::close() !!}
      </div>
    </div>
  </div>
@include('admin.partials.jquery_service_plan_templates')

@push('scripts')
<script type="text/javascript">
  var $total_services;
  var $service_plans;
  var $getPlanUrl = '{{url('getPlan')}}';
  jQuery(document).ready(function(){
    $total_services = 0;
    $service_plans = <?php echo json_encode($service_plans); ?>;
  });

  jQuery( '#lead_complete_form' ).submit(function( event ) {
    console.log( $( this ).serializeArray() );
    event.preventDefault();
    jQuery('.form_spinner').hide();
    var data = jQuery(this).serializeArray();
    var $url = jQuery(this).attr('action');
    jQuery.ajax({
        type: 'POST',
        url: $url,
        data: data,
        dataType: "json",
        beforeSend: function() {
          // setting a timeout
          jQuery('.form_spinner').show();
          jQuery('#lead_complete_submit').attr('disabled','disabled'); 
        },
        success: function(data) {
          if(data.success == 1){
            jQuery('#lead_complete_submit').removeAttr('disabled'); 
            jQuery('#modal_form_lead_complete').modal('toggle');
            jQuery('#lead_complete_form').trigger("reset");
            jQuery('span.help-block').html('');
            jQuery('#fk_client_id').html(data.clients);
            jQuery('#fk_client_id').select2({width: '40%'});
            swal("Lead Completed!", "Lead has been Completed!", "success");
            setTimeout( window.location.reload() , 2500);
          }
        },
        error: function(data) { // if error occured
          jQuery('span.help-block').hide();
          jQuery('#lead_complete_submit').removeAttr('disabled');

          var errors = data.responseJSON;
          
          jQuery.each(errors, function(index, item) {
            
            jQuery('.service_row').each(function(index1, item1){
              var $temp = index.split('.');
              var $tmp = item['0'].split('.');

              if($temp[2] == 'transaction'){
                if(jQuery(this).find('.payment_type_append').find('.error_' + $temp[4] + $temp[3]).length > 0){
                  jQuery(this).find('.error_' + $temp[4] + $temp[3]).html(item['0']).show();
                }
              } else {
                if(jQuery(this).find('.error_services_' + $temp[2]).length > 0){
                  jQuery(this).find('.error_services_' + $temp[2]).html(item['0']).show();
                }
              }
            });

            if (jQuery('#error_' + index).length > 0) {
              jQuery('#error_' + index).html(item['0']).show();
            }
          });

          jQuery('span.help-block').animate({scrollTop: 0}, "slow");
        },
        complete: function() {
          jQuery('#lead_complete_submit').removeAttr('disabled');
          jQuery('.form_spinner').hide();
        }
    });
  });
</script>
@endpush