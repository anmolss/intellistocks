 <div id="action_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="action_modal_form_spinner" style="display:none;">
	            <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal">&times;</button>
				    <h5 class="modal-title">Please Wait...</h5>
				</div>
				<div class="modal-body text-center">
					<div class="col-sm-1 text-center">
		                <i class="icon-spinner2 spinner"></i>
		            </div>
				</div>
				<div class="modal-footer">
				    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				</div>
        	</div>
            
            <div id="modal_content_append"></div>
        </div>
    </div>
</div>