@foreach($items as $item)
    <li@lm-attrs($item) @lm-endattrs>
        <a href="{!! $item->url() !!}" class="legitRipple @if($item->hasChildren()) has-ul @endif" >
            @if(isset($item->data['icon']))
                <i class="{!! $item->data['icon'] !!}"></i>
                <span> {!! $item->title !!} </span>
            @else
                <span @if($item->hasChildren())class="title" @endif> {!! $item->title !!}</span> @if($item->hasChildren())<i class="icon-arrow"></i>@endif
            @endif
        </a>
        @if($item->hasChildren())
            <ul>
                @include('admin.partials.main_menu_items', array('items' => $item->children()))
            </ul>
        @endif
    </li>

    @if($item->divider)
        <li{!! Lavary\Menu\Builder::attributes($item->divider) !!}></li>
    @endif
@endforeach
