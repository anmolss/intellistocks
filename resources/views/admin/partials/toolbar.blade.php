<div class="navbar navbar-default navbar-component navbar-xs">
    <ul class="nav navbar-nav visible-xs-block">
        <li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-filter">
        <ul class="nav navbar-nav">
            <li class="active"><a href="#settings" data-toggle="tab"><i class="icon-pencil7"></i> Edit</a></li>
            <li><a href="#profile" data-toggle="tab"><i class="icon-user position-left"></i> Profile</a></li>
        </ul>
        <div class="navbar-right">
            <ul class="nav navbar-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-gear"></i> <span class="visible-xs-inline-block position-right"> Options</span> <span class="caret"></span></a>
                    @if(Auth::user()->id == $item->id || Auth::user()->hasRole('super-admin'))
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{{route('admin::users.passwordsettings', ['users' => $item->id])}}" ><i class="icon-clippy"></i> Update password</a></li>
                        <li><a href="{{route('admin::users.logs', ['users' => $item->id])}}" ><i class="fa fa-list-ul"></i>Logs</a></li>
                    </ul>
                    @endif
                </li>
            </ul>
        </div>
    </div>
</div>