<!-- Vertical form modal -->
<div id="add_client_modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Create Person</h5>
			</div>
			{!! Form::open([
                'route' => 'admin::clients.store',
                'class' => 'ui-form person_create_form',
                'id'	=> 'add_client_form',
                ]) !!}
				<div class="modal-body">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								{!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
				                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
				                <span class="help-block" id="error_name" style="display:none;"></span>
							</div>

							<div class="col-sm-6">
								{!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
				                {!! Form::email('email',null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
				                <span class="help-block" id="error_email" style="display:none;"></span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								{!! Form::label('fk_service_id', 'Service Interested In:', ['class' => 'control-label']) !!}
			                    {!! Form::select('fk_service_id', $services, null, ['class' => 'form-control select2-search-modal'] )!!}
				                <span class="help-block" id="error_service" style="display:none;"></span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								{!! Form::label('primary_number', 'Primary Number:', ['class' => 'control-label']) !!}
				                {!! Form::text('primary_number',  null, ['class' => 'form-control', 'placeholder' => 'Primary Number']) !!}
				                <span class="help-block" id="error_primary_number" style="display:none;"></span>
							</div>

							<div class="col-sm-6">
								{!! Form::label('secondary_number', 'Secondary Number:', ['class' => 'control-label']) !!}
				                {!! Form::text('secondary_number',  null, ['class' => 'form-control', 'placeholder' => 'Secondary Number']) !!}
				                <span class="help-block" id="error_secondary_number" style="display:none;"></span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								{!! Form::label('company_name', 'Company name:', ['class' => 'control-label']) !!}
				                {!! Form::text('company_name', null, ['class' => 'form-control', 'placeholder' => 'Company Name']) !!}
				                <span class="help-block" id="error_company_name" style="display:none;"></span>
							</div>

							<div class="col-sm-6">
								{!! Form::label('company_type', 'Company type:', ['class' => 'control-label']) !!}
				                {!! Form::text('company_type',  null, ['class' => 'form-control', 'placeholder' => 'Company Type']) !!}
				                <span class="help-block" id="error_company_type" style="display:none;"></span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								{!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
				                {!! Form::textarea('address', null, ['class' => 'form-control', 'rows' => '3', 'placeholder' => 'Address']) !!}
				                <span class="help-block" id="error_address" style="display:none;"></span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								{!! Form::label('city', 'City:', ['class' => 'control-label']) !!}
				                {!! Form::text('city',null, ['class' => 'form-control', 'placeholder' => 'City']) !!}
				                <span class="help-block" id="error_city" style="display:none;"></span>
							</div>
							
							<div class="col-sm-6">
								{!! Form::label('zipcode', 'Zipcode:', ['class' => 'control-label']) !!}
				                {!! Form::text('zipcode', null, ['class' => 'form-control', 'placeholder' => 'Zipcode']) !!}
								<span class="help-block" id="error_zipcode" style="display:none;"></span>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
					{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			        <div class="form_spinner col-sm-1 text-center" style="display:none;">
			            <i class="icon-spinner2 spinner"></i>
			        </div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<!-- /vertical form modal -->