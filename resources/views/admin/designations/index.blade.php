@extends('admin.layout.app')

@section('page_title', 'Designation')
@section('page_subtitle', 'List of designations')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="over-title margin-bottom-15">
                Designations
                <a href="{{route('admin::designations.create')}}" class="btn btn-primary pull-right">Add New <i class="fa fa-plus"></i> </a>
            </h5>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-hover" id="sample-table-2">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Department</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($list) > 0)
                    @foreach($list as  $key => $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{!! $item->description !!}</td>
                            <td>{!! $item->departments->name !!}</td>
                            <td class="center">
                                <ul class="icons-list">
                                    <li class="dropdown dropdown-menu-left">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-cog7"></i><span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{route('admin::designations.edit', ['designations' => $item->id])}}"><i class="icon-pencil7 pull-right"></i> Edit</a></li>
                                            <li class="divider"></li>
                                            <li class="text-danger-700">
                                                {!! BootForm::open()->delete()->action(route('admin::designations.destroy', ['designations' => $item->id]))!!}
                                                <a class="delete_anchor" style="color: inherit; overflow: hidden; display: block; padding: 2px 16px;"><i class="icon-trash  pull-right"></i> Delete</a>
                                                {!! BootForm::close() !!}
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" align="center">No Roles Found</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
            $('a.delete_anchor').on('click', function(){
                if(confirm('Are you sure you want to detele this?')){
                    var form = $(this).closest("form");
                    form.submit();
                } else {
                    return false;
                }
            });
    </script>
@endsection