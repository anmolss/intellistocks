@extends('admin.layout.app')

@section('page_title', 'Designation')
@section('page_subtitle', 'Create new Designation')


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Designation <small class="text-bold">Add</small>
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->post()->action(route('admin::designations.store')) !!}
                @include('admin.designations._form', ['submitBtnText' => "Add New Designation"])
            {!! BootForm::close() !!}
        </div>
    </div>

@endsection