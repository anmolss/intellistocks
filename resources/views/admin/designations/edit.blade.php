@extends('admin.layout.app')

@section('page_title', 'Designations')
@section('page_subtitle', 'Edit designation')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Designation <small class="text-bold">Edit</small>
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->put()->action(route('admin::designations.update', ['designations' => $item->id] ))->multipart() !!}
            {!! BootForm::bind($item) !!}

            @include('admin.designations._form', ['submitBtnText' => "Edit Designation"])

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection