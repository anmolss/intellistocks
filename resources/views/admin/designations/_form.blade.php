<fieldset>
    <div class="row">
        <div class="col-md-6">
            {!! BootForm::text('Name <span class="text-danger">*</span>', 'name')->placeholder('Name')->required()!!}
            {!! BootForm::textarea('Description <span class="text-danger">*</span>', 'description')->placeholder('description')->rows(3) !!}
            {!! BootForm::select('Department <span class="text-danger">*</span>', 'department_id',$dept_list)->addClass('selectbox')->required()!!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div>
                <span class="text-danger">*</span> Required Fields
            </div>
        </div>
        <div class="col-md-6">
            {!! BootForm::submit($submitBtnText . ' <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right') !!}
        </div>
    </div>
</fieldset>
