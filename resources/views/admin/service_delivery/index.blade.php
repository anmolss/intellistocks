@extends('admin.layout.app')

@section('page_title', 'Service Delivery')
@section('page_subtitle', 'List of Service Delivery')

@section('content')
    <form class="form-horizontal" action="#" id="customer-frm-filter" method="GET">
        <div class="panel panel-flat panel-collapsed">
            <div class="panel-heading">
                <h6 class="panel-title text-semibold"><i class="icon-equalizer position-left"></i> Filters</h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse" class=""></a></li>
                    </ul>
                </div>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        {!! Form::label('service', 'Service:', ['class' => 'control-label col-lg-3']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::select('service[]', $services,  Request::input('service', []), ['class' => 'form-control select service_type', 'multiple' => 'multiple', 'id' => 'services'  ] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        {!! Form::label('symbol', 'Symbol:', ['class' => 'control-label col-lg-3']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::text('symbol', Request::input('symbol', null), ['class' => 'form-control' ] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-block"><span class="hidden-sm">Filter </span><i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                    <div class="col-sm-6">
                                        <a title="Reset Filter!" id="reset_search" href="javascript:" class="btn btn-success btn-rounded btn-block"><span class="hidden-sm">Reset </span><i class="icon-spinner11 position-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('creation_range', 'Delivery Date:', ['class' => 'control-label col-lg-4']) !!}
                                        <div class="col-lg-8">
                                            {!! Form::text('delivery_range', Request::input('delivery_range', Carbon::parse('1 year ago')->format('d/m/Y') . ' - ' . Carbon::now()->format('d/m/Y')), ['class' => 'form-control daterange-delivery_range']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="panel panel-flat" style="float: left;">
        <table class="table table-striped table-hover" id="sample-table-2">
            <thead>
            <tr>
                <th>Delivery Date</th>
                <th>Service Name</th>
                <th>Scrip</th>
                <th>Instrument</th>
                <th>Exchange</th>
                <th>Call Type</th>
                <th>No. of Customers</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Delivery Report</th>
                <th>Action</th>
                {{--<th>Actions</th>--}}
            </tr>
            </thead>
            <tbody>
            @if($list->count() > 0)
                @foreach($list as  $item)
                    @if($item->has('services'))
                        @foreach($item->services as $itm_serv)
                            <tr>
                                {{--<td><a href="{{route('admin::services.show', ['services' => $itm_serv->service->id])}}" tooltip-placement="top" tooltip="Edit"  class="text-primary-600">{{ $itm_serv->service->name }}</a></td>
                                <td>{{ config('rothmans.instrument_type.'.$item->instrument) }}</td>
                                <td>{{ config('rothmans.exchange.'.$item->exchange) }}</td>
                                <td>{{ $item->symbol }}</td>
                                <td>{{ config('rothmans.sd_call_type.'.$item->call_type) }}</td>
                                <td>{{ $item->range_from }} - {{ $item->range_to }}</td>
                                <td>{{ $item->sd_users->count() }}</td>
                                <td>{{ $item->created_at->toDateString() }}</td>--}}
                                <td>{{ date('d-m-Y', strtotime($item->created_at->toDateString())) }}</td>
                                <td><a href="{{route('admin::services.show', ['services' => $itm_serv->service->id])}}" tooltip-placement="top" tooltip="Edit"  class="text-primary-600">{{ $itm_serv->service->name }}</a></td>
                                <td><a href="{{route('admin::scrip.show', ['scrip' => $itm_serv->service->id])}}" tooltip-placement="top" tooltip="Edit" target="_blank" class="text-primary-600">{{ $item->symbol }}</a></td>
                                <td>{{ config('rothmans.instrument_type.'.$item->instrument) }}</td>
                                <td>{{ config('rothmans.exchange.'.$item->exchange) }}</td>
                                <td>{{ config('rothmans.sd_call_type.'.$item->call_type) }}</td>
                                <td>
                                    <!-- <?php $user_ids = ""; ?>
                                    @foreach($item->sd_users as $user)
                                        <?php $user_ids = $user_ids . "," . $user->user_id; ?>
                                    @endforeach
                                    -->
                                    <a href="#1" data-toggle="modal" data-title="{{ $itm_serv->user_id_group }}"
                                       data-target="#customerModal">{{ $itm_serv->customers_count }}</a>
                                </td>
                                <td>
                                    @if($item->exchange == 'nfo')
                                        {{ $itm_serv->nfo_qty }} Lots
                                    @else
                                        {{ $itm_serv->qty }} %
                                    @endif
                                </td>
                                <td>
                                    @if(strpos($itm_serv->msg, 'Range'))
                                        {{ $item->range_from }} - {{ $item->range_to }}
                                        {{--{{ substr($itm_serv->msg, strpos($itm_serv->msg, 'Range ')+6, -18) }}--}}
                                    @else
                                        {{ $item->price }}
                                        {{--{{ substr($itm_serv->msg, strpos($itm_serv->msg, '@')+1, -18) }}--}}
                                    @endif
                                </td>
                                {{--<td>{{ $item->range_from }} - {{ $item->range_to }}</td>--}}
                                <td>
                                    <!--<?php
                                        $success = 0;
                                        $failure = 0;
                                        $success_user_ids = "";
                                        $failure_user_ids = "";
                                    ?>
                                    @foreach($item->sd_users as $user)
                                            @if($user->delivery_status == 'success')
                                                <?php
                                                    $success++;
                                                    $success_user_ids = $success_user_ids . "," . $user->user_id;
                                                ?>
                                            @else
                                                <?php
                                                    $failure++;
                                                    $failure_user_ids = $failure_user_ids . "," . $user->user_id;
                                                ?>
                                            @endif
                                    @endforeach-->
                                    <i class="fa fa-check-circle" aria-hidden="true" style="color: green;"></i> :
                                        <a href="#1"  data-toggle="modal" data-title="{{ $itm_serv->success_user_ids }}"
                                           data-target="#customerModal">{{ $itm_serv->success_count }}</a><br>
                                    <i class="fa fa-exclamation-circle" aria-hidden="true" style="color: red;"></i> :
                                        <a href="#1" data-toggle="modal" data-title="{{ $itm_serv->failure_user_ids }}"
                                           data-target="#customerModal">{{ $itm_serv->failure_count }}</a>
                                </td>
                                <td>
                                    <a href="service_delivery/create?service_name={{ $itm_serv->service->name }}&service_id={{ $itm_serv->service->id }}&symbol={{ $item->symbol }}&exchange={{ $item->exchange }}&call_type=re_entry&instrument={{ $item->nfo_instrument }}&company_name={{ $item->name }}&status={{ $item->status }}&price={{ $item->last }}"
                                       data-toggle="tooltip" data-placement="bottom" title="Re Entry">
                                        <i class="fa fa-sign-in" aria-hidden="true"></i></a>&nbsp;
                                    <a href="service_delivery/create?service_name={{ $itm_serv->service->name }}&service_id={{ $itm_serv->service->id }}&symbol={{ $item->symbol }}&exchange={{ $item->exchange }}&call_type=exit&instrument={{ $item->nfo_instrument }}&company_name={{ $item->name }}&status={{ $item->status }}&price={{ $item->last }}"
                                       data-toggle="tooltip" data-placement="bottom" title="Exit">
                                        <i class="fa fa-times" aria-hidden="true"></i></a>
                                    <a href="#1" data-toggle="modal"
                                       data-title="{{  $itm_serv->msg }}"
                                       data-target="#messageModal">
                                        <i class="icon-eye" data-toggle="tooltip" data-placement="bottom" title="View Message"></i>
                                    </a>
                                </td>
                                {{--<td>
                                    <ul class="icons-list">
                                        <li><a href="{{route('admin::service_delivery.edit', ['service_delivery' => $item->id])}}" data-popup="tooltip" title="" data-original-title="Edit"><i class="icon-pencil7"></i></a></li>
                                    </ul>
                                </td>--}}
                            </tr>
                        @endforeach
                    @endif
                @endforeach
            @else
                <tr>
                    <td colspan="6" align="center">No Service Deliveries Found</td>
                </tr>
            @endif
            </tbody>
        </table>

        <div class="panel-footer">{{ $list->appends(Request::except('page'))->links() }}</div>
    </div>

    <!--Bootstrap Dialog for Messages start-->
    <div class="modal fade" id="messageModal"
         tabindex="-1" role="dialog"
         aria-labelledby="messageModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"
                        id="messageModalLabel">Message</h4>
                </div>
                <div class="modal-body">
                    <p>

                    </p>
                </div>
                <div class="modal-footer">
                    <span class="pull-right">
                    <button type="button"
                            class="btn btn-primary"
                            data-dismiss="modal">Close</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!--Bootstrap Dialog for Messages end-->

    <!--Bootstrap Dialog for customer start-->
    <div class="modal fade" id="customerModal"
         tabindex="-1" role="dialog"
         aria-labelledby="customerModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"
                        id="customerModalLabel">Customers</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <span class="pull-right">
                    <button type="button"
                            class="btn btn-primary"
                            data-dismiss="modal">Close</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!--Bootstrap Dialog for customer end-->

@endsection

@push('scripts')
    <script type="text/javascript" src="{{asset('js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('bower_components/StickyTableHeaders/js/jquery.stickytableheaders.min.js') }}"></script>
    <script type="text/javascript">

        $('.daterange-delivery_range').daterangepicker({
            applyClass: 'bg-info',
            cancelClass: 'btn-default',
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        // Enable Select2 select for the length option
        $('select').select2({
            minimumResultsForSearch: Infinity,
            ropdownCssClass: 'border-teal',
            containerCssClass: 'border-teal text-primary-700',
        });
        $('table#customers-table').stickyTableHeaders();

        $(function() {
            $('#messageModal').on("show.bs.modal", function (e) {
                $("#messageModal .modal-body p").html($(e.relatedTarget).data('title'));
            });
        });

        $(function() {
            $('#customerModal').on("show.bs.modal", function (e) {
                $("#customerModal table tbody").empty();
                var user_ids = $(e.relatedTarget).data('title');
                $.ajax({
                    url:'{!! route('admin::service_delivery.serviceCustomerData') !!}',
                    data:{ user_ids : user_ids},
                    success:function(data){
                        $.each(data, function(key, value) {
                            var $status = value['status'];
                            var $status_class = 'success';
                            if($status == 0){
                                $status_class = 'danger';
                                $status = 'Inactive';
                            }else{
                                $status = 'Active';
                            }
                            var $html = '<tr>\n' +
                                '                            <td><a href="/customers/'+value['id']+'" target="_blank">'+value['name']+'</a></td>\n' +
                                '                            <td>'+value['email']+'</td>\n' +
                                '                            <td><span class="label label-'+$status_class+'">'+$status+'</span></td>\n' +
                                '                        </tr>'
                            $('#customerModal table tbody').append($html);
                        });
                    },error:function(data,er){
                        alert("Error="+er);
                    }
                });
            });
        });

        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

@endpush
