@extends('admin.layout.app')

@section('page_title', 'Service Delivery')
@section('page_subtitle', 'Create New Service Delivery')


@section('content')
    <div class="row" ng-controller="servicesController as servCtrl" ng-cloak>
        {!! BootForm::open()->post()->action(route('admin::service_delivery.store'))->name("service_delivery")->id("service_delivery")->attribute('novalidate angular-validator autocomplete', "off") !!}
            @include('admin.service_delivery._form')
        {!! BootForm::close() !!}
    </div>
@endsection