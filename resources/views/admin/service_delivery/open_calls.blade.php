@extends('admin.layout.app')

@section('page_title', 'Service Delivery')
@section('page_subtitle', 'Create New Service Delivery')


@section('content')
    <div class="row" ng-controller="servicesController as servCtrl" ng-cloak>
        {!! BootForm::open()->post()->action(route('admin::service_delivery.open_calls'))->name("service_delivery")->id("service_delivery")->attribute('novalidate angular-validator autocomplete', "off") !!}
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Open Calls</h6>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-5 form-group">
                        <label class="control-label">Symbol <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-line-chart"></i></span>
                            <input type="text" name="symbol" ng-readonly="asyncSelected_temp.exchange"  ng-model="asyncSelected_temp" placeholder="Please start type stock name"
                                   uib-typeahead="address as address.symbol for address in getStocks($viewValue)" typeahead-loading="loadingLocations"
                                   typeahead-min-length="3" typeahead-no-results="noResults" class="form-control" typeahead-template-url="customTemplate.html"
                                   typeahead-wait-ms="100">

                            <span class="input-group-addon" ng-hide="asyncSelected_temp.exchange"><i ng-show="loadingLocations" class="icon-spinner2 spinner"></i></span>
                            <a class="input-group-btn" ng-show="asyncSelected_temp.exchange" ng-click="asyncSelected_temp = null"><i class="icon-cross2 text-danger"></i></a>
                        </div>
                        <div ng-show="noResults">
                            <i class="glyphicon glyphicon-remove"></i> No Results Found
                        </div>

                        <script type="text/ng-template" id="customTemplate.html">
                            <a>
                                <span ng-bind-html="match.model.company | uibTypeaheadHighlight:query"></span> ( <abbr class="text-size-mini" ng-bind-html="match.model.symbol | uibTypeaheadHighlight:query"></abbr> )
                                <strong class="pull-right" ng-bind-html="match.model.status| uppercase | uibTypeaheadHighlight:query"></strong>
                                <br>
                                <strong class="pull-left text-size-small" ng-bind-html="match.model.exchange | uppercase | uibTypeaheadHighlight:query"></strong>
                                <small ng-if="match.model.instrument !== ''" class="pull-right text-size-small ">Instrument: <i ng-bind-html="match.model.instrument| uppercase  | uibTypeaheadHighlight:query"></i></small>
                                <small ng-if="match.model.price !== ''" class="pull-right text-size-small ">Price: <i ng-bind-html="match.model.price | currency:'&#8377;':2"></i></small>
                            </a>
                        </script>

                    </div>

                    <div class="col-md-7"  ng-show="asyncSelected_temp.exchange && instrument !== 'stock'">
                        <div class="form-group col-md-4">
                            <label class="control-label" for="payment_mode">Expiry Date<span class="text-danger">*</span></label>
                            <select name="expiry" ng-options="e as e.name for e in expiries track by e.id" ng-model="expiry" ng-disabled="expiries.length == 0" class="form-control">
                                <option value="">Select Expiry</option>
                            </select>
                        </div>

                        <div ng-hide="instrument!='option'">
                            {!! BootForm::select('Type <span class="text-danger">*</span>', 'option_type', config('rothmans.sd_option_type'))->attribute('ng-model', "option_type")->attribute('ng-disabled', "instrument!='option' || expiry == null")->addGroupClass('col-md-4') !!}
                            <div class="form-group col-md-4">
                                <label class="control-label" for="payment_mode">Strike<span class="text-danger">*</span></label>
                                <select name="strike" ng-options="s as s.name for s in strikes track by s.id" ng-model="strike" ng-disabled="strikes.length == 0" class="form-control">
                                    <option value="">Select Strike</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title text-light">Open Services</h6>
            </div>
            <table class="table table-sm table-responsive">
                <thead>
                    <tr>
                        <th>Service Name</th>
                        <th>Customer Name</th>
                        <th>Qty</th>
                    </tr>
                </thead>
                <tbody ng-repeat="service in open_services | orderBy:'name'">
                    <tr ng-repeat="customer in service.cust_5plus.customers">
                        <th>@{{ service.name }}</th>
                        <td><a href="/customers/@{{customer.id}}" target="_blank">@{{customer.name}}</a></td>
                        <td>@{{ customer.service_deliveries.used_mapped_perten | number }}%</td>
                    </tr>
                    <tr ng-repeat="customer in service.cust_5less.customers">
                        <th>@{{ service.name }}</th>
                        <td>@{{customer.name}}</td>
                        <td>@{{ customer.service_deliveries.used_mapped_perten | number }}%</td>
                    </tr>
                </tbody>
                <tbody ng-hide="open_services.length">
                    <tr>
                        <td>No Service Available</td>
                    </tr>
                </tbody>

            </table>
        </div>
        {!! BootForm::close() !!}
    </div>
@endsection

@section('scripts')
    @parent

    <script type="text/javascript">

        app.filter('client_count', function(){
            return function(input, symbol, num){
                if(input.length > 0){
                    if(symbol == 'gt')
                        return input.filter(function(itm){ if(itm.service_deliveries.length == 0) return true; return itm.service_deliveries.used_mapped_perten >= num; });
                    else
                        return input.filter(function(itm){if(itm.service_deliveries.length == 0) return false; return itm.service_deliveries.used_mapped_perten < num && itm.service_deliveries.used_mapped_perten > 0; });
                }
                return [];
            }
        });

        app.controller('servicesController',function($scope, $http, $log, $document, client_countFilter, sweet, $window ){
            var servCtrl = this;

            $scope.getStocks = function(val, no_filter){
                var instrument = '';
                if(!no_filter){
                    no_filter = false;
                } else {
                    instrument = $scope.nfo_instrument;
                }

                return $http.post('{{url('service_delivery/get_stocks')}}', {
                    type: val, instrument: instrument ,no_group: no_filter
                }).then(function successCallback(response) {
                    if(no_filter)
                    {
                        $scope.stock_list = response.data;
                        return [];
                    }
                    else
                    {
                        return _.map(response.data, function (item) {
                            return {
                                symbol: item.symbol,
                                price: item.price,
                                exchange: item.exchange,
                                instrument: item.instrument,
                                company: item.company,
                                status: item.status
                            };
                        });
                    }
                }, function errorCallback(response) {
                    $scope.asyncSelected_temp = null;
                    $scope.symbolObj = null;
                });
            };




            $scope.getNfoStock = function(){
                if($scope.nfo_instrument == '' || $scope.asyncSelected_temp.symbol == ''){
                    return;
                }

                var data = { symbol: $scope.asyncSelected_temp.symbol, nfo_instrument: $scope.nfo_instrument, expiry_date: $scope.expiry.id };
                if($scope.instrument == 'option'){
                    data = {symbol: $scope.asyncSelected_temp.symbol, nfo_instrument: $scope.nfo_instrument, expiry_date: $scope.expiry.id, strike: $scope.strike.id, option_type: $scope.option_type };
                }

                $http.post('{{url('service_delivery/get_nfo_stocks')}}', data)
                    .then(function successCallback(response) {
                        $scope.symbolObj = response.data;

                        // for open service
                        $scope.getServices('exit', true);

                    }, function errorCallback(response) {
                        $scope.asyncSelected_temp = null;
                        $scope.symbolObj = null;
                    });
            };
            $scope.getServices = function(call_type, open){
                if(!$scope.symbolObj){
                    return false;
                }

                $scope.services = null;
                $scope.customers = [];
                var expiry = $scope.expiry.id;
                var nfo_instrument = $scope.nfo_instrument;
                var strike = $scope.strike.id;
                var option_type = ($scope.option_type == null) ? '' : $scope.option_type;
                var requestUrl = '{{url('service_delivery/get_open_services')}}';

                $http.post(requestUrl,{
                    call_type: call_type, symbol: $scope.symbolObj.symbol, exchange: $scope.exchange,nfo_instrument: nfo_instrument, expiry_date: expiry, strike: strike, option_type: option_type
                }).then(function successCallback(response) {
                    $scope.open_services = response.data;
                }, function errorCallback(response) {
                    $scope.open_services = null;

                });
            };


            /// varibles
            $scope.asyncSelected_temp = '';
            $scope.instrument = '';
            $scope.exchange = '';
            $scope.nfo_instrument = '';
            $scope.expiry = {id:''};
            $scope.default_strike = {id:0};
            $scope.open_services = [];

            // watch
            $scope.$watch('asyncSelected_temp', function(newval) {
                if(newval !== null && typeof newval === 'object')
                {
                    switch (newval.instrument) {
                        case 'FUTIDX':
                        case 'FUTIVX':
                        case 'FUTSTK':
                            $scope.instrument = 'future';
                            break;
                        case 'OPTIDX':
                        case 'OPTSTK':
                            $scope.instrument = 'option';
                            break;
                        default:
                            $scope.instrument = 'stock';
                    }

                    $scope.symbolObj = null;

                    $scope.exchange = newval.exchange;
                    $scope.nfo_instrument = newval.instrument;

                    if(newval.exchange == 'nfo'){
                        $scope.getStocks( newval.symbol, true);
                    }
                    if(newval.exchange != 'nfo'){
                        $scope.symbolObj = $scope.asyncSelected_temp;
                        $scope.getServices('exit', true);
                    }

                }
                else {
                    $scope.instrument = '';
                    $scope.exchange = '';
                    $scope.nfo_instrument = '';
                    $scope.symbolObj = null;
                    $scope.open_services = [];
                    $scope.option_type = null;
                    $scope.strikes = null;
                    $scope.strike = $scope.default_strike;
                    $scope.expiry = {id:''};
                    $scope.expiries = null;
                }
            });

            $scope.$watch('stock_list', function(){
                if($scope.stock_list) {
                    $scope.expiries =  _.map(_.uniqBy($scope.stock_list, 'expiry_date'), function (item) {
                        return {
                            id: item.expiry_date,
                            name: item.expiry_date
                        };
                    });
                }
            });
            $scope.$watch('expiry', function(newval) {
                if(newval == null) {
                    $scope.option_type = null;
                    $scope.strikes = null;
                    $scope.strike = $scope.default_strike;
                }
                if($scope.instrument == 'future'){
                    $scope.getNfoStock();
                }
            });

            $scope.$watch('option_type', function(newval) {
                $scope.strikes = null;
                $scope.strike = $scope.default_strike;
                if(newval != null){
                    var strike = _.filter($scope.stock_list,function (item) {
                        return (item.option_type.toUpperCase() === $scope.option_type.toUpperCase()
                        & item.expiry_date.toUpperCase() === $scope.expiry.id.toUpperCase());
                    });

                    $scope.strikes =  _.map(strike, function (item) {
                        return {
                            id: item.strike,
                            name: item.strike
                        };
                    });
                }
            });

            $scope.$watch('strike', function(newval) {
                if(newval !== null && typeof newval === 'object')
                {
                    $scope.getNfoStock();
                }
            });
        });

    </script>

@endsection