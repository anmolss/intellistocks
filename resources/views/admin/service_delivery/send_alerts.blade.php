@extends('admin.layout.app')

@section('page_title', 'Service Delivery')
@section('page_subtitle', 'Send Alerts')


@section('content')
    <div class="row" ng-controller="SendAlertController as alertCtrl" ng-cloak>
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Send Alerts</h6>
            </div>
            <div class="panel-body">
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="users">Customers Status</label>
                        <ui-select ng-model="$parent.sel_status" title="Chose Status">
                            <ui-select-match placeholder="Select Status...">@{{$select.selected.val}}</ui-select-match>
                            <ui-select-choices repeat="s in $parent.status track by $index">
                                <div ng-bind-html="s.val | highlight: $select.search"></div>
                            </ui-select-choices>
                        </ui-select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="users">Services</label>
                        <ui-select multiple tagging-label="Services"
                                   ng-model="$parent.sel_services"
                                   sortable="true"
                                   title="Services">
                            <ui-select-match placeholder="Add Services...">@{{$item.name}}</ui-select-match>
                            <ui-select-choices repeat="service in $parent.services | propsFilter: {name: $select.search}">
                                <div ng-bind-html="service.name | highlight: $select.search"></div>
                            </ui-select-choices>
                        </ui-select>
                    </div>

                    <div class="form-group" ng-show="sel_status.key == 'expired' || sel_status.key == 'expiring'">
                        <label class="control-label" for="users">Expiry Range</label>
                        {!! Form::text('expiry_range', null, ['class' => 'form-control expiry_range', 'ng-model' => 'expiry_range']) !!}
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="users">Relationship Manager</label>
                        <ui-select multiple tagging-label="Managers"
                                   ng-model="$parent.rel_manager"
                                   sortable="true"
                                   title="Services">
                            <ui-select-match placeholder="Selecte Managers...">@{{$item.val}}</ui-select-match>
                            <ui-select-choices repeat="man in $parent.managers | propsFilter: {val: $select.search}">
                                <div ng-bind-html="man.val | highlight: $select.search"></div>
                            </ui-select-choices>
                        </ui-select>
                    </div>

                    <div class="form-group">
                        <button type="button" ng-click="filter_user()" class="btn btn-primary">Filter <i class="icon-arrow-right14 position-right"></i></button>
                    </div>

                </div>



                <div class="col-md-6 border-left">
                    {!! BootForm::open()->post()->action(route('admin::service_delivery.post_alerts'))->name("send_alerts")->id("send_alerts")->attribute('novalidate angular-validator autocomplete', "off") !!}
                        <div class="form-group">
                            <label class="control-label" for="users">Customers</label>
                            <ui-select multiple tagging-label="(Add Customers)"
                                       ng-model="$parent.sel_users"
                                       sortable="true"
                                       title="Add new IP">
                                <ui-select-match placeholder="Add Customers...">@{{$item.name}}</ui-select-match>
                                <ui-select-choices repeat="user in $parent.users | propsFilter: {name: $select.search, username: $select.search}">
                                    <div ng-bind-html="user.name | highlight: $select.search"></div>
                                    <small ng-bind-html="user.email | highlight: $select.search"></small>
                                </ui-select-choices>
                            </ui-select>
                            <a href="javascript:void(0)" ng-click="sel_users = []">Clear All</a>
                            <a href="javascript:void(0)" class="pull-right">Count: @{{ sel_users.length | number }}</a>

                            {!! BootForm::hidden("users")->attribute('ng-value', 'list_users') !!}
                        </div>

                        <div class="form-group">
                            <label for="mode">Communication Mode <span class="text-danger">*</span></label>
                            <label class="checkbox-inline" ng-repeat="alert_mode in alert_modes">
                                <input type="checkbox" name="mode[]" apply-uniform ng-value="alert_mode.value"
                                       ng-model="alert_mode.selected" ng-change="optionToggled()">@{{ alert_mode.label }}
                            </label>
                            <label class="checkbox-inline" ><input type="checkbox" apply-uniform ng-model="isAllSelected" ng-click="toggleAll()" value="0">All</label>
                            {{--{!! BootForm::inlineCheckbox('System', 'mode[]')->value(0) !!}
                            {!! BootForm::inlineCheckbox('SMS', 'mode[]')->value(1)->addClass('styled')!!}
                            {!! BootForm::inlineCheckbox('Mail', 'mode[]')->value(2)->addClass('styled')!!}
                            {!! BootForm::inlineCheckbox('All', 'mode_all')->value(3)->checked()->addClass('styled')!!}--}}
                        </div>

                        {!! BootForm::textarea('Message', "message")->rows(3)->addClass('alert_message') !!}

                        @include('admin.partials.inline_template', ['add_msg_to' => 'alert_message', "type" => 'alert', 'lable_col_size' => false])

                    <div class="form-group">
                            {!! BootForm::submit('Broadcast <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right') !!}
                        </div>
                    {!! BootForm::close() !!}
                </div>
            </div>
            </div>
        </div>

    </div>
@endsection



@section('scripts')
    @parent

    <script type="text/javascript">
        $('.expiry_range').daterangepicker({
            applyClass: 'bg-info',
            cancelClass: 'btn-default',
            locale: {
                format: 'DD/MM/YYYY'
            }
        });

        app.filter('propsFilter', function() {
            return function(items, props) {
                var out = [];

                if (angular.isArray(items)) {
                    var keys = Object.keys(props);

                    items.forEach(function(item) {
                        var itemMatches = false;

                        for (var i = 0; i < keys.length; i++) {
                            var prop = keys[i];
                            var text = props[prop].toLowerCase();
                            if(item[prop] !== null) {
                                if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                                    itemMatches = true;
                                    break;
                                }
                            }
                        }

                        if (itemMatches) {
                            out.push(item);
                        }
                    });
                } else {
                    out = items;
                }

                return out;
            };
        });

        app.controller('SendAlertController',function($scope, $http, $log, $document, sweet, $window ){
            $scope.users = {!! json_encode($users) !!};

            $scope.services = {!! json_encode($services) !!};
            $scope.status = {!! json_encode($status) !!};
            $scope.managers = {!! json_encode($managers) !!};

            $scope.sel_users = [];

            $scope.$watch("sel_users", function (newValue) {
                $scope.list_users = _.map($scope.sel_users, 'id').join(', ');
                console.log($scope.list_users);
            });

            $scope.filter_user = function () {

                return $http.post('{{route('admin::send_alert.filter_user')}}', {
                    status : ($scope.sel_status) ? $scope.sel_status.key : '',
                    services : _.map($scope.sel_services, 'id'),
                    managers : _.map($scope.rel_manager, 'key'),
                    expiry_range : $scope.expiry_range,
                }).then(function successCallback(response) {
                    $scope.sel_users = response.data;
                }, function errorCallback(response) {
                    $scope.sel_users = [];
                });
            };


            $scope.alert_modes = [
                {value:'0',label: 'System', selected:true},
                {value:'1',label: 'SMS', selected:true},
                {value:'2',label: 'Email', selected:true}
            ];
            $scope.isAllSelected = true;

            $scope.toggleAll = function() {
                var toggleStatus = $scope.isAllSelected;
                angular.forEach($scope.alert_modes, function(itm){ itm.selected = toggleStatus; });

            }

            $scope.optionToggled = function(){
                $scope.isAllSelected = $scope.alert_modes.every(function(itm){ return itm.selected; })
            }


        });

    </script>

@endsection