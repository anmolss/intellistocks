<fieldset ng-controller="servicesController as servCtrl" ng-cloak>
    {!! BootForm::open()->post()->action(route('admin::service_delivery.store'))->name("service_delivery")->id("service_delivery")->attribute('ng-submit', 'preview(service_delivery)')->attribute('ng-validate', 'validationOptions')->attribute('autocomplete', "off") !!}
    <div class="row">
        <div class="col-md-12   ">
            @if($errors->count())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                {!! BootForm::select('Service Type', 'payment_mode', config('rothmans.payment_mode'))->attribute('ng-change', 'getServices(payment_mode)')->attribute('ng-model', 'payment_mode')->addGroupClass('col-md-4')!!}
                <div class="form-group col-md-4">
                    <label class="control-label" for="service_id">Service<span class="text-danger">*</span></label>
                    <select name="service_id" ng-options="service as service.name for service in services track by service.id" ng-model="service" ng-disabled="payment_mode == ''" class="form-control"></select>
                </div>

                <div class="form-group col-md-4">
                    <label class="control-label" for="payment_mode">Instrument<span class="text-danger">*</span></label>
                    <select name="instrument" ng-model="instrument" ng-disabled="payment_mode == ''" class="form-control">
                        @foreach(config('rothmans.instrument_type') as $key =>$val)
                            <option value="{{$key}}"
                                    @if($key == 'future' || $key == 'options')
                                    ng-if="payment_mode == 1"
                                    @endif
                            >{{$val}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-4">
                    <label class="control-label" for="exchange">Exchange<span class="text-danger">*</span></label>
                    <select name="exchange" ng-model="exchange" ng-disabled="!instrument" class="form-control">
                        @foreach(config('rothmans.exchange') as $key =>$val)
                            <option value="{{$key}}"
                                    @if($key == 'nfo')
                                    ng-if="instrument == 'future' || instrument == 'options'"
                                    @elseif($key != 'nfo')
                                    ng-if="instrument != 'future' && instrument != 'options'"
                                    @endif
                            >{{$val}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-4" ng-if="exchange=='nfo'">
                    <label class="control-label" for="nfo_intruments">NFO Instruments<span class="text-danger">*</span></label>
                    <select name="nfo_instrument" ng-model="servCtrl.nfo_intruments" class="form-control">
                        <option value="">Select NFO</option>
                        @foreach($nfo_intruments as $val)
                            <option value="{{$val}}"
                                    @if(0 === strpos($val, 'FUT'))
                                    ng-if="instrument == 'future'"
                                    @elseif(0 === strpos($val, 'OPT'))
                                    ng-if="instrument == 'options'"
                                    @endif
                            >{{$val}}</option>
                        @endforeach
                    </select>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    {!! BootForm::select('Call Type <span class="text-danger">*</span>', 'call_type', config('rothmans.sd_call_type'))->attribute('ng-model', 'call_type')->attribute('ng-disabled',"payment_mode == ''")->required() !!}
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label">Template <span class="text-danger">*</span></label>
                    <ui-select ng-model="$parent.sel_template" ui-select-required
                               sortable="true"
                               title="Select Template">
                        <ui-select-match placeholder="Select Template...">@{{$select.selected.name}}</ui-select-match>
                        <ui-select-choices repeat="template in $parent.templates track by $index"
                                           refresh-delay="20">
                            <div ng-bind-html="template.name | highlight: $select.search"></div>
                        </ui-select-choices>
                    </ui-select>
                    {!! BootForm::hidden('template_id', '')->attribute('ng-value', 'sel_template.id') !!}
                    {{--<small class="pull-right">{!! Html::link(url('/templates/create'), 'Create New Template', ['target' => '_blank']) !!}</small>--}}
                </div>
            </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Symbol <span class="text-danger">*</span></label>
                            <input type="text" name="symbol" ng-model="asyncSelected_temp" placeholder="Please start type stock name" uib-typeahead="address as address.symbol for address in getStocks($viewValue)" typeahead-loading="loadingLocations" typeahead-min-length="2" typeahead-no-results="noResults" class="form-control" typeahead-template-url="customTemplate.html" typeahead-on-select="">
                            <i ng-show="loadingLocations" class="glyphicon glyphicon-refresh"></i>
                            <div ng-show="noResults">
                                <i class="glyphicon glyphicon-remove"></i> No Results Found
                            </div>
                        </div>

                        <script type="text/ng-template" id="customTemplate.html">
                            <a>
                                <span ng-bind-html="match.model.symbol | uibTypeaheadHighlight:query"></span>
                                <span ng-bind-html="match.model.price" class="pull-right"></span>
                            </a>
                        </script>

                    </div>


                    <div class="col-md-8"  ng-hide="payment_mode == 0 || instrument == 'stocks'">
                        <div class="form-group col-md-4">
                            <label class="control-label" for="payment_mode">Expiry Date<span class="text-danger">*</span></label>
                            <select name="expiry" ng-options="expiry as expiry.name for expiry in expiries track by expiry.id" ng-model="expiry" ng-disabled="expiries.length == 0" class="form-control">
                                <option value="">Select Expiry</option>
                            </select>
                        </div>


                        <div ng-hide="instrument!='options'">
                            {!! BootForm::select('Type <span class="text-danger">*</span>', 'options_type', config('rothmans.sd_options_type'))->attribute('ng-model', "options_type")->attribute('ng-disabled', "instrument!='options'")->addGroupClass('col-md-4') !!}
                            <div class="form-group col-md-4">
                                <label class="control-label" for="payment_mode">Strike<span class="text-danger">*</span></label>
                                <select name="strike" ng-options="strike as strike.name for strike in strikes track by strike.id" ng-model="strike" ng-disabled="strikes.length == 0" class="form-control">
                                    <option value="">Select Strike</option>
                                </select>
                            </div>
                        </div>


                    </div>

                    <div class="col-md-12">
                        {!! BootForm::text('Price <span class="text-danger">*</span>', 'price')->attribute('ng-model', "asyncSelected.price")->id('price')->required()->addGroupClass('col-md-4') !!}
                        <div ng-hide="!price_range">
                            {!! BootForm::text('Range From <span class="text-danger">*</span>', 'range_from')->addGroupClass('col-md-4')->attribute('ng-model', "range_from")->id('range_from')->required() !!}
                            {!! BootForm::text('Range To <span class="text-danger">*</span>', 'range_to')->addGroupClass('col-md-4')->attribute('ng-model', "range_to")->id('range_to')->required() !!}
                        </div>
                        <br>
                        {!! BootForm::inlineCheckbox('Range', 'price_range')->attribute('ng-model', 'price_range')->value(1) !!}
                    </div>
                </div>





                <div class="row">
                    <div class="col-md-6" ng-if="instrument=='stocks' && payment_mode == '0'">
                        {!! BootForm::text('Qty (in %)<span class="text-danger">*</span>', 'qty')->attribute('ng-model', "servCtrl.qty")->required()->addGroupClass('col-md-8') !!}
                    </div>
                    <div class="col-md-6" ng-if="instrument=='stocks' && payment_mode == '1'">
                        {!! BootForm::text('Qty<span class="text-danger">*</span>', 'qty')->attribute('ng-model', "servCtrl.qty")->required()->addGroupClass('col-md-8') !!}
                        {!! BootForm::text('Value <span class="text-danger">*</span>', 'value')->attribute('ng-model', "servCtrl.qty_value")->required()->addGroupClass('col-md-4')->readonly() !!}
                    </div>

                    <div class="col-md-6" ng-if="instrument=='future'">
                        {!! BootForm::text('Lot Size <span class="text-danger">*</span>', 'qty_lot_size')->attribute('ng-model', "servCtrl.qty_lot_size")->defaultValue(2500)->required()->addGroupClass('col-md-4')->readonly()  !!}
                        {!! BootForm::text('Lots <span class="text-danger">*</span>', 'qty')->attribute('ng-model', "servCtrl.qty")->required()->addGroupClass('col-md-4') !!}
                        {!! BootForm::text('Value <span class="text-danger">*</span>', 'value')->attribute('ng-model', "servCtrl.qty_value")->required()->addGroupClass('col-md-4')->readonly() !!}
                    </div>

                    <div class="col-md-6" ng-if="instrument=='options'">
                        {!! BootForm::text('Lot Size <span class="text-danger">*</span>', 'qty_lot_size')->attribute('ng-model', "servCtrl.qty_lot_size")->defaultValue(2500)->required()->addGroupClass('col-md-4')->readonly() !!}
                        {!! BootForm::text('Lots <span class="text-danger">*</span>', 'qty')->attribute('ng-model', "servCtrl.qty")->required()->addGroupClass('col-md-4') !!}
                        {!! BootForm::text('Value <span class="text-danger">*</span>', 'value')->attribute('ng-model', "servCtrl.qty_value")->required()->addGroupClass('col-md-4')->readonly() !!}
                    </div>


                    {!! BootForm::text('Stop Loss', 'stop_loss')->addGroupClass('col-md-6')->attribute('ng-model', "stop_loss") !!}
                </div>


                <div class="row">
                    {!! BootForm::text('Date and Time','date_time')->addGroupClass('col-md-4')->attribute('ng-model', "servCtrl.date_time.date")->attribute('is-open', "servCtrl.date_time.open")->attribute('ng-focus', "openCalendar()")->attribute('datetime-picker', "yyyy-MM-dd HH:mm") !!}
                    {!! BootForm::text('Remark', 'remark')->addGroupClass('col-md-4')->attribute('ng-model', "remark") !!}
                    {!! BootForm::text('Signature', 'signature')->addGroupClass('col-md-4')->attribute('ng-model', "signature") !!}
                </div>

                <div class="row">
                    {!! BootForm::text('Target 1', 'target_1')->addGroupClass('col-md-6')->attribute('ng-model', "target_1") !!}
                    {!! BootForm::text('Target 2', 'target_2')->addGroupClass('col-md-6')->attribute('ng-model', "target_2") !!}
                </div>

                <div class="row form-group">
                    <label for="status">Mode <span class="text-danger">*</span></label>
                    {!! BootForm::inlineRadio('SMS', 'mode')->value(0)->addClass('styled') !!}
                    {!! BootForm::inlineRadio('Mail', 'mode')->value(1)->addClass('styled')!!}
                    {!! BootForm::inlineRadio('Both', 'mode')->value(2)->checked()->addClass('styled')!!}
                </div>

                <br><br><br><br>

                {!! BootForm::textarea('Message To Client', 'msg')->attribute('ng-model', 'client_msg')->rows(3)->required() !!}
        </div>

    </div>

    <div class="row form-group well well-sm">
        <div class="col-md-6">
            <div class="checkbox">
                <label class="control-label"><input apply-uniform type="checkbox" ng-click="toggleAll()" ng-model="isAllSelected">Select all</label>
            </div>
            <div class="checkbox" ng-repeat="cust_grp in customer_group_by_qty" ng-if="cust_grp.cust_count" >
                <label class="control-label"><input apply-uniform type="checkbox" ng-model="cust_grp.selected" ng-checked="cust_grp.selected"  ng-change="allocationToggled(cust_grp)">@{{ cust_grp.label }} Allocation (@{{ cust_grp.cust_count }}) suggested (@{{ ((servCtrl.qty * cust_grp.section)/100) | number:2 }}%)</label>
            </div>
        </div>
        <div class="col-md-6">
            <label class="control-label" for="customers">Customers</label>
            <strong class="pull-right"> @{{ (customers | filter:selected='true').length | number }} / @{{ customers.length | number }}</strong>

            <input type="search" value="" ng-model="filter_customer.name" class="form-control"  placeholder="filter customer..." aria-label="filter customer" />
            <div style="height: 225px; overflow: auto;">
                <div class="checkbox" ng-repeat="customer in customers | orderBy:'text' | filter:filter_customer">
                    <label class="control-label"><input apply-uniform type="checkbox" name="customers[@{{ customer.remaining_qty }}][@{{ customer.plan_id }}][]" ng-model="customer.selected" ng-checked="customer.selected" ng-value="customer.id" ng-change="optionToggled()">@{{ customer.name }}{{-- (<strong><em>Plan: </em></strong>@{{ customer.plan_name }}, <strong><em>Available Slab: </em></strong>@{{ customer.available_qty }}, <strong><em> Max Allowed: </em></strong>@{{ customer.available_qty }})--}}</label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div>
                <span class="text-danger">*</span> Required Fields
            </div>
        </div>
        <div class="col-md-6">
            {!! BootForm::button('Preview <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right')->attribute('ng-click', 'preview()')!!}
        </div>
    </div>

    {!! BootForm::close() !!}


    <script type="text/ng-template" id="final.html">
        <div class="modal-header">
            <h3 class="modal-title" id="modal-title">Message Preview</h3>
        </div>
        <div class="modal-body" id="modal-body">
            <dl class="dl-horizontal">
                <dt>Service Name</dt> <dd>@{{ $ctrl.service_txt }}</dd>

                <div ng-if="instrument=='stocks' && exchange!='nfo'">
                    <dt>Qty</dt> <dd>@{{ $ctrl.qty | number:2 }}%</dd>
                </div>
                <div ng-if="instrument!='stocks' || exchange!='nfo'">
                    <dt>Qty</dt> <dd>@{{ $ctrl.qty | number:0 }}</dd>
                    <dt>Value</dt> <dd>@{{ $ctrl.qty_value | number:2 }}</dd>
                </div>


                <dt>Broadcasting to</dt> <dd><strong>@{{ ($ctrl.customers | filter:selected='true').length | number }} / @{{ $ctrl.customers.length | number }}</strong> customers</dd>


                <dt>Message</dt> <dd>@{{ $ctrl.client_msg }}</dd>
            </dl>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" ng-if="$ctrl.client_msg" type="button" ng-click="$ctrl.ok()">OK</button>
            <button class="btn btn-warning" ng-if="$ctrl.client_msg" type="button" ng-click="$ctrl.cancel()">Cancel</button>
        </div>
    </script>

</fieldset>


@section('scripts')
    @parent

    <script type="text/javascript">

        /*$("select").select2();*/

        app.controller('servicesController',function($scope, $http, $uibModal,$log, $document ){
            var servCtrl = this;

            $scope.service = '';
            $scope.options_type = '';

            $scope.payment_mode = '';
            $scope.instruments_type = {!! json_encode(config('rothmans.instrument_type')) !!};
            $scope.exchanges = {!! json_encode(config('rothmans.exchange')) !!};

            $scope.customer_group_by_qty = [
                {section: "100", org_section: "100", label: "100%", selected: false, cust_count:0},
                {section: "75", org_section: "75", label: "75%", selected: false, cust_count:0},
                {section: "50", org_section: "50", label: "50%", selected: false, cust_count:0},
                {section: "25", org_section: "25", label: "25%", selected: false, cust_count:0},
                {section: "24.99", org_section: "24.99", label: "<25", selected: false, cust_count:0},
            ];


            $scope.getClient = function(){
                $scope.customers = [];
                var url_data = {'_token': '{{csrf_token()}}', 'service_id': $scope.service.id , 'symbol': $scope.asyncSelected.symbol, 'qty': servCtrl.qty, 'call_type': $scope.call_type }
                $http({
                    method: 'POST',
                    url: '{{url('service_delivery/get_client')}}',
                    data: url_data
                }).then(function successCallback(response) {
                    $scope.customers = response.data.customers;
                    if(response.data.error) {
                        show_stack_top_right('error', response.data.error);
                        return false;
                    }
                    angular.forEach($scope.customer_group_by_qty , function(value) {
                        value.cust_count=0;
                        value.section=value.org_section;
                    });

                    angular.forEach($scope.customers, function(item, key){
                        $scope.customers[key]['selected'] = false;

                        angular.forEach($scope.customer_group_by_qty , function(value) {
                            // Increment each number by one when you hit it
                            if ($scope.customers[key]['remaining_qty'] == value.org_section) {
                                if ($scope.customers[key]['available_qty'] != value.section)
                                    value.section = $scope.customers[key]['available_qty'];
                                value.cust_count++;
                            }
                        });
                    });
                }, function errorCallback(response) {

                });
            };
            $scope.getServices = function(){
                $http({
                    method: 'POST',
                    url: '{{url('service_delivery/get_services')}}',
                    data: {'_token': '{{csrf_token()}}', 'payment_mode': $scope.payment_mode }
                }).then(function successCallback(response) {
                    $scope.services = response.data;
                }, function errorCallback(response) {

                });
            };
            $scope.getTemplate = function(){
                $http({
                    method: 'POST',
                    url: '{{url('service_delivery/get_templates')}}',
                    data: {'_token': '{{csrf_token()}}', 'service_id': $scope.service.id , 'call_type': $scope.call_type }
                }).then(function successCallback(response) {
                    $scope.templates = response.data;
                }, function errorCallback(response) {

                });
            };

            $scope.getStocks = function(val, no_filter){
                if(!no_filter) no_filter = false;
                return $http.post('{{url('service_delivery/get_stocks')}}', {
                    '_token': '{{csrf_token()}}', symbol: $scope.exchange, type: val, nfo_intruments: servCtrl.nfo_intruments, no_group: no_filter
                }).then(function successCallback(response) {
                    if(no_filter)
                    {
                        $scope.stock_list = response.data;
                        return response.data;
                    }
                    else
                    {
                        return _.map(_.uniqBy(response.data , 'symbol'), function (item) {
                            return {
                                symbol: item.symbol,
                                price: item.price
                            };
                        });
                    }
                }, function errorCallback(response) {

                });
            };

            $scope.getNfoStock = function(){
                var data = {'_token': '{{csrf_token()}}', symbol: $scope.asyncSelected_temp.symbol, nfo_intruments: servCtrl.nfo_intruments, expiry_date: $scope.expiry.id };
                if($scope.instruments_type == 'options'){
                    data = {'_token': '{{csrf_token()}}', symbol: $scope.asyncSelected_temp.symbol, nfo_intruments: servCtrl.nfo_intruments, expiry_date: $scope.expiry.id, strike: $scope.strike.id, option_type: $scope.options_type };
                }

                $http({
                    method: 'POST',
                    url: '{{url('service_delivery/get_nfo_stocks')}}',
                    data: data,
                }).then(function successCallback(response) {
                    $scope.asyncSelected = response.data;
                }, function errorCallback(response) {

                });
            };

            $scope.validationOptions = {
                rules: {
                    customers: {
                        required: true
                    },
                    service_id: {
                        required: true
                    },
                    msg: {
                        required: true
                    }
                }
            };


            $scope.service = '';
            $scope.customers = [];
            $scope.exchange = 'bse';
            $scope.qty_type = 'qty';
            $scope.range_from = 0;
            $scope.range_to = 0;
            $scope.asyncSelected = null;
            $scope.signature = '- Intellistocks';

            servCtrl.qty_value = 0;
            servCtrl.date_time = {
                date: new Date()
            };

            servCtrl.qty_lot_size = 2500;


            $scope.filter_payment = function(keys){

                if($scope.payment_mode == 0 && keys == 'stock'){
                    return true;
                } if($scope.payment_mode == 1 && keys != 'stock') {
                    return true;
                }
                return false;
            };




            $scope.$watchGroup(['service', 'call_type'], function() {
                if($scope.service && $scope.call_type){
                    //$scope.getClient();
                    $scope.getTemplate();
                }
            });

            $scope.$watch('sel_template', function(newval) {
                if(newval){
                    $scope.client_msg = $scope.sel_template.body;
                    $scope.replaceTokenInPreview();
                }
            });

            $scope.$watch('asyncSelected_temp', function(newval) {
                if(newval)  {
                    if( $scope.instrument != 'stocks'){
                        $scope.getStocks( $scope.asyncSelected_temp.symbol, true);
                    }
                    else {
                        $scope.asyncSelected = $scope.asyncSelected_temp;
                    }
                }
            });

            $scope.$watch('stock_list', function(){
                if($scope.stock_list) {
                    $scope.expiries =  _.map(_.uniqBy($scope.stock_list, 'expiry_date'), function (item) {
                        return {
                            id: item.expiry_date,
                            name: item.expiry_date
                        };
                    });
                }
            });

            $scope.$watchGroup(['stock_list', 'asyncSelected_temp', 'expiry', 'option_type', 'strike'], function(newval) {

                if($scope.stock_list != '' && $scope.expiry  != ''&& $scope.option_type != '' && $scope.strike != '' )
                    $scope.getNfoStock()
            });


            $scope.$watch('expiry', function(newval) {
                if($scope.instrument != 'options'){
                    $scope.getNfoStock()
                }
            });

            $scope.$watch('options_type', function(newval) {
                $scope.strikes = [];
                if(newval != ''){
                    var strike = _.filter($scope.stock_list,function (item) {
                        return (item.option_type.toUpperCase() === $scope.options_type.toUpperCase()
                                & item.expiry_date.toUpperCase() === $scope.expiry.id.toUpperCase());
                    });

                    $scope.strikes =  _.map(strike, function (item) {
                            return {
                                id: item.strike,
                                name: item.strike
                            };
                    });

                }
            });

            $scope.$watch('price_range', function(newval) {
                if(typeof $scope.asyncSelected !== undefined && $scope.price_range)
                {
                    $scope.range_from = ($scope.asyncSelected.price * 0.95).toFixed(2);
                    $scope.range_to = ($scope.asyncSelected.price * 1.05).toFixed(2);
                }
            });


            $scope.$watchGroup(['servCtrl.qty', 'asyncSelected'], function(newval) {
                if(typeof $scope.asyncSelected !== undefined  && servCtrl.qty){
                    if($scope.exchange == 'nfo'){
                        if(!$scope.price_range){
                            servCtrl.qty_value = servCtrl.qty * servCtrl.qty_lot_size * $scope.asyncSelected.price;
                        }
                        else {
                            servCtrl.qty_value = servCtrl.qty * servCtrl.qty_lot_size * (($scope.range_from + $scope.range_to)/2);
                        }
                    } else {
                        if(!$scope.price_range){
                            servCtrl.qty_value = $scope.asyncSelected.price * servCtrl.qty;
                        }
                        else {
                            servCtrl.qty_value = $scope.asyncSelected.price * (($scope.range_from + $scope.range_to)/2);
                        }

                    }

                    $scope.getClient();


                    $scope.replaceTokenInPreview(servCtrl);
                }
            });


            $scope.$watchGroup(['sel_template', 'range_from', 'range_to', 'stop_loss', 'signature', 'remark', 'date_time', 'target_1', 'target_2' ], function() {
                if($scope.sel_template){
                    $scope.replaceTokenInPreview(servCtrl);
                }
            });

            $scope.preview = function () {
                if($scope.service_delivery.validate()) {
                    $scope.replaceTokenInPreview(servCtrl);
                    $scope.open();
                }
            };

            $scope.openCalendar = function(e, picker) {
                servCtrl.date_time.open = true;
            };


            $scope.toggleAll = function() {
                var toggleStatus = !$scope.isAllSelected;
                angular.forEach($scope.customers, function(itm){ itm.selected = toggleStatus; });

            };

            $scope.optionToggled = function(){
                $scope.isAllSelected = $scope.customers.every(function(itm){ return itm.selected; })
            };

            $scope.allocationToggled = function(allocation){
                var toggleStatus = allocation.selected;
                angular.forEach($scope.customers, function(itm){
                    if(allocation.org_section == itm.remaining_qty) { itm.selected = toggleStatus; }
                });
            };


            $scope.replaceTokenInPreview = function(servCtrl){
                var service =($scope.service != '') ? $scope.service.name : '[SERVICE]';
                var qty =(typeof servCtrl.qty != undefined) ? servCtrl.qty: '[QTY]';
                var range_from=($scope.range_from != undefined) ?$scope.range_from: '';
                var range_to=($scope.range_to != undefined) ?$scope.range_to: '';
                var stop_loss=($scope.stop_loss != undefined) ? $scope.stop_loss: '[STOPLOSS]';

                var target1 =($scope.target_1 != undefined) ? $scope.target_1: '[TARGET1]';
                var target2 =($scope.target_2 != undefined) ? $scope.target_2: '[TARGET2]';
                var symbol =($scope.asyncSelected != undefined) ? $scope.asyncSelected.symbol: '[SYMBOL]';
                var price=($scope.asyncSelected != undefined) ? $scope.asyncSelected.price: '[PRICE]';
                var fixed=($scope.price_range != undefined) ?$scope.price_range: '';
                var signature=($scope.signature != undefined) ?$scope.signature: '';
                var remark=($scope.remark != undefined) ?$scope.remark: '';
                var call_mode = ($scope.call_type != undefined) ?$scope.call_type: '';
                //var call_given=$('#call_given').val();
                //var bal_qty=  (call_mode == 'sell') ? ($('#bal_qty').text()-$('#qty').val()) : (parseFloat($('#bal_qty').text()) + parseFloat($('#qty').val()));
                //var prev_qty=$('#last_qty').val();
                //var prev_bal_price=$('#prev_bal_price').val();
                //var cum_qty=$('#cum_qty').val();
                //var avg_price=$('#call_avg_price').text();

                //var prev_date=$('#prev_date').val();
                //var stockopendate=$('#stockopendate').val();
                //var exit_avgprice=$('#exit_avgprice').val();

                if(fixed!='')
                {
                    var range = range_text = fixed;

                    var range_text = '@ '+range;
                }
                else
                {
                    var range = ((parseFloat(range_from) + parseFloat(range_to))/2);

                    var range_text = 'Range '+range_from+'-'+range_to;
                }


//                var earning='';
//
//                earning = (((call_mode == 'sell') ? (parseFloat(range) - parseFloat(avg_price)) : (parseFloat(avg_price) - parseFloat(range))) * Math.abs($('#bal_qty').text())).toFixed(2) ;
//
//                $('#call_net_result').text(earning);
//
//                earning = ((earning < 0) ? 'Loss ':'Profit ') + earning;


                var content = $scope.sel_template.body;

                content= content.replace(/\[SERVICE]/g, service).replace(/\[QTY]/g, qty).replace(/\[SYMBOL]/g, symbol).replace(/\[TARGET1]/g, target1).replace(/\[TARGET2]/g, target2).replace(/\[RANGE]/g, range_text).replace(/\[SIGNATURE]/g, signature).replace(/\[REMARK]/g, remark).replace(/\[STOPLOSS]/g, stop_loss).replace(/\[PRICE]/g, price);


                //content= content.replace(/\[CALL_GIVEN]/g, call_given).replace(/\[EARNING]/g, earning)

                if(typeof bal_qty !="undefined")
                {
                    content= content.replace(/\[BALQTY]/g, Math.abs(bal_qty));
                }

                if(typeof prev_qty !="undefined")
                {
                    content= content.replace(/\[PREVQTY]/g, prev_qty);
                }

                if(typeof cum_qty !="undefined")
                {
                    content= content.replace(/\[CUMQTY]/g, cum_qty);
                }

                if(typeof exit_avgprice !="undefined")
                {
                    content= content.replace(/\[AVG_PRICE]/g, exit_avgprice);
                }

                if(typeof prev_bal_price !="undefined")
                {
                    content= content.replace(/\[PREV_RANGE]/g, prev_bal_price);
                }

                if(typeof prev_date !="undefined")
                {
                    content= content.replace(/\[PREV_DATE]/g, prev_date);
                }

                if(typeof stockopendate !="undefined")
                {
                    content= content.replace(/\[STCKOPNDATE]/g, stockopendate);
                }

                if(typeof earning !="undefined")
                {
                    content= content.replace(/\[EARNING]/g, earning);
                }

                $scope.client_msg = content;
            };

            $scope.open = function (size, parentSelector) {
                var parentElem = parentSelector ?
                        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'final.html',
                    controller: 'ModalInstanceCtrl',
                    controllerAs: '$ctrl',
                    appendTo: parentElem,
                    resolve: {
                        client_msg: function () {
                            return $scope.client_msg;
                        },
                        customers: function () {
                            return $scope.customers;
                        },
                        qty: function () {
                            return servCtrl.qty;
                        },
                        qty_value: function () {
                            return servCtrl.qty_value;
                        },
                        service_txt: function () {
                            return $scope.service.name;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    document.getElementById("service_delivery").submit();
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };
        });


        app.controller('ModalInstanceCtrl', function ($uibModalInstance, client_msg, customers, qty, qty_value,service_txt) {
            var $ctrl = this;

            $ctrl.client_msg = client_msg;
            $ctrl.customers = customers;
            $ctrl.qty = qty;
            $ctrl.qty_value = qty_value;
            $ctrl.service_txt = service_txt;

            $ctrl.ok = function () {
                $uibModalInstance.close();

            };

            $ctrl.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        });

    </script>

@endsection