@extends('admin.layout.app')

@section('page_title', 'Service Delivery')
@section('page_subtitle', 'Edit Service Delivery')


@section('content')
    <div class="row" ng-controller="servicesController as servCtrl" ng-cloak>
        {!! BootForm::open()->post()->action(route('admin::service_delivery.store'))->name("service_delivery")->id("service_delivery")->attribute('novalidate angular-validator autocomplete', "off") !!}
        @if($errors->count())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Service Delivery <small class="text-bold">Edit</small></h6>
                <div class="heading-elements">
                    <a href="{{back()}}" class="btn btn-primary heading-btn text-white">Back <i class="fa fa-reply"></i> </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-5 form-group">
                        <label class="control-label">Symbol <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-line-chart"></i></span>
                            <input type="text" name="symbol" ng-readonly="asyncSelected_temp.exchange"  ng-model="asyncSelected_temp" placeholder="Please start type stock name"
                                   uib-typeahead="address as address.symbol for address in getStocks($viewValue)" typeahead-loading="loadingLocations"
                                   typeahead-min-length="3" typeahead-no-results="noResults" class="form-control" typeahead-template-url="customTemplate.html"
                                   typeahead-wait-ms="100">

                            <span class="input-group-addon" ng-hide="asyncSelected_temp.exchange"><i ng-show="loadingLocations" class="icon-spinner2 spinner"></i></span>
                            <a class="input-group-btn" ng-show="asyncSelected_temp.exchange" ng-click="asyncSelected_temp = null"><i class="icon-cross2 text-danger"></i></a>
                        </div>
                        <div ng-show="noResults">
                            <i class="glyphicon glyphicon-remove"></i> No Results Found
                        </div>

                        <script type="text/ng-template" id="customTemplate.html">
                            <a>
                                <span ng-bind-html="match.model.company | uibTypeaheadHighlight:query"></span> ( <abbr class="text-size-mini" ng-bind-html="match.model.symbol | uibTypeaheadHighlight:query"></abbr> )
                                <br>
                                <strong class="pull-left text-size-small" ng-bind-html="match.model.exchange | uppercase | uibTypeaheadHighlight:query"></strong>
                                <small ng-if="match.model.instrument !== ''" class="pull-right text-size-small ">Instrument: <i ng-bind-html="match.model.instrument| uppercase  | uibTypeaheadHighlight:query"></i></small>
                            </a>
                        </script>

                    </div>

                    <div class="col-md-5 form-group">
                        <label class="control-label" for="delivery_date">Delivery Date<span class="text-danger">*</span></label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input name="delivery_date" type="text" class="form-control" uib-datepicker-popup="yyyy-MM-dd" ng-model="delivery_date" is-open="open_cal" ng-required="true" close-text="Close" datepicker-options="" />
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-sm" ng-click="open_cal = true;"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-flat" ng-show="delivery_steps > 1">
            <div class="panel-heading">
                <h6 class="panel-title">Service Deliveries</h6>
            </div>
            <div class="table-responsive">
                <table class="table table-xxs">
                    <thead>
                    <tr>
                        <td></td>
                        <th>Stock Detail</th>
                        <th>Sent on</th>
                        <th>Call Type</th>
                        <th>Services</th>
                        <th>Customer</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr ng-show="service_deliveries.length" ng-repeat="delivery in service_deliveries"  ng-class="{'bg-teal-300' : selected_delivery === delivery.id}">
                            <td>
                                <input apply-uniform type="radio" name="delivery" ng-model="$parent.selected_delivery" ng-value="delivery.id">
                            </td>
                            <td class="col-md-2">@{{ delivery.symbol }} (<strong class="text-size-small" ng-bind-html="delivery.exchange | uppercase"></strong>)
                                <small ng-if="delivery.instrument === ''" class="pull-right text-size-small ">
                                    <br>
                                    (<span ng-bind-html="delivery.nfo_instrument| uppercase"></span>,
                                    <span ng-bind-html="delivery.expiry| date"></span>,
                                    <span ng-bind-html="delivery.option_type | uppercase"></span>,
                                    <span ng-bind-html="delivery.strike | number"></span>)
                                </small>
                            </td>
                            <td>@{{ delivery.created_at }}</td>
                            <td>@{{ delivery.call_type | uppercase }}</td>
                            <td>
                                <div ng-class="{'dropdown':delivery.services.length > 2}">
                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span ng-repeat="service in delivery.services | limitTo : 2">@{{ service.service.name }} <span ng-if="!$last">, </span></span>
                                        <span ng-show="delivery.services.length > 2">, @{{  delivery.services.length - 2 | number }} <i class="caret"></i></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-xs bg-slate-600">
                                        <li ng-repeat="service in delivery.services">
                                            <a>@{{ service.service.name }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                <div ng-class="{'dropdown':delivery.sd_users.length > 2}">
                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span ng-repeat="users in delivery.sd_users | limitTo : 2">@{{ user.user.name }} <span ng-if="!$last">, </span></span>
                                        <span ng-show="delivery.sd_users.length > 2">, @{{  delivery.sd_users.length - 2 | number }} <i class="caret"></i></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-xs bg-slate-600">
                                        <li ng-repeat="user in delivery.sd_users">
                                            <a>@{{ user.user.name }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td class="dropdown">@{{ delivery.sd_users.length | number }}</td>
                            <td>
                                <a>Recall</a> | <a ng-click="edit_delivery(delivery.id)">Edit price</a>
                            </td>
                        </tr>
                        <tr ng-hide="service_deliveries.length">
                            <td colspan="7" class="text-center">No Result Found</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="panel panel-flat" ng-show="delivery_steps > 2  && customers.length > 0">
            <div class="panel-body tabbable">
                <uib-tabset active="customer_tabindex">
                    <uib-tab index="service.id" ng-repeat="service in services | orderBy:'name' | filter:{selected:true}" select="sendby_service_func(service)" ng-if="(customers | filter:{service_id:service.id}:true).length > 0">
                        <uib-tab-heading>
                            @{{ service.name }} <i ng-show="service.loading" class="icon-spinner2 spinner"></i>
                        </uib-tab-heading>

                        <div class="col-md-12" ng-show="(customers | filter:{service_id:service.id}:true).length">
                            <div class="col-md-6">

                                <div class="checkbox" ng-repeat="cust_grp in customer_group_by_qty[service.id]" ng-if="cust_grp.cust_count && call_type != 'exit'" >
                                    <label class="control-label"><input apply-uniform type="checkbox" ng-model="cust_grp.selected" ng-change="allocationToggled(cust_grp, service.id)">
                                        @{{ cust_grp.label }} Allocation (@{{ cust_grp.cust_count }}) suggested
                                        <span ng-show="$parent.exchange == 'nse'">( @{{ ((service.quota * cust_grp.section)/100) | number:2 }}%)</span>
                                        <span ng-show="$parent.exchange == 'nfo'">( @{{ ((service.quota * cust_grp.section)/100) | number:0 }} Lot(s) )</span>
                                    </label>
                                </div>

                                <div class="checkbox" ng-if="call_type == 'exit' && (customers | filter:{service_id:service.id}:true | client_count:'gt':5).length >0" >
                                    <label class="control-label">
                                        <input apply-uniform type="checkbox" ng-model="customer_group_by_qty[service.id][0].selected" ng-change="allocationToggled(customer_group_by_qty[service.id][0], service.id, 'exit', '5plus')">
                                        @{{ (customers | filter:{service_id:service.id}:true | client_count:'gt':5).length }} customer have 5 and above %
                                    </label>
                                    <select name="service[@{{ service.id }}][qty_5plus]"
                                            ng-model="$parent.service.quota_5plus" ng-disabled="!service.selected" required>
                                        <option value="">Select Amount</option>
                                        <option value="25">25%</option>
                                        <option value="50">50%</option>
                                        <option value="75">75%</option>
                                        <option value="100">100%</option>
                                    </select>
                                </div>
                                <div class="checkbox" ng-if="call_type == 'exit' && (customers | filter:{service_id:service.id}:true | client_count:'lt':5).length >0" >
                                    <label class="control-label">
                                        <input apply-uniform type="checkbox" ng-model="customer_group_by_qty[service.id][1].selected" ng-change="allocationToggled(customer_group_by_qty[service.id][1], service.id, 'exit', '5less')">
                                        @{{ (customers | filter:{service_id:service.id}:true | client_count:'lt':5).length }} customer have below 5%
                                    </label>

                                    <select name="service[@{{ service.id }}][qty_5less]"
                                            ng-model="$parent.service.quota_5less" ng-disabled="!service.selected" required
                                            ng-show="(customers | filter:{service_id:service.id}:true | client_count:'lt':5).length >0">
                                        <option value="">Select Amount</option>
                                        <option value="25">25%</option>
                                        <option value="50">50%</option>
                                        <option value="75">75%</option>
                                        <option value="100">100%</option>
                                    </select>
                                </div>

                                <div class="row">
                                    <h6>Service Preview</h6>
                                    <table class="table table-borderless table-xxs">
                                        <tbody>
                                        <tr><th class="col-md-4">Broadcasting to</th> <td>@{{ ($parent.customers | filter:{selected:true,service_id:service.id}:true).length | number }} / @{{ ($parent.customers | filter:{service_id:service.id}:true).length | number }} customers</td></tr>

                                        <tr ng-if="$parent.$parent.exchange=='nfo'"><th>Qty</th> <td>@{{ service.quota | number:0 }}</td></tr>
                                        <tr ng-if="$parent.$parent.exchange=='nfo'"><th>Value</th> <td>@{{ service.value | number:2 }}</td></tr>

                                        <tr ng-if="$parent.$parent.exchange!='nfo' && $parent.$parent.call_type != 'exit'"><th>Qty</th> <td>@{{ service.quota | number:2 }}%</td></tr>
                                        <tr ng-if="$parent.$parent.exchange!='nfo' && $parent.$parent.call_type != 'exit'"><th>Message</th> <td>@{{ $parent.$parent.client_msg | previewMsg:service }}</td></tr>


                                        <tr ng-if="$parent.$parent.exchange!='nfo' && $parent.$parent.call_type == 'exit'">
                                            <td colspan="2" class="no-padding">
                                                <table class="table table-borderless table-xxs" ng-if="($parent.customers | filter:{service_id:service.id}:true | client_count:'gt':5).length >0">
                                                    <tr><th colspan="2">Available Quota 5% and above</th></tr>
                                                    <tr><th class="col-md-4">Qty</th> <td>@{{ service.quota_5plus | number:2 }}%</td></tr>
                                                    <tr><th>Message</th> <td>@{{ $parent.$parent.client_msg | previewMsg:service:service.quota_5plus }}</td></tr>
                                                </table>
                                                <table class="table table-borderless table-xxs" ng-if="($parent.customers | filter:{service_id:service.id}:true | client_count:'lt':5).length >0">
                                                    <tr><th colspan="2">Available Quota below 5%</th></tr>
                                                    <tr><th class="col-md-4">Qty</th> <td>@{{ service.quota_5less | number:2 }}%</td></tr>
                                                    <tr><th>Message</th> <td>@{{ $parent.$parent.client_msg | previewMsg:service:service.quota_5less }}</td></tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr ng-if="$parent.$parent.call_type != 'exit'">
                                            <td colspan="2" class="no-padding">
                                                <table class="table table-borderless table-xxs">
                                                    <tr><th colspan="2">RVR</th>    </tr>
                                                    <tr><th class="col-md-4">Risk in This Call</th> <td>@{{ service.risk }}</td></tr>
                                                    <tr><th>Reward from This Call</th> <td>@{{ service.reward  }}</td></tr>
                                                    <tr><th>Risk vs Reward Ratio</th> <td>@{{ service.rvr }}</td></tr>
                                                </table>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <label class="control-label" for="customers">Customers</label>
                                <strong class="pull-right"> @{{ (customers | filter:{selected:'true',service_id:service.id}).length | number }} / @{{ (customers | filter:{service_id:service.id}).length | number }}</strong>

                                <input type="search" value="" ng-model="filter_customer.name" class="form-control"  placeholder="filter customer..." aria-label="filter customer" />
                                <div style="height: 225px; overflow: auto;">
                                    <div class="checkbox" ng-repeat="customer in customers | orderBy:'text' | filter:filter_customer | filter:{service_id:service.id}: true ">
                                        <label class="control-label">
                                            <input apply-uniform type="checkbox"
                                                   name="customers[@{{ customer.available_qty }}][@{{ customer.service_id }}][@{{ customer.plan_id }}][]"
                                                   ng-model="customer.selected" ng-value="customer.id" ng-click="optionToggled('customer')">
                                            @{{ customer.name }} (<strong><em>Plan: </em></strong>@{{ customer.plan_name }})
                                        </label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12" ng-show="!(customers | filter:{service_id:service.id}:true).length">
                            <p>No Customer Available</p>
                        </div>

                    </uib-tab>
                </uib-tabset>
            </div>
        </div>

        <div class="panel panel-flat" ng-show="delivery_steps > 2 && customers.length > 0">
            <div class="panel-body">

                <div class="row">
                    {!! BootForm::text('Date and Time','date_time')->addGroupClass('col-md-3')->attribute('ng-model', "servCtrl.date_time.date")->attribute('is-open', "servCtrl.date_time.open")->attribute('ng-focus', "openCalendar()")->attribute('datetime-picker', "yyyy-MM-dd HH:mm") !!}
                    {!! BootForm::text('Remark', 'remark')->addGroupClass('col-md-5')->attribute('ng-model', "remark") !!}
                    {!! BootForm::text('Signature', 'signature')->addGroupClass('col-md-4')->attribute('ng-model', "signature") !!}
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="status">Mode <span class="text-danger">*</span></label>
                        {!! BootForm::inlineRadio('System', 'mode')->value(0)->addClass('styled') !!}
                        {!! BootForm::inlineRadio('SMS', 'mode')->value(1)->addClass('styled')!!}
                        {!! BootForm::inlineRadio('Mail', 'mode')->value(2)->addClass('styled')!!}
                        {!! BootForm::inlineRadio('All', 'mode')->value(3)->checked()->addClass('styled')!!}
                    </div>
                </div>

            </div>

            <div class="panel-footer">
                <div class="heading-elements">
                    <div class="heading-btn pull-left">
                        {!! BootForm::button('Deliver All <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-info')->attribute('ng-disabled', 'delivery_steps != 3')->type('submit')->attribute('ng-click', 'validate_form($event)')!!}
                    </div>
                    <div class="heading-btn pull-right">
                        {!! BootForm::button('Deliver <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-info')->attribute('ng-disabled', 'delivery_steps != 3')->attribute('ng-click', 'send_delivery(sendby_service)')->attribute('name', 'only_service')->attribute('ng-value', 'service.id')!!}
                    </div>
                </div>
            </div>
        </div>



        {!! BootForm::hidden('exchange','')->attribute('ng-value', 'exchange') !!}
        {!! BootForm::hidden('instrument','')->attribute('ng-value', 'instrument') !!}
        {!! BootForm::hidden('nfo_instrument','')->attribute('ng-value', 'nfo_instrument') !!}
        {!! BootForm::hidden('call_type','')->attribute('ng-value', 'call_type') !!}
        {!! BootForm::hidden('template_id', '')->attribute('ng-value', 'template.id') !!}
        {!! BootForm::hidden('msg', '')->attribute('ng-value', 'client_msg') !!}
        {!! BootForm::hidden('price', '')->attribute('ng-value', 'final_price') !!}

        {!! BootForm::close() !!}

        <script type="text/ng-template" id="uib/template/tabs/tabset.html">
            <div>
                <ul class="nav nav-@{{tabset.type || 'tabs'}} nav-lg nav-tabs-component bg-blue noripple"
                    ng-class="{'nav-stacked': vertical, 'nav-justified': justified}" ng-transclude></ul>
                <div class="tab-content">
                    <div class="tab-pane" ng-repeat="tab in tabset.tabs"
                         ng-class="{active: tabset.active === tab.index}"
                         uib-tab-content-transclude="tab">
                    </div>
                </div>
            </div>
        </script>
    </div>
@endsection



@section('scripts')
    @parent

    <script type="text/javascript">

        app.filter('previewMsg', function(){
            return function(input, service, quota){
                if(!quota)
                {
                    quota = service.quota;
                }
                return input.replace(/\[SERVICE]/g, service.name).replace(/\[QTY]/g, quota);
            }
        });

        app.filter('client_count', function(){
            return function(input, symbol, num){
                if(input.length > 0){
                    if(symbol == 'gt')
                        return input.filter(function(itm){ if(itm.service_deliveries.length == 0) return true; return itm.service_deliveries[0].used_limit >= num; });
                    else
                        return input.filter(function(itm){if(itm.service_deliveries.length == 0) return false; return itm.service_deliveries[0].used_limit < num; });
                }
                return [];
            }
        });

        app.controller('servicesController',function($scope, $http, $log, $document ){
            var servCtrl = this;

            $scope.getStocks = function(val, no_filter){
                var instrument = '';
                if(!no_filter){
                    no_filter = false;
                } else {
                    instrument = $scope.nfo_instrument;
                }

                return $http.post('{{url('service_delivery/get_stocks')}}', {
                    type: val, instrument: instrument ,no_group: no_filter
                }).then(function successCallback(response) {
                    if(no_filter)
                    {
                        $scope.stock_list = response.data;
                        return [];
                    }
                    else
                    {
                        return _.map(response.data, function (item) {
                            return {
                                symbol: item.symbol,
                                price: item.price,
                                exchange: item.exchange,
                                instrument: item.instrument,
                                company: item.company
                            };
                        });
                    }
                }, function errorCallback(response) {
                    $scope.asyncSelected_temp = null;
                    $scope.symbolObj = null;
                });
            };


            $scope.validate_form = function($event, validate_only){
                $event.preventDefault();
                var error = true;
                if(validate_only === null){
                    validate_only = true;
                }

                if($scope.services.filter(function(service){return service.selected && service.quota != '' && !isNaN(service.quota) && service.quota > 0;}).length == 0){
                    show_stack_top_right('error', 'No Services Selected');
                    error = false;
                    return error;
                }

                if(error){
                    angular.forEach($scope.services, function(service) {
                        if(service.selected === true && $scope.customers.filter(function(item){return service.id === item.service_id;}).length > 0){
                            if(service.selected === true && $scope.customers.filter(function(item){return service.id === item.service_id && item.selected === true;}).length == 0){
                                show_stack_top_right('error', 'No Client Selected in '+service.name);
                                if(error){
                                    $scope.customer_tabindex = service.id;
                                }
                                error = false;
                            }
                        }
                    });
                }

                if(error && !validate_only){
                    return document.getElementById("service_delivery").submit();
                }
                return error;
            };

            $scope.sendby_service_func = function(service) {
                $scope.sendby_service = service;
            };

            $scope.getServices = function(call_type, open){
                if(!$scope.symbolObj){
                    return false;
                }

                $scope.services = null;
                $scope.customers = [];
                var expiry = ($scope.expiry == null) ? '' : $scope.expiry.id;
                var nfo_instrument = $scope.nfo_instrument;
                var strike = ($scope.strike == null) ? 0 : $scope.strike.id;
                var option_type = ($scope.option_type == null) ? '' : $scope.option_type;

                var requestUrl = '{{url('service_delivery/get_open_services')}}';


                $http.post(requestUrl,{
                    call_type: call_type, symbol: $scope.symbolObj.symbol, exchange: $scope.exchange,nfo_instrument: nfo_instrument, expiry_date: option_type, strike: strike, option_type: option_type
                }).then(function successCallback(response) {
                    $scope.services = response.data;
                }, function errorCallback(response) {
                });
            };

            $scope.getServiceDeliveries = function(){
                if(!$scope.symbolObj && $scope.delivery_date){
                    return false;
                }

                $scope.services = null;
                $scope.customers = [];

                var requestUrl = '{{url('service_delivery/get_service_deliveries')}}';


                $http.post(requestUrl,{
                    symbol: $scope.symbolObj.symbol, exchange: $scope.exchange,nfo_instrument: $scope.nfo_instrument, delivery_date: $scope.delivery_date
                }).then(function successCallback(response) {
                    $scope.service_deliveries = response.data;
                }, function errorCallback(response) {

                });
            };

            $scope.openCalendar = function(e, picker) {
                servCtrl.date_time.open = true;
            };


            $scope.updateStep = function(step) {
                if(step == 1){
                    $scope.services = null;
                    $scope.customers = [];
                    $scope.stop_loss = undefined;
                    $scope.target_1 = undefined;
                    $scope.target_2 = undefined;

                } else if(step == 2){
                    $scope.customers = [];
                }
                $scope.delivery_steps = step;
            };

            $scope.update_value = function(service) {
                service.value = service.quota * servCtrl.qty_lot_size * $scope.final_price;
            };

            $scope.getCustomer = function(service, open){
                if(!service.quota || isNaN(service.quota)){
                    if($scope.call_type != 'exit'){
                        return false;
                    } else{
                        service.quota = 100;
                    }
                }
                if($scope.services.filter(function(item){return true === item.selected;}).length == 0)
                {
                    $scope.customers = [];
                }
                service.loading = true;

                $http.post('{{url('service_delivery/get_client')}}', {
                    'service_id': service.id , 'symbol': $scope.symbolObj.symbol, exchange: $scope.exchange, include_existing: $scope.include_existing, 'qty': service.quota, 'call_type': $scope.call_type, service: service, price: $scope.final_price, lot_size: servCtrl.qty_lot_size
                }).then(function successCallback(response) {
                    $scope.customers = $scope.customers.filter(function(item){return service.id !== item.service_id;});

                    service.include_existing = $scope.include_existing;

                    if(response.data.error) {
                        show_stack_top_right('warning', response.data.error);
                        service.loading = false;
                        return false;
                    }

                    $scope.customer_group_by_qty[service.id] = [];
                    var i = 0;

                    angular.forEach(response.data.customers, function(item, key){

                        if($scope.customer_group_by_qty[item.service_id].length == 0){
                            $scope.customer_group_by_qty[item.service_id] = angular.copy($scope.customer_group_by_qty_default);
                        }

                        angular.forEach($scope.customer_group_by_qty[item.service_id] , function(value) {
                            // Increament each number by one when you hit it
                            if (item['remaining_qty'] == value.org_section) {
                                value.section = item['available_qty'];
                                value.cust_count++;
                            }
                        });
                    });


                    $scope.customers = $scope.customers.concat(response.data.customers);

                    window.setTimeout(function(){
                        $scope.customer_tabindex = service.id;
                        service.loading = false;
                    }, 10);

                }, function errorCallback(response) {
                    if(response.data.error) {
                        show_stack_top_right('info', 'Error Occur. Please try again');
                        return false;
                    }
                });
            };

            $scope.getTemplate = function(){
                $http.post('{{url('service_delivery/get_templates')}}',{
                    call_type: $scope.call_type, exchange: $scope.exchange
                }).then(function successCallback(response) {
                    //$scope.templates = response.data;

                    $scope.template = response.data;
                    $scope.client_msg = response.data.body;

                }, function errorCallback(response) {
                    $scope.templates = '';
                });
            };


            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0,0,0,0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }

            $scope.cal_options = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.delivery_date = new Date();


            /// varibles
            $scope.asyncSelected_temp = '';
            $scope.instrument = '';
            $scope.exchange = '';
            $scope.nfo_instrument = '';
            $scope.expriry = null;
            $scope.range_from = 0;
            $scope.range_to = 0;
            $scope.delivery_steps = 1;
            $scope.isAllSelected = {
                service: false,
                customer: false
            };
            $scope.customers = [];
            $scope.sendby_service = null;
            $scope.tabindex = 2;
            $scope.customer_tabindex = 0;
            $scope.selected_delivery = 0;


            $scope.customer_group_by_qty_default = [
                {section: "100", org_section: "100", label: "100%", selected: false, cust_count:0},
                {section: "75", org_section: "75", label: "75%", selected: false, cust_count:0},
                {section: "50", org_section: "50", label: "50%", selected: false, cust_count:0},
                {section: "25", org_section: "25", label: "25%", selected: false, cust_count:0},
                {section: "24.99", org_section: "24.99", label: "<25", selected: false, cust_count:0},
            ];
            $scope.customer_group_by_qty = [];
            $scope.signature = '- Intellistocks';
            servCtrl.date_time = {
                date: new Date(),
                open: false
            };
            $scope.final_price = 0;

            // watch
            $scope.$watch('asyncSelected_temp', function(newval) {
                if(newval !== null && typeof newval === 'object')
                {
                    switch (newval.instrument) {
                        case 'FUTIDX':
                        case 'FUTIVX':
                        case 'FUTSTK':
                            $scope.instrument = 'future';
                            break;
                        case 'OPTIDX':
                        case 'OPTSTK':
                            $scope.instrument = 'option';
                            break;
                        default:
                            $scope.instrument = 'stock';
                    }

                    $scope.symbolObj = null;

                    $scope.exchange = newval.exchange;
                    $scope.nfo_instrument = newval.instrument;


                    $scope.symbolObj = $scope.asyncSelected_temp;

                    $scope.getServiceDeliveries();

                }
                else {
                    $scope.instrument = '';
                    $scope.exchange = '';
                    $scope.nfo_instrument = '';
                    $scope.symbolObj = null;
                    $scope.updateStep(1);
                }
            });

            $scope.$watch('stock_list', function(){
                if($scope.stock_list) {
                    $scope.expiries =  _.map(_.uniqBy($scope.stock_list, 'expiry_date'), function (item) {
                        return {
                            id: item.expiry_date,
                            name: item.expiry_date
                        };
                    });
                }
            });
            $scope.$watch('delivery_date', function(newval) {
                if(newval == null && !$scope.symbolObj ) {
                    return false;
                }
                $scope.getServiceDeliveries();
            });

            $scope.$watch('symbolObj', function(newval) {
                if(newval !== null && typeof newval === 'object')
                {
                    $scope.price_range = false;
                    $scope.range_from = (newval.price * 0.95).toFixed(2);
                    $scope.range_to = (newval.price * 1.05).toFixed(2);
                    $scope.updateStep(2);
                }
                else {
                    $scope.updateStep(1);
                }
            });

            $scope.$watch('customers', function (newval) {
                if( $scope.services && $scope.services.length && newval !== null && typeof newval === 'object')
                {
                    $scope.updateStep(3);
                }
            });

            $scope.$watchGroup(['customers','symbolObj', 'price_range', 'range_from', 'range_to', 'stop_loss', 'target_1', 'target_2', 'signature', 'remark'], function(newval) {

                if($scope.price_range) {
                    $scope.final_price =((parseFloat($scope.range_from) + parseFloat($scope.range_to)) / 2).toFixed(2);
                } else if($scope.symbolObj !== undefined && $scope.symbolObj!== [] && $scope.symbolObj !== null) {
                    $scope.final_price = parseFloat($scope.symbolObj.price);
                } else {
                    $scope.final_price = 0;
                }

                if($scope.services && $scope.services.length){

                    angular.forEach($scope.services, function(itm){
                        if(!isNaN(itm.quota) && itm.quota){
                            itm.value = itm.quota * servCtrl.qty_lot_size * $scope.final_price;
                            if($scope.stop_loss){
                                $scope.stop_loss = parseFloat($scope.stop_loss);
                                itm.risk = parseFloat(itm.quota * ($scope.final_price - $scope.stop_loss)).toFixed(2);
                            } else {
                                itm.risk = '-NA-';
                            }
                            if($scope.target_1){
                                $scope.target_1 = parseFloat($scope.target_1);
                                if($scope.target_2){
                                    $scope.target_2 = parseFloat($scope.target_2);
                                    itm.reward =  parseFloat(itm.quota * ((($scope.target_1+$scope.target_2)/2) - $scope.final_price)).toFixed(2);
                                    if($scope.stop_loss != ''){
                                        itm.rvr = parseFloat(((($scope.target_1+$scope.target_2)/2) - $scope.final_price)/($scope.final_price - $scope.stop_loss)).toFixed(2);
                                    } else {
                                        itm.rvr = '-NA-';
                                    }

                                } else {
                                    itm.reward =  parseFloat(itm.quota * ($scope.target_1 - $scope.final_price)).toFixed(2);
                                    if($scope.stop_loss != ''){
                                        itm.rvr = parseFloat(($scope.target_1 - $scope.final_price)/($scope.final_price - $scope.stop_loss)).toFixed(2);
                                    } else {
                                        itm.rvr = '-NA-';
                                    }
                                }
                            } else {
                                itm.reward = '-NA-';
                                itm.rvr = '-NA-';
                            }
                        }
                    });
                }

                if( $scope.customers && $scope.customers.length){
                    $scope.replaceTokenInPreview();
                }

            });



            $scope.preview = function () {
                if($scope.service_delivery.validate()) {
                    $scope.replaceTokenInPreview(servCtrl);
                    $scope.open();
                }
            };

            $scope.allocationToggled = function(allocation, service_id, exit, type){
                var toggleStatus = allocation.selected;

                if(exit){
                    if(type == '5plus') {
                        angular.forEach($scope.customers, function(itm){
                            if(service_id == itm.service_id
                                && itm.service_deliveries[0].used_limit >=5)
                            {
                                itm.selected = toggleStatus;
                            }
                        });

                    } else {
                        angular.forEach($scope.customers, function(itm){
                            if( service_id == itm.service_id
                                && itm.service_deliveries[0].used_limit < 5)
                            {
                                itm.selected = toggleStatus;
                            }
                        });
                    }
                } else {
                    angular.forEach($scope.customers, function(itm){
                        if(allocation.org_section == itm.remaining_qty
                            && service_id == itm.service_id) { itm.selected = toggleStatus; }
                    });
                }
            };


            $scope.replaceTokenInPreview = function(servCtrl){
                var range_from=($scope.range_from != undefined) ?$scope.range_from: '';
                var range_to=($scope.range_to != undefined) ?$scope.range_to: '';
                var stop_loss=($scope.stop_loss != undefined) ? $scope.stop_loss: '[STOPLOSS]';
                var target1 =($scope.target_1 != undefined) ? $scope.target_1: '[TARGET1]';
                var target2 =($scope.target_2 != undefined) ? $scope.target_2: '[TARGET2]';
                var symbol =($scope.symbolObj != undefined) ? $scope.symbolObj.symbol: '[SYMBOL]';
                var price=($scope.symbolObj != undefined) ? $scope.final_price: '[PRICE]';
                var fixed=(typeof $scope.price_range != undefined && $scope.price_range) ? false: true;
                var signature=($scope.signature != undefined) ?$scope.signature: '';
                var remark=($scope.remark != undefined) ?$scope.remark: '';

                if(fixed)
                {
                    var range = range_text = price;

                    var range_text = '@ '+range;
                }
                else
                {
                    var range = ((parseFloat(range_from) + parseFloat(range_to))/2);

                    var range_text = 'Range '+ range;
                }

                var content = $scope.template.body;
                content= content.replace(/\[SYMBOL]/g, symbol).replace(/\[TARGET1]/g, target1).replace(/\[TARGET2]/g, target2).replace(/\[RANGE]/g, range_text).replace(/\[SIGNATURE]/g, signature).replace(/\[REMARK]/g, remark).replace(/\[STOPLOSS]/g, stop_loss).replace(/\[PRICE]/g, price);
                $scope.client_msg = content;
            };

        });


        app.config(['$validationProvider', function ($validationProvider) {
            $validationProvider
                .setExpression({
                    validate_price_opt: function(value, $scope, element, attrs) {
                        if($scope.symbolObj !== null && typeof $scope.symbolObj === 'object'){
                            switch (attrs.prop){
                                case 'stop_loss':
                                    return parseFloat($scope.stop_loss) < parseFloat($scope.final_price);
                                    break;
                                case 'target_1':
                                    if($scope.target_1 == ''){
                                        return true;
                                    }
                                    return parseFloat($scope.target_1) > parseFloat($scope.final_price);
                                    break;
                                case 'target_2':
                                    if($scope.target_2 == ''){
                                        return true;
                                    }
                                    return parseFloat($scope.target_2) > parseFloat($scope.final_price)
                                        && parseFloat($scope.target_2) > parseFloat($scope.target_1);
                                    break;
                            }
                        }

                    },
                    validate_range: function(value, $scope, element, attrs) {
                        if($scope.symbolObj !== null && typeof $scope.symbolObj === 'object' && $scope.price_range){
                            return parseFloat($scope.range_from) < parseFloat($scope.range_to);
                        } else if(!$scope.price_range){
                            return true;
                        }
                    },
                    valid_quota: function(value, $scope, element, attrs) {
                        return !isNaN(value) || value === '';
                    }
                })
                .setDefaultMsg({
                    validate_price_opt: {
                        error: 'Invalid',
                        success: ''
                    },
                    validate_range: {
                        error: 'Range from must be less then Range To',
                        success: ''
                    },
                    valid_quota: {
                        error: 'Please provide valid number',
                        success: ''
                    }
                });
        }]);

    </script>

@endsection