
@if($errors->count())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title">Service Delivery <small class="text-bold">Add</small></h6>
        <div class="heading-elements">
            <a href="{{url()->previous()}}" class="btn btn-primary heading-btn text-white">Back <i class="fa fa-reply"></i> </a>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4 form-group">
                <label class="control-label">Stock <span class="text-danger">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-line-chart"></i></span>
                    <input type="text" name="symbol" ng-readonly="asyncSelected_temp.exchange"  ng-model="asyncSelected_temp" placeholder="Please start type stock name"
                           uib-typeahead="address as address.symbol for address in getStocks($viewValue)" typeahead-loading="loadingLocations"
                           typeahead-min-length="2" typeahead-no-results="noResults" class="form-control" typeahead-template-url="customTemplate.html"
                           typeahead-wait-ms="100">

                    <span class="input-group-addon" ng-hide="asyncSelected_temp.exchange"><i ng-show="loadingLocations" class="icon-spinner2 spinner"></i></span>
                    <a class="input-group-btn" ng-show="asyncSelected_temp.exchange" ng-click="asyncSelected_temp = null; servCtrl.qty_lot_size = null"><i class="icon-cross2 text-danger"></i></a>
                </div>
                <span class="help-block" ng-show="noResults">
                    <i class="glyphicon glyphicon-remove"></i> No Results Found
                </span>

                <script type="text/ng-template" id="customTemplate.html">
                    <a>
                        <span ng-bind-html="match.model.company | uibTypeaheadHighlight:query"></span> ( <abbr class="text-size-mini" ng-bind-html="match.model.symbol | uibTypeaheadHighlight:query"></abbr> )
                        <strong class="pull-right" ng-bind-html="match.model.status| uppercase | uibTypeaheadHighlight:query"></strong>
                        <br>
                        <strong class="pull-left text-size-small" ng-bind-html="match.model.exchange | uppercase | uibTypeaheadHighlight:query"></strong>
                        <small ng-if="match.model.instrument !== ''" class="pull-left text-size-small ">&nbsp;&nbsp;<i ng-bind-html="match.model.instrument| uppercase  | uibTypeaheadHighlight:query"></i></small>
                        <small ng-if="match.model.price !== ''" class="pull-right text-size-small "><i ng-bind-html="match.model.price | currency:'&#8377;':2"></i></small>
                    </a>
                </script>

            </div>

            <div class="col-md-8"  ng-show="asyncSelected_temp.exchange && instrument !== 'stock'">
                <div class="form-group col-md-3">
                    <label class="control-label" for="payment_mode">Expiry Date<span class="text-danger">*</span></label>
                    <select name="expiry" ng-options="e as e.name for e in expiries track by e.id" ng-model="expiry" ng-disabled="expiries.length == 0" class="form-control expiry-date-con">
                        <option value="">Select Expiry</option>
                    </select>
                </div>

                <div ng-hide="instrument!='option'">
                    {!! BootForm::select('Type <span class="text-danger">*</span>', 'option_type', config('rothmans.sd_option_type'))->attribute('ng-model', "option_type")->attribute('ng-disabled', "instrument!='option' || expiry == null")->addGroupClass('col-md-3') !!}
                    <div class="form-group col-md-3">
                        <label class="control-label" for="payment_mode">Strike<span class="text-danger">*</span></label>
                        <select name="strike" ng-options="s as s.name for s in strikes track by s.id" ng-model="strike" ng-disabled="strikes.length == 0" class="form-control">
                            <option value="">Select Strike</option>
                        </select>
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <label class="control-label">Lots Size</label>
                    <input type="text" class="form-control" ng-model="servCtrl.qty_lot_size" value="0" readonly="readonly">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat" ng-show="open_services.length">
    <div class="panel-heading">
        <h6 class="panel-title text-light">Open Services</h6>
    </div>
    <table class="table table-sm table-responsive" style="table-layout: fixed;">
        <tbody>
            <tr ng-repeat="service in open_services | orderBy:'name'">
                <th>@{{ service.name }}</th>
                <td><span class="dropdown" ng-show="service.cust_5less.count > 0">
                      <a class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span>@{{ service.cust_5less.count | number }} customer have below 5%</span>
                          <i class="caret"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-menu-xs bg-slate-600">
                        <li ng-repeat="customer in service.cust_5less.customers">
                          <a>@{{customer.name}}  (@{{ customer.service_deliveries.used_mapped_perten | number }}% )</a>
                        </li>
                      </ul>
                    </span>
                </td>
                <td><span class="dropdown" ng-show="service.cust_5plus.count > 0">
                      <a class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span>@{{ service.cust_5plus.count | number }} customer have 5 and above %</span>
                          <i class="caret"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-menu-xs bg-slate-600">
                        <li ng-repeat="customer in service.cust_5plus.customers">
                          <a>@{{customer.name}} (@{{ customer.service_deliveries.used_mapped_perten | number }}% )</a>
                        </li>
                      </ul>
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div class="panel panel-flat" ng-show="delivery_steps > 1" id="tabbed_pane">
    <div class="panel-body tabbable">
        <uib-tabset active="tabindex">
            <uib-tab index="3" heading="Buy" select="setCallType('new')">
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox-inline">
                            <label class="control-label"><input apply-uniform type="checkbox" ng-click="toggleAll('service')" ng-model="isAllSelected.service">Select all</label>
                        </div>
                        <div class="checkbox-inline">
                            <label class="control-label"><input apply-uniform type="checkbox" ng-model="$parent.$parent.include_existing" >Include Existing Customer</label>
                        </div>
                    </div>

                    <div class="row" ng-repeat="service in services | orderBy:'name'">
                        <div class="col-md-4">
                            <div class="checkbox">
                                <label class="control-label"><input apply-uniform type="checkbox" name="service[@{{ service.id }}][id]" ng-model="service.selected" ng-value="service.id" ng-checked="optionToggled('service')">@{{ service.name }}</label>

                                {!! BootForm::hidden('service[@{{ service.id }}][template_id]', '')->attribute('ng-value', 'templates[@{{ service.id }}].id')->attribute('ng-if', 'service.selected') !!}
                                {!! BootForm::hidden('msg[@{{ service.id }}]', '')->attribute('ng-value', 'client_msg[@{{ service.id }}]')->attribute('ng-if', 'service.selected') !!}
                            </div>
                        </div>
                        <div class="col-md-5" ng-if="$parent.exchange =='nse' && service.selected">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Quantity for @{{ service.name }} " name="service[@{{ service.id }}][qty]" ng-model="service.quota" ng-change="getCustomer(service)" ng-disabled="!service.selected" max="{{config('rothmans.allowed_stock_limit.max')}}" required  validator="valid_quota" valid-method="watch">
                                <span class="input-group-addon">%</span>
                                <span class="input-group-addon" ng-show="service.loading"><i class="icon-spinner2 spinner"></i></span>
                            </div>
                        </div>
                        <div class="col-md-8" ng-if="$parent.exchange =='nfo' && service.selected">
                            {!! BootForm::text('Lots <span class="text-danger">*</span>', 'service[@{{ service.id }}][qty]')->attribute('ng-model', "service.quota")->attribute('ng-change', "update_value(service);getCustomer(service);")->required()->addGroupClass('col-md-3') !!}
                            {!! BootForm::text('Value <span class="text-danger">*</span>', 'service[@{{ service.id }}][value]')->attribute('ng-model', "service.value")->required()->addGroupClass('col-md-4')->readonly() !!}
                            {{-- {!! BootForm::text('Lot Size <span class="text-danger">*</span>', 'service[@{{ service.id }}][lot_size]')->attribute('ng-model', "servCtrl.qty_lot_size")->defaultValue(2500)->required()->addGroupClass('col-md-3')->readonly()  !!}--}}
                            <div class="col-md-1">
                                <span class="input-group-addon" ng-show="service.loading"><i class="icon-spinner2 spinner"></i></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 border-left">
                        <h6 class="no-margin text-semibold text-center">Final Price: @{{ $parent.$parent.final_price | currency:"&#8377;" }} / @{{ symbolObj.price | currency:"&#8377;" }}</h6>
                        <span class="row"></span>
                        <div class="col-md-12">
                            <div ng-hide="!$parent.$parent.price_range" class="pull-right col-md-2" style="z-index: 2;position: relative;top: -8px;">
                                <input type="text" class="form-control" ng-change="changeRange()" ng-model="$parent.$parent.range_per"/>
                            </div>
                            <div class="pull-right">
                                {!! BootForm::inlineCheckbox('Range', 'price_range')->attribute('ng-model', '$parent.$parent.price_range')->value(1)->attribute('apply-uniform', true)->attribute('ng-change','change()')  !!}
                            </div>
                            {!! BootForm::text('Price <span class="text-danger">*</span>', 'price')->attribute('ng-model', '$parent.$parent.final_price')->id('price')->required() !!}
                        </div>
                        <div ng-hide="!$parent.$parent.price_range">
                            {!! BootForm::text('Range From <span class="text-danger">*</span>', 'range_from')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.range_from')->id('range_from')->required()->attribute('ng-change','change()')->attribute('validator', 'validate_range')->attribute('valid-method', 'watch')  !!}
                            {!! BootForm::text('Range To <span class="text-danger">*</span>', 'range_to')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.range_to')->id('range_to')->required()->attribute('ng-change','change()')->attribute('validator', 'validate_range')->attribute('valid-method', 'watch')  !!}
                        </div>

                        {!! BootForm::text('Stop Loss', 'stop_loss')->addGroupClass('col-md-12')->attribute('ng-model', '$parent.$parent.stop_loss')->attribute('validator', 'validate_price_opt')->attribute('valid-method', 'watch')->attribute('prop', 'stop_loss')->attribute('ng-change','change()')  !!}
                        {!! BootForm::text('Target 1', 'target_1')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.target_1')->attribute('validator', 'validate_price_opt')->attribute('valid-method', 'watch')->attribute('prop', 'target_1')->attribute('ng-change','change()')  !!}
                        {!! BootForm::text('Target 2', 'target_2')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.target_2')->attribute('validator', 'validate_price_opt')->attribute('valid-method', 'watch')->attribute('prop', 'target_2')->attribute('ng-change','change()')  !!}
                    </div>

            </uib-tab>
            <uib-tab index="2" id="exit" heading="Exit" ng-if="open_services.length > 0" select="setCallType('exit')">
                <div class="col-md-8">
                    {{--<div class="form-group">
                        <div class="checkbox-inline">
                            <label class="control-label"><input apply-uniform type="checkbox" ng-click="toggleAll('service')" ng-model="isAllSelected.service">Select all</label>
                        </div>
                    </div>--}}

                    <div class="row" ng-repeat="service in services | orderBy:'name'">
                        <div class="col-md-4">
                            <div class="checkbox">
                                <label class="control-label"><input apply-uniform type="checkbox" name="service[@{{ service.id }}][id]" ng-model="service.selected" ng-value="service.id" ng-change="exit_call_func(service)">@{{ service.name }}</label>
                                {!! BootForm::hidden('service[@{{ service.id }}][template_id]', '')->attribute('ng-value', 'templates[@{{ service.id }}].id')->attribute('ng-if', 'service.selected') !!}
                                {!! BootForm::hidden('msg[@{{ service.id }}]', '')->attribute('ng-value', 'client_msg[@{{ service.id }}]')->attribute('ng-if', 'service.selected') !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 border-left">
                        <h6 class="no-margin text-semibold text-center">Final Price: @{{ $parent.$parent.$parent.final_price | currency:"&#8377;" }} / @{{ $parent.symbolObj.price | currency:"&#8377;" }}</h6>
                        <span class="row"></span>
                        <div class="col-md-12">
                            <div class="pull-right">
                                {!! BootForm::inlineCheckbox('Range', 'price_range')->attribute('ng-model', '$parent.$parent.$parent.price_range')->value(1)->attribute('apply-uniform', true)->attribute('ng-change','change()')  !!}
                            </div>
                            {!! BootForm::text('Price <span class="text-danger">*</span>', 'price')->attribute('ng-model', '$parent.$parent.$parent.final_price')->id('price')->required()->attribute('ng-change','change()')   !!}
                        </div>
                        <div ng-hide="!$parent.$parent.$parent.price_range">
                            {!! BootForm::text('Range From <span class="text-danger">*</span>', 'range_from')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.$parent.range_from')->id('range_from')->required()->attribute('ng-change','change()')->attribute('validator', 'validate_range')->attribute('valid-method', 'watch')  !!}
                            {!! BootForm::text('Range To <span class="text-danger">*</span>', 'range_to')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.$parent.range_to')->id('range_to')->required()->attribute('ng-change','change()')->attribute('validator', 'validate_range')->attribute('valid-method', 'watch')  !!}
                        </div>
                    </div>
            </uib-tab>
            <uib-tab index="1" id="Rentry" heading="Rentry" ng-if="open_services.length > 0" select="setCallType('re_entry')">
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox-inline">
                            <label class="control-label"><input apply-uniform type="checkbox" ng-click="toggleAll('service')" ng-model="isAllSelected.service">Select all</label>
                        </div>
                    </div>

                    <div class="row" ng-repeat="service in services | orderBy:'name'">
                        <div class="col-md-4">
                            <div class="checkbox">
                                <label class="control-label"><input apply-uniform type="checkbox" name="service[@{{ service.id }}][id]" ng-model="service.selected" ng-value="service.id" ng-checked="optionToggled('service')">@{{ service.name }}</label>
                                {!! BootForm::hidden('service[@{{ service.id }}][template_id]', '')->attribute('ng-value', 'templates[@{{ service.id }}].id')->attribute('ng-if', 'service.selected') !!}
                                {!! BootForm::hidden('msg[@{{ service.id }}]', '')->attribute('ng-value', 'client_msg[@{{ service.id }}]')->attribute('ng-if', 'service.selected') !!}
                            </div>
                        </div>
                        <div class="col-md-6" ng-if="$parent.$parent.exchange =='nse' && service.selected">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Quantity for @{{ service.name }} " name="service[@{{ service.id }}][qty]" ng-model="service.quota" ng-change="getCustomer(service)" ng-disabled="!service.selected" max="{{config('rothmans.allowed_stock_limit.max')}}" required  validator="valid_quota" valid-method="watch">
                                <span class="input-group-addon">%</span>
                                <span class="input-group-addon" ng-show="service.loading"><i class="icon-spinner2 spinner"></i></span>
                            </div>
                        </div>
                        <div class="col-md-8" ng-if="$parent.$parent.exchange =='nfo' && service.selected">
                            {!! BootForm::text('Lots <span class="text-danger">*</span>', 'service[@{{ service.id }}][qty]')->attribute('ng-model', "service.quota")->attribute('ng-change', "update_value(service);getCustomer(service);")->required()->addGroupClass('col-md-3') !!}
                            {!! BootForm::text('Value <span class="text-danger">*</span>', 'service[@{{ service.id }}][value]')->attribute('ng-model', "service.value")->required()->addGroupClass('col-md-4')->readonly() !!}
                            {!! BootForm::text('Lot Size <span class="text-danger">*</span>', 'service[@{{ service.id }}][lot_size]')->attribute('ng-model', "servCtrl.qty_lot_size")->defaultValue(2500)->required()->addGroupClass('col-md-3')->readonly()  !!}
                            <div class="input-group col-md-1">
                                <span class="input-group-addon" ng-show="service.loading"><i class="icon-spinner2 spinner"></i></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 border-left">
                    <h6 class="no-margin text-semibold text-center">Final Price: @{{ $parent.$parent.$parent.final_price | currency:"&#8377;" }} / @{{ $parent.symbolObj.price | currency:"&#8377;" }}</h6>
                    <span class="row"></span>
                    <div class="col-md-12">
                        <div class="pull-right">
                            {!! BootForm::inlineCheckbox('Range', 'price_range')->attribute('ng-model', '$parent.$parent.$parent.price_range')->value(1)->attribute('apply-uniform', true)->attribute('ng-change','change()')  !!}
                        </div>
                        {!! BootForm::text('Price <span class="text-danger">*</span>', 'price')->attribute('ng-model', '$parent.$parent.$parent.final_price')->id('price')->required()->attribute('ng-change','change()')   !!}
                    </div>
                    <div ng-hide="!$parent.$parent.$parent.price_range">
                        {!! BootForm::text('Range From <span class="text-danger">*</span>', 'range_from')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.$parent.range_from')->id('range_from')->required()->attribute('ng-change','change()')->attribute('validator', 'validate_range')->attribute('valid-method', 'watch')  !!}
                        {!! BootForm::text('Range To <span class="text-danger">*</span>', 'range_to')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.$parent.range_to')->id('range_to')->required()->attribute('ng-change','change()')->attribute('validator', 'validate_range')->attribute('valid-method', 'watch')  !!}
                    </div>

                    {!! BootForm::text('Stop Loss', 'stop_loss')->addGroupClass('col-md-12')->attribute('ng-model', '$parent.$parent.$parent.stop_loss')->attribute('validator', 'validate_price_opt')->attribute('valid-method', 'watch')->attribute('prop', 'stop_loss')->attribute('ng-change','change()')  !!}
                    {!! BootForm::text('Target 1', 'target_1')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.$parent.target_1')->attribute('validator', 'validate_price_opt')->attribute('valid-method', 'watch')->attribute('prop', 'target_1')->attribute('ng-change','change()')  !!}
                    {!! BootForm::text('Target 2', 'target_2')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.$parent.target_2')->attribute('validator', 'validate_price_opt')->attribute('valid-method', 'watch')->attribute('prop', 'target_2')->attribute('ng-change','change()')  !!}
                </div>
            </uib-tab>

            <uib-tab index="0" heading="Sell" ng-hide="instrument!='option'" select="setCallType('new')">
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox-inline">
                            <label class="control-label"><input apply-uniform type="checkbox" ng-click="toggleAll('service')" ng-model="isAllSelected.service">Select all</label>
                        </div>
                        <div class="checkbox-inline">
                            <label class="control-label"><input apply-uniform type="checkbox" ng-model="$parent.$parent.include_existing" >Include Existing Customer</label>
                        </div>
                    </div>

                    <div class="row" ng-repeat="service in services | orderBy:'name'">
                        <div class="col-md-4">
                            <div class="checkbox">
                                <label class="control-label"><input apply-uniform type="checkbox" name="service[@{{ service.id }}][id]" ng-model="service.selected" ng-value="service.id" ng-checked="optionToggled('service')">@{{ service.name }}</label>

                                {!! BootForm::hidden('service[@{{ service.id }}][template_id]', '')->attribute('ng-value', 'templates[@{{ service.id }}].id')->attribute('ng-if', 'service.selected') !!}
                                {!! BootForm::hidden('msg[@{{ service.id }}]', '')->attribute('ng-value', 'client_msg[@{{ service.id }}]')->attribute('ng-if', 'service.selected') !!}
                            </div>
                        </div>
                        <div class="col-md-5" ng-if="$parent.exchange =='nse' && service.selected">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Quantity for @{{ service.name }} " name="service[@{{ service.id }}][qty]" ng-model="service.quota" ng-change="getCustomer(service)" ng-disabled="!service.selected" max="{{config('rothmans.allowed_stock_limit.max')}}" required  validator="valid_quota" valid-method="watch">
                                <span class="input-group-addon">%</span>
                                <span class="input-group-addon" ng-show="service.loading"><i class="icon-spinner2 spinner"></i></span>
                            </div>
                        </div>
                        <div class="col-md-8" ng-if="$parent.exchange =='nfo' && service.selected">
                            {!! BootForm::text('Lots <span class="text-danger">*</span>', 'service[@{{ service.id }}][qty]')->attribute('ng-model', "service.quota")->attribute('ng-change', "update_value(service);getCustomer(service);")->required()->addGroupClass('col-md-3') !!}
                            {!! BootForm::text('Value <span class="text-danger">*</span>', 'service[@{{ service.id }}][value]')->attribute('ng-model', "service.value")->required()->addGroupClass('col-md-4')->readonly() !!}
                            {{-- {!! BootForm::text('Lot Size <span class="text-danger">*</span>', 'service[@{{ service.id }}][lot_size]')->attribute('ng-model', "servCtrl.qty_lot_size")->defaultValue(2500)->required()->addGroupClass('col-md-3')->readonly()  !!}--}}
                            <div class="col-md-1">
                                <span class="input-group-addon" ng-show="service.loading"><i class="icon-spinner2 spinner"></i></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 border-left">
                    <h6 class="no-margin text-semibold text-center">Final Price: @{{ $parent.$parent.final_price | currency:"&#8377;" }} / @{{ symbolObj.price | currency:"&#8377;" }}</h6>
                    <span class="row"></span>
                    <div class="col-md-12">
                        <div class="pull-right">
                            {!! BootForm::inlineCheckbox('Range', 'price_range')->attribute('ng-model', '$parent.$parent.price_range')->value(1)->attribute('apply-uniform', true)->attribute('ng-change','change()')  !!}
                        </div>
                        {!! BootForm::text('Price <span class="text-danger">*</span>', 'price')->attribute('ng-model', '$parent.$parent.final_price')->id('price')->required() !!}
                    </div>
                    <div ng-hide="!$parent.$parent.price_range">
                        {!! BootForm::text('Range From <span class="text-danger">*</span>', 'range_from')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.range_from')->id('range_from')->required()->attribute('ng-change','change()')->attribute('validator', 'validate_range')->attribute('valid-method', 'watch')  !!}
                        {!! BootForm::text('Range To <span class="text-danger">*</span>', 'range_to')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.range_to')->id('range_to')->required()->attribute('ng-change','change()')->attribute('validator', 'validate_range')->attribute('valid-method', 'watch')  !!}
                    </div>

                    {!! BootForm::text('Stop Loss', 'stop_loss')->addGroupClass('col-md-12')->attribute('ng-model', '$parent.$parent.stop_loss')->attribute('validator', 'validate_price_opt')->attribute('valid-method', 'watch')->attribute('prop', 'stop_loss')->attribute('ng-change','change()')  !!}
                    {!! BootForm::text('Target 1', 'target_1')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.target_1')->attribute('validator', 'validate_price_opt')->attribute('valid-method', 'watch')->attribute('prop', 'target_1')->attribute('ng-change','change()')  !!}
                    {!! BootForm::text('Target 2', 'target_2')->addGroupClass('col-md-6')->attribute('ng-model', '$parent.$parent.target_2')->attribute('validator', 'validate_price_opt')->attribute('valid-method', 'watch')->attribute('prop', 'target_2')->attribute('ng-change','change()')  !!}
                </div>

            </uib-tab>

            <uib-tab index="4" heading="Custom Message" classes="pull-right" select="alert('custom')" >
                <p>Custom Template</p>
            </uib-tab>
        </uib-tabset>
    </div>
</div>

<div class="panel panel-flat" ng-show="delivery_steps > 2  && customers.length > 0">
    <div class="panel-body tabbable">
        <uib-tabset active="customer_tabindex">
            <uib-tab id="service_tab" index="service.id" ng-repeat="service in services | orderBy:'name' | filter:{selected:true}" select="sendby_service_func(service)" ng-if="(customers | filter:{service_id:service.id}:true).length > 0">
                <uib-tab-heading>
                    @{{ service.name }} <i ng-show="service.loading" class="icon-spinner2 spinner"></i>
                </uib-tab-heading>

                <div class="col-md-12" ng-show="(customers | filter:{service_id:service.id}:true).length">
                    <div class="col-md-6">

                        <div class="checkbox" ng-repeat="cust_grp in customer_group_by_qty[service.id]" ng-if="cust_grp.cust_count && call_type != 'exit'" >
                            <label class="control-label"><input apply-uniform type="checkbox" ng-model="cust_grp.selected" ng-change="allocationToggled(cust_grp, service.id)">@{{ cust_grp.label }} Allocation (@{{ cust_grp.cust_count }}) suggested <span ng-show="$parent.exchange == 'nse'">( @{{ ((service.quota * cust_grp.section)/100) | num }}%)</span><span ng-show="$parent.exchange == 'nfo'">( @{{ ((service.quota * cust_grp.section)/100) | number:0 }} Lot(s) )</span></label>
                        </div>

                        <div class="checkbox" ng-if="call_type == 'exit' && (customers | filter:{service_id:service.id}:true | client_count:'gt':5).length >0" >
                            <label class="control-label"><input apply-uniform type="checkbox" ng-model="customer_group_by_qty[service.id][0].selected" ng-change="allocationToggled(customer_group_by_qty[service.id][0], service.id, 'exit', '5plus')">@{{ (customers | filter:{service_id:service.id}:true | client_count:'gt':5).length }} customer have 5 and above %</label>
                            <select name="service[@{{ service.id }}][qty_5plus]"
                                    ng-model="$parent.service.quota_5plus" ng-disabled="!service.selected" required>
                                <option value="">Select Amount</option>
                                <option value="25">25%</option>
                                <option value="50">50%</option>
                                <option value="75">75%</option>
                                <option value="100">100%</option>
                            </select>
                        </div>
                        <div class="checkbox" ng-if="call_type == 'exit' && (customers | filter:{service_id:service.id}:true | client_count:'lt':5).length >0" >
                            <label class="control-label"><input apply-uniform type="checkbox" ng-model="customer_group_by_qty[service.id][1].selected" ng-change="allocationToggled(customer_group_by_qty[service.id][1], service.id, 'exit', '5less')">@{{ (customers | filter:{service_id:service.id}:true | client_count:'lt':5).length }} customer have below 5%</label>

                            <select name="service[@{{ service.id }}][qty_5less]"
                                    ng-model="$parent.service.quota_5less" ng-disabled="!service.selected" required
                                    ng-show="(customers | filter:{service_id:service.id}:true | client_count:'lt':5).length >0">
                                <option value="">Select Amount</option>
                                <option value="25">25%</option>
                                <option value="50">50%</option>
                                <option value="75">75%</option>
                                <option value="100">100%</option>
                            </select>
                        </div>

                        <div class="row">
                            <h6>Service Preview</h6>
                            <table class="table table-borderless table-xxs">
                                <tbody>
                                <tr><th class="col-md-4">Broadcasting to</th> <td>@{{ ($parent.customers | filter:{selected:true,service_id:service.id}:true).length | number }} / @{{ ($parent.customers | filter:{service_id:service.id}:true).length | number }} customers</td></tr>

                                <tr ng-if="$parent.$parent.exchange=='nfo'"><th>Qty</th> <td>@{{ service.quota | number:0 }} lot(s)</td></tr>
                                <tr ng-if="$parent.$parent.exchange=='nfo'"><th>Value</th> <td>@{{ service.value | number }}</td></tr>
                                <tr ng-if="$parent.$parent.exchange=='nfo'"><th>Message</th> <td>@{{ $parent.$parent.client_msg[service.id] | previewMsg:service }}</td></tr>

                                <!--<tr ng-if="$parent.$parent.exchange!='nfo' && $parent.$parent.call_type != 'exit'"><th>Qty</th> <td>@{{ service.quota | number }}% / @{{ service.quota_stock | number:0 }}</td></tr>-->
                                <tr ng-if="$parent.$parent.exchange!='nfo' && $parent.$parent.call_type != 'exit'"><th>Qty</th> <td>@{{ service.quota | number }}%</td></tr>
                                <tr ng-if="$parent.$parent.exchange!='nfo' && $parent.$parent.call_type != 'exit'"><th>Message</th> <td>@{{ $parent.$parent.client_msg[service.id] | previewMsg:service }}</td></tr>


                                <tr ng-if="$parent.$parent.exchange!='nfo' && $parent.$parent.call_type == 'exit'">
                                    <td colspan="2" class="no-padding">
                                        <table class="table table-borderless table-xxs" ng-if="($parent.customers | filter:{service_id:service.id}:true | client_count:'gt':5).length >0">
                                            <tr><th colspan="2">Available Quota 5% and above</th></tr>
                                            <tr><th class="col-md-4">Qty</th> <td>@{{ service.quota_5plus | number }}%</td></tr>
                                            <tr><th>Message</th> <td>@{{ $parent.$parent.client_msg[service.id] | previewMsg:service:service.quota_5plus }}</td></tr>
                                        </table>
                                        <table class="table table-borderless table-xxs" ng-if="($parent.customers | filter:{service_id:service.id}:true | client_count:'lt':5).length >0">
                                            <tr><th colspan="2">Available Quota below 5%</th></tr>
                                            <tr><th class="col-md-4">Qty</th> <td>@{{ service.quota_5less | number }}%</td></tr>
                                            <tr><th>Message</th> <td>@{{ $parent.$parent.client_msg[service.id] | previewMsg:service:service.quota_5less }}</td></tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr ng-if="$parent.$parent.call_type != 'exit'">
                                    <td colspan="2" class="no-padding">
                                        <table class="table table-borderless table-xxs">
                                            <tr><th colspan="2">RVR</th>    </tr>
                                            <tr><th class="col-md-4">Risk in This Call</th> <td>@{{ service.risk }}</td></tr>
                                            <tr><th>Reward from This Call</th> <td>@{{ service.reward  }}</td></tr>
                                            <tr><th>Risk vs Reward Ratio</th> <td>@{{ service.rvr }}</td></tr>
                                        </table>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <label class="control-label" for="customers">Customers</label>
                        <strong class="pull-right"> @{{ (customers | filter:{selected:'true',service_id:service.id}).length | number }} / @{{ (customers | filter:{service_id:service.id}).length | number }}</strong>

                        <input type="search" value="" ng-model="filter_customer.name" class="form-control"  placeholder="filter customer..." aria-label="filter customer" />
                        <div style="height: 225px; overflow: auto;">
                            <div class="checkbox" ng-repeat="customer in customers | orderBy:'text' | filter:filter_customer | filter:{service_id:service.id}: true ">
                                <label class="control-label"><input apply-uniform type="checkbox" name="customers[@{{ customer.available_qty }}][@{{ customer.service_id }}][@{{ customer.plan_id }}][]" ng-model="customer.selected" ng-value="customer.id" ng-click="optionToggled('customer')">@{{ customer.name }} (<strong><em>Plan: </em></strong>@{{ customer.plan_name }})</label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12" ng-show="!(customers | filter:{service_id:service.id}:true).length">
                    <p>No Customer Available</p>
                </div>

            </uib-tab>
        </uib-tabset>
    </div>
</div>

<div class="panel panel-flat" ng-show="delivery_steps > 2 && customers.length > 0">
    <div class="panel-body">

        <div class="row">
            {!! BootForm::text('Date and Time','date_time')->addGroupClass('col-md-3')->attribute('ng-model', "servCtrl.date_time.date")->attribute('is-open', "servCtrl.date_time.open")->attribute('ng-focus', "openCalendar()")->attribute('datetime-picker', "yyyy-MM-dd HH:mm") !!}
            {!! BootForm::text('Remark', 'remark')->addGroupClass('col-md-5')->attribute('ng-model', "remark") !!}
            {!! BootForm::text('Signature', 'signature')->addGroupClass('col-md-4')->attribute('ng-model', "signature") !!}
        </div>
        <div class="row">
            <div class="form-group">
                <label for="status">Mode <span class="text-danger">*</span></label>
                <label class="checkbox-inline" ng-repeat="alert_mode in alert_modes">
                    <input type="checkbox" name="mode[]" apply-uniform ng-value="alert_mode.value"
                           ng-model="alert_mode.selected" ng-change="optionToggled('alert_mode')">@{{ alert_mode.label }}
                </label>
                <label class="checkbox-inline" ><input type="checkbox" apply-uniform ng-model="isAllSelected.alert_mode" ng-click="toggleAll('alert_mode')" value="0">All</label>
                {{--{!! BootForm::inlineRadio('System', 'mode')->value(0)->addClass('styled') !!}
                {!! BootForm::inlineRadio('SMS', 'mode')->value(1)->addClass('styled')!!}
                {!! BootForm::inlineRadio('Mail', 'mode')->value(2)->addClass('styled')!!}
                {!! BootForm::inlineRadio('All', 'mode')->value(3)->checked()->addClass('styled')!!}--}}
            </div>
        </div>

    </div>

    <div class="panel-footer">
        <div class="heading-elements">
            <div class="heading-btn pull-left">
                {!! BootForm::button('Deliver All <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-info')->attribute('ng-disabled', 'delivery_steps != 3')->type('submit')->attribute('ng-click', 'validate_form($event)')!!}
            </div>
            <div class="heading-btn pull-right">
                {!! BootForm::button('Deliver <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-info')->attribute('ng-disabled', 'delivery_steps != 3')->attribute('ng-click', 'send_delivery(sendby_service)')->attribute('name', 'only_service')->attribute('ng-value', 'service.id')!!}
            </div>
        </div>
    </div>
</div>



{!! BootForm::hidden('exchange','')->attribute('ng-value', 'exchange') !!}
{!! BootForm::hidden('instrument','')->attribute('ng-value', 'instrument') !!}
{!! BootForm::hidden('nfo_instrument','')->attribute('ng-value', 'nfo_instrument') !!}
{!! BootForm::hidden('call_type','')->attribute('ng-value', 'call_type') !!}
{!! BootForm::hidden('price', '')->attribute('ng-value', 'final_price') !!}



<script type="text/ng-template" id="uib/template/tabs/tabset.html">
    <div>
        <ul class="nav nav-@{{tabset.type || 'tabs'}} nav-lg nav-tabs-component bg-blue noripple"
            ng-class="{'nav-stacked': vertical, 'nav-justified': justified}" ng-transclude></ul>
        <div class="tab-content">
            <div class="tab-pane" ng-repeat="tab in tabset.tabs"
                 ng-class="{active: tabset.active === tab.index}"
                 uib-tab-content-transclude="tab">
        </div>
      </div>
    </div>
</script>


@section('scripts')
    @parent

    <script type="text/javascript">

        app.filter('num', function() {
            return function(input) {
                if(isNaN(input)){
                    return null;
                }
                return parseFloat(input.toFixed(2));
            }
        });


        app.filter('previewMsg', function(){
            return function(input, service ,quota){
                if(!quota)
                {
                    quota = service.quota;
                }
                return input.replace(/\[SERVICE]/g, service.name).replace(/\[SERVICECODE]/g, service.code).replace(/\[QTY]/g, quota);
            }
        });

        app.filter('client_count', function(){
            return function(input, symbol, num){
                if(input.length > 0){
                    if(symbol == 'gt')
                        return input.filter(function(itm){ if(itm.service_deliveries.length == 0) return true; return itm.service_deliveries.used_mapped_perten >= num; });
                    else
                        return input.filter(function(itm){if(itm.service_deliveries.length == 0) return false; return itm.service_deliveries.used_mapped_perten < num && itm.service_deliveries.used_mapped_perten > 0; });
                }
                return [];
            }
        });

        app.controller('servicesController',function($scope, $http, $log, $document, client_countFilter, sweet, $window ){
            var servCtrl = this;

            $scope.getStocks = function(val, no_filter){
                var instrument = '';
                if(!no_filter){
                    no_filter = false;
                } else {
                    instrument = $scope.nfo_instrument;
                }
                //alert($scope.nfo_instrument);
                return $http.post('{{url('service_delivery/get_stocks')}}', {
                    type: val, instrument: instrument ,no_group: no_filter,
                    _token:"{{ csrf_token() }}"
                }).then(function successCallback(response) {
                    if(no_filter)
                    {
                        $scope.stock_list = response.data;
                        return [];
                    }
                    else
                    {
                        return _.map(response.data, function (item) {
                            return {
                                symbol: item.symbol,
                                price: item.price,
                                exchange: item.exchange,
                                instrument: item.instrument,
                                company: item.company,
                                status: item.status
                            };
                        });
                    }
                }, function errorCallback(response) {
                    $scope.asyncSelected_temp = null;
                    $scope.symbolObj = null;
                });
            };

            $scope.parseInt = parseInt;
            $scope.changeRange = function () {
                /*var percentage = $scope.range_per.trim();
                if(percentage==''){
                    percentage = "05";
                }
                if(percentage.length == 1){
                    percentage = "0"+percentage;
                }
                var finalPrice = $scope.final_price;
                var start = 1-parseFloat("0."+percentage);
                var end = 1+parseFloat("0."+percentage);
                $scope.range_from = (finalPrice * start).toFixed(2);
                $scope.range_to = (finalPrice * end).toFixed(2);*/

                var percentage = $scope.range_per.trim();
                var numberRegex = /^-?\d+\.?\d*$/;
                if(!percentage.match(numberRegex)){
                    return;
                }
                if(percentage==''){
                    percentage = "5";
                }
                var finalPrice = parseFloat($scope.final_price);
                var result = (parseFloat(percentage) / 100) * finalPrice;
                $scope.range_from = (finalPrice - result).toFixed(2);
                $scope.range_to = (finalPrice + result).toFixed(2);
            };

            $scope.send_delivery = function(service){
                if($scope.validate_service(service)){

                    return sweet.show({
                                title: 'Confirm',
                                text: 'Do you want to send delivery for <b>'+ service.name + "<b> ?",
                                type: 'info',
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Send',
                                closeOnConfirm: false,
                                closeOnCancel: true,
                                showLoaderOnConfirm: true,
                                html: true
                            }, function(isConfirm) {
                                if(isConfirm) {
                                    var data = $('#service_delivery').serialize() + "&only_service="+service.id;
                                    service.loading = true;
                                    return $http({
                                        method: 'POST',
                                        url: '{{url('service_delivery')}}',
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        data: data
                                    }).then(function successCallback(response) {
                                        service.selected = false;
                                        $scope.customers = $scope.customers.filter(function(item){return service.id !== item.service_id;});
                                        //show_stack_top_right('success', 'Service delivery sent successfully');
                                        service.loading = false;
                                        sweet.show('Delivered!', 'Service delivery sent successfully', 'success');

                                        setTimeout(function(){
                                            if($scope.services.filter(function(service){return service.selected;}).length == 0){
                                                $window.location.reload();
                                            }
                                        }, 500);

                                    }, function errorCallback(response) {
                                        console.log(response.data);
                                        service.loading = false;
                                        sweet.show('Cancelled', 'Error Occured', 'error');
                                    });
                                } else {
                                    return false;
                                }
                            });

                }

            };

            $scope.validate_service = function(service){
                var error = true;
                if(service != null){
                    var customer = $scope.customers.filter(function(item){return service.id === item.service_id && item.selected === true;});
                    if(customer.length == 0){
                        show_stack_top_right('error', 'Please select atleast 1 client');
                        return false;
                    }

                    if($scope.call_type == 'exit'){
                        if(client_countFilter(customer, 'gt', 5).length > 0 && ( typeof service.quota_5plus == 'undefined' || service.quota_5plus == 0)){
                            show_stack_top_right('error', 'Please select Exit amount for customers have quota 5 and above %');
                            error = false;
                            return false;
                        }
                        if(client_countFilter(customer, 'lt', 5).length > 0 && ( typeof service.quota_5less == 'undefined' || service.quota_5less == 0)){
                            show_stack_top_right('error', 'Please select Exit amount for customers have quota below 5%');
                            error = false;
                            return false;
                        }
                    }
                } else {
                    show_stack_top_right('error', 'Invalid service');
                    return false;
                }
                return error;
            };

            $scope.validate_form = function($event, validate_only){
                $event.preventDefault();
                var error = true;
                if(validate_only === null){
                    validate_only = true;
                }

                if($scope.services.filter(function(service){return service.selected && service.quota != '' && !isNaN(service.quota) && service.quota > 0;}).length == 0){
                    show_stack_top_right('error', 'No Services Selected');
                    error = false;
                    return error;
                }

                if(error){
                    angular.forEach($scope.services, function(service) {
                        if(!error) {
                            return false;
                        }
                        if(service.selected === true && $scope.customers.filter(function(item){return service.id === item.service_id;}).length > 0){
                            var customer = $scope.customers.filter(function(item){return service.id === item.service_id && item.selected === true;});
                            if(service.selected === true && customer.length == 0 ){
                                show_stack_top_right('error', 'No Client Selected in '+service.name);
                                if(error){
                                    $scope.customer_tabindex = service.id;
                                }
                                error = false;
                                return false;
                            }

                            if($scope.call_type == 'exit'){
                                if(client_countFilter(customer, 'gt', 5).length > 0 && ( typeof service.quota_5plus == 'undefined' || service.quota_5plus == 0)){
                                    show_stack_top_right('error', 'Please select Exit amount for customers have quota 5 and above %');
                                    error = false;
                                    return false;
                                }
                                if(client_countFilter(customer, 'lt', 5).length > 0 && ( typeof service.quota_5less == 'undefined' || service.quota_5less == 0)){
                                    show_stack_top_right('error', 'Please select Exit amount for customers have quota below 5%');
                                    error = false;
                                    return false;
                                }
                            }
                        }
                    });
                }

                if(error && !validate_only){
                    return sweet.show({
                        title: 'Confirm',
                        text: "Do you want to send delivery ?",
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Send'
                    }, function(isConfirm) {
                        if(isConfirm) {
                            return document.getElementById("service_delivery").submit();
                        }
                    });
                }
                return error;
            };

            $scope.getNfoStock = function(){
                if($scope.nfo_instrument == '' || $scope.asyncSelected_temp.symbol == ''){
                    return;
                }

                var data = { symbol: $scope.asyncSelected_temp.symbol, nfo_instrument: $scope.nfo_instrument, expiry_date: $scope.expiry.id };
                if($scope.instrument == 'option'){
                    data = {symbol: $scope.asyncSelected_temp.symbol, nfo_instrument: $scope.nfo_instrument, expiry_date: $scope.expiry.id, strike: $scope.strike.id, option_type: $scope.option_type };
                }

                $http.post('{{url('service_delivery/get_nfo_stocks')}}', data)
                    .then(function successCallback(response) {
                        $scope.symbolObj = response.data;

                        servCtrl.qty_lot_size = response.data.lot_size;

                        // for open service
                        $scope.getServices('exit', true);
                        $scope.setCallType('new');

                    }, function errorCallback(response) {
                        $scope.asyncSelected_temp = null;
                        $scope.symbolObj = null;
                    });
            };



            $scope.sendby_service_func = function(service) {
                $scope.sendby_service = service;
            };

            $scope.setCallType = function(call_val) {
                $scope.call_type = call_val;
                if(call_val != null && $scope.symbolObj != undefined && $scope.symbolObj != null ){
                    $scope.getServices(call_val);
                    //$scope.getTemplate();
                }
            };

            angular.element(function () {
                var req_symbol = '{{ app('request')->input('symbol') }}';
                if(!req_symbol){
                    return false;
                }
                $scope.call_type = '{{ app('request')->input('call_type') }}';
                var service_name = '{{ app('request')->input('service_name') }}';
                $scope.exchange = '{{ app('request')->input('exchange') }}';
                $scope.asyncSelected_temp = {
                        "symbol":"{{ app('request')->input('symbol') }}", 'price': "{{ app('request')->input('price') }}",
                        'exchange' : $scope.exchange, 'instrument' : "{{ app('request')->input('instrument') }}", 'company' : "{{ app('request')->input('company_name') }}",
                        'status' : "{{ app('request')->input('status') }}"
                };

                /*if($scope.exchange == 'nfo'){
                    var interval2 = setInterval(function(){
                        var $len = angular.element(".expiry-date-con option").size();
                        if($len > 1){
                            clearInterval(interval2);
                            angular.element(".expiry-date-con").val(angular.element(".expiry-date-con option:eq(1)").val());
                        }
                    }, 1000);
                }*/

                var interval1 = setInterval(function(){
                    angular.element("html, body").scrollTop(angular.element(document).height());
                    var $len = angular.element(".tab-pane label:contains('"+service_name+"')").length;
                    if($len != 0){
                        clearInterval(interval1);
                        var scope = angular.element(".tab-pane label:contains('"+service_name+"')").scope();
                        scope.service['selected'] = true;
                        $scope.$apply();
                        $scope.getCustomer(scope.service);
                    }
                }, 1000);
            });

            $scope.getServices = function(call_type, open){
                if(!$scope.symbolObj){
                    return false;
                }

                $scope.services = null;
                $scope.customers = [];
                var expiry = $scope.expiry.id;
                var nfo_instrument = $scope.nfo_instrument;
                var strike = $scope.strike.id;
                var option_type = ($scope.option_type == null) ? '' : $scope.option_type;

                var requestUrl = '{{url('service_delivery/get_services')}}';
                if(open){
                    requestUrl = '{{url('service_delivery/get_open_services')}}';
                }
                $http.post(requestUrl,{
                    call_type: call_type, symbol: $scope.symbolObj.symbol, exchange: $scope.exchange,nfo_instrument: nfo_instrument, expiry_date: expiry, strike: strike, option_type: option_type
                }).then(function successCallback(response) {
                    if(!open) {
                        $scope.services = response.data;
                    }
                    else {
                        $scope.open_services = response.data;
                    }
                }, function errorCallback(response) {
                    console.log(response.data);
                    $scope.setCallType(null);
                });
            };

            $scope.openCalendar = function(e, picker) {
                servCtrl.date_time.open = true;
            };

            $scope.toggleAll = function(type, service_id) {
                if(type == 'service'){
                    var toggleStatus = !$scope.isAllSelected.service;
                    angular.forEach($scope.services, function(itm){
                        itm.selected = toggleStatus;
                        if(!toggleStatus){
                            itm.quota = 0;
                        }
                    });
                }
                else if(type == 'customer'){
                    var toggleStatus = !$scope.isAllSelected.customer;
                    angular.forEach($scope.customers, function(itm){
                        if(service_id == itm.service_id)
                            itm.selected = toggleStatus;
                    });
                }
                else if(type == 'alert_mode'){
                    var toggleStatus = $scope.isAllSelected.alert_mode;
                    angular.forEach($scope.alert_modes, function(itm){
                        if(service_id == itm.service_id)
                            itm.selected = toggleStatus;
                    });
                }

            };

            $scope.optionToggled = function(type){
                if(type == 'service'){
                    $scope.isAllSelected.service = $scope.services.every(function(itm){ return itm.selected; });
                } /*else if(type == 'customer'){
                    $scope.isAllSelected.customer = $scope.customers.every(function(itm){ return itm.selected; });
                }*/
                else if(type == 'alert_mode'){
                    $scope.isAllSelected.alert_mode = $scope.alert_modes.every(function(itm){ return itm.selected; });
                }
            };
            $scope.exit_call_func = function(service){
                if($scope.call_type == 'exit'){
                    $scope.optionToggled('service');
                    if(service.selected){
                        $scope.getCustomer(service);
                    }
                }
            };

            $scope.updateStep = function(step) {
                if(step == 1){
                    $scope.services = null;
                    $scope.open_service = [];
                    $scope.customers = [];
                    $scope.stop_loss = undefined;
                    $scope.target_1 = undefined;
                    $scope.target_2 = undefined;

                } else if(step == 2){
                    $scope.customers = [];
                }
                $scope.delivery_steps = step;
            };

            $scope.update_value = function(service) {
                service.value = service.quota * servCtrl.qty_lot_size * $scope.final_price;
            }

            $scope.getCustomer = function(service, open){
                if(!service.quota || isNaN(service.quota)){
                    if($scope.call_type != 'exit'){
                        return false;
                    } else{
                        if($scope.instrument == 'stock') {
                            service.quota = 100;
                        } else {
                            service.quota = 1;
                        }

                    }
                }

                if(service.quota == 0){
                    return false;
                }

                if($scope.services.filter(function(item){return true === item.selected;}).length == 0)
                {
                    $scope.customers = [];
                }
                service.loading = true;

                $scope.getTemplate(service.id);

                var server_data  = {
                    service_id: service.id ,
                    symbol: $scope.symbolObj.symbol,
                    exchange: $scope.exchange,
                    nfo_instrument: $scope.nfo_instrument,
                    expiry_date: $scope.expiry.id,
                    strike: $scope.strike.id,
                    option_type: $scope.option_type,
                    include_existing: $scope.include_existing,
                    qty: service.quota,
                    call_type: $scope.call_type,
                    service: service,
                    price: $scope.final_price,
                    lot_size: servCtrl.qty_lot_size
                };

                /*let urlParameters = Object.keys(server_data).map((i) => i+'='+server_data[i]).join('&')

                console.log(urlParameters);*/

                $http.post('{{url('service_delivery/get_client')}}',server_data).then(function successCallback(response) {
                    $scope.customers = $scope.customers.filter(function(item){return service.id !== item.service_id;});

                    service.include_existing = $scope.include_existing;

                    if(response.data.error) {
                        show_stack_top_right('warning', response.data.error);
                        service.loading = false;
                        return false;
                    }

                    $scope.customer_group_by_qty[service.id] = [];
                    var i = 0;

                    angular.forEach(response.data.customers, function(item, key){

                        if($scope.customer_group_by_qty[item.service_id].length == 0){
                            $scope.customer_group_by_qty[item.service_id] = angular.copy($scope.customer_group_by_qty_default);
                        }

                        angular.forEach($scope.customer_group_by_qty[item.service_id] , function(value) {
                            // Increament each number by one when you hit it
                            if (item['remaining_qty'] == value.org_section) {
                                value.section = item['available_qty'];
                                //console.log(value.section);
                                value.cust_count++;
                            }
                        });
                    });


                    $scope.customers = $scope.customers.concat(response.data.customers);

                    window.setTimeout(function(){

                        $scope.customer_tabindex = service.id;
                        service.loading = false;
                    }, 10);

                }, function errorCallback(response) {
                    service.loading = false;
                    if(response.data.error) {
                        show_stack_top_right('info', 'Error Occur. Please try again');
                        return false;
                    }
                });
            };

            $scope.getTemplate = function(service_id){
                $http.post('{{url('service_delivery/get_templates')}}',{
                    call_type: $scope.call_type, exchange: $scope.exchange, service_id: service_id
                }).then(function successCallback(response) {
                    $scope.templates[service_id] = response.data;

                    $scope.template = response.data;
                    $scope.client_msg[service_id] = response.data.body;
                }, function errorCallback(response) {
                    $scope.templates[service_id] = '';
                    $scope.client_msg[service_id] = '';
                });
            };


            /// varibles
            $scope.asyncSelected_temp = '';
            $scope.instrument = '';
            $scope.exchange = '';
            $scope.nfo_instrument = '';
            $scope.expiry = {id:''};
            $scope.default_strike = {id:0};
            $scope.range_from = 0;
            $scope.range_to = 0;
            $scope.delivery_steps = 1;
            $scope.isAllSelected = {
                service: false,
                customer: false,
                alert_mode: true
            };
            $scope.customers = [];
            $scope.sendby_service = null;
            $scope.tabindex = 3;
            $scope.customer_tabindex = 0;
            var c_type="{{ app('request')->input('call_type') }}";
            if(c_type == 're_entry'){
                $scope.tabindex = 1;
            }else if(c_type == 'exit'){
                $scope.tabindex = 2;
                $scope.customer_tabindex = parseInt("{{ app('request')->input('service_id') }}");
            }
            $scope.templates = [];
            $scope.client_msg = [];
            $scope.range_per = 5;


            $scope.customer_group_by_qty_default = [
                {section: "100", org_section: "100", label: "100%", selected: false, cust_count:0},
                {section: "75", org_section: "75", label: "75%", selected: false, cust_count:0},
                {section: "50", org_section: "50", label: "50%", selected: false, cust_count:0},
                {section: "25", org_section: "25", label: "25%", selected: false, cust_count:0},
                {section: "24.99", org_section: "24.99", label: "<25", selected: false, cust_count:0},
            ];
            $scope.customer_group_by_qty = [];
            $scope.signature = '- Intellistocks';
            servCtrl.date_time = {
                date: new Date(),
                open: false
            };
            //servCtrl.qty_lot_size = 2500;
            servCtrl.qty_lot_size = null;
            $scope.include_existing = false;
            $scope.open_services = [];
            $scope.final_price = 0;

            $scope.alert_modes = [
                {value:'0',label: 'System', selected:true},
                {value:'1',label: 'SMS', selected:true},
                {value:'2',label: 'Email', selected:true}
            ];


            // watch
            $scope.$watch('asyncSelected_temp', function(newval) {
                if(newval !== null && typeof newval === 'object')
                {
                    switch (newval.instrument) {
                        case 'FUTIDX':
                        case 'FUTIVX':
                        case 'FUTSTK':
                            $scope.instrument = 'future';
                            break;
                        case 'OPTIDX':
                        case 'OPTSTK':
                            $scope.instrument = 'option';
                            break;
                        default:
                            $scope.instrument = 'stock';
                    }

                    $scope.symbolObj = null;

                    $scope.exchange = newval.exchange;
                    $scope.nfo_instrument = newval.instrument;

                    if(newval.exchange == 'nfo'){
                        $scope.getStocks( newval.symbol, true);
                    }
                    if(newval.exchange != 'nfo'){
                        $scope.symbolObj = $scope.asyncSelected_temp;

                        $scope.getServices('exit', true);
                        if($scope.call_type == 'new') {
                            $scope.setCallType('new');
                        }
                    }

                }
                else {
                    $scope.instrument = '';
                    $scope.exchange = '';
                    $scope.nfo_instrument = '';
                    $scope.symbolObj = null;
                    $scope.open_services = [];
                    $scope.option_type = null;
                    $scope.strikes = null;
                    $scope.strike = $scope.default_strike;
                    $scope.expiry = {id:''};
                    $scope.expiries = null;
                    $scope.updateStep(1);
                }
            });

            $scope.$watch('stock_list', function(){
                if($scope.stock_list) {
                    $scope.expiries =  _.map(_.uniqBy($scope.stock_list, 'expiry_date'), function (item) {
                        return {
                            id: item.expiry_date,
                            name: item.expiry_date
                        };
                    });
                }
            });
            $scope.$watch('expiry', function(newval) {
                if(newval == null) {
                    $scope.option_type = null;
                    $scope.strikes = null;
                    $scope.strike = $scope.default_strike;
                }
                if($scope.instrument == 'future'){
                    $scope.getNfoStock();
                }
            });

            $scope.$watch('option_type', function(newval) {
                $scope.strikes = null;
                $scope.strike = $scope.default_strike;
                if(newval != null){
                    var strike = _.filter($scope.stock_list,function (item) {
                        return (item.option_type.toUpperCase() === $scope.option_type.toUpperCase()
                        & item.expiry_date.toUpperCase() === $scope.expiry.id.toUpperCase());
                    });

                    $scope.strikes =  _.map(strike, function (item) {
                        return {
                            id: item.strike,
                            name: item.strike
                        };
                    });
                }
            });

            $scope.$watch('strike', function(newval) {
                if(newval !== null && typeof newval === 'object')
                {
                    $scope.getNfoStock();
                }
            });

            $scope.$watch('symbolObj', function(newval) {
                if(newval !== null && typeof newval === 'object')
                {
                    $scope.price_range = false;
                    $scope.range_from = (newval.price * 0.95).toFixed(2);
                    $scope.range_to = (newval.price * 1.05).toFixed(2);
                    $scope.updateStep(2);
                }
                else {
                    $scope.updateStep(1);
                }
            });

            $scope.$watch('customers', function (newval) {
                if( $scope.services && $scope.services.length && newval !== null && typeof newval === 'object')
                {
                    $scope.updateStep(3);
                }
            });

            $scope.$watch('include_existing', function (newval) {
                $scope.getServices("new");
            });

            $scope.$watch('final_price', function (newval) {
                if($scope.instrument != 'stock'){
                    $scope.getServices($scope.call_type);
                }
            });

            $scope.$watchGroup(['customers','symbolObj', 'final_price','price_range', 'range_from', 'range_to', 'stop_loss', 'target_1', 'target_2', 'signature', 'remark', 'template'], function(newval) {
                if($scope.price_range) {
                    $scope.final_price =((parseFloat($scope.range_from) + parseFloat($scope.range_to)) / 2).toFixed(2);
                } else if($scope.symbolObj !== undefined && $scope.symbolObj!== [] && $scope.symbolObj !== null) {
                    if($scope.final_price == $scope.symbolObj.price || $scope.final_price == 0){
                        $scope.final_price = parseFloat($scope.symbolObj.price);
                    }
                } else {
                    $scope.final_price = 0;
                }

                if($scope.services && $scope.services.length){

                    angular.forEach($scope.services, function(itm){
                        if(!isNaN(itm.quota) && itm.quota){
                            itm.value = itm.quota * servCtrl.qty_lot_size * $scope.final_price;

                            var base_amt = 1000000 * itm.quota / 100;
                            var deliver_qty = Math.round( base_amt / $scope.final_price);


                            itm.quota_stock = deliver_qty;

                            if($scope.stop_loss){
                                $scope.stop_loss = parseFloat($scope.stop_loss);
                                itm.risk = parseFloat(deliver_qty * ($scope.final_price - $scope.stop_loss)).toFixed(2);
                            } else {
                                itm.risk = '-NA-';
                            }
                            if($scope.target_1){
                                $scope.target_1 = parseFloat($scope.target_1);
                                if($scope.target_2){
                                    $scope.target_2 = parseFloat($scope.target_2);
                                    itm.reward =  parseFloat(deliver_qty * ((($scope.target_1+$scope.target_2)/2) - $scope.final_price)).toFixed(2);
                                    if($scope.stop_loss != ''){
                                        itm.rvr = parseFloat(((($scope.target_1+$scope.target_2)/2) - $scope.final_price)/($scope.final_price - $scope.stop_loss)).toFixed(2);
                                    } else {
                                        itm.rvr = '-NA-';
                                    }

                                } else {
                                    itm.reward =  parseFloat(deliver_qty * ($scope.target_1 - $scope.final_price)).toFixed(2);
                                    if($scope.stop_loss != ''){
                                        itm.rvr = parseFloat(($scope.target_1 - $scope.final_price)/($scope.final_price - $scope.stop_loss)).toFixed(2);
                                    } else {
                                        itm.rvr = '-NA-';
                                    }
                                }
                            } else {
                                itm.reward = '-NA-';
                                itm.rvr = '-NA-';
                            }
                        }

                        if( $scope.customers && $scope.customers.length && itm.selected){
                            $scope.replaceTokenInPreview(itm.id);
                        }
                    });
                }

            });



            $scope.preview = function () {
                if($scope.service_delivery.validate()) {
                    $scope.replaceTokenInPreview(servCtrl);
                    $scope.open();
                }
            };

            $scope.allocationToggled = function(allocation, service_id, exit, type){
                var toggleStatus = allocation.selected;

                if(exit){
                    if(type == '5plus') {
                        angular.forEach($scope.customers, function(itm){
                            if(service_id == itm.service_id
                                && itm.service_deliveries.used_mapped_perten >=5)
                            {
                                itm.selected = toggleStatus;
                            }
                        });

                    } else {
                        angular.forEach($scope.customers, function(itm){
                            if( service_id == itm.service_id
                                && itm.service_deliveries.used_mapped_perten < 5)
                            {
                                itm.selected = toggleStatus;
                            }
                        });
                    }
                } else {
                    angular.forEach($scope.customers, function(itm){
                        if(allocation.org_section == itm.remaining_qty
                            && service_id == itm.service_id) { itm.selected = toggleStatus; }
                    });
                }
            };


            $scope.replaceTokenInPreview = function(service_id){
                var range_from=($scope.range_from != undefined) ?$scope.range_from: '';
                var range_to=($scope.range_to != undefined) ?$scope.range_to: '';
                var stop_loss=($scope.stop_loss != undefined) ? "SL " + $scope.stop_loss + ', ': '';
                var target =($scope.target_1 != undefined) ? "Tgt " + $scope.target_1: '';
                    target += ($scope.target_2 != undefined) ? " - "+$scope.target_2: '';
                var symbol =($scope.symbolObj != undefined) ? $scope.symbolObj.symbol: '[SYMBOL]';
                var price=($scope.symbolObj != undefined) ? $scope.final_price: '[PRICE]';
                var fixed=(typeof $scope.price_range != undefined && $scope.price_range) ? false: true;
                var signature=($scope.signature != undefined) ?$scope.signature: '';
                var remark=($scope.remark != undefined) ?$scope.remark: '';

                var expiry_date=($scope.symbolObj != undefined) ? moment($scope.expiry).format('MMM, YY'): '[EXPIRYMONTH]';


                var content = '';
                if($scope.templates[service_id] !== undefined){
                    content = $scope.templates[service_id].body;
                }

                if(fixed) {
                    var range = range_text = price;
                    var range_text = '@'+range;
                }
                else {
                    var range = ((parseFloat(range_from) + parseFloat(range_to))/2);
                    //var range_text = 'Range '+ range;
                    var range_text = 'Range '+ range_from + " - " + range_to;
                }

                content= content.replace(/\[SYMBOL]/g, symbol).replace(/\[EXPIRYMONTH]/g, expiry_date).replace(/\[TARGET]/g, target).replace(/\[RANGE]/g, range_text).replace(/\[SIGNATURE]/g, signature).replace(/\[REMARK]/g, remark).replace(/\[STOPLOSS]/g, stop_loss).replace(/\[PRICE]/g, range_text);
                $scope.client_msg[service_id] = content;
            };

        });


        app.config(['$validationProvider', function ($validationProvider) {
            $validationProvider
                .setExpression({
                    validate_price_opt: function(value, $scope, element, attrs) {
                        if($scope.symbolObj !== null && typeof $scope.symbolObj === 'object'){
                            switch (attrs.prop){
                                case 'stop_loss':
                                    return parseFloat($scope.stop_loss) < parseFloat($scope.final_price);
                                    break;
                                case 'target_1':
                                    if($scope.target_1 == ''){
                                        return true;
                                    }
                                    return parseFloat($scope.target_1) > parseFloat($scope.final_price);
                                    break;
                                case 'target_2':
                                    if($scope.target_2 == ''){
                                        return true;
                                    }
                                    return parseFloat($scope.target_2) > parseFloat($scope.final_price)
                                        && parseFloat($scope.target_2) > parseFloat($scope.target_1);
                                    break;
                            }
                        }

                    },
                    validate_range: function(value, $scope, element, attrs) {
                        if($scope.symbolObj !== null && typeof $scope.symbolObj === 'object' && $scope.price_range){
                            return parseFloat($scope.range_from) < parseFloat($scope.range_to);
                        } else if(!$scope.price_range){
                            return true;
                        }
                    },
                    valid_quota: function(value, $scope, element, attrs) {
                       return !isNaN(value) || value === '';
                    }
                })
                .setDefaultMsg({
                    validate_price_opt: {
                        error: 'Invalid',
                        success: ''
                    },
                    validate_range: {
                        error: 'Range from must be less then Range To',
                        success: ''
                    },
                    valid_quota: {
                        error: 'Please provide valid number',
                        success: ''
                    }
                });
        }]);

    </script>

@endsection