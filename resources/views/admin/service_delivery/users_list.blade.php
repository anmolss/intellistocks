@extends('admin.layout.app')

@section('page_title', 'Users')
@section('page_subtitle', 'List of Users')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
           
        </div>
        <div class="panel-body">
            <table class="table table-striped table-hover" id="sample-table-2">
                <thead>
                    <tr>
                        <th>Service Name</th>
                        <th>Plan</th>
                        <th>User Name</th>
                        <th>Call Type</th>
                        <th>Symbol</th>
                        <th>Quantity</th>
                        <th>Message</th>
                    </tr>
                </thead>
                 <tbody>
                @if(count($list) > 0)
                    @foreach($list as  $key => $item)
                       
                         <tr>
                            <td>{{ $item->service->name }}</td>
                            <td>{{ $item->plan->name }}</td>
                            <td>{{ $item->user->name }}</td>
                            <td>{{ $item->call_type }}</td>
                            <td>{{ $item->symbol }}</td>
                            <td>{{ $item->qty }}</td>
                            <td>{{ $item->msg }}</td>
                            
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" align="center">No User list Found</td>
                    </tr>
                @endif
                </tbody>
            </table>

            {{ $list->links() }}
        </div>
    </div>

@endsection
