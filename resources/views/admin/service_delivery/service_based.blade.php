@extends('admin.layout.app')

@section('page_title', 'Profit and Loss')
@section('page_subtitle', 'Service Based')

@section('content')
    <form weclass="form-horizontal" action="#" id="customer-frm-filter" method="GET">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title text-semibold"><i class="icon-equalizer position-left"></i> Filters</h6>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        {!! Form::label('creation_range', 'Creation Date:', ['class' => 'control-label col-lg-4']) !!}
                                        <div class="col-lg-8">
                                            {!! Form::text('delivery_range', $default_delivery_range, ['class' => 'form-control daterange-delivery_range']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        {!! Form::label('service', 'Service:', ['class' => 'control-label col-lg-3']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::select('service[]', $services, $default_service, ['class' => 'form-control', 'id' => 'services', 'multiple'  ] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 ">
                                    <button type="submit" class="btn btn-primary col-sm-3 col-sm-offset-2">Filter <i class="icon-arrow-right14 position-right"></i></button>
                                    <a title="Reset Filter!" id="reset_search" href="javascript:" class="btn btn-success btn-rounded col-sm-3 col-sm-offset-2">Reset <i class="icon-spinner11 position-right"></i></a>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="panel panel-flat table-responsive">
        <table class="table table-hover" id="customers-table">
            <thead>
            <tr>
                <th>Service Name</th>
                <th># of Customers with Available limit</th>
                <th>Total calls</th>
                <th>Open Calls</th>
                <th>Close Calls</th>
                <th>Positive Calls</th>
                <th>Wining % </th>
                <th>Open P&L</th>
                <th>Realized P&L</th>
                <th>Total P&L</th>
                {{--<th>Nifty Returns P&L</th>
                <th>Return Multiple of Nifty</th>--}}
                <th>ROI Annualized</th>
                <th>Customers with remaining calls</th>
                {{--<th>Expiring/ed Calls</th>--}}
                <th>Today's P&L</th>
                <th>Gross Contribution of winning calls</th>
                <th>Gross Contribution of Losing calls</th>
                <th>Cumulative Returns from all calls</th>
                <th>Reward Vs Risk </th>
                <th>Average months Position held</th>
                <th>Weighted Average cash deployed</th>
            </tr>
            </thead>
            <tbody>
                @foreach($pnl_services as $service)
                    <tr>
                        <td>{{ $service['service_name'] }}</td>
                        <td>{{ $service['avail_user_total'] }} / {{ $service['user_total'] }}</td>
                        <td>{{ $service['total_call'] }}</td>
                        <td>{{ $service['open_call'] }}</td>
                        <td>{{ $service['close_call'] }}</td>
                        <td>{{ $service['positive_call'] }}</td>
                        <td>{{ $service['winning_percent'] }}% </td>
                        <td>{{ $service['open_profit'] }}</td>
                        <td>{{ $service['booked_profit'] }}</td>
                        <td>{{ $service['total_profit'] }}</td>
                        {{--<td>Nifty Returns P&L</td>
                        <td>Return Multiple of Nifty</td>--}}
                        <td>{{ $service['net_roi'] }}%</td>
                        <td>{{ $service['avail_user_total'] }}</td>
                        {{--<td>Expiring/ed Calls</td>--}}
                        <td>{{ $service['today_pnl'] }}</td>
                        <td>{{ $service['gross_profit'] }}%</td>
                        <td>{{ $service['gross_loss'] }}%</td>
                        <td>{{ $service['cumulative_return_all_call'] }}%</td>
                        <td>{{ $service['r_v_r'] }}</td>
                        <td>{{ $service['months_pos'] }}</td>
                        <td>NA</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection


@push('scripts')
<script type="text/javascript">
    $('.daterange-delivery_range').daterangepicker({
        applyClass: 'bg-info',
        cancelClass: 'btn-default',
        locale: {
            format: 'DD/MM/YYYY'
        }
    });
    // Enable Select2 select for the length option
    $('select').select2({
        minimumResultsForSearch: Infinity
    });
</script>

@endpush