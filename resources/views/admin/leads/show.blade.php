@extends('admin.layout.app')

@section('page_title', 'Leads')
@section('page_subtitle', 'List of Leads')

@section('content')
  <style type="text/css">
.note-box{
    background: #dedede !important;
    padding: 15px;
}
  </style>
<?php // echo '<pre>',print_r($service_plans),'</pre>'; ?>

  <div class="row">
    <div class="col-md-9">
      <div class="panel bg-info-800">
        <div class="panel-body">
          <h3 class="no-margin-top">
          Title : {{$leads->title}}
            @if($leads->has_discount == '1')
            <span class="pull-right">
              Discount : 
              @if($leads->discount_type == 'percent')
              <span class="label label-icon"><h5><mark>{{$leads->discount_value}} %</mark></h5></span>
              @elseif($leads->discount_type == 'amount')
              <span class="label label-icon"><h5><mark>{{$leads->discount_value}} Rs.</mark></h5></span>
              @endif
            </span>
            @endif
          </h3>
          @if($leads->days_until_contact < 2)
          Follow up: 
          <span class="label border-left-danger label-striped">
            @if($leads->status == 1)<span class="badge badge-flat border-danger text-danger-600">{!! $leads->days_until_contact !!}</span>@endif
            {{date('d, F Y, H:i', strTotime($leads->contact_date))}}
          </span>
          <button type="button" class="btn btn-danger btn-rounded" data-toggle="modal" data-target="#ModalFollowUp"><i class="glyphicon glyphicon-calendar" ></i> Update Date</button>
            @else
            <div class="col-sm-12">
            Next Follow up: 
            <span class="label border-left-success label-striped">
            <span class="badge badge-flat border-success text-success-600">{!! $leads->days_until_contact !!} days</span>
            {{date('d, F Y, H:i', strTotime($leads->contact_date))}}
            </span>
            @if($leads->status == 1)
              <button type="button" class="btn btn-success btn-rounded" data-toggle="modal" data-target="#ModalFollowUp"><i class="glyphicon glyphicon-calendar" ></i> Update Date</button>
            @endif
            </div> <!--Remove days left if lead is completed-->
            @endif
            <div class="clearfix"></div>
          <hr class="grey">
          <p>Description : {{nl2br($leads->note)}}</p><br />
        </div>
        <div class="panel-footer panel-footer-condensed">
          <div class="heading-elements">
            <span class="heading-text">
                Created at: {{ date('d F, Y, H:i:s', strtotime($leads->created_at))}} 
                @if($leads->updated_at != $leads->created_at) 
                  <br/>Modified at: {{date('d F, Y, H:i:s', strtotime($leads->updated_at))}}
                @endif
            </span>
              @if($leads->status != 2)
              {!! Form::model($leads, [
                'method' => 'PATCH',
                'route' => ['admin::leads.updatestatus', $leads->id],
                ]) !!}
              <div class="form-inline pull-right">
                <div class="form-group text-semibold"> 
                <?php 
                  $a=Config::get('rothmans.lead_status');  
                  unset($a[2]); 
                ?>

                  {!! Form::label('status', 'status:', ['class' => 'control-label']) !!}
                  {!! Form::select('status', $a, [$leads->status], ['class' => 'form-control']) !!}
                  @if ($errors->has('status'))
                      <span class="help-block">
                          <strong>{{ $errors->first('status') }}</strong>
                      </span>
                  @endif
                </div> 
                {!! Form::submit('Update Status', ['class' => 'btn btn bg-primary-400 btn-rounded btn-icon btn-xs legitRipple']) !!}
              </div>
              {!! Form::close() !!}
              @endif
          </div>
        </div>  
      </div>
    </div>
    <div class="col-sm-3">
      <div class="panel panel-body border-top-primary text-center">
        <div class="content-group-sm" id="v-bottom-basic" style="height: 200px">
          @if($leads->confidence !="")
          <div class="progress vertical bottom">
            <div class="progress-bar progress-bar-primary" data-transitiongoal-backup="{{$leads->confidence}}" data-transitiongoal="{{$leads->confidence}}">
              <span class="sr-only">Confidence :{{$leads->confidence}}%</span>
            </div>
          </div>
          @endif
          <div class="progress vertical bottom">
            <div class="progress-bar progress-bar-danger" data-transitiongoal-backup="{{Config::get('rothmans.lead_priority')[$leads->priority]}}" data-transitiongoal="100">
              <span class="sr-only">{{Config::get('rothmans.lead_priority')[$leads->priority]}}</span>
            </div>
          </div>
        </div>
        <div class="col-sm-12 content-group-sm">
        @if($leads->confidence !="")
          <span class="label label-primary vertical">Confidence</span>
        @endif
          <span class="label label-danger vertical">Priority</span>
        </div>
      </div>
    </div>
  </div>
    <div class="row">
        <div class="col-md-6">   
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h6>Client Informations</h6>
                </div>

                <div class="panel-body">
                    <h1 class="moveup">{{$leads->clientAssignee->name}} ({{$leads->clientAssignee->company_name}})</h1>
                    <div class="col-md-6">
                        @if($leads->clientAssignee->email != "") 
          
                          <p> <span class="glyphicon glyphicon-envelope" aria-hidden="true" data-toggle="tooltip" title="Email" data-placement="left" > </span> 
                                       <a href="mailto:{{$leads->clientAssignee->email}}" data-toggle="tooltip" data-placement="left" >{{$leads->clientAssignee->email}}</a></p>
                        @endif
                        @if($leads->clientAssignee->primary_number != "") 
                        <!--Work Phone-->            
                        <p> <span class="glyphicon glyphicon-headphones" aria-hidden="true" data-toggle="tooltip" title="Primary number" data-placement="left"> </span> 
                                       <a href="tel:{{$leads->clientAssignee->work_number}}">{{$leads->clientAssignee->primary_number}}</a></p>
                        @endif
                        @if($leads->clientAssignee->secondary_number != "")           
                        <!--Secondary Phone-->            
                        <p> <span class="glyphicon glyphicon-phone" aria-hidden="true" data-toggle="tooltip" title="Secondary number" data-placement="left"> </span> 
                                       <a href="tel:{{$leads->clientAssignee->secondary_number}}">{{$leads->clientAssignee->secondary_number}}</a></p>
                        @endif
                        @if($leads->clientAssignee->address || $leads->clientAssignee->zipcode || $leads->clientAssignee->city != "")            
                        <!--Address-->            
                        <p> <span class="glyphicon glyphicon-home" aria-hidden="true" data-toggle="tooltip" title="Address/Zip code/city" data-placement="left"> </span>  {{$leads->clientAssignee->address}} <br />{{$leads->clientAssignee->zipcode}} {{$leads->clientAssignee->city}}
                                        </p>
                        @endif
                    </div>    

                    <div class="col-md-6">
                        @if($leads->clientAssignee->company_name != "")
                             <!--Company-->            
                            <p> <span class="glyphicon glyphicon-star" aria-hidden="true" data-toggle="tooltip" title="Company name" data-placement="left"> </span> {{$leads->clientAssignee->company_name}}</p>
                        @endif
                       
                        @if($leads->clientAssignee->industry != "")
                          <!--Industry-->            
                            <p> <span class="glyphicon glyphicon-briefcase" aria-hidden="true" data-toggle="tooltip" title="Industry" data-placement="left"> </span> {{$leads->clientAssignee->industry}}</p>
                        @endif
                        @if($leads->clientAssignee->company_type!= "")               
                            <!--Company Type-->            
                            <p> <span class="glyphicon glyphicon-globe" aria-hidden="true" data-toggle="tooltip" title="Company type" data-placement="left"> </span> 
                                     {{$leads->clientAssignee->company_type}}</p>
                        @endif

                        @if($leads->clientAssignee->fk_service_id != "" && $leads->clientAssignee->fk_service_id != "0")
                             <!--Company-->            
                            <p> <span class="icon-hammer-wrench" aria-hidden="true" data-toggle="tooltip" title="Company name" data-placement="left"> </span> {{$services[$leads->clientAssignee->fk_service_id]}}</p>
                        @endif

                    </div>
                    <div class="clearfix"></div>
                 </div> 
          </div>
      </div>

      <div  class="col-md-3">   
           <div class="panel panel-warning">
                <div class="panel-heading">
                    <h6>Assigned Employee</h6>
                </div>
              <div class="panel-body">
                  <h1 class="moveup">{{$leads->assignee->name}}</h1>
                  @if($leads->assignee->email!= "") 
                  <p>
                      <span class="glyphicon glyphicon-envelope" aria-hidden="true"> </span> 
                      <a href="mailto:{{$leads->assignee->email}}">{{$leads->assignee->email}}</a>
                  </p>
                  @endif
                  @if($leads->assignee->work_number!= "")
                  <p>
                      <span class="glyphicon glyphicon-headphones" aria-hidden="true"> </span> 
                      <a href="tel:{{$leads->assignee->work_number}}">{{$leads->assignee->work_number}}</a>
                  </p>
                  @endif
                  @if($leads->assignee->personal_number!= "")
                  <p>
                      <span class="glyphicon glyphicon-phone" aria-hidden="true"> </span> 
                  <a href="tel:{{$leads->assignee->personal_number}}">{{$leads->assignee->personal_number}}</a>
                  </p>
                  @endif       
                  @if($leads->assignee->address!= "")
                  <p> 
                      <span class="glyphicon glyphicon-home" aria-hidden="true"> </span>  {{$leads->assignee->address}}
                  </p>
                  @endif 
              </div>
          </div>
      </div>
      <div class="col-md-3">
        <div class="panel panel-body border-top-info">
          <h6 class="no-margin text-semibold">Lead information</h6>
            <div class="well">
              <p>
                  Assigned to: <a href="{{route('admin::leads.show', $leads->assignee->id)}}">{{$leads->assignee->name}}</a>
              </p>
              <div>
                Created Date : {{date('d, F Y, H:i', strTotime($leads->created_at))}} 
              </div>
              <div>
                Open Date : {{date('d, F Y, H:i', strTotime($leads->open_date))}} 
              </div>
              <div>
                Expected Close Date : {{date('d, F Y, H:i', strTotime($leads->close_date))}} 
              </div>
                <div>
                Purpose : <?php  echo Config::get('rothmans.lead_purpose')[$leads->lead_type];?>
                </div>
                <div>
                Source: <?php  echo Config::get('rothmans.lead_source')[$leads->source];?>
                </div>
                <div>
                Capital: <?php  echo Config::get('rothmans.capital')[$leads->capital];?>
                </div>
                <div>
                Discount : <?php  echo Config::get('rothmans.discount_offered')[$leads->has_discount];?>
                </div>
            </div>
        </div>
      </div>
        </div>
      <div class="row">
      <div class="col-sm-12">
        <div class="timeline timeline-left">
              <div class="timeline-row" >
                  <div class="timeline-icon"> 
                      <div class="bg-warning-400">
                          <i class="icon-cash3"></i>
                      </div>
                  </div> 
                  <div class="panel panel-flat timeline-content">
                      <h6 class="panel-heading">Lead Follow Ups</h6>   
                      <div class="panel-body">
                          <ul class="media-list chat-list content-group">
                              @forelse($leads->followups as $followup)
                              <li class="media" >
                                  <div class="media-body">
                                  <p class="text-warning">{{$followup->note}}</p>
                                  <p class="smalltext">Old Date: 
                                    {{date('d, F Y H:i', strTotime($followup->old_contact_date))}}
                                  </p>
                                  <p class="smalltext">New Date: 
                                    {{date('d, F Y H:i', strTotime($followup->new_contact_date))}}
                                  </p>
                                  <p class="smalltext">Created at: 
                                    {{date('d, F Y H:i', strTotime($followup->created_at))}} 
                                    @if($followup->updated_at != $followup->created_at) 
                                    <br/>Modified at: {{date('d F, Y, H:i:s', strtotime($followup->updated_at))}}
                                    @endif
                                  </p>
                                  </div>
                              </li>
                              @empty
                              <li  class="media">
                                No Follow Ups.!!!
                              </li>
                              @endforelse
                          </ul>
                      </div>
                  </div>
              </div>
      </div>
      <div class="col-md-9">
          <div class="timeline timeline-left">
             
              <?php $i=1 ?>
              <div class="timeline-row" >
                  <div class="timeline-icon"> 
                      <div class="bg-info-400">
                          <i class="icon-comment-discussion"></i>
                      </div>
                  </div> 
                  <div class="panel panel-flat timeline-content">
                      <h6 class="panel-heading">Notes</h6>   
                      <div class="panel-body"> 
                          <ul class="media-list chat-list content-group"> 
                              @foreach($leads->notes as $note)
                              <li class="media" >
                                  <div  class="media-left">#{{$i++}}</div>
                                  <div class="media-body">
                                  <p>  {{$note->note}}</p>
                                  <p class="smalltext">Note by: 
                                    <a href="{{route('admin::users.show', $note->user->id)}}"> {{$note->user->name}} </a>
                                  </p>
                                  <p class="smalltext">Created at: 
                                    {{ date('d F, Y, H:i:s', strtotime($note->created_at))}} 
                                    @if($note->updated_at != $note->created_at) 
                                    <br/>Modified at: {{date('d F, Y, H:i:s', strtotime($note->updated_at))}}
                                    @endif
                                  </p>
                                  </div>
                              </li>

                              @endforeach
                          </ul>
                           {!! Form::open(array('url' => array('/leads/notes',$leads->id, ))) !!}
                      <h4 class="">Submit New Note</h4>
                      <div class="form-group">
                        {!! Form::textarea('note', null, ['class' => 'form-control note-box content-group']) !!}
                        
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                    </div>
                   
                  </div>

              </div>
          </div>
          <br />
      </div>
      
      <div class="col-sm-3">
        <div class="panel panel-body border-top-info">
          <h6 class="no-margin text-semibold">Activity Log</h6>
          <div class="well" style="overflow-y:scroll; height:300px;">
            <dl>
              @foreach($leads->logs as $activity)
                <dt>{{date('d, F Y H:i', strTotime($activity->created_at))}}</dt>
                <dd>{{$activity->text}}</dd>
              @endforeach
            </dl>
          </div>
        </div>
        
        <div class="panel panel-body border-top-info">
          <h6 class="no-margin text-semibold">Update information</h6>
            <div class="well">
            
              @if($leads->status != 2)
              @ability('super-admin', 'assign-lead')
                  {!! Form::model($leads, [
                 'method' => 'PATCH',
                  'url' => ['leads/updateassign', $leads->id],
                  ]) !!}
                  <label>Select New Employee To Assign</label>
                  {!! Form::select('fk_user_id_assign', $users, null, ['class' => 'form-control ui search selection top right pointing search-select', 'id' => 'search-select']) !!}
                  {!! Form::submit('Assign new Employee', ['class' => 'btn btn-primary form-control closebtn']) !!}
                  {!! Form::close() !!}
                  <br>
              @endability
                <button class="btn btn-success form-control closebtn movedown" data-toggle="modal" data-target="#modal_form_lead_complete">Complete Lead</button>
               @else
               <label>Can't Assign New User Because Lead Is Completed!</label>
               @endif
            </div>
        </div>
        
        <div class="panel panel-body border-top-warning">
          <h6 class="no-margin text-semibold">Update Basic Info</h6>
            <div class="well">
              @if($leads->status != 2)
                  {!! Form::model($leads, [
                 'method' => 'PATCH',
                  'url' => ['leads/updatebasicinfo', $leads->id],
                  ]) !!}
                  <div class="form-group">
                    {!! Form::label('priority', 'Priority Level:', ['class' => 'control-label ']) !!}
                    {!! Form::select('priority', Config::get('rothmans.lead_priority'), null, ['class' => 'form-control' ] ) !!}
                  </div>
                  <div class="form-group">
                    {!! Form::label('confidence', 'Confidence:', ['class' => 'control-label']) !!}
                    <div class="form-group has-feedback">
                      {!! Form::number('confidence', null, ['class' => 'form-control input-lg']) !!}
                      <div class="form-control-feedback">
                        <i class="icon-percent"></i>
                      </div>
                    </div>
                    <span class="help-block">
                      <strong>{{ $errors->first('confidence') }}</strong>
                    </span>
                  </div>
                  <div class="form-group">
                    {!! Form::label('has_discount', 'Discount offered:', ['class' => 'control-label ']) !!}
                    {!! Form::select('has_discount', Config::get('rothmans.discount_offered'), null, ['class' => 'form-control ' ] ) !!}
                    <span id="discount_fields" style="display:none;">  
                        <div class="form-group">
                            {!! Form::label('discount_value', 'Discount:', ['class' => 'control-label']) !!}
                            <div class="form-group has-feedback">
                                {!! Form::number('discount_value', null, ['class' => 'form-control input-lg', 'disabled' => 'disabled']) !!}
                                <div class="form-control-feedback">
                                    <i class="icon-percent"></i>
                                </div>
                            </div>
                        </div> 
                    </span>
                    @if ($errors->has('has_discount'))
                        <span class="help-block">
                            <strong>{{ $errors->first('has_discount') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('discount_type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('discount_type') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('discount_value'))
                        <span class="help-block">
                            <strong>{{ $errors->first('discount_value') }}</strong>
                        </span>
                    @endif
                  </div>
                  {!! Form::submit('Save', ['class' => 'btn btn-primary form-control closebtn']) !!}
                  {!! Form::close() !!}
               @else
               <label>Can't Change info Because Lead Is Completed!</label>
               @endif
            </div>
        </div>
      </div>
      
    </div>
  </div>
    <div class="clearfix"></div>
    <div class="modal fade" id="ModalFollowUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Change Deadline</h4>
                </div>

                <div class="modal-body">
              
                  {!! Form::model($leads, [
                    'method' => 'PATCH',
                    'route' => ['admin::leads.followup', $leads->id],
                    ]) !!}
                      <div class="col-sm-7">
                        <div class="form-group">
                          {!! Form::label('contact_date', 'Next follow up Date:', ['class' => 'control-label']) !!}
                          {!! Form::date('contact_date', \Carbon\Carbon::now()->addDays(7), ['class' => 'form-control']) !!}
                        </div>
                      </div>
                      <div class="col-sm-2">
                        <div class="form-group">
                          {!! Form::label('contact_time', 'Time:', ['class' => 'control-label']) !!}
                          {!! Form::time('contact_time', '11:00', ['class' => 'form-control']) !!}  
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="col-sm-12">
                        <div class="form-group">
                          {!! Form::label('note', 'Note:', ['class' => 'control-label']) !!}
                          {!! Form::textarea('note', '', ['class' => 'form-control note-box content-group']) !!}
                        </div>
                      </div>
                    <div class="modal-footer">
                        <br />
                        <button type="button" class="btn btn-default col-lg-6" data-dismiss="modal">Close</button>
                        <div class="col-lg-6">
                          {!! Form::submit('Update follow up', ['class' => 'btn btn-success form-control closebtn']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    @include('admin.partials.lead_complete', array('hrusers' => $hrusers, 'lead' => $leads, 'services' => $services, 'plans' => $plans, 'recipients' => $users, 'service_plans' => $service_plans))
@stop

@push('scripts')
<script type="text/javascript">
$(function() {
  jQuery('#has_discount').on('change', function(){
      if(jQuery(this).val() == '1'){
          jQuery('#discount_fields').show();
          jQuery('#discount_value').prop('disabled', false);
      } else {
          jQuery('#discount_fields').hide();
          jQuery('#discount_value').prop('disabled', true);
      }
  });
});
jQuery(document).ready(function(){
    if(jQuery('#has_discount').val() == '1'){
        jQuery('#discount_fields').show();
        jQuery('#discount_value').prop('disabled', false);
    }
});
</script>
@endpush