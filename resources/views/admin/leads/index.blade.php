@extends('admin.layout.app')

@section('page_title', 'Leads')
@section('page_subtitle', 'List of Leads')
@section('content')
<form class="form-horizontal" action="#" id="leads-frm-filter" method="GET">
    <div class="panel panel-flat panel-collapsed">
        <div class="panel-heading">
            <h6 class="panel-title text-semibold"><i class="icon-equalizer position-left"></i> Filters</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse" class=""></a></li>
                </ul>
            </div>
        <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('created_range', 'Created Date:', ['class' => 'control-label col-lg-4']) !!}
                                    <div class="col-lg-8">
                                        {!! Form::text('created_range', null, ['class' => 'form-control daterange-created']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('close_range', 'Expected Close Date:', ['class' => 'control-label col-lg-4']) !!}
                                    <div class="col-lg-8">
                                        {!! Form::text('close_range', null, ['class' => 'form-control daterange-close']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('open_range', 'Expected Open Date:', ['class' => 'control-label col-lg-4']) !!}
                                    <div class="col-lg-8">
                                        {!! Form::text('open_range', null, ['class' => 'form-control daterange-open']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('follow_range', 'Next Follow Up Date:', ['class' => 'control-label col-lg-4']) !!}
                                    <div class="col-lg-8">
                                        {!! Form::text('follow_range', null, ['class' => 'form-control daterange-follow']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('status', 'Status:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                    {!! Form::select('status', [null=>'Select Status'] + Config::get('rothmans.lead_status'), null, ['class' => 'form-control select', 'data-placeholder' => 'Select Status'  ] ) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('created_by', 'Created By:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                    {!! Form::select('created_by', [null=>'Select User'] + json_decode(json_encode($users), true), null, ['class' => 'form-control select', 'data-placeholder' => 'Select User'] ) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('source', 'Soruce:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                    {!! Form::select('source', [null=>'Select Lead Source'] + Config::get('rothmans.lead_source'), null, ['class' => 'form-control select', 'data-placeholder' => 'Select Lead Source' ] ) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('priority', 'Priority:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                    {!! Form::select('priority', [null=>'Select Priority'] + Config::get('rothmans.lead_priority'), null, ['class' => 'form-control select', 'data-placeholder' => 'Select Priority' ] )!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('assigned_to', 'Assigned To:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                    {!! Form::select('assigned_to', [null=>'Select User'] + json_decode(json_encode($assign_users), true), null, ['class' => 'form-control select', 'data-placeholder' => 'Select User' ] )!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary col-sm-3 col-sm-offset-2">Filter <i class="icon-arrow-right14 position-right"></i></button>
                                <a title="Reset Filter!" id="reset_search" href="javascript:" class="btn btn-success btn-rounded col-sm-3 col-sm-offset-2">Reset <i class="icon-spinner11 position-right"></i></a>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="panel panel-flat">
    <div  class="panel-body" >
        <table class="table datatable-basic" id="leads-table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Created by</th>
                    <th>Created On</th>
                    <th>Close Date</th>
                    <th>Open Date</th>
                    <th>Next Follow</th>
                    <th>Assigned</th>
                    <th>Stage</th>
                    <th>Source</th>
                    <th>Priority</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>    
@stop

@push('scripts')
<script>
$(function() {
    $('.daterange-created').daterangepicker({
        applyClass: 'bg-info',
        cancelClass: 'btn-default'
    });
    $('.daterange-close').daterangepicker({
        applyClass: 'bg-danger',
        cancelClass: 'btn-default'
    });
    $('.daterange-open').daterangepicker({
        applyClass: 'bg-warning',
        cancelClass: 'btn-default'
    });
    $('.daterange-follow').daterangepicker({
        applyClass: 'bg-success',
        cancelClass: 'btn-default'
    });
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [{ 
            orderable: false,
            width: '100px'
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    var oTable = $('#leads-table').DataTable({
        processing: true,
        serverSide: true,
        "order": [ [2, 'desc'] ],
        ajax: {
            url: '{!! route('admin::leads.data', ['status' => '0']); !!}',
            data: function (d) {
                d.search = $('.dataTables_filter input[type=search]').val();
                d.status = $('#status').val();
                d.priority = $('#priority').val();
                d.source = $('#source').val();
                d.created_by = $('#created_by').val();
                d.assigned_to = $('#assigned_to').val();
                d.created_range = $('#created_range').val();
                d.close_range = $('#close_range').val();
                d.open_range = $('#open_range').val();
                d.follow_range = $('#follow_range').val();
            }
        },
        columns: [
            { data: 'titlelink', name: 'title' },
            { data: 'fk_user_id_created', name: 'fk_user_id_created'},
            { data: 'created_at', name: 'created_at', },
            { data: 'close_date', name: 'close_date', },
            { data: 'open_date', name: 'open_date', },
            { data: 'contact_date', name: 'contact_date', },
            { data: 'fk_user_id_assign', name: 'fk_user_id_assign' },
            { data: 'status', name: 'status' },
            { data: 'source', name: 'source' },
            { data: 'priority', name: 'priority' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false, class: 'center'}
        ]
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Name Search...');

    $('.dataTables_filter input[type=search]').on('input',function(e){
        oTable.draw();
    });

    $('#leads-frm-filter').on('submit', function(e) {
        e.preventDefault();
        oTable.draw();
    });
   
    $('#reset_search').on('click', function(e) {
        oTable.search( '' );
        $('#leads-frm-filter').trigger("reset");
        $('.select').select2();
        oTable.draw();
    });

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });
    $('.select').select2({
    });
    
});
</script>
@endpush