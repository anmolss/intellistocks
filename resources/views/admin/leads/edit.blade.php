@extends('admin.layout.app')

@section('page_title', 'Leads')
@section('page_subtitle', 'Edit Lead')

@section('content')
<style type="text/css">
.help-block{
    color: #c10000;
    font-weight: normal !important;
}
</style>
<div class="panel panel-flat"> 
    <div class="panel-body"> 
         {!! Form::model($lead, [
                'method' => 'PATCH',
                'route' => ['admin::leads.update', $lead->id],
                ]) !!}
    <div class="row"> 
        <div class="col-md-12">   
            <div class="form-group">
                {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            
                @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
                </div>
        </div>
    </div>
     <div class="row"> 
        <div class="col-md-12"> 
            <div class="form-group">
                {!! Form::label('note', 'Note:', ['class' => 'control-label']) !!}
                {!! Form::textarea('note', null, ['class' => 'form-control']) !!}
                @if ($errors->has('note'))
                    <span class="help-block">
                        <strong>{{ $errors->first('note') }}</strong>
                    </span>
                @endif
            </div>
         </div>
     </div>       
    <div class="row form-inline">
        <div class="col-md-5 form-group"> 
            
            {!! Form::label('lead_type', 'Purpose:', ['class' => 'control-label ']) !!}
            {!! Form::select('lead_type', Config::get('rothmans.lead_purpose'), null, ['class' => 'form-control ' ] )
             !!}
            @if ($errors->has('lead_type'))
                <span class="help-block">
                    <strong>{{ $errors->first('lead_type') }}</strong>
                </span>
            @endif
           
        </div>
        <div class="col-md-5 form-group"> 
          
            {!! Form::label('status', 'Status:', ['class' => 'control-label ']) !!}
            {!! Form::select('status', Config::get('rothmans.lead_status'), null, ['class' => 'form-control' ] )
             !!}
            @if ($errors->has('status'))
                <span class="help-block">
                    <strong>{{ $errors->first('status') }}</strong>
                </span>
            @endif
        </div>      
        <div style="clear:both"></div>
    </div>

     <div class="row form-inline">
        <div class="col-md-5 form-group"> 
           
            {!! Form::label('source', 'Soruce:', ['class' => 'control-label ']) !!}
            {!! Form::select('source', Config::get('rothmans.lead_source'), null, ['class' => 'form-control ' ] )
             !!}
            @if ($errors->has('source'))
                <span class="help-block">
                    <strong>{{ $errors->first('source') }}</strong>
                </span>
            @endif
           
        </div>
        <div class="col-md-5 form-group"> 
          
            {!! Form::label('capital', 'Capital:', ['class' => 'control-label ']) !!}
            {!! Form::select('capital', Config::get('rothmans.capital'), null, ['class' => 'form-control' ] )
             !!}
            @if ($errors->has('capital'))
                <span class="help-block">
                    <strong>{{ $errors->first('capital') }}</strong>
                </span>
            @endif
        </div>      
        <div style="clear:both"></div>
    </div>

    <div class="row form-inline">
        <div class="col-md-5 form-group"> 
           
            {!! Form::label('has_discount', 'Discount offered:', ['class' => 'control-label ']) !!}
            {!! Form::select('has_discount', Config::get('rothmans.discount_offered'), null, ['class' => 'form-control ' ] ) !!}
            <span id="discount_fields" style="display:none;">  
                <div class="form-group">
                    {!! Form::label('discount_value', 'Discount:', ['class' => 'control-label']) !!}
                    <div class="form-group has-feedback">
                        {!! Form::number('discount_value', null, ['class' => 'form-control input-lg', 'disabled' => 'disabled']) !!}
                        <div class="form-control-feedback">
                            <i class="icon-percent"></i>
                        </div>
                    </div>
                </div> 
            </span>
            @if ($errors->has('has_discount'))
                <span class="help-block">
                    <strong>{{ $errors->first('has_discount') }}</strong>
                </span>
            @endif
            @if ($errors->has('discount_type'))
                <span class="help-block">
                    <strong>{{ $errors->first('discount_type') }}</strong>
                </span>
            @endif
            @if ($errors->has('discount_value'))
                <span class="help-block">
                    <strong>{{ $errors->first('discount_value') }}</strong>
                </span>
            @endif
           
        </div>
        <div class="col-md-5 form-group"> 
          
            {!! Form::label('priority', 'Priority Level:', ['class' => 'control-label ']) !!}
            {!! Form::select('priority', Config::get('rothmans.lead_priority'), null, ['class' => 'form-control' ] )
             !!}
            @if ($errors->has('priority'))
                <span class="help-block">
                    <strong>{{ $errors->first('priority') }}</strong>
                </span>
            @endif
        </div>      
        <div style="clear:both"></div>
    </div>
     <div class="row form-inline">
       <div class="form-group col-md-5 ">
            {!! Form::label('open_date', 'Open Date:', ['class' => 'control-label']) !!}
            {!! Form::date('open_date', $lead['open_date'] ? $lead['open_date']->format('Y-m-d') : '', ['class' => 'form-control']) !!}
            @if ($errors->has('open_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('open_date') }}</strong>
                </span>
            @endif
            {!! Form::time('open_time', $lead['open_date'] ? $lead['open_date']->format('h:i') : '', ['class' => 'form-control']) !!}
            @if ($errors->has('open_time'))
                <span class="help-block">
                    <strong>{{ $errors->first('open_time') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group col-md-5 ">
            {!! Form::label('contact_date', 'Next follow up:', ['class' => 'control-label']) !!}
            {!! Form::date('contact_date', $lead['contact_date'] ? $lead['contact_date']->format('Y-m-d') : '', ['class' => 'form-control']) !!}
            @if ($errors->has('contact_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('contact_date') }}</strong>
                </span>
            @endif
            {!! Form::time('contact_time', $lead['contact_date'] ? $lead['contact_date']->format('h:i') : '', ['class' => 'form-control']) !!}
            @if ($errors->has('contact_time'))
                <span class="help-block">
                    <strong>{{ $errors->first('contact_time') }}</strong>
                </span>
            @endif
        </div>
       

    </div>
    <div class="row form-inline">
       
        
        <div class="form-group col-md-5 ">
            {!! Form::label('close_date', 'Expect Close Date:', ['class' => 'control-label']) !!}
            {!! Form::date('close_date', $lead['close_date'] ? $lead['close_date']->format('Y-m-d') : '', ['class' => 'form-control']) !!}
            @if ($errors->has('close_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('close_date') }}</strong>
                </span>
            @endif
            
            {!! Form::time('close_time', $lead['contact_date'] ? $lead['contact_date']->format('h:i') : '', ['class' => 'form-control']) !!}
            @if ($errors->has('close_time'))
                <span class="help-block">
                    <strong>{{ $errors->first('close_time') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group col-md-5 ">
            {!! Form::label('confidence', 'Confidence:', ['class' => 'control-label']) !!}
            {!! Form::text('confidence', null, ['class' => 'form-control']) !!} %
            @if ($errors->has('confidence'))
                <span class="help-block">
                    <strong>{{ $errors->first('confidence') }}</strong>
                </span>
            @endif
            
           
        </div>
    </div>
   
   <div class="row form-inline">

        <div class="form-group col-md-5">
            {!! Form::label('fk_user_id_assign', ' Assign Employee:', ['class' => 'control-label']) !!}
            {!! Form::select('fk_user_id_assign', $users, null, ['class' => 'form-control select2-search']) !!}
            @if ($errors->has('fk_user_id_assign'))
                <span class="help-block">
                    <strong>{{ $errors->first('fk_user_id_assign') }}</strong>
                </span>
            @endif
        </div>
        
        <div class="form-group col-md-5">
             @if(Request::get('client') != "")
             {!! Form::hidden('fk_client_id', Request::get('client')) !!}
             @else
            {!! Form::label('fk_client_id', 'Assign Client:', ['class' => 'control-label']) !!}
            {!! Form::select('fk_client_id', $clients, null, ['class' => 'form-control select2-search']) !!}
            @endif
            @if ($errors->has('fk_client_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('fk_client_id') }}</strong>
                </span>
            @endif

            @ability('super-admin', 'create-client')
                <button type="button" class="btn btn-success btn-icon btn-xs" data-toggle="modal" data-target="#add_client_modal"><i class="icon-plus3"></i></button>
            @endability
        </div>
       <div style="clear:both"></div>

    </div>

    <div class="form-group pull-right " style="padding:10px;">

      {!! Form::submit('Update Lead', ['class' => 'btn btn-primary']) !!}
    </div>

</div>
  
    </div>

{!! Form::close() !!}
</div>
@ability('super-admin', 'create-client')
@include('admin.partials.add_client_modal', array('services' => $services))
@endability
@stop
@push('scripts')
<script type="text/javascript">
$(function() {
    $('.select2-search').select2({width: '40%'});
    $('.select2-search-modal').select2();
    jQuery('#has_discount').on('change', function(){
        if(jQuery(this).val() == '1'){
            jQuery('#discount_fields').show();
            jQuery('#discount_value').prop('disabled', false);
        } else {
            jQuery('#discount_fields').hide();
            jQuery('#discount_value').prop('disabled', true);
        }
    });
});
jQuery(document).ready(function(){
    if(jQuery('#has_discount').val() == '1'){
        jQuery('#discount_fields').show();
        jQuery('#discount_value').prop('disabled', false);
    }
});
</script>
@ability('super-admin', 'create-client')
<script type="text/javascript">
    (function ($) {
    jQuery.fn.serializeFormJSON = function () {

            var o = {};
            var a = this.serializeArray();
            jQuery.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    })(jQuery);

    jQuery( '.person_create_form' ).submit(function( event ) {
        event.preventDefault();
        jQuery('.form_spinner').hide();
        var data = jQuery(this).serializeFormJSON();
        var $url = jQuery(this).attr('action');
        jQuery.ajax({
            type: 'POST',
            url: $url,
            data: data,
            dataType: "json",
            beforeSend: function() {
                // setting a timeout
                jQuery('.form_spinner').show();
            },
            success: function(data) {
                if(data.success == 1){
                    jQuery('#add_client_modal').modal('toggle');
                    jQuery('#add_client_form').trigger("reset");
                    jQuery('span.help-block').html('');
                    jQuery('#fk_client_id').html(data.clients);
                    jQuery('#fk_client_id').select2({width: '40%'});
                    swal("Client Added!", "Client Has Been Added!", "success")
                }
            },
            error: function(data) { // if error occured
                jQuery('span.help-block').hide();
                var errors = data.responseJSON;
                    jQuery.each(errors, function(index, item) {
                        if (jQuery('#error_' + index).length > 0) {
                            jQuery('#error_' + index).show().html(item['0']);
                        }
                    });
                jQuery('span.help-block').animate({scrollTop: 0}, "slow");
            },
            complete: function() {
                jQuery('.form_spinner').hide();
            }
        });
        
    });
</script>
@endability
@endpush