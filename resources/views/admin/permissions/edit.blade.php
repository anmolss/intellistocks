@extends('admin.layout.app')



@section('page_title', 'Permission')
@section('page_subtitle', 'Edit permission')


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Permission <small class="text-bold">Edit</small>
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->put()->action(route('admin::permissions.update', ['permissions' => $item->id] ))->multipart() !!}
            {!! BootForm::bind($item) !!}

            @include('admin.permissions._form', ['submitBtnText' => "Edit Permission"])

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection