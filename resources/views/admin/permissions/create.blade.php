@extends('admin.layout.app')

@section('page_title', 'Role')
@section('page_subtitle', 'Create new Role')


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Permission <small class="text-bold">Add</small>
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->post()->action(route('admin::permissions.store'))->multipart() !!}
                @include('admin.permissions._form', ['submitBtnText' => "Add New Permission"])
            {!! BootForm::close() !!}
        </div>
    </div>

@endsection