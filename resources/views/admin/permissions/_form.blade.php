<fieldset>
    <div class="row">
        <div class="col-md-6">
            {!! BootForm::text('Display Name', 'display_name')->placeholder('Display Name')->required()!!}
            {!! BootForm::textarea('Description', 'description')->rows(3)->placeholder('description')->helpBlock('Description ') !!}
        </div>

        <div class="col-md-6">
            {!! BootForm::text('Type', 'controllers')->placeholder('Type e.g. Users, Roles, etc.')->required()!!}
            {!! BootForm::text('Group Type', 'group_by')->placeholder('description')->helpBlock('Group All types for proper view ') !!}
        </div>
    </div>
</fieldset>
<div class="row">
    <div class="col-md-6">
        <div>
            Required Fields
        </div>
    </div>
    <div class="col-md-4 ">
        {!! BootForm::submit($submitBtnText . ' <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary') !!}
    </div>
</div>