@extends('admin.layout.app')

@section('page_title', 'Service')
@section('page_subtitle', 'Edit Service')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Service <small class="text-bold">Edit</small>
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->put()->action(route('admin::services.update', ['roles' => $item->id] ))->multipart() !!}
            {!! BootForm::bind($item) !!}

            @include('admin.services._form', ['submitBtnText' => "Edit Service"])

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection