@extends('admin.layout.app')

@section('page_title', 'Renew Service')
@section('page_subtitle', 'Enter Details to Renew Service')

@section('content')
<style type="text/css">
.help-block{
    color: #c10000;
    font-weight: normal !important;
}
</style>
<div class="row edit_row">
  <!-- Advanced legend -->
  <div class="form-horizontal">
    <div class="panel panel-flat">
      <div class="panel-heading">
        <h5 class="panel-title">Renew Service Form</h5>
      </div>

      <div class="panel-body">
        {!! Form::open(['route' => 'admin::clients.store_client_user', 'class' => 'ui-form client_create_form']) !!}
          <input type="hidden" name="customer_id" value="{{$customer->id}}">
          <input type="hidden" name="payment_mode" value="0">
          <input type="hidden" name="user_service_renew" value="yes">
          
          <fieldset>
            <legend class="text-semibold">
              	<i class="icon-user-tie position-left"></i>
              	Customer Info
              	<a class="control-arrow" data-toggle="collapse" data-target="#demo1">
                	<i class="icon-circle-down2"></i>
              	</a>
            </legend>

            <div class="collapse in" id="demo1">
              	<div class="col-sm-12 form-group"> 
	              	<h1 class="text-center">Customer : {{$customer->username}}</h1>
              	</div>
            </div>
          </fieldset>

          <fieldset>
            <legend class="text-semibold">
              <i class="icon-magazine position-left"></i>
              Services & Plans
              <a class="control-arrow" data-toggle="collapse" data-target="#demo2">
                <i class="icon-circle-down2"></i>
              </a>
            </legend>

            <div class="collapse in" id="demo2">
              <div class="form-group" id="service_append">
                <div class="col-sm-12">
                <input type="hidden" name="services[0][user_service_id]" value="{{$service['id']}}">
                  <div class="col-sm-12 well service_row" data-count='0'>
                    <div class="row">
                      <div class="col-sm-6">
                        {!! Form::label('services[0][fk_service_id]', 'Service Type:', ['class' => 'control-label']) !!}
                        {!! Form::select('services[0][fk_service_id]', [null=>'Select Service'] + $services->toArray(), $service['service_id'], ['class' => 'form-control select2-search-modal service_type', 'disabled'] )!!}
                        <input type="hidden" name="services[0][fk_service_id]" value="{{$service['service_id']}}">
                        <span class="help-block error_services_fk_service_id" style="display:none;"></span>
                      </div>
                      
                      <div class="col-sm-6">
                        {!! Form::label('fk_plan_id', 'Plan:', ['class' => 'control-label']) !!}
                        {!! Form::select('services[0][fk_plan_id]', [null=>'Select Plan'] + $plans->toArray(), $service['plan_id'], ['class' => 'form-control select2-search-modal plans', 'disabled'] )!!}
                        <input type="hidden" name="services[0][fk_plan_id]" value="{{$service['plan_id']}}">
                        <span class="help-block error_services_fk_plan_id" style="display:none;"></span>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-6" id="capital_container" style="display:none;">
                        <div class="form-group">
                          <label class="control-label">Enter Capital:</label>
                          <input type="text" name="services[0][capital]" class="form-control reflect_capital_amnt" value="">
                          <span class="help-block error_services_capital" style="display:none;"></span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group" id="discount-container" style="display:none">
                            <label for="" class="control-label">Discount: </label>
                            <input class="form-control reflect_amt_key_up edit_discount txt-discount" placeholder="Discount (%)" name="services[0][discount]" type="number" min="0" max="0" value="">
                          <span class="help-block error_discount" style="display:none;"></span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                          {!! Form::label('payment_type[]', 'Payment Type:', ['class' => 'control-label']) !!}
                          {!! Form::select('services[0][payment_type]', ['' => 'Select Payment Type', 'full' => 'Full', 'partial' => 'Partial'], null, ['class' => 'form-control select2-search-modal payment_type'] )!!}
                        <span class="help-block error_services_payment_type" style="display:none;"></span>
                      </div>

                      <div class="col-sm-4">
                          {!! Form::label('fk_payment_type', 'Payment:', ['class' => 'control-label']) !!}
                          {!! Form::select('services[0][fk_payment_type]', ['' => 'Select Payment Type', 'cash' => 'Cash', 'cheque' => 'Cheque', 'net_banking' => 'Net Banking'], null, ['class' => 'form-control select2-search-modal fk_payment_type'] )!!}
                        <span class="help-block error_services_fk_payment_type" style="display:none;"></span>
                      </div>

                      <div class="col-sm-4">
                        <label class="control-label">Activation Date:</label>
                        <input type="text" name="services[0][activation_date]" class="form-control date-picker service-activation-date" data-date-format="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>">
                        <span class="help-block error_services_activation_date" style="display:none;"></span>
                      </div>
                    </div>
                    <br>
                    <div class="payment_append form-group">
                        <div class="col-sm-12">
                          <div class="row payment_type_append"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="plan_append">
                      <div class="col-sm-12" style="margin-top:10px;">
                          <div class="table-responsive">
                              <table class="table table-xxs table-bordered">
                                  <thead>
                                      <tr>
                                          <th colspan="10" class="text-center bg-primary">Plan Details</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td class="text-center">Please Select Service & Plan</td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="plan_spinner col-sm-1 text-center" style="display:none;">
                        <i class="icon-spinner2 spinner"></i>
                    </div>

                    <div class="row form-group" style="margin-top:10px;">
                    	<div class="well text-center">
		                    <ul class="list-inline list-inline-separate no-margin-bottom">
          								<li><span><strong>Comments : </strong></span>{{$service['comments']}}</li>
          								<li><span><strong>Force Verified : </strong></span>{{$service['service_verified']}}</li>
          								<li><span><strong>Force Verify Reason : </strong></span>{{$service['force_verify_reason']}}</li>
          							</ul>
          						</div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </fieldset>

          <div class="text-right">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                <div class="form_spinner col-sm-1 text-center" style="display:none;">
                    <i class="icon-spinner2 spinner"></i>
                </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  <!-- /a legend -->
</div>
@include('admin.partials.jquery_service_plan_templates')
@stop

@push('scripts')
<script type="text/javascript">
  var $total_services;
  var $service_plans;
  var $getPlanUrl = '{{url('getPlan')}}';
  var $getCustomerUrl = "{{route('admin::clients.customers')}}";
  var $is_edit = false;
  var $change_warning_count = 1;
  jQuery(window).load(function(){
    $total_services = 0;
    $service_plans = <?php echo json_encode($service_plans); ?>;
    var $plan_id = $('.edit_row').find(".plans").val();
    var $el = $('.edit_row').find(".plans");
  	var $el_details = $('.edit_row').find(".plan_append");

    getPlan($plan_id, $el, $el_details);
  });
</script>
@endpush