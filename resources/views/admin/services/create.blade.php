@extends('admin.layout.app')

@section('page_title', 'Service')
@section('page_subtitle', 'Create New Service')


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Service <small class="text-bold">Add</small>
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->post()->action(route('admin::services.store')) !!}
                @include('admin.services._form', ['submitBtnText' => "Add New Service"])
            {!! BootForm::close() !!}
        </div>
    </div>

@endsection