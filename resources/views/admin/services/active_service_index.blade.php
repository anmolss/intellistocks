@extends('admin.layout.app')

@section('page_title', 'Active Services')
@section('page_subtitle', 'List of Assigned Services')
@section('content')
<form class="form-horizontal" action="#" id="service-frm-filter" method="GET">
    <div class="panel panel-flat panel-collapsed">
        <div class="panel-heading">
            <h6 class="panel-title text-semibold"><i class="icon-equalizer position-left"></i> Filters</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse" class=""></a></li>
                </ul>
            </div>
        <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('created_range', 'Created at:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::text('created_range', null, ['class' => 'form-control daterange']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('service', 'Service:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::select('service', $services, null, ['class' => 'form-control select service_type', 'multiple' => 'multiple', 'id' => 'services'  ] ) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('start_range', 'Active Date:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::text('start_range', null, ['class' => 'form-control daterange']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('plan', 'Plan:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                    {!! Form::select('plan', $plans, null, ['class' => 'form-control select select_plans', 'multiple' => 'multiple', 'id' => 'plans'] ) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('expire_range', 'Expire on:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::text('expire_range', null, ['class' => 'form-control daterange']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    {!! Form::label('service_status', 'Service Status:', ['class' => 'control-label col-lg-3']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::select('service_status', [null => 'Select Status'] + Config::get('rothmans.service_statuses'), null, ['class' => 'form-control ' ] )!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-primary">Filter <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    {!! Form::label('kyc_status', 'KYC Status:', ['class' => 'control-label col-lg-4']) !!}
                                    <div class="col-lg-8">
                                        {!! Form::select('kyc_status', [null => 'Select Status'] + ['Yes' => 'Yes', 'No' => 'No'], null, ['class' => 'form-control ' ] )!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('transcript_status', 'Transcript Status:', ['class' => 'control-label col-lg-4']) !!}
                                        <div class="col-lg-8">
                                            {!! Form::select('transcript_status', [null => 'Select Status'] + ['Yes' => 'Yes', 'No' => 'No'], null, ['class' => 'form-control ' ] )!!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('consent_status', 'Consent Status:', ['class' => 'control-label col-lg-4']) !!}
                                        <div class="col-lg-8">
                                            {!! Form::select('consent_status', [null => 'Select Status'] + ['Yes' => 'Yes', 'No' => 'No'], null, ['class' => 'form-control ' ] )!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <a title="Reset Filter!" id="reset_search" href="javascript:" class="btn btn-success btn-rounded">Reset <i class="icon-spinner11 position-right"></i></a>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-xs-4">
                <div class="media-left media-middle">
                    <span class="btn border-success text-success btn-flat btn-rounded btn-xs btn-icon"><i class="icon-check2"></i></span>
                </div>
                <div class="media-left">
                    <h5 class="text-semibold no-margin">
                        {{$counts->Active}} <small class="display-block no-margin">Active Services</small>
                    </h5>
                </div>
            </div>
            <div class="col-md-2 col-md-3 col-xs-4">
                <div class="media-left media-middle">
                    <span class="btn border-warning text-warning btn-flat btn-rounded btn-xs btn-icon"><i class="icon-esc"></i></span>
                </div>
                <div class="media-left">
                    <h5 class="text-semibold no-margin">
                        {{$counts->Pending}} <small class="display-block no-margin">Pending Services</small>
                    </h5>
                </div>
            </div>
            <div class="col-md-2 col-md-3 col-xs-4">
                <div class="media-left media-middle">
                    <span class="btn border-orange-800 text-orange-800 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-sort-time-asc"></i></span>
                </div>
                <div class="media-left">
                    <h5 class="text-semibold no-margin">
                        {{$counts->Expired}} <small class="display-block no-margin">Expired Services</small>
                    </h5>
                </div>
            </div>
            <div class="col-md-2 col-md-3 col-xs-4">
                <div class="media-left media-middle">
                    <span class="btn border-teal-300 text-teal-300 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-reload-alt"></i></span>
                </div>
                <div class="media-left">
                    <h5 class="text-semibold no-margin">
                        {{$counts->Renewed}} <small class="display-block no-margin">Renewed Services</small>
                    </h5>
                </div>
            </div>
            <div class="col-md-2 col-md-3 col-xs-4">
                <div class="media-left media-middle">
                    <span class="btn border-primary text-primary btn-flat btn-rounded btn-xs btn-icon"><i class="icon-exclamation"></i></span>
                </div>
                <div class="media-left">
                    <h5 class="text-semibold no-margin">
                        {{$counts->Suspended}} <small class="display-block no-margin">Suspended Services</small>
                    </h5>
                </div>
            </div>
            <div class="col-md-2 col-md-3 col-xs-4">
                <div class="media-left media-middle">
                    <span class="btn border-info text-info btn-flat btn-rounded btn-xs btn-icon"><i class="icon-arrow-up12"></i></span>
                </div>
                <div class="media-left">
                    <h5 class="text-semibold no-margin">
                        {{$counts->TempExtended}} <small class="display-block no-margin">Temp. Extended Services</small>
                    </h5>
                </div>
            </div>
            <div class="col-md-2 col-md-3 col-xs-4">
                <div class="media-left media-middle">
                    <span class="btn border-slate-800 text-slate-800 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-cross2"></i></span>
                </div>
                <div class="media-left">
                    <h5 class="text-semibold no-margin">
                        {{$counts->Deleted}} <small class="display-block no-margin">Deleted Services</small>
                    </h5>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
       <table class="table table-hover" id="services-table">
            <thead>
                <tr>
                    <th>Service</th>
                    <th>Plan</th>
                    <th>Status</th>
                    <th>Start Date</th>
                    <th>Expiry Date</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>   
@include('admin.partials.action_modal')
<div id="verify_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
          <form class="verify_modal_form" id="verify_form" action="{{route('admin::verifyService')}}" enctype="multipart/form-data">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Verify Service</h5>
            </div>
            <div class="modal-body">
              <input type="hidden" class="user_service_id" name="user_service_id" value="">
              <div class="col-md-12">
                <label style="font-weight:bold;">Service Name : </label>
                <span id="verify_s_name"></span>  
              </div>
              <div class="col-md-12">
                <label style="font-weight:bold;">Plan :</label>
                <span id="verify_p_name"></span>  
              </div>
              <div class="clearfix"></div>
              <div class="col-md-6">
                <label class="checkbox-inline">
                  <input type="checkbox" name="transcript_status" value="yes" class="transcript_status checkbox">
                  <span class="text-bold text-danger">Transcript of Onboarding call</span>
                </label>
              </div>
              <div class="col-md-6 transcript_file" style="display:none;">
                <input type="file" name="transcript_file">
              </div>
              <div class="clearfix"></div>
              <br>
              <div class="col-md-6">
                <label class="checkbox-inline">
                  <input type="checkbox" name="kyc_status" value="yes" class="kyc_status checkbox">
                  <span class="text-bold text-danger">KYC Form</span>
                </label>
              </div>
              <div class="col-md-6 kyc_file" style="display:none;">
                <input type="file" name="kyc_file">
              </div>
              <div class="clearfix"></div>
              <br>
              <div class="col-md-6">
                <label class="checkbox-inline">
                  <input type="checkbox" name="consent_status" value="yes" class="consent_status checkbox">
                  <span class="text-bold text-danger">Consent</span>
                </label>
              </div>
              <div class="col-md-6">
                <p><strong>Consent IP : </strong><span class="consent_ip"></span></p>
              </div>
              <div class="clearfix"></div>
              <br>
              <div class="alert alert-warning verify_warning" style="display:none;">You are about to activate this service after accepting all checks<br/>Activation Date is : <span class="text-semibold activation_date">N/A</span> <br/>Expiry date would be : <span class="text-semibold expiry_date">N/A</span></div>
              <div class="clearfix"></div>
              <div class="alert alert-warning balance_warning" style="display:none;">Payment of this Service is not Cleared yet Are you sure you want to continue?</div>
              <div class="col-md-12 verify-alert" style="margin:10px 0px;display:none;">
                 <div class="alert alert-success"></div>
              </div>
            </div>
            <div class="modal-footer">
              <span class="verify_service_form_spinner" style="display:none;">
                <i class="icon-spinner2 spinner"></i>
              </span>
              <button type="submit" class="btn btn-primary save_verification">Save</button>
              <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
          </form>
            <div id="modal_content_append"></div>
        </div>
    </div>
</div>
@stop

@push('scripts')
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.service_type').on('change', function(){
            var services = jQuery(this).val();
            jQuery.ajax({
                type: 'POST',
                url: '{{url('getMultiplePlans')}}',
                data: {'services':services},
                dataType: "json",
                success: function(data) {
                    jQuery(".select_plans").empty();
                    jQuery.each(data[0], function(key,value) {
                        jQuery(".select_plans").append(jQuery("<option></option>").attr("value", key).text(value));
                        jQuery('select').select2({
                            minimumResultsForSearch: Infinity,
                        });
                    });
                }
            });
        });
    });
</script>
<script>
$(function() {
    $('.daterange').daterangepicker({
        applyClass: 'bg-info',
        cancelClass: 'btn-default'
    });
    $('.daterange').val('');
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [{ 
            orderable: false,
            width: '100px'
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    var oTable = $('#services-table').DataTable({
        processing: true,
        serverSide: true,
        "order": [ [2, 'desc'] ],
        ajax: {
            url: '{!! route('admin::services.activeServiceData') !!}',
            data: function (d) {
                d.search = $('.dataTables_filter input[type=search]').val();
                d.service = $('#services').val();
                d.plan = $('#plans').val();
                d.created_range = $('#created_range').val();
                d.start_range = $('#start_range').val();
                d.expire_range = $('#expire_range').val();
                d.service_status = $('#service_status').val();
                d.kyc_status = $('#kyc_status').val();
                d.transcript_status = $('#transcript_status').val();
                d.consent_status = $('#consent_status').val();
            }
        },
        columns: [
            { data: 'service.name', name: 'service_name' },
            { data: 'plan.name', name: 'plan_name' },
            { data: 'service_status', name: 'service_status' },
            { data: 'activation_date', name: 'activation_date' },
            { data: 'expire_date', name: 'expire_date' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false},
        ]
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Name Search...');

    $('.dataTables_filter input[type=search]').on('input',function(e){
        oTable.draw();
    });

    $('#service-frm-filter').on('submit', function(e) {
        e.preventDefault();
        oTable.draw();
    });
   
    $('#reset_search').on('click', function(e) {
        oTable.search( '' );
        $('#service-frm-filter').trigger("reset");
        $('select').select2();
        oTable.draw();
    });

    // Enable Select2 select for the length option
    $('select').select2({
        minimumResultsForSearch: Infinity
    });
});
</script>
@endpush