@extends('admin.layout.app')

@section('page_title', 'Portfolio')
@section('page_subtitle', 'Create Portfolio')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="over-title margin-bottom-15">
                Portfolio
                <a href="{{ url()->previous() }}">Back</a>
            </h5>
        </div>
        <div class="panel-body" data-ng-controller="MainCtrl as ctrl">
            <a class="btn btn-default" ng-click="addStock()">Add Stock</a>

            <div id="grid1" ui-grid="gridOptions" ui-grid-edit ui-grid-selection ui-grid-exporter ui-grid-cellnav class="grid"></div>
        </div>

    </div>


    <script type="text/ng-template" id="uiSelect">
        <ui-select-wrap>
            <ui-select ng-model="MODEL_COL_FIELD" theme="selectize" ng-disabled="disabled" append-to-body="true">
                <ui-select-match placeholder="Choose...">@{{ COL_FIELD }}</ui-select-match>
                <ui-select-choices repeat="item in col.colDef.editDropdownOptionsArray | filter: $select.search">
                    <span>@{{ item }}</span>
                </ui-select-choices>
            </ui-select>
        </ui-select-wrap>
    </script>

@endsection


@section('styles')
    @parent

    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">

@endsection

@section('scripts')
    @parent
    {{--<script type="text/javascript" src="{{asset('js/ui-grid-edit-datepicker.js')}}"></script>--}}

    <script type="text/javascript">
        app.service('GetStocks', function($http){

            this.stocks = function () {
                return $http.get('{{route('admin::api.get_stock_list')}}');
            };

        });

        app.controller('MainCtrl', ['$scope', '$http', '$log', '$timeout', 'uiGridConstants', 'GetStocks', function ($scope, $http, $log, $timeout, uiGridConstants, GetStocks) {
            var today = new Date();
            var nextWeek = new Date();
            nextWeek.setDate(nextWeek.getDate() + 7);
            $scope.myData = [
                {
                    "index": 0,
                    "guid": "cca75401-c249-4e1b-a37b-cabf405e691a",
                    "isActive": false,
                    "balance": "3319.02",
                    "percent": 40,
                    "company": 10510008,
                    "mixedDate": "1967-3-12"
                },
                {
                    "index": 1,
                    "guid": "3a803ff7-a162-4bc9-be53-620c84e7684d",
                    "isActive": true,
                    "balance": "1952.59",
                    "percent": 22,
                    "company": 10520003,
                    "mixedDate": "1980-11-24"
                }
            ];

            $scope.addStock = function(){
                $scope.myData.push({
                    "index": '',
                    "guid": '',
                    "isActive": true,
                    "balance": 0,
                    "percent": 0,
                    "company": 'Please type name',
                    "mixedDate": null
                });

                {{--
                $http.get('{{route('admin::event.my_event', ['msg'=>"Stock Added in Portfolio"])}}')
                .success(function(data) {
                });
                --}}

            };

            $scope.stocks = [];
            GetStocks.stocks().then(function(data){
                data.data.forEach(function(company){
                    $scope.stocks[company.id] = company.name;
                });
            });

            console.log($scope.stocks);


            $scope.gridOptions = {
                data: $scope.myData,
                //enableFiltering: true,
                enableRowSelection: true,
                enableSelectAll: true,
                selectionRowHeaderWidth: 35,
                enableGridMenu: true,
                rowHeight: 35,
                showGridFooter:false,
                showColumnFooter: true,
                enableCellEditOnFocus: true,
                onRegisterApi: function(gridApi){
                    $scope.gridApi = gridApi;
                },
                enableCellSelection:true,
                columnDefs: [
                    {
                        field: 'company' ,
                        //cellFilter: "mapCompanyName",
                        pinnedLeft:true,
                        editableCellTemplate: 'uiSelect',
                        editDropdownOptionsArray: $scope.stocks
                    },
                    {
                        field: 'percent',
                        cellFilter: 'percentage',
                        filterHeaderTemplate: '<div class="ui-grid-filter-container" ng-repeat="colFilter in col.filters"><div my-custom-modal></div></div>',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        aggregationHideLabel: true
                    },
                    { field: 'balance', cellFilter: 'currency:"&#8377;"', aggregationType: uiGridConstants.aggregationTypes.sum,},
                    {
                        field: 'mixedDate', cellFilter: 'textDate:"dd-MM-yyyy"', type:'date', enableFiltering: true
                    }
                ]
            };

        }])
        /*.filter('mapCompanyName', function(GetStocks) {
            var companies = GetStocks.list();
            return function(input) {
                if (!input){
                    return '';
                } else {
                    return companies[input];
                }
            };
        })*/
        .filter('percentage', ['$filter', function ($filter) {
            return function (input, decimals) {
                return $filter('number')(input, decimals) + '%';
            };
        }])
        .directive('myCustomDropdown', function() {
            return {
                template: '<select class="form-control" ng-model="colFilter.term" ng-options="option.id as option.value for option in colFilter.options"></select>'
            };
        })
        .filter('textDate', ['$filter', function ($filter) {
            return function (input, format) {
                var date = new Date(input);
                return $filter('date')(date, format);
            };
        }])

        .controller('myCustomModalCtrl', function( $scope, $compile, $timeout ) {
            var $elm;

            $scope.showAgeModal = function() {
                $scope.listOfAges = [];

                $scope.col.grid.appScope.gridOptions.data.forEach( function ( row ) {
                    if ( $scope.listOfAges.indexOf( row.age ) === -1 ) {
                        $scope.listOfAges.push( row.age );
                    }
                });
                $scope.listOfAges.sort();

                $scope.gridOptions = {
                    data: [],
                    enableColumnMenus: false,
                    onRegisterApi: function( gridApi) {
                        $scope.gridApi = gridApi;

                        if ( $scope.colFilter && $scope.colFilter.listTerm ){
                            $timeout(function() {
                                $scope.colFilter.listTerm.forEach( function( age ) {
                                    var entities = $scope.gridOptions.data.filter( function( row ) {
                                        return row.age === age;
                                    });

                                    if( entities.length > 0 ) {
                                        $scope.gridApi.selection.selectRow(entities[0]);
                                    }
                                });
                            });
                        }
                    }
                };

                $scope.listOfAges.forEach(function( age ) {
                    $scope.gridOptions.data.push({age: age});
                });

                var html = '<div class="modal" ng-style="{display: \'block\'}"><div class="modal-dialog"><div class="modal-content"><div class="modal-header">Filter Ages</div><div class="modal-body"><div id="grid1" ui-grid="gridOptions" ui-grid-selection class="modalGrid"></div></div><div class="modal-footer"><button id="buttonClose" class="btn btn-primary" ng-click="close()">Filter</button></div></div></div></div>';
                $elm = angular.element(html);
                angular.element(document.body).prepend($elm);

                $compile($elm)($scope);

            };

            $scope.close = function() {
                var ages = $scope.gridApi.selection.getSelectedRows();
                $scope.colFilter.listTerm = [];

                ages.forEach( function( age ) {
                    $scope.colFilter.listTerm.push( age.age );
                });

                $scope.colFilter.term = $scope.colFilter.listTerm.join(', ');
                $scope.colFilter.condition = new RegExp($scope.colFilter.listTerm.join('|'));

                if ($elm) {
                    $elm.remove();
                }
            };
        })
        .directive('myCustomModal', function() {
            return {
                template: '<label>@{{colFilter.term}}</label><button ng-click="showAgeModal()">...</button>',
                controller: 'myCustomModalCtrl'
            };
        })
        .directive('uiSelectWrap', uiSelectWrap);


        uiSelectWrap.$inject = ['$document', 'uiGridEditConstants'];
        function uiSelectWrap($document, uiGridEditConstants) {
            return function link($scope, $elm, $attr) {
                $document.on('click', docClick);

                function docClick(evt) {
                    if ($(evt.target).closest('.ui-select-container').size() === 0) {
                        $scope.$emit(uiGridEditConstants.events.END_CELL_EDIT);
                        $document.off('click', docClick);

                    }
                }
            };
        }

    </script>

@endsection