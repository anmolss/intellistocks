@extends('admin.layout.app')

@section('page_title', 'Services')
@section('page_subtitle', 'List of servies')

@section('content')

    <form class="form-horizontal" action="#" id="customer-frm-filter" method="GET">
        <div class="panel panel-flat panel-collapsed">
            <div class="panel-heading">
                <h6 class="panel-title text-semibold"><i class="icon-equalizer position-left"></i> Filters</h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse" class=""></a></li>
                    </ul>
                </div>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        {!! Form::label('status', 'Status:', ['class' => 'control-label col-lg-4']) !!}
                                        <div class="col-lg-8">
                                            {!! Form::select('status', ['' => 'Select Status', 1=>'Active', 0=>'Inactive'],  Request::input('status', ''), ['class' => 'form-control select ' ] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        {!! Form::label('watchlist', 'In Watchlist:', ['class' => 'control-label col-lg-4']) !!}
                                        <div class="col-lg-8">
                                            {!! Form::select('watchlist', [ '' => 'Select Status',1=>'Yes', 0=>'No'],  Request::input('watchlist', ''), ['class' => 'form-control select '  ] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-block"><span class="hidden-sm">Filter </span><i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                    <div class="col-sm-6">
                                        <a title="Reset Filter!" id="reset_search" href="javascript:" class="btn btn-success btn-rounded btn-block"><span class="hidden-sm">Reset </span><i class="icon-spinner11 position-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="over-title margin-bottom-15">
                Services
                @permission('create-services')
                <a href="{{route('admin::services.create')}}" class="btn btn-primary pull-right">Add New <i class="fa fa-plus"></i> </a>
                @endpermission
            </h5>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-hover" id="sample-table-2">
                <thead>
                    <tr>
                        <th>Name of Service</th>
                        <th>Customers (Active)</th>
                        <th>Today's P&L</th>
                        <th>Total P&L</th>
                        <th>No. of Plans</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($list as  $key => $item)
                        <tr>
                            <td>
                                <a href="{{route('admin::services.show', ['services' => $item->id])}}" tooltip-placement="top" tooltip="Edit"  class="text-primary-600">{{ $item->name }}</a>
                            </td>
                            <td>{{ $item->active_users }}</td>
                            <td>
                                @if(isset($reports[$item->id]))
                                    {{ $reports[$item->id]['today_pnl'] }}
                                @else
                                    -NA-
                                @endif
                            </td>
                            <td>
                                @if(isset($reports[$item->id]))
                                    {{ $reports[$item->id]['total_profit'] }}
                                @else
                                    -NA-
                                @endif
                            </td>
                            <td>{{ $item->total_plans }}</td>

                            <td><span class="label label-{{ ($item->status)? 'success' : 'danger' }}">{{ ($item->status)? 'Active' : 'Inactive' }}</span></td>
                            <td class="center">
                                <ul class="icons-list">
                                    <li class="dropdown dropdown-menu-left">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-cog7"></i><span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            @if($item->is_portfolio)
                                                <li><a href="{{ route('admin::portfolios.create', ['id' =>$item->id]) }}"><i class="icon-pencil7 pull-right"></i> Portfolio</a></li>
                                            @endif

                                            <li><a href="{{ route('admin::plans.index', ['id' =>$item->id]) }}"><i class="icon-pencil7 pull-right"></i>Plans</a></li>
                                            <li><a href="{{route('admin::services.edit', ['services' => $item->id])}}"><i class="icon-pencil7 pull-right"></i> Edit</a></li>

                                            @if(!isset($reports[$item->id]) || count($reports[$item->id]['stocks']) == 0)
                                                <li class="divider"></li>
                                                <li class="text-danger-700">
                                                    {!! BootForm::open()->delete()->action(route('admin::services.destroy', ['serviced' => $item->id]))!!}
                                                    <a class="delete_anchor" style="color: inherit; overflow: hidden; display: block; padding: 2px 16px;"><i class="icon-trash  pull-right"></i> Delete</a>
                                                    {!! BootForm::close() !!}
                                                </li>
                                            @endif
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>

                    @empty
                        <tr>
                            <td colspan="6" align="center">No Services Found</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>


        </div>

        <div class="panel-footer">{{ $list->appends(Request::except('page'))->links() }}</div>
    </div>

@endsection


@section('scripts')
    @parent
    <script type="text/javascript">
        $('a.delete_anchor').on('click', function(){
            if(confirm('Are you sure you want to detele this?')){
                var form = $(this).closest("form");
                form.submit();
            } else {
                return false;
            }
        });

        // Enable Select2 select for the length option
        $('select').select2({
            minimumResultsForSearch: Infinity
        });
    </script>
@endsection