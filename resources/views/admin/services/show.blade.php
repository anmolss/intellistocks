@extends('admin.layout.app')

@section('page_title', 'All Services')
@section('page_subtitle', $item->name)

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="tabbable">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="activity">
                        <div class="timeline timeline-left content-group">
                            <div class="timeline-container">

                                <div class="timeline-row">
                                    <div class="timeline-icon">
                                        <div class="bg-info-400">
                                            <i class="icon-info3"></i>
                                        </div>
                                    </div>

                                    <div class="panel panel-flat timeline-content">
                                        <div class="panel-heading">
                                            <h6 class="text-semibold no-margin">Basic Info</h6>
                                        </div>
                                        <table class="table table-bordered table-sm">
                                            <tbody>
                                                <tr class="border-top border-top-grey-300">
                                                    <th class="text-right">Service Name</th>
                                                    <td>{{$item->name}}</td>
                                                    <th class="text-right">Symbol</th>
                                                    <td>{{$item->code}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="text-right">Start Date</th>
                                                    <td>{{Carbon\Carbon::parse($item->created_at)->toDateString()}}</td>
                                                    <th class="text-right">Plans</th>
                                                    <td class="text-semibold"><a href="{{ route('admin::plans.index', ['id' =>$item->id]) }}">{{$item->plans->count()}} <i class="fa fa-arrow-right text-right"></i></a></td>
                                                </tr>

                                                <tr>
                                                    <th class="text-right">Total Customers</th>
                                                    <td>{{$users->count()}}</td>
                                                    <th class="text-right">Active Customers</th>
                                                    <td>{{$users->where('service_status', 'Active')->count()}}</td>
                                                </tr>

                                                <tr>
                                                    <th class="text-right">Status</th>
                                                    <td>{{($item->status) ? "Active" : 'Inactive'}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="timeline-row">
                                    <div class="timeline-icon">
                                        <div class="bg-success-400">
                                            <i class=" icon-stats-growth"></i>
                                        </div>
                                    </div>

                                    <div class="panel panel-flat timeline-content">
                                        <div class="panel-heading">
                                            <h6 class="text-semibold no-margin">Statistics</h6>
                                        </div>
                                        <table class="table table-bordered table-sm">
                                            <tbody>
                                            @if(!empty($report))
                                            <tr class="border-top border-top-grey-300">
                                                <th class="text-right">Total Calls</th>
                                                <td>{{$report['total_call']}}</td>
                                                <th class="text-right">Positive Calls</th>
                                                <td>{{$report['positive_call']}}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-right">Open Calls</th>
                                                <td>{{$report['open_call']}}</td>
                                                <th class="text-right">Close Calls</th>
                                                <td>{{$report['close_call']}}</td>
                                            </tr>

                                            <tr>
                                                <th class="text-right">Total P&L</th>
                                                <td>{{$report['total_profit']}}</td>
                                                <th class="text-right">Today's P&L</th>
                                                <td>{{$report['today_pnl']}}</td>
                                            </tr>


                                            <tr>
                                                <th class="text-right">Open P&L</th>
                                                <td>{{$report['open_profit']}}</td>
                                                <th class="text-right">Realized P&L</th>
                                                <td>{{$report['booked_profit']}}</td>
                                            </tr>

                                            <tr>
                                                <th class="text-right">Winning %</th>
                                                <td>{{$report['winning_percent']}}</td>
                                                <th class="text-right">ROI (Annually)</th>
                                                <td>{{$report['net_roi']}}</td>
                                            </tr>

                                            <tr>
                                                <th class="text-right">Gross Contribution of winning calls</th>
                                                <td>{{$report['gross_profit']}}%</td>
                                                <th class="text-right">Gross Contribution of Losing calls</th>
                                                <td>{{$report['gross_loss']}}</td>
                                            </tr>

                                            <tr>
                                                <th class="text-right">Open Calls</th>
                                                <td>{{$report['open_call']}}</td>
                                                <th class="text-right">Closed Calls</th>
                                                <td>{{$report['close_call']}}</td>
                                            </tr>
                                            @else
                                            <tr>
                                                <td colspan="4">No Stats available for this service</td>
                                            </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    @if(!empty($report))

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="panel  panel-flat border-left-lg border-left-success timeline-content">
                                                <div class="panel-heading">
                                                    <h6 class="text-semibold no-margin-top">Top 5 Profit Stocks</h6>
                                                </div>
                                                <table class="table table-xxs">
                                                    <thead>
                                                    <tr>
                                                        <th class="p-10 pr-5">Symbol</th>
                                                        <th class="p-10 pr-5">Status</th>
                                                        <th class="p-10 pr-5">CMP</th>
                                                        <th class="p-10 pr-5">Change</th>
                                                        <th class="p-10 pr-5">Total P&L</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse(collect($report['stocks'])->filter(function ($value, $key) {return $value['total_profit'] > 0;})->sortByDesc('total_profit')->take(5) as $stock)
                                                        <tr>
                                                            <td>{{$stock['symbol']}}</td>
                                                            <td>{{$stock['status']}}</td>
                                                            <td>{{$stock['cmp']}}</td>
                                                            <td>
                                                                @if($stock['cmp'] - $stock['prev_close'] >= 0)
                                                                    <span class="text-success-600">{{ round($stock['cmp'] - $stock['prev_close'] , 2) }}</span>
                                                                @else
                                                                    <span class="text-danger">{{ round($stock['cmp'] - $stock['prev_close'] , 2) }}</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($stock['total_profit'] >= 0)
                                                                    <span class="text-success-600">{{ $stock['total_profit'] }}</span>
                                                                @else
                                                                    <span class="text-danger">{{ $stock['total_profit'] }}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @empty
                                                        <tr>
                                                            <td colspan="5">No Stocks</td>
                                                        </tr>
                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="panel panel-flat border-left-lg border-left-danger timeline-content">
                                                <div class="panel-heading">
                                                    <h6 class="text-semibold no-margin-top">Top 5 Losing Stocks</h6>
                                                </div>
                                                <table class="table table-xxs">
                                                    <thead>
                                                    <tr>
                                                        <th class="p-10 pr-5">Symbol</th>
                                                        <th class="p-10 pr-5">Status</th>
                                                        <th class="p-10 pr-5">CMP</th>
                                                        <th class="p-10 pr-5">Change</th>
                                                        <th class="p-10 pr-5">Total P&L</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse(collect($report['stocks'])->filter(function ($value, $key) {return $value['total_profit'] < 0;})->sortBy('total_profit')->take(5) as $stock)
                                                        <tr>
                                                            <td>{{$stock['symbol']}}</td>
                                                            <td>{{$stock['status']}}</td>
                                                            <td>{{$stock['cmp']}}</td>
                                                            <td>
                                                                @if($stock['cmp'] - $stock['prev_close'] >= 0)
                                                                    <span class="text-success-600">{{ round($stock['cmp'] - $stock['prev_close'] , 2) }}</span>
                                                                @else
                                                                    <span class="text-danger">{{ round($stock['cmp'] - $stock['prev_close'] , 2) }}</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($stock['total_profit'] >= 0)
                                                                    <span class="text-success-600">{{ $stock['total_profit'] }}</span>
                                                                @else
                                                                    <span class="text-danger">{{ $stock['total_profit'] }}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @empty
                                                        <tr>
                                                            <td colspan="5">No Stocks</td>
                                                        </tr>
                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    @endif
                                </div>

                                <div class="timeline-row">
                                    <div class="timeline-icon">
                                        <div class="bg-danger-400">
                                            <i class="icon-user-check"></i>
                                        </div>
                                    </div>

                                    <div class="panel panel-flat timeline-content">
                                        <div class="panel-heading">
                                            <h6 class="text-semibold no-margin">Active Users</h6>
                                        </div>
                                        <table class="table table-sm">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse($users->where('service_status', 'Active') as $user)
                                                <tr>
                                                    <td>{{Html::link('/customers/'.$user->user->id, $user->user->name, ['target'=> '_blank'])}}</td>
                                                    <td>{{ $user->user->email}}</td>
                                                    <td><span class="label label-{{ ($user->service_status == 'Active')? 'success' : 'danger' }}">{{ $user->service_status }}</span></td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="3">No Active Users</td>
                                                </tr>
                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection