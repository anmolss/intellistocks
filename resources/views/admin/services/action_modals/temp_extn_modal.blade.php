<form action="{{$href}}" class="action_modal_form">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h5 class="modal-title">Temporary Extention</h5>
</div>
<div class="modal-body">
	<div class="row">
		<div class="well border-bottom-lg border-top-lg border-bottom-warning border-top-warning text-center">
			<h6 class="no-margin text-semibold">Details</h6>
			<br>
	        <ul class="list-inline list-inline-separate no-margin-bottom">
				<li><strong>Activation Date</strong> - <u>{{$user_service->activation_date}}</u></li>
				<li><strong>Expiry Date</strong> - <u>{{(!empty($user_service->expire_date))?$user_service->expire_date:'N/A'}}</u></li>
				<li><strong>Status</strong> - <u>{{$user_service->service_status}}</u></li>
				<li><strong>Extension Count</strong> - <u>{{$user_service->temp_extn_count}}</u></li>
			</ul>
		</div>
	</div>
	<br>
	<input type="hidden" name="form_type" value="real">
	<input type="hidden" name="id" value="{{$id}}">
	<div class="row">
		<div class="form-group">
			<label class="control-label col-sm-4">Days Of Extension Period</label>
			<div class="col-sm-3">
				<select class="form-control" name="days_select">
					<option value="5">5 Days</option>
					<option value="10">10 Days</option>
					<option value="15">15 Days</option>
				</select>
			</div>
			<div class="col-sm-1">
				<p style="margin-top:10px;"><strong>OR</strong></p>	
			</div>
			<div class="col-sm-3">
				<input type="text" name="days_text" placeholder="Enter Days" class="form-control">
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<div class="col-sm-1 text-center form_spinner" style="display:none;">
        <i class="icon-spinner2 spinner"></i>
    </div>
    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Submit form</button>
</div>
</form>