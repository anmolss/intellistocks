<form action="{{$href}}" class="action_modal_form">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h5 class="modal-title">Terminate Service</h5>
</div>
<div class="modal-body">
	<input type="hidden" name="form_type" value="real">
	<input type="hidden" name="id" value="{{$id}}">
	<div class="row">
		<div class="form-group">
			<label class="control-label col-sm-3">Reason</label>
			<div class="col-sm-9">
				<input type="text" name="reason" placeholder="Reason Of Termination" class="form-control">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<label class="control-label col-sm-3">Comments</label>
			<div class="col-sm-9">
				<textarea rows="5" cols="5" name="comments" class="form-control" placeholder="Comments"></textarea>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<div class="col-sm-offset-3">
				<label class="checkbox-inline checkbox-right">
					<input type="checkbox" class="styled" name="sms_notification" checked="checked" value="yes">
					SMS
				</label>

				<label class="checkbox-inline checkbox-right">
					<input type="checkbox" class="styled" name="email_notification" value="yes">
					Email
				</label>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<div class="col-sm-1 text-center form_spinner" style="display:none;">
        <i class="icon-spinner2 spinner"></i>
    </div>
    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Submit form</button>
</div>
</form>