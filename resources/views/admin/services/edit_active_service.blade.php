@extends('admin.layout.app')

@section('page_title', 'Edit Service')
@section('page_subtitle', 'edit transaction data of service')

@section('content')
<?php
	$is_edit = true;
?>
<style type="text/css">
.help-block{
    color: #c10000;
    font-weight: normal !important;
}
</style>
<div class="row edit_row">
  <!-- Advanced legend -->
  <div class="form-horizontal">
    <div class="panel panel-flat">
      <div class="panel-heading">
        <h5 class="panel-title">Edit Service Form</h5>
      </div>

      <div class="panel-body">
        {!! Form::open(['route' => 'admin::clients.store_client_user', 'class' => 'ui-form client_create_form']) !!}
          <input type="hidden" name="customer_id" value="{{$customer->id}}">
          <input type="hidden" name="payment_mode" value="0">
          <input type="hidden" name="user_service_edit" value="yes">
          
          <fieldset>
            <legend class="text-semibold">
              <i class="icon-user-tie position-left"></i>
              Customer Info
              <a class="control-arrow" data-toggle="collapse" data-target="#demo1">
                <i class="icon-circle-down2"></i>
              </a>
            </legend>

            <div class="collapse in" id="demo1">
              <div class="col-sm-12 form-group"> 
	              <h1 class="text-center">Customer : {{$customer->username}}</h1>
              </div>
            </div>
          </fieldset>

          <fieldset>
            <legend class="text-semibold">
              <i class="icon-magazine position-left"></i>
              Services & Plans
              <a class="control-arrow" data-toggle="collapse" data-target="#demo2">
                <i class="icon-circle-down2"></i>
              </a>
            </legend>

            <div class="collapse in" id="demo2">
              <div class="form-group" id="service_append">
                <div class="col-sm-12">
                <input type="hidden" name="services[0][user_service_id]" value="{{$service['id']}}">
                <input type="hidden" name="services[0][transaction_history_id]" value="{{$service['latest_transaction_history_id']}}">
                  <div class="col-sm-12 well service_row" data-count='0' data-service_fee="{{$service['plan']['service_fee']}}" data-tax="{{$service['latest_transactions'][0]['applied_tax']}}">
                    <div class="row">
                      <div class="col-sm-6">
                        {!! Form::label('services[0][fk_service_id]', 'Service Type:', ['class' => 'control-label']) !!}
                        {!! Form::select('services[0][fk_service_id]', [null=>'Select Service'] + $services->toArray(), $service['service_id'], ['class' => 'form-control select2-search-modal service_type', 'disabled'] )!!}
                        <input type="hidden" name="services[0][fk_service_id]" value="{{$service['service_id']}}">
                        <span class="help-block error_services_fk_service_id" style="display:none;"></span>
                      </div>
                      
                      <div class="col-sm-6">
                        {!! Form::label('fk_plan_id', 'Plan:', ['class' => 'control-label']) !!}
                        {!! Form::select('services[0][fk_plan_id]', [null=>'Select Plan'] + $plans->toArray(), $service['plan_id'], ['class' => 'form-control select2-search-modal plans', 'disabled'] )!!}
                        <input type="hidden" name="services[0][fk_plan_id]" value="{{$service['plan_id']}}">
                        <span class="help-block error_services_fk_plan_id" style="display:none;"></span>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group" id="capital_container" style="display:none;">
                          <label class="control-label">Enter Capital:</label>
                          <input type="text" name="services[0][capital]" class="form-control reflect_capital_amnt" value="{{$service['service_capital']}}">
                          <span class="help-block error_services_capital" style="display:none;"></span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group" id="discount-container" style="display:none">
                          <label for="" class="control-label">Discount: </label>
                          <input class="form-control reflect_amt_key_up edit_discount txt-discount" placeholder="Discount (%)" name="services[0][discount]" type="number" min="0" max="0" value="{{$service['latest_transactions'][0]['discount']}}" {{($is_transaction_cleared == 'yes')?'disabled':''}}>
                          @if($is_transaction_cleared == 'yes')
                            <input type="hidden" name="services[0][discount]" value="{{$service['latest_transactions'][0]['discount']}}">
                          @endif
                          <span class="help-block error_discount" style="display:none;"></span>
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-sm-4">
                        {!! Form::label('payment_type[]', 'Payment Type:', ['class' => 'control-label']) !!}
                        @if($is_transaction_cleared == 'yes')
                          {!! Form::select('services[0][payment_type]', ['' => 'Select Payment Type', 'full' => 'Full', 'partial' => 'Partial'], $service['latest_transactions'][0]['pay_type'], ['class' => 'form-control select2-search-modal payment_type', 'disabled'] )!!}
                          <input type="hidden" name="services[0][payment_type]" value="{{$service['latest_transactions'][0]['pay_type']}}">
                        @elseif($is_transaction_cleared == 'no')
                          {!! Form::select('services[0][payment_type]', ['' => 'Select Payment Type', 'full' => 'Full', 'partial' => 'Partial'], $service['latest_transactions'][0]['pay_type'], ['class' => 'form-control select2-search-modal payment_type'] )!!}
                        @endif
                        <span class="help-block error_services_payment_type" style="display:none;"></span>
                      </div>

                      <div class="col-sm-4">
                        @if($service['latest_transactions'][0]['pay_type'] == 'partial')
                          {!! Form::label('fk_payment_type', 'Payment:', ['class' => 'control-label']) !!}
                          {!! Form::select('services[0][fk_payment_type]', ['' => 'Select Payment Type', 'cash' => 'Cash', 'cheque' => 'Cheque', 'net_banking' => 'Net Banking'], null, ['class' => 'form-control select2-search-modal fk_payment_type', 'disabled'] )!!}
                        @else
                          @if($is_transaction_cleared == 'yes')
                              {!! Form::label('fk_payment_type', 'Payment:', ['class' => 'control-label']) !!}
                              {!! Form::select('services[0][fk_payment_type]', ['' => 'Select Payment Type', 'cash' => 'Cash', 'cheque' => 'Cheque', 'net_banking' => 'Net Banking'], $service['latest_transactions'][0]['payment_type'], ['class' => 'form-control select2-search-modal fk_payment_type', 'disabled'] )!!}
                              <input type="hidden" name="services[0][fk_payment_type]" value="{{$service['latest_transactions'][0]['payment_type']}}">
                            @elseif($is_transaction_cleared == 'no')
                              {!! Form::label('fk_payment_type', 'Payment:', ['class' => 'control-label']) !!}
                              {!! Form::select('services[0][fk_payment_type]', ['' => 'Select Payment Type', 'cash' => 'Cash', 'cheque' => 'Cheque', 'net_banking' => 'Net Banking'], $service['latest_transactions'][0]['payment_type'], ['class' => 'form-control select2-search-modal fk_payment_type'] )!!}
                            @endif                
                        @endif
                        <span class="help-block error_services_fk_payment_type" style="display:none;"></span>
                      </div>

                      <div class="col-sm-4">
                        <label class="control-label">Activation Date:</label>
                        <input type="text" name="services[0][activation_date]" class="form-control date-picker service-activation-date" data-date-format="dd-mm-yyyy" value="{{(new Carbon($service['activation_date']))->format('d-m-Y')}}" disabled>
                        <input type="hidden" name="services[0][activation_date]" value="{{(new Carbon($service['activation_date']))->format('d-m-Y')}}">
                        <span class="help-block error_services_activation_date" style="display:none;"></span>
                      </div>
                    </div>
                    <br>
                    <div class="payment_append form-group">
                      <div class="col-sm-12">
                        <div class="row payment_type_append">
                          @if($service['latest_transactions'][0]['pay_type'] == 'partial')
                            <div style="display: block;">
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <label class="form-label">Select Partial Pay Count</label>

                                  <select class="form-control select_partial_pay">
                                    <option value="" 
                                    {{(@$service['latest_transactions'][0]['cleared'] == 'Yes' || 
                                       @$service['latest_transactions'][1]['cleared'] == 'Yes' || 
                                       @$service['latest_transactions'][2]['cleared'] == 'Yes') ? 'disabled' : '' }}
                                    >Select Count</option>
                                    <option value="1" {{(count($service['latest_transactions']) == 1)?'selected':''}}
                                    {{(@$service['latest_transactions'][1]['cleared'] == 'Yes' || 
                                       @$service['latest_transactions'][2]['cleared'] == 'Yes') ? 'disabled' : '' }}
                                    >ONE</option>
                                    <option value="2" {{(count($service['latest_transactions']) == 2)?'selected':''}} 
                                    {{(@$service['latest_transactions'][2]['cleared'] == 'Yes')?'disabled':''}}
                                    >TWO</option>
                                    <option value="3" {{(count($service['latest_transactions']) == 3)?'selected':''}}>THREE</option>
                                  </select>
                                </div>
                              </div>
                              <div class="col-sm-12">
                                @if(count($service['latest_transactions']) >= 1)
                                  <div class="row 1st_partial_pay" style="">
                                    <h4>#1st Pay</h4>
                                    <input type="hidden" name="services[0][transaction][0][id]" value="{{ (isset($service['latest_transactions'][0]['id']))?$service['latest_transactions'][0]['id']:'0' }}">
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        <label class="control-label">Payment:</label>
                                        <select class="form-control select2-search-modal fk_partial_payment_type0" name="services[0][transaction][0][fk_payment_type]" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                          <option value="">Payment Type</option>
                                          <option value="cash" {{($service['latest_transactions'][0]['payment_type'] == 'cash')?'selected':''}}>Cash</option>
                                          <option value="cheque" {{($service['latest_transactions'][0]['payment_type'] == 'cheque')?'selected':''}}>Cheque</option>
                                          <option value="net_banking" {{($service['latest_transactions'][0]['payment_type'] == 'net_banking')?'selected':''}}>Net Banking </option>
                                        </select>
                                        @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                          <input type="hidden" name="services[0][transaction][0][fk_payment_type]" value="{{$service['latest_transactions'][0]['payment_type']}}">
                                        @endif
                                        <span class="help-block error_fk_payment_type" style="display:none;"></span>
                                      </div>
                                    </div>

                                    <div class="col-md-10">
                                      <div class="row">
                                        <div class="col-md-5">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="amount[]" class="control-label">Amount:</label>
                                              <input class="form-control reflect_amt_key_up" placeholder="Amount" name="services[0][transaction][0][amount]" type="text" value="{{$service['latest_transactions'][0]['amount']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                                <input type="hidden" name="services[0][transaction][0][amount]" value="{{$service['latest_transactions'][0]['amount']}}">
                                              @endif
                                              <span class="help-block error_amount0" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="date[]" class="control-label">Date:</label>
                                              <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="services[0][transaction][0][date]" type="text" value="{{(new Carbon($service['latest_transactions'][0]['date']))->format('d-m-Y')}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                                <input type="hidden" name="services[0][transaction][0][date]" value="{{$service['latest_transactions'][0]['date']}}">
                                            @endif
                                              <span class="help-block error_date0" style="display:none;"></span>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="col-md-7 extra-filed-container0">
                                          @if($service['latest_transactions'][0]['payment_type'] == 'cash')
                                            <div style="display: block;">
                                        <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="transaction_id[]" class="control-label">Transaction Id:</label>
                                            <input class="form-control" placeholder="Transaction Id" name="services[0][transaction][0][transaction_id]" type="text" value="{{$service['latest_transactions'][0]['transaction_id']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                            @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][0][transaction_id]" value="{{$service['latest_transactions'][0]['transaction_id']}}">
                                            @endif
                                            <span class="help-block error_transaction_id" style="display:none;"></span>
                                          </div>
                                        </div>

                                        <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="received_from[]" class="control-label">Name:</label>
                                            <input class="form-control" placeholder="Name" name="services[0][transaction][0][received_from]" type="text" value="{{$service['latest_transactions'][0]['received_from']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                            @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][0][received_from]" value="{{$service['latest_transactions'][0]['received_from']}}">
                                          @endif
                                            <span class="help-block error_received_from" style="display:none;"></span>
                                          </div>
                                        </div>
                                      </div>
                                          @elseif($service['latest_transactions'][0]['payment_type'] == 'cheque')
                                            <div style="display: block;">
                                        <div class="col-md-5">
                                          <div class="form-group">
                                            <label for="cheque_number" class="control-label">Cheque Number:</label>
                                            <input class="form-control" placeholder="Cheque Number" name="services[0][transaction][0][cheque_number]" type="text" value="{{$service['latest_transactions'][0]['cheque_number']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                            @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][cheque_number]" value="{{$service['latest_transactions'][0]['cheque_number']}}">
                                          @endif
                                            <span class="help-block error_transaction_id" style="display:none;"></span>
                                          </div>
                                        </div>

                                        <div class="col-md-5">
                                          <div class="form-group">
                                            <label for="bank" class="control-label">Bank:</label>
                                            <input class="form-control" placeholder="Bank" name="services[0][transaction][0][bank]" type="text" value="{{$service['latest_transactions'][0]['bank']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                            @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][bank]" value="{{$service['latest_transactions'][0]['bank']}}">
                                          @endif
                                            <span class="help-block error_received_from" style="display:none;"></span>
                                          </div>
                                        </div>

                                        <div class="col-md-2">
                                          <label class="control-label">Cleared:</label>
                                          <select class="form-control" name="services[0][transaction][0][cleared]" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                            <option value="No" {{($service['latest_transactions'][0]['cleared'] == 'No')?'selected':''}}>No</option>
                                            <option value="Yes" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'selected':''}}>Yes</option>
                                          </select>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][cleared]" value="{{$service['latest_transactions'][0]['cleared']}}">
                                        @endif
                                        </div>
                                      </div>
                                          @elseif($service['latest_transactions'][0]['payment_type'] == 'net_banking')
                                            <div style="display: block;">
                                        <div class="col-md-3">
                                          <label for="type[]" class="control-label">Type:</label>
                                          <select class="form-control select2-search-modal" name="services[0][transaction][0][type]" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                            <option value="NEFT" {{($service['latest_transactions'][0]['type'] == 'NEFT')?'selected':''}}>NEFT</option>
                                            <option value="RTGS" {{($service['latest_transactions'][0]['type'] == 'RTGS')?'selected':''}}>RTGS</option></select>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][type]" value="{{$service['latest_transactions'][0]['type']}}">
                                        @endif
                                          <span class="help-block error_type" style="display:none;"></span>
                                        </div>

                                        <div class="col-md-4">
                                          <div class="form-group">
                                            <label for="transaction_id[]" class="control-label">Transaction Id:</label>
                                            <input class="form-control" placeholder="Transaction Id" name="services[0][transaction][0][transaction_id]" type="text" value="{{$service['latest_transactions'][0]['transaction_id']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                            @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][transaction_id]" value="{{$service['latest_transactions'][0]['transaction_id']}}">
                                          @endif
                                            <span class="help-block error_transaction_id" style="display:none;"></span>
                                          </div>
                                        </div>

                                        <div class="col-md-5">
                                          <div class="form-group">
                                            <label for="received_from[]" class="control-label">Name:</label>
                                            <input class="form-control" placeholder="Name" name="services[0][transaction][0][received_from]" type="text" value="{{$service['latest_transactions'][0]['received_from']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                            @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][received_from]" value="{{$service['latest_transactions'][0]['received_from']}}">
                                          @endif
                                            <span class="help-block error_received_from" style="display:none;"></span>
                                          </div>
                                        </div>
                                      </div>
                                          @endif
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                @else
                                  <div class="row 1st_partial_pay" style="display:none;">
                                    <h4>#1st Pay</h4>
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        <label class="control-label">Payment:</label>
                                        <select class="form-control select2-search-modal fk_partial_payment_type0" name="services[0][transaction][0][fk_payment_type]">
                                          <option value="">Payment Type</option>
                                          <option value="cash">Cash</option>
                                          <option value="cheque">Cheque</option>
                                          <option value="net_banking">Net Banking </option>
                                        </select>
                                        <span class="help-block error_fk_payment_type" style="display:none;"></span>
                                      </div>
                                    </div>

                                    <div class="col-md-10">
                                      <div class="row">
                                        <div class="col-md-5">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="amount[]" class="control-label">Amount:</label>
                                              <input class="form-control reflect_amt_key_up" placeholder="Amount" name="services[0][transaction][0][amount]" type="text" value="0">
                                              <span class="help-block error_amount0" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="date[]" class="control-label">Date:</label>
                                              <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="services[0][transaction][0][date]" type="text" value="0">
                                              <span class="help-block error_date0" style="display:none;"></span>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="col-md-7 extra-filed-container0">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                @endif
                                @if(count($service['latest_transactions']) >= 2)
                                  <div class="row 2nd_partial_pay" style="">
                                    <h4>#2nd Pay</h4>
                                    <input type="hidden" name="services[0][transaction][1][id]" value="{{$service['latest_transactions'][1]['id']}}">
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        <label class="control-label">Payment:</label>
                                        <select class="form-control select2-search-modal fk_partial_payment_type1" name="services[0][transaction][1][fk_payment_type]" {{($service['latest_transactions'][1]['cleared'] == 'Yes')?'disabled':''}}>
                                          <option value="">Payment Type</option>
                                          <option value="cash" {{($service['latest_transactions'][1]['payment_type'] == 'cash')?'selected':''}}>Cash</option>
                                          <option value="cheque" {{($service['latest_transactions'][1]['payment_type'] == 'cheque')?'selected':''}}>Cheque</option>
                                          <option value="net_banking" {{($service['latest_transactions'][1]['payment_type'] == 'net_banking')?'selected':''}}>Net Banking </option>
                                        </select>
                                          @if($service['latest_transactions'][1]['cleared'] == 'Yes')
                                        <input type="hidden" name="services[0][transaction][1][fk_payment_type]" value="{{$service['latest_transactions'][1]['payment_type']}}">
                                    @endif
                                        <span class="help-block error_fk_payment_type" style="display:none;"></span>
                                      </div>
                                    </div>

                                    <div class="col-md-10">
                                      <div class="row">
                                        <div class="col-md-5">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="amount[]" class="control-label">Amount:</label>
                                              <input class="form-control reflect_amt_key_up" placeholder="Amount" name="services[0][transaction][1][amount]" type="text" value="{{$service['latest_transactions'][1]['amount']}}" {{($service['latest_transactions'][1]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][1]['cleared'] == 'Yes')
                                          <input type="hidden" name="services[0][transaction][1][amount]" value="{{$service['latest_transactions'][1]['amount']}}">
                                      @endif
                                              <span class="help-block error_amount1" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="date[]" class="control-label">Date:</label>
                                              <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="services[0][transaction][1][date]" type="text" value="{{(new Carbon($service['latest_transactions'][1]['date']))->format('d-m-Y')}}" {{($service['latest_transactions'][1]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][1]['cleared'] == 'Yes')
                                          <input type="hidden" name="services[0][transaction][1][date]" value="{{$service['latest_transactions'][1]['date']}}">
                                      @endif
                                              <span class="help-block error_date1" style="display:none;"></span>
                                            </div>
                                          </div>
                                        </div>

                                          <div class="col-md-7 extra-filed-container1">
                                            @if($service['latest_transactions'][1]['payment_type'] == 'cash')
                                              <div style="display: block;">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="transaction_id[]" class="control-label">Transaction Id:</label>
                                              <input class="form-control" placeholder="Transaction Id" name="services[0][transaction][1][transaction_id]" type="text" value="{{$service['latest_transactions'][1]['transaction_id']}}" {{($service['latest_transactions'][1]['cleared'] == 'Yes')?'disabled':''}}>
                                            @if($service['latest_transactions'][1]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][1][transaction_id]" value="{{$service['latest_transactions'][1]['transaction_id']}}">
                                          @endif
                                              <span class="help-block error_transaction_id" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="received_from[]" class="control-label">Name:</label>
                                              <input class="form-control" placeholder="Name" name="services[0][transaction][1][received_from]" type="text" value="{{$service['latest_transactions'][1]['received_from']}}" {{($service['latest_transactions'][1]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][1]['cleared'] == 'Yes')
                                                <input type="hidden" name="services[0][transaction][1][received_fromreceived_from]" value="{{$service['latest_transactions'][1]['received_from']}}">
                                            @endif
                                              <span class="help-block error_received_from" style="display:none;"></span>
                                            </div>
                                          </div>
                                        </div>
                                            @elseif($service['latest_transactions'][1]['payment_type'] == 'cheque')
                                              <div style="display: block;">
                                          <div class="col-md-5">
                                            <div class="form-group">
                                              <label for="cheque_number" class="control-label">Cheque Number:</label>
                                              <input class="form-control" placeholder="Cheque Number" name="services[0][transaction][1][cheque_number]" type="text" value="{{$service['latest_transactions'][1]['cheque_number']}}" {{($service['latest_transactions'][1]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][1]['cleared'] == 'Yes')
                                                <input type="hidden" name="services[0][transaction][1][cheque_number]" value="{{$service['latest_transactions'][1]['cheque_number']}}">
                                            @endif
                                              <span class="help-block error_transaction_id" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-5">
                                            <div class="form-group">
                                              <label for="bank" class="control-label">Bank:</label>
                                              <input class="form-control" placeholder="Bank" name="services[0][transaction][1][bank]" type="text" value="{{$service['latest_transactions'][1]['bank']}}" {{($service['latest_transactions'][1]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][1]['cleared'] == 'Yes')
                                                <input type="hidden" name="services[0][transaction][1][bank]" value="{{$service['latest_transactions'][1]['bank']}}">
                                            @endif
                                              <span class="help-block error_received_from" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-2">
                                            <label class="control-label">Cleared:</label>
                                            <select class="form-control" name="services[0][transaction][1][cleared]" {{($service['latest_transactions'][1]['cleared'] == 'Yes')?'disabled':''}}>
                                              <option value="No" {{($service['latest_transactions'][1]['cleared'] == 'No')?'selected':''}}>No</option>
                                              <option value="Yes" {{($service['latest_transactions'][1]['cleared'] == 'Yes')?'selected':''}}>Yes</option>
                                            </select>
                                            @if($service['latest_transactions'][1]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][1][cleared]" value="{{$service['latest_transactions'][1]['cleared']}}">
                                          @endif
                                          </div>
                                        </div>
                                            @elseif($service['latest_transactions'][1]['payment_type'] == 'net_banking')
                                              <div style="display: block;">
                                          <div class="col-md-3">
                                            <label for="type[]" class="control-label">Type:</label>
                                            <select class="form-control select2-search-modal" name="services[0][transaction][1][type]" {{($service['latest_transactions'][1]['cleared'] == 'Yes')?'disabled':''}}>
                                              <option value="NEFT" {{($service['latest_transactions'][1]['type'] == 'NEFT')?'selected':''}}>NEFT</option>
                                              <option value="RTGS" {{($service['latest_transactions'][1]['type'] == 'RTGS')?'selected':''}}>RTGS</option>
                                            </select>
                                            @if($service['latest_transactions'][1]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][1][type]" value="{{$service['latest_transactions'][1]['type']}}">
                                          @endif
                                            <span class="help-block error_type" style="display:none;"></span>
                                          </div>

                                          <div class="col-md-4">
                                            <div class="form-group">
                                              <label for="transaction_id[]" class="control-label">Transaction Id:</label>
                                              <input class="form-control" placeholder="Transaction Id" name="services[0][transaction][1][transaction_id]" type="text" value="{{$service['latest_transactions'][1]['transaction_id']}}" {{($service['latest_transactions'][1]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][1]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][1][transaction_id]" value="{{$service['latest_transactions'][1]['transaction_id']}}">
                                            @endif
                                              <span class="help-block error_transaction_id" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-5">
                                            <div class="form-group">
                                              <label for="received_from[]" class="control-label">Name:</label>
                                              <input class="form-control" placeholder="Name" name="services[0][transaction][1][received_from]" type="text" value="{{$service['latest_transactions'][1]['received_from']}}" {{($service['latest_transactions'][1]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][1]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][1][received_from]" value="{{$service['latest_transactions'][1]['received_from']}}">
                                            @endif
                                              <span class="help-block error_received_from" style="display:none;"></span>
                                            </div>
                                          </div>
                                        </div>
                                            @endif
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                @else
                                  <div class="row 2nd_partial_pay" style="display:none;">
                                    <h4>#2nd Pay</h4>
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        <label class="control-label">Payment:</label>
                                        <select class="form-control select2-search-modal fk_partial_payment_type1" name="services[0][transaction][1][fk_payment_type]">
                                          <option value="">Payment Type</option>
                                          <option value="cash">Cash</option>
                                          <option value="cheque">Cheque</option>
                                          <option value="net_banking">Net Banking </option>
                                        </select>
                                        <span class="help-block error_fk_payment_type" style="display:none;"></span>
                                      </div>
                                    </div>

                                    <div class="col-md-10">
                                      <div class="row">
                                        <div class="col-md-5">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="amount[]" class="control-label">Amount:</label>
                                              <input class="form-control reflect_amt_key_up" placeholder="Amount" name="services[0][transaction][1][amount]" type="text" value="0">
                                              <span class="help-block error_amount1" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="date[]" class="control-label">Date:</label>
                                              <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="services[0][transaction][1][date]" type="text" value="0">
                                              <span class="help-block error_date1" style="display:none;"></span>
                                            </div>
                                          </div>
                                        </div>

                                          <div class="col-md-7 extra-filed-container1"></div>
                                      </div>
                                    </div>
                                  </div>
                                @endif
                                @if(count($service['latest_transactions']) >= 3)
                                  <div class="row 3rd_partial_pay" style="">
                                    <h4>#3rd Pay</h4>
                                    <input type="hidden" name="services[0][transaction][2][id]" value="{{$service['latest_transactions'][2]['id']}}">
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        <label class="control-label">Payment:</label>
                                        <select class="form-control select2-search-modal fk_partial_payment_type2" name="services[0][transaction][2][fk_payment_type]" {{($service['latest_transactions'][2]['cleared'] == 'Yes')?'disabled':''}}>
                                          <option value="">Payment Type</option>
                                          <option value="cash" {{($service['latest_transactions'][2]['payment_type'] == 'cash')?'selected':''}}>Cash</option>
                                          <option value="cheque" {{($service['latest_transactions'][2]['payment_type'] == 'cheque')?'selected':''}}>Cheque</option>
                                          <option value="net_banking" {{($service['latest_transactions'][2]['payment_type'] == 'net_banking')?'selected':''}}>Net Banking </option>
                                        </select>
                                        @if($service['latest_transactions'][2]['cleared'] == 'Yes')
                                      <input type="hidden" name="services[0][transaction][2][fk_payment_type]" value="{{$service['latest_transactions'][2]['payment_type']}}">
                                    @endif
                                        <span class="help-block error_fk_payment_type" style="display:none;"></span>
                                      </div>
                                    </div>

                                    <div class="col-md-10">
                                      <div class="row">
                                        <div class="col-md-5">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="amount[]" class="control-label">Amount:</label>
                                              <input class="form-control reflect_amt_key_up" placeholder="Amount" name="services[0][transaction][2][amount]" type="text" value="{{$service['latest_transactions'][2]['amount']}}" {{($service['latest_transactions'][2]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][2]['cleared'] == 'Yes')
                                          <input type="hidden" name="services[0][transaction][2][amount]" value="{{$service['latest_transactions'][2]['amount']}}">
                                      @endif
                                              <span class="help-block error_amount2" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="date[]" class="control-label">Date:</label>
                                              <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="services[0][transaction][2][date]" type="text" value="{{(new Carbon($service['latest_transactions'][2]['date']))->format('d-m-Y')}}" {{($service['latest_transactions'][2]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][2]['cleared'] == 'Yes')
                                          <input type="hidden" name="services[0][transaction][2][date]" value="{{$service['latest_transactions'][2]['date']}}">
                                      @endif
                                              <span class="help-block error_date2" style="display:none;"></span>
                                            </div>
                                          </div>
                                        </div>

                                          <div class="col-md-7 extra-filed-container2">
                                            @if($service['latest_transactions'][2]['payment_type'] == 'cash')
                                              <div style="display: block;">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="transaction_id[]" class="control-label">Transaction Id:</label>
                                              <input class="form-control" placeholder="Transaction Id" name="services[0][transaction][2][transaction_id]" type="text" value="{{$service['latest_transactions'][2]['transaction_id']}}" {{($service['latest_transactions'][2]['cleared'] == 'Yes')?'disabled':''}}>
                                            @if($service['latest_transactions'][2]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][2][transaction_id]" value="{{$service['latest_transactions'][2]['transaction_id']}}">
                                          @endif
                                              <span class="help-block error_transaction_id" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="received_from[]" class="control-label">Name:</label>
                                              <input class="form-control" placeholder="Name" name="services[0][transaction][2][received_from]" type="text" value="{{$service['latest_transactions'][2]['received_from']}}" {{($service['latest_transactions'][2]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][2]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][2][received_from]" value="{{$service['latest_transactions'][2]['received_from']}}">
                                            @endif
                                              <span class="help-block error_received_from" style="display:none;"></span>
                                            </div>
                                          </div>
                                        </div>
                                            @elseif($service['latest_transactions'][2]['payment_type'] == 'cheque')
                                              <div style="display: block;">
                                          <div class="col-md-5">
                                            <div class="form-group">
                                              <label for="cheque_number" class="control-label">Cheque Number:</label>
                                              <input class="form-control" placeholder="Cheque Number" name="services[0][transaction][2][cheque_number]" type="text" value="{{$service['latest_transactions'][2]['cheque_number']}}" {{($service['latest_transactions'][2]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][2]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][2][cheque_number]" value="{{$service['latest_transactions'][2]['cheque_number']}}">
                                                @endif
                                              <span class="help-block error_transaction_id" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-5">
                                            <div class="form-group">
                                              <label for="bank" class="control-label">Bank:</label>
                                              <input class="form-control" placeholder="Bank" name="services[0][transaction][2][bank]" type="text" value="{{$service['latest_transactions'][2]['bank']}}" {{($service['latest_transactions'][2]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][2]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][2][bank]" value="{{$service['latest_transactions'][2]['bank']}}">
                                                @endif
                                              <span class="help-block error_received_from" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-2">
                                            <label class="control-label">Cleared:</label>
                                            <select class="form-control" name="services[0][transaction][2][cleared]" {{($service['latest_transactions'][2]['cleared'] == 'Yes')?'disabled':''}}>
                                              <option value="No" {{($service['latest_transactions'][2]['cleared'] == 'No')?'selected':''}}>No</option>
                                              <option value="Yes" {{($service['latest_transactions'][2]['cleared'] == 'Yes')?'selected':''}}>Yes</option>
                                            </select>
                                            @if($service['latest_transactions'][2]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][2][cleared]" value="{{$service['latest_transactions'][2]['cleared']}}">
                                              @endif
                                          </div>
                                        </div>
                                            @elseif($service['latest_transactions'][2]['payment_type'] == 'net_banking')
                                              <div style="display: block;">
                                          <div class="col-md-3">
                                            <label for="type[]" class="control-label">Type:</label>
                                            <select class="form-control select2-search-modal" name="services[0][transaction][2][type]" {{($service['latest_transactions'][2]['cleared'] == 'Yes')?'disabled':''}}>
                                              <option value="NEFT" {{($service['latest_transactions'][2]['type'] == 'NEFT')?'selected':''}}>NEFT</option>
                                              <option value="RTGS" {{($service['latest_transactions'][2]['type'] == 'RTGS')?'selected':''}}>RTGS</option>
                                            </select>
                                            @if($service['latest_transactions'][2]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][2][type]" value="{{$service['latest_transactions'][2]['type']}}">
                                              @endif
                                            <span class="help-block error_type" style="display:none;"></span>
                                          </div>

                                          <div class="col-md-4">
                                            <div class="form-group">
                                              <label for="transaction_id[]" class="control-label">Transaction Id:</label>
                                              <input class="form-control" placeholder="Transaction Id" name="services[0][transaction][2][transaction_id]" type="text" value="{{$service['latest_transactions'][2]['transaction_id']}}" {{($service['latest_transactions'][2]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][2]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][2][transaction_id]" value="{{$service['latest_transactions'][2]['transaction_id']}}">
                                                @endif
                                              <span class="help-block error_transaction_id" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-5">
                                            <div class="form-group">
                                              <label for="received_from[]" class="control-label">Name:</label>
                                              <input class="form-control" placeholder="Name" name="services[0][transaction][2][received_from]" type="text" value="{{$service['latest_transactions'][2]['received_from']}}" {{($service['latest_transactions'][2]['cleared'] == 'Yes')?'disabled':''}}>
                                              @if($service['latest_transactions'][2]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][2][received_from]" value="{{$service['latest_transactions'][2]['received_from']}}">
                                                @endif
                                              <span class="help-block error_received_from" style="display:none;"></span>
                                            </div>
                                          </div>
                                        </div>
                                            @endif
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                              @else
                                <div class="row 3rd_partial_pay" style="display:none;">
                                    <h4>#3rd Pay</h4>
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        <label class="control-label">Payment:</label>
                                        <select class="form-control select2-search-modal fk_partial_payment_type2" name="services[0][transaction][2][fk_payment_type]">
                                          <option value="">Payment Type</option>
                                          <option value="cash">Cash</option>
                                          <option value="cheque">Cheque</option>
                                          <option value="net_banking">Net Banking </option>
                                        </select>
                                        <span class="help-block error_fk_payment_type" style="display:none;"></span>
                                      </div>
                                    </div>

                                    <div class="col-md-10">
                                      <div class="row">
                                        <div class="col-md-5">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="amount[]" class="control-label">Amount:</label>
                                              <input class="form-control reflect_amt_key_up" placeholder="Amount" name="services[0][transaction][2][amount]" type="text" value="0">
                                              <span class="help-block error_amount2" style="display:none;"></span>
                                            </div>
                                          </div>

                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="date[]" class="control-label">Date:</label>
                                              <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="services[0][transaction][2][date]" type="text" value="0">
                                              <span class="help-block error_date2" style="display:none;"></span>
                                            </div>
                                          </div>
                                        </div>

                                          <div class="col-md-7 extra-filed-container2">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                              @endif
                              </div>
                            </div>
                          @elseif($service['latest_transactions'][0]['pay_type'] == 'full')
                            @if($service['latest_transactions'][0]['payment_type'] == 'cash')
                              <div style="display: block;">
                                 <div class="col-sm-12">
                                    <h5>Cash Pay</h5>
                                    <div class="form-group">
                                      <div class="row">
                                        <input type="hidden" name="services[0][transaction][0][id]" value="{{$service['latest_transactions'][0]['id']}}">
                                        <div class="col-sm-2">
                                          <label for="amount" class="control-label">Amount:</label>
                                          <input class="form-control reflect_amt_key_up" placeholder="Amount" name="services[0][transaction][0][amount]" type="text" value="{{$service['latest_transactions'][0]['amount']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                            @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                              <input type="hidden" name="services[0][transaction][0][amount]" value="{{$service['latest_transactions'][0]['amount']}}">
                                            @endif
                                          <span class="help-block error_amount0" style="display:none;"></span>
                                        </div>
                                        <div class="col-sm-3">
                                          <label for="date" class="control-label">Date:</label>
                                          <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="services[0][transaction][0][date]" type="text" value="{{(new Carbon($service['latest_transactions'][0]['date']))->format('d-m-Y')}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][date]" value="{{$service['latest_transactions'][0]['date']}}">
                                          @endif
                                          <span class="help-block error_date0" style="display:none;"></span>
                                        </div>
                                        <div class="col-sm-3">
                                          <label for="transaction_id" class="control-label">Transaction Id:</label>
                                          <input class="form-control" placeholder="Transaction Id" name="services[0][transaction][0][transaction_id]" type="text" value="{{$service['latest_transactions'][0]['transaction_id']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][transaction_id]" value="{{$service['latest_transactions'][0]['transaction_id']}}">
                                          @endif
                                          <span class="help-block error_transaction_id" style="display:none;"></span>
                                        </div>
                                        <div class="col-sm-4">
                                          <label for="received_from" class="control-label">Name:</label>
                                          <input class="form-control" placeholder="Name" name="services[0][transaction][0][received_from]" type="text" value="{{$service['latest_transactions'][0]['received_from']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][received_from]" value="{{$service['latest_transactions'][0]['received_from']}}">
                                          @endif
                                          <span class="help-block error_received_from" style="display:none;"></span>
                                        </div>
                                      </div>
                                    </div>
                                 </div>
                              </div>
                            @elseif($service['latest_transactions'][0]['payment_type'] == 'cheque')
                              <div style="display: block;">
                                <div class="col-sm-12">
                                    <h5>Cheque Pay</h5>
                                    <div class="form-group">
                                      <div class="row">
                                        <input type="hidden" name="services[0][transaction][0][id]" value="{{$service['latest_transactions'][0]['id']}}">
                                        <div class="col-sm-2">
                                          <label for="amount" class="control-label">Amount:</label>
                                          <input class="form-control reflect_amt_key_up" placeholder="Amount" name="services[0][transaction][0][amount]" type="text" value="{{$service['latest_transactions'][0]['amount']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][amount]" value="{{$service['latest_transactions'][0]['amount']}}">
                                          @endif
                                          <span class="help-block error_amount0" style="display:none;"></span>
                                        </div>
                                        <div class="col-sm-3">
                                          <label for="date[]" class="control-label">Date:</label>
                                          <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="services[0][transaction][0][date]" type="text" value="{{(new Carbon($service['latest_transactions'][0]['date']))->format('d-m-Y')}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][date]" value="{{$service['latest_transactions'][0]['date']}}">
                                          @endif
                                          <span class="help-block error_date0" style="display:none;"></span>
                                        </div>
                                        <div class="col-sm-2">
                                          <label for="cheque_number" class="control-label">Cheque Number:</label>
                                          <input class="form-control" placeholder="Cheque Number" name="services[0][transaction][0][cheque_number]" type="text" value="{{$service['latest_transactions'][0]['cheque_number']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][cheque_number]" value="{{$service['latest_transactions'][0]['cheque_number']}}">
                                          @endif
                                          <span class="help-block error_cheque_number" style="display:none;"></span>
                                        </div>
                                        <div class="col-sm-3">
                                          <label for="bank" class="control-label">Bank:</label>
                                          <input class="form-control" placeholder="Bank" name="services[0][transaction][0][bank]" type="text" value="{{$service['latest_transactions'][0]['bank']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][bank]" value="{{$service['latest_transactions'][0]['bank']}}">
                                          @endif
                                          <span class="help-block error_bank" style="display:none;"></span>
                                        </div>
                                        <div class="col-md-2">
                                          <label class="control-label">Cleared:</label>
                                          <select class="form-control" name="services[0][transaction][0][cleared]" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                            <option value="No" {{($service['latest_transactions'][0]['cleared'] == 'No')?'selected':''}}>No</option>
                                            <option value="Yes" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'selected':''}}>Yes</option>
                                          </select>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][cleared]" value="{{$service['latest_transactions'][0]['cleared']}}">
                                          @endif
                                        </div>
                                      </div>
                                    </div>
                                 </div>
                              </div>
                            @elseif($service['latest_transactions'][0]['payment_type'] == 'net_banking')
                              <div style="display: block;">
                                <div class="col-sm-12">
                                    <h5>Net Banking</h5>
                                    <div class="form-group">
                                      <div class="row">
                                        <input type="hidden" name="services[0][transaction][0][id]" value="{{$service['latest_transactions'][0]['id']}}">
                                        <div class="col-md-2">
                                          <label for="amount[]" class="control-label">Amount:</label>
                                          <input class="form-control reflect_amt_key_up" placeholder="Amount" name="services[0][transaction][0][amount]" type="text" value="{{$service['latest_transactions'][0]['amount']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][amount]" value="{{$service['latest_transactions'][0]['amount']}}">
                                          @endif
                                          <span class="help-block error_amount0" style="display:none;"></span>
                                        </div>

                                        <div class="col-md-2">
                                          <label for="date[]" class="control-label">Date:</label>
                                          <input class="form-control restrict-me date-picker" data-date-format="dd-mm-yyyy" placeholder="Date" name="services[0][transaction][0][date]" type="text" value="{{(new Carbon($service['latest_transactions'][0]['date']))->format('d-m-Y')}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][date]" value="{{$service['latest_transactions'][0]['date']}}">
                                          @endif
                                          <span class="help-block error_date0" style="display:none;"></span>
                                        </div>

                                        <div class="col-md-2">
                                          <label for="type[]" class="control-label">Type:</label>
                                          <select class="form-control select2-search-modal" name="services[0][transaction][0][type]" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                            <option value="NEFT" {{($service['latest_transactions'][0]['type'] == 'NEFT')?'selected':''}}>NEFT</option>
                                            <option value="RTGS" {{($service['latest_transactions'][0]['type'] == 'RTGS')?'selected':''}}>RTGS</option>
                                          </select>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][type]" value="{{$service['latest_transactions'][0]['type']}}">
                                          @endif
                                          <span class="help-block error_type" style="display:none;"></span>
                                        </div>

                                        <div class="col-md-2">
                                          <label for="transaction_id[]" class="control-label">Transaction Id:</label>
                                          <input class="form-control" placeholder="Transaction Id" name="services[0][transaction][0][transaction_id]" type="text" value="{{$service['latest_transactions'][0]['transaction_id']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][transaction_id]" value="{{$service['latest_transactions'][0]['transaction_id']}}">
                                          @endif
                                          <span class="help-block error_transaction_id" style="display:none;"></span>
                                        </div>
                                      
                                        <div class="col-md-2">
                                          <label for="received_from[]" class="control-label">Name:</label>
                                          <input class="form-control" placeholder="Name" name="services[0][transaction][0][received_from]" type="text" value="{{$service['latest_transactions'][0]['received_from']}}" {{($service['latest_transactions'][0]['cleared'] == 'Yes')?'disabled':''}}>
                                          @if($service['latest_transactions'][0]['cleared'] == 'Yes')
                                            <input type="hidden" name="services[0][transaction][0][received_from]" value="{{$service['latest_transactions'][0]['received_from']}}">
                                          @endif
                                          <span class="help-block error_received_from" style="display:none;"></span>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                              </div>
                            @endif
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="plan_append">
                      <div class="col-sm-12" style="margin-top:10px;">
                          <div class="table-responsive">
                              <table class="table table-xxs table-bordered">
                                  <thead>
                                      <tr>
                                          <th colspan="10" class="text-center bg-primary">Plan Details</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td class="text-center">Please Select Service & Plan</td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row form-group" style="margin-top:10px;">
                    	<h6 class="text-center">Old Transaction Data</h6>
                    	<div class="table-responsive">
          							<table class="table table-bordered table-striped table-hover table-xxs">
          								<thead>
          									<tr class="bg-blue">
          										<th>Invoice No.</th>
          										<th>Pay Via</th>
          										<th>Service Fee</th>
          										<th>Applied Tax</th>
          										<th>Discount</th>
          										<th>Payable Amount</th>
          										<th>Paid Amount</th>
          										<th>Payment Type</th>
          										<th>Recipient</th>
          										<th>Cheque Number</th>
          										<th>Bank / Net Pay Type</th>
          										<th>Net Pay Type</th>
          										<th>Transaction ID</th>
          										<th>Recieved By</th>
          										<th>Cleared</th>
          										<th>Date</th>
          									</tr>
          								</thead>
          								<tbody>
          									@foreach($service['latest_transactions'] as $transaction)
          										<tr>
          											<td>{{$transaction['id']}}</td>
          											<td>{{$transaction['payment_type']}}</td>
          											<td>{{$transaction['service_fee']}}</td>
          											<td>{{$transaction['applied_tax']}}%</td>
          											<td>{{$transaction['discount']}}%</td>
          											<td>{{$transaction['payable_amount']}}</td>
          											<td>{{$transaction['amount']}}</td>
          											<td>{{$transaction['pay_type']}}</td>
          											<td>{{$transaction['recipient']}}</td>
          											<td>{{$transaction['cheque_number']}}</td>
          											<td>{{$transaction['bank']}}</td>
          											<td>{{$transaction['payment_type']}}</td>
          											<td>{{$transaction['transaction_id']}}</td>
          											<td>{{$transaction['received_from']}}</td>
          											<td>{{$transaction['cleared']}}</td>
          											<td>{{(new Carbon($transaction['date']))->format('d/m/Y')}}</td>
          										</tr>
          									@endforeach
          								</tbody>
          							</table>      		
                    	</div>
                    </div>

                    <div class="plan_spinner col-sm-1 text-center" style="display:none;">
                        <i class="icon-spinner2 spinner"></i>
                    </div>
                    
                    <div class="row form-group" style="margin-top:10px;">
                    	<div class="well text-center">
		                    <ul class="list-inline list-inline-separate no-margin-bottom">
          								<li><span><strong>Comments : </strong></span>{{$service['comments']}}</li>
          								<li><span><strong>Force Verified : </strong></span>{{$service['service_verified']}}</li>
          								<li><span><strong>Force Verify Reason : </strong></span>{{$service['force_verify_reason']}}</li>
          							</ul>
          						</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </fieldset>

          <div class="text-right">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                <div class="form_spinner col-sm-1 text-center" style="display:none;">
                    <i class="icon-spinner2 spinner"></i>
                </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  <!-- /a legend -->
</div>
@include('admin.partials.jquery_service_plan_templates', ['is_edit' => $is_edit, 'service' => $service])
@stop

@push('scripts')
<script type="text/javascript">
  var $total_services;
  var $service_plans;
  var $getPlanUrl = '{{url('getPlan')}}';
  var $getCustomerUrl = "{{route('admin::clients.customers')}}";
  var $is_edit = true;
  var $change_warning_count = 1;
  var lastValue = '{{$service['latest_transactions'][0]['pay_type']}}';
  jQuery(window).load(function(){
    $total_services = 0;
    $service_plans = <?php echo json_encode($service_plans); ?>;
    var $plan_id = $('.edit_row').find(".plans").val();
    var $el = $('.edit_row').find(".plans");
  	var $el_details = $('.edit_row').find(".plan_append");

    getPlan($plan_id, $el, $el_details);
  });
</script>
@endpush