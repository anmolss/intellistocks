<fieldset data-ng-controller="servicesController as ctrl">
    <div class="row">
        <div class="col-md-12">
            @if($errors->count())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                {!! BootForm::text('Name <span class="text-danger">*</span>', 'name')->placeholder('Name')->required()->addGroupClass('col-md-6')!!}
                {!! BootForm::text('Symbol', 'code')->placeholder('Symbol')->addGroupClass('col-md-6')!!}
            </div>
            {!! BootForm::textarea('Description <span class="text-danger">*</span>', 'description')->placeholder('Description')->rows(3) !!}

            <div class="row">
                {!! BootForm::select('Payment Mode', 'payment_mode', config('rothmans.payment_mode'))->atribute('ng-model', 'payment_mode')->addGroupClass('col-md-6') !!}
                <div ng-show="payment_mode == 0">
                    {!! BootForm::select('Allowed Durations', 'allowed_duration', config('rothmans.service_duration'))->addGroupClass('col-md-6')->multiple()->required() !!}
                </div>
            </div>

            <div class="row">
                {!! BootForm::select('Service Type', 'service_type_id', config('rothmans.service_type'))->addGroupClass('col-md-6') !!}
                {!! BootForm::select('Trade Type', 'trade_type_id', config('rothmans.trade_type'))->addGroupClass('col-md-6') !!}
            </div>

            <div class="row">
                {!! BootForm::select('Call Type <span class="text-danger">*</span>', 'call_type',  config('rothmans.call_type'))->placeholder('Call Type')->required()->addGroupClass('col-md-6') !!}
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="display-block" for="status">Active</label>
                        {!! BootForm::inlineRadio('Yes', 'status')->value(1)->addClass('styled')->checked() !!}
                        @if(!$active_users)
                            {!! BootForm::inlineRadio('No', 'status')->value(0)->addClass('styled') !!}
                        @else
                            {!! BootForm::inlineRadio('No', 'status')->value(0)->addClass('styled')->attribute('disabled', 'disabled') !!}
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="display-block" for="status">Add to Watchlist</label>
                        {!! BootForm::inlineRadio('Yes', 'watchlist')->value(1)->addClass('styled')->checked() !!}
                        {!! BootForm::inlineRadio('No', 'watchlist')->value(0)->addClass('styled') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    <fieldset>
        <legend>Service delivery Template</legend>
        <uib-tabset active="tabindex">
            @foreach($default_templates as $exchange => $templates)
                <uib-tab heading="{{strtoupper($exchange)}}" ng-show="{{ ($exchange == 'nse' || $exchange == 'nfo' ) ? true : false }}">
                    @foreach($templates as $index => $template)
                        <div class="col-md-12 form-group">
                            {!! BootForm::textarea($template['name'].' <span class="text-danger">*</span>', 'template['.$template['id'].'][body]')->value(old('template.['.$template['id'].'][body]', $template['body']))->id('template_'.$template['id'].'_body')->placeholder('Description')->rows(2)->addGroupClass('mb-5') !!}
                            {!! BootForm::hidden('template['.$template['id'].'][id]')->value(($template['status']) ?  $template['id'] : '')  !!}
                            {!! BootForm::hidden('template['.$template['id'].'][name]')->value($template['name']) !!}
                            {!! BootForm::hidden('template['.$template['id'].'][status]')->value(1) !!}
                            {!! BootForm::hidden('template['.$template['id'].'][template_type]')->value($template['template_type'])!!}
                            {!! BootForm::hidden('template['.$template['id'].'][exchange]')->value($template['exchange']) !!}

                            <div class="border-left-lg border-left-info">
                                @foreach ($tokens as $token_val)
                                    <a onclick="insertAtCaret('template_{{$template['id']}}_body', '{{$token_val}}')" class="label bg-blue-700 text-bold m-5">{{$token_val}}</a>
                                @endforeach

                            </div>
                        </div>
                    @endforeach
                </uib-tab>
            @endforeach
        </uib-tabset>


    </fieldset>


    <div class="row">
        <div class="col-md-6">
            <span class="text-danger">*</span> Required Fields
        </div>
        <div class="col-md-6">
            {!! BootForm::submit($submitBtnText . ' <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right') !!}
        </div>
    </div>
</fieldset>


@section('scripts')
    @parent

    <script type="text/javascript">

        app.controller('servicesController',function($scope){
            @if(old('payment_mode') != null)
                $scope.payment_mode = '{!! old('payment_mode') !!}';
            @else
                $scope.payment_mode = '{!! isset($item) ? $item->payment_mode : 0 !!}';
            @endif
        });


        $('select').not('multiselect-full').select2({});

        function getToken(obj, message_id) {
            var token = $(obj).text();
            $('#'+message_id).val($('#'+message_id).val() + token);
        }

    </script>

@endsection