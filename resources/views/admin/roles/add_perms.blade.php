@extends('admin.layout.app')

@section('page_title', 'Roles')
@section('page_subtitle', 'Update Permissions')

@section('content')
    <div class="panel panel-flat">

        {!! BootForm::open()->post()->action(route('admin::roles.permissions', ['role' => $role->id] )) !!}

        <div class="panel-heading">
            <h5 class="panel-title">
                Permissions

                {!! BootForm::inlineCheckbox('Select All', null)->addClass('styled')->id('select_all_perms') !!}
                {!! BootForm::submit('Submit <i class="fa fa-arrow-circle-right"></i>')->addClass('btn btn-primary pull-right text-white') !!}
            </h5>
        </div>

        <table class="table table-hover tablet">
            @foreach( $permissions as $group => $controllers)
                <thead>
                <tr class="pt-10">
                    <th colspan="2">{{ $group }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $controllers as $controller => $c_perms)
                    <tr>
                        <td>{{ $controller }}</td>
                        <td class="form-group">
                            @foreach( $c_perms as $perm)
                                @if(in_array($perm->id , (array) $role_perms))
                                    {!! BootForm::inlineCheckbox($perm->display_name, 'perms[]')->addClass('styled')->value($perm->id)->checked() !!}
                                @else
                                    {!! BootForm::inlineCheckbox($perm->display_name, 'perms[]')->addClass('styled')->value($perm->id) !!}
                                @endif
                            @endforeach
                        </td>
                    </tr>
                @endforeach
                </tbody>
            @endforeach
        </table>

        <div class="panel-footer">
            <div class="col-md-12   ">
                {!! BootForm::submit('Submit <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right') !!}
            </div>
        </div>
        {!! BootForm::close() !!}
    </div>
@endsection

@section('scripts')
    @parent

    <script type="text/javascript">
        $(document).ready(function () {

            $('#select_all_perms').on('click', function(){
                $('form :checkbox').attr("checked",$(this).is(':checked'));
                $.uniform.update();
            });
        })

    </script>
@endsection