@extends('admin.layout.app')

@section('page_title', 'Role')
@section('page_subtitle', 'Create new Role')


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Role <small class="text-bold">Add</small>
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->post()->action(route('admin::roles.store')) !!}
                @include('admin.roles._form', ['submitBtnText' => "Add New Role"])
            {!! BootForm::close() !!}
        </div>
    </div>

@endsection