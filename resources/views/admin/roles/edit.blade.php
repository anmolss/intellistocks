@extends('admin.layout.app')

@section('page_title', 'Roles')
@section('page_subtitle', 'Edit role')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Role <small class="text-bold">Edit</small>
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->put()->action(route('admin::roles.update', ['roles' => $item->id] ))->multipart() !!}
            {!! BootForm::bind($item) !!}

            @include('admin.roles._form', ['submitBtnText' => "Edit Role"])

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection