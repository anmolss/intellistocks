@extends('admin.layout.app')

@section('page_title', 'Roles')
@section('page_subtitle', 'List of roles')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="over-title margin-bottom-15">
                Roles
                <a href="{{route('admin::roles.create')}}" class="btn btn-primary pull-right">Add New <i class="fa fa-plus"></i> </a>
            </h5>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-hover" id="sample-table-2">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Short Name</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($list) > 0)
                    @foreach($list as  $key => $item)
                        <tr>
                            <td>
                                <a href="{{route('admin::roles.edit', ['roles' => $item->id])}}" tooltip-placement="top" tooltip="Edit"  class="text-primary-600">{{ $item->display_name }}</a>
                            </td>
                            <td>{{ $item->name }}</td>
                            <td>{!! $item->description !!}</td>
                            <td class="center">
                                <ul class="icons-list">
                                    <li class="dropdown dropdown-menu-left">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-cog7"></i><span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{route('admin::roles.permissions', ['role' => $item->id])}}"><i class="icon-add-to-list pull-right"></i>Permissions</a></li>
                                            <li><a href="{{route('admin::roles.edit', ['roles' => $item->id])}}"><i class="icon-pencil7 pull-right"></i> Edit</a></li>
                                            <li class="divider"></li>
                                            <li class="text-danger-700">
                                                {!! BootForm::open()->delete()->action(route('admin::roles.destroy', ['roles' => $item->id]))!!}
                                                <a class="delete_anchor" style="color: inherit; overflow: hidden; display: block; padding: 2px 16px;"><i class="icon-trash  pull-right"></i> Delete</a>
                                                {!! BootForm::close() !!}
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" align="center">No Roles Found</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
            $('a.delete_anchor').on('click', function(){
                if(confirm('Are you sure you want to detele this?')){
                    var form = $(this).closest("form");
                    form.submit();
                } else {
                    return false;
                }
            });
    </script>
@endsection