<fieldset>
    <div class="row">
        <div class="col-md-6">
            {!! BootForm::text('Display Name <span class="text-danger">*</span>', 'display_name')->placeholder('Display Name')->required()!!}
            {!! BootForm::textarea('Description <span class="text-danger">*</span>', 'description')->placeholder('description')->rows(3)->helpBlock('Description will created automatically') !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div>
                <span class="text-danger">*</span> Required Fields
            </div>
        </div>
        <div class="col-md-6">
            {!! BootForm::submit($submitBtnText . ' <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right') !!}
        </div>
    </div>
</fieldset>
