@extends('admin.layout.app')
@section('page_title', 'Team')
@section('page_subtitle', 'Edit role')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Edit <small class="text-bold">Team</small>

            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->put()->action(route('admin::teams.update',$team))->novalidate() !!}
            {!! Bootform::bind($team) !!}
            <fieldset>

                {!! BootForm::text('Name <span class="text-danger">*</span>', 'name')->placeholder('Name')->required()!!}


                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        {!! BootForm::submit( 'Save <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right') !!}
                    </div>
                </div>


            </fieldset>
            {!! BootForm::close() !!}

        </div>
    </div>
@endsection
