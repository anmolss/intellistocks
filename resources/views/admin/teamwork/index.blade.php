@extends('admin.layout.app')

@section('page_title', 'Teams')
@section('page_subtitle', 'List of teams')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="over-title margin-bottom-15">
                Teams
                @permission('create-teams')
                    <a href="{{route('admin::teams.create')}}" class="btn btn-primary pull-right">Add New <i class="fa fa-plus"></i> </a>
                @endpermission
            </h5>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-hover" id="sample-table-2">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Status</th>

                </tr>
                </thead>
                <tbody>

                    @foreach($teams as $team)
                        <tr>
                            <td> <a href="{{route('admin::teams.edit', ['teams' => $team->id])}}" tooltip-placement="top" tooltip="Edit"  class="text-primary-600">{{$team->name}}</a>
                            <td>
                                @if(auth()->user()->isOwnerOfTeam($team))
                                    <span class="label label-success">Owner</span>
                                @else
                                    <span class="label label-primary">Member</span>
                                @endif
                            </td>
                            <td class="text-primary-600">
                                @if(is_null(auth()->user()->currentTeam) || auth()->user()->currentTeam->getKey() !== $team->getKey())
                                    <a href="{{route('admin::teams.switch', $team)}}" class="btn btn-sm btn-default">
                                        <i class="fa fa-sign-in"></i> Switch
                                    </a>
                                @else
                                    <span class="label label-default">Current team</span>
                                @endif
                            </td>
                            <td class="text-primary-600">
                                <a href="{{route('admin::teams.members.show', $team)}}" class="btn btn-sm btn-default">
                                    <i class="fa fa-users"></i> Members
                                </a>
                            </td>

                            @if(auth()->user()->isOwnerOfTeam($team))
                            <td class="center">
                                <ul class="icons-list">
                                    <li class="dropdown dropdown-menu-left">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-cog7"></i><span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{route('admin::teams.edit', $team)}}"><i class="icon-pencil7 pull-right"></i> Edit</a></li>
                                            <li class="divider"></li>
                                            <li class="text-danger-700">
                                                {!! BootForm::open()->delete()->action(route('admin::teams.destroy', $team))!!}
                                                <a class="delete_anchor" style="color: inherit; overflow: hidden; display: block; padding: 2px 16px;"><i class="icon-trash  pull-right"></i> Delete</a>
                                                {!! BootForm::close() !!}
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
