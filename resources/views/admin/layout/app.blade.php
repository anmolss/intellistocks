<!DOCTYPE html>
<html lang="en" ng-app="rothmansApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="Socket-ID" content="">
    <title>Rothmans CRM</title>


    @section('styles')
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href="{{asset('bower_components/angular-bootstrap/ui-bootstrap-csp.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('bower_components/angular-ui-select/dist/select.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('bower_components/angular-ui-grid/ui-grid.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('bower_components/angular-bootstrap/ui-bootstrap-csp.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/core.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/components.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/colors.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">

    @show

</head>
<!-- end: HEAD -->
<body>
<div id="app">
    <!-- sidebar -->
    @include('admin.partials.top_nav')
    <!-- / sidebar -->

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('admin.partials.main_menu')
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">
                <!-- Content area -->
                <div class="content">
                    @include('admin.partials.flash')

                    @include('admin.partials.breadcrumb')

                    @yield('content')

                    <!-- Footer -->
                    @include('admin.partials.footer')
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->
</div>


<script type="text/javascript" src="{{asset('js/plugins/loaders/pace.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/core/libraries/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/core/libraries/bootstrap.min.js') }}"></script>
<!-- Theme JS files -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- /theme JS files -->
<script type="text/javascript" src="{{asset('js/plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/plugins/notifications/pnotify.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/forms/styling/uniform.min.js')}}"></script>

<script type="text/javascript" src="{{asset('js/plugins/ui/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/plugins/pickers/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{asset('js/plugins/pickers/datepicker.js') }}"></script>

<script type="text/javascript" src="{{asset('js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/plugins/ui/ripple.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/plugins/forms/styling/uniform.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
<script type="text/javascript" src="{{asset('js/pages/form_multiselect.js') }}"></script>
<script type="text/javascript" src="{{asset('js/plugins/forms/inputs/maxlength.min.js') }}"></script>


{{--<script src="https://js.pusher.com/3.2/pusher.min.js"></script>--}}
<script type="text/javascript" src="{{asset('js/plugins/loaders/progressbar.min.js') }}"></script>

@include('admin.partials.angular_import')

@section('scripts')

    <script type="text/javascript" src="{{ asset('js/core/app.js') }}"></script>
    <script type="text/javascript">

        jQuery(document).ready(function(){
            jQuery(function () {
                jQuery.ajaxSetup({
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') }
                });
            });

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    language: 'en',
                    orientation: "left",
                    autoclose: true,
                    orientation: 'right'
                });
            }
        });

        jQuery(document).delegate('.btn-delete','click',function(e) {
            $url = jQuery(this).attr('href');
            e.preventDefault();
            swal({   
                title: "Are you sure?",   
                text: "You will not be able to revert this!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Delete",   
                cancelButtonText: "Cancel",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true,
            }, function(isConfirm){   
                if (isConfirm) {
                    jQuery.ajax({
                        type: 'POST',
                        url: $url,
                        beforeSend: function() {
                            jQuery('.form_spinner').show();
                        },
                        success: function(data) {
                            swal("Deleted!", "Data has been deleted.", "success");
                        },
                        error: function(xhr) {
                            alert(xhr.statusText + xhr.responseText);
                        },
                        complete: function() {
                            location.reload();
                        }
                    });
                } else {
                    swal("Cancelled", "Data Not Deleted :)", "error");   
                } 
            });
        });

        jQuery(document).delegate('.modal-action','click',function(e) {
            e.preventDefault;
            var $data_id = jQuery(this).attr('data-id');
            var $data_action = jQuery(this).attr('data-action');
            var $url = jQuery(this).attr('href');
            jQuery('#action_modal').find('#modal_content_append').empty();
            jQuery.ajax({
                type: 'POST',
                url: $url,
                data: {'id':$data_id, 'action':$data_action},
                dataType: 'json',
                beforeSend: function() {
                    jQuery('.action_modal_form_spinner').show();
                },
                success: function(data) {
                    jQuery('.action_modal_form_spinner').hide();
                    if (data.success == 1) {
                        jQuery('#action_modal').find('#modal_content_append').html(data.content);  
                    }
                },
                error: function(xhr) {
                    alert(xhr.statusText + xhr.responseText);
                },
                complete: function() {
                    jQuery('.action_modal_form_spinner').hide();
                }
            });
        });

        jQuery(document).delegate('.action_modal_form', 'submit', function(e){
            e.preventDefault();
            $data = jQuery(this).serializeArray();
            $url = jQuery(this).attr('action');
            jQuery.ajax({
                type: 'POST',
                url: $url,
                data: $data,
                dataType: 'json',
                beforeSend: function() {
                    jQuery('.form_spinner').show();
                },
                success: function(data) {
                    jQuery('.form_spinner').hide();
                    if (data.success_form == 1) {
                        jQuery('#action_modal').modal('toggle');
                        swal({
                            title: "Done",   
                            text: data.msg,   
                            type: "success", 
                            closeOnConfirm: true
                        }, 
                        function(){
                            location.reload();
                        });
                    } else if (data.success_form == 2) {
                        jQuery('#action_modal').modal('toggle');
                        swal("Ohh Snap", data.msg, "warning");
                    }

                },
                error: function(xhr) {
                    alert(xhr.statusText + xhr.responseText);
                },
                complete: function() {
                    jQuery('.form_spinner').hide();
                }
            });
        });

      jQuery(document).ready(function(){
        $('#v-bottom-basic .progress-bar').each(function () {
            var $pb = $(this);
            if ($pb.attr('data-transitiongoal-backup') == 'High') {
                $pb.attr('data-transitiongoal', '100');
                $prog = 'High';
            } else if ($pb.attr('data-transitiongoal-backup') == 'Medium'){
                $pb.attr('data-transitiongoal', '50');
                $prog = 'Medium';
            } else if ($pb.attr('data-transitiongoal-backup') == 'Low'){
                $pb.attr('data-transitiongoal', '25');
                $prog = 'Low';
            } else {
                $prog = 'no';
                $pb.attr('data-transitiongoal', $pb.attr('data-transitiongoal-backup'));
            }
            if( $prog != 'no'){
                $pb.progressbar({display_text: 'fill', use_percentage: false, amount_format: function(amount_part, amount_total) { return $prog; }});
            } else {
                $pb.progressbar({display_text: 'fill'});
            }
        });
      });
    </script>
    {{--<script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('b863837543382139d2a0', {
            cluster: 'ap1',
            encrypted: true
        });

        var channel = pusher.subscribe('test_channel');
        channel.bind('my_event', function(data) {
            show_stack_top_right('success',data.message);
        });
    </script>--}}
@show

@stack('scripts')

{{--<script src="http://notify.rothmans.dev:6001/socket.io/socket.io.js"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>--}}
<script type="text/javascript" src="{{asset('js/custom.js') }}"></script>
</body>
</html>
