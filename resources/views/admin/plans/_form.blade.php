<fieldset ng-controller="servicesController as servCtrl" ng-cloak>
    <div class="row">
        <div class="col-md-12   ">
            @if($errors->count())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

                <div class="row form-group">
                    {!! BootForm::text('Service Name <span class="text-danger">*</span>', 'service_name')->placeholder('Name')->disabled()->value($service->name)->addGroupClass('col-md-6')!!}

                    <div class="form-group col-md-6">
                        <label class="control-label" for="payment_mode">Plan Category</label>
                        <select name="category" id="category" class="form-control" ng-model="category">
                            <option ng-repeat="(key, value) in pay_options" value="@{{ key }}">@{{value}}</option>
                        </select>
                    </div>

                    {!! BootForm::hidden('service_id', '')->attribute('ng-value', 'service.id')!!}
                </div>

                <div class="row " ng-cloak>
                    <div ng-if="category=='profit_sharing_fixed'" class="well row">
                        {!! BootForm::text('Share in Profit (in %)<span class="text-danger">*</span>', 'plan_detail[amount]')->addGroupClass('col-md-6') !!}
                    </div>

                    <div ng-if="category=='profit_sharing_slab'" class="well row">
                        <div ng-repeat="detail in plan_detail track by $index" class="row">

                            {!! BootForm::text('Range From <span class="text-danger">*</span>', 'plan_detail[@{{ $index }}][from]')->inline()->addGroupClass('col-md-3')->attribute('ng-model', '$parent.plan_detail[$index].from')->attribute('validator', 'checkDetails')->attribute('valid-method', 'watch')->attribute('index', '@{{$index}}')->attribute('prop', 'from') !!}
                            {!! BootForm::text('To <span class="text-danger">*</span>', 'plan_detail[@{{ $index }}][to]')->inline()->addGroupClass('col-md-3')->attribute('ng-model', '$parent.plan_detail[$index].to')->attribute('validator', 'checkDetails')->attribute('valid-method', 'watch')->attribute('index', '@{{$index}}')->attribute('prop', 'to') !!}
                            {!! BootForm::text('Percent <span class="text-danger">*</span>', 'plan_detail[@{{ $index }}][percent]')->inline()->addGroupClass('col-md-4')->attribute('ng-model', '$parent.plan_detail[$index].percent')->attribute('validator', 'checkDetails')->attribute('valid-method', 'watch')->attribute('index', '@{{$index}}')->attribute('prop', 'percent') !!}

                            <a class="btn btn-primary" ng-if="$last" ng-click="addDetail($parent.plan_detail[$index].to)">+</a>
                            <a class="btn btn-danger" ng-if="!$first" ng-click="removeDetail($index)">&times;</a>
                        </div>
                    </div>
                </div>

            {!! BootForm::text('Name <span class="text-danger">*</span>', 'name')->placeholder('Name')->required()!!}


            <div class="row form-group">
                {!! BootForm::text('Minimum Call <span class="text-danger">*</span>', 'min_call')->addGroupClass('col-md-6')->required() !!}
                {!! BootForm::text('Maximum Call <span class="text-danger">*</span>', 'max_call')->addGroupClass('col-md-6')->required() !!}
            </div>




            <div class="row form-group">
                <div class="col-md-12"  ng-if="category=='profit_sharing_fixed' || category=='profit_sharing_slab'">
                    {!! BootForm::select('Bill Period (in Days) <span class="text-danger">*</span>', 'bill_period', config('rothmans.service_duration'))->addGroupClass('col-md-6')->required() !!}
                    {!! BootForm::text('Annual Fee <span class="text-danger">*</span>', 'annual_fee')->required()->addGroupClass('col-md-6') !!}
                </div>
                <div class="col-md-12"  ng-if="category=='time_based'">
                    {!! BootForm::text('Service Fees <span class="text-danger">*</span>', 'service_fee')->default(0)->required()->addGroupClass('col-md-6') !!}
                    {!! BootForm::select('Duration <span class="text-danger">*</span>', 'duration', $allowed_durations)->addGroupClass('col-md-6')->required() !!}
                </div>
            </div>

            <div class="row form-group">
                {!! BootForm::text('Grace Period (in Days) <span class="text-danger">*</span>', 'grace_period')->addGroupClass('col-md-4')->required() !!}
                {!! BootForm::text('Maximum Discount <span class="text-danger">*</span>', 'max_discount')->addGroupClass('col-md-4')->default(0)->required() !!}
            </div>

            <div class="row form-group">
                <div class="col-md-6">
                    <label class="control-label" for="ip_restrict">Applicable Tax <span class="text-danger">*</span></label>
                    <ui-select multiple
                               ng-model="$parent.applicable_tax"
                               sortable="true"
                               title="Add taxes">
                        <ui-select-match placeholder="Add Taxes...">@{{$item.name}}</ui-select-match>
                        <ui-select-choices repeat="tax in $parent.taxes track by $index"
                                           refresh-delay="20">
                            <div ng-bind-html="tax.name | highlight: $select.search"></div>
                            <small>
                                tax: @{{tax.value}}
                            </small>
                        </ui-select-choices>
                    </ui-select>
                    <select style="display: none;" name="applicable_tax[]" multiple>
                        <option ng-repeat="tax in applicable_tax | orderBy:'text'" value="@{{tax.id}}" selected>@{{tax.name}}</option>
                    </select>

                </div>
            </div>


            <div class="row form-group">
                <div class="col-md-6">
                    <label class="control-label" for="nav_cal">NAV Caculation <span class="text-danger">*</span></label>
                    {!! BootForm::inlineRadio('Absolute', 'nav_cal')->value(0)->addClass('styled')->addGroupClass(6) !!}
                    {!! BootForm::inlineRadio('Booked', 'nav_cal')->value(1)->addClass('styled')->addGroupClass(6)!!}
                </div>


                <div class="col-md-6">
                    <label for="status">Active <span class="text-danger">*</span></label>
                    {!! BootForm::inlineRadio('Yes', 'status')->value(1)->addClass('styled')->addGroupClass(6) !!}
                    @if(!$active_users)
                        {!! BootForm::inlineRadio('No', 'status')->value(0)->addClass('styled')->addGroupClass(6) !!}
                    @else
                        {!! BootForm::inlineRadio('No', 'status')->value(0)->addClass('styled')->addGroupClass(6)->attribute('disabled', 'disabled') !!}
                    @endif
                </div>

            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <div>
                <span class="text-danger">*</span> Required Fields
            </div>
        </div>
        <div class="col-md-6">
            {!! BootForm::submit($submitBtnText . ' <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right')->attribute('ng-disabled', 'plans.$invalid') !!}
        </div>
    </div>
</fieldset>


@section('scripts')
    @parent

    <script type="text/javascript">
        app.config(['$validationProvider', function ($validationProvider) {
            $validationProvider
                .setExpression({
                    checkDetails: function(value, scope, element, attrs) {
                        if ( isNaN(value) || value < 0) {
                            return false;
                        }
                        switch (attrs.prop) {
                            case 'from':
                                if (value <= parseInt(scope.plan_detail[attrs.index].to)
                                    && (
                                        (0 < parseInt(attrs.index) && parseInt(scope.plan_detail[attrs.index - 1].to) == value)
                                        ||
                                        0 == parseInt(attrs.index)
                                    )) {
                                    return true;
                                }
                                break;
                            case 'to':
                                if (scope.plan_detail[attrs.index].to != '' && value >= parseInt(scope.plan_detail[attrs.index].from) ) {
                                    return true;
                                }
                                break;
                            case 'percent':
                                if (value => 0) {
                                    return true;
                                }
                                break;
                        }

                    }
                })
                .setDefaultMsg({
                    checkDetails: {
                        error: 'Invalid',
                        success: ''
                    }
                });
        }]);


        app.controller('servicesController',function($scope){

            var servCtrl = this;

            $scope.service = {!! $service->toJson() !!};

            $scope.taxes = JSON.parse('{!! json_encode($taxes) !!}');

            @if(old('applicable_tax') != null)
                $scope.applicable_tax = {!! json_encode(old('applicable_tax')) !!};
            @else
                $scope.applicable_tax = {!! isset($item) ? json_encode($item->applicable_tax) : '[]' !!};
            @endif

            angular.forEach($scope.applicable_tax, function(value, key) {
                $scope.applicable_tax[key] = $scope.taxes.filter(function(item) { return item.id == value; })[0];
            });


            $scope.pay_options = {!! json_encode(config('rothmans.plan_category.'. $service->payment_mode)) !!};

            @if(old('payment_mode') != null)
                $scope.payment_mode = '{!! old('payment_mode') !!}';
            @else
                $scope.payment_mode = '{{ $service->payment_mode }}';
            @endif

            @if(old('category') != null)
                $scope.category = '{!! old('category') !!}';
            @else
                $scope.category = '{{ isset($item) ? $item->category : key(config('rothmans.plan_category.'. $service->payment_mode)) }}';
            @endif

            @if(old('plan_detail') != null)
                $scope.plan_detail = {!! json_encode(old('plan_detail')) !!};
            @else
                $scope.plan_detail = {!! (isset($item) && $item->plan_detail != '') ? json_encode($item->plan_detail ) : '[{"from":0,"to":10,"percent":0, "error": false}]' !!};
            @endif


            $scope.removeDetail = function ($index) {
                $scope.plan_detail.splice($index, 1);
            };

            $scope.addDetail = function (to_val) {
                to_val = parseInt(to_val);
                $scope.plan_detail.push({"from":to_val,"to":to_val +10,"percent":0});
            };

        });

    </script>

@endsection