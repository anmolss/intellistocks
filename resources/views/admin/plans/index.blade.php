@extends('admin.layout.app')

@section('page_title', 'Plans')
@section('page_subtitle', 'List of plans')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="over-title margin-bottom-15">
                Plans
                @permission('create-plans')
                <a href="{{route('admin::plans.create', ['service' => $service->id])}}" class="btn btn-primary pull-right">Add New <i class="fa fa-plus"></i> </a>
                @endpermission
            </h5>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-hover" id="sample-table-2">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Active Since</th>
                        <th>No of Customers</th>
                        <th>Initial Amount</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @if(count($list) > 0)
                    @foreach($list as  $key => $item)
                        <tr>
                            <td>
                                <a href="{{route('admin::plans.edit', ['plan' => $item->id, 'service' => $item->service->id])}}" tooltip-placement="top" tooltip="Edit"  class="text-primary-600">{{ $item->name }}</a>
                            </td>
                            <td>{{ $item->created_at->format(config('rothmans.date_format')) }}</td>
                            <td>{{ $item->total_users }}</td>
                            {{--<td>{{ config('rothmans.currency_format')->format($item->service_fee) }}</td>--}}
                            <td>&#8377; {{  number_format(($service->payment_mode==0) ? $item->service_fee : $item->annual_fee, 2) }}</td>
                            <td>{!! ($item->status)? '<span class="label label-success"><i class="icon-check"></i></span>' : '<span class="label label-danger"><i class="icon-cross3"></i></span>' !!}</td>

                            <td class="center">
                                <ul class="icons-list">
                                    <li class="dropdown dropdown-menu-left">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-cog7"></i><span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{route('admin::plans.edit', ['plan' => $item->id, 'service' => $service->id])}}"><i class="icon-pencil7 pull-right"></i> Edit</a></li>
                                            <li class="divider"></li>
                                            <li class="text-danger-700">
                                                {!! BootForm::open()->delete()->action(route('admin::plans.destroy', ['plan' => $item->id, 'service' => $service->id]))!!}
                                                <a class="delete_anchor" style="color: inherit; overflow: hidden; display: block; padding: 2px 16px;"><i class="icon-trash  pull-right"></i> Delete</a>
                                                {!! BootForm::close() !!}
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" align="center">No Services Found</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        $('a.delete_anchor').on('click', function(){
            if(confirm('Are you sure you want to detele this?')){
                var form = $(this).closest("form");
                form.submit();
            } else {
                return false;
            }
        });
    </script>
@endsection