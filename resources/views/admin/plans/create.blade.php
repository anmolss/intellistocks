@extends('admin.layout.app')

@section('page_title', 'Plan')
@section('page_subtitle', 'Create New Plan')


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Plan <small class="text-bold">Add</small>
                <a href="{{url()->previous()}}" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->post()->action(route('admin::plans.store', ['service'=> $service->id]))->name("plans") !!}
                @include('admin.plans._form', ['submitBtnText' => "Add Plan"])
            {!! BootForm::close() !!}
        </div>
    </div>

@endsection