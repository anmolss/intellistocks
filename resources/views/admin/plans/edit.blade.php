@extends('admin.layout.app')

@section('page_title', 'Plans')
@section('page_subtitle', 'Edit Plan')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Plan <small class="text-bold">Edit</small>
                <a href="{{URL::previous()}}" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->put()->action(route('admin::plans.update', ['plan' => $item->id, 'service' => $service->id] ))->multipart()->name("plans") !!}
            {!! BootForm::bind($item) !!}

            @include('admin.plans._form', ['submitBtnText' => "Edit Service"])

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection