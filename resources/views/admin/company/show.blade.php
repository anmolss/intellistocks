@extends('admin.layout.app')

@section('page_title', $company['name'])

@section('content')

    {{--<div class="panel panel-flat">--}}
        {{--<div class="panel-heading">--}}
            {{--<h5 class="over-title margin-bottom-15">--}}
                {{--Company--}}
            {{--</h5>--}}
            {{----}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="row">
        <div class="col-md-12">
            <div class="panel invoice-grid">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list list-unstyled">
                                <li><span class="text-semibold">ISIN Code: </span>&nbsp;{{$company['security_master']['i_s_i_n_code']}}</li>
                                <li><span class="text-semibold">Incorporation Date: </span><span class="text-semibold">{{$company['incorporation_date']}}</span></li>
                            </ul>
                        </div>

                        <div class="col-sm-6">
                            <ul class="list list-unstyled text-right">
                                <li><span class="text-semibold">Business Group Name: </span>{{$company['business_group_name']}}</span></li>
                                <li class="dropdown">
                                    <span class="text-semibold">Industry Name: </span>&nbsp;
                                    {{$company['industry_name']}}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="panel-footer panel-footer-condensed">
                    <div class="heading-elements">

                        <div class="col-sm-6">

                                <table class="table table-hover tablet">
                                    <thead>
                                    <tr class="pt-10">
                                        <th colspan="2">BSE Code</th>
                                        <th colspan="2">NSE Code</th>
                                    </tr>
                                    </thead>
                                    @foreach( $company['listing_master'] as $group => $controllers)
                                        <tbody>
                                            <tr>
                                                <td colspan="2">{{ $controllers['scrip_code1_given_by_exchange'] }}</td>
                                                <td colspan="2">{{ $controllers['scrip_code2_given_by_exchange'] }}</td>
                                            </tr>
                                        </tbody>
                                    @endforeach
                                </table>
                        </div>
                        <ul class="list list-unstyled text-right">
                            <li><span class="text-semibold">BSE Last trade price - </span>@foreach( $bse as $group => $controllers)
                                    {{$controllers['last_trade_price']}}@break;
                                @endforeach</li>
                            <li class="dropdown">
                                <span class="text-semibold">NSE Last trade price - </span>&nbsp;
                                @foreach( $nse['0'] as $group => $controllers)
                                    {{$controllers['last_traded_price']}}
                                    @break;
                                @endforeach
                            </li>
                        </ul>




                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">

                    </div>


                        <div class="tabbable">
                            <ul class="nav nav-tabs">
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Corporate Action <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#basic-tab1" data-toggle="tab">Board Meetings </a></li>
                                        <li><a href="#basic-tab2" data-toggle="tab">Dividends</a></li>
                                        <li><a href="#basic-tab3" data-toggle="tab">Bonus</a></li>
                                        <li><a href="#basic-tab4" data-toggle="tab">Rights</a></li>
                                        <li><a href="#basic-tab5" data-toggle="tab">Splits</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Company Info <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#basic-tab6" data-toggle="tab">Management</a></li>
                                        <li><a href="#basic-tab7" data-toggle="tab">Background</a></li>
                                        <li><a href="#basic-tab8" data-toggle="tab">Listing</a></li>
                                        <li><a href="#basic-tab9" data-toggle="tab">Location</a></li>
                                    </ul></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Annual Report <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#basic-tab10" data-toggle="tab">Raw Materials</a></li>
                                        {{--<li><a href="#basic-tab4" data-toggle="tab"></a></li>--}}
                                    </ul>
                                </li>
                                <li><a href="#basic-tab11" data-toggle="tab">BSE</a></li>
                                <li><a href="#basic-tab12" data-toggle="tab">NSE</a></li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="basic-tab1">
                                    <div class="col-sm-6">

                                        <table class="table table-hover tablet">
                                            <thead>
                                            <tr class="pt-10">
                                                <th colspan="2">Meeting Date</th>
                                                <th colspan="2">Remark</th>
                                            </tr>
                                            </thead>
                                            @foreach( $company['board_meeting'] as $group => $controllers)
                                                <tbody>
                                                <tr>
                                                    <td colspan="2">{{ date('d-m-Y', strtotime($controllers['board_meet_date']))}}</td>
                                                    <td colspan="2">{{ $controllers['purpose'] }}</td>
                                                </tr>
                                                </tbody>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="basic-tab2">
                                    <div class="col-sm-6">

                                        <table class="table table-hover tablet">
                                            <thead>
                                            <tr class="pt-10">
                                                <th colspan="2">Announcement Date</th>
                                                <th colspan="2">Effective Date</th>
                                                <th colspan="2">Dividend type</th>
                                                <th colspan="2">Dividend</th>
                                                <th colspan="2">Remarks</th>
                                            </tr>
                                            </thead>
                                            @foreach( $company['dividend'] as $group => $controllers)
                                                <tbody>
                                                <tr>
                                                    <td colspan="2">{{ date('d-m-Y', strtotime($controllers['date_of_announcement'])) }}</td>
                                                    <td colspan="2"></td>
                                                    <td colspan="2">{{ $controllers['interim_or_final'] }}</td>
                                                    {{--<td colspan="2">{{ $controllers['percentage'] }}</td>--}}
                                                    <td colspan="2">{{ $controllers['remarks'] }}</td>
                                                </tr>
                                                </tbody>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="basic-tab3">
                                    <div class="col-sm-6">

                                        <table class="table table-hover tablet">
                                            <thead>
                                            <tr class="pt-10">
                                                <th colspan="2">Announcement Date</th>
                                                <th colspan="2">Bonus Ratio</th>
                                                <th colspan="2">Record Date</th>
                                                <th colspan="2">Ex-Bonus Date</th>
                                            </tr>
                                            </thead>
                                            @foreach( $company['bonuse'] as $group => $controllers)
                                                <tbody>
                                                <tr>
                                                    <td colspan="2">{{ date('d-m-Y', strtotime($controllers['date_of_announcement'])) }}</td>
                                                    <td colspan="2">{{ $controllers['ratio_offerred'] }}</td>
                                                    <td colspan="2">{{ $controllers['record_date'] }}</td>
                                                    {{--<td colspan="2">{{ $controllers['percentage'] }}</td>--}}
                                                </tr>
                                                </tbody>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="basic-tab4">
                                    <div class="col-sm-12">

                                        <table class="table table-hover tablet">
                                            <thead>
                                            <tr class="pt-10">
                                                <th colspan="2">Announcement Date</th>
                                                <th colspan="2">Rights Ratio</th>
                                                <th colspan="2">Face Value</th>
                                                <th colspan="2">Premium</th>
                                                <th colspan="3">Record Date</th>
                                                <th colspan="3">Ex-Rights Date</th>
                                            </tr>
                                            </thead>
                                            @foreach( $company['right'] as $group => $controllers)
                                                <tbody>
                                                <tr>
                                                    <td colspan="2">{{ date('d-m-Y', strtotime($controllers['date_of_announcement'])) }}</td>
                                                    <td colspan="2">{{ $controllers['ratio_offering'] }}:{{ $controllers['ratio_existing'] }}</td>
                                                    <td colspan="2">{{ $controllers['face_value_existing_instrument'] }}</td>
                                                    <td colspan="2">{{ $controllers['rights_premium'] }}</td>
                                                    <td colspan="3">{{ $controllers['record_date'] }}</td>
                                                    <td colspan="3">{{ $controllers['xr_date'] }}</td>
                                                </tr>
                                                </tbody>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="basic-tab5">
                                    <div class="col-sm-6">

                                        <table class="table table-hover tablet">
                                            <thead>
                                            <tr class="pt-10">
                                                <th colspan="2">Announcement Date</th>
                                                <th colspan="2">Old FV</th>
                                                <th colspan="2">New FV</th>
                                            </tr>
                                            </thead>
                                                <tbody>
                                                <tr>
                                                    <td colspan="2">{{date('d-m-Y', strtotime($company['split']['date_of_announcement'])) }}</td>
                                                    <td colspan="2">{{$company['split']['old_face_value'] }}</td>
                                                    <td colspan="2">{{ $company['split']['new_face_value'] }}</td>

                                                </tr>
                                                </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="basic-tab6">
                                    <div class="col-sm-6">

                                        <table class="table table-hover tablet">
                                            <thead>
                                            <tr class="pt-10">
                                                <th colspan="3">Name</th>
                                                <th colspan="3">Designation</th>
                                            </tr>
                                            </thead>
                                            @foreach( $company['management_team'] as $group => $controllers)
                                                <tbody>
                                                <tr>
                                                    <td colspan="3">{{ $controllers['person_name'] }}</td>
                                                    <td colspan="3">{{ $controllers['designation_description'] }}</td>
                                                </tr>
                                                </tbody>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="basic-tab7">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel panel-flat">
                                                <div class="panel-body">
                                                    <div class="panel panel-flat col-lg-6">
                                                        <table class="table table-borderless table-xs content-group-sm">
                                                            <tbody>
                                                            <tr>
                                                                <td class="text-semibold"> Industry Name:</td>
                                                                <td class="text-right"><span class="pull-right"><a href="#">{{$company['background_info']['industry_name']}}</a></span></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-semibold"> House Name:</td>
                                                                <td class="text-right">{{$company['background_info']['business_group_name']}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-semibold"> Year Of Incorporation:</td>
                                                                <td class="text-right">{{$company['incorporation_year']}}</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    {{--<div class="panel panel-flat col-lg-6">--}}
                                                        {{--<table class="table table-borderless table-xs content-group-sm">--}}
                                                            {{--<tbody>--}}
                                                            {{--<tr>--}}
                                                                {{--<td class="text-semibold">Address</td>--}}
                                                                {{--<td class="text-right"></td>--}}
                                                            {{--</tr>--}}
                                                            {{--<tr>--}}
                                                                {{--<td class="text-semibold">District</td>--}}
                                                                {{--<td class="text-right"></td>--}}
                                                            {{--</tr>--}}
                                                            {{--<tr>--}}
                                                                {{--<td class="text-semibold">State</td>--}}
                                                                {{--<td class="text-right"></td>--}}
                                                            {{--</tr>--}}
                                                            {{--<tr>--}}
                                                                {{--<td class="text-semibold"> Pin Code</td>--}}
                                                                {{--<td class="text-right"></td>--}}
                                                            {{--</tr>--}}
                                                            {{--<tr>--}}
                                                                {{--<td class="text-semibold"> Tel. No.</td>--}}
                                                                {{--<td class="text-right"></td>--}}
                                                            {{--</tr>--}}
                                                            {{--<tr>--}}
                                                                {{--<td class="text-semibold"> Fax No.</td>--}}
                                                                {{--<td class="text-right"></td>--}}
                                                            {{--</tr>--}}
                                                            {{--<tr>--}}
                                                                {{--<td class="text-semibold">Email : </td>--}}
                                                                {{--<td class="text-right">Internet :</td>--}}
                                                            {{--</tr>--}}
                                                            {{--</tbody>--}}
                                                        {{--</table>--}}
                                                    {{--</div>--}}
                                                    <div class="row container-fluid">
                                                    </div>

                                                    <h6 class="text-semibold">Registrars</h6>

                                                    <div class="table-responsive content-group col-lg-6">
                                                        <table class="table table-framed">
                                                            <tbody>
                                                            <tr>
                                                                <td><span class="text-semibold">Name</span></td>
                                                                <td>
                                                                    <div class="input-group input-group-transparent">
                                                                        {{$company['registrar']['agency_name']}}
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><span class="text-semibold">Address</span></td>
                                                                <td>
                                                                    <div class="input-group input-group-transparent">
                                                                        {{$company['registrar']['address']}} {{$company['registrar']['address2']}} {{$company['registrar']['address3']}} {{$company['registrar']['address4']}} {{$company['registrar']['city_name']}} - {{$company['registrar']['pin_code']}}, {{$company['registrar']['state_name']}}
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><span class="text-semibold">Tel. No.</span></td>
                                                                <td>
                                                                    <div class="input-group input-group-transparent">
                                                                        {{$company['registrar']['telephone_numbers']}}
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><span class="text-semibold">Fax No.</span></td>
                                                                <td>
                                                                    <div class="input-group input-group-transparent">
                                                                        {{$company['registrar']['fax']}}
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><span class="text-semibold">Email</span></td>
                                                                <td>
                                                                    <div class="input-group input-group-transparent">
                                                                        {{$company['registrar']['email_id']}}
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><span class="text-semibold">Internet</span></td>
                                                                <td>
                                                                    <div class="input-group input-group-transparent">
                                                                        {{$company['registrar']['website']}}
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="basic-tab8">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h6 class="panel-title">Listing Details - {{$company['name']}}</h6>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table text-nowrap">
                                                <thead>
                                                <tr>
                                                    <th style="width: 300px;">Listing Information</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach( $company['listing_master'] as $group => $controllers)
                                                    <tr>
                                                        <td>
                                                            <div class="media-body">
                                                                <span class="text-semibold">BSE Code : </span>{{$controllers['scrip_code1_given_by_exchange']}}
                                                                <div class="text-size-small"><span class="text-semibold"> NSE Code : </span> {{$controllers['scrip_code2_given_by_exchange']}}</div>
                                                                <div class="text-size-small"><span class="text-semibold">BSE Group : </span> {{$controllers['exchange_group']}}</div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="basic-tab9">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h6 class="panel-title">Location Details - {{$company['name']}}</h6>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table text-nowrap">
                                                <thead>
                                                <tr>
                                                    <th style="width: 300px;">Location Type</th>
                                                    <th>Address</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach( $company['location'] as $group => $controllers)
                                                <tr>
                                                    <td class="text-center">
                                                        <div class="media-left media-middle">
                                                                {{$controllers['address_type']}}
                                                        </div>
                                                    </td>
                                                    <td>
                                                            <span class="text-semibold"> {{$controllers['address']}}{{$controllers['address2']}}{{$controllers['address3']}}{{$controllers['address4']}}</span>
                                                            <span class="display-block text-semibold">{{$controllers['city_name']}} - {{$controllers['pin_code']}}</span>
                                                            <span class="display-block text-semibold">{{$controllers['state_name']}}</span>
                                                            <span class="display-block text-semibold">Phone : {{$controllers['telephone_numbers']}}</span>
                                                            <span class="display-block text-semibold">Fax : {{$controllers['fax']}}</span>
                                                            <span class="display-block text-semibold">Email : {{$controllers['email_id']}}</span>
                                                            <span class="display-block text-semibold">Internet : {{$controllers['website']}}</span>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="basic-tab10">
                                    <div class="content-wrapper">
                                        <!-- Page header -->
                                        <div class="page-header">
                                        </div>
                                        <!-- /page header -->
                                        <!-- Content area -->
                                        <div class="content">

                                            <!-- Basic modals -->
                                            <div class="panel panel-flat">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title">Raw Materials</h5>
                                                </div>

                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>Product Name</th>
                                                            <th>Unit</th>
                                                            <th>Quantity</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach( $company['raw_material'] as $group => $controllers)
                                                        <tr>
                                                            <td style="width: 20%;">{{$controllers['product_name']}}</td>
                                                            <td >{{$controllers['raw_mat_uom']}}</td>
                                                            <td>{{$controllers['raw_mat_quantity']}}</td>
                                                        </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- /content area -->

                                    </div>
                                </div>

                                <div class="tab-pane" id="basic-tab11">
                                    <div class="col-sm-12">

                                        <table class="table table-hover tablet">
                                            <thead>
                                            <tr class="pt-10">
                                                <th colspan="3">Ticker Name</th>
                                                <th colspan="3">Last Trade Time</th>
                                                <th colspan="3">Last Trade Price </th>
                                                <th colspan="3">High Price</th>
                                                <th colspan="3">Low Price</th>
                                                <th colspan="3">Open Price</th>
                                                <th colspan="3">Close Price</th>
                                                <th colspan="3">Bid Price</th>
                                                <th colspan="3">Offer Price</th>
                                            </tr>
                                            </thead>
                                            @foreach($bse as $group => $controllers)
                                                <tbody>
                                                <tr>
                                                    <td colspan="3">{{ $controllers['ticker_name'] }}</td>
                                                    <td colspan="3">{{ date('d-m-Y', strtotime($controllers['last_trade_time'])) }}</td>
                                                    <td colspan="3">{{ $controllers['last_trade_price'] }}</td>
                                                    <td colspan="3">{{ $controllers['high_price'] }}</td>
                                                    <td colspan="3">{{ $controllers['low_price'] }}</td>
                                                    <td colspan="3">{{ $controllers['open_price'] }}</td>
                                                    <td colspan="3">{{ $controllers['close_price'] }}</td>
                                                    <td colspan="3">{{ $controllers['bid_price'] }}</td>
                                                    <td colspan="3">{{ $controllers['offer_price'] }}</td>
                                                </tr>
                                                </tbody>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="basic-tab12">
                                    <div class="col-sm-12">

                                        <table class="table table-hover tablet">
                                            <thead>
                                            <tr class="pt-10">
                                                <th colspan="3">Ticker Name</th>
                                                <th colspan="3">Last Trade Time</th>
                                                <th colspan="3">Last Trade Price </th>
                                                <th colspan="3">High Price</th>
                                                <th colspan="3">Low Price</th>
                                                <th colspan="3">Open Price</th>
                                                <th colspan="3">Close Price</th>
                                                <th colspan="3">Best Buy Price</th>
                                                <th colspan="3">Best Sell Price</th>
                                            </tr>
                                            </thead>
                                            @foreach( $nse['0'] as $group => $controllers)
                                                <tbody>
                                                <tr>
                                                    <td colspan="3">{{ $controllers['ticker_name'] }}</td>
                                                    <td colspan="3">{{ date('d-m-Y', strtotime($controllers['last_trade_time'])) }}</td>
                                                    <td colspan="3">{{ $controllers['last_traded_price'] }}</td>
                                                    <td colspan="3">{{ $controllers['high_price'] }}</td>
                                                    <td colspan="3">{{ $controllers['low_price'] }}</td>
                                                    <td colspan="3">{{ $controllers['open_price'] }}</td>
                                                    <td colspan="3">{{ $controllers['close_price'] }}</td>
                                                    <td colspan="3">{{ $controllers['best_buy_price'] }}</td>
                                                    <td colspan="3">{{ $controllers['best_sell_price'] }}</td>
                                                </tr>
                                                </tbody>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>

@endsection

