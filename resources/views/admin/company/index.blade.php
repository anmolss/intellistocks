@extends('admin.layout.app')

@section('page_title', 'Company')
@section('page_subtitle', 'List of company')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="over-title margin-bottom-15">
                Company
            </h5>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-hover" id="sample-table-2">
                <thead>
                <tr>
                    <th>Company_code</th>
                    <th>company_name</th>
                </tr>
                </thead>
                <tbody>
                @if(count($list) > 0)
                    @foreach($list as  $key => $item)
                        <tr>
                            <td>
                                <a href="{{route('admin::company.show', ['companies' => $item->id])}}" tooltip-placement="top" tooltip="Edit"  class="text-primary-600">{{ $item->id }}</a>
                            </td>
                            <td>{{ $item->name }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" align="center">No Roles Found</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection