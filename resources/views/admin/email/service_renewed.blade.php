<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
  <table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto;" width="680">
    <tbody>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <table cellpadding="0" cellspacing="0" style="border: 1px solid #ccc; width: 100%;">
            <tbody>
              <tr>
                <th style="background: #13466d; color: #fff; font-size: 18px; padding: 5px; text-align: left;">Your Service "{{json_decode($user_service->service_details, true)['name']}}" has been Renewed.</th>
              </tr>
              <tr>
                <td style="padding: 10px;">
                  <h2>Hi! {{$user->name}}</h2>
                  <p>"{{$user_by->name}}" Has Renewed Your Service - "{{json_decode($user_service->service_details, true)['name']}}".</p>
                </td>
              </tr>
              <tr>
                <td style="padding: 10px;">
                This is an auto generated message, please do not reply to this mail.​In case you need any further assistance. You can contact us either at 09015 310123 or mail us at: <a href="mailto:services@rothmanscapital.com">services@rothmanscapital.com</a>
                  <br>
                  Happy Earning !!!<br>
                  Team Rothmans Capital
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td style="padding: 5px; text-align: justify; font-size: 10px;">
          <p><strong>Standard Disclaimer:</strong><br>
          Stock trading is inherently risky and you agree to assume complete and full responsibility for the outcomes of all trading decisions that you make, including but not limited to loss of capital. None of the stock trading calls made by Rothmans Capital, its associate companies or analysts,it should be construed as an offer to buy or sell securities, nor advice to do so. All comments and posts made by Rothmans Capital, group companies associated with it and employees/owners are for information purposes only and under no circumstances should be used for actual trading. Under no circumstances should any person at this site make trading decisions based solely on the information discussed herein. We are not a qualified financial advisor and you should not construe any information discussed herein to constitute investment advice. It is informational in nature.<br>
          You should consult a qualified broker or other financial advisor prior to making any actual investment or trading decisions. You agree to not make actual stock trades based on comments on the site, nor on any techniques presented nor discussed in this site or any other form of information presentation. All information is for educational and informational use only. You agree to consult with a registered investment advisor, which we are not, prior to making any trading decision of any kind. Hypothetical or simulated performance results have certain inherent limitations. Unlike an actual performance record, simulated results do not represent actual trading. No representation is being made that any account will or is likely to achieve profits or losses similar to those shown.<br>
          Every effort has been made to accurately represent our service(s) and its potential. Any claims made or examples given are just creative/marketing tools, they should not be relied on in any way in making a decision whether or not to purchase. Any testimonials and examples used are exceptional results or creative thought work, don't apply to the average trader and are not intended to represent or guarantee that anyone will achieve the same or similar results. Each individual's success depends on his or her background, dedication, discipline and motivation--as well as other factors not always known and sometimes out beyond your control. There is no guarantee you will duplicate the results stated here. You recognize stock markets has inherent risk for loss of capital. By using this website or subscribing to our services, you agree to all terms. Please read all agreements, notices and disclaimers before subscribing our services.<br>
          Any reference or income examples of our businesses, incomes, results, and/or examples of others results or incomes on any of our web pages, in our products and/or services are not typical, they are exceptional results, which do not apply to the average person and are not intended to represent or guarantee that anyone will achieve the same or similar results. The results shown and reference should be considered by you as extra-ordinary.<br>
          ANY SERVICE ORDERS INCLUDING PREPAYMENTS GIVEN BY YOU IS STRICTLY ON NON REFUNDABLE BASIS We do not sell opportunities that guarantee profit. The income or financial examples given are in no way meant as a representation of actual or possible earnings, nor are they meant as an inducement, promise, guarantee or prediction of income of any kind. if you use information provided by us, we have no idea how much money you'll make or if you'll make any money at all. We are not promising, predicting, implying or even hinting that you'll make any money. You may even lose money. Stock trading involves high degree of risk. It's up to you to decide what's right for you. All we can do is share what works for us. It's up to you to apply any information we share to your businesses.</p>
        </td>
      </tr>
    </tbody>
  </table>
</body>
</html>