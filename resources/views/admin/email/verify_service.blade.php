<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
  <table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto;" width="680">
    <tbody>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <p>Welcome and thank you for subscribing with us and making us a part of your wealth creation process. Our goal is to help you make more money and find opportunities that multiply your wealth. We promise you will love your Rothmans experience so let&rsquo;s get started.</p>
          <p>Here are the details for your subscription:-</p>
          <table cellpadding="0" cellspacing="0" style="border: 1px solid #ccc; width: 100%;">
            <tbody>
              <tr>
                <th style="background: #13466d; color: #fff; font-size: 18px; padding: 5px; text-align: left;">Confirmation for {{json_decode($user_service->service_details, true)['name']}}</th>
              </tr>
              <tr>
                <td style="padding: 10px;">
                  <h2>Hi! {{$user->name}}</h2>
                  <p>Dear {{$user->name}} we have processed your subscription. You are requested to confirm your following details to enable us to activate your services.</p>
                  <table cellpadding="5" cellspacing="0" style="border: 1px solid #ccc; width: 100%;">
                    <tbody>
                      <tr style="background: #eaeaea;">
                        <td>Name</td>
                        <td>{{$user->name}}</td>
                      </tr>
                      <tr>
                        <td>Mobile</td>
                        <td>{{$user->mobile}}</td>
                      </tr>
                      <tr style="background: #eaeaea;">
                        <td>Email</td>
                        <td>
                          <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                        </td>
                      </tr>
                      <tr>
                        <td>Address</td>
                        <td>{{$user->address1}}</td>
                      </tr>
                      <tr style="background: #eaeaea;">
                        <td>City</td>
                        <td>{{$user->city}}</td>
                      </tr>
                      <tr>
                        <td>State</td>
                        <td>{{$user->state}}</td>
                      </tr>
                      <tr style="background: #eaeaea;">
                        <td>Pincode</td>
                        <td>{{$user->zip_code}}</td>
                      </tr>
                      <tr>
                        <td>Service</td>
                        <td>{{json_decode($user_service->service_details, true)['name']}}</td>
                      </tr>
                      <tr style="background: #eaeaea;">
                        <td>Activation Date</td>
                        <td>{{$user_service->activation_date}}</td>
                      </tr>
                      <tr>
                        <td>Comments</td>
                        <td>{{$user_service->comments}}</td>
                      </tr>
                    </tbody>
                  </table>
                  <p>Thank you for choosing Rothmans Capital. Kindly click the below button for verification.</p>
                  <p align="center"><a href="{!! route('auth::services.verify_mail', $user->verification_code) !!}" rel="nofollow" style="color: #fff; background-color: #337ab7; border-color: #2e6da4; padding: 6px 12px; font-size: 14px; border: 1px solid transparent; text-decoration: none;">I Agree, Confirm</a></p>
                </td>
              </tr>
              <tr>
                <td style="padding: 10px;">
                  We are here to answer your questions and we genuinely value user feedback. So please do not hesitate to contact us at ​ 08527869576 or ​ <a href="mailto:services@rothmanscapital.com">services@rothmanscapital.com</a> even if it is just to say Hi.<br>
                  <br>
                  Always at your service<br>
                  Team Rothmans
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td style="padding: 5px; text-align: justify; font-size: 10px;">
          <p><strong>Standard Disclaimer:</strong><br>
          Stock trading is inherently risky and you agree to assume complete and full responsibility for the outcomes of all trading decisions that you make, including but not limited to loss of capital. None of the stock trading calls made by Rothmans Capital, its associate companies or analysts,it should be construed as an offer to buy or sell securities, nor advice to do so. All comments and posts made by Rothmans Capital, group companies associated with it and employees/owners are for information purposes only and under no circumstances should be used for actual trading. Under no circumstances should any person at this site make trading decisions based solely on the information discussed herein. We are not a qualified financial advisor and you should not construe any information discussed herein to constitute investment advice. It is informational in nature.<br>
          You should consult a qualified broker or other financial advisor prior to making any actual investment or trading decisions. You agree to not make actual stock trades based on comments on the site, nor on any techniques presented nor discussed in this site or any other form of information presentation. All information is for educational and informational use only. You agree to consult with a registered investment advisor, which we are not, prior to making any trading decision of any kind. Hypothetical or simulated performance results have certain inherent limitations. Unlike an actual performance record, simulated results do not represent actual trading. No representation is being made that any account will or is likely to achieve profits or losses similar to those shown.<br>
          Every effort has been made to accurately represent our service(s) and its potential. Any claims made or examples given are just creative/marketing tools, they should not be relied on in any way in making a decision whether or not to purchase. Any testimonials and examples used are exceptional results or creative thought work, don't apply to the average trader and are not intended to represent or guarantee that anyone will achieve the same or similar results. Each individual's success depends on his or her background, dedication, discipline and motivation--as well as other factors not always known and sometimes out beyond your control. There is no guarantee you will duplicate the results stated here. You recognize stock markets has inherent risk for loss of capital. By using this website or subscribing to our services, you agree to all terms. Please read all agreements, notices and disclaimers before subscribing our services.<br>
          Any reference or income examples of our businesses, incomes, results, and/or examples of others results or incomes on any of our web pages, in our products and/or services are not typical, they are exceptional results, which do not apply to the average person and are not intended to represent or guarantee that anyone will achieve the same or similar results. The results shown and reference should be considered by you as extra-ordinary.<br>
          ANY SERVICE ORDERS INCLUDING PREPAYMENTS GIVEN BY YOU IS STRICTLY ON NON REFUNDABLE BASIS We do not sell opportunities that guarantee profit. The income or financial examples given are in no way meant as a representation of actual or possible earnings, nor are they meant as an inducement, promise, guarantee or prediction of income of any kind. if you use information provided by us, we have no idea how much money you'll make or if you'll make any money at all. We are not promising, predicting, implying or even hinting that you'll make any money. You may even lose money. Stock trading involves high degree of risk. It's up to you to decide what's right for you. All we can do is share what works for us. It's up to you to apply any information we share to your businesses.</p>
        </td>
      </tr>
    </tbody>
  </table>
</body>
</html>