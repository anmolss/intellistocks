@extends('admin.layout.app')

@section('page_title', 'Stock Watchlist')
@section('page_subtitle', 'Manage Stock Watchlist')


@section('content')
    <div class="row" ng-controller="servicesController as servCtrl" ng-cloak>
        @if(BootForm::open())
        @endif
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Select Stock for watchlist</h6>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4 form-group">
                        <label class="control-label">Symbol <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-line-chart"></i></span>
                            <input type="text" name="symbol" ng-readonly="asyncSelected_temp.exchange"  ng-model="asyncSelected_temp" placeholder="Please start type stock name"
                                   uib-typeahead="address as address.symbol for address in getStocks($viewValue)" typeahead-loading="loadingLocations"
                                   typeahead-min-length="3" typeahead-no-results="noResults" class="form-control" typeahead-template-url="customTemplate.html"
                                   typeahead-wait-ms="100">

                            <span class="input-group-addon" ng-hide="asyncSelected_temp.exchange"><i ng-show="loadingLocations" class="icon-spinner2 spinner"></i></span>
                            <a class="input-group-btn" ng-show="asyncSelected_temp.exchange" ng-click="asyncSelected_temp = null"><i class="icon-cross2 text-danger"></i></a>
                        </div>
                        <div ng-show="noResults">
                            <i class="glyphicon glyphicon-remove"></i> No Results Found
                        </div>

                        <script type="text/ng-template" id="customTemplate.html">
                            <a>
                                <span ng-bind-html="match.model.company | uibTypeaheadHighlight:query"></span> ( <abbr class="text-size-mini" ng-bind-html="match.model.symbol | uibTypeaheadHighlight:query"></abbr> )
                                <br>
                                <strong class="pull-left text-size-small" ng-bind-html="match.model.exchange | uppercase | uibTypeaheadHighlight:query"></strong>
                                <small ng-if="match.model.instrument !== ''" class="pull-right text-size-small ">Instrument: <i ng-bind-html="match.model.instrument| uppercase  | uibTypeaheadHighlight:query"></i></small>
                                <small ng-if="match.model.price !== ''" class="pull-right text-size-small ">Price: <i ng-bind-html="match.model.price | currency:'&#8377;':2"></i></small>
                            </a>
                        </script>
                    </div>

                    <div class="col-md-6"  ng-show="asyncSelected_temp.exchange && instrument !== 'stock'">
                        <div class="form-group col-md-4">
                            <label class="control-label" for="payment_mode">Expiry Date<span class="text-danger">*</span></label>
                            <select name="expiry" ng-options="e as e.name for e in expiries track by e.id" ng-model="expiry" ng-disabled="expiries.length == 0" class="form-control">
                                <option value="">Select Expiry</option>
                            </select>
                        </div>

                        <div ng-hide="instrument!='option'">
                            {!! BootForm::select('Type <span class="text-danger">*</span>', 'option_type', config('rothmans.sd_option_type'))->attribute('ng-model', "option_type")->attribute('ng-disabled', "instrument!='option' || expiry == null")->addGroupClass('col-md-4') !!}
                            <div class="form-group col-md-4">
                                <label class="control-label" for="payment_mode">Strike<span class="text-danger">*</span></label>
                                <select name="strike" ng-options="s as s.name for s in strikes track by s.id" ng-model="strike" ng-disabled="strikes.length == 0" class="form-control">
                                    <option value="">Select Strike</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-2 form-group mt-10">
                        <button type="button" class="btn btn-primary mt-20" ng-disabled="symbolObj == null" ng-click="addToWatchlist()">Add To WatchList</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title text-light">Stock List</h6>
            </div>
            <table class="table table-sm table-responsive">
                <thead>
                    <tr>
                        <th>Service Name</th>
                        <th>Exchange</th>
                        <th>CMP</th>
                        <th>Gain/ Loss</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody >
                    <tr ng-repeat="stock in stocks track by $index">
                        <th>@{{ stock.symbol }}</th>
                        <td>
                            <strong>@{{ stock.exchange | uppercase }} </strong>
                            <small ng-if="stock.exchange == 'nfo'">
                                (@{{ stock.instrument | uppercase }}, @{{ stock.expiry | date:"dd-MM-yyyy" }}
                                <span ng-if="stock.instrument == 'OPTSTK' || stock.instrument == 'OPTIDX'">
                                    , @{{ stock.option_type | uppercase }}, @{{ stock.strike }}
                                </span>)
                            </small>
                        </td>
                        <td>@{{ stock.cmp | currency:"&#8377; " }}</td>
                        <td>
                            <span class="text-success-600" ng-if="stock.cmp - stock.prev_close > 0"><i class="icon-stats-growth2 position-left"></i> @{{ (stock.cmp - stock.prev_close) | number}}</span>
                            <span class="text-danger" ng-if="stock.cmp - stock.prev_close < 0"><i class="icon-stats-decline2 position-left"></i> @{{ (stock.cmp - stock.prev_close) | number}}</span>
                        </td>
                        <td><a ng-click="remove_watchlist($index, stock)" class="text-danger"><i class="icon icon-cross"></i> </a></td>
                    </tr>
                    <tr ng-show="!stocks.length">
                        <td colspan="5">No Stocks in Watchlist</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    @parent

    <script type="text/javascript">

        app.controller('servicesController',function($scope, $http, sweet ){
            var servCtrl = this;

            $scope.getStocks = function(val, no_filter){
                var instrument = '';
                if(!no_filter){
                    no_filter = false;
                } else {
                    instrument = $scope.nfo_instrument;
                }

                return $http.post('{{route('admin::stock_watchlist.get_all_stock')}}', {
                    type: val, instrument: instrument ,no_group: no_filter
                }).then(function successCallback(response) {
                    if(no_filter)
                    {
                        $scope.stock_list = response.data;
                        return [];
                    }
                    else
                    {
                        return _.map(response.data, function (item) {
                            return {
                                symbol: item.symbol,
                                price: item.price,
                                exchange: item.exchange,
                                instrument: item.instrument,
                                company: item.company
                            };
                        });
                    }
                }, function errorCallback(response) {
                    $scope.asyncSelected_temp = null;
                    $scope.symbolObj = null;
                });
            };




            $scope.getNfoStock = function(){
                if($scope.nfo_instrument == '' || $scope.asyncSelected_temp.symbol == ''){
                    return;
                }

                var data = { symbol: $scope.asyncSelected_temp.symbol, nfo_instrument: $scope.nfo_instrument, expiry_date: $scope.expiry.id };
                if($scope.instrument == 'option'){
                    data = {symbol: $scope.asyncSelected_temp.symbol, nfo_instrument: $scope.nfo_instrument, expiry_date: $scope.expiry.id, strike: $scope.strike.id, option_type: $scope.option_type };
                }

                $http.post('{{url('service_delivery/get_nfo_stocks')}}', data)
                    .then(function successCallback(response) {
                        $scope.symbolObj = response.data;
                    }, function errorCallback(response) {
                        $scope.asyncSelected_temp = null;
                        $scope.symbolObj = null;
                    });
            };
            $scope.addToWatchlist = function(){
                if(!$scope.symbolObj){
                    return false;
                }

                var expiry = $scope.expiry.id;
                var nfo_instrument = $scope.nfo_instrument;
                var strike = $scope.strike.id;
                var option_type = ($scope.option_type == null) ? '' : $scope.option_type;

                $http.post('{{route('admin::stock_watchlist.store')}}',{
                    symbol: $scope.symbolObj.symbol, exchange: $scope.exchange, instrument: nfo_instrument, expiry: expiry, strike: strike, option_type: option_type
                }).then(function successCallback(response) {
                    if(response.data.type ==='success') {
                        $scope.stocks.push(response.data.stock);
                        sweet.show('Success',response.data.msg, response.data.type);
                    } else {
                        sweet.show('Error',response.data.msg, response.data.type);
                    }

                    $scope.instrument = '';
                    $scope.exchange = '';
                    $scope.nfo_instrument = '';
                    $scope.symbolObj = null;
                    $scope.asyncSelected_temp = '';
                    $scope.option_type = null;
                    $scope.strikes = null;
                    $scope.strike = $scope.default_strike;
                    $scope.expiry = {id:''};
                    $scope.expiries = null;

                }, function errorCallback(response) {
                    sweet.show('Error Occured!','Please try again.', 'error');
                });
            };

            $scope.remove_watchlist= function($index, stock){
                sweet.show({
                    title: 'Are you sure?',
                    text: 'You wanted to remove stock <b>'+ stock.symbol + "<b> from watchlist?",
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Remove From Watchlist',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    showLoaderOnConfirm: true,
                    html: true
                }, function(isConfirm) {
                    if(isConfirm) {
                        return $http.post('{{route('admin::stock_watchlist.destroy')}}',{
                            id: stock.id
                        }).then(function successCallback(response) {
                            if(response.data.deleted == true){
                                $scope.stocks.splice($index,1);
                                sweet.show('Success','Stock successfully removed from Watchlist', 'success');
                            } else {
                                sweet.show('Error Occured! ','Please try again.', 'error');
                            }
                        }, function errorCallback(response) {
                            sweet.show('Error Occured!','Please try again.', 'error');
                        });
                    } else {
                        return false;
                    }
                });
            };


            /// varibles
            $scope.asyncSelected_temp = '';
            $scope.instrument = '';
            $scope.exchange = '';
            $scope.nfo_instrument = '';
            $scope.expiry = {id:''};
            $scope.default_strike = {id:0};
            $scope.stocks = {!! json_encode($stocks) !!};

            // watch
            $scope.$watch('asyncSelected_temp', function(newval) {
                if(newval !== null && typeof newval === 'object')
                {
                    switch (newval.instrument) {
                        case 'FUTIDX':
                        case 'FUTIVX':
                        case 'FUTSTK':
                            $scope.instrument = 'future';
                            break;
                        case 'OPTIDX':
                        case 'OPTSTK':
                            $scope.instrument = 'option';
                            break;
                        default:
                            $scope.instrument = 'stock';
                    }

                    $scope.symbolObj = null;

                    $scope.exchange = newval.exchange;
                    $scope.nfo_instrument = newval.instrument;

                    if(newval.exchange == 'nfo'){
                        $scope.getStocks( newval.symbol, true);
                    }
                    if(newval.exchange != 'nfo'){
                        $scope.symbolObj = $scope.asyncSelected_temp;
                    }

                }
                else {
                    $scope.instrument = '';
                    $scope.exchange = '';
                    $scope.nfo_instrument = '';
                    $scope.symbolObj = null;
                    $scope.option_type = null;
                    $scope.strikes = null;
                    $scope.strike = $scope.default_strike;
                    $scope.expiry = {id:''};
                    $scope.expiries = null;
                }
            });

            $scope.$watch('stock_list', function(){
                if($scope.stock_list) {
                    $scope.expiries =  _.map(_.uniqBy($scope.stock_list, 'expiry_date'), function (item) {
                        return {
                            id: item.expiry_date,
                            name: item.expiry_date
                        };
                    });
                }
            });
            $scope.$watch('expiry', function(newval) {
                if(newval == null) {
                    $scope.option_type = null;
                    $scope.strikes = null;
                    $scope.strike = $scope.default_strike;
                }
                if($scope.instrument == 'future'){
                    $scope.getNfoStock();
                }
            });

            $scope.$watch('option_type', function(newval) {
                $scope.strikes = null;
                $scope.strike = $scope.default_strike;
                if(newval != null){
                    var strike = _.filter($scope.stock_list,function (item) {
                        return (item.option_type.toUpperCase() === $scope.option_type.toUpperCase()
                        & item.expiry_date.toUpperCase() === $scope.expiry.id.toUpperCase());
                    });

                    $scope.strikes =  _.map(strike, function (item) {
                        return {
                            id: item.strike,
                            name: item.strike
                        };
                    });
                }
            });
            $scope.$watch('strike', function(newval) {
                if(newval !== null && typeof newval === 'object')
                {
                    $scope.getNfoStock();
                }
            });
        });

    </script>

@endsection