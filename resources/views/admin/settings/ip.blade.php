@extends('admin.layout.app')

@section('page_title', 'IP Management')
@section('page_subtitle', 'Update Ip restriction')


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Role <small class="text-bold">Add</small>
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                <ul class="nav nav-tabs nav-tabs-bottom">
                    <li @if($type=='global')class="active" @endif><a href="#ip_global" onclick="update_url('{{route('admin::manage_ip.show', ['type'=>'global'], false)}}')" data-toggle="tab">Global Restrictions <i class="icon-menu7 position-left"></i></a></li>
                    <li @if($type=='users')class="active" @endif><a href="#ip_users" onclick="update_url('{{route('admin::manage_ip.show', ['type'=>'users'], false)}}')" data-toggle="tab">User Level Restriction <i class="icon-user position-left"></i></a></li>
                    <li @if($type=='teams')class="active" @endif><a href="#ip_teams" onclick="update_url('{{route('admin::manage_ip.show', ['type'=>'teams'], false)}}')" data-toggle="tab">Team Level Restriction <i class="icon-users position-left"></i></a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane  @if($type=='global')active @endif" id="ip_global">
                        @include('admin.settings.ip.global')
                    </div>

                    <div class="tab-pane @if($type=='users')active @endif" id="ip_users">
                        @include('admin.settings.ip.users')
                    </div>

                    <div class="tab-pane @if($type=='teams')active @endif" id="ip_teams">
                        @include('admin.settings.ip.teams')
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    @parent

    <script type="text/javascript">
        function update_url(url)
        {
            window.history.pushState('object or string', 'Title', url);
        }
    </script>
@endsection