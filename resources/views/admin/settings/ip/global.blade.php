<div class="row" ng-controller="globalCtrl">
{!! BootForm::open()->action(route('admin::manage_ip.show', ['type'=>'global'])) !!}
    <div class="row">
        <div class="col-md-9">
            <label class="control-label" for="ip_restrict">Global IP Restriction</label>
            <ui-select multiple tagging-label="(Add IP)"
                       tagging
                       ng-model="$parent.ips"
                       sortable="true"
                       title="Add new IP">
                <ui-select-match placeholder="Add Ips...">@{{$item}}</ui-select-match>
                <ui-select-choices repeat="ip in $parent.ips track by $index"
                                   refresh="selectIpFunc($select)"
                                   refresh-delay="20">
                    @{{ip}}
                </ui-select-choices>
            </ui-select>
            <ol><li>Empty means allow all IP's.</li><li>Please use semicolon for multiple entries</li></ol>
            {!! BootForm::hidden("ip")->attribute('ng-value', 'global_ips') !!}
        </div>
        <div class="col-md-3">
            {!! BootForm::submit('Update <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right') !!}
        </div>
    </div>
{!! BootForm::close() !!}
</div>





@section('scripts')
    @parent

    <script type="text/javascript">
        app.controller('globalCtrl', function($scope, $http) {
            $scope.disabled = undefined;
            $scope.searchEnabled = undefined;
            $scope.team_id = $scope.team_ips = undefined;
            $scope.searchRes = [];
            $scope.ips = {!! json_encode($global_ip) !!};
            $scope.selectIpFunc = function($select) {
                _.remove($select.selected, function (ip) {
                    var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
                    return !(ip !== '' && ip !== undefined && ip.match(ipformat));
                });
            };

            $scope.$watch("ips", function (newValue) {
                $scope.global_ips = newValue.join(", ");
            });

        });
    </script>

@endsection
