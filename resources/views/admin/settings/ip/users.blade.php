<div class="row" ng-controller="usersCtrl">
    {!! BootForm::open()->action(route('admin::manage_ip.show', ['type'=>'users'])) !!}


    {!! BootForm::hidden("ip")->attribute('ng-value', 'user_ips') !!}
    {!! BootForm::hidden('user_id')->attribute('ng-value', 'user_id') !!}

    <div class="col-md-3">
        <div class="form-group">
            <label class="control-label" for="ip_restrict">Select user by Name, Mobile, Email and Username</label>
            <ui-select
                    ng-model="selectUser"
                    on-select="onSelectCallback($item, $model)"
                    data-ng-disabled="disabled"
                    ng-required="true">
                <ui-select-match placeholder="Search for Users">@{{$select.selected.name}}</ui-select-match>
                <ui-select-choices refresh="selectUserFunc($select)" refresh-delay="300" repeat="searchRes in searchRes">
                    <span ng-bind-html="searchRes.name | highlight: $select.search"></span>
                </ui-select-choices>
            </ui-select>
        </div>
    </div>
    <div class="col-md-6">
        <label class="control-label" for="ip_restrict">Add Ips</label>
        <ui-select multiple tagging-label="(Add IP)"
                   tagging
                   ng-model="$parent.ips"
                   sortable="true"
                   title="Add new IP">
            <ui-select-match placeholder="Add Ips...">@{{$item}}</ui-select-match>
            <ui-select-choices repeat="ip in $parent.ips track by $index"
                               refresh="selectIpFunc($select)"
                               refresh-delay="0">
                @{{ip}}
            </ui-select-choices>
        </ui-select>
    </div>

    <div class="col-md-3">
        {!! BootForm::submit('Update <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right') !!}
    </div>

    {!! BootForm::close() !!}
</div>



<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Name</th>
        <th>email</th>
        <th>Ips</th>
    </tr>
    </thead>
    <tbody>
    @if(count($user_list) > 0)
        @foreach($user_list as  $key => $item)
            <tr>
                <td>{{ $item->name }}</td>
                <td>{!! $item->email !!}</td>
                <td>{!! implode(", ",$item->ipList->pluck('ip')->toArray()) !!}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="6" align="center">No Users Found</td>
        </tr>
    @endif
    </tbody>
</table>

@section('scripts')
    @parent

    <script type="text/javascript">

        app.controller('usersCtrl', function($scope, $http) {
            $scope.disabled = undefined;
            $scope.searchEnabled = undefined;
            $scope.user_id = $scope.user_ips = undefined;
            $scope.searchRes = [];
            $scope.ips = [];

            $scope.onSelectCallback = function($item, $model) {
                $scope.ips = [];

                _.each($model.ip_list, function($item, $key){
                    $scope.ips.push($item.ip);
                });
                $scope.user_id = $model.id;
                $scope.user_ips = $scope.ips.join(", ");
            };


            $scope.selectUserFunc = function($select) {
                return $http.post('{{ route('admin::manage_ip.selectuser') }}', {
                    name: $select.search
                }).then(function(response){
                    $scope.searchRes = response.data;
                });
            };

            $scope.selectIpFunc = function($select) {
                _.remove($select.selected, function (ip) {
                    var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
                    return !(ip !== '' && ip !== undefined && ip.match(ipformat));
                });

                if($scope.ips.length )
                    $scope.user_ips = $scope.ips.join(", ");
            };

            $scope.$watch("ips", function (newValue) {
                $scope.user_ips = newValue.join(", ");
            });
        });
    </script>

@endsection