<fieldset>
    <div class="row">
        <div class="col-md-6">
            {!! BootForm::text('Token Name <span class="text-danger">*</span>', 'name')->required()!!}
            {!! BootForm::text('Content <span class="text-danger">*</span>', 'content')->required()!!}
            {!! BootForm::inlineCheckbox('Replaceable', 'is_replaceable')->value(1)->addClass('styled') !!}            
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div>
                <span class="text-danger">*</span> Required Fields
            </div>
        </div>
        <div class="col-md-6">
            {!! BootForm::submit($submitBtnText . ' <i class="fa fa-arrow-circle-right"></i>')->addClass('btn-primary pull-right') !!}
        </div>
    </div>
</fieldset>
@section('scripts')
   @parent
    <script type="text/javascript">
    $(".js-example-basic-multiple").select2();  
    </script>
@endsection
