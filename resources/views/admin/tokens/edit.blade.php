@extends('admin.layout.app')

@section('page_title', 'Token')
@section('page_subtitle', 'Edit token')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                Token <small class="text-bold">Edit</small>
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->put()->action(route('admin::tokens.update', ['tokens' => $item->id] ))->multipart() !!}
            {!! BootForm::bind($item) !!}
            
            @include('admin.tokens._form', ['submitBtnText' => "Edit Token"])

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection