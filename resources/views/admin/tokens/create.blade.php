@extends('admin.layout.app')

@section('page_title', 'Token')
@section('page_subtitle', 'Create new Token')


@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">
                <a href="javascript:window.history.back()" class="btn btn-primary pull-right text-white">Back <i class="fa fa-reply"></i> </a>
            </h6>
        </div>
        <div class="panel-body">
            {!! BootForm::open()->post()->action(route('admin::tokens.store'))->novalidate() !!}
                @include('admin.tokens._form', ['submitBtnText' => "Add New Token"])
            {!! BootForm::close() !!}
        </div>
    </div>

@endsection