@extends('admin.layout.app')

@section('page_title', 'Dashboard')
@section('page_subtitle', 'overview & stats')


@section('content')
    <div class="row">

        @if(!empty($graph_data_transaction))
            <div class="col-md-6">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Earnings Statistics</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-sm-12 text-center" id="earning_graph_loader">
                            <center><i class="icon-spinner2 spinner fa-2x"></i> Loading Graph Data</center>
                        </div>
                        <div class="chart-container text-center">
                            <div class="chart display-inline-block" id="google-column"></div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(!empty($graph_data_services))
            <div class="col-sm-6">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Service Usage Statistics</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-sm-12 text-cneter" id="service_graph_loader">
                            <center><i class="icon-spinner2 spinner fa-2x"></i> Loading Graph Data</center>
                        </div>
                        <div class="chart-container text-center">
                            <div class="display-inline-block" id="google-donut"></div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(!empty($graph_data_service_status))
            <div class="col-sm-6">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Service Status Wise Statistics</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-sm-12 text-cneter" id="service_status_graph_loader">
                            <center><i class="icon-spinner2 spinner fa-2x"></i> Loading Graph Data</center>
                        </div>
                        <div class="chart-container text-center">
                            <div class="display-inline-block" id="google-pie-3d"></div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(!empty($graph_data_plans))
            <div class="col-sm-6">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Plan Usage Statistics</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-sm-12 text-cneter" id="plan_graph_loader">
                            <center><i class="icon-spinner2 spinner fa-2x"></i> Loading Graph Data</center>
                        </div>
                        <div class="chart-container text-center">
                            <div class="display-inline-block" id="google-pie"></div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        {{--
        @if(!empty($inactive_user))
            <div class="col-md-6">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Inactive Users</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Username</th>
                                    <th>Created On</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($inactive_user as $inactiveList)
                                <tr>
                                    <td>{{$inactiveList->id}}</td>
                                    <td><a href="{{route('admin::users.edit', ['users' => $inactiveList->id])}}" tooltip-placement="top" tooltip="Edit"  class="text-primary-600">{{$inactiveList->username}}</a></td>
                                     <td>{{ Carbon::parse($inactiveList->created_at)->format('d-m-Y') }}</td>
                                    <td><span class="label label-danger">{{$inactiveList->status? "Active" : "Inactive"}}</span></td>
                                </tr>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
        --}}
        <div class="col-sm-12">
            <!-- Accordion with different panel styling -->
            <div class="panel-group panel-group-control panel-group-control-right" id="accordion-styled">
                @ability('super-admin', 'payment-statistics')
                <div class="panel">
                    <div class="panel-heading bg-teal-300">
                        <h6 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group1">Upcoming Payments ({{$upcoming_payments_count}})</a>
                        </h6>
                    </div>
                    <div id="accordion-styled-group1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-framed">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Customer</th>
                                            <th>Relationship Manager</th>
                                            <th>Service</th>
                                            <th>Amount</th>
                                            <th>Pay No.</th>
                                            <th>Date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($upcoming_payments as $no => $upcoming_payment)
                                            <tr>
                                                <td>{{$no+1}}</td>
                                                <td>
                                                    <ul>
                                                        <li>Name : <a href="{{ url('customers').'/'.$upcoming_payment->user_id }}">{{$upcoming_payment->name}}</a></li>
                                                        <li>Email : {{$upcoming_payment->email}}</li>
                                                        <li>Mobile : {{$upcoming_payment->mobile}}</li>
                                                    </ul>
                                                </td>
                                                <td>{{$upcoming_payment->rm_name}}</td>
                                                <td>{{json_decode($upcoming_payment->service_details, true)['name']}}</td>
                                                <td>Rs. {{round($upcoming_payment->amount, 2)}}</td>
                                                <td>
                                                    @if($upcoming_payment->pay_type == 'partial')
                                                        {{'Installment No. '.$upcoming_payment->pay_no}}
                                                    @else
                                                        {{'Full Pay'}}
                                                    @endif
                                                </td>
                                                <td>{{Carbon::parse($upcoming_payment->date)->toDateString() .' ('.Carbon::parse($upcoming_payment->date)->diffForHumans().')'}}</td>
                                                <td>
                                                    <ul class="icons-list">
                                                        <li><a href="{{ url('customers').'/'.$upcoming_payment->user_id.'?tab=email' }}" title="Mail"><i class="icon-mention"></i></a></li>
                                                        <li><a href="{{ url('customers').'/'.$upcoming_payment->user_id.'?tab=sms' }}" title="SMS"><i class="icon-envelop3"></i></a></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="9" class="text-center">No Data Available</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-heading bg-teal-300">
                        <h6 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion-styled" href="#lapsed_payments">Lapsed Payments ({{ count($lapsed_payments) }})</a>
                        </h6>
                    </div>
                    <div id="lapsed_payments" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-framed">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Customer</th>
                                        <th>Relationship Manager</th>
                                        <th>Service</th>
                                        <th>Amount</th>
                                        <th>Pay No.</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($lapsed_payments as $no => $upcoming_payment)
                                        <tr>
                                            <td>{{$no+1}}</td>
                                            <td>
                                                <ul>
                                                    <li>Name : <a href="{{ url('customers').'/'.$upcoming_payment->user_id }}">{{$upcoming_payment->name}}</a></li>
                                                    <li>Email : {{$upcoming_payment->email}}</li>
                                                    <li>Mobile : {{$upcoming_payment->mobile}}</li>
                                                </ul>
                                            </td>
                                            <td>{{$upcoming_payment->rm_name}}</td>
                                            <td>{{json_decode($upcoming_payment->service_details, true)['name']}}</td>
                                            <td>Rs. {{round($upcoming_payment->amount, 2)}}</td>
                                            <td>
                                                @if($upcoming_payment->pay_type == 'partial')
                                                    {{'Installment No. '.$upcoming_payment->pay_no}}
                                                @else
                                                    {{'Full Pay'}}
                                                @endif
                                            </td>
                                            <td>{{Carbon::parse($upcoming_payment->date)->toDateString() .' ('.Carbon::parse($upcoming_payment->date)->diffForHumans().')'}}</td>
                                            <td>
                                                <ul class="icons-list">
                                                    <li><a href="{{ url('customers').'/'.$upcoming_payment->user_id.'?tab=email' }}" title="Mail"><i class="icon-mention"></i></a></li>
                                                    <li><a href="{{ url('customers').'/'.$upcoming_payment->user_id.'?tab=sms' }}" title="SMS"><i class="icon-envelop3"></i></a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="9" class="text-center">No Data Available</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endability

                <div class="panel" ng-controller="userWatchlistController as servCtrl" ng-cloak ng-init="getUsers()">
                    <div class="panel-heading bg-teal-300">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group-watchlist">Watchlist Users (@{{ users.length | number }})</a>
                        </h6>
                    </div>
                    <div id="accordion-styled-group-watchlist" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-framed">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Service</th>
                                            <th>Plan</th>
                                            <th>Renewal Date</th>
                                            <th>Expiry</th>
                                            <th>Today's P&L</th>
                                            <th>Total P&L</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="user in users track by $index">
                                            <td>@{{ $index + 1 }}</td>
                                            <td><a href="{{ url('customers') }}/@{{ user.user_id }}">@{{ user.user_name }}</a></td></td>
                                            <td>@{{ user.email }}</td>
                                            <td>@{{ user.mobile }}</td>
                                            <td>@{{ user.service_details }}</td>
                                            <td>@{{ user.plan_details }}</td>
                                            <td>@{{ user.expiry_date }}</td>
                                            <td>@{{ user.days_left }}</td>
                                            <td>
                                                <span class="text-success-600" ng-if="user.pnl.total_profit >= 0"><i class="icon-stats-growth2 position-left"></i> @{{ user.pnl.total_profit | number }}</span>
                                                <span class="text-danger" ng-if="user.pnl.total_profit < 0"><i class="icon-stats-decline2 position-left"></i> @{{ user.pnl.total_profit | number }}</span>
                                            </td>
                                            <td>
                                                <span class="text-success-600" ng-if="user.pnl.today_pnl >= 0"><i class="icon-stats-growth2 position-left"></i> @{{ user.pnl.today_pnl | number }}</span>
                                                <span class="text-danger" ng-if="user.pnl.today_pnl < 0"><i class="icon-stats-decline2 position-left"></i> @{{ user.pnl.today_pnl | number }}</span>
                                            </td>

                                            <td>
                                                <ul class="icons-list">
                                                    <li><a href="{{ url('customers') }}/@{{ user.user_id }}?tab=email" title="Mail"><i class="icon-mention"></i></a></li>
                                                    <li><a href="{{ url('customers') }}/@{{ user.user_id }}?tab=sms" title="SMS"><i class="icon-envelop3"></i></a></li>
                                                    <li><a ng-click="remove_watchlist($index, user)" class="text-danger" title="Remove"><i class="icon icon-cross"></i> </a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr ng-show="!users.length">
                                            <td colspan="11" align="center">No Users in Watchlist</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel" ng-controller="stockWatchlistController as servCtrl" ng-cloak ng-init="getStocks()">
                    <div class="panel-heading bg-teal-300">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group-watchlist-stock">Watchlist Stocks (@{{ stocks.length | number }})</a>
                        </h6>
                    </div>
                    <div id="accordion-styled-group-watchlist-stock" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-framed">
                                    <thead>
                                    <tr>
                                        <th>Stock Name</th>
                                        <th>CMP</th>
                                        <th>Gain/loss</th>
                                        <th>Gain/loss (%)</th>
                                        <th>Stock high</th>
                                        <th>Stock low</th>
                                        <th>Services</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="stock in stocks track by $index">
                                            <th>@{{ stock.symbol }} <br>
                                                <strong>@{{ stock.exchange | uppercase }} </strong>
                                                <small ng-if="stock.exchange == 'nfo'">
                                                    (@{{ stock.instrument | uppercase }}, @{{ stock.expiry | date:"dd-MM-yyyy" }}
                                                    <span ng-if="stock.instrument == 'OPTSTK' || stock.instrument == 'OPTIDX'">
                                                        , @{{ stock.option_type | uppercase }}, @{{ stock.strike }}
                                                    </span>)
                                                </small>
                                            </td>
                                            <td>@{{ stock.cmp | currency:"&#8377;" }}</td>
                                            <td>
                                                <span class="text-success-600" ng-if="stock.gain_loss >= 0"><i class="icon-stats-growth2 position-left"></i> @{{ stock.gain_loss | number }}</span>
                                                <span class="text-danger" ng-if="stock.gain_loss < 0"><i class="icon-stats-decline2 position-left"></i> @{{ stock.gain_loss | number }}</span>
                                            </td>
                                            <td>
                                                <span class="text-success-600" ng-if="stock.gain_loss >= 0"><i class="icon-stats-growth2 position-left"></i> @{{ stock.gain_loss_p | number }}%</span>
                                                <span class="text-danger" ng-if="stock.gain_loss < 0"><i class="icon-stats-decline2 position-left"></i> @{{ stock.gain_loss | number }}%</span>
                                            </td>
                                            <td>@{{ stock.stock_high | currency:"&#8377;" }}<br>(@{{ stock.stock_high_date | date:"dd-MM-yyyy" }})</td>
                                            <td>@{{ stock.stock_low | currency:"&#8377;" }}<br>(@{{ stock.stock_low_date | date:"dd-MM-yyyy" }})</td>
                                            <td>
                                                <ul class="list list-condensed no-margin-bottom">
                                                    <li ng-repeat="service in stock.services">@{{ service }}</li>
                                                    <li ng-show="!stock.services.length">NA</li>
                                                </ul>
                                            </td>
                                            <td><a ng-click="remove_watchlist($index, stock)" class="text-danger"><i class="icon icon-cross"></i> </a></td>
                                        </tr>
                                        <tr ng-show="!stocks.length">
                                            <td colspan="8">No Stocks in Watchlist</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel" ng-controller="serviceWatchlistController as servCtrl" ng-cloak ng-init="getServices()">
                    <div class="panel-heading bg-teal-300">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group-watchlist-service">Watchlist Services (@{{ services.length | number }})</a>
                        </h6>
                    </div>
                    <div id="accordion-styled-group-watchlist-service" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-framed">
                                    <thead>
                                    <tr>
                                        <th>Service Name</th>
                                        <th>Value</th>
                                        <th>Gain/loss</th>
                                        <th>Gain/loss (%)</th>
                                        <th>Highest</th>
                                        <th>Lowest</th>
                                        <th>Today's Pnl</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="service in services track by $index">
                                            <th><a target="_blank" href="{{route('admin::services.index')}}/@{{service.id}}"> @{{ service.name }}</a></td>
                                            <td>
                                                <span ng-if="isNaN(service.cmp)">@{{ service.cmp | currency:"&#8377;" }}</span>
                                                <span ng-if="!isNaN(service.cmp)">@{{ service.cmp  }}</span>
                                            </td>
                                            <td>
                                                <span ng-if="isNaN(service.gain_loss)">@{{ service.gain_loss }}</span>
                                                <span ng-if="!isNaN(service.gain_loss)">
                                                    <span class="text-success-600" ng-if="service.gain_loss >= 0"><i class="icon-stats-growth2 position-left"></i> @{{ service.gain_loss | number }}</span>
                                                    <span class="text-danger" ng-if="service.gain_loss < 0"><i class="icon-stats-decline2 position-left"></i> @{{ service.gain_loss | number }}</span>
                                                </span>
                                            </td>
                                            <td>
                                                <span ng-if="isNaN(service.gain_loss_p)">@{{ service.gain_loss_p }}</span>
                                                <span ng-if="!isNaN(service.gain_loss_p)">
                                                    <span class="text-success-600" ng-if="service.gain_loss >= 0"><i class="icon-stats-growth2 position-left"></i> @{{ service.gain_loss_p | number }}%</span>
                                                    <span class="text-danger" ng-if="service.gain_loss < 0"><i class="icon-stats-decline2 position-left"></i> @{{ service.gain_loss_p | number }}%</span>
                                                </span>
                                            </td>
                                            <td>NA</td>
                                            <td>NA</td>
                                            <td>
                                                <span ng-if="isNaN(service.today_pnl)">@{{ service.today_pnl }}</span>
                                                <span ng-if="!isNaN(service.today_pnl)">
                                                    <span class="text-success-600" ng-if="service.today_pnl >= 0"><i class="icon-stats-growth2 position-left"></i> @{{ service.today_pnl | currency:'&#8377;':2 }}</span>
                                                    <span class="text-danger" ng-if="service.today_pnl < 0"><i class="icon-stats-decline2 position-left"></i> @{{ service.today_pnl | currency:'&#8377;':2 }}</span>
                                                </span>
                                            </td>
                                            <td><a ng-click="remove_watchlist($index, service)" class="text-danger"><i class="icon icon-cross"></i> </a></td>
                                        </tr>
                                        <tr ng-show="!services.length">
                                            <td colspan="8">No Service in your Watchlist</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-heading bg-teal-300">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group2">Upcoming Renewals ({{count($renewals)}})</a>
                        </h6>
                    </div>
                    <div id="accordion-styled-group2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-framed">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Relationship Manager</th>
                                            <th>Service</th>
                                            <th>Plan</th>
                                            <th>Renewal Date</th>
                                            <th>Days Left</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($renewals as $no => $renewal)
                                            <?php
                                                $activation_date = Carbon::parse($renewal->expire_date);
                                                $now = Carbon::now();
                                                $days_left = ($activation_date->diff($now)->days < 1) ? 'today' : $activation_date->diffForHumans($now);
                                            ?>
                                            <tr>
                                                <td>{{$no+1}}</td>
                                                <td><a href="{{ url('customers').'/'.$renewal->user_id }}">{{$renewal->user_name}}</a></td>
                                                <td>{{$renewal->email}}</td>
                                                <td>{{$renewal->mobile}}</td>
                                                <td>{{$renewal->rm_name}}</td>
                                                <td>{{json_decode($renewal->service_details)->name}}</td>
                                                <td>{{json_decode($renewal->plan_details)->name}}</td>
                                                <td>{{ Carbon::parse($renewal->expire_date)->format('d-m-Y') }}</td>
                                                <td>{{$days_left}}</td>
                                                <td>
                                                    <ul class="icons-list">
                                                        <li><a href="{{ url('customers').'/'.$renewal->user_id.'?tab=email' }}" title="Mail"><i class="icon-mention"></i></a></li>
                                                        <li><a href="{{ url('customers').'/'.$renewal->user_id.'?tab=sms' }}" title="SMS"><i class="icon-envelop3"></i></a></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="10" class="text-center">No Data Available</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-heading bg-teal-300">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group3">Lapsed Renewals ({{count($lapsed_renewals)}})</a>
                        </h6>
                    </div>
                    <div id="accordion-styled-group3" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-framed">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Relationship Manager</th>
                                            <th>Service</th>
                                            <th>Plan</th>
                                            <th>Renewal Date</th>
                                            <th>Expired Days</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($lapsed_renewals as $no => $lapsed_renewal)
                                            <?php
                                                $activation_date = Carbon::parse($lapsed_renewal->activation_date)->addDays(json_decode($lapsed_renewal->plan_details)->duration);
                                                $now = Carbon::now();
                                                $days_left = ($activation_date->diff($now)->days < 1) ? 'today' : $activation_date->diffForHumans($now);
                                            ?>
                                            <tr>
                                                <td>{{$no+1}}</td>
                                                <td><a href="{{ url('customers').'/'.$lapsed_renewal->user_id }}">{{$lapsed_renewal->user_name}}</a></td>
                                                <td>{{$lapsed_renewal->email}}</td>
                                                <td>{{$lapsed_renewal->mobile}}</td>
                                                <td>{{$lapsed_renewal->rm_name}}</td>
                                                <td>{{json_decode($lapsed_renewal->service_details)->name}}</td>
                                                <td>{{json_decode($lapsed_renewal->plan_details)->name}}</td>
                                                <td>{{ Carbon::parse($lapsed_renewal->activation_date)->addDays(json_decode($lapsed_renewal->plan_details)->duration)->format('d-m-Y') }}</td>
                                                <td>{{$days_left}}</td>
                                                <td>
                                                    <ul class="icons-list">
                                                        <li><a href="{{ url('customers').'/'.$lapsed_renewal->user_id.'?tab=email' }}" title="Mail"><i class="icon-mention"></i></a></li>
                                                        <li><a href="{{ url('customers').'/'.$lapsed_renewal->user_id.'?tab=sms' }}" title="SMS"><i class="icon-envelop3"></i></a></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="10" class="text-center">No Data Available</td>
                                            </tr>
                                        @endforelse
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-heading bg-teal-300">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group4">Pending Verification ({{count($pending_verifications)}})</a>
                        </h6>
                    </div>
                    <div id="accordion-styled-group4" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-framed">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Relationship Manager</th>
                                            <th>Service</th>
                                            <th>Activation Date</th>
                                            <th>Transcript Status</th>
                                            <th>KYC Status</th>
                                            <th>Consent Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($pending_verifications as $no => $pending_verification)
                                            <tr>
                                                <td>{{$no+1}}</td>
                                                <td><a href="{{ url('customers').'/'.$pending_verification->user_id }}">{{$pending_verification->user_name}}</a></td>
                                                <td>{{$pending_verification->rm_name}}</td>
                                                <td>{{json_decode($pending_verification->service_details)->name}}</td>
                                                <td>{{Carbon::parse($pending_verification->activation_date)->format('d-m-Y')}}</td>
                                                <td>{{$pending_verification->transcript_status}}</td>
                                                <td>{{$pending_verification->kyc_status}}</td>
                                                <td>{{$pending_verification->consent_status}}</td>
                                                <td>
                                                    <ul class="icons-list">
                                                        <li><a href="{{ url('customers').'/'.$pending_verification->user_id.'?tab=email' }}" title="Mail"><i class="icon-mention"></i></a></li>
                                                        <li><a href="{{ url('customers').'/'.$pending_verification->user_id.'?tab=sms' }}" title="SMS"><i class="icon-envelop3"></i></a></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="9" class="text-center">No Data Available</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel" ng-controller="pendingServiceActivation" ng-cloak ng-init="getUsers()">
                    <div class="panel-heading bg-teal-300">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion-styled" href="#pending_service_activation">Pending Service Activation (@{{ users.length | number }})</a>
                        </h6>
                    </div>
                    <div id="pending_service_activation" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-framed">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Relationship Manager</th>
                                        <th>Service</th>
                                        <th>Activation Date</th>
                                        <th>Reasons</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="user in users track by $index">
                                            <td>@{{ $index+1 }}</td>
                                            <td><a href="{{ url('customers').'/'}}@{{ user.user_id}}">@{{user.user_name}}</a></td>
                                            <td>@{{ user.rm_name }}</td>
                                            <td>@{{ user.service_details | valueFromJson:'name' }}</td>
                                            <td>@{{ user.activation_date | date:'dd-MM-yyyy'}}</td>
                                            <td class="p-5">
                                                <ul class="list-unstyled no-margin">
                                                    <li ng-show="user.payment_received == 0">
                                                        <div class="checkbox-inline">
                                                            <label class="control-label">Payment Received</label>
                                                        </div>
                                                    </li>
                                                    <li ng-show="user.portfolio_review == 0">
                                                        <div class="checkbox-inline">
                                                            <label class="control-label"><input apply-uniform type="checkbox" ng-click="updatePendings($index, user, 'portfolio_review')" ng-model="users[$index].portfolio_review">Portfolio Review</label>
                                                        </div>
                                                    </li>
                                                    <li ng-show="user.on_boarding == 0">
                                                        <div class="checkbox-inline">
                                                            <label class="control-label"><input apply-uniform type="checkbox" ng-click="updatePendings($index, user, 'on_boarding')" ng-model="users[$index].on_boarding">On Boarding</label>
                                                        </div>
                                                    </li>
                                                    <li ng-show="user.portfolio_shared == 0">
                                                        <div class="checkbox-inline">
                                                            <label class="control-label no-margin"><input apply-uniform type="checkbox" ng-click="updatePendings($index, user, 'portfolio_shared')" ng-model="users[$index].portfolio_shared">Share Portfolio</label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul class="icons-list">
                                                    <li><a href="{{ url('customers') }}/@{{ user.user_id }}?tab=email" title="Mail"><i class="icon-mention"></i></a></li>
                                                    <li><a href="{{ url('customers') }}/@{{ user.user_id }}?tab=sms" title="SMS"><i class="icon-envelop3"></i></a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr ng-show="!users.length">
                                            <td colspan="7">No Pending Users</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-heading bg-teal-300">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group5">Temporary Extentions ({{count($temp_ext)}})</a>
                        </h6>
                    </div>
                    <div id="accordion-styled-group5" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-framed">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Relationship Manager</th>
                                            <th>Service</th>
                                            <th>Plan</th>
                                            <th>Expiry date</th>
                                            <th>Days Extended</th>
                                            <th>Days Left</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($temp_ext as $no => $temp_extn)
                                            <?php
                                                $actual_expiry = Carbon::parse($temp_extn->activation_date)->addDays(json_decode($temp_extn->plan_details)->duration);
                                                $now = Carbon::now();
                                                $days_left = ($actual_expiry->diff($now)->days < 1) ? 'today' : $actual_expiry->diffForHumans($now);
                                            ?>
                                            <tr>
                                                <td>{{$no+1}}</td>
                                                <td><a href="{{ url('customers').'/'.$temp_extn->user_id }}">{{$temp_extn->user_name}}</a></td>
                                                <td>{{$temp_extn->email}}</td>
                                                <td>{{$temp_extn->mobile}}</td>
                                                <td>{{$temp_extn->rm_name}}</td>
                                                <td>{{json_decode($temp_extn->service_details)->name}}</td>
                                                <td>{{json_decode($temp_extn->plan_details)->name}}</td>
                                                <td>{{ $actual_expiry->format('d-m-Y') }}
                                                </td>
                                                <td>{{$temp_extn->extn_days}} Days</td>
                                                <td>{{$days_left}}</td>
                                                <td>
                                                    <ul class="icons-list">
                                                        <li><a href="{{ url('customers').'/'.$temp_extn->user_id.'?tab=email' }}" title="Mail"><i class="icon-mention"></i></a></li>
                                                        <li><a href="{{ url('customers').'/'.$temp_extn->user_id.'?tab=sms' }}" title="SMS"><i class="icon-envelop3"></i></a></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="11" class="text-center">No Data Available</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-heading bg-teal-300">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group6">Transactions ({{count($transactions)}})</a>
                        </h6>
                    </div>
                    <div id="accordion-styled-group6" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-framed">
                                    <thead>
                                        <tr>
                                            <th>Invoice No</th>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>Service</th>
                                            <th>Plan</th>
                                            <th>Amount</th>
                                            <th>Pay Type</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($transactions as $no => $transaction)
                                            <tr>
                                                <td># {{$transaction->invoice_id}}</td>
                                                <td><a href="{{ url('customers').'/'.$transaction->user_id }}">{{$transaction->user_name}}</a></td>
                                                <td>{{$transaction->mobile}}</td>
                                                <td>{{json_decode($transaction->service_details)->name}}</td>
                                                <td>{{json_decode($transaction->plan_details)->name}}</td>
                                                <td>{{$transaction->amount}}</td>
                                                <td>{{ucfirst(str_replace('_', ' ', $transaction->payment_type))}} / {{ucfirst($transaction->pay_type)}}</td>
                                                <td>
                                                    <ul class="icons-list">
                                                        <li><a href="{{ url('customers').'/'.$transaction->user_id.'?tab=email' }}" title="Mail"><i class="icon-mention"></i></a></li>
                                                        <li><a href="{{ url('customers').'/'.$transaction->user_id.'?tab=sms' }}" title="SMS"><i class="icon-envelop3"></i></a></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="8" class="text-center">No Data Available</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /accordion with different panel styling -->    
        </div>
    </div>
@endsection

<!-- graph script -->

@push('scripts')
<script type="text/javascript">
    // Column chart
    // ------------------------------

    // Initialize chart
    google.load("visualization", "1", {packages:["corechart"]});
    @if(!empty($graph_data_transaction))
        google.setOnLoadCallback(drawColumn);
        function drawColumn() {

            // Data
            var json_string = '{{$graph_data_transaction}}';
            var data = google.visualization.arrayToDataTable(JSON.parse(json_string.replace(/&quot;/g,'"')));


            // Options
            var options_column = {
                fontName: 'Roboto',
                height: 300,
                fontSize: 12,
                chartArea: {
                    left: '20%',
                    width: '100%',
                    height: 250
                },
                animation:{
                    duration: 3000,
                    easing: 'out',
                    "startup": true,
                },
                tooltip: {
                    textStyle: {
                        fontName: 'Roboto',
                        fontSize: 13
                    }
                },
                vAxis: {
                    title: 'Amount',
                    titleTextStyle: {
                        fontSize: 13,
                        italic: false
                    },
                    gridlines:{
                        color: '#e5e5e5',
                        count: 10
                    },
                    minValue: 0
                },
                legend: {
                    position: 'top',
                    alignment: 'center',
                    textStyle: {
                        fontSize: 12
                    }
                }
            };

            // Draw chart
            var column = new google.visualization.ColumnChart($('#google-column')[0]);
            column.draw(data, options_column);
            jQuery('#earning_graph_loader').hide();
        }
    @endif
    @if(!empty($graph_data_services))
        google.setOnLoadCallback(drawDonut);
        function drawDonut() {

            var json_string = '{{$graph_data_services}}';
            var data = google.visualization.arrayToDataTable(JSON.parse(json_string.replace(/&quot;/g,'"')));

            // Options
            var options_donut = {
                fontName: 'Roboto',
                pieHole: 0.55,
                height: 300,
                width: 500,
                fontSize: 12,
                chartArea: {
                    left: 50,
                    width: '90%',
                    height: '90%'
                }
            };
            

            // Instantiate and draw our chart, passing in some options.
            var donut = new google.visualization.PieChart($('#google-donut')[0]);
            donut.draw(data, options_donut);
            jQuery('#service_graph_loader').hide();
        }
    @endif
    @if(!empty($graph_data_plans))
        google.setOnLoadCallback(drawPie);
        function drawPie() {
            var json_string = '{{$graph_data_plans}}';
            var data = google.visualization.arrayToDataTable(JSON.parse(json_string.replace(/&quot;/g,'"')));

            // Options
            var options_pie = {
                fontName: 'Roboto',
                height: 300,
                width: 500,
                pieHole: 0.55,
                chartArea: {
                    left: 50,
                    width: '90%',
                    height: '90%'
                }
            };
            

            // Instantiate and draw our chart, passing in some options.
            var pie = new google.visualization.PieChart($('#google-pie')[0]);
            pie.draw(data, options_pie);
            jQuery('#plan_graph_loader').hide();
        }
    @endif
    @if(!empty($graph_data_service_status))
        google.setOnLoadCallback(drawPie3d);
        function drawPie3d() {
            var json_string = '{{$graph_data_service_status}}';
            var serch_index = JSON.parse(json_string.replace(/&quot;/g,'"'));
            var data = google.visualization.arrayToDataTable(serch_index);
            
            var q = "Active";
            var res = Object.keys(serch_index).filter(function(val, key) {
              return serch_index[val].indexOf(q) !== -1
            })[0];

            // Options
            var options_pie_3d = {
                fontName: 'Roboto',
                is3D: true,
                height: 300,
                width: 540,
                /*
                colors:['#2ec7c9','#b6a2de', '#5ab1ef', '#ffb980', '#d87a80', '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa', '#07a2a4', '#9a7fd1'],
                */
                chartArea: {
                    left: 50,
                    width: '95%',
                    height: '95%'
                },
                pieSliceText: 'label',
                slices: {  
                    [parseInt(res)-1] : {offset: 0.2}
                }
            };


            // Instantiate and draw our chart, passing in some options.
            var pie_3d = new google.visualization.PieChart($('#google-pie-3d')[0]);
            pie_3d.draw(data, options_pie_3d);
            jQuery('#service_status_graph_loader').hide();
        }
    @endif

    app.controller('stockWatchlistController',function($scope, $http, sweet ){

        $scope.stocks = null;

        $scope.getStocks = function(){

            $http.get('{{route('admin::stock_watchlist.list')}}')
                .then(function successCallback(response) {
                    $scope.stocks = response.data;

                }, function errorCallback(response) {

                });
        };

        $scope.remove_watchlist= function($index, stock){
            sweet.show({
                title: 'Are you sure?',
                text: 'You wanted to remove stock <b>'+ stock.symbol + "</b> from watchlist?",
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Remove From Watchlist',
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true,
                html: true
            }, function(isConfirm) {
                if(isConfirm) {
                    return $http.post('{{route('admin::stock_watchlist.destroy')}}',{
                        id: stock.id
                    }).then(function successCallback(response) {
                        if(response.data.deleted == true){
                            $scope.stocks.splice($index,1);
                            sweet.show('Success','Stock successfully removed from Watchlist', 'success');
                        } else {
                            sweet.show('Error Occured! ','Please try again.', 'error');
                        }
                    }, function errorCallback(response) {
                        sweet.show('Error Occured!','Please try again.', 'error');
                    });
                } else {
                    return false;
                }
            });
        };


    });

    app.controller('serviceWatchlistController',function($scope, $http, sweet ){

        $scope.services = null;

        $scope.getServices = function(){

            $http.get('{{route('admin::services.watched_by_me')}}')
                .then(function successCallback(response) {
                    $scope.services = response.data;

                }, function errorCallback(response) {

                });
        };

        $scope.remove_watchlist= function($index, service){
            sweet.show({
                title: 'Are you sure?',
                text: 'You wanted to remove service <b>'+ service.name + "</b> from watchlist?",
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Remove From Watchlist',
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true,
                html: true
            }, function(isConfirm) {
                if(isConfirm) {
                    return $http.post('{{route('admin::services.remove_my_watched')}}',{
                        id: service.id
                    }).then(function successCallback(response) {
                        if(response.data.deleted == true){
                            $scope.services.splice($index,1);
                            sweet.show('Success','Service successfully removed from Watchlist', 'success');
                        } else {
                            sweet.show('Error Occured! ','Please try again.', 'error');
                        }
                    }, function errorCallback(response) {
                        sweet.show('Error Occured!','Please try again.', 'error');
                    });
                } else {
                    return false;
                }
            });
        };
    });

    app.controller('userWatchlistController',function($scope, $http, sweet ){

        $scope.users = null;

        $scope.getUsers = function(){

            $http.get('{{route('admin::clients.watched_by_me')}}')
                .then(function successCallback(response) {
                    $scope.users = response.data;

                }, function errorCallback(response) {

                });
        };

        $scope.remove_watchlist= function($index, user){
            sweet.show({
                title: 'Are you sure?',
                text: 'You wanted to remove user <b>'+ user.user_name + "</b> from watchlist?",
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Remove From Watchlist',
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true,
                html: true
            }, function(isConfirm) {
                if(isConfirm) {
                    return $http.post('{{route('admin::clients.add_to_watch', '')}}/'+ user.id,{
                        id: user.id
                    }).then(function successCallback(response) {
                        if(response.data.response == 1){
                            $scope.users.splice($index,1);
                            sweet.show('Success','User successfully removed from Watchlist', 'success');
                        } else {
                            sweet.show('Error Occured! ','Please try again.', 'error');
                        }
                    }, function errorCallback(response) {
                        sweet.show('Error Occured!','Please try again.', 'error');
                    });
                } else {
                    return false;
                }
            });
        };
    });

    app.filter('valueFromJson', function(){
        return function(input, column){
            data = JSON.parse(input);
            return data[column];
        }
    });

    app.controller('pendingServiceActivation',function($scope, $http, sweet ){

        $scope.users = {};
        $scope.pending_scope = "0";
        $scope.getUsers = function(){

            $http.get('{{route('admin::clients.get_pending_service_activation')}}')
                .then(function successCallback(response) {
                    $scope.users = response.data;
                }, function errorCallback(response) {

                });
        };

        $scope.updatescope = function ($index, key) {
            $scope.users[$index][key] = "0";
        };

        $scope.updatePendings= function($index, user, key){
            return sweet.show({
                title: 'Are you sure?',
                text: 'You are updating client\'s pending service.',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Update Customer Service',
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true,
                html: true
            }, function(isConfirm) {
                if(isConfirm) {
                    $http.post('{{route('admin::clients.update_pending_service_activation')}}',{
                        data_id: user.us_id, key: key
                    }).then(function successCallback(response) {
                        console.log(response);
                        if(response.data.response == 1){
                            sweet.show('Success','Updated successfully', 'success');
                        } else {
                            sweet.show('Error Occured! ','Please try again.', 'error');
                            $scope.updatescope($index, key);
                        }
                    }, function errorCallback(response) {
                        sweet.show('Error Occured!','Please try again.', 'error');
                        $scope.updatescope($index, key);
                    });
                    return true;

                } else {
                    $scope.$apply(function () {
                        $scope.users[$index][key] = "0";
                    });
                    return false;
                }
            });
        };
    });
</script>
@endpush
