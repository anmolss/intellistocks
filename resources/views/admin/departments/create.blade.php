@extends('admin.layout.app')

@section('page_title', 'Departments')
@section('page_subtitle', 'New Department')
@section('content')
<div class="panel panel-flat">
  <div  class="panel-body" >
{!! Form::open([
        'route' => 'admin::departments.store',
        ]) !!}
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
    {!! Form::label('name', 'Department name:', ['class' => 'control-label']) !!}
    {!! Form::text('name', null,['class' => 'form-control']) !!}
    @if ($errors->has('name'))
        <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    {!! Form::label('description', 'Department description:', ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
{!! Form::submit('Create New Department', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}
</div>
</div>
@endsection