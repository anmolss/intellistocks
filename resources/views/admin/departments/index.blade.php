@extends('admin.layout.app')

@section('page_title', 'Departments')
@section('page_subtitle', 'List of Departments')
@section('content')
<div class="panel panel-flat">
      <div  class="panel-body" >
            
        <table class="table table-hover">
            
            <thead>
                <thead>
                      <tr>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Date</th>
                          <th>Action</th>
                      </tr>
                </thead>
            <tbody>
            <?php if(count($department)>0) {?>
                @foreach($department as $dep)

                <tr>
                    <td>{{$dep->name}}</td>
                    <td>{{Str_limit($dep->description, 50)}}</td>
                    <td>{{$dep->created_at ? with(new Carbon($dep->created_at))->format('d/m/Y') : ''}}</td>
                    <td>  
                        <a href="<?php echo  route('admin::departments.edit', $dep->id); ?>" class="btn btn-link" >
                            <i class="icon-pencil"></i>
                        </a> 
                        <a href="<?php echo  route('admin::departments.destroy', $dep->id); ?>" class="btn btn-link btn-delete" >
                            <i class="icon-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                <?php } else {?>
                <tr>
                    <td colspan="4">
                        No Record Found.   
                    </td>
                </tr>
                <?php } ?>
          </tbody>
      </table>

    </div>

</div> 
         
@stop
