@extends('admin.layout.login')

@section('content')
    {!! BootForm::open()->post()->action(url('/password/reset')) !!}

    <div class="panel panel-body login-form">
        <div class="text-center">
            <div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
            <h5 class="content-group">Reset Password</h5>
        </div>
        <input type="hidden" name="token" value="{{ $token }}">

        {!! BootForm::inputGroup('Email Id', 'email')->value(Request::get('email'))->hideLabel()->placeholder('Email Id')->beforeAddon('<i class="icon-user text-muted"></i>') !!}
        {!! BootForm::inputGroup('Password', 'password')->type('password')->placeholder('Password')->hideLabel()->beforeAddon('<i class="icon-user-lock text-muted"></i>') !!}
        {!! BootForm::inputGroup('Confirm Password', 'password_confirmation')->type('password')->placeholder('Confirm Password')->hideLabel()->beforeAddon('<i class="icon-user-lock text-muted"></i>') !!}

        {!! BootForm::submit('Reset password <i class="icon-arrow-right14 position-right"></i>')->addClass('btn bg-pink-400 btn-block legitRipple') !!}
    </div>

    {!! BootForm::close() !!}
@endsection
