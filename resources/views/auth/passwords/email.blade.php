@extends('admin.layout.login')

<!-- Main Content -->
@section('content')

    {!! BootForm::open()->post()->action(url('/password/email')) !!}

    <div class="panel panel-body login-form">
        <div class="text-center">
            <div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
            <h5 class="content-group">Password recovery <small class="display-block">We'll send you instructions in email</small></h5>
        </div>

        {!! BootForm::inputGroup('Email Id', 'email')->hideLabel()->placeholder('Email Id')->beforeAddon('<i class="icon-user text-muted"></i>') !!}

        {!! BootForm::submit('Reset password <i class="icon-arrow-right14 position-right"></i>')->addClass('btn bg-pink-400 btn-block legitRipple') !!}
    </div>

    {!! BootForm::close() !!}

@endsection
