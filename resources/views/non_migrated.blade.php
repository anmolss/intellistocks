@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Non Migrated Users</div>

                <div class="panel-body">
                    @foreach($a AS $k => $v)
                        <div class="list-group">
                            <li class="list-group-item active">{{'Name'}} : {{$v['name']}}</li>
                            <?php unset($v['name']); ?>
                            @foreach($v AS $k1 => $v1)
                                <li class="list-group-item">{{$k1}} : {{$v1}}</li>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
