@extends('layouts.error')

@section('content')
	<div class="text-center content-group">
		<h1 class="error-title">405</h1>
		<h5>Oops, an error has occurred. Not allowed!</h5>
	</div>
@endsection