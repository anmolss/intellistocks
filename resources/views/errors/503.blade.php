@extends('layouts.error')

@section('content')
    <div class="text-center content-group">
        <h1 class="error-title">Error</h1>
        <h5>Oops, an error has occurred. Internal server error!</h5>
    </div>
@endsection