
@extends('layouts.error')

@section('content')

	<div class="text-center content-group">
		<h1 class="error-title">403</h1>
		<h5>Oops, an error has occurred. Forbidden!</h5>
	</div>

@endsection
