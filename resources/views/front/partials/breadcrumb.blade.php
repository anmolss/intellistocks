<!-- Page header content -->
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - @yield('page_title') <small>@yield('page_subtitle')</small></h4>
		</div>
	</div>
<!-- /page header content -->