		<!-- Second navbar -->
		<div class="navbar navbar-inverse navbar-transparent" id="navbar-second">
			<ul class="nav navbar-nav visible-xs-block">
				<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>

			<div class="navbar-collapse collapse" id="navbar-second-toggle">
				<ul class="nav navbar-nav navbar-nav-material">
					
				</ul>

				<ul class="nav navbar-nav navbar-nav-material navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-info22"></i>
							<span class="visible-xs-inline-block position-right">Help</span>
							<span class="caret"></span>
						</a>

						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="#"><i class="icon-question3"></i> FAQ</a></li>
							<li><a href="#"><i class="icon-envelop5"></i> Contact Us</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- /second navbar -->