	<!-- Footer -->
	<div class="navbar navbar-default navbar-fixed-bottom footer bg-footer">
		<ul class="nav navbar-nav visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="footer">
			<div class="navbar-text">
				&copy; {{date('Y')}}. {{link_to('/management', 'Rothmans CRM')}}
			</div>

			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="https://www.intellistocks.com/about" target="_blank">About</a></li>
					<li><a href="https://www.intellistocks.com/terms-condition-disclaimers" target="_blank">Terms</a></li>
					<li><a href="https://www.intellistocks.com/contact" target="_blank">Contact</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /footer -->