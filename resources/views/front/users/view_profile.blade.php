@extends('front.layouts.app')
@section('page_title', 'Profile')

@section('page_subtitle', 'My Profile')
@section('content')
	<div class="row">
		@if(isset($flash) && !empty($flash))
			<div class="panel bg-success">
				<div class="panel-heading">
					<h6 class="panel-title">Success</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="close"></a></li>
	                	</ul>
	            	</div>
	            </div>

				<div class="panel-body">
					{{$flash}}
				</div>
			</div>
		@endif
		<div class="col-sm-6">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-user position-left"></i> Personal details</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
	            	</div>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

				<table class="table table-borderless table-xs content-group-sm">
					<tbody>
						<tr>
							<td><i class="icon-vcard position-left"></i> Name:</td>
							<td class="text-right">{{(!empty($user['name']))?$user['name']:'N/A'}}</td>
						</tr>
						<tr>
							<td><i class="icon-vcard position-left"></i> Username:</td>
							<td class="text-right">{{(!empty($user['username']))?$user['username']:'N/A'}}</td>
						</tr>
						<tr>
							<td><i class="icon-mail-read position-left"></i> Email:</td>
							<td class="text-right">{{(!empty($user['email']))?$user['email']:'N/A'}}</td>
						</tr>
						<tr>
							<td><i class="icon-iphone position-left"></i> Mobile:</td>
							<td class="text-right">{{(!empty($user['mobile']))?$user['mobile']:'N/A'}}</td>
						</tr>
						<tr>
							<td><i class="icon-calendar52 position-left"></i> Joined On:</td>
							<td class="text-right">{{(!empty($user['created_at']))?(new Carbon($user['created_at']))->format('d/m/Y'):'N/A'}}</td>
						</tr>
						<tr>
							<td><i class="icon-collaboration position-left"></i> Relationship Manager:</td>
							<td class="text-right">{{(!empty($users[$user['customer_meta']['fk_relation_manager_id']]))?$users[$user['customer_meta']['fk_relation_manager_id']]:'N/A'}}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-address-book2 position-left"></i> Address details</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
	            	</div>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

				<table class="table table-borderless table-xs content-group-sm">
					<tbody>
						<tr>
							<td><i class="icon-location3 position-left"></i> Add Line 1:</td>
							<td class="text-right">{{(!empty($user['address1']))?$user['address1']:'N/A'}}</td>
						</tr>
						<tr>
							<td><i class="icon-location3 position-left"></i> Add Line 2:</td>
							<td class="text-right">{{(!empty($user['address2']))?$user['address2']:'N/A'}}</td>
						</tr>
						<tr>
							<td><i class="icon-location4 position-left"></i> City:</td>
							<td class="text-right">{{(!empty($user['city']))?$user['city']:'N/A'}}</td>
						</tr>
						<tr>
							<td><i class="icon-map position-left"></i> State:</td>
							<td class="text-right">{{(!empty($user['state']))?$user['state']:'N/A'}}</td>
						</tr>
						<tr>
							<td><i class="icon-map4 position-left"></i> Zip Code:</td>
							<td class="text-right">{{(!empty($user['zip_code']))?$user['zip_code']:'N/A'}}</td>
						</tr>
						<tr>
							<td><i class="icon-user-tie position-left"></i> Approached via:</td>
							<td class="text-right">{{(!empty($users[$user['customer_meta']['fk_source']]))?$users[$user['customer_meta']['fk_source']]:'N/A'}}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
	</div>
@endsection