@extends('front.layouts.app')

@section('page_title', 'Dashboard')
@section('page_subtitle', 'Welcome,  '.Auth::user()->name)

@section('content')
	@if(count($expiring_services) > 0)
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-body border-left-warning">
					<h3>Expiring Services in next 30 Days</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-xxs table-framed">
							<thead>
								<tr>
									<th>#</th>
									<th>Service</th>
									<th>Expiry</th>
								</tr>
							</thead>
							<tbody>
								@foreach($expiring_services as $no => $expiring_service)
									<tr>
										<td>{{$no+1}}</td>
										<td>{{json_decode($expiring_service['service_details'],true)['name']}} ( {{json_decode($expiring_service['plan_details'],true)['name']}} )</td>
										<td>{{(new Carbon($expiring_service['expire_date']))->format('d-m-Y h:i A')}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
			    </div>
			</div>
		</div>
	@endif
	<div class="row">
		<div class="col-sm-9">
			<div class="navbar navbar-default navbar-component navbar-xs text-center">
				<h4 class="navbar-text" style="float:none !important;"><i class="icon-direction"></i> <span class="text-semibold">Transactions Timeline</span></h4>
			</div>
			@if(isset($timeline_transactions) && !empty($timeline_transactions) && (count($timeline_transactions)>0))
			<!-- Timeline -->
			<div class="timeline timeline-left content-group">
				<div class="timeline-container">
					@foreach ($timeline_transactions as $date => $transactions)
						<!-- Date stamp -->
						<div class="timeline-date text-muted">
							<i class="icon-history position-left"></i><span class="text-semibold">{{(new Carbon($date))->formatLocalized('%A')}} </span>, {{(new Carbon($date))->formatLocalized('%B %d, %Y')}}
						</div>
						<!-- /date stamp -->

						@foreach (array_chunk($transactions, 2, true) as $column)
						    <!-- Invoices -->
							<div class="timeline-row">
								<div class="timeline-icon">
									<div class="bg-success">
										<i class="icon-cash3"></i>
									</div>
								</div>

								<div class="row">
						        @foreach ($column as $transaction)
						        	@if($transaction['payment_type'] == 'cash')
						        		<?php $color = 'success'; ?>
									@elseif($transaction['payment_type'] == 'cheque')
										<?php $color = 'primary'; ?>
									@elseif($transaction['payment_type'] == 'net_banking')
										<?php $color = 'warning'; ?>
									@endif
						            <div class="col-lg-6">
										<div class="panel border-left-lg border-left-{{$color}} invoice-grid timeline-content">
											<div class="panel-body">
												<div class="row">
													<div class="col-sm-6">
														<h6 class="text-semibold no-margin-top">{{json_decode($transaction['user_service']['service_details'], true)['name']}}</h6>
														<ul class="list list-unstyled">
															<li>Invoice #: &nbsp;{{($transaction['pay_type'] == 'partial')?$transaction['id'].'_'.$transaction['pay_no']:$transaction['id']}}</li>
															<li>Issued on: <span class="text-semibold">{{(new Carbon($transaction['created_at']))->format('d-m-Y h:i A')}}</span></li>
															<li>Pay Type: {{ucwords($transaction['pay_type'])}}</li>
														</ul>
													</div>

													<div class="col-sm-6">
														<h6 class="text-semibold text-right no-margin-top">Rs. {{$transaction['amount']}}</h6>
														<ul class="list list-unstyled text-right">
															<li>Paid Via: <span class="text-semibold">{{$transaction['payment_type']}}</span></li>
															@if($transaction['payment_type'] == 'cash')
															<li>Transaction No: {{$transaction['transaction_id']}}</li>
															<li>Received From: {{(!empty($transaction['received_from']))?$transaction['received_from']:'N/A'}}</li>
															@elseif($transaction['payment_type'] == 'cheque')
															<li>Cheque No: {{$transaction['cheque_number']}}</li>
															<li>Bank: {{$transaction['bank']}}</li>
															@elseif($transaction['payment_type'] == 'net_banking')
															<li>Transaction No: {{$transaction['transaction_id']}}</li>
															<li>Received From: {{(!empty($transaction['received_from']))?$transaction['received_from']:'N/A'}}</li>
															@endif
															<li>Cleared Status: {{$transaction['cleared']}}</li>
														</ul>
													</div>
												</div>
											</div>

											<div class="panel-footer panel-footer-condensed">
												<div class="heading-elements">
													<span class="heading-text">
														<span class="status-mark border-danger position-left"></span> Pay Date: <span class="text-semibold">{{$date}}</span>
													</span>
													<!--
													<ul class="list-inline list-inline-condensed heading-text pull-right">
														<li><a href="#" class="text-default" data-toggle="modal" data-target="#invoice"><i class="icon-eye8"></i></a></li>
														<li class="dropdown">
															<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="#"><i class="icon-printer"></i> Print invoice</a></li>
																<li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
															</ul>
														</li>
													</ul>
													-->
												</div>
											</div>
										</div>
									</div>
						        @endforeach
						      	</div>
							</div>
							<!-- /invoices -->
						@endforeach
					@endforeach
				</div>
			</div>
			@else
			<div class="navbar navbar-default navbar-component navbar-xs text-center">
				<h4 class="navbar-text" style="float:none !important;">No Transactions Are Made Yet</h4>
			</div>
			@endif
		</div>
		<div class="col-sm-3">
			<div class="navbar navbar-default navbar-component navbar-xs text-center">
				<h4 class="navbar-text" style="float:none !important;">
					<i class="icon-hammer-wrench"></i> <span class="text-semibold">Services and Plans</span>
				</h4>
			</div>
				@forelse($user_services as $no => $user_service)
					<div class="panel panel-flat">
						<div class="table-responsive">
							<table class="table table-xlg">
								<thead>
									<tr class="bg-intelli-blue">
										<th>{{json_decode($user_service['service_details'], true)['name']}}</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<ul class="list list-square no-margin-bottom">
												<li><span class="text-semibold">Plan :</span>{{json_decode($user_service['plan_details'], true)['name']}}</li>
												<li><span class="text-semibold">Status :</span>
													@if($user_service['service_status'] == 'Active')
														<span class="bg-success text-highlight">{{$user_service['service_status']}}</span>
													@elseif($user_service['service_status'] == 'Pending')
														<span class="bg-warning text-highlight">{{$user_service['service_status']}}</span>
													@elseif($user_service['service_status'] == 'Expired')
														<span class="bg-danger text-highlight">{{$user_service['service_status']}}</span>
													@else
														<span class="bg-slate-800 text-highlight">{{$user_service['service_status']}}</span>
													@endif
												</li>
												<li><span class="text-semibold">Expiry :</span>{{(new Carbon($user_service['expire_date']))->formatLocalized('%A')}} </span>, {{(new Carbon($user_service['expire_date']))->formatLocalized('%B %d, %Y')}}</li>
											</ul>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				@empty
					<div class="panel panel-flat text-center">
						<h4 class="text-muted p-5">No Active Services Found...!!!</h4>
					</div>
				@endforelse
		</div>
	</div>
@endsection