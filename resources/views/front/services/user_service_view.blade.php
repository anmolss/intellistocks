@extends('front.layouts.app')
@section('page_title', json_decode($user_service['service_details'], true)['name'])

@section('page_subtitle', 'View Service')
@section('content')
	<?php
	$service_details = json_decode($user_service['service_details']);
	$plan_details = json_decode($user_service['plan_details']);
	?>

	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><span class="text-bold">Service User</span></h6>
			</div>
			<div class="panel-body">
				<div class="well">
					<fieldset>
						<div class="col-sm-3">
							<dl class="dl-horizontal">
								<dt>Name : </dt>
								<dd>{{$user_service['user']['name']}}</dd>
							</dl>
						</div>
						<div class="col-sm-3">
							<dl class="dl-horizontal">
								<dt>Username : </dt>
								<dd>{{$user_service['user']['username']}}</dd>
							</dl>
						</div>
						<div class="col-sm-3">
							<dl class="dl-horizontal">
								<dt>Email : </dt>
								<dd>{{$user_service['user']['email']}}</dd>
							</dl>
						</div>
						<div class="col-sm-3">
							<dl class="dl-horizontal">
								<dt>Mobile : </dt>
								<dd>{{$user_service['user']['mobile']}}</dd>
							</dl>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><span class="text-bold">Service Details</span><span class="pull-right">(created : {{(new Carbon($user_service['created_at']))->format('d/m/Y')}}, modified : {{(new Carbon($user_service['updated_at']))->format('d/m/Y')}})</span></h6>
			</div>
			<div class="panel-body">
				<div class="well">
					<fieldset>
						<div class="col-sm-4">
							<dl class="dl-horizontal">
								<dt>Service : </dt>
								<dd>{{$service_details->name}}</dd>
								<dt>Description : </dt>
								<dd>{{$service_details->description}}</dd>
								<dt>Comments : </dt>
								<dd>{{$user_service['comments']}}</dd>
								<dt>Service Status : </dt>
								<dd>{{$user_service['service_status']}}</dd>
							</dl>
						</div>
						<div class="col-sm-4">
							<dl class="dl-horizontal">
								<dt>Activation Date : </dt>
								<dd>{{(new Carbon($user_service['activation_date']))->format('d/m/Y')}}</dd>
								<dt>Expire Date : </dt>
								<dd>{{(new Carbon($user_service['expire_date']))->format('d/m/Y')}}</dd>
								@if($user_service['service_verified'] == 'Yes')
								<dt>Force Verified : </dt>
								<dd>
									<span class="label label-success label-icon"><i class="icon-checkmark2"></i></span>
								</dd>
								<dt>Force Verify Reason : </dt>
								<dd>{{$user_service['force_verify_reason']}}</dd>
								@endif
							</dl>
						</div>

						<div class="col-sm-4">
							<dl class="dl-horizontal">
								<dt>Transcript : </dt>
								<dd>
									@if($user_service['transcript_status'] == 'Yes')
										<span class="label label-success label-icon"><i class="icon-checkmark2"></i></span>
									@else
										<span class="label label-danger label-icon"><i class="icon-cross3"></i></span>
									@endif
									&nbsp;
									@if($user_service['transcript_file'] != '')
									<a href="{{asset($user_service['transcript_file']['path'].'/'.$user_service['transcript_file']['file_name'])}}" data-loading-text="<i class='icon-spinner4 spinner position-left'></i> Downloading" class="btn btn-default btn-loading"><i class="icon-download4 position-left"></i> Download</a>
									@endif
								</dd>
								<dt>KYC : </dt>
								<dd>
									@if($user_service['kyc_status'] == 'Yes')
										<span class="label label-success label-icon"><i class="icon-checkmark2"></i></span>
									@else
										<span class="label label-danger label-icon"><i class="icon-cross3"></i></span>
									@endif
									&nbsp;
									@if($user_service['kyc_file'] != '')
									<a href="{{asset($user_service['kyc_file']['path'].'/'.$user_service['kyc_file']['file_name'])}}" data-loading-text="<i class='icon-spinner4 spinner position-left'></i> Downloading" class="btn btn-default btn-loading"><i class="icon-download4 position-left"></i> Download</a>
									@endif
								</dd>
								<dt>Consent : </dt>
								<dd>
									@if($user_service['consent_status'] == 'Yes')
										<span class="label label-success label-icon"><i class="icon-checkmark2"></i></span>
									@else
										<span class="label label-danger label-icon"><i class="icon-cross3"></i></span>
									@endif
									&nbsp;
									@if($user_service['consent_ip'] != '')
										{{$user_service['consent_ip']}}
									@elseif($user_service['service_verified'] == 'Yes' && $user_service['consent_ip'] == '')
										'Force Verified'
									@endif
								</dd>
							</dl>
						</div>
	                </fieldset>
				</div>
			</div>
		</div>
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><span class="text-bold">Service Plan</span></h6>
			</div>
			<div class="panel-body">
				<div class="well">
					<fieldset>
						<div class="col-sm-4">
							<dl class="dl-horizontal">
								<dt>Plan : </dt>
								<dd>{{$plan_details->name}}</dd>
							</dl>
						</div>
						<div class="col-sm-4">
							<dl class="dl-horizontal">
								<dt>Taxes : </dt>
								<dd>
									@foreach($plan_details->applicable_tax as $key)
										{{$taxes[$key]}}<br/>
									@endforeach
								</dd>
							</dl>
						</div>
						<div class="col-sm-4">
							<dl class="dl-horizontal">
								<dt>Duration : </dt>
								<dd>{{$plan_details->duration}}</dd>
							</dl>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><span class="text-bold">Transactions</span></h6>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover table-xxs">
							<thead>
								<tr class="bg-blue">
									<th>Invoice No.</th>
									<th>Pay Via</th>
									<th>Service Fee</th>
									<th>Applied Tax</th>
									<th>Discount</th>
									<th>Paid Amount</th>
									<th>Payment Type</th>
									<th>Recipient</th>
									<th>Cheque Number</th>
									<th>Bank / Net Pay Type</th>
									<th>Net Pay Type</th>
									<th>Transaction ID</th>
									<th>Recieved By</th>
									<th>Cleared</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody>
								
								@foreach($user_service['transections'] as $transaction)
									<tr>
										<td>{{$transaction['id']}}</td>
										<td>{{$transaction['payment_type']}}</td>
										<td>{{$transaction['service_fee']}}</td>
										<td>{{$transaction['applied_tax']}}%</td>
										<td>{{$transaction['discount']}}%</td>
										<td>{{$transaction['amount']}}</td>
										<td>{{$transaction['pay_type']}}</td>
										<td>{{$transaction['recipient']}}</td>
										<td>{{$transaction['cheque_number']}}</td>
										<td>{{$transaction['bank']}}</td>
										<td>{{$transaction['payment_type']}}</td>
										<td>{{$transaction['transaction_id']}}</td>
										<td>{{$transaction['received_from']}}</td>
										<td>{{$transaction['cleared']}}</td>
										<td>{{(new Carbon($transaction['date']))->format('d/m/Y')}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><span class="text-bold">Service Actions</span></h6>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover table-xxs">
							<thead>
								<tr class="bg-blue">
									<th>#</th>
									<th>Done By</th>
									<th>Activity</th>
									<th>Transcript Status</th>
									<th>KYC Status</th>
									<th>Consent Status</th>
									<th>Extended Days</th>
									<th>Old Expiry</th>
									<th>New Expiry</th>
									<th>Termination Reason</th>
									<th>Comments</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody>
								@foreach($user_service['user_service_action'] as $no => $action)
								<tr>
									<td>{{$no+1}}</td>
									<td>{{$action['user']->name}}</td>
									<td>{{$action['activity']}}</td>
									<td>{{$action['transcript_status']}}</td>
									<td>{{$action['kyc_status']}}</td>
									<td>{{$action['consent_status']}}</td>
									<td>{{$action['extn_days']}}</td>
									<td>{{(new Carbon($action['old_expiry_date']))->format('d/m/Y')}}</td>
									<td>{{(new Carbon($action['new_expiry_date']))->format('d/m/Y')}}</td>
									<td>{{$action['termination_reason']}}</td>
									<td>{{$action['comments']}}</td>
									<td>{{(new Carbon($action['created_at']))->format('d/m/Y')}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
<script type="text/javascript">
jQuery(document).ready(function () {
	jQuery('.btn-loading').click(function () {
        var btn = jQuery(this);
        btn.button('loading')
        setTimeout(function () {
            btn.button('reset')
        }, 2000)
    });
});
</script>
@endpush