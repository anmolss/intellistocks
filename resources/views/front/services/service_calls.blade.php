@extends('front.layouts.app')

@section('page_title', 'Service Calls')
@section('page_subtitle', 'Welcome,  '.Auth::user()->name)

@section('content')
	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Services Calls</h5>
				<div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="collapse"></a></li>
                	</ul>
            	</div>
			<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

			<div class="panel-body">
				Following Services Calls are being made to you till yet.
			</div>

			<div class="table-responsive">
				<table class="table table-striped table-hover text-center">
					<thead>
			            <tr class="bg-intelli-blue">
			              	<th>Service Name</th>
	                        <th>Date</th>
							<th>Symbol</th>
	                        <th>Call Type</th>
							<th>Quantity</th>
							<th>Price</th>
	                        <th>Message</th>
			            </tr>
					</thead>
					<tbody>
						@forelse($service_calls as $key => $service_call)
							<tr>
	                            <td>{{ $service_call->service->name }}</td>
	                            <td>{{ \Carbon\Carbon::parse($service_call->created_at)->format(config('rothmans.date_format')) }}</td>
								<td>{{ $service_call->symbol }}</td>
								<td>{{ config('rothmans.front_sd_call_type.'.$service_call->call_type ) }}</td>
								@if($service_call->sd->exchange == 'nse')
	                            	<td>{{ $service_call->qty }}%</td>
								@else
									<td>{{ $service_call->value }}</td>
								@endif
								<td>{{ $service_call->sd->price }}</td>
	                            <td align="left">{{ $service_call->msg }}</td>
	                        </tr>
				        @empty
				            <tr>
				              <td colspan="11">
				                <div class="alert bg-warning alert-rounded">
				                  <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				                  <span class="text-semibold">No Data!</span> No Record Found.
				                </div>
				              </td>
				            </tr>
				        @endforelse 
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection