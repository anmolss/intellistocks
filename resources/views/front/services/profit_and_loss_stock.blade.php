@extends('front.layouts.app')
@section('page_title', 'Profit And Loss')

@section('page_subtitle', 'View By Stock')
@section('content')
	{{--<form weclass="form-horizontal" action="#" id="customer-frm-filter" method="GET">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title text-semibold"><i class="icon-equalizer position-left"></i> Filters</h6>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-12">
						<fieldset>
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										{!! Form::label('creation_range', 'Creation Date:', ['class' => 'control-label col-lg-4']) !!}
										<div class="col-lg-8">
											{!! Form::text('delivery_range', $default_delivery_range, ['class' => 'form-control daterange-delivery_range']) !!}
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										{!! Form::label('service', 'Service:', ['class' => 'control-label col-md-2']) !!}
										<div class="col-lg-10">
											{!! Form::select('service[]', $services, $default_service, ['class' => 'form-control', 'id' => 'services', 'multiple'  ] ) !!}
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<button type="submit" class="btn btn-primary col-sm-6">Filter <i class="icon-arrow-right14 position-right"></i></button>
									<a title="Reset Filter!" id="reset_search" href="javascript:" class="btn btn-success btn-rounded col-md-5 col-md-offset-1">Reset <i class="icon-spinner11 position-right"></i></a>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</form>--}}
	<div class="panel panel-flat small" style="width: 104%;">
		<table class="table table-hover table-xxs" id="customers-table">
				<thead>
				<tr class="bg-blue">
					<th>Stock Name</th>
					<th>Status</th>
					<th>Entry date</th>
					<th>Entry Price</th>
					<th>CMP / EXIT PRICE</th>
					<th>Close / Current Date</th>
					<th>Open Profit</th>
					<th>Booked Profit</th>
					<th>Total Profit</th>
					<th>Overall Profit (%)</th>
					<th><span data-toggle="tooltip" title="Open Position / Stock Weight of Total Investment">Open Stock Weight (%)</span></th>
					<th><span data-toggle="tooltip" title="Closed Position / Stock Weight of Total Investment">Close Stock Weight (%)</span></th>
					<th><span data-toggle="tooltip" title="Stock Weight of Total Investment">Close Stock Weight (%)</span></th>
					<th>Net ROI</th>
					<th>Tgt <br> Sl</th>
					<th>Away % from Tgt and SL</th>
				</tr>
				</thead>
				<tbody>
				@foreach($pnl_stocks as $key => $stock)
					<tr>
						<th class="clickable cursor-pointer text-blue-700" data-toggle="collapse" data-parent="#customers-table" data-target=".{{str_slug($key)}}">{{ $stock['symbol'] }}+</th>
						<td>{{ $stock['status'] }}</td>
						<td>{{ $stock['entry_date'] }}</td>
						<td>{{ $stock['avg_price'] }}</td>
						<td>{{ $stock['exit_price'] }}</td>
						<td>{{ $stock['exit_date'] }}</td>
						<td>{{ $stock['open_profit'] }}</td>
						<td>{{ $stock['booked_profit'] }}</td>
						<td>{{ $stock['total_profit'] }}</td>
						<td>{{ $stock['profit_percent'] }}%</td>
						<td>{{ $stock['open_stock_weight'] }}%</td>
						<td>{{ $stock['close_stock_weight'] }}%</td>
						<td>{{ $stock['total_stock_weight'] }}%</td>
						<td>{{ $stock['net_roi'] }}%</td>

						<th>{{ $stock['target_mean'] }} <br> {{ $stock['sl_mean'] }}</th>
						<th>{{ $stock['target_p_away'] }}% <br> {{ $stock['sl_p_away'] }}%</th>
					</tr>

					<tr class="collapse {{str_slug($key)}}">
						<td colspan="17" class="no-padding">
							<table class="table bg-teal table-xxs">
								<caption class="text-center"><h3>Transaction History</h3></caption>
								<thead>
								<tr class="bg-teal-700">
									<th>Transactions Date</th>
									<th>Stock Name</th>
									<th>Price</th>
									<th>Qty</th>
									<th>Call Type</th>
									<th>Status</th>

									<th>Open Profit</th>
									<th>Booked Profit</th>
									<th>Total</th>
								</tr>
								</thead>
								<tbody>
								@foreach($stock['deliveries'] as $key => $stock)
									<tr class="bg-teal">
										<td>{{ \Carbon\Carbon::parse($stock['entry_date'])->format(config('rothmans.date_format')) }}</td>
										<td>{{ $stock['symbol'] }}</td>
										<td>{{ $stock['price'] }}</td>
										<td>{{ $stock['qty'] }}%<br>{{ $stock['curr_qty'] }}</td>
										<td>{{ config('rothmans.front_sd_call_type.'.$stock['call_type']) }}</td>
										<td>{{ $stock['status'] }}</td>
										<td>{{ $stock['open_profit'] }}</td>
										<td>{{ $stock['booked_profit'] }}</td>
										<td>{{ $stock['total_profit'] }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>

	</div>
@endsection


@push('scripts')
<script type="text/javascript" src="{{asset('bower_components/StickyTableHeaders/js/jquery.stickytableheaders.min.js') }}"></script>
<script type="text/javascript">



    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });

    $('table#customers-table').stickyTableHeaders();

</script>
@endpush