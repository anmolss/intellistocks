@extends('front.layouts.app')

@section('page_title', 'Services')
@section('page_subtitle', 'Welcome,  '.Auth::user()->name)

@section('content')
	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Active Services</h5>
				<div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="collapse"></a></li>
                	</ul>
            	</div>
			<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

			<div class="panel-body">
				Following Services are assigned to you currently.
			</div>

			<div class="table-responsive">
				<table class="table table-striped table-hover text-center">
					<thead>
						<tr class="bg-intelli-blue">
			              	<th rowspan="2" class="text-center" width="50">#</th>
			              	<th rowspan="2" class="text-center">Service</th>
			              	<th rowspan="2" class="text-center">Plan</th>
			              	<th colspan="3" class="text-center">Amount</th>
			              	<th colspan="3" class="text-center">Date</th>
			              	<th rowspan="2" class="text-center" width="85">Status</th>
			              	<th rowspan="2" class="text-center" width="125">Actions</th>
			            </tr>
			            <tr class="bg-intelli-blue">
			              	<th class="text-center" width="100">Payable</th>
			              	<th class="text-center" width="100">Paid</th>
			              	<th class="text-center" width="100">Uncleared</th>
			              	<th class="text-center" width="115">Activation</th>
			              	<th class="text-center" width="115">Expiry</th>
			              	<th class="text-center" width="125">Created</th>
			            </tr>
					</thead>
					<tbody>
						@forelse($user_services as $key => $user_service)
			            <tr>
			              <td>{{$key+1}}</td>
			              <td>{{json_decode($user_service->service_details, true)['name']}}</td>
			              <td>{{json_decode($user_service->plan_details, true)['name']}}</td>
			              <?php
			                foreach ($user_service->transections as $transaction) {
				                if($user_service->latest_transaction_history_id == $transaction->transaction_history_id){
				                  $amount_payable = $transaction->payable_amount;
				                } else {
				                  $amount_payable = 0;
				                }
				              }    
				                if(isset($user_service->transections)) {
				                    $all = $user_service->transections->toArray();
				                    if(count($all) > 0){
				                      $amount_paid = array_sum(
				                          array_map(
				                            function($item) use ($user_service)  {
				                              if($item['cleared'] == 'Yes' && $user_service->latest_transaction_history_id == $item['transaction_history_id']) { 
				                                return $item['amount']; 
				                              } else { 
				                                return 0; 
				                              } 
				                            }, $all
				                          )
				                        );
				                  }
				                }

				              $balance = round($amount_payable, 0, PHP_ROUND_HALF_UP) - round($amount_paid, 0, PHP_ROUND_HALF_UP);

			              ?>

			              <td>{{$amount_payable}} Rs.</td>
			              <td>{{$amount_paid}} Rs.</td>
			              <td>{{$balance}} Rs.</td>
			              <td>{{(new Carbon($user_service->activation_date))->format('d-m-Y')}}</td>
			              <td>{{(!empty($user_service->expire_date))?((new Carbon($user_service->expire_date))->format('d-m-Y')):'N/A'}}</td>
			              <td>{{(new Carbon($user_service->created_at))->format('d-m-Y h:i A')}}</td>
			              <td>
			                @if($user_service->service_status == 'Active')
			                  <label class="bg-success text-highlight">{{$user_service->service_status}}</label>
			                @else
			                  <label class="bg-warning text-highlight">{{$user_service->service_status}}</label>
			                @endif
			              </td>
			              <td class="text-center">
			                <ul class="icons-list">
	                            <li><a href="{{route('front::services.userServiceView', $user_service->id)}}" title="View"><i class="icon-eye2"></i></a></li>
	                        </ul>
			              </td>
			            </tr>
			          @empty
			            <tr>
			              <td colspan="11">
			                <div class="alert bg-warning alert-rounded">
			                  <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
			                  <span class="text-semibold">No Data!</span> No Services Assigned.
			                </div>
			              </td>
			            </tr>
			          @endforelse 
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection