<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'bypass_emails' => [
        'montypaliwal.biz@gmail.com',
        'jaya.paliwal28@gmail.com',
        'savitha.somanbca@gmail.com',
        'admin@yopmail.com',
    ],

    'developers' => false,

    'date_format' => 'd-m-Y',

    'payment_mode' => [
        'Prepaid', 'Postpaid'
    ],

    'plan_category' => [
        [
            'time_based' => 'Time Based', //
        ],
        [
            'profit_sharing_fixed' => 'Profit Sharing (Fixed)', //
            'profit_sharing_slab' => 'Profit Sharing (Slab)', //
        ],
    ],

    'filter_by_condition' => [
        'null' => [
            'label' => 'None'
        ],
        'ie' => [
            'label' => 'Is Equal to'
        ],
        'ntempt' => [
            'label' => 'Is not Equal to'
        ],
        'iet' => [
            'label' => 'Is Equal to'
        ],
        'intet' => [
            'label' => 'Is not Equal to'
        ],
        'gt'=> [
            'label' => 'Greater then',
            'type' => [
                'number', 'date', 'percent', 'currency'
            ]
        ],
        'gte'=> [
            'label' => 'Greater then or equal to',
            'type' => [
                'number', 'date', 'percent', 'currency'
            ]
        ],
        'lt'=> [
            'label' => 'Less then',
            'type' => [
                'number', 'date', 'percent', 'currency'
            ]
        ],
        'lte'=> [
            'label' => 'Less then or equal to',
            'type' => [
                'number', 'date', 'percent', 'currency'
            ]
        ],
        'btw'=> [
            'label' => 'Is between',
            'type' => [
                'number', 'date', 'percent', 'currency'
            ]
        ],
        'ntbtw'=> [
            'label' => 'Is not between',
            'type' => [
                'number', 'date', 'percent', 'currency'
            ]
        ],
    ],

    'billing_type' => [
        'Prepaid', 'Postpaid'
    ],

    'lead_purpose' => [
        'Invest & create wealth', 'HNI Service', 'Others'
    ],
    'lead_status' => [
        'Pending', 'Open', 'Completed', 'Lost', 'Canceled'
    ],

    'capital' => [
        '3-10 lacs', '10-25 lacs', 'Above 25 lacs', 'Not Disclosed'
    ],
    'lead_source' => [
        'Website','Call', 'Others'
    ],
    'lead_priority' => [
        'High','Medium', 'Low'
    ],
    'discount_offered' => [
        'No','Yes'
    ],
    'service_type' =>  [
        'Investment' , 'Trading', 'SIP'
    ],
    'trade_type' =>  [
        'Long', 'Long & Short',
    ],
    'service_category' =>  [
        'Retail', 'HNI'
    ],
    'call_type' =>  [
        'Percentage'
    ],
    'service_statuses' =>  [
        'Active'=>'Active',
        'Pending' => 'Pending',
        'Expired' =>'Expired',
        'Suspended' => 'Suspended'
    ],

    'instrument_type' =>  [
        'stock'=>'Equity',
        'future' => 'Future',
        'option' =>'Option'
    ],
    'exchange' =>  [
        'bse' => 'BSE', 'nse' => 'NSE', 'nfo' => 'NFO'
    ],
    'sd_call_type' =>  [
        ''=> 'Select Call Type',
        'new'=>'Buy',
        'exit'=>'Exit',
        're_entry' => 'Reentry',
        'adjust' => 'Adjusted',
        'recall' => 'Recalled',
        'partial_recall' => 'Partial Recall'
    ],
    'sd_qty_type' =>  [
        //"qty" => 'Quantity',
        'pc' => 'Percent',
        //'val' => 'Value'
    ],
    'sd_option_type' =>  [
        "ce" => 'Call',
        'pe' => 'Put',
    ],

    'soft_sms' => [
        'username' => 'rothmans',
        'password' => 'Canvas@4',
        'send' => 'ROTHMN',
    ],
    'template_type' => [
        'buy' => 'Buy',
        'sell' => 'Sell',
        'other' => 'Other',
    ],
    'partial_date_restriction' => '40',
    'allowed_stock_limit' => [
        'nfo' => [
            'min' => 0.001,
            'max' => 100
        ],
        'min' => 0.001,
        'max' => 10

    ],
    'service_duration' => [
        30 => '1 Month',
        90 => '3 Months',
        180 => '6 Months',
        270 => '9 Months',
        365 => '1 Year',
        730 => '2 Year',
        1095 => '3 Year',
        1460 => '4 Year',
        1825 => '5 Year',
    ],

    'front_sd_call_type' =>  [
        ''=> 'Select Call Type',
        'new'=>'Buy',
        'exit'=>'Sell',
        're_entry' => 'Buy',
        'adjust' => 'Adjusted',
        'recall' => 'Recalled',
        'partial_recall' => 'Partial Recall'
    ],

    'cash_utility_stock_wise' => [
        'nse' => 100,
        'bse' => 100,
        'optstk' => 100,
        'optidx' => 100,
        'futivx' => 15,
        'futstk' => 25,
        'futidx' => 15,
    ],

    'template_type' => [
        'user_sms' => 'User SMS',
        'user_email' => 'User Email',
        'alert' => 'Alert',
        //'delivery' => 'Service Delivery',
        'others' => 'Others',
    ],

    'investor_type' => [
        "" => "", 'Aggressive', 'Conservative', 'Moderator'
    ],

    'horizon' => [
        "" => "", 'Long Term', 'Mid Term', 'Short Term'
    ],

];
